module.exports = {
  apps : [
    {
      name: "sb",
      script: "server/run.js",
      watch: false,
      node_args : "",
      "max_memory_restart" : "4096M",
      
      "error_file": "/resource/log/pm2/sb-error.log",
      "out_file": "/resource/log/pm2/sb-out.log",
      "merge_logs": true,
            
      env: {
        "NODE_ENV": "development",
      },
    },
  ],
}
