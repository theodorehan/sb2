import JSONFile from 'jsonfile';
import Promise from 'bluebird';
import uuid from 'uuid';
import path from 'path';
import fse from 'fs-extra';
import range from 'lodash/range';
import remove from 'lodash/remove';

import Errors from '../Helpers/Error';

import {
  Node,
} from '../Helpers/FileNode';

import {
  WORKER_SESSION_DIR_PATH,
} from '../App';

export default class WorkerManager {
  ex;

  constructor(expressInstance) {
    this.ex = expressInstance;
  }

  removeGarbageSessions(){
    return new Promise((resolve, reject)=>{

      this.readAll()
        .then((list)=>{
          Promise.all(
            list.map((sessionData)=>{
              let {containerID} = sessionData;

              return new Promise((resolve, reject) => {
                this.delete(sessionData.sessionID)
                  .then(()=>{
                    this.ex.get('docker').deleteContainer(containerID)
                      .then(()=> resolve(), (e)=> {throw e});
                  }, (e) => {

                    console.error('Fail remove container', e);
                  });
              });
            })
          )
          .then(()=>{
            resolve();
          });

        })
    })
  }

  read(_sessionID, _cb) {
    let filepath = path.join(WORKER_SESSION_DIR_PATH, `${_sessionID}.json`);

    return new Promise((resolve, reject)=>{

      JSONFile.readFile(filepath, (_err, _data) => {
        if (_err) {
          reject(_err);
          typeof _cb === 'function' && _cb(Errors('WORKER.SESSION.NOT_FOUND'));
        } else {
          resolve(_data);
          typeof _cb === 'function' && _cb(null, _data);
        }
      })
    });

  }

  write(_sessionID, sessionData, _cb) {
    let filepath = path.join(WORKER_SESSION_DIR_PATH, `${_sessionID}.json`);
    JSONFile.writeFile(filepath, sessionData, {}, function (_err) {
      if (_err) {
        _cb(Errors('WORKER.FAILED_CREATE_SESSION'));
      } else {
        _cb(null);
      }
    })
  }

  delete(_sessionID){
    return new Promise((resolve,reject)=>{
      fse.remove(path.join(WORKER_SESSION_DIR_PATH, `${_sessionID}.json`))
        .then(()=> resolve(), ()=>reject());
    });
  }

  readAll(){
    let dirNode = Node.convertToNode(WORKER_SESSION_DIR_PATH);

    return new Promise((resolve)=>{
      dirNode.loadChildrenAll()
        .then(()=>{
          let sessionFiles = dirNode.children.filter((node)=> /\.json$/.test(node.name)).map((node) => node.name );

          Promise.all(sessionFiles.map((filename) => this.read(filename.replace(/\.json$/,''))))
            .then((list) => resolve(list));
        })
    })
  }

  createSession(studioSessionID, newSessionID) {

    return new Promise((_resolve, _reject) => {
      this.readAll()
        .then((sessionDataList)=>{
          let config = this.ex.get('config');


          let sessionData = {
            sessionID : newSessionID,
            containerID : newSessionID,
            workerConnected : false,
            workerStatus : 'none',
            studioSessionID,
            startingConfig : {
              docker_container_pool_type : config.docker_container_pool_type,
              docker_container_port_host : config.docker_container_port_host,
            },
          };

          if( config.docker_container_pool_type === 'on_port' ){
            let ranger = config.docker_container_port_range.split('~');
            let start = parseInt(ranger[0]);
            let end = parseInt(ranger[1]);
            let size = end - start;
            let remainedPort;

            let rand;
            while(true){
              rand = Math.floor(Math.random()*size) + start;
              if(
                sessionDataList.find(
                  (otherSessionData)=> otherSessionData.startingConfig.docker_container_pool_type === 'on_port' && otherSessionData.accessPort === rand
                )
              ){
                continue;
              } else {
                remainedPort = rand;
                break;
              }
            }

            sessionData.accessPort = remainedPort;
          } else {
            sessionData.accessPort = 9090;
          }

          this.write(newSessionID, sessionData, function(_err){
            if( _err ){
              _reject(_err)
            } else {
              _resolve(sessionData);
            }
          });
        });
    });

  }
}
