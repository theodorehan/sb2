import path from 'path';
import fs from 'fs';
import FormData from 'form-data';
import prettyJSON from 'prettyjson-256';
import FSLogger from './Logger';
import STDLogger from '../logger';
import Axios from 'axios';
import pm2 from 'pm2';

import {studio, dashboard, login, sessionFilter} from "./Helpers/PageRouteMiddleware";

import ip from 'ip';

export const SERVICE_BUILDER_PROJECT_ROOT = path.join(__dirname,'../../');

export const ASSIGNED_STORE_DIR_PATH = path.join(SERVICE_BUILDER_PROJECT_ROOT, '/server/DataStore/');

export const SITE_STORE_DIR = path.join(ASSIGNED_STORE_DIR_PATH, '/Site/');

export const TEMPORARY_STORE_DIR_PATH = path.join( SERVICE_BUILDER_PROJECT_ROOT, '/.temporaryStore/');

export const STUDIO_SESSION_DIR_PATH = path.join(TEMPORARY_STORE_DIR_PATH, '/studioSession/');
export const USER_SESSION_DIR_PATH = path.join(TEMPORARY_STORE_DIR_PATH, '/userSession/');

export const WORKER_SESSION_DIR_PATH = path.join(TEMPORARY_STORE_DIR_PATH, '/workerSession/');

export const PUBLISHING_META_STORE = path.join(TEMPORARY_STORE_DIR_PATH, '/publishing-meta/');

export const SITE_INFO_DIR_PATH = path.join(TEMPORARY_STORE_DIR_PATH, '/sites/');

export const USER_FS_DB_DIR = path.join(TEMPORARY_STORE_DIR_PATH, '/users/');

export const SITE_ARCHIVE_BUFFER_PATH = path.join( TEMPORARY_STORE_DIR_PATH, '/siteArchiveBuffer/');
export const LOG_DIR_PATH = path.join(SERVICE_BUILDER_PROJECT_ROOT, '/server/log/');


export const DEFAULT_WORKSPACE_PATH = path.join(SERVICE_BUILDER_PROJECT_ROOT, '../sb_instance_workspace');



async function sendFormData(url, params){
  let paramKeys = Object.keys(params);

  let formData = new FormData();
  for(let i = 0; i < paramKeys.length; i++ ){
    formData.append(paramKeys[i], params[paramKeys[i]]);
  }


  return new Promise((resolve, reject) => {
    let data = '';

    formData.submit(url, function(err, res){
      res.on('data', (chunk) => {
        // console.log(`BODY: ${chunk}`);

        data = JSON.parse(chunk.toString());
      });
      res.on('end', () => {
        resolve({data})
        data = null;
        // console.log('No more data in response.');
      });
    })
  });


}

export default function (expressInstance, AppDependencies, _ready) {
  STDLogger.info("Start real app");

  // Load config
  let config = AppDependencies.Config('./config/server.ini') || AppDependencies.Config('./config/.server.ini.sample');


  expressInstance.use(async function(req, res, next ){
    let director = Director.get();

    // console.log('HHH', req.headers);
    let proxyCookieString = `iceJWT=${req.cookies.iceJWT || ''}; iceRefreshJWT=${req.cookies.iceRefreshJWT || ''};`;

    req.ice2 = Axios.create({
      baseURL : `http://${director.SERVICE_DEFAULT_API_HOST}`,
      headers : {
        Cookie : proxyCookieString,
        'User-Agent' : req.headers['user-agent'],
        // 'Host' : req.headers['host'],
      },
    });


    req.proxyCookieString = `iceJWT=${req.cookies.iceJWT || ''}; iceRefreshJWT=${req.cookies.iceRefreshJWT || ''};`;

    // req.sendFormData = sendFormData;



    next();
  });


  expressInstance.get('/', async function(req,res,next){

    await sessionFilter(req,res);

    if( req.stash.user && req.stash.user.signed ) {
      return res.redirect(`/dashboard`);
    }

    next();
  });

  expressInstance.get('/login', async function(req,res,next){
    await sessionFilter(req,res);


    if( req.stash.user && req.stash.user.signed ) {
      return res.redirect(`/dashboard`);
    }


    next();
  });

  expressInstance.get('/studio', async function(req,res,next){


    try{
      await studio(req,res);
      next();
    } catch(e){

      res.redirect(`/login?from=${encodeURIComponent(req.originalUrl)}`);
    }
  });

  expressInstance.get('/dashboard', async function(req,res,next){

    try{
      await dashboard(req,res);
    } catch(e){
      console.error(e);
      return res.redirect(`/login?from=${encodeURIComponent(req.originalUrl)}`);
    }

    next();
  });


  AppDependencies.API(expressInstance);


  // Setting app config
  expressInstance.set('config', config);
  STDLogger.info('Config Loaded \n' + prettyJSON.render(expressInstance.get('config')));

  // Setting DB Connection
  // expressInstance.set('dbconn', DBConn(expressInstance));

  expressInstance.set('AssignedStore', new AppDependencies.AssignedStore(expressInstance));

  expressInstance.set('StudioSession', new AppDependencies.StudioSession(expressInstance));

  expressInstance.set('UserSession', new AppDependencies.UserSession(expressInstance));

  expressInstance.set('WorkerManager', new AppDependencies.WorkerManager(expressInstance));


  expressInstance.set("Monitor", new AppDependencies.Monitor(expressInstance));


  console.log('Create DockerManager');
  // expressInstance.set('docker', new AppDependencies.DockerSupport(expressInstance, config));

  // ContinuationLib(expressInstance, function (_err) {
  //   if (_err) {
  //     throw new Error('Fail to continuation.');
  //   }
  // });

  AppDependencies.API(expressInstance);

  AppDependencies.WSocket(expressInstance);

  AppDependencies.StudioRenderProxy(expressInstance);

  require('./WSocket/DirectSiteManaging').default(expressInstance, expressInstance.siteManagingIO);

  expressInstance.get('WorkerManager').removeGarbageSessions()
    .then(()=>{

      _ready();
    }, (err) => {
      console.error(err)
    })
}


let loggerInstance = null;
export class Logger {
  static get() {
    return loggerInstance;
  }

  constructor(expressInstance){
    loggerInstance = this;

    let logRootDir = LOG_DIR_PATH;

    let defaultLogDirpath = logRootDir;

    let logDirpath = defaultLogDirpath;
    if (Director.get().ServerConfig.log_absolte_dirpath) {
      logDirpath = Director.get().ServerConfig.log_absolte_dirpath;
    }

    if( fs.existsSync(logDirpath) ){
      try {
        var openFd = fs.openSync(logDirpath, 'r');
      } catch(e) {
        if( e.code == "EACCES"){
          STDLogger.error(`permission denied. check a directory ${logDirpath}`);
        } else {
          STDLogger.error(`unknown error. mail to theskyend0@gmail.com`);
        }
        process.exit(1);
      }

      var logDirStat = fs.fstatSync(openFd);

      if( !logDirStat.isDirectory() ){
        STDLogger.error(`Log directory is not directory`);
        process.exit(1);
      }
    } else {
      STDLogger.error(`No exists log directory : ${logDirpath}`);
      process.exit(1);
    }


    let fslogger = new FSLogger(path.join(logDirpath, '/report.log'));

    let accessLogger = new FSLogger(path.join(logDirpath, '/access.log'));

    let mailingLogger = new FSLogger(path.join(logDirpath, '/mailing.log'));
    let workerLogger = new FSLogger(path.join(logDirpath, '/worker.log'));
    let studio = new FSLogger(path.join(logDirpath, '/studio.log'));

    let workerContainer = new FSLogger(path.join(logDirpath, '/worker_container.log'));


    this.leveling = {
      info : fslogger.info,
      error : fslogger.error,
      warn : fslogger.warn,

      access : accessLogger.info,

      mailing : mailingLogger,
      worker : workerLogger,
      studio : studio,
      workerContainer : workerContainer,
    };


    // USER_SESSION_DIR_PATH

  }

  get info(){
    return this.leveling.info;
  }

  get error(){
    return this.leveling.error;
  }

  get warn(){
    return this.leveling.warn;
  }

  get access(){
    return this.leveling.access;
  }

  get mailing(){
    return this.leveling.mailing;
  }

  get worker(){
    return this.leveling.worker;
  }

  get studio(){
    return this.leveling.studio;
  }

  get workerContainer(){
    return this.leveling.workerContainer;
  }
}






let instance = null;
export class Director {

  /**
   * get
   * @returns {Director}
   */
  static get() {
    return instance;
  }

  express;
  _serverConfig;
  constructor(ex, AppDependencies) {
    if (instance) {
      throw new Error("Duplicated spplication super instance Constructor call.");
    }
    instance = this;

    this.express = ex;

    this._serverConfig = AppDependencies.Config('./config/server.ini') || AppDependencies.Config('./config/.server.ini.sample');

    this.createDirStructure();

    pm2.connect((err)=> {
      if (err) {
        console.error(err);
        process.exit(2);
      }

      const exitHandler = (options, exitCode) => {


        let deleteList = this.EDITABLES.map((e) => {
          return new Promise(( resolve, reject ) => {
            pm2.delete(e.id + ':by-builder', (err, apps) => {
              resolve();
              console.log('delete pm2 process');
              if (err) throw err
            });
          })
        });

        Promise.all(deleteList)
            .then(() => {
              if (options.exit) process.exit();
            })
      }

//do something when app is closing
      process.on('exit', exitHandler.bind(null,{cleanup:true}));

//catches ctrl+c event
      process.on('SIGINT', exitHandler.bind(null, {exit:true}));

// catches "kill pid" (for example: nodemon restart)
      process.on('SIGUSR1', exitHandler.bind(null, {exit:true}));
      process.on('SIGUSR2', exitHandler.bind(null, {exit:true}));

//catches uncaught exceptions
      process.on('uncaughtException', exitHandler.bind(null, {exit:true}));



      this.EDITABLES.map((e) => {

        pm2.start({
          name: e.id + ':by-builder',
          cwd:  e.installDir,
          script    : 'server-node.js',         // Script to be run
          exec_mode : 'cluster',        // Allows your app to be clustered
          instances : 2,                // Optional: Scales your app by 4
          max_memory_restart : '350M',   // Optional: Restarts your app if it reaches 100Mo
          env: {
            BUILDER_DOMAIN : this.BUILDER_DOMAIN,
            SERVICE_PORT : e.port,
            // PATH:process.env.PATH,
            FORCE_HTTP_API_HOST : this.SERVICE_DEFAULT_API_HOST,
            CONFIG_LEVEL : e.config,
            NODE_ENV : e.node_env,
            // FORCE_API_RESOURCE_HOST : this.SERVICE_DEFAULT_API_HOST,
            RUN_IN_BUILDER : true,

          }
        }, (err, apps) => {
          // pm2.disconnect();   // Disconnects from PM2
          if (err) throw err
        });
      })

    });
  }

  get ServerConfig(){
    return this._serverConfig;
  }

  get WORKSPACE_PATH(){
    return this.ServerConfig.workspace_dir || DEFAULT_WORKSPACE_PATH;
  }

  get BUILDER_DOMAIN(){
    if( process.env.NODE_ENV === 'development' ){
      return this.ServerConfig.builder_domain_development;
    } else if( process.env.NODE_ENV === 'production' ){
      return this.ServerConfig.builder_domain_production;
    }
  }


  get BUILDER_STUDIO_PORT(){
    return this.ServerConfig.builder_port_studio;
  }

  get BUILDER_STUDIO_SSL_PORT(){
    return this.ServerConfig.builder_port_studio_ssl;
  }

  get BUILDER_WORKER_PORT(){
    return this.ServerConfig.builder_port_worker;
  }

  get BUILDER_DIRECT_SITE_MANAGE_PORT(){
    return this.ServerConfig.builder_port_direct_site_manager;
  }



  get EXTERNAL_URL(){
    return this.ServerConfig.external_url;
  }

  get BUILDER_URL(){
    //${Director.get().BUILDER_DOMAIN}${Director.get().BUILDER_STUDIO_PORT


    let url = this.BUILDER_DOMAIN;
    if( this.BUILDER_STUDIO_PORT !== 80 ){
      url += ":" + this.BUILDER_STUDIO_PORT;
    }

    return url;
  }

  get GOOGLE_MAP_DEFAULT_API_KEY(){
    return this.ServerConfig.service_google_map_default_api_key;
  }

  get WORKER_KEEP_ALIVE(){
    return this.ServerConfig.docker_worker_keep_alive_mode;
  }

  get SERVICE_DEFAULT_API_HOST(){

    return this.ServerConfig.service_default_api_host;
  }


  get SERVICE_DEFAULT_API_RESOURCE_HOST(){

    return this.ServerConfig.service_default_api_resource_host;
  }

  get DEFAULT_SITE_CONFIG_LEVEL(){
    return this.ServerConfig.default_site_config_level;
  }

  get DOCKER_CONTAINER_NAMESPACE(){
    return this.ServerConfig.docker_container_namespace || 'service-builder';
  }

  get EDITABLES(){

    let items = this.ServerConfig.editableConfig.split(',');
    items = items.map((itemString) => {
      let itemString2 = itemString.trim();
      let fieldValues = itemString2.split(":");

      return {
        'id': fieldValues[0],
        'name': fieldValues[1],
        'ip': fieldValues[2],
        'port': fieldValues[3],
        'installDir': fieldValues[4],
        'node_env': fieldValues[5] || "production",
          'config': fieldValues[6] || 'prod'
      }
    })

    return items;
  }

  createDirStructure() {

    let workspacePath = this.WORKSPACE_PATH;

    if( !fs.existsSync(workspacePath) ){
      fs.mkdirSync(workspacePath);
    }

    if( !fs.existsSync(ASSIGNED_STORE_DIR_PATH) ){
      fs.mkdirSync(ASSIGNED_STORE_DIR_PATH);
    }

    if( !fs.existsSync(TEMPORARY_STORE_DIR_PATH) ){
      fs.mkdirSync(TEMPORARY_STORE_DIR_PATH);
    }

    if( !fs.existsSync(STUDIO_SESSION_DIR_PATH) ){
      fs.mkdirSync(STUDIO_SESSION_DIR_PATH);
    }

    if( !fs.existsSync(USER_SESSION_DIR_PATH) ){
      fs.mkdirSync(USER_SESSION_DIR_PATH);
    }

    if( !fs.existsSync(WORKER_SESSION_DIR_PATH) ){
      fs.mkdirSync(WORKER_SESSION_DIR_PATH);
    }

    if( !fs.existsSync(SITE_INFO_DIR_PATH) ){
      fs.mkdirSync(SITE_INFO_DIR_PATH);
    }

    if( !fs.existsSync(SITE_ARCHIVE_BUFFER_PATH) ){
      fs.mkdirSync(SITE_ARCHIVE_BUFFER_PATH);
    }

    if( !fs.existsSync(LOG_DIR_PATH) ){
      fs.mkdirSync(LOG_DIR_PATH);
    }

    if( !fs.existsSync(PUBLISHING_META_STORE) ){
      fs.mkdirSync(PUBLISHING_META_STORE);
    }
  }
  // error(_subject, _errorObject, _errorTrace)
}



