export const vue_basic = Object.freeze({
  FRONT_UI_FRAMEWORK : 'vue',

  STUDIO_INACCESSIBLE_FILES : ['node_modules','.idea', 'dist', /^\./], // excludes 스튜디오에서 직접적인 파일 접근과 쓰기 읽기가 불가능한 파일(디렉토리)목록
  TARBALL_PACKING_EXCLUDES : ['node_modules','.idea', 'dist', '.git'],
  CORE_STRUCTURED_FILES : [
    'ServiceAssembly',
    'themeInfo',
    'src',
    '.babelrc',
    'componentGathering.js',
    'nuxt.config.js',
    'package.json',
    'README.md',
    'server.js',
    'server-node.js',
    'server',
    'server.key',
    'server.crt',
    'server.csr',
  ],
});
