import JSONFile from 'jsonfile';
import Promise from 'bluebird';
import path from 'path';

import {
  SITE_INFO_DIR_PATH,
} from '../App';

export default class SiteModel {
  seq;
  id;
  siteName;
  assignedStoreID;
  masterUserSeq;
  owner;

  constructor({
    seq,
    id,
    siteName,
    assignedStoreID,
    masterUserSeq,
    owner,
  }){
    this.seq = seq;
    this.id = id;
    this.siteName = siteName;
    this.assignedStoreID = assignedStoreID;
    this.masterUserSeq = masterUserSeq;
    this.owner = owner;
  }

  static findOne(siteID) {
    return new Promise((_resolve, _reject)=>{
      JSONFile.readFile(path.join(SITE_INFO_DIR_PATH, `/${siteID}.json`), {}, function (_err, data) {

        if( _err ){
          _reject(_err);
        } else {
          _resolve(new SiteModel(data));
        }
      });
    })

  }

  save(){

  }


  toJSON(){
    return {
      seq : this.seq,
      id : this.id,
      siteName : this.siteName,
      assignedStoreID : this.assignedStoreID,
      masterUserSeq : this.masterUserSeq,
      owner : this.owner,
    }
  }
}
