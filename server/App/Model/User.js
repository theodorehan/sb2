import JSONFile from 'jsonfile';
import Promise from 'bluebird';
import path from 'path';

import {
  USER_FS_DB_DIR,
} from '../App';

export default class UserModel {
  seq;
  id;
  email;
  password;
  screenName;
  avatarImg;
  signupDate;
  active;
  blocked;
  level;
  admin;


constructor({
    seq,
    id,
    email,
    password,
    screenName,
    avatarImg,
    signupDate,
    active,
    blocked,
    level,
    admin,
    studioSessionID,
  }){

    this.seq = seq;
    this.id = id;
    this.email = email;
    this.password = password;
    this.screenName = screenName;
    this.avatarImg = avatarImg;
    this.signupDate = signupDate;
    this.active = active;
    this.blocked = blocked;
    this.level = level;
    this.admin = admin;
    this.studioSessionID = studioSessionID;
  }

  static findOneBySeq(seq) {
    return new Promise((_resolve, _reject)=>{
      JSONFile.readFile(path.join(USER_FS_DB_DIR, `/${seq}.json`), {}, function (_err, data) {

        if( _err ){
          _reject(_err);
        } else {
          _resolve(new UserModel(data));
        }
      });
    })
  }

  save(){

  }


  toJSON(){
    return {
      seq : this.seq,
      id : this.id,
      email : this.email,
      screenName : this.screenName,
      avatarImg : this.avatarImg,
      signupDate : this.signupDate,
      active : this.active,
      blocked : this.blocked,
      level : this.level,
      admin : this.admin,
      studioSessionID : this.studioSessionID,
    }
  }
}
