import pick from 'lodash/pick';

import {
  SITE_INFO_DIR_PATH,
} from '../App';

import {
  SITE_FS_ACCESS_GET_ROUTES_TREE,
  GET_SITE_INFO_MODEL,
} from '../Helpers/Site';

import {
  Logger,
} from '../App';

import UserWorkspaceManager from '../Helpers/Service/UserWorkspaceManager';

import * as DEFINE_STRUCTURE_TYPES from '../foundationData/service/defineStructureTypes'

import Errors from '../Helpers/Error';
import UserModel from '../Model/User';
import CertifiedDataSet from '../Helpers/CertifiedDataSet';

export default function (expressInstance) {



  expressInstance.use('/api/*', function(req, res, next){
    req.ex = expressInstance;
    req.CertifiedDataSet = new CertifiedDataSet();



    next();
  });

  expressInstance.use('/api/user/:userSessionID/*', async function(req, res, next){
    let { userSessionID } = req.params;


    // let header = {};
    // if( req.cookies.iceJWT && req.cookies.iceRefreshJWT ){
    //   header =  {
    //     Cookie : `iceJWT=${req.cookies.iceJWT || ''}; iceRefreshJWT=${req.cookies.iceRefreshJWT || ''};`,
    //   };
    // }
    //
    // let meResponse = await req.ice2.request({
    //   url : '/session/me',
    //   method : 'get',
    //   withCredentials:true,
    //   headers : header,
    // });
    //
    // let {
    //   data : userData,
    // } = meResponse
    //
    //
    // req.CertifiedDataSet.userModel = {
    //   "seq": 0,
    //   "id": "system",
    //   "email": "jessicajones@ice2.com",
    //   "password": "system",
    //   "screenName": "Jessica Jones",
    //   "avatarImg": "/public/images/uxceo-128.jpg",
    //   "signupDate": 1495084195444,
    //   "active": true,
    //   "blocked": false,
    //   "level": 1,
    //   "admin": true
    // };
    // req.CertifiedDataSet.userSessionData = {
    //   "seq": 0,
    //   "id": "system",
    //   "email": "jessicajones@ice2.com",
    //   "screenName": "Jessica Jones",
    //   "avatarImg": "/public/images/uxceo-128.jpg",
    //   "signupDate": 1495084195444,
    //   "active": true,
    //   "blocked": false,
    //   "level": 1,
    //   "admin": true
    // };



    expressInstance.get("UserSession").read(userSessionID)
      .then(( userSessionData )=>{
        UserModel.findOneBySeq(userSessionData.seq)
          .then((userModel)=>{

            /**
             * CertifiedDataSet Add
             *  userModel
             */
            req.CertifiedDataSet.userModel = { ...pick(userModel, ['id', 'seq']) };
            req.CertifiedDataSet.userSessionData = { ...pick(userSessionData, []) };


            console.log(req.CertifiedDataSet.userModel);
            console.log(req.CertifiedDataSet.userSessionData);

            next();
          },(err)=>{
            let responseError = Errors('USER.SESSION.INVALID');

            req.tracer.errorEmit(err, 'user', {
              responseError,
            });

            next(responseError);
          })
      }, ( err )=>{
        let responseError = Errors('USER.SESSION.NOT_FOUND');

        req.tracer.errorEmit(err, 'user', {
          responseError,
        });

        next(responseError);
      });
  })

  expressInstance.use('/api/user/:userSessionID/site/:siteID/:siteOwner/*', function(req, res, next){
    let { siteID, userSessionID } = req.params;

    /**
     * CertifiedDataSet Add
     *  siteModel
     */
    /**
     * req.userWorkspace
     */
    let userWorkspaceManager = new UserWorkspaceManager(expressInstance, req.CertifiedDataSet.userModel);
    userWorkspaceManager.readSiteInfo(siteID)
      .then((info)=>{
        req.CertifiedDataSet.siteModel = info;
        req.userWorkspace = userWorkspaceManager;

        next();
      }, (err) => {

        let responseError = Errors('SITE.FAIL_ACCESS.NO_PERMISSION');
        req.tracer.errorEmit(err, 'site', {
          siteID,
          userSessionID,
          responseError,
        });

        next(responseError);
      });
    //
    // return;
    // console.log('user workspace path', userWorkspaceManager.getWorkpacePath());
    //
    // GET_SITE_INFO_MODEL(siteID)
    //   .then((siteModel)=>{
    //
    //
    //     if( siteModel.masterUserSeq === req.CertifiedDataSet.userModel.seq ){
    //       /**
    //        * CertifiedDataSet Add
    //        *  siteModel
    //        */
    //       req.CertifiedDataSet.siteModel = siteModel;
    //
    //       next();
    //     } else {
    //       let responseError = Errors('SITE.FAIL_ACCESS.NO_PERMISSION');
    //
    //       req.tracer.errorEmit(err, 'site', {
    //         siteID,
    //         userSessionID,
    //         responseError,
    //       });
    //
    //       next(responseError);
    //     }
    //   }, (err)=>{
    //
    //     let responseError = Errors('SITE.FAIL_ACCESS.SITE_NO_EXISTS');
    //     req.tracer.errorEmit(err, 'site', {
    //       siteID,
    //       userSessionID,
    //       responseError,
    //     });
    //
    //     next(responseError);
    //   });
  });

  expressInstance.use('/api/user/:userSessionID/site/:siteID/:siteOwner/store/*', function(req, res, next){
    let { siteID } = req.params;
    let { siteModel } = req.CertifiedDataSet;



    /**
     * CertifiedDataSet Add
     *  storeDefine
     *  storeStructure
     */
    req.CertifiedDataSet.storeDefine = siteModel;
    req.CertifiedDataSet.storeStructure = DEFINE_STRUCTURE_TYPES[siteModel.SERVICE_STRUCTURE_TYPE];

    if( !req.CertifiedDataSet.storeStructure ){
      let responseError = Errors('SITE.STORE.INVALID_STRUCTURE_TYPE');
      req.tracer.errorEmit(responseError, 'site_store', {
        siteID,
        structureType : siteModel.SERVICE_STRUCTURE_TYPE,
      });

      next(responseError);
      // res.status(responseError.statusCode).json(responseError);
    } else {
      next();
    }
  });
}
