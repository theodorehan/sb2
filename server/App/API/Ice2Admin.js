var Promise = require("bluebird");
import parse5 from 'parse5';
import ComponentNode from 'abstract-component-node';
import btoa from 'btoa';

export default function (expressInstance) {

  expressInstance.get('/api/ice2admin/contents-edit', function (req, res) {

    let {
      baseSchemeUrl,
      id,
      siteId,
      siteOwner,
      sessionID,
    } = req.query; // 어드민 체크

    // 어드민이 아니면 튕겨낸다.

    //http://builder.localhost:8090/api/ice2admin/contents-edit?baseSchemeUrl=/node/blog&id=1&siteOwner=system&siteId=theme-modern.5824a43c-7ce7-466a-813c-6be68547873b

    let directiveName = `@contents@`;
    let querySet = `baseSchemeUrl=${baseSchemeUrl}&id=${id}`;
    directiveName += btoa(querySet);

    let queryMap = {
      'constraint-mode' : '', // 제약 모드
      'mode' : '',
      'allows' : 'others', // 허용하는 에디팅 요소 contents, curation, others
      'tools': 'settings,resource', // 사용 가능한 코어툴 ,로 구분
      'canvastools' : 'contents-palette,mini-modifier', // 사용 가능한 Canvas Tool ,로 구분
      // 'route' : route, // 현재 페이지 파라미터
      'sessionID' : sessionID, // 세션아이디 , 어드민/빌더 공통 세션
      'site' : siteId, // 사이트 ID
      'owner' : siteOwner, // 소유자 ID
      'layout' : 'central', // 기본으로 사용할 layout ,로 구분 [left,right,top,bottom,central]
      'deviceMode' : 'desktop',
      directiveName,
    }

    let queryString = Object.keys(queryMap).map((key)=>`${key}=${queryMap[key]}`).join('&');
    console.log(queryString);
    res.redirect(`/studio?${queryString}`);
  });


  expressInstance.get('/api/ice2admin/curation-edit', function (req, res) {

    let {
      baseSchemeUrl,
      id,
      siteId,
      siteOwner,
      sessionID,
    } = req.query; // 어드민 체크

    // 어드민이 아니면 튕겨낸다.


    let directiveName = `@content@`;
    let querySet = `baseSchemeUrl=${baseSchemeUrl}&id=${id}`;
    directiveName += btoa(querySet);

    let queryMap = {
      'constraint-mode' : '', // 제약 모드
      'mode' : 'curation',
      'allows' : 'others', // 허용하는 에디팅 요소 contents, curation, others
      'tools': 'settings,resource', // 사용 가능한 코어툴 ,로 구분
      'canvastools' : 'contents-palette,mini-modifier', // 사용 가능한 Canvas Tool ,로 구분
      // 'route' : route, // 현재 페이지 파라미터
      'sessionID' : sessionID, // 세션아이디 , 어드민/빌더 공통 세션
      'site' : siteId, // 사이트 ID
      'owner' : siteOwner, // 소유자 ID
      'layout' : 'central', // 기본으로 사용할 layout ,로 구분 [left,right,top,bottom,central]
      'deviceMode' : 'desktop',
      directiveName,
    }

    let queryString = Object.keys(queryMap).map((key)=>`${key}=${queryMap[key]}`).join('&');

    res.redirect(`/studio?${queryString}`);
  });
}
