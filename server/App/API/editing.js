import uuid from 'uuid';
import path from 'path';

import multiPartUpload from 'multer';
import base64Img from 'base64-img';
import Promise from "bluebird";
import Errors from '../Helpers/Error';

import {
  createSiteManager,
  ACCESSIBLE_LEVEL_STUDIO,
} from '../Helpers/Service/SiteManager';

import Site from '../Helpers/Service/Site';


import UserWorkspaceManager from '../Helpers/Service/UserWorkspaceManager';

import {directiveNameIceSchema, retrieve , save} from "../Helpers/IceServiceFragment";

export default function (expressInstance) {

  expressInstance.get('/api/user/:userSessionID/site/:siteID/:siteOwner/store/getRouteDirectiveTree', function (req, res, next) {
    let { directiveName, directiveNameFallback } = req.query;


    let fallbackSchema = directiveNameFallback ? directiveNameIceSchema(directiveNameFallback) : null;
    let schema = directiveNameIceSchema(directiveName);
    if( schema ){
        console.log("=============== schema", schema);
      retrieve(schema, fallbackSchema,req.ice2)
        .then((item) => {
          req.result = {
            treeJSON : item.json,
            serviceFragmentId : item.serviceFragmentId,
            item : item,
          };

          next();
        })
    } else {
      let userWorkspaceManger = new UserWorkspaceManager(expressInstance, req.CertifiedDataSet.userModel);
      userWorkspaceManger.getSiteManager(req.CertifiedDataSet, ACCESSIBLE_LEVEL_STUDIO)
        .then((siteManager)=>{

          siteManager.getRouteDirective(directiveName).then(
            (treeJSON)=>{

              req.result = {
                treeJSON,
                item : item,
              };
              next();
            }, (err) => {
              next(err);
            })
        })
    }
  });

  expressInstance.post('/api/user/:userSessionID/site/:siteID/:siteOwner/store/modify-directive-json', function (req, res, next) {
    let { directiveName, directiveJSON, id = null, description, publishNow } = req.body;


      let schema = directiveNameIceSchema(directiveName);
      if( schema ) {
          if (id !== null) {
              schema.serviceFragmentId = id;
          }
          save(schema, directiveJSON, req.ice2, description, publishNow, req.proxyCookieString)
              .then((result) => {

                  console.log(result);

                  let {json, serviceFragmentId, version} = result;

                  req.result = {
                      version: version,
                      directiveJSON: json,
                      serviceFragmentId: serviceFragmentId,
                      result,
                  };

                  next();
              }, (err) => {
                  console.error(err);

                  next(Errors('SITE.MODIFY.FAIL_SAVE_FRAGMENT'));
              });

      }
          return;

    let userWorkspaceManger = new UserWorkspaceManager(expressInstance, req.CertifiedDataSet.userModel);

    console.log("userWorkspaceManger",userWorkspaceManger);
    userWorkspaceManger.getSiteManager(req.CertifiedDataSet, ACCESSIBLE_LEVEL_STUDIO)
      .then((siteManager)=>{
        console.log('get siteManager', siteManager);
        let schema = directiveNameIceSchema(directiveName);
        if( schema ){
          if( id !== null  ){
            schema.serviceFragmentId = id;
          }
          save(schema, directiveJSON, req.ice2, description,publishNow, req.proxyCookieString )
            .then((result) => {

              console.log(result);

              let {json, serviceFragmentId, version} = result;

              let sitePath = siteManager.getSiteDirPath();
              let site = new Site(sitePath);
              site.modified(Date.now())
                .then(() => {
                  req.result = {
                    version : version,
                    directiveJSON : json,
                    serviceFragmentId:serviceFragmentId,
                    result,
                  };
                  next();
                });
            }, (err) => {
              console.error(err);

              next(Errors('SITE.MODIFY.FAIL_SAVE_FRAGMENT'));
            });

        } else {
          siteManager.modifyRouteDirective(directiveName, directiveJSON).then(
            (result) => {
              let {filePathInSiteRoot, directiveJSON} = result;

              let sitePath = siteManager.getSiteDirPath();
              let site = new Site(sitePath);
              site.modified(Date.now())
                .then(() => {
                  req.result = {
                    directiveJSON,
                  };
                  next();
                })


            }, (err) => {
              next(err);
            })
        }
      })
  });

  expressInstance.get('/api/user/:userSessionID/site/:siteID/:siteOwner/store/create-new-page', function (req, res, next) {
    let {path, name} = req.query;
    console.log(path, name);
    let userWorkspaceManger = new UserWorkspaceManager(expressInstance, req.CertifiedDataSet.userModel);
    userWorkspaceManger.getSiteManager(req.CertifiedDataSet, ACCESSIBLE_LEVEL_STUDIO)
      .then((siteManager)=>{

        siteManager.createNewPage(path, name)
          .then((node)=>{

            let sitePath = siteManager.getSiteDirPath();
            let site = new Site(sitePath);
            site.modified(Date.now())
              .then(()=>{
                res.result = {
                  pageNode : node,
                }

                next();
              })

          });
      })

  });
}
