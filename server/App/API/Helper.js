var Promise = require("bluebird");
import parse5 from 'parse5';
import ComponentNode from 'abstract-component-node';


var template = `<div>
    <banner-topbanner></banner-topbanner>
    <fragments-header header-data-retrieve-key="mainCategory"></fragments-header>
    <!-- content_wrap -->
    <main id="content_wrap">
      <section id="contents">

        <!-- lnb -->
        <div class="in_contents">
          <div class="in_container">
            <!-- lnb_contents_box -->
            <div class="lnb_contents_box">
              <!-- lnb -->
              <servicecenter-lnb></servicecenter-lnb>
              <!-- // lnb -->

              <!-- lnb_contents -->
              <div class="lnb_contents">
                <div class="contents_box">

                  <!-- faq_srch_box -->
                  <div class="faq_srch_box">
                    <div class="tit">
                      <p><span>FAQ</span></p>
                      <p>Search</p>
                    </div>
                    <div class="field">
                      <div class="field_inbox">
                        <input type="text" class="txt big">
                        <a href="#"><span>검색</span></a>
                      </div>
                    </div>
                    <div class="info">
                      <div class="info_inbox">
                        <p>고객센터 이용안내</p>
                        <p class="mont">02-398-8880</p>
                        <p><span>평일</span><span class="mont">09:00~18:00</span></p>
                      </div>
                      <div class="info_inbox">
                        <a href="#" class="btn"><span>1:1 문의하기</span></a>
                        <a href="#" class="btn"><span>1:1 답변확인</span></a>
                      </div>
                    </div>
                  </div>
                  <!-- // faq_srch_box -->

                  <!-- faq_cat_box -->
                  <div class="faq_cat_box">
                    <div class="faq_cat_inbox">
                      <p class="tit">주문/배송/결제</p>
                      <ul>
                        <li><a href="#">배송</a></li>
                        <li><a href="#">결제</a></li>
                        <li><a href="#">주문</a></li>
                        <li><a href="#">현금영수증</a></li>
                      </ul>
                    </div>

                    <div class="faq_cat_inbox">
                      <p class="tit">취소/반품/교환/환불</p>
                      <ul>
                        <li><a href="#">취소</a></li>
                        <li><a href="#">교환/반품</a></li>
                        <li><a href="#">환불</a></li>
                      </ul>
                    </div>

                    <div class="faq_cat_inbox">
                      <p class="tit">회원혜택</p>
                      <ul>
                        <li><a href="#">Y포인트</a></li>
                        <li><a href="#">쿠폰</a></li>
                        <li><a href="#">회원등급</a></li>
                      </ul>
                    </div>

                    <div class="faq_cat_inbox">
                      <p class="tit">모바일 이용안내</p>
                      <ul>
                        <li><a href="#">모바일 웹</a></li>
                        <li><a href="#">모바일 앱</a></li>
                      </ul>
                    </div>

                    <div class="faq_cat_inbox">
                      <p class="tit">기타</p>
                      <ul>
                        <li><a href="#">상품</a></li>
                        <li><a href="#">사이트 이용</a></li>
                      </ul>
                    </div>
                  </div>
                  <!-- // faq_cat_box -->

                  <!-- service_link_box -->
                  <div class="srv_link_box">
                    <div class="srv_tit">
                      <p>서비스<br>바로가기</p>
                    </div>
                    <div class="srv_link">
                      <ul>
                        <li><a href="#"><span>주문/배송조회</span></a></li>
                        <li><a href="#"><span>취소/교환/반품</span></a></li>
                        <li><a href="#"><span>현금영수증 발급</span></a></li>
                        <li><a href="#"><span>Y포인트 안내</span></a></li>
                        <li><a href="#"><span>회원혜택</span></a></li>
                      </ul>
                    </div>
                  </div>
                  <!-- // service_link_box -->

                  <!-- rec_list_box faq_top5_box -->
                  <div class="rec_list_box faq_top5_box">
                    <p class="tit">자주찾는 질문 TOP5</p>
                    <ul>
                      <li><a href="#" class="btn_modal" data-src="faq_top5"><span class="no">1</span><span class="subject">[결제] 비회원도 주문결제가 가능한가요?비회원도 주문결제가 가능한가요?비회원도 주문결제가 가능한가요?</span></a></li>
                      <li><a href="#" class="btn_modal" data-src="faq_top5"><span class="no">2</span><span class="subject">[기타] 신용카드매출전표는 어떻게 발급 받나요?</span></a></li>
                      <li><a href="#" class="btn_modal" data-src="faq_top5"><span class="no">3</span><span class="subject">[주문] 주문했는데 배송지 변경, 제작문구를 변경하고 싶습니다.</span></a></li>
                      <li><a href="#" class="btn_modal" data-src="faq_top5"><span class="no">4</span><span class="subject">[기타] 브라우저 호환성 설정 안내</span></a></li>
                      <li><a href="#" class="btn_modal" data-src="faq_top5"><span class="no">5</span><span class="subject">[결제] 해외발급 결제수단 이용 안내</span></a></li>
                    </ul>
                  </div>
                  <!-- // rec_list_box faq_top5_box -->

                  <!-- notice_box -->
                  <div class="rec_list_box notice_box">
                    <p class="tit">공지사항</p>
                    <ul>
                      <li><a href="#"><span class="subject">[결제] 비회원도 주문결제가 가능한가요?</span></a></li>
                      <li><a href="#"><span class="subject">[기타] 신용카드매출전표는 어떻게 발급 받나요?</span></a></li>
                      <li><a href="#"><span class="subject">[주문] 주문했는데 배송지 변경, 제작문구를 변경하고 싶습니다.</span></a></li>
                      <li><a href="#"><span class="subject">[기타] 브라우저 호환성 설정 안내</span></a></li>
                      <li><a href="#"><span class="subject">[결제] 해외발급 결제수단 이용 안내</span></a></li>
                    </ul>
                    <a href="#" class="view_more"><span>공지사항 더보기</span></a>
                  </div>
                  <!-- // notice_box -->

                </div>
              </div>
              <!-- // lnb_contents -->
            </div>
            <!-- // lnb_contents_box -->
          </div>
        </div>
        <!-- // lnb -->


        <!-- 자주찾는 질문 TOP 5 모달 -->
        <div class="modal faq_top5">
          <div class="modal_layer">
            <div class="modal_header">
              <p class="tit">자주 찾는 질문 TOP5</p>
            </div>
            <div class="modal_body">
              <div class="faq_tit">1. [주문] 쇼핑백에 담은 상품은 얼마간 보관되나요?</div>
              <div class="faq_memo">
                <p>배송완료 상태이나 아직 수령하지 못하신 경우는 아래 내용을 참고 부탁드립니다. 배송지 부재 상태로 인근에 보관되었을 수 있습니다. [경비실, 소화전, 인근 편의점(점포), 옆집 등 ] &nbsp; 실제 수령을 하지 못하신 상태라면 배송 상태에서 조회되는 운송장을 바탕으로 해당 택배사에 배송완료 상태이나 아직 수령하지 못하신 경우는 아래 내용을 참고 부탁드립니다. 배송완료 상태이나 아직 수령하지 못하신 경우는 아래 내용을 참고 부탁드립니다. 배송지 부재 상태로 인근에 보관되었을 수 있습니다. [경비실, 소화전, 인근 편의점(점포), 옆 집 등 ] &nbsp; 실제 수령을 하지 못하신 상태라면 배송 상태에서 조회되는 운송장을 바탕으로 해당 택배사에 배송완료 상태이나 아직 수령하지 못하신 경우는 아래 내용을 참고 부탁드립니다. 배송완료 상태이나 아직 수령하지 못하신 경우는 아래 내용을 참고 부탁드립니다. 배송지 부재 상태로 인근에 보관되었을 수 있습니다. [경비실, 소화전, 인근 편의점(점포), 옆집 등 ]배송완료 상태이나 아직 수령하지 못하신 경우는 아래 내용을 참고 부탁드립니다. 배송지 부재 상태로 인근에 보관되었을 수 있습니다. [경비실, 소화전, 인근 편의점(점포), 옆집 등 ] &nbsp; 실제 수령을 하지 못하신 상태라면 배송 상태에서 조회되는 운송장을 바탕으로 해당 택배사에 배송완료 상태이나 아직 수령하지 못하신 경우는 아래 내용을 참고 부탁드립니다. 배송완료 상태이나 아직 수령하지 못하신 경우는 아래 내용을 참고 부탁드립니다. 배송지 부재 상태로 인근에 보관되었을 수 있습니다. [경비실, 소화전, 인근 편의점(점포), 옆 집 등 ] &nbsp; 실제 수령을 하지 못하신 상태라면 배송 상태에서 조회되는 운송장을 바탕으로 해당 택배사에 배송완료 상태이나 아직 수령하지 못하신 경우는 아래 내용을 참고 부탁드립니다. 배송완료 상태이나 아직 수령하지 못하신 경우는 아래 내용을 참고 부탁드립니다. 배송지 부재 상태로 인근에 보관되었을 수 있습니다. [경비실, 소화전, 인근 편의점(점포), 옆집 등 ]배송완료 상태이나 아직 수령하지 못하신 경우는 아래 내용을 참고 부탁드립니다. 배송지 부재 상태로 인근에 보관되었을 수 있습니다. [경비실, 소화전, 인근 편의점(점포), 옆집 등 ] &nbsp; 실제 수령을 하지 못하신 상태라면 배송 상태에서 조회되는 운송장을 바탕으로 해당 택배사에 배송완료 상태이나 아직 수령하지 못하신 경우는 아래 내용을 참고 부탁드립니다. 배송완료 상태이나 아직 수령하지 못하신 경우는 아래 내용을 참고 부탁드립니다. 배송지 부재 상태로 인근에 보관되었을 수 있습니다. [경비실, 소화전, 인근 편의점(점포), 옆 집 등 ] &nbsp; 실제 수령을 하지 못하신 상태라면 배송 상태에서 조회되는 운송장을 바탕으로 해당 택배사에 배송완료 상태이나 아직 수령하지 못하신 경우는 아래 내용을 참고 부탁드립니다. 배송완료 상태이나 아직 수령하지 못하신 경우는 아래 내용을 참고 부탁드립니다. 배송지 부재 상태로 인근에 보관되었을 수 있습니다. [경비실, 소화전, 인근 편의점(점포), 옆집 등 ]배송완료 상태이나 아직 수령하지 못하신 경우는 아래 내용을 참고 부탁드립니다. 배송지 부재 상태로 인근에 보관되었을 수 있습니다. [경비실, 소화전, 인근 편의점(점포), 옆집 등 ] &nbsp; 실제 수령을 하지 못하신 상태라면 배송 상태에서 조회되는 운송장을 바탕으로 해당 택배사에 배송완료 상태이나 아직 수령하지 못하신 경우는 아래 내용을 참고 부탁드립니다. 배송완료 상태이나 아직 수령하지 못하신 경우는 아래 내용을 참고 부탁드립니다. 배송지 부재 상태로 인근에 보관되었을 수 있습니다. [경비실, 소화전, 인근 편의점(점포), 옆 집 등 ] &nbsp; 실제 수령을 하지 못하신 상태라면 배송 상태에서 조회되는 운송장을 바탕으로 해당 택배사에 배송완료 상태이나 아직 수령하지 못하신 경우는 아래 내용을 참고 부탁드립니다. 배송완료 상태이나 아직 수령하지 못하신 경우는 아래 내용을 참고 부탁드립니다. 배송지 부재 상태로 인근에 보관되었을 수 있습니다. [경비실, 소화전, 인근 편의점(점포), 옆집 등 ]</p>
              </div>
            </div>
            <div class="modal_close">
              <a href="#"><span>닫기</span></a>
            </div>
          </div>
        </div>
        <!-- // 자주찾는 질문 TOP 5 모달 -->

      </section>
    </main>
    <!-- // content_wrap -->

    <fragments-footer></fragments-footer>
    <fragments-wing></fragments-wing>

  </div>`;

export default function (expressInstance) {

  expressInstance.get('/api/helper/template-converter', function (req, res) {

    res.send(`<!DOCTYPE html>
      <html>
        <head>
          <title>Hello</title>
        </head>
        <body>
          <form method="post" action="/api/helper/template-conv">
            <textarea name="template" style="max-width:100%;min-width:100%;width:100%;display: inline-block;box-sizing: border-box; border: 1px solid #ececec;
    background-color: #fff;
    box-shadow: 0 0 3px 0px #efefef;
    border-radius: 10px;
    font-size: 12px;
    outline:none;
    color: #777;" rows="30">${template}</textarea>
    
            <input type="submit" style="
    width: 150px;
    height: 30px;
    display: block;
    outline:none;
    border: 1px solid #ececec;
    background-color: #fff;
    box-shadow: 0 0 3px 0px #efefef;
    border-radius: 10px;
    font-size: 12px;
    color: #777;
" value="Convert"/>
          </form>
        </body>
      </html>`);
  });

  expressInstance.post('/api/helper/template-conv', function (req, res) {
      let {template} = req.body;

      let vDom = parse5.parseFragment(template.trim());




      let nodeName, attr;
      let result = parse5RecursiveMapToNode(vDom.childNodes[0], function(elementNode){
        nodeName = elementNode.nodeName;

        if( nodeName === '#comment' ){
          return null;
        } else if( nodeName === '#text'  ){

          return {
            tag : '#text',
            nodeValue : elementNode.value,
          }
        }
        let result = {
          tag : nodeName,
        };

        if( Array.isArray(elementNode.attrs)){
          let props = {};
          let attrs = {};

          for( let i = 0; i < elementNode.attrs.length; i++ ){
            attr = elementNode.attrs[i];

            if( /^(class|id|name|href|src|style)$/.test(attr.name)){
              attrs[attr.name] = attr.value;
            } else if ( /^:/.test(attr.name) ){

            } else {
              props[attr.name] = attr.value;
            }
          }

          result.props = props;
          result.attrs = attrs;
        }

        return result;
      });


      res.json(result);
  });
}


function parse5RecursiveMapToNode(_node, func){
  var node = _node;

  let nodeResult = func(node);

  if( node.childNodes ){
    nodeResult.children = [];

    var childNode, childComponentNode;
    for(var i = 0; i < node.childNodes.length ; i++ ){
      childNode = node.childNodes[i];

      childComponentNode = parse5RecursiveMapToNode(childNode, func);
      if( childComponentNode ){
        nodeResult.children.push(childComponentNode);
      }
    }
  }

  return nodeResult;
}
