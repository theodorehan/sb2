export default function (expressInstance) {
  expressInstance.get('/api/user/:userSessionID/site/:siteID/:siteOwner/create-studio-session', function(req, res, next){
    let {userSessionID, siteID} = req.params;


    expressInstance.get('StudioSession').createSession(userSessionID, siteID)
      .then(function(studioSessionID){

        req.result = {
          studioSessionID,
        };

        next();
      }, function(err){
        console.error(err);
        next(err);
      });

  });
}
