import uuid from 'uuid';
import path from 'path';
import range from 'lodash/range';

import multiPartUpload from 'multer';
import base64Img from 'base64-img';
import Promise from "bluebird";

import {
  createSiteManager,
  ACCESSIBLE_LEVEL_STUDIO,
} from '../Helpers/Service/SiteManager';

import Site from '../Helpers/Service/Site';

import UserWorkspaceManager from '../Helpers/Service/UserWorkspaceManager';

import {Director} from '../App';

export default function (expressInstance) {

  expressInstance.get('/api/user/:userSessionID/site/:siteID/:siteOwner/publish/policy', function (req, res, next) {
    let { directiveName } = req.query;
    let config = Director.get().ServerConfig;

    req.userWorkspace.getSiteManager(req.CertifiedDataSet, ACCESSIBLE_LEVEL_STUDIO)
      .then((siteManager)=>{

        let result = {
          policy : {
            fields : [
              {
                name : 'port',
                type : 'number',
              },
            ],

            enabledPorts : range(33030,33050),
          },
        };

        req.result = result;
        next();
      });
  });

  expressInstance.get('/api/user/:userSessionID/site/:siteID/:siteOwner/publish/states', function (req, res, next) {

    req.userWorkspace.getSiteManager(req.CertifiedDataSet, ACCESSIBLE_LEVEL_STUDIO)
      .then((siteManager)=>{
        let sitePath = siteManager.getSiteDirPath();

        let site = new Site(sitePath);
        site.getPublishedNodeStates()
          .then((json)=>{
            req.result = {
              states : json,
            };

            next();
          });
      })

  });
}
