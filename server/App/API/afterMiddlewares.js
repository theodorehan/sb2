
export default function (expressInstance) {
  expressInstance.use('/api/*',function (req, res, next) {
    res.status(200).json({
      ...req.result,
      result : 'success',
    })
  });

  expressInstance.use('/api/*',function (err, req, res, next) {
    console.error(err);
    res.status(err.statusCode).json(err);
  });

  // expressInstance.use(function(err, req, res, next){
  //
  // });
}
