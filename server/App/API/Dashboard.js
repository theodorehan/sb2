import uuid from 'uuid';
import path from 'path';

import multiPartUpload from 'multer';
import base64Img from 'base64-img';
import Promise from "bluebird";


import {
    createSiteManager,
    ACCESSIBLE_LEVEL_STUDIO,
} from '../Helpers/Service/SiteManager';

import UserWorkspaceManager from '../Helpers/Service/UserWorkspaceManager';


import {
    Node,
} from '../Helpers/FileNode';

import AssignedStoreManager from '../assignedStore';

import {
    Director,
} from '../App';

import Site from '../Helpers/Service/Site';

export default function (expressInstance) {

    expressInstance.get('/api/user/:userSessionID/dashboard/site-list', function (req, res, next) {


        /**
         *
         *
         * accessKey: "jeju"
         info: {themeName: "jeju", SERVICE_FRONT_FRAMEWORK: "vue", SERVICE_STRUCTURE_TYPE: "vue_basic",…}
         HOT_LINE_KEY: "ygoon-desktop"
         SERVICE_FRONT_FRAMEWORK: "vue"
         SERVICE_STRUCTURE_TYPE: "vue_basic"
         created: 1541522046538
         device-size: "fit"
         lastPublished: null
         modified: null
         owner: "system"
         published: false
         secret: "5c5b998fe78574ba522d05c7cb4c0a0eca0def965ae9486b0e38ec416333db32"
         serviceVersion: "1.0"
         siteAccessName: "jeju"
         siteName: "jj"
         themeName: "jeju"
         themeOriginDirName: "jeju"
         name: "jeju"
         siteOwner: "system"
         thumbnails: ["thumb01.png", "thumb02.png", "thumb03.png"]
         version: "0.2.2"
         */
        console.log('eee', Director.get().EDITABLES);


        req.result = {
            list: Director.get().EDITABLES.map((editable) => ({
                accessKey: editable.id,
                name: editable.name,
                siteOwner: 'system',
                version: "0.2.2",
                info: {
                    HOT_LINE_KEY: editable.id,
                    SERVICE_FRONT_FRAMEWORK: "vue",
                    SERVICE_STRUCTURE_TYPE: "vue_basic",
                    created: 1541522046538,
                    "device-size": "fit",
                    lastPublished: null,
                    modified: null,
                    owner: "system",
                    published: false,
                    secret: "5c5b998fe78574ba522d05c7cb4c0a0eca0def965ae9486b0e38ec416333db32",
                    serviceVersion: "1.0",
                    siteAccessName: editable.id,
                    siteName: editable.name,
                    themeName: editable.id,
                    themeOriginDirName: editable.id,
                },

                thumbnails: ["thumb01.png"]
            }))
        }
        next();

        // let userModel = req.CertifiedDataSet.userModel;
        //
        // let workspaceManager = new UserWorkspaceManager(expressInstance, userModel);
        //
        // workspaceManager.getSiteList()
        //     .then((list) => {
        //         console.log(list);
        //         req.result = {list};
        //         next()
        //     });
    });

    expressInstance.get('/api/user/:userSessionID/dashboard/create-site', function (req, res, next) {
        let userModel = req.CertifiedDataSet.userModel;
        let {themeDirName, themeName, siteName} = req.query;
        let {userSessionID} = req.params;

        let workspaceManager = new UserWorkspaceManager(expressInstance, userModel);
        let assignedStoreManager = new AssignedStoreManager(expressInstance);


        assignedStoreManager.siteArchiving(themeDirName)
            .then((archivePath) => {


                workspaceManager.createWithBringArchiveFileByPath(themeDirName, themeName, archivePath, userModel, siteName)
                    .then((siteAccessName) => {
                        req.result = {
                            siteAccessName,
                        }
                        next();

                        console.log(`BUILD_URL : /api/user/${userSessionID}/site/${siteAccessName}/store/build`);
                        // res.redirect(`/api/user/${userSessionID}/site/${siteAccessName}/build`);
                    });
            });
    });
}
