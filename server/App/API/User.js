/**
 * Created by seonwoong on 2017. 5. 10..
 */
import uuid from 'uuid';
var Promise = require("bluebird");
import UserModel from '../Model/User';
import FormData from 'form-data';
import Errors from '../Helpers/Error';
import {Director} from "../App";

export default function (expressInstance) {
  // console.log('_res_res_res');
  // expressInstance.get('/api/user/test', function (_req, _res) {
  //   console.log('_res', _res);
  //   var getSqlConnection = expressInstance.get('dbconn');
  //   Promise.using(getSqlConnection(), function (connection) {
  //     return connection.query('select * from USER').then(function (row) {
  //       _res.json(row)
  //     }).catch(function (error) {
  //       console.log(error);
  //     });
  //   })
  // });


  expressInstance.post('/api/user/sign-in', async function(req, res, next){
    let {id, pw} = req.body;


    if( !id ){
      return next(Errors('SIGNIN.ID_FIELD_IS_REQUIRED'))
    }

    if( !pw ){
      return next(Errors('SIGNIN.PW_FIELD_IS_REQUIRED'))
    }

    if( !req.cookies.iceJWT ){
      return next(Errors('SESSION.NO_JWT'))
    }

    try {
      let formData = new FormData();
      formData.append('userId', id);
      formData.append('password', pw);



      let result = await req.ice2.request({
        url : '/session/login' ,
        method:'post',
        headers: {
          ...formData.getHeaders(),
          Cookie : `iceJWT=${req.cookies.iceJWT || ''}; iceRefreshJWT=${req.cookies.iceRefreshJWT || ''};`,
        },
        data : formData,
      })

      if( result.data.result == '200' ){
        req.result = {
          ...result.data,
        };


        return next();
      }



      next(Errors('SIGNIN.USER_NOT_FOUND'))

    } catch(e) {
      console.log(e);
      next(e);
    }
  });

  expressInstance.get('/api/user/signin', function (_req, _res, next) {



    console.log('eee', Director.get().EDITABLES);


    UserModel.findOneBySeq(0)
      .then((userModel)=>{

        let sessionID = uuid.v4();

        expressInstance.get('UserSession').write(sessionID, userModel.toJSON(), function (_err) {
          if (_err) {
            next(_err);
          } else {
            _req.result = {
              sessionID,
              ...userModel.toJSON(),
            };
            next();
          }

        });
      });

    // _req.result = {
    //   active: true,
    //   admin: true,
    //   avatarImg: "/public/images/uxceo-128.jpg",
    //   blocked: false,
    //   email: "jessicajones@ice2.com",
    //   id: "system",
    //   level: 1,
    //   result: "success",
    //   screenName: "Jessica Jones",
    //   seq: 0,
    //   sessionID: "5b7c3138-9d10-4cb5-8b46-d9ce934d4a67",
    //   signupDate: 1495084195444,
    // }

    // next();
  });

  expressInstance.get('/api/user/sessionCheck', function (_req, _res, next) {
    next();
  });

  expressInstance.get('/api/user/read', function (_req, _res, next) {
    let assignedUserData = Object.assign({}, userData, {
      result: 'success',
    });

    _req.result = assignedUserData;
    next();
  });
}
