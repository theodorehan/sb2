import uuid from 'uuid';
import path from 'path';

import multiPartUpload from 'multer';
import base64Img from 'base64-img';
import Promise from "bluebird";

import {
  createSiteManager,
  ACCESSIBLE_LEVEL_STUDIO,
} from '../Helpers/Service/SiteManager';

import UserWorkspaceManager from '../Helpers/Service/UserWorkspaceManager';


var upload = multiPartUpload({storage: storage});
var storage = multiPartUpload.diskStorage({
  destination: function (req, file, cb) {
    let assignedStoreID = req.params.assignedStoreID;
    cb(null, 'server/DataStore/Site/' + assignedStoreID + '/src/statics/images')
  },
  filename: function (req, file, cb) {
    cb(null, path.parse(file.originalname).name + '_' + Date.now() + path.parse(file.originalname).ext);
  },
});




export default function (expressInstance) {

  expressInstance.get('/api/user/:userSessionID/site/:siteID/:siteOwner/store/get-tree', function (req, res, next) {
    let userWorkspaceManger = new UserWorkspaceManager(expressInstance, req.CertifiedDataSet.userModel);
    userWorkspaceManger.getSiteManager(req.CertifiedDataSet, ACCESSIBLE_LEVEL_STUDIO)
      .then((siteManager)=>{
        req.result = {
          root : siteManager.siteNode,
        };
        next();
      })

  });

  expressInstance.get('/api/user/:userSessionID/site/:siteID/:siteOwner/store/builtin-component-categories', function (req, res, next) {
    let userWorkspaceManger = new UserWorkspaceManager(expressInstance, req.CertifiedDataSet.userModel);
    userWorkspaceManger.getSiteManager(req.CertifiedDataSet, ACCESSIBLE_LEVEL_STUDIO)
      .then((siteManager)=>{
        siteManager.getBuiltInComponentMajorCategories().then(
          (categories)=>{

            req.result = {
              categories,
            };

            next();
          }, (err) => {
            next(err);
          })
      })

  });

  expressInstance.get('/api/user/:userSessionID/site/:siteID/:siteOwner/store/builtin-component-categories/sub', function (req, res, next) {
    let {path} = req.query;

    let userWorkspaceManger = new UserWorkspaceManager(expressInstance, req.CertifiedDataSet.userModel);
    userWorkspaceManger.getSiteManager(req.CertifiedDataSet, ACCESSIBLE_LEVEL_STUDIO)
      .then((siteManager)=>{
        siteManager.getBuiltInComponentCategorySubWithChildren(path).then(
          (filledCategoryItem)=>{

            req.result = {filledCategoryItem};
            next();
          }, (err) => {
            next(err);
          })
      })
  });

  expressInstance.get('/api/user/:userSessionID/site/:siteID/:siteOwner/store/builtin-component-categories/component-preview', function (req, res, next) {
    let {path} = req.query;
    console.log('Component preview :', path);


    let userWorkspaceManger = new UserWorkspaceManager(expressInstance, req.CertifiedDataSet.userModel);
    userWorkspaceManger.getSiteManager(req.CertifiedDataSet, ACCESSIBLE_LEVEL_STUDIO)
      .then((siteManager)=>{
        siteManager.getBuiltInComponentPreview(path).then(
          (previewComponent)=>{

            req.result = {previewComponent};
            next();
          }, (err) => {
            next(err);
          })
      })
  });

  expressInstance.get('/api/user/:userSessionID/site/:siteID/:siteOwner/store/builtin-component-categories/component-preview-image', function (req, res, next) {
    let {path} = req.query;


    let userWorkspaceManger = new UserWorkspaceManager(expressInstance, req.CertifiedDataSet.userModel);
    userWorkspaceManger.getSiteManager(req.CertifiedDataSet, ACCESSIBLE_LEVEL_STUDIO)
      .then((siteManager)=>{
        let componentImagePath = siteManager.getBuiltInComponentPreviewImagePath(path);

        if( componentImagePath ){
          res.sendFile(componentImagePath);
        } else {
          res.redirect('/public/images/vue-logo.png');
        }
      })
  });

  expressInstance.get('/api/user/:userSessionID/site/:siteID/:siteOwner/store/fragment-component-categories', function (req, res, next) {

  });

  expressInstance.get('/api/user/:userSessionID/site/:siteID/:siteOwner/store/included-component-categories', function (req, res, next) {

  });

  expressInstance.get('/api/user/:userSessionID/site/:siteID/:siteOwner/store/store-component-categories', function (req, res, next) {

  });



  // expressInstance.get('/api/user/:userSessionID/site/:siteID/:siteOwner/store/file-read-utf8', function (_req, _res, next) {
  //   let {assignedStoreID, filepath} = _req.query;
  //
  //
  //   if (typeof filepath !== 'string') {
  //     return _res.status(406).json({
  //       result: 'fail',
  //       error: 'filepath must be a String.',
  //     });
  //   }
  //
  //   if (filepath.indexOf('/') !== 0) {
  //     return _res.status(406).json({
  //       result: 'fail',
  //       error: "filepath first letter must be a '/'.",
  //     });
  //   }
  //
  //   if (filepath.indexOf('..') > -1) {
  //     return _res.status(406).json({
  //       result: 'fail',
  //       error: "filepath couldn't include relative path.",
  //     });
  //   }
  //
  //   let defensivePath = path.resolve(filepath);
  //
  //   let info = expressInstance.get('AssignedStore').getFileDataAsUTF8(assignedStoreID, defensivePath, (err, data) => {
  //     if (err) {
  //       return _res.status(err.statusCode).json({
  //         err,
  //       });
  //     }
  //
  //     _res.status(200).json({
  //       result: 'success',
  //       data: data,
  //     });
  //   });
  // });

  // expressInstance.post('/api/assignedStore/fileUpload/:assignedStoreID', upload.single('file'), function (_req, _res, next) {
  //   if (_req.file != undefined) {
  //
  //     next();
  //   }
  // });
  //
  // expressInstance.post('/api/assignedStore/imageSave', function (_req, _res, next) {
  //   // base64Img.img(_req.body.base64Img, 'server/DataStore/Site/' + _req.body.assignedStoreID + '/src/statics/images', path.parse(_req.body.imageID).name + '_modified' + path.parse(_req.body.imageID).ext, function (err, filepath) {
  //   base64Img.img(_req.body.base64Img, 'server/DataStore/Site/' + _req.body.assignedStoreID + '/src/statics/images', path.parse(_req.body.imageID).name + '_modified', function (err, filepath) {
  //     console.log('err', err);
  //     if(!err){
  //       next();
  //     }
  //   });
  // });
}

