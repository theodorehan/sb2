import uuid from 'uuid';
var Promise = require("bluebird");
import JSONFile from 'jsonfile';
import path from 'path';
import Site from '../Helpers/Service/Site';

import {
  Director,
  SITE_STORE_DIR,
} from '../App';

import {
  Node,
} from '../Helpers/FileNode';

import Errors from '../Helpers/Error';

export default function (expressInstance) {


  expressInstance.get('/api/user/:userSessionID/template/site-template-list', function (req, res, next) {

      req.result = {
          list : Director.get().EDITABLES.map((editable) => ({
              accessKey: editable.id,
              name: editable.name,
              siteOwner: 'system',
              version: "0.2.2",
              info: {
                  HOT_LINE_KEY: editable.id,
                  SERVICE_FRONT_FRAMEWORK: "vue",
                  SERVICE_STRUCTURE_TYPE: "vue_basic",
                  created: 1541522046538,
                  "device-size": "fit",
                  lastPublished: null,
                  modified: null,
                  owner: "system",
                  published: false,
                  secret: "5c5b998fe78574ba522d05c7cb4c0a0eca0def965ae9486b0e38ec416333db32",
                  serviceVersion: "1.0",
                  siteAccessName: editable.id,
                  siteName: "jj",
                  themeName: editable.id,
                  themeOriginDirName: editable.id,
              },

              thumbnails: ["thumb01.png"]
          }))
      };
      next();


      return;
    let templateDirNode = Node.convertToNode(SITE_STORE_DIR);
    templateDirNode.loadChildrenAll()
      .then(()=>{

        Promise.all(templateDirNode.children.filter((node)=>node.isDir).map((node)=> new Promise((resolve, reject)=>{
          let site = new Site(node.systemPath);

          site.getGatheringSiteMeta()
            .then((meta) => {
              resolve(Object.assign(node, meta));
            })
        })))
          .then((list)=>{
            req.result = {
              list : list
                .map((node) => ({
                  name:node.name,
                  version : node.version,
                  thumbnails : node.thumbnails,
                  info : node.info,
                })),
            };

            next();
          })
      });
  });

  expressInstance.get('/api/user/:userSessionID/template/site-template-image', function (req, res, next) {
    let {sitename, i} = req.query;
    let sitePath = path.join(SITE_STORE_DIR, '/', sitename);

    let site = new Site(sitePath);

    res.sendFile(site.getThumbnailPath(i));
  });
}
