import uuid from 'uuid';
var Promise = require("bluebird");
import JSONFile from 'jsonfile';
import path from 'path';

import {
  SITE_INFO_DIR_PATH,
  Director,
} from '../App';


import {
  ACCESSIBLE_LEVEL_STUDIO,
} from '../Helpers/Service/SiteManager';

import Errors from '../Helpers/Error';
import UserWorkspaceManager from '../Helpers/Service/UserWorkspaceManager';
import Site from '../Helpers/Service/Site';


export default function (expressInstance) {


    // req.result = {
    //     list : Director.get().EDITABLES.map((editable) => ({
    //         accessKey: editable.id,
    //         name: editable.name,
    //         siteOwner: 'system',
    //         version: "0.2.2",
    //         info: {
    //             HOT_LINE_KEY: editable.id,
    //             SERVICE_FRONT_FRAMEWORK: "vue",
    //             SERVICE_STRUCTURE_TYPE: "vue_basic",
    //             created: 1541522046538,
    //             "device-size": "fit",
    //             lastPublished: null,
    //             modified: null,
    //             owner: "system",
    //             published: false,
    //             secret: "5c5b998fe78574ba522d05c7cb4c0a0eca0def965ae9486b0e38ec416333db32",
    //             serviceVersion: "1.0",
    //             siteAccessName: editable.id,
    //             siteName: "jj",
    //             themeName: editable.id,
    //             themeOriginDirName: editable.id,
    //         },
    //
    //         thumbnails: ["thumb01.png"]
    //     }))
    // };



  expressInstance.get('/api/user/:userSessionID/site/:siteID/:siteOwner/info', function (req, res, next) {
    req.result = req.CertifiedDataSet.siteModel;

    next();
  });

  expressInstance.get('/api/user/:userSessionID/site/:siteID/:siteOwner/configs', function (req, res, next) {


      let http =    {
          "defaultAPIHost" : Director.get().SERVICE_DEFAULT_API_HOST,
              "defaultAPIResourceHost" : Director.get().SERVICE_DEFAULT_API_RESOURCE_HOST,
      };

      req.result = {
          prod : {
              http: http,
          },
          dev : {
              http: http,
          },
          stage : {
              http: http,
          },
          main : {
              http: http,
          },
          force : {
              http: http,
          },
          level: 'force'
      };

      next();


      return;
    req.userWorkspace.getSiteManager(req.CertifiedDataSet, 'studio')
      .then((siteManager)=>{
        let sitePath = siteManager.getSiteDirPath();

        let site = new Site(sitePath);

        site.readConfigEachLevel()
          .then((data) => {
          console.log(data);
            req.result = data;
            req.result['force'] = {
              http : {
                "defaultAPIHost" : Director.get().SERVICE_DEFAULT_API_HOST,
                "defaultAPIResourceHost" : Director.get().SERVICE_DEFAULT_API_RESOURCE_HOST,
              },
            };
            req.result.level = Director.get().DEFAULT_SITE_CONFIG_LEVEL;
            next();
          });
      });
  });

  expressInstance.get('/api/user/:userSessionID/site/:siteID/:siteOwner/store/build', function (req, res, next) {
    req.result = req.CertifiedDataSet.siteModel;

    let userWorkspaceManger = new UserWorkspaceManager(expressInstance, req.CertifiedDataSet.userModel);
    userWorkspaceManger.getSiteManager(req.CertifiedDataSet, 'studio')
      .then((siteManager)=>{

        siteManager.siteNPMInstall()
          .then(()=>{
            console.log("NPM Install Done");

            siteManager.siteBuild()
              .then(()=>{
                console.log("Site build done");

                next();
              }, (err) => {
                next(err);
              })
          }, (err)=>{
            next(err);
          })
      })
  });



  expressInstance.get('/api/user/:userSessionID/site/:siteID/:siteOwner/store/root-route', function(req, res, next){

    let userWorkspaceManger = new UserWorkspaceManager(expressInstance, req.CertifiedDataSet.userModel);
    userWorkspaceManger.getSiteManager(req.CertifiedDataSet, 'studio')
      .then((siteManager)=>{
        siteManager.getRootRoute()
          .then((routeNode)=>{
            req.result = {
              route : routeNode.exportToJSON(),
            }
            next();
          })
      })

    //
    // SITE_FS_ACCESS_GET_ROUTES_TREE(req.CertifiedDataSet.siteModel.id)
    //   .then((routes)=>{
    //
    //     req.result = {routes};
    //     next();
    //
    //     console.log('SITE_FS_ACCESS_GET_ROUTES_TREE RESULT ', routes)
    //   }, (_err)=>{
    //     console.log("SITE_FS_ACCESS_GET_ROUTES_TREE ERROR ", _err);
    //   })

  })

  expressInstance.get('/api/user/:userSessionID/site/:siteID/:siteOwner/store/resource-categories', function(req, res, next){
    let {target} = req.query;

    let userWorkspaceManger = new UserWorkspaceManager(expressInstance, req.CertifiedDataSet.userModel);
    userWorkspaceManger.getSiteManager(req.CertifiedDataSet, ACCESSIBLE_LEVEL_STUDIO)
      .then((siteManager)=>{
        siteManager.getResourceMajorCategories(target).then(
          (categories)=>{

            req.result = {
              categories,
            };

            next();
          }, (err) => {
            next(err);
          })
      })

  })


  expressInstance.get('/api/user/:userSessionID/site/:siteID/:siteOwner/store/resource-categories/sub', function(req, res, next){
    let {target, path} = req.query;


    let userWorkspaceManger = new UserWorkspaceManager(expressInstance, req.CertifiedDataSet.userModel);
    userWorkspaceManger.getSiteManager(req.CertifiedDataSet, ACCESSIBLE_LEVEL_STUDIO)
      .then((siteManager)=>{
        siteManager.getResourceCategorySubWithChildren(target,path).then(
          (filledCategoryItem)=>{

            req.result = {filledCategoryItem};
            next();
          }, (err) => {
            next(err);
          })
      })

  })


  expressInstance.use(/\/api\/user\/([\w\d\-]+)\/site\/([\w\d\-.]+)\/([\w\d\-.]+)\/store\/resource\/get\/(.*)$/, function(req, res, next){
    let resourcePath = req.params['3'];
      let safeResourcePath = path.resolve('/'+resourcePath);



    let userWorkspaceManger = new UserWorkspaceManager(expressInstance, req.CertifiedDataSet.userModel);
    userWorkspaceManger.getSiteManager(req.CertifiedDataSet, ACCESSIBLE_LEVEL_STUDIO)
      .then((siteManager)=>{
        let sitePath = siteManager.getSiteDirPath();
        let resourceSystemPath = path.join(sitePath, '/src/static/', safeResourcePath);

        res.header("Access-Control-Allow-Origin", "*");
        // res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        res.sendFile(resourceSystemPath);
      })
  })

  expressInstance.get('/api/user/:userSessionID/site/:siteID/:siteOwner/store/site-thumbnail', function(req, res, next){
    let {name} = req.query;


    let userWorkspaceManger = new UserWorkspaceManager(expressInstance, req.CertifiedDataSet.userModel);
    userWorkspaceManger.getSiteManager(req.CertifiedDataSet, ACCESSIBLE_LEVEL_STUDIO)
      .then((siteManager)=>{
        let sitePath = siteManager.getSiteDirPath();

        let site = new Site(sitePath);
        res.sendFile(site.getThumbnailPath(name));
      })

  })

  expressInstance.get('/api/user/:userSessionID/site/:siteID/:siteOwner/store/update-theme', function(req, res, next){
    let {siteID, siteOwner} = req.params;

    if( Director.get().WORKER_KEEP_ALIVE ){
      let workerId = `${siteOwner}.${siteID}`;

      let workerClient = expressInstance.get('WorkerSocketsManager').findClient(workerId);
      console.log(workerId);
      if( workerClient ){
        workerClient.shutdown();
      }
    }


    let userWorkspaceManger = new UserWorkspaceManager(expressInstance, req.CertifiedDataSet.userModel);
    userWorkspaceManger.getSiteManager(req.CertifiedDataSet, ACCESSIBLE_LEVEL_STUDIO)
      .then((siteManager)=>{
        let sitePath = siteManager.getSiteDirPath();

        let site = new Site(sitePath);
        site.getInfo()
          .then((info)=>{
            if( req.CertifiedDataSet.userModel.id === info.owner ){

              site.updateTheme()
                .then((result)=>{
                  res.result = {result};
                  next();
                });
            } else {
              next(Errors('SITE.THEME.UPDATE.NO_PERMISSION'))
            }
          })
      });
  })


  expressInstance.post('/api/user/:userSessionID/site/:siteID/:siteOwner/store/resource/upload-base64-image', function (req, res, next) {
    let { target, data, resourcePath, overwrite } = req.body;

    let safeResourcePath = path.resolve(`/${resourcePath}`); // 상대경로인자를 제거하기 위해 /로 설정하고 resolve한다.
    safeResourcePath = safeResourcePath.replace(/^\//, ''); // 상대경로 제거를 위해 부여한 /를 제거하여 리소스패스를 구한다.

    let userWorkspaceManger = new UserWorkspaceManager(expressInstance, req.CertifiedDataSet.userModel);
    userWorkspaceManger.getSiteManager(req.CertifiedDataSet, ACCESSIBLE_LEVEL_STUDIO)
      .then((siteManager)=>{
        if( target === 'image' ){
          siteManager.writeResourceBase64Image(safeResourcePath, data, overwrite)
            .then((err)=>{
              if( err ){
                next(err);
              } else {
                next();
              }
            });

          return;
        }

        next(new Error(''));
      })
  });
}
