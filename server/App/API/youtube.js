 import ax from 'axios';
import uuid from 'uuid';
var Promise = require("bluebird");
import {
  Director,
} from '../index';

export default function (expressInstance) {

  expressInstance.get('/api/youtube/search', function (req, res, next) {
    let {value, maxResults, order, pageToken } = req.query;
    let apiKey = Director.get().ServerConfig.service_youtube_default_api_key;

    ax.get(`https://www.googleapis.com/youtube/v3/search`, {
      params : {
        part: 'snippet',
        order: 'relevance',
        // videoEmbeddable : true,
        maxResults,
        order,
        pageToken,
        q: value,
        key: apiKey,
      },
    })
      .then((res) => {
        let {data} = res;
        req.result = data;
        console.log('response data', data);
        next();
      },(err)=>{
        console.error('Error', err.response.data.error.message);
        console.log('Error End======')
        next({message : err.response.data.error.message});
      })
  });

}
