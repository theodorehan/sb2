import User from './User';
import Site from './Site';
import AssignedStore from './AssignedStore';
import Studio from './Studio';
import Editing from './editing';
import beforeMiddlewares from './beforeMiddlewares';
import afterMiddlewares from './afterMiddlewares';
import SiteTemplate from './SiteTemplate';
import Dashboard from './Dashboard';
import Helper from './Helper';
import Publish from './publish';
import Ice2Admin from './Ice2Admin';
import youtube from './youtube';
import Monitor from './Monitor';


export default function (expressInstance) {

  beforeMiddlewares(expressInstance);

  User(expressInstance);
  Site(expressInstance);
  AssignedStore(expressInstance);
  Studio(expressInstance);
  Editing(expressInstance);
  SiteTemplate(expressInstance);
  Dashboard(expressInstance);
  Helper(expressInstance);
  Publish(expressInstance);
  Ice2Admin(expressInstance);
  youtube(expressInstance);
  Monitor(expressInstance);

  afterMiddlewares(expressInstance);
}
