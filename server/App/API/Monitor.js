export default function (app) {
  app.get('/api/monitor/sites', function(req, res, next){

    let a = [];
    app.siteInstances.forEach((site,id) => {
      a.push({
        ip : site.ip,
        port : site.port,
        port_ssl : site.port_ssl,
        siteId : site.siteId,
        owner : site.owner,
        isDead : site.isDead,
      });
    });


    res.json(a);
  })
}
