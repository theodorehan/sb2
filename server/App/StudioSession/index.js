import JSONFile from 'jsonfile';
import path from 'path';

import Errors from '../Helpers/Error';
import Promise from 'bluebird';
import uuid from 'uuid';

import {
  STUDIO_SESSION_DIR_PATH,
  Logger,
} from '../App';

export default class StudioSessionManager {
  ex;

  constructor(expressInstance) {
    this.ex = expressInstance;
  }

  read(_sessionID, _cb) {
    let filepath = path.join(STUDIO_SESSION_DIR_PATH, `${_sessionID}.json`);

    return new Promise((resolve, reject)=>{
      JSONFile.readFile(filepath, (_err, _data) => {



        if (_err) {
          reject(Errors('USER.SESSION.NOT_FOUND'));
          _cb && _cb(Errors('USER.SESSION.NOT_FOUND'));

        } else {
          resolve(_data);
          _cb && _cb(null, _data);
        }
      })
    })
  }


  write(_sessionID, sessionData, _cb) {
    let that = this;

    let filepath = path.join(STUDIO_SESSION_DIR_PATH, `${_sessionID}.json`);
    JSONFile.writeFile(filepath, sessionData, {}, function (_err) {
      if (_err) {
        _cb(Errors('SIGNIN.FAILED_CREATE_SESSION'));
      } else {


        Logger.get().info('Write Studio Session Studio[%s] with User[%s].', _sessionID, sessionData.userSession);
        _cb(null);
      }
    })
  }




  createSession(_userSessionID, _siteID, _cb) {
    let that = this;

    console.log('_userSessionID',_userSessionID, _siteID);
    return new Promise(function (_resolve, _reject){
      // 유저 세션 읽기
      that.ex.get("UserSession").read(_userSessionID, (_err, data)=>{

        // console.log('user session data', data)
        if( _err ){
          _reject(_err);
        } else {
          _resolve(data);
        }
      });
    }).then(
      function resolved(_userSessionData){
        let studioSessionID = uuid.v4();

        // CREATE STUDIO SESSION
        let studioSessionData = {
          userSession : _userSessionID,
          siteID : _siteID,
          workerInstanceSessionID : null,
        };

        return new Promise((_resolve, _reject)=>{

          // StudioSession 쓰기
          that.write(studioSessionID, studioSessionData, (_err)=>{

            if( _err ){
              _reject(_err);
            } else {

              _resolve([studioSessionID, _userSessionData]);
            }
          });
        });
      }
    ).then(
      function resolved([_studioSessionID, _userSessionData]){


        // User Session에 studioSessionID 를 추가 한다.
        return new Promise(function(_resolve, _reject){
          that.ex.get("UserSession").write(_userSessionID, {
            ..._userSessionData,
            studioSessionID : _studioSessionID,
          }, (_err, data)=>{

            if( _err ){
              _reject(_err)
            } else {

              Logger.get().info('Created Studio Session Studio[%s] with User[%s].', _studioSessionID, _userSessionID);
              _resolve(_studioSessionID);
            }
          });
        });
      }
    )
  }
}
