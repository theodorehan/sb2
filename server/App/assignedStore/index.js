import path from 'path';
import fs from 'fs';
import ls from 'ls';
import lsAll from 'ls-all';
import Errors from '../Helpers/Error';
import tar from 'tar';
import uuid from 'uuid';


import {
  ASSIGNED_STORE_DIR_PATH,
  SITE_ARCHIVE_BUFFER_PATH,
  Logger,
} from '../App'

import {
  Node,
} from '../Helpers/FileNode';


export default class AssignedStoreManager {
  constructor(express) {
    this.ex = express;
  }

  getSiteStorePath(_storeID){
    return path.join(ASSIGNED_STORE_DIR_PATH, '/Site/', _storeID, '/');
  }

  getSiteTree(_storeID, excludeOr, include, _cb) {

    let rootNode = this.getSiteNode(_storeID);

    rootNode.loadChildrenRecursiveAll({
      exclude: excludeOr,
      include : include,
    }).then(()=>{
      _cb(rootNode)
    });
  }

  getSiteNode(_storeID){
    let storePath = this.getSiteStorePath(_storeID);

    return Node.convertToNode(storePath);
  }

  ls(_filepath) {
    return ls(_filepath);
  }


  // getSiteFileInfo(_storeID, _filepath, _cb) {
  //   let filePath = path.join(ASSIGNED_STORE_DIR_PATH, '/site/', _storeID, _filepath);
  //
  //
  //   fs.exists(filePath, (exists) => {
  //     if (exists) {
  //       fs.open(filePath, 'r', (err, fd) => {
  //         if (err) {
  //           _cb(Errors('AS.CAN_NOT_OPEN_FILE', err));
  //         } else {
  //           // fs.fstat(fd, (err, stats)=>{
  //           //   if( err ){
  //           //     _cb(Errors('AS.CAN_NOT_READ_STATS', err));
  //           //   } else {
  //           //
  //           //     console.log(stats)
  //           //     _cb(null, stats);
  //           //   }
  //           // });
  //
  //
  //         }
  //       });
  //     } else {
  //       _cb(Errors('AS.FILE_DOES_NOT_EXIST'));
  //     }
  //   });
  // }


  getFileDataAsUTF8(_storeID, _filepath, _cb) {
    let filePath = path.join(ASSIGNED_STORE_DIR_PATH, '/site/', _storeID, _filepath);


    fs.exists(filePath, (exists) => {
      if (exists) {

        fs.readFile(filePath, 'utf8', (err, data) => {
          if (err) {
            _cb(Errors('AS.CAN_NOT_READ_FILE', err));
          } else {

            _cb(null, data);
          }
        });

      } else {
        _cb(Errors('AS.FILE_DOES_NOT_EXIST'));
      }
    });
  }


  siteArchiving(_storeID) {
    let archiveName = `${uuid.v4()}.${_storeID}.tar.gz`;
    let tarballPath = path.join(SITE_ARCHIVE_BUFFER_PATH, archiveName);
    let assignStorePath = this.getSiteStorePath(_storeID);


    let storePath = this.getSiteStorePath(_storeID);
    let node = Node.convertToNode(storePath);

    return new Promise((resolve, reject)=>{

      node.loadChildrenRecursiveAll({
        exclude: ['node_modules','.idea', 'dist'],
      }).then(()=>{
        let targetFileAndDirList = node.children.map( (node)=>node.systemPath.replace(assignStorePath,''));

        tar.c(
          {
            gzip: true,
            file: tarballPath,
            cwd : assignStorePath,
          },
          targetFileAndDirList,
        ).then( (_) => {
          resolve(tarballPath);
        }, (err) =>{

          Logger.get().error(`사이트 압축 실패 ${_storeID}` ,err);
          reject(Errors('AS.FAIL_SITE_ARCHIVING'));
        });
      });
    });
  }
}
