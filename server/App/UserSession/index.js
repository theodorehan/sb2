import JSONFile from 'jsonfile';
import path from 'path';

import Errors from '../Helpers/Error';

import {
  USER_SESSION_DIR_PATH,
  Logger,
} from '../App';

export default class UserSessionManager {
  ex;

  constructor(expressInstance) {
    this.ex = expressInstance;
  }

  read(_sessionID, _cb) {
    let filepath = path.join(USER_SESSION_DIR_PATH,`${_sessionID}.json`);

    return new Promise((resolve, reject)=> {
      JSONFile.readFile(filepath, (_err, _data) => {
        if (_err) {
          reject(Errors('USER.SESSION.NOT_FOUND'));
          _cb && _cb(Errors('USER.SESSION.NOT_FOUND'));
        } else {
          _data.sessionID = _sessionID;

          resolve(_data);
          _cb && _cb(null, _data);
        }
      })
    });
  }


  write(_sessionID, sessionData, _cb){
    let that = this;
    let filepath = path.join(USER_SESSION_DIR_PATH,`${_sessionID}.json`);
    JSONFile.writeFile(filepath, sessionData, {}, function(_err){
      if( _err ){
        _cb(Errors('SIGNIN.FAILED_CREATE_SESSION'));
      } else {
        Logger.get().info('Write User Session ID[%s] of User[%s].', sessionData.id, _sessionID);

        _cb(null);
      }
    })
  }
}
