import proxy from 'express-http-proxy';


export default function (expressInstance){
  expressInstance.get('/render/:studioID/*', function(_req, _res){
    let studioID = _req.params.studioID;
    let tailPath = _req.params[0];


    _res.json(_req.params);
  });


  expressInstance.use('/proxy', proxy('localhost:8080'));
}
