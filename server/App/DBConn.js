import Mysql from 'promise-mysql';

export default function (expressInstance) {
  let config = expressInstance.get('config');

  let pool = Mysql.createPool({
    host: config.dbhost,
    user: config.dbuser,
    password: config.dbpass,
    database: config.dbname,
    connectionLimit: 100,
  });

 return function getSqlConnection() {
   return pool.getConnection().disposer(function (connection) {
     pool.releaseConnection(connection);
   });
 }
};
