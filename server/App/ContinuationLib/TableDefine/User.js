export const TableName = 'USER';

export const DDL = (dbConfig) => `CREATE TABLE ${TableName}
(
    seq BIGINT PRIMARY KEY AUTO_INCREMENT,
    id VARCHAR(255),
    email VARCHAR(255),
    password VARCHAR(255),
    screenName VARCHAR(255),
    signupDate DATETIME,
    active BOOL DEFAULT false,
    blocked BOOL,
    level INT
);
CREATE UNIQUE INDEX ${TableName}_seq_uindex ON ${dbConfig.dbname}.${TableName} (seq);
CREATE UNIQUE INDEX ${TableName}_id_uindex ON ${dbConfig.dbname}.${TableName} (id);
`;
