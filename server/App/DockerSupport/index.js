import {Writable} from 'stream';


import Docker from 'dockerode';
import ip from 'ip';
import {
  Logger,
  Director,
} from '../App';
export default class DockerManager {
  ex;
  docker;

  constructor(ex, config){
    this.ex = ex;

    if( config.docker_connect_type === 'unix_domain_socket' ){
      this.docker = new Docker({
        socketPath : config.docker_socket_path,
        Promise: require('bluebird'),
      });
    } else if ( config.docker_connect_type === 'http' ){
      this.docker = new Docker({
        host: config.docker_connect_ip,
        port: config.docker_connect_port,
      });
    }

    // this.docker.run('worker', [], process.stdout, function(err, data, container){
    //   console.log('1',err, data, container);
    // }).on('container', function(){
    //   console.log('2', arguments)
    // });

    // this.clearAll();
  }


  deleteContainer(containerID){
    return new Promise((resolve, reject)=>{

      let container = this.docker.getContainer(containerID);

      if( container ){
        container.kill((err, data) => {
          if( err ){
            Logger.get().workerContainer.error(`Fail killing container [${containerID}]`,err);
          }

          container.remove((err, data)=>{
            if( err ){
              Logger.get().workerContainer.error(`Fail removing container [${containerID}]`,err);

              reject(err);
            } else {
              resolve();
            }
          })
        })
      } else {
        // reject(new Error(`Container[${containerID}] not found.`));


        // 컨테이너가 존재 하지 않아 삭제 하지 못했지만 없어서 삭제하지 못한건 에러로 간주하지 않겠다.
        resolve();

        // 하지만 에러로 기록은 한다.
        Logger.get().workerContainer.error(`Not found container [${containerID}]`);
      }
    });
  }

  createWorkerContainer(workerSessionData, tryCount = 0){
    let config = this.ex.get('config');
    let {containerID} = workerSessionData;

    let options = {
      Env : [
        `SB_CONNECT_HOSTNAME=${ip.address()}`,
        `SB_INSTANCE_WORKER_ID=${workerSessionData.sessionID}`,
        `SB_BUILDER_DOMAIN=${process.env.NODE_ENV === 'development' ? config.builder_domain_development:config.builder_domain_production}`,
        `INTERNAL_SERVICE_PORT=3090`,
        `DISABLE_COMPONENT_CACHE=true`,
        `DISABLE_API_CACHE=true`,
        `SERVICE_NODE_ENV=production`,
        `RUN_IN_BUILDER=true`,
        `CONFIG_LEVEL=${Director.get().DEFAULT_SITE_CONFIG_LEVEL}`,
        `FORCE_API_RESOURCE_HOST=${Director.get().SERVICE_DEFAULT_API_RESOURCE_HOST}`,
        `FORCE_HTTP_API_HOST=${Director.get().SERVICE_DEFAULT_API_HOST}`,
        `DISABLED_SERVICE_BUILDER_SERVICE_CLIENT=1`,
      ],
      name : workerSessionData.containerID,
    };


    Logger.get().workerContainer.info(`Created container ${containerID}`,workerSessionData);


    if( workerSessionData.startingConfig.docker_container_pool_type === 'on_port' ){
      options.PortBindings ={ "3090/tcp": [{ "HostPort": String(workerSessionData.accessPort) }] };
    }


    // container /etc/hosts 추가
    // docker_container_extra_hosts="hostname:IP,hostname:IP,..." -> ExtraHosts = ["hostname:IP"]
    if( typeof config.docker_container_extra_hosts === 'string' ){
      options.ExtraHosts = config.docker_container_extra_hosts.split(',').map((str) => str.trim()).filter((s) => !!s);
    }


    // 컨테이너를 직접 지정하기 위해
    if( process.env.PREPARED_WORKER_ID_FOR_TEST ){
      return new Promise((resolve, reject)=>{

        let env = '--env ' + options.Env.join(' --env ');

        Logger.get().workerContainer.info(`TEST :: Wait Test Container[${process.env.PREPARED_WORKER_ID_FOR_TEST }] wait`,workerSessionData);

        this.ex.get('ttyLogger').info(`You run worker container by manual`);
        this.ex.get('ttyLogger').info(`docker run ${env} --env SB_INSTANCE_WORKER_ID=${process.env.PREPARED_WORKER_ID_FOR_TEST} -p ${workerSessionData.accessPort}:3000 worker`)
        this.ex.get('ttyLogger').info(`or`)
        this.ex.get('ttyLogger').info(`docker run -it ${env} --env SB_INSTANCE_WORKER_ID=${process.env.PREPARED_WORKER_ID_FOR_TEST} -p ${workerSessionData.accessPort}:3000 worker bash`)
        resolve();
      });
    }

    let outputStream = new Writable({
      write(chunk, encoding, callback) {
        let message = chunk.toString();

        Logger.get().workerContainer.info(`[${containerID}] >> ${message}`);
        callback();
      },
      writev(chunks, callback) {
        console.log(chunks.toString());
        callback();
      },
    });

    // let errStream = new Writable({
    //   write(chunk, encoding, callback) {
    //     let message = chunk.toString();
    //
    //     Logger.get().workerContainer.error(`[${containerID}] >> ${message}`);
    //     callback();
    //   },
    //   writev(chunks, callback) {
    //     // ...
    //   },
    // });

    return new Promise((resolve, reject)=>{
      this.docker.run('worker', [], outputStream, options, async (err, data, container) => {
        // 컨테이너 종료

        if( err ){
          Logger.get().workerContainer.error(`Container[${containerID}] finished with error >> ${err}`, data);

          let containerID = workerSessionData.containerID;

          try{
            Logger.get().workerContainer.error(`Will be deleted container [${containerID}]`, data);
            await this.deleteContainer(containerID);
          } catch (e) {

          }

          if( tryCount < 5 ){

            Logger.get().workerContainer.error(`Retry create container [${containerID}] tryCount:${tryCount}`, data);

            try {
              await this.createWorkerContainer(workerSessionData, tryCount + 1);

              return resolve();
            } catch(e){
              return reject(e);
            }

          }


          reject(err);

        } else {
          Logger.get().workerContainer.info(`Container[${containerID}] finished >>`,data);
        }
      }).on('container', function(){
        // 컨테이너 시작
        Logger.get().workerContainer.info(`Container[${containerID}] running`,workerSessionData);
        resolve();
      });
    });
  }

  clearWorkers(){

  }
}
