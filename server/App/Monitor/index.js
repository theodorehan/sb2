
export default function (ex) {
  ex.get('/admin/monitor', function (req,res,next) {

    res.json({
      STUDIO_CLIENTS : ex.get('StudioSocketsManager').monitorJSON(),
      WORKER_CLIENTS : ex.get('WorkerSocketsManager').monitorJSON(),
    });
  });
}
