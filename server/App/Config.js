import LoadINIFile from 'read-ini-file';
import path from 'path';
import mapValues from 'lodash/mapValues';
import ip from 'ip';

export default function(_filepath){

  try{
    let configJSON = LoadINIFile.sync(_filepath);


    let checks = [
      'external_url',
    ];


    let keys = Object.keys(configJSON);


    for(let i = 0; i < checks.length; i++ ){
      if( !keys.find((key) => key == checks[i] ) ){
        throw new Error(`Config Error : Need a ${checks[i]} field.`);
      }
    }

    return mapValues(configJSON, function(value, key){

      if( typeof value == 'string' ){
        return value.replace(/\[host\]/g, ip.address());
      }

      return value;
    });
  } catch(e){
    return null;
  }
}
