import uuid from 'uuid';
import {
  Logger,
} from '../App';



export default class ActionTracer {
  userSessionID;
  studioSessionID;
  workerSessionID;

  errorStack;


  constructor(ip, method, url, xhr, time, hostName){

    this.ip = ip;
    this.method = method;
    this.url = url;
    this.xhr = xhr;
    this.reqtime = time;
    this.hostName = hostName;


    this.actionID = uuid.v4();

    this.errorStack = [];

    Logger.get().access(`${ip}:${this.reqtime} - [${method}]${hostName}${url} - ${xhr ? '[XHR]' : ''} #${this.actionID}`);
  }

  setUserSessionID(id){
    this.userSessionID = id;
  }

  setStudioSessionID(id){
    this.studioSessionID = id;
  }

  setWorkerSessionID(id){
    this.workerSessionID = id;
  }

  get identifyString() {
    let str = '[';
    if( this.userSessionID ){
      str += 'u:'+this.userSessionID + ',';
    }

    if( this.studioSessionID ){
      str += 's:'+this.studioSessionID + ',';
    }

    if( this.workerSessionID ){
      str += 'w:'+this.workerSessionID + ',';
    }
    return str + ']';
  }


  errorEmit(error, _subject, _data){
    if( !( error instanceof Error ) ){
      throw new Error("error 객체가 아닙니다.");
    }

    if( typeof _subject !== 'string'){
      throw new Error('_subject is not string. :errorEmit(error, _subject, _data)');
    }

    let loggingParams = [`#${this.actionID} - ${_subject} : ${error}`,_data];

    Logger.get().error.apply(null, loggingParams);
  }

  finish(){

  }
}
