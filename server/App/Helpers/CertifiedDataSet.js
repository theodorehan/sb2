export default class CertifiedDataSet{

  userModel;
  siteModel;
  storeDefine;
  storeStructure;
  constructor(){

  }

  get userModel(){
    return this._userModel;
  }

  set userModel(_userModel){
    this._userModel = _userModel;
  }

  get siteModel(){
    return this._siteModel;
  }

  set siteModel(siteModel){
    this._siteModel = siteModel;
  }

  get storeDefine(){
    return this._storeDefine;
  }

  set storeDefine(storeDefine){
    this._storeDefine = storeDefine;
  }

  get storeStructure(){
    return this._storeStructure;
  }

  set storeStructure(storeStructure){
    this._storeStructure = storeStructure;
  }
}
