import fs from 'fs';
import path from 'path';
import Promise from 'bluebird';
import lsAll from 'ls-all';


export const NODE_PROJECT_EXCLUDES = ['node_modules','.idea', 'dist'];


export class Node {

  systemPath;

  isDir;
  name;
  size;
  extension;
  children;
  modify;

  /**
   *
   * @param _filepath
   * @returns {Node}
   */
  static convertToNode(_filepath) {
    if( typeof _filepath !== 'string') throw new Error("path is not String.")

    let leastStepStartIndex = _filepath.lastIndexOf('/') + 1;

    let file = _filepath.slice(leastStepStartIndex);

    if( !fs.existsSync(_filepath) ){
      return null;
    }

    let stat = fs.statSync(_filepath);

    let node = new Node(_filepath, stat.isDirectory(), file, stat);

    let extensionStartDotIndex = file.lastIndexOf('.');

    if (extensionStartDotIndex > -1) {
      node.extension = file.slice(extensionStartDotIndex);
    }

    return node;
  }

  constructor(_systemPath, isDir, name, stat, extension) {
    this.systemPath = _systemPath;

    this.isDir = isDir;
    this.name = name;
    this.size = stat.size;
    this.modify = stat.mtime;

    if (extension)
      this.extension = extension;

    if (isDir) {
      this.children = [];
    }
  }

  loadChildrenAll(_options) {
    if (this.isDir) {
      let {exclude, include} = _options || {};

      let fullPath = this.systemPath;

      let searchString = path.join(fullPath);

      return new Promise((_resolve, _reject) => {
        lsAll([searchString]).then((_list) => {
          let files = _list[0].files;

          this.children = files.map((_fileinfo) => Node.convertToNode(_fileinfo.path));


          this.children = this.children.filter((node) => {
            if (exclude) {
              if (exclude instanceof RegExp) {
                return !exclude.test(node.name);
              } else if (typeof exclude === 'string') {
                return exclude !== node.name;
              } else if ( Array.isArray(exclude) ){
                let matched = false;

                for(let i = 0; i < exclude.length; i++ ){

                  if( typeof exclude[i] === 'string'){
                    if( /^path:/.test(exclude[i]) ){
                      let matchPath = exclude[i].replace(/^path:/,'');
                      if( node.systemPath.indexOf(matchPath) === 0 ){
                        matched = true;
                      }
                    } else if (node.name === exclude[i]){
                      matched = true;
                    }
                  } else if ( exclude[i] instanceof RegExp && exclude[i].test(node.name) ){

                    matched = true;
                  }
                }

                return !matched;
              }
            } else if (include) {
              if (include instanceof RegExp) {
                return include.test(node.name);
              } else if (typeof include === 'string') {
                return include === node.name;
              }else if ( Array.isArray(include) ){
                let matched = false;

                for(let i = 0; i < include.length; i++ ){
                  if( typeof include[i] === 'string' && node.name === include[i]){
                    matched = true;
                  } else if ( include[i] instanceof RegExp && include[i].test(node.name) ){
                    matched = true;
                  }
                }

                return matched;
              }
            } else {
              return node;
            }
          });


          _resolve();
        });
      });
    } else {
      throw new Error(`I'am not Directory.\n${this.systemPath} is not Directory.`);
    }
  }


  loadChildrenRecursiveAll(_options) {
    return this.loadChildrenAll(_options).then(() => new Promise((resolve)=> {

      Promise.all(this.children.map((_node) => {
        if (_node.isDir) {
          return _node.loadChildrenRecursiveAll(_options);
        }

        return new Promise((resolve) => resolve());
      })).then(() => {
        resolve();
      });

    }));
  }


  // _func 에서 true 가 반환되면 방문 중지
  recursiveVisit(_func){
    if( _func(this) === true ){
      return;
    }

    if( this.isDir ){
      for(let i = 0; i < this.children.length; i++ ){
        this.children[i].recursiveVisit(_func);
      }
    }
  }

  childrenPick(_func){
    for(let i = 0; i < this.children.length; i++ ){

      if( _func(this.children[i]) === true ) {
        return this.children[i];
      }
    }
  }

  //

  /**
   * stridePath
   * @description 자신의 하위 노드에 대한 패스를 입력했을 때 패스를 따라 최하위의 노드를 찾아 반환한다.
   * @param _path : String
   *  자신의 노드명은 패스에 포함하지 않아야 한다.
   * @returns {*}
   */
  stridePath(_path){
    let pathToken = _path.split('/');
    let routeDirectiveNode = this;
    for( let i = 0; i < pathToken.length; i++ ){
      routeDirectiveNode = routeDirectiveNode.findChild(pathToken[i]);

      if( !routeDirectiveNode ){
        return null;
      }
    }

    return routeDirectiveNode;
  }

  findChild(_name){
    let node = null;

    this.childrenPick((_node)=>{
      if( _node.name === _name ){
        node = _node;
        return true;
      }
    });

    return node;
  }

  find(_name){
    let result = [];

    this.recursiveVisit((_n)=>{
      if( _n.name === _name ){
        result.push(_n);
      }
    });

    return result;
  }

  findOne(_name){
    let result = null;

    this.recursiveVisit((_n)=>{
      if( _n.name === _name ){
        result = _n;

        return true;
      }
    });

    return result;
  }

  createChildDirNodeSync(name){
    const dirpath = path.join(this.systemPath, '/' , name);
    fs.mkdirSync(dirpath);

    const newChild = Node.convertToNode(dirpath);
    this.children.push(newChild);
    return newChild;
  }

  readContents(){
    return new Promise((resolve, reject)=>{

    })
  }

  nodeExport() {
    let o = {
      isDir: this.isDir,
      name: this.name,
      size: this.size,
      extension: this.extension,
      modify : this.modify,
    };

    if (this.isDir) {
      o.children = this.children.map((_node) => _node.nodeExport());
    }

    return o;
  }
}
