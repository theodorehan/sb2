import URLSearchParams from 'url-search-params';
import FormData from 'form-data';
import request from 'request';


const defaultDirective = {
  "tag": "core-system-grid",
  "attrs": {},
  "props": {},
  "children": [
    {
      "tag": "core-system-row",
      "children": [
        {
          "tag": "core-system-column",
          "props": {
            "xs": 12,
            "sm": 12,
            "md": 12,
            "lg": 12,
          },
          "children": [],
        },
      ],
    },
  ],
};

export {directiveNameIceSchema} from "../../../app/utils/iceFragment";


export async function save(schema, json, ice2, description, publishNow, proxyCookie) {
  let data = {};
  let version = schema.version;

  if( schema.meta1 || schema.meta2 || schema.meta3 ){

    if( schema.meta1 ){
      data['metaKey1'] = schema.meta1;
    }

    if( schema.meta2 ){
      data['metaKey2'] = schema.meta2;
    }

    if( schema.meta3 ){
      data['metaKey3'] = schema.meta3;
    }



    let lastVersionRes = await retrieve(schema, null, ice2, true);
    // console.log(lastVersionRes)
    data['version'] = (lastVersionRes.version || 0) + 1;
  }

  if( publishNow ){
    data.status = 'y';
  } else {
    data.status = 'n';
  }

  data.description = description;
  data['json'] = JSON.stringify(json);

  return await new Promise((resolve, reject) => {

    request.post(
      {
        url:`${ice2.defaults.baseURL}/node/serviceFragment/save.json`,
        formData: data,
        headers : {
          Cookie :  proxyCookie,
        },
      },
      function optionalCallback(err, httpResponse, body) {
        if (err) {
          reject(new Error('fail'));
          return console.error('upload failed:', err);
        }

        let resJson = JSON.parse(body);
        console.log('/node/serviceFragment/save.json');
        console.log('Response : ', resJson);
        if(resJson.result == '200' || resJson.status == 200 ){
          let item = resJson.item;

          console.log('req successful!  Server responded with:', body);

          return resolve(item);
        }

        let errorObj = new Error(`${resJson.error} : ${resJson.message}`)

        reject(errorObj)
    });
  });
}


export function retrieve(schema, fallbackSchema, ice2, unconditionalHighestVersion){
  return new Promise((resolve, reject) => {
    if( schema.method == '#' ){
      ice2.request({
        url : '/node/serviceFragment/read.json',
        params : {
          serviceFragmentId : schema.id,
          status_matching : 'y',
        },
      }).then((r) => {
        let {
          data,
        } = r;

        if( data.item ){
          return resolve(data.item);
        }

        reject('not found');
      })
    } else if ( schema.method == '?' ){
        let query = {
        metaKey1_matching : schema.meta1,
        metaKey2_matching : schema.meta2,
        metaKey3_matching : schema.meta3,
        sorting: 'version desc',
        limit:1,
        status_matching : 'y',
      };

      if( unconditionalHighestVersion ){
        delete query.status_matching;
      }


      ice2.request({
        url : '/node/serviceFragment/list.json',
        params : query,
      }).then((r) => {
        let {
          data,
        } = r;
        console.log('retrieve-fragment', ice2.config, data, query)

        if( data.items && data.items[0] ){
          return resolve(data.items[0]);
        }

        reject(e);
      }).catch((e) => {
        reject(e);
      })
    }
  }).then((item) => {

    console.log(' Item >>', item);

    let json = item.json;
    let message = '';

    try {
      if( typeof json === 'string' ){
        json = JSON.parse(json);
      }
    } catch(e) {
      message = 'Broken fragment json';
    }

    return {
      ...item,
      json,
      message,
    }
  }, (err) => {

    console.log('Error>>>>>>>>>>::', err)

    if( fallbackSchema ){
      return retrieve.bind(this)(fallbackSchema, null, ice2)
        .then((item) => {

          if( schema.method == "#" ){
            return {
              serviceFragmentId : schema.id,
              json : item.json,
            }
          } else if ( schema.method == '?' ){
            return {
              json : item.json,
              metaKey1 : schema.meta1,
              metaKey2 : schema.meta2,
              metaKey3 : schema.meta3,
            }
          }
        })
    }


    if( schema.method == "#" ){
      return {
        serviceFragmentId : schema.id,
        json : defaultDirective,
      }
    } else if ( schema.method == '?' ){
      return {
        json : defaultDirective,
        metaKey1 : schema.meta1,
        metaKey2 : schema.meta2,
        metaKey3 : schema.meta3,
      }
    }
  }).catch((e) => {
    console.error(e);
    return null;
  })
}
