import {getValueByKeyPath} from '../../../../app/utils/ObjectExplorer';
let Errors = {};

createError(400, "DB_ERROR", "DB Error");

createError(400, "NORMAL", "normal");

// SIGNUP
createError(400, "SIGNUP.FULLNAME_FIELD_IS_REQUIRED", "Fullname field is required");
createError(400, "SIGNUP.EMAIL_FIELD_IS_REQUIRED", "EMAIL field is required");
createError(400, "SIGNUP.PW_FIELD_IS_REQUIRED", "PW field is required");
createError(400, "SIGNUP.PW_CONFIRM_FIELD_IS_REQUIRED", "PW_CONFIRM field is required");
createError(400, "SIGNUP.PASSWORD_IS_NOT_MATCHED", "Confirm password is not matched");
createError(400, "SIGNUP.NORMAL", "Error.");
createError(400, "SIGNUP.ALREADY_USED_EMAIL", 'Already used email.');
createError(400, "SIGNUP.SYSTEM_IS_NOT_ALLOW_FREE_SIGNUP", "System is not allow free signup.");
createError(400, "SIGNUP.FAIL_SENT_MAIL", "failed sent account certification mail");
createError(400, "SIGNUP.FAIL_CREATE_CERTIFICATION", 'failed create certification');
createError(400, 'SIGNUP.FAIL_CERTIFY_USER', 'fail certify user');
createError(400, 'SIGNUP.NOT_FOUND_CERTIFICATION', "Not found certification");
createError(400, 'SIGNUP.NOT_FOUND_CERTIFICATION_USER', "Not found certification user");
createError(400, "SIGNUP.USER_NOT_MATCHED_CERTIFICATION", "");
createError(400, "SIGNUP.COULD_NOT_FIND_CERTIFICATION_USER", "");

// SIGNIN
createError(400, "SIGNIN.ID_FIELD_IS_REQUIRED", "ID field is required");
createError(400, "SIGNIN.EMAIL_FIELD_IS_REQUIRED", "EMAIL field is required");
createError(400, "SIGNIN.PW_FIELD_IS_REQUIRED", "PW field is required");
createError(400, "SIGNIN.NORMAL", "Error.");
createError(400, "SIGNIN.PASSWORD_IS_NOT_MATCHED", "password is not matched");
createError(400, "SIGNIN.FAILED_CREATE_SESSION", "fail create session.");
createError(400, "SIGNIN.DO_CERTIFY_EMAIL", "do certify email");// 인증 메일 전송 요청 필요
createError(400, "SIGNIN.USER_IS_NOT_AVAILABLE_USER", "user is not available");
createError(400, "SIGNIN.USER_NOT_FOUND", "User not found");



// ICE CONNECT
createError(504, "SESSION.NO_JWT", "JWT Token is not exists");
createError(500, "SESSION.NOT_PERMISSION", 'Need a signed session');

// USER
createError(400, "USER.SESSION.NOT_FOUND", "User session not found"); // 세션을 찾을 수 없음
createError(400, "USER.SESSION.INVALID", "User session invalid"); // 세션이 유효하지 않음 유저 정보와 다름

// SITE
createError(400, "SITE.NOT_FOUND", "Site not found"); // 세션을 찾을 수 없음
createError(400, "SITE.USER_NO_PERMISSION", "Site user no permission");
createError(400, "SITE.FAIL_ACCESS.NEED_STUDIO_ID_PARAM", "사이트에 접근할 수 없습니다. studioID 파라미터가 필요합니다.");
createError(400, "SITE.FAIL_ACCESS.NO_PERMISSION", "사이트에 접근할 수 없습니다. 권한없음.");
createError(400, "SITE.FAIL_ACCESS.NEED_USER_SESSION_ID_PARAM", "사이트에 접근할 수 없습니다. userSessionID 파라미터가 필요합니다.");
createError(400, "SITE.FAIL_ACCESS.SITE_NO_EXISTS", "사이트가 존재하지 않습니다.");

createError(400, "SITE.STORE.INVALID_DEFINE", "사이트의 데이터가 유효하지 않습니다.");
createError(400, "SITE.STORE.INVALID_STRUCTURE_TYPE", "사이트의 구조 데이터 타입이 유효하지 않습니다.");
createError(500, "SITE.MODIFY.FAIL_SAVE_FRAGMENT", "사이트 프래그먼트 저장 실패");

// SITE.THEME.UPDATE
createError(400, "SITE.THEME.UPDATE.NO_PERMISSION", "사이트 테마 업데이트를 할 수 없습니다.");

// STUDIO
createError(400, "STUDIO.SESSION.NOT_FOUND", "Studio session not found"); // 세션을 찾을 수 없음
createError(400, "STUDIO.WORKER.FAIL_CREATE_SESSION", "Fail create worker session."); // 워커 세션 생성 실패
createError(400, "STUDIO.WORKER.CLIENT_NOT_FOUIND", "Not found worker client."); // 워커 클라이언트 찾을 수 없음.

// WORKER
createError(400, "WORKER.SESSION.NOT_FOUND", "Worker session not found");
createError(400, "WORKER.FAILED_CREATE_SESSION", "Fail create worker session.");


// SESSION
createError(400, "SESSION.NOT_FOUND_RELATED_USER", "Not found related user");// 데이터베이스에 세션에 해당하는 유저가 존재하지 않음
createError(400, "SESSION.COULD_NOT_FIND_RELATED_USER", "Couldn't find user."); // DataBase find Error
createError(400, "SESSION.INVALID_SESSION_OF_USER", "Invalid session of user");
createError(400, "SESSION.INVALID_SESSION", "Invalid session");
createError(500, "SESSION.FAIL_DELETE_SESSION", "Fail delete session");
createError(400, "SESSION.READ_ERROR", "Fail read session read.");

// UPLOAD_TEMPLATE
createError(500, "UPLOAD_TEMPLATE.TEMPLATE_UPLOAD_PATH_IS_NOT_DIRECTORY", "Temp template upload directory is not Directory.");

// PROJECT
createError(500, "PROJECT.CREATE.FAIL_UPLOAD_TEMPLATE_ZIP", "Fail upload template zip");
createError(500, "PROJECT.CREATE.ERROR", "Error");
createError(500, "PROJECT.CREATE.FAIL_CREATE_PROJECT", "Fail create project.");
createError(500, "PROJECT.CREATE.FAIL_CREATE_ROOTDIR", "Fail create rootdir.");
createError(500, "PROJECT.CREATE.FAIL_ROOTDIR_LINK", "Fail create rootdir and project relation.");
createError(500, "PROJECT.VFNODE.ROOT_FIND_FAIL", "Fail find project root vfnode");
createError(500, "PROJECT.VFNODE.FAIL_CREATE_DIR", "Fail create dir");
createError(500, "PROJECT.VFNODE.FAIL_CREATE_FILE", "Fail create filenode");
createError(500, "PROJECT.VFNODE.FAIL_READ", "Fail read vfnode");
createError(406, "PROJECT.VFNODE.CREATE.ALREADY_EXISTS_CHILDNODE_NAME", "fail create name is already exsists.");
createError(406, "PROJECT.VFNODE.CREATE.UPPER_VFNODE_IS_NOT_DIRECTORY", "fail create upper vfnode is not directory.");

// WORK
createError(500, "WORK.PROJECT_TEMPLATE_PARSER.FAILED_START", "failed start project template parser.");
createError(500, "WORK.PROJECT_TEMPLATE_PARSER.FAILED_CREATE_WORKER", "failed create project template parser worker.");
createError(500, "WORK.FAILED_UPDATE", "failed update work document.");


// AssignedStore
createError(500, "AS.FILE_DOES_NOT_EXIST", "File does not exist.");
createError(500, "AS.CAN_NOT_OPEN_FILE", "File does not exist.");
createError(500, "AS.CAN_NOT_READ_STATS", "File does not exist.");
createError(500, "AS.FAIL_SITE_ARCHIVING", "Fail to archiving site.");

// SITE_MANAGER.SITE_DATA
createError(416, "SITE_MANAGER.SITE_DATA.STRUCTURE_BROKEN", "Site data structure broken.");
createError(406, "SITE_MANAGER.SITE_DATA.INVALID_COMPONENT_CATEGORY", "Site data invalid component category.");
createError(404, "SITE_MANAGER.SITE_DATA.COMPONENT_NO_EXISTS", "Component no exists.");
createError(500, "SITE_MANAGER.SITE_DATA.FAIL_COMPONENT_PREVIEW_RENDERING", "Component preview rendering fail.");

createError(500, "SITE_MANAGER.SITE_DATA.ROUTE_DIRECTIVE_NO_EXISTS", "Site directive no exists.");
createError(500, "SITE_MANAGER.SITE_DATA.FAIL_MODIFY_DIRECTIVE_JSON", "Fail modify directive json.");

// SITE_MANAGER.ROUTE.CREATE
createError(406, "SITE_MANAGER.ROUTE.CREATE.FAIL_INVALID_CHARACTERS", "Included invalid characters.");

// SITE_MANAGER.RESOURCE.WRITE
createError(500, "SITE_MANAGER.RESOURCE.WRITE.FAIL_BASE64_IMAGE_WRITE", "Fail write image.");

function createError(_statusCode, _key, _message) {
  let e = new Error(_message);

  e.key = _key;
  e.statusCode = _statusCode;
  e.reason = _message;
  e.result = 'error';

  Errors[_key] = e;
}

export default function RETRIEVE(_ERROR_PATH) {
  let errorSpec = Errors[_ERROR_PATH];

  if (errorSpec) {

    let error = new Error(errorSpec.reason);
    error.statusCode = errorSpec.statusCode;
    error.result = 'error';
    error.key = errorSpec.key;
    error.reason = errorSpec.reason;

    return error;
  }

  throw new Error(`Not found Error'${_ERROR_PATH}' Object. `);
}
