import fs from 'fs';
import path from 'path';
import Promise from 'bluebird';
import mv from 'mv';
import tar from 'tar';
import uuid from 'uuid';
import jsonFile from 'jsonfile';
import {
  createSiteManager,
} from './SiteManager';

import Site from './Site';

import {
  Director,
} from '../../App';

import {
  Node,
} from '../FileNode';

export default class UserWorkspaceManager {
  userModel;
  ex;

  constructor(ex, userModel){
    this.ex = ex;
    this.userModel = userModel;


    this.checkInstall();
  }

  getSiteManager(CertifiecDataSet, accessibleLevel){
    return createSiteManager(this.ex, CertifiecDataSet, accessibleLevel, this.getWorkpacePath());
  }

  checkInstall(){
    let workspacePath = this.getWorkpacePath();


    if( !fs.existsSync(workspacePath) ){
      fs.mkdirSync(workspacePath);
    }

    let siteDir = this.getSiteDirPath();
    if( !fs.existsSync(siteDir) ){
      fs.mkdirSync(siteDir);
    }
  }

  getWorkpacePath(){
    let workspaceDir = Director.get().WORKSPACE_PATH;

    return path.join(workspaceDir, `/${this.userModel.seq}_${this.userModel.id}/`);
  }

  getSiteDirPath(){
    return path.join(this.getWorkpacePath(), '/sites/');
  }

  getSiteList(){
    let siteDir = this.getSiteDirPath();

    return new Promise((resolve, reject)=>{
      let node = Node.convertToNode(siteDir);
      node.loadChildrenAll()
        .then(()=>{

          Promise.all(node.children
            .filter((node)=>node.isDir)
            .map((node)=>new Promise((resolve)=>{

                let site = new Site(node.systemPath);
                node.siteOwner = this.userModel.id;

                site.getGatheringSiteMeta()
                  .then((meta) => {
                    resolve(Object.assign(node, meta));
                  })
              })
            ))
            .then((list)=>resolve(list.map((node)=>({
              name : node.name,
              accessKey : node.name,
              version : node.version,
              thumbnails : node.thumbnails,
              siteOwner: node.siteOwner,
              info: node.info,
            }))),
              (err)=>{
                console.error(err);
              });
        })
    })
  }

  createWithBringArchiveFileByPath(templateDirName, themeName, archivePath, userModel, siteName){
    let designatedStageFilePath = path.join(this.getSiteDirPath(), `/stage-${templateDirName}.tar.gz`);

    return new Promise((resolve, reject)=>{

      mv(archivePath, designatedStageFilePath, () => {
        let siteAccessName = `${templateDirName}.${uuid.v4()}`;

        let newSiteDirPath = path.join(this.getSiteDirPath(),siteAccessName);

        if( !fs.existsSync(newSiteDirPath) ){
          fs.mkdirSync(newSiteDirPath);
        }

        tar.x(  // or tar.extract(
          {
            file: designatedStageFilePath,
            cwd : newSiteDirPath,
          }
        ).then(()=>{

          let site = new Site(newSiteDirPath);
          site.setInfo({
            themeName : themeName,
            themeOriginDirName : templateDirName,
            siteName : siteName || themeName,
            owner : userModel.id,
            created : Date.now(),
            serviceVersion : '1.0',
            modified : null,
            lastPublished : null,
            published: false,
            siteAccessName: siteAccessName,
          })
            .then(()=>{
              site.initialConfig()
                .then(()=>{
                  resolve(siteAccessName);
                });
            });
        });
      });

    });
  }


  getSiteInfoJSONPath(siteID){
    return path.join(this.getWorkpacePath(), `/sites/${siteID}/meta/.SERVICE_DEFINE.json`);
  }


  readSiteInfo(siteID){
    return new Promise((resolve, reject)=>{


      let sites = Director.get().EDITABLES.map((editable) => ({
        ...editable,
        HOT_LINE_KEY: editable.id,
        siteId: editable.id,
        SERVICE_FRONT_FRAMEWORK: "vue",
        SERVICE_STRUCTURE_TYPE: "vue_basic",
        created: 1541522046538,
        "device-size": "fit",
        lastPublished: null,
        modified: null,
        owner: "system",
        published: false,
        secret: "5c5b998fe78574ba522d05c7cb4c0a0eca0def965ae9486b0e38ec416333db32",
        serviceVersion: "1.0",
        siteAccessName: editable.id,
        siteName: "jj",
        themeName: editable.id,
        themeOriginDirName: editable.id,
      }));


      let site = sites.find((s) => s.siteId === siteID );

      resolve({
        id: siteID,
        ...site,
      })

      // jsonFile.readFile(this.getSiteInfoJSONPath(siteID), function(err, obj){
      //   if( err ){
      //     reject(err);
      //   } else {
      //     resolve({
      //       id : siteID,
      //       ...obj,
      //     });
      //   }
      // });
    });
  }
}
