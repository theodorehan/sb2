import Promise from 'bluebird';
import http from 'http';
import FormData from 'form-data';
import child_process from 'child_process'
import {MetaNode} from 'abstract-component-node';
import RouteNode from '../../../../app/utils/RouteNode';
// import VueSingleRenderer from 'vue-single-component-html-renderer';
import jsonFile from 'jsonfile';
import path from 'path';
import uuid from 'uuid';
import tar from 'tar';
import namor from 'namor';
import fs from 'fs-extra';
import base64Img from 'base64-img';
import Site from './Site';
import Axios from 'axios';

import Errors from '../Error';
import {Node} from '../FileNode';

import {
  SITE_ARCHIVE_BUFFER_PATH,
  Logger,
  Director,
} from '../../App'

export const ACCESSIBLE_LEVEL_STUDIO = 'studio';
export const ACCESSIBLE_LEVEL_SYSTEM = 'system'; // 모든 파일에 접근 가능
export const ACCESSIBLE_LEVELS = [ACCESSIBLE_LEVEL_STUDIO,ACCESSIBLE_LEVEL_SYSTEM];

const NODE_MODULE_MANAGER = 'npm';

export const ETC_PATHNAME = '___etc';

export default class SiteServiceManager {
  siteNode;
  id;
  workspacePath;
  CertifiedDataSet;
  ex;


  constructor(ex, CertifiedDataSet, accessibleLevel, workspacePath, readyCallback){
    if( ACCESSIBLE_LEVELS.indexOf(accessibleLevel) === -1 ){
      throw new Error("Accessible level is required");
    }
    this.ex = ex;
    this.id = CertifiedDataSet.siteModel.id;
    this.workspacePath = workspacePath;
    this.CertifiedDataSet = CertifiedDataSet;

    this.siteNode = Node.convertToNode(this.getSiteDirPath());

    // this.siteNode.loadChildrenRecursiveAll({
    //   exclude : accessibleLevel === ACCESSIBLE_LEVEL_STUDIO ? ['node_modules','.idea', 'dist', /^\./] : [],
    // }).then( () => readyCallback(null) );
  }

  getSiteDirPath(){
    return path.join(this.workspacePath, '/sites/',this.id,'/')
  }

  getRoutesFilePath(){
    return path.join(this.getSiteDirPath(), '/directives/routes.json');
  }

  getPageJSONPath(pageKey){
    return path.join(this.getSiteDirPath(), '/directives/root', pageKey + '.json');
  }

  getPageVuePath(pageKey){
    return path.join(this.getSiteDirPath(), '/directives/root', pageKey + '.vue');
  }

  getAbsolutePathAtSite(systemPath){
    return systemPath.replace(this.getSiteDirPath(), '/');
  }

  getRootRoute(){
    return new Promise((resolve, reject)=>{
      let result = this.siteNode.findOne('routes.json');

      jsonFile.readFile(result.systemPath, {}, (err, data)=>{
        if( err ){
          reject(err)
        } else {
          resolve(RouteNode.importFromJSON(data));
        }
      });
    })
  }
  //
  // getRouteDirectivePathOfRouteNode(node){
  //   let parentGraph = node.getLinealDescentList();
  //   let pathTokens = parentGraph.map((node) => node.path);
  //
  //   let routePath = '/' + pathTokens.join('/');
  //   routePath = routePath.replace(/^\/\//, '/');
  //
  //   if( routePath === '/' ){
  //     return 'index';
  //   } else {
  //     return routePath.replace(/:/g,'@').replace(/^\//,'') + (node.group ? '/index':'');
  //   }
  // }

  createNewPage(originalRoutePath, routeName){

    return new Promise((resolve, reject)=>{

      // routePath에 .이 포함되면 거부한다.
      if( /\./.test(originalRoutePath) ){
        return reject(Errors('SITE_MANAGER.ROUTE.CREATE.FAIL_INVALID_CHARACTERS'));
      }


      this.getRootRoute()
        .then((routeNode)=>{
          let routePath = originalRoutePath.replace(/\/$/, '');
          let rootRouteNode = routeNode;
          let newPageRouteNode = RouteNode.importFromJSON({
            name : routeName,
            page : true,
            path : '',
            group : false,
          });

          let componentPath = routePath.replace(/^\//,'').replace(/:/g, '@');
          let routePathTokens = routePath.split('/');
          routePathTokens[0] = '/'; // path 는 항상 /로 시작하므로 슬래쉬로 스플릿 했을 때 0번 인덱스는 빈 문자열이 된다. 노드의 Path와 맞춰주기 위해 /로 치환한다.

          let routePathToken;
          let routeNodeRef = rootRouteNode;
          let garbagePathKeys = []; // 잔류 페이지키 최종 routeMap apply 후 잔류 페이지 제거 루틴 시작,

          console.log('routePathTokens', routePathTokens)

          // Ensure path group / 마지막 패스 노드가 위치할 그룹 노드를 보장해준다. ( 있으면 pass 또는 중간의 패스가 그룹이 아니라면 그룹으로 변환, 없으면 생성 )
          // slice(1, routePathTokens.length-1) 의 이유는 첫번째 토큰은 건너 띄고 마지막 패스 노드는 새로 만들어질 페이지 이므로 제외하기 위함
          Promise.map(routePathTokens.slice(1, routePathTokens.length-1),(routePathToken, index, length) => {

            console.log('routePathToken -> ', routePathToken);
            let findResult = routeNodeRef.visitChildren((node)=>node.path === routePathToken);
            console.log('result', findResult);


            return new Promise((resolve, reject) => {
              if( findResult ){
                routeNodeRef = findResult;

                if( !findResult.group ){
                  // 그룹이 아닌 Node 는 페이지 이다.
                  // 그룹이 아닌 페이지는 페이지 패스로 각 파일이름이 할당 되며.
                  // 각 페이지와 동시에 그룹으로 변화할 때 파일명이 index로 변경되고 해당 그룹디렉토리로 편입된다.
                  // 그러므로 group 으로 변경될 떄 파일을 이동해야 되는데, 파일이동은 studio 런타임에서는 워커에서 에러가 발생한다.
                  // (route 테이블이 변경되지 않은 상태에서 파일을 이동시켜 버리면 파일참조에 실패하기 때문에)
                  // 그러므로 파일을 복사 한다. 단 .json 파일만 복사 하며 .vue 파일은 다시 생성하도록 한다. (json 파일 참조를 위해 .vue 파일의 내용을 수정 해야 하기 때문)
                  // 그리고 worker 로 복사가 끝난 후 site 에서 잔류 파일을 삭제 한다.

                  let directivePathKey = findResult.getDirectivePath();
                  let directiveJSONPath = this.getPageJSONPath(directivePathKey);

                  garbagePathKeys.push(directivePathKey); // 잔류 페이지 등록
                  console.log('directivePath ->> ', directiveJSONPath);


                  // page 복사 루틴 시작
                  // 후 resolve 호출
                  fs.readJson(directiveJSONPath)
                    .then((pageJSON)=>{
                      console.log(directiveJSONPath,'->', pageJSON);

                      findResult.setData('group', true);
                      findResult.setData('children', []);
                      let groupPageDirectiveJSONPathKey = findResult.getDirectivePath();
                      let groupPageDirectiveJSONPath = this.getPageJSONPath(groupPageDirectiveJSONPathKey);
                      console.log('New DirectivePath ->> ', directiveJSONPath,'->',groupPageDirectiveJSONPath);


                      this.applyPageJSON(groupPageDirectiveJSONPathKey, pageJSON)
                        .then(()=>{
                          this.applyPageVue(groupPageDirectiveJSONPathKey)
                            .then(()=>{

                              resolve();
                            }, (err)=> reject(err));
                        },(err)=> reject(err));
                    });
                } else {
                  resolve();
                }
              } else {



                let newRouteNode = RouteNode.importFromJSON({
                  path : routePathToken,
                  name : routePathToken,
                  group : true,
                  page : false,
                  children : [],
                });
                console.log('routeNodeRef', routeNodeRef)
                routeNodeRef.appendChild(newRouteNode);
                routeNodeRef = newRouteNode;

                resolve();
              }
            });
          })
            .then(() => {
              newPageRouteNode.setData('path', routePathTokens[routePathTokens.length -1]);
              routeNodeRef.appendChild(newPageRouteNode);

              this.modifyRoutes(rootRouteNode.exportToJSON())
                .then(()=>{
                  let routesFileSystemPath = this.getRoutesFilePath();

                  let absolutePathAtSite = this.getAbsolutePathAtSite(routesFileSystemPath);

                  let studioClient = this.getStudioClient();
                  if( studioClient ){
                    let workerClient = studioClient.workerClient;

                    this.applyPageJSON(componentPath, this.createNewPageJSON())
                      .then(()=>{
                        this.applyPageVue(componentPath)
                          .then(()=>{
                            workerClient.applySiteFile(absolutePathAtSite, routesFileSystemPath, {
                              withRestart : true,
                            })
                              .then(()=>{

                                // 잔류 페이지 제거
                                Promise.all(garbagePathKeys.map(this.removePage.bind(this)))
                                  .then(() => {

                                    resolve(routeNode.name);
                                  });
                              });
                          }, (err)=> reject(err));
                      },(err)=> reject(err));
                  } else {
                    reject(new Error('Not found studioClient'));
                  }
                }, (err) =>  reject(err));
            });

        })
    })
  }


  removePage(pageKey){
    let pageVuePath = this.getPageVuePath(pageKey);
    let pageJSONPath = this.getPageJSONPath(pageKey);


    return new Promise((resolve, reject) => {

      fs.remove(pageVuePath)
        .then(()=>{
          fs.remove(pageJSONPath)
            .then(()=>{
              resolve();

              // worker 는 상관없다 어차피 휘발성 구조이므로.
              // worker 에 apply 하지 않는다.
            })
        });
    });
  }

  modifyRoutes(routes){
    let routesFilePath = this.getRoutesFilePath();
    return new Promise((resolve, reject)=>{
      jsonFile.writeFile(routesFilePath, routes, function(err){
        if( err ){
          reject(err);
        } else {
          resolve();
        }
      });
    });
  }


  /**
   *
   * @param pageKey
   * @param json
   */
  applyPageJSON(pageKey, json){
    return new Promise((resolve, reject)=>{
      let pageJSONPath = this.getPageJSONPath(pageKey);

      fs.outputJSON(pageJSONPath, json, (err) => {
        if( err ){
          return reject(err);
        }

        let studioClient = this.getStudioClient();

        if( studioClient ){
          let workerClient = studioClient.workerClient;
          workerClient.applySiteFile(this.getAbsolutePathAtSite(pageJSONPath), pageJSONPath)
            .then(()=>{
              resolve();
            });
        } else {
          reject( new Error('Not found studioClient'));
        }
      });
    });
  }

  /**
   * applyPageVue
   * @param pageKey
   */
  applyPageVue(pageKey){
    return new Promise((resolve, reject)=>{
      let pageVuePath = this.getPageVuePath(pageKey);

      fs.outputFile(pageVuePath, this.getNewPageVueTemplate(pageKey), (err) => {
        if( err ){

          return reject(err);
        }

        let studioClient = this.getStudioClient();
        if( studioClient ){
          let workerClient = studioClient.workerClient;

          workerClient.applySiteFile(this.getAbsolutePathAtSite(pageVuePath), pageVuePath)
            .then(()=>{
              resolve();
            });
        } else {
          reject(new Error('Not found studioClient.'));
        }

      });
    });
  }

  createNewPageJSON(){
    return {
      "tag" : "core-system-grid",
      "classes" : {
      },
      "props" : {
      },
      "children" : [],
    };
  }

  getNewPageVueTemplate(pageKey){
    return `
      <script>
        export default require('~/../ServiceAssembly/directiveRoute.js').default('root/${pageKey}');
      </script>
      `;
  }

  writeResourceBase64Image(safeResourcePath, data, overwrite){
    let srcNode = this.siteNode.findChild('src');
    let staticNode = srcNode.findChild('static');

    let staticSystemPath = staticNode.systemPath;
    let destination = path.join(staticSystemPath, '/image/', safeResourcePath.replace(/^___etc\//,''));

    return new Promise((resolve, reject)=>{
      base64Img.img(data, path.dirname(destination), path.basename(destination), function (err, filepath) {
        console.log('image write',err, filepath);
          if(err){
            reject(Errors('SITE_MANAGER.RESOURCE.WRITE.FAIL_BASE64_IMAGE_WRITE'));
          } else {
            resolve();
          }
        });
    });
  }


  getResourceMajorCategories(target){
    return new Promise((resolve, reject)=>{
      let srcNode = this.siteNode.findChild('src');
      let staticNode = srcNode.findChild('static');

      if( !staticNode ) return reject(Errors('SITE_MANAGER.SITE_DATA.STRUCTURE_BROKEN'));

      let resourceTargetNode = staticNode.findChild(target);

      let categories = resourceTargetNode.children.filter( (_node) => !!_node.isDir );

      categories = categories.map((node)=> this.nodeConvertToResourceCategoryItem(node));

      let etcItems = resourceTargetNode.children.filter((node) => !node.isDir);

      let etcItemLength = etcItems.length;

      categories.push({
        name : 'etc',
        pathName : ETC_PATHNAME,
        itemCount : etcItemLength,
      });

      resolve(categories);
    })
  }

  getResourceCategorySubWithChildren(target, path){
    return new Promise((resolve, reject)=>{
      let srcNode = this.siteNode.findChild('src');

      if( !srcNode ) return reject(Errors('SITE_MANAGER.SITE_DATA.STRUCTURE_BROKEN'));

      let staticNode = srcNode.findChild('static');
      let resourceTargetNode = staticNode.findChild(target);

      if( path === ETC_PATHNAME ){

        let categoryItem = this.nodeConvertToResourceCategoryItem(resourceTargetNode);

        categoryItem.children = resourceTargetNode.children.filter((node)=> !node.isDir)
          .map((node)=>this.nodeConvertToResourceCategoryItem(node));

        categoryItem.name = 'etc';
        categoryItem.pathName = ETC_PATHNAME;
        categoryItem.itemCount = categoryItem.children.length;

        resolve( categoryItem );
      } else {
        let tokenizedPath = path.split('/');

        let targetNode = resourceTargetNode;
        for(let i = 0; i < tokenizedPath.length; i++ ){
          targetNode = targetNode.findChild(tokenizedPath[i]);

          if( !targetNode ){
            return reject(Errors('SITE_MANAGER.SITE_DATA.INVALID_COMPONENT_CATEGORY'));
          }
        }

        let categoryItem = this.nodeConvertToResourceCategoryItem(targetNode);

        categoryItem.children = targetNode.children.map((node)=>this.nodeConvertToResourceCategoryItem(node));

        resolve( categoryItem );
      }
    })
  }

  getBuiltInComponentMajorCategories(){
    return new Promise((resolve, reject)=>{
      let srcNode = this.siteNode.findChild('src');

      if( !srcNode ) return reject(Errors('SITE_MANAGER.SITE_DATA.STRUCTURE_BROKEN'));

      let builtinComponentsNode = srcNode.findChild('components');

      let categories = builtinComponentsNode.children.filter( (_node) => !!_node.isDir );

      categories = categories.map((node)=> this.nodeConvertToComponentCategoryItem(node) );

      let etcItems = this.filterComponentCategoryItem(builtinComponentsNode.children);
      etcItems = this.filterCategoryVueItem(etcItems);
      let etcItemLength = etcItems.length;

      categories.push({
        name : 'etc',
        pathName : ETC_PATHNAME,
        itemCount : etcItemLength,
      });

      resolve(categories);
    })
  }


  getBuiltInComponentCategorySubWithChildren(path){
    return new Promise((resolve, reject)=>{
      let srcNode = this.siteNode.findChild('src');

      if( !srcNode ) return reject(Errors('SITE_MANAGER.SITE_DATA.STRUCTURE_BROKEN'));

      let builtinComponentsNode = srcNode.findChild('components');

      if( path === ETC_PATHNAME ){

        let categoryItem = this.nodeConvertToComponentCategoryItem(builtinComponentsNode);

        categoryItem.children = this.filterCategoryVueItem(this.filterComponentCategoryItem(builtinComponentsNode.children)
          .map((node)=>this.nodeConvertToComponentCategoryItem(node)));

        categoryItem.name = 'etc';
        categoryItem.pathName = ETC_PATHNAME;
        categoryItem.itemCount = categoryItem.children.length;

        resolve( categoryItem );
      } else {
        let tokenizedPath = path.split('/');

        let targetNode = builtinComponentsNode;
        for(let i = 0; i < tokenizedPath.length; i++ ){
          targetNode = targetNode.findChild(tokenizedPath[i]);

          if( !targetNode ){
            return reject(Errors('SITE_MANAGER.SITE_DATA.INVALID_COMPONENT_CATEGORY'));
          }
        }

        let categoryItem = this.nodeConvertToComponentCategoryItem(targetNode);

        categoryItem.children = this.filterComponentCategoryItem(targetNode.children)
          .map((node)=>this.nodeConvertToComponentCategoryItem(node));

        resolve( categoryItem );
      }
    })
  }

  getBuiltInComponentPreview(componentPath){
    return new Promise((resolve, reject)=>{
      let srcNode = this.siteNode.findChild('src');

      if( !srcNode ) return reject(Errors('SITE_MANAGER.SITE_DATA.STRUCTURE_BROKEN'));

      let builtinComponentsNode = srcNode.findChild('components');

      let componentPathToken = componentPath.split('/');
      let findResult = builtinComponentsNode;
      for(let i = 0; i < componentPathToken.length;i++){
        if( componentPathToken[i] === ETC_PATHNAME ) continue;

        findResult = findResult.findChild(componentPathToken[i]);

        if( !findResult ){
          break;
        }
      }

      if( findResult ){

        // VueSingleRenderer(findResult.systemPath)
        //   .then(({html, style})=>{
        //     resolve({html, style});
        //   }, (err)=>{
        //     reject(Errors('SITE_MANAGER.SITE_DATA.FAIL_COMPONENT_PREVIEW_RENDERING'))
        //   })
      } else {
        reject(Errors('SITE_MANAGER.SITE_DATA.COMPONENT_NO_EXISTS'))
      }
    });
  }

  getBuiltInComponentPreviewImagePath(componentPath){
    let srcNode = this.siteNode.findChild('src');

    let componentPreviewImagePath = componentPath.replace(/\.vue$/, '.png');

    if( !srcNode ) return reject(Errors('SITE_MANAGER.SITE_DATA.STRUCTURE_BROKEN'));

    let builtinComponentsNode = srcNode.findChild('components');

    let componentImagePathToken = componentPreviewImagePath.split('/');
    let findResult = builtinComponentsNode;
    for(let i = 0; i < componentImagePathToken.length;i++){
      if( componentImagePathToken[i] === ETC_PATHNAME ) continue;

      findResult = findResult.findChild(componentImagePathToken[i]);

      if( !findResult ){
        return null;
      }
    }

    return findResult.systemPath;
  }

  defaultDirectiveJSON(){
    return {
      "tag": "core-system-grid",
      "classes": {},
      "props": {},
      "children": [
        {
          "tag": "core-system-row",
          "children": [
            {
              "tag": "core-system-column",
              "props": {
                "xs": 12,
                "sm": 12,
                "md": 12,
                "lg": 12,
              },
              "children": [

              ],
            },
          ],
        },
      ],
    };
  }

  getRouteDirective(_directivePath){
    let directivesNode = this.siteNode.findChild('directives');

    if( /^@contents@/.test(_directivePath) ){


      return new Promise((resolve, reject)=>{
        let contentsNode = directivesNode.findChild('@contents@');
        if( !contentsNode ) {
          return resolve(this.defaultDirectiveJSON());
        }
        let directiveName = _directivePath.replace('@contents@','');

        jsonFile.readFile(path.join(contentsNode.systemPath,  '/', directiveName+'.json'), (_err, _data) => {
          if (_err) {
            resolve(this.defaultDirectiveJSON());
          } else {
            resolve(_data);
          }
        });
      });
    } else {

      let directiveJSONPath = _directivePath + '.json';
      let routeDirectiveNode = directivesNode.stridePath(directiveJSONPath);

      /**
       * 계층 구조의 패스의 경우 계층을 타고 탐색하도록 수정한다.
       */


      return new Promise((resolve, reject)=>{
        if( !routeDirectiveNode ){
          reject(Errors('SITE_MANAGER.SITE_DATA.ROUTE_DIRECTIVE_NO_EXISTS'));
          return;
        }

        jsonFile.readFile(routeDirectiveNode.systemPath, (_err, _data) => {
          if (_err) {
            reject(Errors('SITE_MANAGER.SITE_DATA.ROUTE_DIRECTIVE_NO_EXISTS'));
          } else {
            resolve(_data);
          }
        });
      });
    }
  }


  modifyRouteDirective(_directivePath, directiveJSON){
    let directivesNode = this.siteNode.findChild('directives');

    if( /^@contents@/.test(_directivePath) ){
      let contentsNode = directivesNode.findChild('@contents@');

      if( !contentsNode ){
        directivesNode.createChildDirNodeSync('@contents@');
        contentsNode = directivesNode.findChild('@contents@');
      }

      let directiveName = _directivePath.replace('@contents@','');

      return new Promise((resolve, reject)=>{
        let directiveSavePath = path.join(contentsNode.systemPath, '/', directiveName+'.json');

        jsonFile.writeFile(directiveSavePath, directiveJSON, (_err) => {
          if (_err) {
            reject(Errors('SITE_MANAGER.SITE_DATA.FAIL_MODIFY_DIRECTIVE_JSON'));
          } else {
            let absolutePathAtSite = this.getAbsolutePathAtSite(directiveSavePath);

            let studioClient = this.getStudioClient();
            if( studioClient ){
              let workerClient = studioClient.workerClient;

              workerClient.applySiteFile(absolutePathAtSite, directiveSavePath)
                .then(()=>{
                  resolve({
                    filePathInSiteRoot : directiveSavePath.replace(this.getSiteDirPath(), '/'),
                    directiveJSON,
                  });
                });
            } else {
              reject(new Error('Not found studioClient'));
            }
          }
        });
      });
    } else {

      let directiveJSONPath = _directivePath + '.json';
      let routeDirectiveNode = directivesNode.stridePath(directiveJSONPath);

      /**
       * 계층 구조의 패스의 경우 계층을 타고 탐색하도록 수정한다.
       */


      return new Promise((resolve, reject)=>{
        if( !routeDirectiveNode ){
          reject(Errors('SITE_MANAGER.SITE_DATA.ROUTE_DIRECTIVE_NO_EXISTS'));
          return;
        }

        jsonFile.writeFile(routeDirectiveNode.systemPath, directiveJSON, (_err) => {
          if (_err) {
            reject(Errors('SITE_MANAGER.SITE_DATA.FAIL_MODIFY_DIRECTIVE_JSON'));
          } else {
            let absolutePathAtSite = this.getAbsolutePathAtSite(routeDirectiveNode.systemPath);

            let studioClient = this.getStudioClient();
            if( studioClient ){
              let workerClient = studioClient.workerClient;

              workerClient.applySiteFile(absolutePathAtSite, routeDirectiveNode.systemPath)
                .then(()=>{
                  resolve({
                    filePathInSiteRoot : routeDirectiveNode.systemPath.replace(this.getSiteDirPath(), '/'),
                    directiveJSON,
                  });
                });


              /**
               * 따로 동작하는 사이트에 Directive 를 반영하기 위해 만든 코드
               *
               */
              console.log('Level', Director.get().DEFAULT_SITE_CONFIG_LEVEL)
              let site = new Site(this.getSiteDirPath());
              site.readServiceConfig(Director.get().DEFAULT_SITE_CONFIG_LEVEL)
                .then((config) => {
                  let {
                    detachedSites,
                  } = config;
                  console.log('config',config)

                  site.getInfo()
                    .then((info) => {
                      let {
                        secret,
                      } = info;
                      console.log('info', info);

                      let formData = new FormData();
                      formData.append('type', 'text');
                      formData.append('relativePath', absolutePathAtSite);
                      formData.append('systemPath', routeDirectiveNode.systemPath);
                      formData.append('file', JSON.stringify(directiveJSON));

                      Promise.map(detachedSites || [], (siteURL) => new Promise((resolve, reject) => {
                          console.log('Send /_app/builder-hook/recieve-file');

                          let siteURLTokens = siteURL.split(':');
                          let proto = siteURLTokens[0];
                          let host = siteURLTokens[1].replace(/^\/\//, '');
                          let port = siteURLTokens[2];

                          formData.submit({
                            host: host,
                            port : parseInt(port || '80'),
                            path: '/_app/builder-hook/recieve-file',
                            headers: {
                              'secret': secret,
                            },
                          }, (err, res) => {
                            if( err ) {
                              resolve(0);
                            } else {
                              resolve(0);
                            }
                          })
                        })
                      )
                        .then(() => {
                          console.log('Complete apply detached sites');
                        })
                    })
                });


            } else {
              reject(new Error('Not found studioClient'));
            }




          }
        });
      });
    }
  }

  getStudioClient(){
    let {studioSessionID} = this.CertifiedDataSet.userSessionData;
    let studioClient = this.ex.get('StudioSocketsManager').findClient(studioSessionID);

    return studioClient;
  }

  nodeConvertToComponentCategoryItem(node){
    if( node.isDir ){


      return {

        name : node.name,
        pathName : node.name,
        hasSubCategory : true,
        itemCount : this.filterComponentCategoryItem(node.children).length,
      };
    } else {
      let componentNameBaseDir = path.join(this.siteNode.systemPath, '/src/components/');

      return {
        componentKey : node.systemPath.replace(componentNameBaseDir,'').replace(/\.vue/i,'').replace(new RegExp(path.sep, 'g'), '-').toLowerCase(),
        name : node.name,
        pathName : node.name,
      }
    }
  }

  nodeConvertToResourceCategoryItem(node){
    if( node.isDir ){


      return {
        name : node.name,
        pathName : node.name,
        modify : node.modify,
        hasSubCategory : true, // subCategory 는 리소스 파일도 포함한다. 디렉토리의 경우 hasSubCategory 속성이 부여된다.
        itemCount : node.children.length,
      };
    } else {
      let resourceBasePath = path.join(this.siteNode.systemPath, '/src/static/');

      return {
        file : node.systemPath.replace(resourceBasePath,''),
        name : node.name,
        modify : node.modify,
        pathName : node.name,
      }
    }
  }

  filterComponentCategoryItem(_array){
    return _array.filter( (node) => node.isDir || /\.vue$/.test(node.name) )
  }

  filterCategoryVueItem(_array){
    return _array.filter( (node) => (!node.isDir) && /\.vue$/.test(node.name) )
  }


  siteArchiving(forRealDeploy) {
    let archiveName = `${uuid.v4()}.${this.id}.tar.gz`;
    let tarballPath = path.join(SITE_ARCHIVE_BUFFER_PATH, archiveName);
    let sitePath = this.getSiteDirPath();

    let node = Node.convertToNode(sitePath);

    return new Promise((resolve, reject)=>{

      node.loadChildrenRecursiveAll({
        exclude: ['node_modules','.idea', 'dist', '.git'], // .nuxt 를 포함한다.
      }).then(()=>{
        let targetFileAndDirList = node.children.map( (node)=>node.systemPath.replace(sitePath,'./'));


        tar.c(
          {
            gzip: true,
            file: tarballPath,
            cwd : sitePath,
            filter : (filepath, stat)=>{

              // 실제 서비스 deploy 전용 tar에는 static 리소스를 포함한다.
              if( !forRealDeploy ){
                if( filepath.indexOf('./src/static/') === 0 ){
                  return false;
                }
              }

              return true;
            },
          },
          targetFileAndDirList,
        ).then( (_) => {
          resolve(tarballPath);
        }, (err) =>{

          Logger.get().error(`사이트 압축 실패 ${this.id}` ,err);
          reject(Errors('AS.FAIL_SITE_ARCHIVING'));
        });
      });
    });
  }


  siteNPMInstall() {
    console.log(`site : ${this.id} ${NODE_MODULE_MANAGER} install started.`);

    let command = `${NODE_MODULE_MANAGER} install --no-optional --ignore-optional --ignore-platform`;
    const child = child_process.exec(command, {
      cwd: this.getSiteDirPath(),
    });

    console.log('Executed command:', command);
    return new Promise((_resolve, _reject) => {
      child.stdout.on('data', (data) => {
        console.log(`NPM Install stdout: ${data}`);
      });
      child.stderr.on('data', (data) => {
        console.log(`NPM Install stderr: ${data}`);
        // _reject(data);
      });
      child.on('close', (code) => {
        console.log(`child process exited with code ${code}`);

        if( code === 0 ){
          console.log(`${NODE_MODULE_MANAGER} installed.`);
          _resolve({result:true});
        } else {
          console.log(`Fail npm install of Site ${code}`);
          _reject(code);
        }
      });
    })
  }

  siteBuild(){
    console.log(`site : ${this.id} ${NODE_MODULE_MANAGER} build started.`);
    let command = `${NODE_MODULE_MANAGER} run build`;

    const child = child_process.exec(command, {
      cwd: this.getSiteDirPath(),
    });

    console.log('Executed command:', command);
    return new Promise((_resolve, _reject) => {
      child.stdout.on('data', (data) => {
        console.log(`Site build process stdout: ${data}`);
      });
      child.stderr.on('data', (data) => {
        console.log(`Site build process stderr: ${data}`);
        // _reject(data);
      });
      child.on('close', (code) => {
        console.log(`child process exited with code ${code}`);

        if( code === 0 ){
          console.log(`${NODE_MODULE_MANAGER} built.`);
          _resolve();
        } else {
          console.log(`Fail build ${code}`);
          _reject(code);
        }
      });
    })
  }
}


export function createSiteManager(ex, CertifiedDataSet, accessibleLevel, workspacePath){
  return new Promise((resolve, reject)=>{
    let siteService = new SiteServiceManager(ex, CertifiedDataSet, accessibleLevel, workspacePath, (err)=>{

      if( err === null ){
        resolve(siteService);
      } else {
        reject(err);
      }
    });
  });
}
