import Promise from 'bluebird';
import {MetaNode} from 'abstract-component-node';
import path from 'path';
import uuid from 'uuid';
import tar from 'tar';
import fs from 'fs';
import fse from 'fs-extra';
import merge from 'lodash/merge';

import Errors from '../Error';
import {Node} from '../FileNode';


import {
  SITE_ARCHIVE_BUFFER_PATH,
  Logger,
  Director,
  SITE_STORE_DIR as THEME_STORE_DIR,
} from '../../App'

import {vue_basic} from "../../foundationData/service/defineStructureTypes"


/**
 * @class Site
 * @description 사용자의 Site 와 theme 를 아우르는 Site Structure를 관리하고 상호작용 하는 클래스
 */
export default class Site {
  siteSystemPath;

  constructor(siteSystemPath){
    this.siteSystemPath = siteSystemPath;

    this.siteNode = Node.convertToNode(this.siteSystemPath);
  }

  loadTreeStructure(){
    return this.siteNode.loadChildrenRecursiveAll({
      exclude : vue_basic.STUDIO_INACCESSIBLE_FILES,
    });
  }


  // level : local, dev, prod, stage
  serviceConfig(level, configJSON){
    return new Promise((resolve, reject)=>{

      this.readServiceConfig(level)
        .then((baseConfig) => {
          let configPath = path.join(this.siteNode.systemPath, '/config/', `app.${level}.config.json`);

          fse.writeJson(configPath, merge({}, baseConfig, configJSON))
            .then(()=>{
              resolve();
            });
        });
    });
  }

  readServiceConfig(level){
    return new Promise((resolve, reject)=>{
      let configPath = path.join(this.siteNode.systemPath, '/config/', `app.${level}.config.json`);

      fse.readJson(configPath)
        .then((json)=>{
          resolve(json);
        })
    });
  }

  initialConfig(){
    return new Promise((resolve, reject)=>{
      let director = Director.get();

      let config = {
        "http" : {
          "defaultAPIHost" : director.ServerConfig.service_default_api_host,
          "timeout" : 10000,
        },


        "contents" : {
          "host" : "[loopback]",
        },

        "builder" : {
          "host" : director.BUILDER_URL,
        },

        "googleMap" : { apiKey : director.GOOGLE_MAP_DEFAULT_API_KEY},
      };


      Promise.all([
        this.serviceConfig('local', Object.assign({}, config, {
          http : Object.assign({}, config.http, {
            'defaultAPIHost' : director.ServerConfig.service_local_default_api_host,
          }),
        })),
        this.serviceConfig('dev',  Object.assign({}, config, {
          http : Object.assign({}, config.http, {
            'defaultAPIHost' : director.ServerConfig.service_dev_default_api_host,
          }),
        })),
        this.serviceConfig('stage',  Object.assign({}, config, {
          http : Object.assign({}, config.http, {
            'defaultAPIHost' : director.ServerConfig.service_stage_default_api_host,
          }),
        })),
        this.serviceConfig('prod',  Object.assign({}, config, {
          http : Object.assign({}, config.http, {
            'defaultAPIHost' : director.ServerConfig.service_prod_default_api_host,
          }),
        })),
      ])
        .then(()=>{
          resolve();
        })
    });
  }

  readConfigEachLevel(){
    return new Promise((resolve, reject) => {
      Promise.all([
        this.readServiceConfig('local'),
        this.readServiceConfig('dev'),
        this.readServiceConfig('stage'),
        this.readServiceConfig('prod'),
      ])
        .spread((local, dev, stage, prod)=>{
          resolve({
            local,
            dev,
            stage,
            prod,
          });
        });
    })
  }

  loadCoreStructuredNodes(){
    return this.siteNode.loadChildrenRecursiveAll({
      include : vue_basic.CORE_STRUCTURED_FILES,
    });
  }


  getInfo(){
    let serviceInfoJSON = path.join(this.siteNode.systemPath, '/meta/.SERVICE_DEFINE.json');
    return fse.readJson(serviceInfoJSON)
  }

  setInfo(info){
    return new Promise((resolve, reject)=>{
      this.getInfo()
        .then((oldInfo)=>{
          let fleshInfo = Object.assign({}, oldInfo, info);

          let serviceInfoJSON = path.join(this.siteNode.systemPath, '/meta/.SERVICE_DEFINE.json');
          fse.writeJson(serviceInfoJSON, fleshInfo)
            .then(()=>{
              resolve();
            })
        });
    })
  }


  modified(timestamp){
    return new Promise((resolve, reject)=>{
      this.getInfo()
        .then((info)=>{
          let {
            modifyCount = 0,
          } = info;

          this.setInfo({
            modifyCount : modifyCount + 1,
            modified : timestamp,
          }).then(()=>{
            resolve();
          })
        });
    });
  }

  getPackageJSON(){
    let packageJSONPath = path.join(this.siteNode.systemPath, '/package.json');
    return fse.readJson(packageJSONPath)
  }

  getVersion(){
    return this.getPackageJSON()
      .then((json) => json.version);
  }

  getThumbnails(){
    let thumbnailsPath = path.join(this.siteNode.systemPath, '/themeInfo/thumbnails');

    let node = Node.convertToNode(thumbnailsPath);
    return new Promise((resolve, reject)=>{
      if( node ){
        node.loadChildrenAll()
          .then(()=>{
            resolve(node.children.map((node)=>node.name));
          }, (err)=>{
            reject(err);
          })
      } else {
        resolve([]);
      }
    })
  }


  getPublishedNodeStates(){
    let packageJSONPath = path.join(this.siteNode.systemPath, '/.published-node-states.json');
    return new Promise((resolve, reject)=>{
      fse.readJson(packageJSONPath)
        .then((json)=>{
          resolve(json);
        }, () => {
          resolve([
            {
              port : null,
            },
          ]);
        })
    })
  }

  getGatheringSiteMeta(){
    return new Promise((resolve, reject)=>{
      let meta = {
        thumbnails : [],
        info : {},
        version : null,
      };

      this.getVersion()
        .then((version)=>{
          meta.version = version;

          this.getThumbnails()
            .then((list)=>{

              meta.thumbnails = list;

              this.getInfo()
                .then((info)=>{

                  meta.info = info;

                  resolve(meta);
                }, (err)=>{
                  reject(err);
                })
            }, (err)=>{
              reject(err);
            })
        }, (err)=>{
          reject(err);
        })
    })
  }

  getThumbnailPath(name){
    return path.join(this.siteNode.systemPath, '/themeInfo/thumbnails/', name);
  }

  updateTheme(){

    return new Promise((resolve, reject) =>{
      this.getInfo()
        .then((info)=>{
          this.lock('update theme')
            .then((lockKey)=>{
              let {themeOriginDirName} = info;
              let theme = new Site(path.resolve(THEME_STORE_DIR, themeOriginDirName));

              theme.loadCoreStructuredNodes()
                .then(()=>{
                  Promise.each(theme.siteNode.children, (node) => new Promise((resolve, reject) => {
                    let halfAbsolutePath = node.systemPath.replace(theme.siteNode.systemPath,'');

                    fse.copy(node.systemPath, path.join(this.siteNode.systemPath, halfAbsolutePath))
                      .then(()=>{
                        resolve()
                      });
                  }))
                    .then(()=>{

                    this.unlock(lockKey)
                      .then(()=>{
                        resolve()
                      })
                    })
                  // 하위 모듈 모두 로딩
                  // Promise.all(theme.siteNode.children.filter((node)=>node.isDir).map((node)=>{
                  //   return node.loadChildrenRecursiveAll();
                  // }))
                  //   .then(()=>{
                  //     let migrateTargetNodes = [];
                  //
                  //     theme.siteNode.recursiveVisit(function (node) {
                  //       migrateTargetNodes.push(node);
                  //     });
                  //
                  //
                  //     Promise.each(migrateTargetNodes, (node)=>{
                  //       return new Promise((resolve, reject) => {
                  //         let halfAbsolutePath = node.systemPath.replace(theme.siteNode.systemPath,'');
                  //         console.log(halfAbsolutePath);
                  //         fse.copy(node.systemPath, path.join(this.siteNode.systemPath, halfAbsolutePath))
                  //           .then(()=>{
                  //             resolve()
                  //           })
                  //       });
                  //     })
                  //     console.log(migrateTargetNodes);
                  //   })
                })
            });
        });
    });
  }


  lockfilePath(){
    return path.join(this.siteNode.systemPath, '/site.lock');
  }

  lock(purpose){
    let lockfile = this.lockfilePath();

    let lockKey = uuid.v4();

    return new Promise((resolve,reject) => {
      let existsLockJson = {
        locks : [],
      };

      if( this.isLock() ){
        existsLockJson = fse.readJsonSync(lockfile);
      }

      existsLockJson.locks.push({purpose, lockKey});

      fse.writeJson(lockfile, existsLockJson)
        .then(()=>{
          resolve(lockKey);
        })
    });
  }

  isLock(){
    return fs.existsSync(this.lockfilePath());
  }

  unlock(lockKey){
    let lockfile = this.lockfilePath();

    return new Promise((resolve,reject)=>{
      if( this.isLock() ){
        fse.readJson(lockfile)
          .then((lockJSON)=>{

            if( lockJSON.locks.find((lock)=> lock.lockKey == lockKey ) ){
              lockJSON.locks = lockJSON.locks.filter((lock)=>lock.lockKey != lockKey );

              if( lockJSON.locks.length > 0 ){
                fse.writeJson(lockfile, lockJSON)
                  .then(()=>{
                    resolve();
                  })
              } else {
                fse.remove(lockfile)
                  .then(()=>{
                    resolve();
                  })
              }
            } else {
              reject(); // lockKey 에 해당하는 lock이 없음
            }
          })
      } else {
        reject(); // 락 상태가 아님
      }
    });
  }
}
