import pick from 'lodash/pick';
import Errors from './Error';


export async function sessionFilter(req, res){
  try {


    console.log('sessionFilter', req.headers);

    let header = {
      Host : req.headers.host,
      'User-Agent' : req.headers['user-agent'],
    };
    if( req.cookies.iceJWT && req.cookies.iceRefreshJWT ){
      header =  {

        Cookie : `iceJWT=${req.cookies.iceJWT || ''}; iceRefreshJWT=${req.cookies.iceRefreshJWT || ''};`,
        'User-Agent' : req.headers['user-agent'],
      };
    }

    let tokenResponse = null;
    try{

      console.log('get token', header)
      // token 동기화
      tokenResponse = await req.ice2.request({
        url : '/api/auth/token',
        method : 'get',
        withCredentials:true,
        headers : header,
      });


      // console.log('tokenResponse',tokenResponse);
    } catch(e){
      console.error(e);
      // token 동기화 쿠키 리셋을 위해 header 를 세팅하지 않고 다시 토큰 호출
      tokenResponse = await req.ice2.request({
        url : '/api/auth/token',
        method : 'get',
        withCredentials:true,
        headers : {
          'User-Agent' : req.headers['user-agent'],
          Host : req.headers.host,
          Cookie : '',
        },
      });
    }

    // console.log(tokenResponse, `iceJWT=${req.cookies.iceJWT}; iceRefreshJWT=${req.cookies.iceRefreshJWT};`);

    let setCookies = tokenResponse.headers['set-cookie'] || [];

    for(let i = 0; i < setCookies.length; i++ ){
      res.append('set-cookie',setCookies[i]);
      // res.append('set-cookie',setCookies[i].replace(/Domain=[\w\d\.\-]+;/i, ''));
    }

    let meResponse = await req.ice2.request({
      url : '/session/me',
      method : 'get',
      withCredentials:true,
      headers : header,
    });

    let {
      data : userData,
    } = meResponse


    req.stash.user = {
      signed : !!userData.userId,
      ...pick(userData, ['userId', 'name', 'status', 'role']),
    };
  } catch(e){
    console.log('ERROR: sessionFilter ',e);
    throw e;
  }
}



export async function studio(req, res) {
  let result = await sessionFilter(req,res);

  console.log('stash>>', req.stash);
  if( req.stash.user && req.stash.user.signed ) {
    return true;
  }


  throw Errors('SESSION.NOT_PERMISSION');
}


export async function dashboard(req, res) {
  // token 처리

  let result = await sessionFilter(req,res);



  if( req.stash.user && req.stash.user.signed ) {
    return true;
  } else {
    throw Errors('SESSION.NOT_PERMISSION');
  }
}


export async function login(req, res) {
  // token 처리

  let result = await sessionFilter(req,res);


  return true;
}
