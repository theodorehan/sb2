import path from 'path';
import Promise from 'bluebird';
import JSONFile from 'jsonfile';

import {
  SITE_STORE_DIR,
} from '../App';

import {
  Node,
  NODE_PROJECT_EXCLUDES,
} from './FileNode';

import SiteModel from '../Model/Site';


export function SITE_FS_ACCESS_GET_ROUTES_TREE(_siteID) {
  // let siteModel =

  return new Promise((resolve, reject)=>{
    SiteModel.findOne(_siteID)
      .then(
      (siteObj)=>{
        resolve(siteObj);
      },
      (err) => {
        reject(err);
      });
  }).then((siteObj)=>
    new Promise((resolve, reject)=>{

      let asID = siteObj.assignedStoreID;
      let siteRootDir = GET_SITE_ROOT_DIR(asID);

      let rootNode = Node.convertToNode(siteRootDir);

      rootNode.loadChildrenRecursiveAll({
        exclude :NODE_PROJECT_EXCLUDES,
      }).then(()=>{

        let result = rootNode.findOne('routes.json');
        JSONFile.readFile(result.systemPath, {}, (_err, _data)=>{
          if( _err ){
            reject(_err)
          } else {
            resolve(_data);
          }
        });

      });
    })
  );
}


export function GET_SITE_ROOT_DIR(_assignedStoreID) {
  return path.join(SITE_STORE_DIR, '/' , _assignedStoreID, '/');
}

export function GET_SITE_INFO(siteID){
  return new Promise((resolve, reject)=>{
    SiteModel.findOne(siteID)
      .then((siteModel)=>{
        resolve(siteModel.toJSON());
      }, (err)=>{
        reject(err);
      })
  });
}


export function GET_SITE_INFO_MODEL(siteID) {
  return new Promise((resolve, reject)=>{
    SiteModel.findOne(siteID)
      .then((siteModel)=>{
        resolve(siteModel);
      }, (err)=>{
        reject(err);
      })
  });
}
