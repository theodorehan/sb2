import color from 'cli-color';
import SiteInstanceClient from './SiteInstanceClient';
import uuid from 'uuid';

export default (app, io) => {
  app.siteInstances = new Map();

  console.log(color.green(`Site connect on`));
  io.on('connect', (sock) => {
    let id = uuid.v4();

    console.log(color.green(`Connected site`));

    sock.on('reveal', (data) => {
      let siteInstanceClient = new SiteInstanceClient(data, id);

      siteInstanceClient.verification()
        .then(() => {
          app.siteInstances.set(id,siteInstanceClient);

          siteInstanceClient.operate(sock);
        }, () => {
          sock.close();
        })
      console.log(data);
    });


  });


  // 연결이 끊긴 사이트 청소 5초에 한번씩
  setInterval(() => {
    app.siteInstances.forEach((v, k) => {
      if( v.isDead ){
        app.siteInstances.delete(k);
      }
    })
  }, 5000);
}
