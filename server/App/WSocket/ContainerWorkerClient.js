import Promise from 'bluebird';
import SocketIOStream from 'socket.io-stream';
import fs from 'fs';
import ipAddress from 'ip-address';
import uuid from 'uuid';
import SiteModel from '../Model/Site';
import UserWorkspaceManger from '../Helpers/Service/UserWorkspaceManager';
import EventEmitter from 'events';

import {
  Logger,
} from '../App';

export default class ContainerClient {
  ex;
  sock;
  sessionID; // Container Worker Session ID
  sessionData;
  ipv4;
  ipv6;

  constructor(expressInstance, workerSessionData, sock, ipv4) {
    this.event = new EventEmitter();

    this.ex = expressInstance;
    this.sessionID = workerSessionData.sessionID;
    this.sessionData = workerSessionData;

    this.sock = sock;

    let remoteAddress = sock.handshake.address;

    console.log(sock.conn,remoteAddress);

    this.ipv6 = remoteAddress; // ::ffff:127.0.0.1

    let address = new ipAddress.Address6(this.ipv6);

    this.ipv4 = ipv4;

    Logger.get().worker.info('Worker connected %s', this.ipv4);

    sock.on('disconnect', () => {
     this.disconnected();
    });



    sock.on('hmr-ready', ()=>{
      this.event.emit('readyToHMRFromWorkerClient');
    });

    sock.on('service-ready', ()=>{
      this.deployState = 'deployed';
      this.event.emit('readyToService');
    });

    sock.on('state', (state)=>{
      this.event.emit('state', state);
    });


    this.deployState = 'undeploy';
  }


  getSessionData(_cb) {
    return new Promise((resolve, reject) => {
      this.ex.get('WorkerManager').read(this.sessionID, (_err, _data) => {
        if (typeof _cb === 'function') _cb(_err, _data);

        if (_err) {
          reject(_err);
        } else {
          resolve(_data);
        }
      });
    })
  }

  deploy(siteManager) {
    Logger.get().worker.info('worker container deploy start.');
    this.deployState = 'deploying';

    siteManager.siteArchiving()
      .then(
        (_result) => {
          console.log('사이트 압축 결과', _result)


          var stream = SocketIOStream.createStream();
          var filename = _result;

          SocketIOStream(this.sock).emit('siteTarball', stream, {name: filename});

          fs.createReadStream(filename).pipe(stream);
        },
        (_err) => {
          console.log('사이트압축 에러', _err);
        }
      );
  }

  applySiteFile(relativePath, systemPath, {withRestart} = {}){
    var stream = SocketIOStream.createStream();
    let streamID = uuid.v4();

    return new Promise((resolve, reject)=>{

      SocketIOStream(this.sock).emit('site-file', stream, {
        relativePath,
        streamID,

        withRestart,
      });

      fs.createReadStream(systemPath).pipe(stream);

      this.sock.once(`res-${streamID}`, (result)=>{
        if( result.result === 'success' ){
          resolve();
        } else {
          reject();
        }
      });
    })
  }

  getRenderingProxyURL(){
    if( this.sessionData.startingConfig.docker_container_pool_type === 'on_port' ){
      return 'http://' +
        this.sessionData.startingConfig.docker_container_port_host +
        ':' +
        this.sessionData.accessPort;
    } else {
      return 'http://' +
        this.ipv4 +
        ':' +
        this.sessionData.accessPort;
    }
  }


  areYouBusy(){
    this.sock.emit('areYouBusy?');
  }


  disconnected(){
    console.log("Work Disconnect");

    this.ex.get("WorkerSocketsManager").removeClient(this.sessionID);
    this.ex.get('WorkerManager').delete(this.sessionID)
      .then(()=>{
        this.ex.get('docker').deleteContainer(this.sessionID)
          .then(()=>{
            this.close();

            this.event.emit('workerDisconnected');
          }, (err) => {
            console.error(err);
          })
      });
  }

  close(){
    this.sock.disconnect();
    this.disconnected();
  }

  shutdown(){
    let workerSessionID = this.sessionID;
    let workerContainerID = this.sessionData.containerID;

    this.ex.get('WorkerManager').delete(workerSessionID)
      .then(()=>{
        this.ex.get('docker').deleteContainer(workerContainerID)
          .then(()=>{
            this.close();
          }, (err) => {
            console.error(err);
          })
      }, (err) => {
        console.error(err);
      })
  }


  jsonInfo(){
    return {
      WORKER : "WORKER",
      sessionID : this.sessionID,
      ip : this.ipv4,
      accessPort : this.sessionData.accessPort,
    };
  }
}
