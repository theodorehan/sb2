import path from 'path';
import Promise from 'bluebird';
import uuid from 'uuid';

import Errors from '../Helpers/Error';


import {
  ASSIGNED_STORE_DIR_PATH,
  Logger,
  Director,
} from '../../App';

export default class StudioClient {
  ex;
  sock;
  sessionID; // Studio Session ID
  workerClient = null;
  sessionData;

  siteManager;
  CertifiedDataSet;

  constructor(expressInstance, sessionID, sock, CertifiecDataSet, siteManager) {
    this.ex = expressInstance;
    this.sessionID = sessionID;
    this.sock = sock;
    this.CertifiedDataSet = CertifiecDataSet;
    this.siteManager = siteManager;


    console.log('siteManager',CertifiecDataSet);


    this.createWorkerSession()

    sock.on('disconnect', () => {
      this.disconnected();
    });

    sock.on('publish-node', (state) => {
      this.publishNode(state);
    });
  }

  publishNode(state){
    console.log('publish', state);
  }


  // with Bind
  createWorkerSession() {

    this.readyToService();
    return;
    return new Promise((_resolve, _reject) => {

      let ownerID = this.CertifiedDataSet.siteModel.owner;
      let siteID = this.CertifiedDataSet.siteModel.id;

      let workerSessionID;

      if( Director.get().WORKER_KEEP_ALIVE ){
        // KEEP_ALIVE 모드 일때는 추론이 가능한 세션아이디를 사용 한다.
        workerSessionID = `${ownerID}.${siteID}`;
      } else {
        workerSessionID = uuid.v4();
      }

      if( process.env.PREPARED_WORKER_ID_FOR_TEST ){
        workerSessionID = process.env.PREPARED_WORKER_ID_FOR_TEST;
      }


      console.log(':createWorkerSession',workerSessionID)

      this.ex.get('WorkerManager').read(workerSessionID)
        .then((workerSessionData)=>{

          console.log('workerSessionData>', workerSessionData);

          this.ex.get('StudioSession').read(this.sessionID, (_err, _data) => {
            if (_err) {
              _reject(_err);
            } else {
              let freshSessionData = Object.assign({}, _data, {
                workerInstanceSessionID: workerSessionID,
              });

              this.ex.get('StudioSession').write(this.sessionID, freshSessionData, (_err) => {
                if (_err) {
                  _reject(_err);
                } else {

                  let workerClient = this.ex.get('WorkerSocketsManager').findClient(workerSessionID);
                  if( workerClient ){
                    this.workerConnected(workerClient);

                    _resolve(workerSessionID);
                  } else {
                    _reject(Errors('STUDIO.WORKER.CLIENT_NOT_FOUIND'));
                  }
                }
              });
            }
          });
        },()=>{
          this.ex.get("WorkerManager").createSession(this.sessionID, workerSessionID)
            .then((workerSessionData) => {

              Logger.get().info('Worker container create', workerSessionData);

              let containerID = workerSessionData.containerID;
              /**
               * Docker Container 생성및 시작 로직
               */
              return new Promise((_resolve, _reject) => {

                this.ex.get('docker').createWorkerContainer(workerSessionData)
                  .then(()=>{



                    this.ex.get('StudioSession').read(this.sessionID, (_err, _data) => {
                      if (_err) {
                        _reject(_err);
                      } else {
                        let freshSessionData = Object.assign({}, _data, {
                          workerInstanceSessionID: workerSessionData.sessionID,
                        });

                        this.ex.get('StudioSession').write(this.sessionID, freshSessionData, (_err) => {
                          if (_err) {
                            _reject(_err);
                          } else {
                            _resolve(workerSessionData.sessionID);
                          }
                        });
                      }
                    });
                  }, (err) => {
                      console.error('예외처리 요망', err);

                      this.ex.get('docker');

                    /**
                     * info: Worker container create sessionID=system.ytn, containerID=system.ytn, workerConnected=false, workerStatus=none, studioSessionID=fb2b06a7-2073-4d6a-ac01-f985f886907f, docker_container_pool_type=on_port, docker_container_port_host=localhost, accessPort=43946
                     info: Created container system.ytn sessionID=system.ytn, containerID=system.ytn, workerConnected=false, workerStatus=none, studioSessionID=fb2b06a7-2073-4d6a-ac01-f985f886907f, docker_container_pool_type=on_port, docker_container_port_host=localhost, accessPort=43946
                     error: Container[system.ytn] finished with error >> Error: (HTTP code 409) unexpected - Conflict. The name "/system.ytn" is already in use by container ad677c7099f68f37d1d889572ee5200b3e0b1ff76552dc33f4cadf544eac1c6f. You have to remove (or rename) that container to be able to reuse that name.
                     (node:26182) UnhandledPromiseRejectionWarning: Error: Finish Container.
                     at /home/ygoon/ICE2ServiceBuilder/server/App/DockerSupport/index.js:148:16
                     at handler (/home/ygoon/ICE2ServiceBuilder/node_modules/dockerode/lib/docker.js:1383:21)
                     at /home/ygoon/ICE2ServiceBuilder/node_modules/dockerode/lib/docker.js:73:23
                     at /home/ygoon/ICE2ServiceBuilder/node_modules/docker-modem/lib/modem.js:262:7
                     at getCause (/home/ygoon/ICE2ServiceBuilder/node_modules/docker-modem/lib/modem.js:284:7)
                     at Modem.buildPayload (/home/ygoon/ICE2ServiceBuilder/node_modules/docker-modem/lib/modem.js:253:5)
                     at IncomingMessage.<anonymous> (/home/ygoon/ICE2ServiceBuilder/node_modules/docker-modem/lib/modem.js:229:14)
                     at IncomingMessage.emit (events.js:164:20)
                     at endReadableNT (_stream_readable.js:1062:12)
                     at process._tickCallback (internal/process/next_tick.js:152:19)
                     (node:26182) UnhandledPromiseRejectionWarning: Unhandled promise rejection. This error originated either by throwing inside of an async function without a catch block, or by rejecting a promise which was not handled with .catch(). (rejection id: 1)
                     (node:26182) [DEP0018] DeprecationWarning: Unhandled promise rejections are deprecated. In the future, promise rejections that are not handled will terminate the Node.js process with a non-zero exit code.



                     [root@backoffice02 ~]# docker ps -a
                     CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS                  PORTS               NAMES
                     a047fb3d0207        worker              "yarn run start"         3 days ago          Dead                                        system.ytn-mobile
                     ad677c7099f6        worker              "yarn run start"         3 days ago          Dead                                        system.ytn
                     6f7c45094849        71367566d3ce        "/bin/sh -c 'yarn ins"   3 days ago          Exited (1) 3 days ago                       prickly_rosalind
                     bd8abe0c1a85        71367566d3ce        "/bin/sh -c 'yarn ins"   3 days ago          Exited (1) 3 days ago                       elegant_keller
                     1b8b5e3b0ba1        f62c652d2878        "/bin/sh -c 'yarn ins"   3 days ago          Exited (1) 3 days ago                       tender_wozniak
                     0d4eb28d9eda        f62c652d2878        "/bin/sh -c 'yarn ins"   3 days ago          Exited (1) 3 days ago                       infallible_almeida
                     52de1ff8ddd3        f62c652d2878        "/bin/sh -c 'yarn ins"   3 days ago          Exited (1) 3 days ago                       elegant_heisenberg
                     */
                  });
              });
            })
            .then(
              (workerSessionData) => {

                Logger.get().info('Worker manager : Created worker session [%s] of Studio [%s]', workerSessionData.sessionID, this.sessionID);

                _resolve(workerSessionData.sessionID);
              },
              (_err) => {

                Logger.get().error('Worker manager : fail create worker sessionID', _err);

                _reject(Errors('STUDIO.WORKER.FAIL_CREATE_SESSION'));
              }
            )
        })


    });
  }


  // worker 클라이언트 연결됨
  // workerClientGreeting(_workerClient){
  //   console.log('Greeting to me from Worker ' + _workerClient.sessionID);
  //   this.workerClient = _workerClient;
  //
  //   this.sock.emit('notice', {
  //     message: 'Greeting to me from Worker ' + _workerClient.sessionID,
  //     date: Date.now(),
  //     level: 0,
  //   });
  //
  //   _workerClient.deploy();
  // }

  getSessionData(_cb) {
    return new Promise((resolve, reject) => {
      this.ex.get('StudioSession').read(this.sessionID, (_err, _data) => {
        if (typeof _cb === 'function') _cb(_err, _data);

        if (_err) {
          reject(_err);
        } else {
          resolve(_data);
        }
      });
    })
  }

  getSessionDataWithUserData(){
    return new Promise((resolve, reject) => {
      this.getSessionData()
        .then((data)=>{
          this.ex.get('UserSession').read(data.userSession)
            .then((userData)=>{
              resolve({
                sessionData : data,
                userData,
              });
            })
        })
    });
  }

  getSiteDataPath(_cb) {
    return new Promise((_resolve, _reject) => {
      this.getSessionData((_err, _data) => {
        if (_err) {
          if (typeof _cb === 'function') {
            _cb(_err);
          }

          _reject(_err);
        } else {
          let dataPath = path.join(ASSIGNED_STORE_DIR_PATH, '/Site/', _data.siteID + '/');

          if (typeof _cb === 'function') {
            _cb(null, dataPath);
          }

          _resolve(dataPath);
        }
      })
    })
  }

  readyToHMRFromWorkerClient(){
    this.sock.emit('ready-to-hmr');
  }

  readyToService(){
    this.sock.emit('ready-to-service')
  }


  workerConnected(workerClient){
    this.workerClient = workerClient;
    // this.workerClient.handshakeByStudio(this);




    this.workerClient.event.on('readyToHMRFromWorkerClient', ()=>{
      this.readyToHMRFromWorkerClient();
    });
    this.workerClient.event.on('readyToService', ()=>{
      this.readyToService();
    });
    this.workerClient.event.on('workerDisconnected', ()=>{
      this.workerDisconnected();
    });

    this.workerClient.event.on('state', (state)=>this.sock.emit('state',state));


    if( this.workerClient.deployState !== 'deployed' && this.workerClient.deployState !== 'deploying' ){
      this.workerClient.deploy(this.siteManager);
    } else if( this.workerClient.deployState === 'deploying' ){
      console.log('deploying')
    } else {
      this.workerClient.areYouBusy();
    }
  }

  workerDisconnected(){
    console.log('Worker Disconnected');
    this.workerClient = null;


    this.sock.emit('render-suspend',{

    });
  }


  disconnected(){
    this.ex.get("StudioSocketsManager").removeClient(this.sessionID);


    // KEEP_ALIVE 모드에서는 워커를 종료 하지 않는다.
    if( !Director.get().WORKER_KEEP_ALIVE && this.workerClient ){

      this.workerClient.shutdown();
    }
  }

  jsonInfo(){
    return {
      STUDIO : "STUDIO",
      sessionID : this.sessionID,
      ip : this.ipv4,
      workerSessionID : this.workerClient && this.workerClient.sessionID,
    };
  }
}
