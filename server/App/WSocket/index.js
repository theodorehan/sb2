import Errors from '../Helpers/Error';
import StudioClient from './StudioClient';
import ContainerWorkerClient from './ContainerWorkerClient';
import UserWorkspaceManger from '../Helpers/Service/UserWorkspaceManager';
import * as DEFINE_STRUCTURE_TYPES from '../foundationData/service/defineStructureTypes'

import {
  createSiteManager,
  ACCESSIBLE_LEVEL_STUDIO,
} from '../Helpers/Service/SiteManager';

import {
  Logger,
} from '../App';
export default function (expressInstance) {
  let StudioSocketsManager = new SocketPoolManager();
  let WorkerSocketsManager = new SocketPoolManager();

  expressInstance.set('StudioSocketsManager', StudioSocketsManager);
  expressInstance.set('WorkerSocketsManager', WorkerSocketsManager);

  expressInstance.get('studioIO').on('connection', function (socket) {
    expressInstance.get('ttyLogger').info("- Studio connected.");

    socket.on('identify', function (data) {
      let {iam} = data;

      if (iam === 'STUDIO') {
        let {studioID, userSessionID} = data;
        expressInstance.get('ttyLogger').info(`-- Identify Studio ${studioID}:${userSessionID}.`);

        StudioCertification(expressInstance, userSessionID, studioID, function (err, resultDatas) {
          if (err) {
            socket.emit('certification', {
              'result': 'denied',
              'err': err,
            });


            // socket.close();
          } else {
            let { CertifiedDataSet, siteManager } = resultDatas;

            if (CertifiedDataSet) {
              socket.emit('certification', {
                'result': 'accept',
              });


              StudioSocketsManager.registerClient(studioID, new StudioClient(expressInstance, studioID, socket, CertifiedDataSet, siteManager));
            }
          }
        });
      }
    });
  });




  expressInstance.get('workerIO').on('connection', function (socket) {
    expressInstance.get('ttyLogger').info("- Worker connected.");

    socket.on('identify', function (data) {
      let {iam} = data;

      if (iam === 'CONTAINER_WORKER') {
        let {workerID, ipv4, ipv6} = data;
        console.log('IPV4',ipv4);

        expressInstance.get('ttyLogger').info(`-- Identify Container Worker ${workerID}.`);


        WorkerCertification(expressInstance, workerID, function (err, _workerSessionData) {
          if (err) {
            socket.emit('certification', {
              'result': 'denied',
              'err': err,
            });

          } else {
            if (_workerSessionData) {
              socket.emit('certification', {
                'result': 'accept',
              });


              let studioID = _workerSessionData.studioSessionID;
              console.log('studio ID',studioID)
              let studioClient = StudioSocketsManager.findClient(studioID);
              if( studioClient ){
                let workerClient = new ContainerWorkerClient(expressInstance, _workerSessionData, socket, ipv4, ipv6);

                WorkerSocketsManager.registerClient(workerID, workerClient);

                studioClient.workerConnected(workerClient);
              } else {
                Logger.get().worker.error('Not found studio');
                socket.disconnect();
              }

            }
          }
        });
      }
    });
  });
}


function StudioCertification(ex, userSessionID, studioID, _cb) {
  let CertifiedDataSet = {
    userModel : null,
    siteModel : null,
    storeDefine : null,
    storeStructure : null,
  };

  ex.get('UserSession').read(userSessionID, (err, data) => {

    console.log('StudioCertification : read user session')
    if (err) {
      _cb(err);
    } else {
      CertifiedDataSet.userModel = data;
      CertifiedDataSet.userSessionData = data;
      console.log('StudioCertification : readed user session')
      let {studioSessionID} = data;

      if (studioSessionID === studioID) {
        console.log('StudioCertification : passed studio id')
        ex.get('StudioSession').read(studioSessionID)
          .then((studioSessionData)=>{
            let {siteID} = studioSessionData;
            console.log('StudioCertification : readed studio session')
            let userWorkspaceManager = new UserWorkspaceManger(ex, CertifiedDataSet.userModel);
            userWorkspaceManager.readSiteInfo(siteID)
              .then((siteInfo)=>{
                console.log('StudioCertification : readed site info')
                CertifiedDataSet.siteModel = siteInfo;
                CertifiedDataSet.storeDefine = siteInfo;
                CertifiedDataSet.storeStructure = DEFINE_STRUCTURE_TYPES[siteInfo.SERVICE_STRUCTURE_TYPE];
                _cb(null, {
                  // siteManager,
                  CertifiedDataSet,
                });

                // userWorkspaceManager.getSiteManager(CertifiedDataSet, ACCESSIBLE_LEVEL_STUDIO)
                //   .then((siteManager)=>{
                //     console.log('StudioCertification : got siteManager')
                //     _cb(null, {
                //       siteManager,
                //       CertifiedDataSet,
                //     });
                //   });
              });
          });
      } else {
        _cb(Errors('STUDIO.SESSION.NOT_FOUND'));
      }
    }
  });
}

function WorkerCertification(ex, workerSessionID, _cb){
  ex.get('WorkerManager').read(workerSessionID, (err, data) => {

    if (err) {
      _cb(err);
    } else {
      _cb(null, data);
    }
  });
}


class SocketPoolManager {
  pool;

  constructor() {
    this.pool = {};
  }

  registerClient(sessionID, Client) {
    this.pool[sessionID] = Client;

    // console.log('register ',sessionID,this.pool);
  }

  removeClient(sessionID) {
    delete this.pool[sessionID];

    // console.log('remove ',sessionID,this.pool);
  }

  findClient(sessionID){
    return this.pool[sessionID];
  }


  monitorJSON(){
    return Object.keys(this.pool).map( (key)=> this.pool[key].jsonInfo() );
  }
}




