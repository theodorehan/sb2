import Promise from 'bluebird';

export default class siteInstanceClient {

  constructor(reveal, id){
    this.siteId = reveal.siteId;
    this.owner = reveal.owner;
    this.port = reveal.port;
    this.port_ssl = reveal.port_ssl;
    this.secret = reveal.secret;
    this.ip = reveal.ip;
    this.sock = null;

    this.id = id;

    this.isDead = false;
  }

  verification(){
    return new Promise((resolve, reject) => {
      // site Id 와 owner 를 이용하여 site 구조체를 찾고
      // secret을 대조하여 동일한 사이트인지 비교한다.

      resolve();
    });
  }

  // 시작
  operate(socket){
    this.sock = socket;

    this.sock.on('disconnect', ()=>{
      this.isDead = true;
    });
  }
}
