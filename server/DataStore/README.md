# Temporarily Data Store

```
DataStore/
  Site/
    [Assigned Store Site ID]/
      Pages/
        *.html
        
      containers/
        *.vue
        
      components/ 
        *.vue
        
      statics/
        image/
        font/
        svg/
        css/
      package.json
      webpack.config.json
    
  Provider/
    SiteTemplate/
      [Template ID]/
        meta.json
        
    PageTemplate/
      [Template ID]/
        meta.json
        
    Component/
      [Component ID]/
        meta.json
        
    PurchasingComponent/
      [Component ID]/
        meta.json
        
    Stock/
      images/
      videos/
```


Provider 에 존재하는 파일들은 편집이 불가능 하고

사용자가 자신의 사이트에 사용하기를 선택하였을 때 사용자의 사이트에 데이터를 복제한다.
