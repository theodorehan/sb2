const argv = require('./argv');

var port = parseInt(argv.port || process.env.PORT || '8080', 10);
var sslPort = parseInt(argv.port || process.env.PORT || '8443', 10);

module.exports = {sslPort: sslPort, port: port};
