/* eslint consistent-return:0 */
const express = require('express');
import http from 'http';
import https from 'https'
const logger = require('./logger');
import path from 'path';
import socketio from 'socket.io';
import fs from 'fs';
import JSONFile from 'jsonfile';
import Proxy from 'express-http-proxy';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import UUIDValidate from 'uuid-validate';
import parseUrl from 'url-parse';
import template from 'lodash/template';


import API from './App/API';
import STDLogger from './logger';
import ContinuationLib from './App/ContinuationLib';
import Config from './App/Config';
import DBConn from './App/DBConn';
import AssignedStore from './App/assignedStore';
import WSocket from './App/WSocket';
import StudioSession from './App/StudioSession';
import UserSession from './App/UserSession';
import WorkerManager from './App/WorkerManager';

import FSLogger from './App/Logger';
import StudioRenderProxy from './App/StudioRenderProxy';
import Monitor from './App/Monitor';
import DockerSupport from './App/DockerSupport';

const AppDependencies = {
  API,
  STDLogger,
  ContinuationLib,
  Config,
  DBConn,
  AssignedStore,
  WSocket,
  StudioSession,
  UserSession,
  WorkerManager,
  FSLogger,
  StudioRenderProxy,
  Monitor,
  DockerSupport,
};

import RealApp from './App';
import ActionTracer from './App/Helpers/ActionTracer';
import {
  Director,
  Logger,
} from './App';

const argv = require('./argv');
// const port = require('./port');
// const {port, sslPort} = require('./port');
const setup = require('./middlewares/frontendMiddleware');


const isDev = process.env.NODE_ENV !== 'production';
const ngrok = (isDev && process.env.ENABLE_TUNNEL) || argv.tunnel ? require('ngrok') : false;
const resolve = require('path').resolve;
const app = express();


var privateKey  = fs.readFileSync( __dirname + '/ssl/key.pem', 'utf8');
var certificate = fs.readFileSync( __dirname + '/ssl/cert.pem', 'utf8');
/**************
 * Start Application Logic
 */

var credentials = {key: privateKey, cert: certificate};
// Singletones
new Director(app,AppDependencies);
new Logger(app);

const server = http.Server(app);
const sslServer = https.Server(credentials, app);

app.set('studioIO', socketio(server));
// app.set('studioIO', socketio(sslServer));

let workerIO_PORT = Director.get().BUILDER_WORKER_PORT;
app.set('workerIO', socketio(workerIO_PORT));

let directSiteManage_PORT = Director.get().BUILDER_DIRECT_SITE_MANAGE_PORT;
app.siteManagingIO = socketio(directSiteManage_PORT);




let port = Director.get().BUILDER_STUDIO_PORT;
let sslPort = Director.get().BUILDER_STUDIO_SSL_PORT;


// hmr 블럭킹 리디렉션 오류 방지
app.use('/__webpack_hmr', (req,res) => res.status(404).send('Disabled'));


/**
 * Tracer Spawn & Inject Request Object
 * & Rendering Proxy
 */
app.use(function (req, res, next) {
  let referer = req.header('Referer');
  let ip = req.ip;
  let ips = req.ips;
  let method = req.method;
  let xhr = req.xhr;
  let time = new Date();
  let url = req.originalUrl;
  let hostname = req.hostname;


  let {
    __s__,
    __not_s__,
  } = req.query;
  // console.log(req);

  // stash 공간 마련
  req.stash = {};

  if(__not_s__){

    req.tracer = new ActionTracer(ip, method, url, xhr, time, hostname);
    return next();
  }

  // console.log('================22222============================',__s__)


  // 스튜디오 세션 파라미터가 입력되지 않을 경우
  if( !__s__ ){

    // 리퍼러 체크
    if( referer ){
      let parsedReferer = parseUrl(referer,true);

      // 리퍼러에 세션 파라미터가 있을 경우
      if( parsedReferer.query.__s__ ){
        // console.log(req.header);
        if( req.url.indexOf('?') > 1 ){
          res.redirect(req.url + `&__s__=${parsedReferer.query.__s__}`);
        } else {
          res.redirect(req.url + `?__s__=${parsedReferer.query.__s__}`);
        }
        return;
      }
    }



    req.tracer = new ActionTracer(ip, method, url, xhr, time, hostname);
    next();
  } else {

    // console.log('============================================',__s__)
    let studioClient = app.get("StudioSocketsManager").findClient(__s__);

    // console.log('studioClient',studioClient)
    if( studioClient ){

      let {CertifiedDataSet} = studioClient;

      // console.log('hhh', CertifiedDataSet);

      if( false && /^\/(image|svg|font|video|music|script|style)(s?)\//.test(req.url) ){
        let siteID = CertifiedDataSet.siteModel.id;
        let siteOwner = CertifiedDataSet.siteModel.owner;
        let userSessionID = CertifiedDataSet.userModel.sessionID

        return res.redirect(`http://${Director.get().EXTERNAL_URL}/api/user/${userSessionID}/site/${siteID}/${siteOwner}/store/resource/get${req.url}${req.url.indexOf('?') > 1? '&__not_s__=1':'?__not_s__=1'}`);
      }

      // console.log('CertifiedDataSet',CertifiedDataSet.siteModel.port)

      if( CertifiedDataSet.siteModel ){
        let proxyURL =  'http://' +
            CertifiedDataSet.siteModel.ip +
            ':' +
            CertifiedDataSet.siteModel.port;

        Proxy(
            proxyURL
        )(req, res, next);
        return;
      }

      if( studioClient.workerClient ){
        let proxyURL = studioClient.workerClient.getRenderingProxyURL();
        console.log('Proxy ' + proxyURL);

        Proxy(
          proxyURL
        )(req, res, next);
      } else {

        Proxy(
          'localhost:9090'
        )(req, res, next);
      }
    } else {

      console.log('Redirect to ' + Director.get().EXTERNAL_URL);
      res.redirect(`//${Director.get().EXTERNAL_URL}`);
    }
  }
})

app.use(cookieParser());
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended: true, limit: '50mb'}));

// parse application/json
app.use(bodyParser.json({limit: '50mb'}));

app.use('/public/', express.static(path.join(process.cwd(), 'public')));
app.use('/dataStore/', express.static(path.join(process.cwd(), '/server/DataStore/Site/')));

// app.use('/dataStore/:storeID', function (req, _res, next) {
//   let {userSessionID, siteID} = req.query;
//   let {storeID} = req.params;
//
//   JSONFile.readFile(path.join(app.get('sitesInfoDirPath'), `/${siteID}.json`), {}, function (_err, siteInfo) {
//
//     if (_err) {
//       _res.status(404).send("Not found");
//     } else {
//
//       if (siteInfo.assignedStoreID === storeID) {
//         app.get('UserSession').read(userSessionID, function (_err, _data) {
//           if (_err) {
//             _res.status(404).send("user session error 3");
//           } else {
//             if (_data.seq === siteInfo.masterUserSeq) {
//               next();
//             } else {
//               _res.status(404).send("user session error 2");
//             }
//           }
//         });
//       } else {
//         _res.status(404).send("user session error 1");
//       }
//     }
//   });
// });

app.set('ttyLogger', logger);
// app.use('/public', path.join(__dirname, '../public/'))
// If you need a backend, e.g. an API, add your custom backend-specific middleware here
// app.use('/api', myApi);


RealApp(app, AppDependencies, () => {

  let SERVER_CONFIG = app.get('config');

  if (!SERVER_CONFIG.builder_domain_production) {
    console.error("\nERROR : Add builder_domain_production field at config/server.ini\n");

    process.exit(1);
  }

  if (!SERVER_CONFIG.builder_domain_development) {
    console.error("\nERROR : Add builder_domain_development field at config/server.ini\n");

    process.exit(1);
  }

  const Builder_domain =
    process.env.NODE_ENV === 'production' ?
      SERVER_CONFIG.builder_domain_production :
      SERVER_CONFIG.builder_domain_development;



  // **** 개발 방식 변경
  // 웹팩 미들웨어와 서버 분리
  // In production we need to pass these values in instead of relying on webpack
  // setup(app, {
  //   outputPath: resolve(process.cwd(), 'build'),
  //   publicPath: '/',
  // });
  // ****
  // ▽ ▼ ▽ ▼ ▽ ▼ ▽ ▼ ▽ ▼ ▽ ▼
  // **** 개발 방식 변경
  // 웹팩 미들웨어와 서버 분리
  // 프론트 빌드는 따로 하고 빌드된 결과물을 서비스한다.
  app.use(express.static(path.join(process.cwd(), 'build')));
  app.get('*', (req, res) => {

    fs.readFile(path.join(process.cwd(), 'build', 'index.html'), (err, file) => {




      if (err) {
        res.sendStatus(404);
      } else {
        let compiled = template(file.toString());

        let templated = compiled({
          stashObject: JSON.stringify(req.stash || {}),
        });

        res.send(templated);
      }
    });
  });
  // ****


  // get the intended host and port number, use localhost and port 3000 if not provided
  const customHost = argv.host || process.env.HOST;
  const host = customHost || null; // Let http.Server use its default IPv6/4 host
  const prettyHost = customHost || 'localhost';

  // Start your app.
  server.listen(port, host, (err) => {
    if (err) {
      return logger.error(err.message);
    }

    // Connect to ngrok in dev mode
    if (ngrok) {
      ngrok.connect(port, (innerErr, url) => {
        if (innerErr) {
          return logger.error(innerErr);
        }

        logger.appStarted(port, prettyHost, workerIO_PORT, directSiteManage_PORT, url);
      });
    } else {
      logger.appStarted(port, prettyHost, workerIO_PORT, directSiteManage_PORT);
    }
  });

  // sslServer.listen(sslPort, host, (err) => {
  //   if (err) {
  //     return logger.error(err.message);
  //   }
  //
  //   // Connect to ngrok in dev mode
  //   if (ngrok) {
  //     ngrok.connect(sslPort, (innerErr, url) => {
  //       if (innerErr) {
  //         return logger.error(innerErr);
  //       }
  //
  //       logger.appStartedSSL(sslPort, prettyHost, workerIO_PORT, url);
  //     });
  //   } else {
  //     logger.appStartedSSL(sslPort, prettyHost, workerIO_PORT);
  //   }
  // });

})


