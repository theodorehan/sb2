/* eslint-disable no-console */

const chalk = require('chalk');
const ip = require('ip');

const divider = chalk.gray('\n-----------------------------------');

/**
 * Logger middleware, you can customize it to make messages more personal
 */
const logger = {

  // Called whenever there's an error on the server we want to print
  error: (err) => {
    console.error(`ERROR\t: ${chalk.red(err)}`);
  },

  info: (infoMsg) => {
    console.info(`INFO\t: ${chalk.blue(infoMsg)}`);
  },

  success: (infoMsg) => {
    console.info(`SUCCESS\t: ${chalk.green(infoMsg)}`);
  },

  // Called when express.js app starts on given port w/o errors
  appStarted: (port, host, worker_sock_port, directSiteManage_PORT, tunnelStarted) => {
    console.log(`Server started ! ${chalk.green('✓')}`);

    // If the tunnel started, log that and the URL it's available at
    if (tunnelStarted) {
      console.log(`Tunnel initialised ${chalk.green('✓')}`);
    }

    console.log(`
${chalk.bold('Access URLs:')}${divider}
Localhost: ${chalk.magenta(`http://${host}:${port}`)}
Websocket: ${chalk.magenta(`ws://${host}:${port}`)} - Public for studio
Websocket: ${chalk.magenta(`ws://${host}:${worker_sock_port}`)} - Internal for worker
Websocket: ${chalk.magenta(`ws://${host}:${directSiteManage_PORT}`)} - External for site
      LAN: ${chalk.magenta(`http://${ip.address()}:${port}`) +
(tunnelStarted ? `\n    Proxy: ${chalk.magenta(tunnelStarted)}` : '')}${divider}
${chalk.blue(`Press ${chalk.italic('CTRL-C')} to stop`)}
    `);
  },


  appStartedSSL: (port, host, worker_sock_port, tunnelStarted) => {
    console.log(`SSL Server started ! ${chalk.green('✓')}`);

    // If the tunnel started, log that and the URL it's available at
    if (tunnelStarted) {
      console.log(`Tunnel initialised ${chalk.green('✓')}`);
    }

    console.log(`
${chalk.bold('Access URLs:')}${divider}
Localhost: ${chalk.magenta(`https://${host}:${port}`)} 
      LAN: ${chalk.magenta(`https://${ip.address()}:${port}`) +
    (tunnelStarted ? `\n    Proxy: ${chalk.magenta(tunnelStarted)}` : '')}${divider}
${chalk.blue(`Press ${chalk.italic('CTRL-C')} to stop`)}
    `);
  },
};

module.exports = logger;

//
// import winston from 'winston';
//
// export default function fileLogger (filename) {
//   var options = {
//     transports: [
//       new winston.transports.Console(),
//     ],
//   };
//   if( filename ){
//     options.transports.push(new winston.transports.File({
//       json: false,
//       filename: filename,
//     }));
//   }
//   var logger = new winston.Logger(options);
//   logger.setLevels(winston.config.syslog.levels);
//   //logger.exitOnError = true;
//   return logger;
// };
