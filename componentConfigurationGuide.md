# 커스텀 컴포넌트 등록 가이드

커스텀 컴포넌트의 등록 필드는 아래와 같이 있습니다.

## `레이아웃명`

컴포넌트의 키임과 동시에 이름을 입력합니다. 영문,숫자,-(하이픈) 만 허용

## `레이아웃 타입`

타입은 현재는 사용하지 않으므로 설정할 필요가 없습니다.

## `레이아웃 HTML`

실제 컴포넌트의 HTML 코드 가 입력되며 `doT` 템플릿 엔진을 사용하여 랜더링 합니다.

[doT 템플릿 엔진 가이드](http://olado.github.io/doT/index.html)

템플릿 가이드를 보시면 기본적인 조건문과 반복문 정도는 사용 할 수 있으니 참고하시면 될것 같습니다.

> 여기에 사용되는 코드는 Vue가 아닌 doT 템플릿 엔진임을 주의하여 주세요.


### 입력 예제 

```

<div class='youtubeLinker'>
  {{= it.html }}
</div>

```

데이터를 바인딩을 위해서는 `{{= it.*** }}` 를 이용합니다.

`=it.` 이후에 오는 코드는 아래의 `속성정의` 에서 정의된 속성키가 사용됩니다.

`it` 는 컴포넌트에 입력된속성을 담고 있는 객체로써 자바스크립트의 객체와 동일함으로 깊이는 자유롭게 접근이 가능합니다.


## `레이아웃 스타일`

컴포넌트의 스타일시트를 입력합니다.

> 다른 스타일과 충돌 가능성이 있으니 class 명을 지으실 때 해당 컴포넌트만의 접두사를 지정하여 사용하시길 권장 합니다.

## `속성정의`

컴포넌트를 설정 할 수 있는 속성들을 JSON 형태로 정의 합니다.

### 입력예제

```

{
	html: {
		label : 'HTML', 
		type: 'rich-text',
		default : '<div> HTML 입력 </div>',
	},
}

```

Root 오브젝트의 key 는 속성의 키로 사용되며 

해당 key 로 입력된 오브젝트는 속성의 정의에 관한 내용이 포함됩니다.

## `기기구분`

기기구분은 현재는 사용하지 않으므로 설정할 필요가 없습니다.

## `썸네일`

빌더에서 컴포넌트 목록에 노출될 썸네일을 업로드 합니다.





# 커스텀 컴포넌트 속성 정의 가이드


## 속성 정의 구조

```json

{
    "label" : "PROPERTY_NAME",
    
    "type": Type,  
    
    "hidden" : false, // Optional. (default : false) 에디터 노출되지 않는 속성이다.
    
    "value" : value, // Optional for Code Enumerate...
    
    "typeStructure" : Any, // Optional
    
    "help" : "This property is something.", // Optional
    
    "description": "설명", // 설명을 입력하면 에디터의 속성조절 패널에서 설명이 노출됩니다.
    
    "thumbnail" : "http://image.com/thumbnail.jpg" // Optional
}

```

## Types 
 
* String
* rich-text
* Code
* Array
* Object
* Number
* Boolean
* Color
* Date
* nodeItemId
* youtubeId
 

## String

Native Type : String

```json

{
    "label" : "PROPERTY_NAME",
    "type": "string"
}

```


## Rich-Text

Native Type : String

```json

{
    "label" : "PROPERTY_NAME",
    "type": "rich-text"
    "default" : "<div> HTML 입력 </div>",
}

```
 
## Code ( Enumerated )

Native Type : String, Number, Boolean,

```json

{
    "label" : "PROPERTY_NAME",
    "type": "code",
    
    "typeStructure" : [
      DefineObject or NativeTypeValue,
      ...
    ], 
    
    "typeStructure" : DefineObject,
}

```


Ex)
```json

{
    "label" : "level",
    "type": "code",
    
    "typeStructure" : [
      1,
      2,
      3,
      {
        "label" : "option 4",
        "type" : "number",
        "value" : 4
      },
      {
        "label" : "option 5",
        "type" : "string",
        "value" : "five"
      }
    ],  
}

```

##  Array

Native Type : Array

```json

{
    "label" : "PROPERTY_NAME",
    "type": "array", 
    
    "typeStructure" : {
      "elementTypes" : [defineObject,...]
    }
}

```

Ex)
```json

{
    "label" : "PROPERTY_NAME",
    "type": "array", 
    
    "typeStructure" : {
      "elementTypes" : [{
        "label" : "numbers",
        "type" : "number"
      },
      {
        "label" : "strings",
        "type" : "string"
      }]
    }
}

```


Ex)
```json

{
    "label" : "rangeNumbers",
    "type": "array", 
    
    "typeStructure" : {
      "elementTypes" : [{
        "label" : "numberRange",
        "type" : "range",
        "typeStructure" : {
          "max" : 99,
          "min" : 0,
          "step" : 2
        }
      }]
    }
}
```

## Object


Native Type : Object

```json
{
    "label" : "PROPERTY_NAME",
    "type": "object", 
    
    "typeStructure" : {
      "FIELD_A" : defineObject,
      "FIELD_B" : defineObject,
    }
}

```


Ex )
```json

{
    "label" : "actionSheet",
    "type": "object", 
    
    "typeStructure" : {
      "actionType" : {
        "label" : "Action Type",
        "required" : true,
        "type" : "enum",
        "typeStructure" : [
          "push",
          "pull",
          "shift",
          "unshift"
        ]
      },
      
      "param_a" : {
        "label" : "parameter A",
        "type" : "number"
      }
    }
}

```


## Number

Native Type : Number

```json

{
    "label" : "PROPERTY_NAME",
    "type": "number",
     
}

```


## Boolean


Native Type : Boolean,

```json

{
    "label" : "PROPERTY_NAME",
    "type": "boolean", 
}

```


 


## Color

Native Type : String

```json

{
    "label" : "PROPERTY_NAME",
    "type": "color", 
}

```


## Date

Native Type : String

```json

{
    "label" : "PROPERTY_NAME",
    "type": "date",
    
    "typeStructure" : {
      "start" : DateFormat, // optional
      "end" : DateFormat, // optional
    }
}

```

 



## nodeItemId

Native Type : String, Number, Object

```json

{
    "label" : "PROPERTY_NAME",
    "type": "nodeItemId", 
    "typeStructure" : {
        "acceptType": "item",
        "tid": "contentsphoto",
        "idPid": "contentsphotoId",
        "filter": "",
        "extraListQuery" : {
            "referenceView" : "fileid"
        },
        "listRepresenter" : {
            "test" : ""
        },
        "thumbnailRule": {
            "type": "thumbnail",
            "idPid": "seq",
            "titlePid": "name.label",
            "imagePid": "file"
        }
    }
}

```


## youtubeId


```json

{
    "label" : "Youtube ID",
    "type": "youtubeId",
    "default": "WNCl-69POro",
},

```



# 속성정의 예제들


```json
{
   "title1":{
      "type":"string",
      "label":"타이틀 1",
      "default":"네팔에서 온 남자, 한국에서 온 여자의",
      "editableTarget":".title1"
   },
   "title2":{
      "type":"string",
      "label":"타이틀 2",
      "default":"제주도 속 네팔을 찾아 떠나는 여행",
      "editableTarget":".title2"
   },
   "storyEnabled":{
      "type":"boolean",
      "label":"스토리 사용",
      "default":true
   },
   "story":{
      "label":"스토리",
      "type":"rich-text",
      "editableTarget":".fr-view.story",
      "default":"\n        '네이버x제주관광공사x크리에이터' 그리고 저 김나희가 함께하는 제주의 숨겨진 곳을 찾아 떠나는 알쓸신제 원정대 (알아두면 쓸모있는 신비한 제주) <br>\n            이번 알쓸신제 원정대를 통해 네팔에서 온 제 남편과 제주도 속 네팔의 향기를 찾아 떠나는 여행을 했어요. <br>\n            이 여행 속에는 저의 수채화 작품과 남편의 영상작품이 함께 있습니다<br>\n            <br> - 보름달"
   }
}
```




```json

{
   "rowImageCount":{
      "type":"number",
      "label":"가로 이미지 개수",
      "default":2
   },
   "rowMargin":{
      "label":"세로 마진",
      "type":"number",
      "default":10
   },
   "colMargin":{
      "label":"가로 마진",
      "type":"number",
      "default":10
   },
   "useBottomBorder":{
      "label":"하단 선 사용",
      "type":"boolean"
   },
   "images":{
      "type":"array",
      "label":"이미지 목록",
      "typeStructure":{
         "label":"이미지 설정",
         "type":"object",
         "typeStructure":{
            "alt":{
               "label":"대체 택스트",
               "type":"number"
            },
            "colSize":{
               "label":"이미지 사이즈 (%)",
               "type":"number",
               "desc":"기본값은 50 입니다.",
               "default":50
            },
            "image":{
               "type":"nodeItemId",
               "label":"이미지",
               "typeStructure":{
                  "acceptType":"item",
                  "tid":"contentsphoto",
                  "idPid":"seq",
                  "filter":"",
                  "target":{
                     "contentsid":"{{: @query.contentsid }}",
                     "useyn":"y"
                  },
                  "extraListQuery":{
                     "referenceView":"photoid"
                  },
                  "customPropertyUxMap":{
                     "contentsid":{
                        "importantLevel":1
                     },
                     "photoid":{
                        "importantLevel":1
                     }
                  },
                  "thumbnailRule":{
                     "type":"thumbnail",
                     "idPid":"seq",
                     "titlePid":"photoid.label",
                     "imagePid":"photoid.item.imgpath"
                  }
               }
            }
         }
      }
   }
}



```
