# ICE2 Service Builder

ver 0.8

# TODO

## Upgrade Setting Tools to version 2

### IDEA
 JotForm : https://www.jotform.com/ 의 형태

### Settings Define Rule
 
```
gerneral
  label;text:색상
    colorInput->bgColor;text:배경색
    colorInput->fontColor;text:글자색
    numberInput->fontSize;text:글자 크기
    selectBox->fontFamily;text:폰트


[ElementClassName]->[propertyKeyPath];[options]

```  

## Development


## Installation

### 1. install docker

서비스빌더가 동작 하기 위해서 Docker 환경이 필요합니다.

아래의 링크에서 도커를 설치 해 주세요.

Mac : https://store.docker.com/editions/community/docker-ce-desktop-mac


#### Docker version testing

[work] Docker version 1.12.6, build 3e8e77d/1.12.6


### 2. Install docker image

도커 이미지를 생성합니다.

`$ cd ./server/App/DockerSupport/Worker/`

`$ docker build —no-cache -t worker .`

or

`$ sudo docker build —no-cache -t worker .`

### 3. npm install



`$ npm i`

> note : 프로젝트 루트 디렉토리에서 실행 합니다.

> info : 인스톨 후에 발생하는 에러는 무시합니다.


## Startup

### 1. Start Builder Server

`$ npm run start`

> Note : 시작전에 Docker 를 띄어주세요.

#### Single Container Test Mode

컨테이너에 직접 접속하여 상태를 확인하며 테스트가 필요 할 때 아래의 명령어로 서버를 가동합니다.

`$ PREPARED_WORKER_ID_FOR_TEST=temporary-container-id npm run start`

서버 가동 후 /dashboard 를 거쳐 /studio 에 접속하면 서버 로그에 테스트 안내메시지가 나타날 것 입니다.

안내 메시지를 따라 docker container 를 가동하여 진단을 시작합니다.

bash 모드로 컨테이너에 접속 했을 때 컨테이너내부에서 `$ npm run start` 를 실행 하여 worker instance 를 실행하면 사이트 데이터를 전송받고 

/studio 에서 에디팅을 시작 할 수 있습니다.

### 2. Build Front App

`$ npm run dev`

빌드가 완료 되면 입장합니다.

> info : 현재 버전을 한번 빌드하면 다음엔 하지 않아도 됩니다. 

### 입장
  
 [`http://localhost:8080`](http://localhost:8090)
 
 또는
 
 [`http://builder.localhost:8080`](http://localhost:8090) 



## 서버에 설치

위에서 설명한 Startup 절은 기본적으로 localhost에서 구동하는 방식입니다.

서버에 설치하고 구동하기 위해서는 약간의 설정이 필요합니다.

### 1. 설치 

Startup 절을 따라 설치하여 주세요.

### 2. 설정 

`/ProjectRoot/config/server.ini`

project root 에서 config 디렉토리의 server.ini 파일을 수정 

#### 2.1. 도메인 설정
 
 
`builder_domain_development` 항목과 

`builder_domain_production` 항목의 값을 

`builder.ion.net` 또는 특정한 도메인으로 입력하여 줍니다.

#### 2.2. 도커 연결 방식 설정 [Centos7 기준]

config 는 아래와 같이 설정합니다.
```ini 
docker_connect_type=http;
docker_connect_ip=localhost
docker_connect_port=2376
```

/etc/sysconfig/docker 파일을 열고 `OPTIONS` 필드에 

`-H tcp://0.0.0.0:2376 -H unix:///var/run/docker.sock` 옵션을 추가 합니다.

```
OPTIONS='--selinux-enabled --log-driver=journald --signature-verification=false -H tcp://0.0.0.0:2376 -H unix:///var/run/docker.sock'
```

도커 재시작

`sudo systemctl restart docker`


### 3. Daemon 으로 구동

```
sudo npm i -g forever 

npm run start-d

```

### 4. 접속

DNS서버를 이용하여 서버에 도메인이 연결 되었을 경우에는 해당 도메인에 지정한 포트 번호를 사용하여 접속 합니다.

#### 도메인 연결이 되지 않았을 경우 

Mac : /etc/hosts 파일에 빌더가 설치된 ip 와 config 에 입력한 도메인정보를 입력합니다

Ex) 125.131.88.254 에 설치되고 builder.ion.net 으로 도메인을 지정 하였을 경우 

```
...
125.131.88.254 builder.ion.net <- 추가 
...
```







## Service Deploy & Repair and operations/maintenance Idea

배포 및 유지보수 

nginx 또는 apache httpd 를 이용해 nuxt 웹앱 이중화.


``` 
                  [ Nginx ]
                     :80
                  /   |   \
                 /    |    \
        [service] [service] [service]
          :8081     :8082     :8083
 
 
```

기동중인 서비스 배포

이중화된 nuxt 노드를 순차적으로 빌드하여 배포하기 




# React Boilerplate README


<img src="https://raw.githubusercontent.com/react-boilerplate/react-boilerplate-brand/master/assets/banner-metal-optimized.jpg" alt="react boilerplate banner" align="center" />

<br />

<div align="center"><strong>Start your next react project in seconds</strong></div>
<div align="center">A highly scalable, offline-first foundation with the best DX and a focus on performance and best practices</div>

<br />

<div align="center">
  <!-- Dependency Status -->
  <a href="https://david-dm.org/react-boilerplate/react-boilerplate">
    <img src="https://david-dm.org/react-boilerplate/react-boilerplate.svg" alt="Dependency Status" />
  </a>
  <!-- devDependency Status -->
  <a href="https://david-dm.org/react-boilerplate/react-boilerplate#info=devDependencies">
    <img src="https://david-dm.org/react-boilerplate/react-boilerplate/dev-status.svg" alt="devDependency Status" />
  </a>
  <!-- Build Status -->
  <a href="https://travis-ci.org/react-boilerplate/react-boilerplate">
    <img src="https://travis-ci.org/react-boilerplate/react-boilerplate.svg" alt="Build Status" />
  </a>
  <!-- Test Coverage -->
  <a href="https://coveralls.io/r/react-boilerplate/react-boilerplate">
    <img src="https://coveralls.io/repos/github/react-boilerplate/react-boilerplate/badge.svg" alt="Test Coverage" />
  </a>
</div>
<div align="center">
    <!-- Backers -->
  <a href="#backers">
    <img src="https://opencollective.com/react-boilerplate/backers/badge.svg" alt="Backers" />
  </a>
      <!-- Sponsors -->
  <a href="#sponsors">
    <img src="https://opencollective.com/react-boilerplate/sponsors/badge.svg" alt="Sponsors" />
  </a>
  <a href="http://thinkmill.com.au/?utm_source=github&utm_medium=badge&utm_campaign=react-boilerplate">
    <img alt="Supported by Thinkmill" src="https://thinkmill.github.io/badge/heart.svg" />
  </a>
  <!-- Gitter -->
  <a href="https://gitter.im/mxstbr/react-boilerplate">
    <img src="https://camo.githubusercontent.com/54dc79dc7da6b76b17bc8013342da9b4266d993c/68747470733a2f2f6261646765732e6769747465722e696d2f6d78737462722f72656163742d626f696c6572706c6174652e737667" alt="Gitter Chat" />
  </a>
</div>

<br />

<div align="center">
  <sub>Created by <a href="https://twitter.com/mxstbr">Max Stoiber</a> and maintained with ❤️ by an amazing <a href="https://github.com/orgs/react-boilerplate/people">team of developers</a>.</sub>
</div>

## Features

<dl>
  <dt>Quick scaffolding</dt>
  <dd>Create components, containers, routes, selectors and sagas - and their tests - right from the CLI!</dd>

  <dt>Instant feedback</dt>
  <dd>Enjoy the best DX (Developer eXperience) and code your app at the speed of thought! Your saved changes to the CSS and JS are reflected instantaneously without refreshing the page. Preserve application state even when you update something in the underlying code!</dd>

  <dt>Predictable state management</dt>
  <dd>Unidirectional data flow allows for change logging and time travel debugging.</dd>

  <dt>Next generation JavaScript</dt>
  <dd>Use template strings, object destructuring, arrow functions, JSX syntax and more, today.</dd>

  <dt>Next generation CSS</dt>
  <dd>Write composable CSS that's co-located with your components for complete modularity. Unique generated class names keep the specificity low while eliminating style clashes. Ship only the styles that are on the page for the best performance.</dd>

  <dt>Industry-standard routing</dt>
  <dd>It's natural to want to add pages (e.g. `/about`) to your application, and routing makes this possible.</dd>

  <dt>Industry-standard i18n internationalization support</dt>
  <dd>Scalable apps need to support multiple languages, easily add and support multiple languages with `react-intl`.</dd>

  <dt>Offline-first</dt>
  <dd>The next frontier in performant web apps: availability without a network connection from the instant your users load the app.</dd>

  <dt>SEO</dt>
  <dd>We support SEO (document head tags management) for search engines that support indexing of JavaScript content. (eg. Google)</dd>
</dl>

But wait... there's more!

  - *The best test setup:* Automatically guarantee code quality and non-breaking
    changes. (Seen a react app with 99% test coverage before?)
  - *Native web app:* Your app's new home? The home screen of your users' phones.
  - *The fastest fonts:* Say goodbye to vacant text.
  - *Stay fast*: Profile your app's performance from the comfort of your command
    line!
  - *Catch problems:* AppVeyor and TravisCI setups included by default, so your
    tests get run automatically on Windows and Unix.

There’s also a <a href="https://vimeo.com/168648012">fantastic video</a> on how to structure your React.js apps with scalability in mind. It provides rationale for the majority of boilerplate's design decisions.

<sub><i>Keywords: React.js, Redux, Hot Reloading, ESNext, Babel, react-router, Offline First, ServiceWorker, `styled-components`, redux-saga, FontFaceObserver</i></sub>

## Quick start

1. Clone this repo using `git clone --depth=1 https://github.com/react-boilerplate/react-boilerplate.git`
1. Run `npm run setup` to install dependencies and clean the git repo.<br />
   *We auto-detect `yarn` for installing packages by default, if you wish to force `npm` usage do: `USE_YARN=false npm run setup`*<br />
   *At this point you can run `npm start` to see the example app at `http://localhost:3000`.*
1. Run `npm run clean` to delete the example app.

Now you're ready to rumble!

> Please note that this boilerplate is **production-ready and not meant for beginners**! If you're just starting out with react or redux, please refer to https://github.com/petehunt/react-howto instead. If you want a solid, battle-tested base to build your next product upon and have some experience with react, this is the perfect start for you.

## Documentation

- [**The Hitchhikers Guide to `react-boilerplate`**](docs/general/introduction.md): An introduction for newcomers to this boilerplate.
- [Overview](docs/general): A short overview of the included tools
- [**Commands**](docs/general/commands.md): Getting the most out of this boilerplate
- [Testing](docs/testing): How to work with the built-in test harness
- [Styling](docs/css): How to work with the CSS tooling
- [Your app](docs/js): Supercharging your app with Routing, Redux, simple
  asynchronicity helpers, etc.
- [**Troubleshooting**](docs/general/gotchas.md): Solutions to common problems faced by developers.

## Supporters

This project would not be possible without the support by these amazing folks. [**Become a sponsor**](https://opencollective.com/react-boilerplate) to get your company in front of thousands of engaged react developers and help us out!

<a href="https://opencollective.com/react-boilerplate/bronze-sponsor/0/website" target="_blank"><img src="https://opencollective.com/react-boilerplate/bronze-sponsor/0/avatar.svg"></a>
<a href="https://opencollective.com/react-boilerplate/bronze-sponsor/1/website" target="_blank"><img src="https://opencollective.com/react-boilerplate/bronze-sponsor/1/avatar.svg"></a>
<a href="https://opencollective.com/react-boilerplate/bronze-sponsor/2/website" target="_blank"><img src="https://opencollective.com/react-boilerplate/bronze-sponsor/2/avatar.svg"></a>
<a href="https://opencollective.com/react-boilerplate/bronze-sponsor/3/website" target="_blank"><img src="https://opencollective.com/react-boilerplate/bronze-sponsor/3/avatar.svg"></a>
<a href="https://opencollective.com/react-boilerplate/bronze-sponsor/4/website" target="_blank"><img src="https://opencollective.com/react-boilerplate/bronze-sponsor/4/avatar.svg"></a>
<a href="https://opencollective.com/react-boilerplate/bronze-sponsor/5/website" target="_blank"><img src="https://opencollective.com/react-boilerplate/bronze-sponsor/5/avatar.svg"></a>
<a href="https://opencollective.com/react-boilerplate/bronze-sponsor/6/website" target="_blank"><img src="https://opencollective.com/react-boilerplate/bronze-sponsor/6/avatar.svg"></a>
<a href="https://opencollective.com/react-boilerplate/bronze-sponsor/7/website" target="_blank"><img src="https://opencollective.com/react-boilerplate/bronze-sponsor/7/avatar.svg"></a>
<a href="https://opencollective.com/react-boilerplate/bronze-sponsor/8/website" target="_blank"><img src="https://opencollective.com/react-boilerplate/bronze-sponsor/8/avatar.svg"></a>
<a href="https://opencollective.com/react-boilerplate/bronze-sponsor/9/website" target="_blank"><img src="https://opencollective.com/react-boilerplate/bronze-sponsor/9/avatar.svg"></a>

----

<a href="https://opencollective.com/react-boilerplate/backer/0/website" target="_blank"><img src="https://opencollective.com/react-boilerplate/backer/0/avatar.svg"></a>
<a href="https://opencollective.com/react-boilerplate/backer/1/website" target="_blank"><img src="https://opencollective.com/react-boilerplate/backer/1/avatar.svg"></a>
<a href="https://opencollective.com/react-boilerplate/backer/2/website" target="_blank"><img src="https://opencollective.com/react-boilerplate/backer/2/avatar.svg"></a>
<a href="https://opencollective.com/react-boilerplate/backer/3/website" target="_blank"><img src="https://opencollective.com/react-boilerplate/backer/3/avatar.svg"></a>
<a href="https://opencollective.com/react-boilerplate/backer/4/website" target="_blank"><img src="https://opencollective.com/react-boilerplate/backer/4/avatar.svg"></a>
<a href="https://opencollective.com/react-boilerplate/backer/5/website" target="_blank"><img src="https://opencollective.com/react-boilerplate/backer/5/avatar.svg"></a>
<a href="https://opencollective.com/react-boilerplate/backer/6/website" target="_blank"><img src="https://opencollective.com/react-boilerplate/backer/6/avatar.svg"></a>
<a href="https://opencollective.com/react-boilerplate/backer/7/website" target="_blank"><img src="https://opencollective.com/react-boilerplate/backer/7/avatar.svg"></a>
<a href="https://opencollective.com/react-boilerplate/backer/8/website" target="_blank"><img src="https://opencollective.com/react-boilerplate/backer/8/avatar.svg"></a>
<a href="https://opencollective.com/react-boilerplate/backer/9/website" target="_blank"><img src="https://opencollective.com/react-boilerplate/backer/9/avatar.svg"></a>


## License

This project is licensed under the MIT license, Copyright (c) 2017 Maximilian
Stoiber. For more information see `LICENSE.md`.
