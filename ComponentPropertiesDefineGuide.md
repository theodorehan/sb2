# Component Properties Define Guide

## Write EDIT_RULES

```javascript
// component.vue

<script>
export default {

  "_editRules" : {
    textEditable : BOOLEAN, // default : false
    acceptChildren : BOOLEAN, // default : false
    props : {
      [PROPERTY_KEY] : DefineObject{ ... },
      ...
    }
  },
   
}
</script>
```

## DefineObject

```json

{
    "label" : "PROPERTY_NAME",
    
    "type": Type,
    
    "required" : true, // Optional. ( default : false )
    
    "autoAdding" : false, // Optional. (default : false) 자동으로 속성을 주입하고 부가적인 프로세스를 처리한다.
    
    "hidden" : false, // Optional. (default : false) 에디터 노출되지 않는 속성이다.
    
    "value" : value, // Optional for Code Enumerate...
    
    "typeStructure" : Any, // Optional
    
    "help" : "This property is something.", // Optional
    
    "thumbnail" : "http://image.com/thumbnail.jpg" // Optional
}

```

## Types 

* URL
* String
* Enum ( code )
* Array
* Object
* Number
* Boolean
* API
* Fetched - 페이지단에서 fetch 된 데이터를 가르킨다.
* Fetching - 컴포넌트가 직접 fetch 할 api 지정 
* StoreData
* Range
* Color
* Date
* Unit ( px, pt, %, rem )
* ~~File~~
* ~~Image~~
* ~~Function~~
* Resource 
* Generating
* Curation Source
* nodeItemId

## URL

Native Type : String

```json

{
    "label" : "PROPERTY_NAME",
    "type": "url"
}

```

## String

Native Type : String

```json

{
    "label" : "PROPERTY_NAME",
    "type": "string"
}

```
 
## Code ( Enumerated )

Native Type : String, Number, Boolean,

```json

{
    "label" : "PROPERTY_NAME",
    "type": "code",
    
    "typeStructure" : [
      DefineObject or NativeTypeValue,
      ...
    ], 
    
    "typeStructure" : DefineObject,
}

```


Ex)
```json

{
    "label" : "level",
    "type": "code",
    
    "typeStructure" : [
      1,
      2,
      3,
      {
        "label" : "option 4",
        "type" : "number",
        "value" : 4
      },
      {
        "label" : "option 5",
        "type" : "string",
        "value" : "five"
      }
    ],  
}

```

##  Array

Native Type : Array

```json

{
    "label" : "PROPERTY_NAME",
    "type": "array", 
    
    "typeStructure" : {
      "elementTypes" : [defineObject,...]
    }
}

```

Ex)
```json

{
    "label" : "PROPERTY_NAME",
    "type": "array", 
    
    "typeStructure" : {
      "elementTypes" : [{
        "label" : "numbers",
        "type" : "number"
      },
      {
        "label" : "strings",
        "type" : "string"
      }]
    }
}

```


Ex)
```json

{
    "label" : "rangeNumbers",
    "type": "array", 
    
    "typeStructure" : {
      "elementTypes" : [{
        "label" : "numberRange",
        "type" : "range",
        "typeStructure" : {
          "max" : 99,
          "min" : 0,
          "step" : 2
        }
      }]
    }
}
```

## Object


Native Type : Object

```json
{
    "label" : "PROPERTY_NAME",
    "type": "object", 
    
    "typeStructure" : {
      "FIELD_A" : defineObject,
      "FIELD_B" : defineObject,
    }
}

```


Ex )
```json

{
    "label" : "actionSheet",
    "type": "object", 
    
    "typeStructure" : {
      "actionType" : {
        "label" : "Action Type",
        "required" : true,
        "type" : "enum",
        "typeStructure" : [
          "push",
          "pull",
          "shift",
          "unshift"
        ]
      },
      
      "param_a" : {
        "label" : "parameter A",
        "type" : "number"
      }
    }
}

```


## Number

Native Type : Number

```json

{
    "label" : "PROPERTY_NAME",
    "type": "number",
     
}

```


## Boolean


Native Type : Boolean,

```json

{
    "label" : "PROPERTY_NAME",
    "type": "boolean", 
}

```



## Fetched

Native Type : String,

```json
{
  "label" : "USE_NAME",
  "required" : true, 
  "type" : "fetched",
  "typeStructure" : {
    "expected" : [
      APISheet{},
      ...
    ]
  }
}
```


expected 필드의 fetchSheet 중 하나를 선택하여 기존 페이지 fetch 목록에서 가져오기 또는 새로 fetch를 등록 한다. 

이 속성을 가진 컴포넌트는 페이지에 추가 직전에 해당 속성 세팅을 완료하여야 페이지에 추가가 성립한다.

페이지의 API fetch 툴을 이용하여 세부조정이 가능하다.




TODO.. ToolSwitch 를 이용해 영역은 유지한체 툴만 교체하여 순차적인 인터페이스를 제공한다.

## Fetching

Native Type : Object,

```json 
{
  "label" : "USE_NAME",
  "type": "fetching"
}
```

컴포넌트가 스스로 fetch 하는 속성 

## API





## StoreData


Native Type : Any,

```json

{
    "label" : "PROPERTY_NAME",
    "type": "storeData", 
}

```


## Range

Native Type : Number

```json

{
    "label" : "PROPERTY_NAME",
    "type": "range",
    
    "typeStructure" : {
      "min" : Number,
      "max" : Number,
      "step" : Number,
    }
}

```


## Color

Native Type : String

```json

{
    "label" : "PROPERTY_NAME",
    "type": "color", 
}

```


## Date

Native Type : String

```json

{
    "label" : "PROPERTY_NAME",
    "type": "date",
    
    "typeStructure" : {
      "start" : DateFormat, // optional
      "end" : DateFormat, // optional
    }
}

```


## Unit ( px, pt, %, rem )

Native Type : String

```json

{
    "label" : "PROPERTY_NAME",
    "type": "unit",
    
    "typeStructure" : {
      "unit" : ["px", "pt", "%", "cm"], 
    }
}

```

## ~~File~~ 

Native Type : String

```json

{
    "label" : "PROPERTY_NAME",
    "type": "file", 
}

```

## ~~Image~~

Native Type : String

```json

{
    "label" : "PROPERTY_NAME",
    "type": "image", 
}

```


## Resource

Native Type : String

```json

{
    "label" : "PROPERTY_NAME",
    "type": "image", 
}

```

## Generating

Native Type : Array, Number, String, Boolean

```json

{
    "label" : "PROPERTY_NAME",
    "type": "generating", 
    "typeStructure" : defineObject
}

```


## nodeItemId

Native Type : String, Number, Object

```json

{
    "label" : "PROPERTY_NAME",
    "type": "nodeItemId", 
    "typeStructure" : {
        "acceptType": "item",
        "tid": "contentsphoto",
        "idPid": "contentsphotoId",
        "filter": "",
        "extraListQuery" : {
            "referenceView" : "fileid"
        },
        "listRepresenter" : {
            "test" : ""
        },
        "thumbnailRule": {
            "type": "thumbnail",
            "idPid": "seq",
            "titlePid": "name.label",
            "imagePid": "file"
        }
    }
}

```
