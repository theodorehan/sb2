import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import './style.scss';

let Node = ({
  node,
  indentCount,
  name,
  NodeIconClass,
  nodeGraph,
  spreadList,
  spreadAll,
  onClick,
  onContextMenu,
  onClickSpread,
  onMouseEnter,
  onMouseLeave,

  getNodeOptions,
  getNodeId,
  getName}) =>

  <li
    className='item'
    onClick={(e)=>onClick(e,nodeGraph)}
    onMouseEnter={(e)=>onMouseEnter(e, node)}
    onMouseLeave={(e)=>onMouseLeave(e, node)}
    onContextMenu={(e)=>onContextMenu(e,nodeGraph)}>

    <span
      className='indent'
      style={{
        width: indentCount * 10,
      }}/>

    { node.children && node.children.length ?
      <span className='node-show-sub' onClick={(e)=>onClickSpread(e, node)}>
        <i className={ classnames("fa",( spreadAll || spreadList.indexOf(getNodeId(node)) > -1)? 'fa-caret-up':'fa-caret-down' )}/>
      </span> : '' }

    { NodeIconClass &&
      <span className='node-icon'>
        <NodeIconClass node={node}/>
      </span>
    }

    <span className='node-name' dangerouslySetInnerHTML={{__html : getName ? getName(node) : (name || node.name) }}/>

    {(() => {
      if(!getNodeOptions) return '';

      let nodeOptionsElement = getNodeOptions(node);


      if( nodeOptionsElement ){
        return <div className='node-options'>
        {nodeOptionsElement}
      </div>;
      }
      return '';
    })()}

    <span className='node-tid'>

    </span>
  </li>


export const NodeList = ({
  node,
  indentCount,
  name,
  NodeIconClass,
  nodeGraph,
  spreadList,
  spreadAll,
  onClick,
  onContextMenu,
  onClickSpread,
  onMouseEnter,
  onMouseLeave,

  getNodeOptions,
  getNodeId,
  getName}) => {

  let childrenContainer = null;

  if ( node.children && node.children.length &&
    (spreadAll || spreadList.indexOf(getNodeId(node)) > -1 )
  ) {
    let sortedChildren = node.children.sort((_a, _b) => {
      if (_a.children === _b.children) {
        if (_a.name > _b.name) {
          return 1;
        } else {
          return -1;
        }
      }

      if (_a.children) {
        return -1;
      }

      if (_b.children) {
        return 1;
      }
    });

    childrenContainer = <li className='has-children'>
      {sortedChildren.map((_child, _i) =>
        <NodeList
          node={_child}
          indentCount={indentCount + 1}
          key={_i}
          NodeIconClass={NodeIconClass}
          onClick={onClick}
          onContextMenu={onContextMenu}
          onClickSpread={onClickSpread}
          onMouseEnter={onMouseEnter}
          onMouseLeave={onMouseLeave}


          nodeGraph={[...nodeGraph, _child]}
          spreadList={spreadList}
          spreadAll={spreadAll}

          getNodeOptions={getNodeOptions}
          getNodeId={getNodeId}
          getName={getName}/>
      )}
    </li>;
  }

  return (
    <ul className="graph-node">
      <Node
        node={node}
        indentCount={indentCount}
        name={name}
        NodeIconClass={NodeIconClass}
        onClick={onClick}
        onContextMenu={onContextMenu}
        onClickSpread={onClickSpread}

        onMouseEnter={onMouseEnter}
        onMouseLeave={onMouseLeave}

        nodeGraph={nodeGraph}
        spreadList={spreadList}
        spreadAll={spreadAll}

        getNodeOptions={getNodeOptions}
        getNodeId={getNodeId}
        getName={getName}/>

      { childrenContainer }
    </ul>
  );
}



export default class TreeWrapper extends Component {
  static propTypes = {
    node: PropTypes.object.isRequired,
    name: PropTypes.string,


    // (node) => Icon Element
    NodeIconClass : PropTypes.func,


    onClickNode : PropTypes.func,
    onContextMenu : PropTypes.func,
    onClickSpread : PropTypes.func,

    onMouseEnterNode : PropTypes.func,
    onMouseLeaveNode : PropTypes.func,

    // 열린 노드 리스트
    spreadList : PropTypes.array,
    spreadAll : PropTypes.bool,

    getName : PropTypes.func,
    getNodeId : PropTypes.func,
    getNodeOptions : PropTypes.func,
  };

  static defaultProps = {
    spreadAll : true,
    spreadList : [],
    getNodeId : (node)=> node.name,
    getNodeOptions : (node) => undefined,
  }

  constructor(props) {
    super(props);
  }

  onMouseEnterNode(e, node){
    this.props.onMouseEnterNode && this.props.onMouseEnterNode(e, node)
  }

  onMouseLeaveNode(e, node){
    this.props.onMouseLeaveNode && this.props.onMouseLeaveNode(e, node)
  }

  onClick(e, node){
    this.props.onClickNode && this.props.onClickNode(e, node)
  }

  onContextMenu(e, node){
    this.props.onContextMenu && this.props.onContextMenu(e, node)
  }

  onClickSpread(e, node ){
    this.props.onClickSpread && this.props.onClickSpread(e, node)
  }

  render() {
    let {node, name, NodeIconClass, spreadList, spreadAll, getNodeId, getNodeOptions } = this.props;

    return <div className="node-tree-wrapper">
      <NodeList
        node={node}
        indentCount={0}
        name={name}
        NodeIconClass={NodeIconClass}
        onClickSpread={:: this.onClickSpread }
        onClick={::this.onClick}
        onContextMenu={::this.onContextMenu}

        onMouseEnter={:: this.onMouseEnterNode}
        onMouseLeave={:: this.onMouseLeaveNode}

        nodeGraph={[node]}
        spreadList={spreadList}
        spreadAll={spreadAll}

        getNodeOptions={getNodeOptions}
        getNodeId={getNodeId}
        getName={this.props.getName}/>
    </div>;
  }
}
