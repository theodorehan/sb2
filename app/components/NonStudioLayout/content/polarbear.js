// @flow
import React, { Component } from 'react';
import PolarbearSVG from '../../artworks/withsvg/polarbear';

export default class Polarbear extends Component {
  render() {
    return (
      <div>
        <div className='logo'>
          <PolarbearSVG />
        </div>
      </div>
    );
  }
}
