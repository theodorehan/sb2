// @flow
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import StatusBar from './StatusBar';

export default class Headline extends Component {
  static propTypes = {
    statusBar:PropTypes.bool,
  }

  static defaultProps = {
    statusBar: true,
  };

  render() {
    return (
      <div className='footline'>
        <div className='copy'>
          <i className='fa fa-copyright'/> 2017 ServiceBuilder I-ON Communications.
        </div>

        { this.props.statusBar ? <StatusBar /> : null}
      </div>
    );
  }
}
