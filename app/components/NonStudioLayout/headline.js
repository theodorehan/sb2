// @flow
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import PolarisServiceBuilder from '../artworks/withsvg/polarisServiceBuilder';
import OnlineServerConfig from '../artworks/withsvg/onlineServerConfigTitle';
import Signin from '../artworks/withsvg/SigninTitle';
import Signup from '../artworks/withsvg/SignupTitle';

export default class Headline extends Component {
  static propTypes = {
    title:PropTypes.string,
  }

  renderTitle(){
    switch(this.props.title){
      case "online-server-config":
        return <OnlineServerConfig/>
      case "signin":
        return <Signin/>
      case "signup":
        return <Signup/>
      default:
        return <PolarisServiceBuilder />
    }
  }

  render() {


    return (
      <div className='headline'>
        { this.renderTitle()}
      </div>
    );
  }
}
