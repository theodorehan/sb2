// @flow
import React, { Component  } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// push 는 Action 이다.
import { push } from 'react-router-redux';

class StatusBar extends Component {
  render() {
    return (
      <div className='status-bar'>
      status bar

      </div>
    );
  }
}




function mapStateToProps(state) {
  return {
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({push}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(StatusBar);
