import {
  connect,
} from 'react-redux';
import React from 'react';
import classnames from 'classnames';

function mapStateToProps(state) {
  return {
    Studio: state.get('Studio'),
  }
}

export default connect(mapStateToProps)(
  ({Studio}) =>
    <span className={ classnames("status-icon", Studio.session.onAir && 'onAir', Studio.session.certified && 'certified')}/>
);
