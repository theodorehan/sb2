import {
  connect,
} from 'react-redux';
import React from 'react';
import classnames from 'classnames';

function mapStateToProps(state) {
  return {
    Studio: state.get('Studio'),
  }
}

export default connect(mapStateToProps)(
  ({Studio}) =>
    <span>{
      Studio.session.id
    }</span>
);
