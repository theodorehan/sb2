// export AvatarLabel from './AvatarLabel';

import {
  getValueByKeyPath,
} from '../../utils/ObjectExplorer';

import FullmoonButton from './FullmoonButton';
import * as UserDisplayUnitConnected from './UserDisplayUnitConnected';
import * as OnAirConnected from './OnAirConnected';
import * as DeviceMode from './DeviceMode';

export function get(path) {
  return getValueByKeyPath({
    // AvatarLabel,
    FullmoonButton : FullmoonButton,
    UserDisplayUnitConnected : UserDisplayUnitConnected,
    OnAirConnected : OnAirConnected,
    DeviceMode : DeviceMode,
  }, path);
}


export FullmoonButton from './FullmoonButton';
export * as UserDisplayUnitConnected from './UserDisplayUnitConnected';
export * as OnAirConnected from './OnAirConnected';
export * as DeviceMode from './DeviceMode';

