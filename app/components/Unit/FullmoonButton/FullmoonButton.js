// @flow
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import './fullmoon.scss';
import '../ui.scss';

import * as ObjectExtends from '../../../utils/ObjectExtends'


class Moon extends Component {
  static propTypes = {
    x: PropTypes.number,
    y: PropTypes.number,
    color: PropTypes.string,
    blender: PropTypes.string,
    onFinish: PropTypes.func,
  }


  endAnimation() {
    if (this.props.onFinish)
      this.props.onFinish();
  }


  render() {
    return (
      <div
        className={classnames("moon-circle", this.props.blender)}
        style={{left: this.props.x, top: this.props.y, backgroundColor: this.props.color || ''}}
        onAnimationEnd={this.endAnimation.bind(this)}>

      </div>
    )
  }
}

export default class FullMoonButton extends Component {

  static propTypes = {
    title : PropTypes.string,
    className : PropTypes.string,
    shape: PropTypes.string, // rect, round-rect, circle
    type: PropTypes.string,
    disabled: PropTypes.bool,
    width: PropTypes.number,
    height: PropTypes.number,
    radius: PropTypes.number,
    border: PropTypes.number,
    borderColor: PropTypes.string,
    fontSize: PropTypes.number,
    backgroundImageURL: PropTypes.string,
    backgroundColor: PropTypes.string,

    hoverWidth : PropTypes.number,
    hoverHeight : PropTypes.number,
    hoverRadius: PropTypes.number,
    hoverBorder: PropTypes.number,
    hoverBorderColor: PropTypes.string,
    hoverFontSize: PropTypes.number,

    normalStyle: PropTypes.object,
    hoverStyle: PropTypes.object,

    moonColor: PropTypes.string,
    moonBlender: PropTypes.string,
    useImageDesc: PropTypes.bool,

    onClick: PropTypes.func,
  };

  static defaultProps = {
    shape: 'circle',
    type: 'button',
    disabled: false,
  };

  state = {
    actionState: 'normal',
    moonPool: [],
  };

  click(_e) {
    var nE = _e.nativeEvent;


    this.state.moonPool.push(
      <Moon
        key={Math.random()}
        x={nE.offsetX}
        y={nE.offsetY}
        color={ this.props.moonColor }
        blender={ this.props.moonBlender }
        onFinish={
          function () {
            this.state.moonPool.shift();
            this.setState({moonPool: this.state.moonPool});
          }.bind(this)
        }/>);

    this.setState({moonPool: this.state.moonPool})

    this.props.onClick && this.props.onClick(_e);
  }

  hover() {

    this.setState({actionState: 'hover'});
  }

  leave() {
    this.setState({actionState: 'normal'});
  }

  createShaper() {

  }

  style() {
    let overrideStyleObject;

    let width = this.props.width;
    let height = this.props.height;
    let radius = this.props.radius;

    let size = this.props.radius ? this.props.radius * 2 : 40;
    let fSize = this.props.fontSize ? this.props.fontSize : 12;
    let border = this.props.border ? this.props.border : 1;
    let borderColor = this.props.borderColor ? this.props.borderColor : '#fff';


    switch (this.state.actionState) {
      case 'normal':
        overrideStyleObject = this.props.normalStyle || {};
        break;
      case 'hover':
        if (this.props.hoverStyle) {
          overrideStyleObject = ObjectExtends.merge(this.props.normalStyle, this.props.hoverStyle, true);
        } else {
          overrideStyleObject = this.props.normalStyle || {};
        }

        if (this.props.shape === 'circle') {
          width = height = this.props.hoverRadius ? this.props.hoverRadius * 2 : size;
        } else {
          width = this.props.hoverWidth || this.props.width;
          height = this.props.hoverHeight || this.props.height;
        }

        fSize = this.props.hoverFontSize ? this.props.hoverFontSize : fSize;
        border = this.props.hoverBorder ? this.props.hoverBorder : border;
        borderColor = this.props.hoverBorderColor ? this.props.hoverBorderColor : borderColor;

    }

    if (this.props.shape === 'circle') {
      radius = '50%';
    } else {
      width = this.props.width;
      height = this.props.height;
    }

    var returnStyle = {
      width: width,
      height: height,
      fontSize: fSize,
      borderWidth: border,
      borderColor: borderColor,
      borderRadius: radius,
      backgroundImage: this.props.backgroundImageURL || null,
    }

    return ObjectExtends.merge(returnStyle, overrideStyleObject, true);
  }


  render() {
    var style = this.style();

    return (
      <button
        title={this.props.title}
        type={this.props.type}
        disabled={this.props.disabled}
        className={classnames('ui ui-button-fullmoon', this.props.useImageDesc ? 'image-desc' : '', this.props.className)}
        style={style}
        onClick={this.click.bind(this)}
        onMouseOver={this.hover.bind(this)}
        onFocus={this.hover.bind(this)}
        onMouseOut={this.leave.bind(this)}
        onBlur={this.leave.bind(this)}>

        { this.state.moonPool || null }
        <div className='content'>
          { this.props.children }
        </div>
      </button>
    )
  }
}
