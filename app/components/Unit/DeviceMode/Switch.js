import {
  connect,
} from 'react-redux';
import React from 'react';
import classnames from 'classnames';

function mapStateToProps(state) {
  return {
    Studio: state.get('Studio'),
  }
}

export default connect(mapStateToProps)(
  ({Studio}) => <div>
    <button>Desktop</button>
    <button>Tablet</button>
    <button>Mobile</button>
    <button>Smaller</button>
  </div>

);
