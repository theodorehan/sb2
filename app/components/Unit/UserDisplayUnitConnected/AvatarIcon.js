import {
  connect,
} from 'react-redux';
import React from 'react';
import Avatar from 'material-ui/Avatar';

function mapStateToProps(state){
  return {
    User : state.get('User'),
  }
}

export default connect(mapStateToProps)(
  (props) =>
    <Avatar
      className='userDisplayUnitConnected avatar-icon'
      src={props.User.info.avatarImg}
      style={{width:'1em', 'height':'1em', fontSize:'auto', verticalAlign:'top'}}/>
);
