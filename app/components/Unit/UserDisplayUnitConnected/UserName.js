import {
  connect,
} from 'react-redux';
import React from 'react';


function mapStateToProps(state){
  return {
    User : state.get('User'),
  }
}

export default connect(mapStateToProps)(
  (props) =>
    <span>
      <b>{props.User.info.screenName}</b>
      { props.User.info.admin ? <b>(admin)</b> : undefined }
    </span>
);
