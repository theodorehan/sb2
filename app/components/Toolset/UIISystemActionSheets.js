import defineActionSheet , {
  Types,
} from '../../utils/UII/actSheet';



export const CLOSE_LAYOUT_WITH_ELASTIC = (layoutID, toolKey,layoutRemove = false ) => defineActionSheet({
  type: Types.ToolInterfacePipeline,
  action: 'closeWithLayout',
  options: {
    layoutID,
    toolKey,
    layoutRemove,
  },
});



export const CLOSE_LAYOUT_ONLY = ( layoutID ) => defineActionSheet({
  type: Types.ToolInterfacePipeline,
  action: 'closeLayoutOnly',
  options: {
    layoutID,
  },
});


export const OPEN_LAYOUT_ONLY = ( layoutID ) => defineActionSheet({
  type: Types.ToolInterfacePipeline,
  action: 'openLayoutOnly',
  options: {
    layoutID,
  },
});


export const SET_STATIC_DATA_BY_LAYOUT = (layoutID) => defineActionSheet({
  type : Types.ToolInterfacePipeline,
  action : 'setStaticDataByLayout',
  options : {
    layoutID,
  },
});
