import defineUIIStatement from '../../../../utils/UII/defineUIIStatement';
import defineActSheet, { Types } from '../../../../utils/UII/actSheet';

export const ToolKey = "Database";
export const Info = {
  type : 'button',
  key : 'Database',
  title : 'Database',
  iconType : 'fa',
  icon : 'database',
  iconColor : 'rgb(81,142,171)',
  options: {
    toolKey : ToolKey,
  },

  actionSheet : defineActSheet({
    "action" : "open",
    "type": Types.ToolInterfacePipeline, // ToolInterfacePipeline
    "options" : {
      "toolKey" : ToolKey,
      "areaIndicator" : "[chaining]",
    },
  }),
}

export const UIIStatement = defineUIIStatement(Info);
