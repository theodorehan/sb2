import * as actionTypes from './actionTypes';

export function SPREAD_ITEM(id) {
  return {
    type : actionTypes.SPREAD_ITEM,
    id,
  }
}
