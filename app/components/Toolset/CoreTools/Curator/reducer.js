import * as actionTypes from './actionTypes';


export const initialState = {
  sources : {

  },

  curationParams : {

  },
};

export default function ProjectExplorerReducer(state = initialState, action) {

  switch (action.type) {
    case actionTypes.SPREAD_ALL :
      return Object.assign({}, state, {
        spreadAll : true,
      });

    default :
      return state;
  }
}
