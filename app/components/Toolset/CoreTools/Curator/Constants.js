import defineUIIStatement from '../../../../utils/UII/defineUIIStatement';
import defineActSheet, { Types } from '../../../../utils/UII/actSheet';

export const ToolKey = "Curator";

export const CreateOpenActionSheet = (areaIndicator) => defineActSheet({
  "action" : "open",
  "type": Types.ToolInterfacePipeline, // ToolInterfacePipeline
  "options" : {
    "toolKey" : ToolKey,
    "areaIndicator" : areaIndicator,
  },
});


export const Info = {
  type : 'button',
  key : 'Curator',
  title : 'app.tool.curator',
  titleType : 'code',
  iconType: 'material',
  icon: 'EditorBubbleChart',
  // iconColor : '#fdfdfd',
  options: {
    toolKey : ToolKey,
  },

  actionSheet : CreateOpenActionSheet('[chaining]'),
}



export const UIIStatement = defineUIIStatement(Info);


