import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import classnames from 'classnames';
import cloneDeep from 'lodash/cloneDeep';

import Toggle from 'material-ui/Toggle';
import { Radio, InputNumber, Select, Input  } from 'antd';
const RadioGroup = Radio.Group;
const Option = Select.Option;
const { TextArea } = Input;


import _ from 'lodash';
import Tool from '../common/Tool';
import ToolHeader from '../common/ToolHeader';
import ToolFooter from '../common/ToolFooter';
import ToolBody from '../common/ToolBody';

import ThemeCurator from './TypeCurator/Theme/Theme';
import NodeItemCurator from './TypeCurator/nodeItem';
import NodeItemsCurator from './TypeCurator/nodeItems';
import GeometryNodeDisplayer from '../../../GeometryNodeDisplayer';
import NodeCurationGroup from '../../../NodeCurationGroup';
import './style.scss';


import {
ToolKey as CanvasToolKey,
} from '../Canvas';

import {
  CreateOpenActionSheet as CreateResourceOpenActionSheet,
} from '../ResourceManager/Constants';

import * as actions from './actions';

import mapValuesDeep from '../../../../utils/mapValuesDeep';

import {notification, Icon} from 'antd';


// Settings
_.templateSettings.interpolate = /\{([\s\S]+?)\}/g;

import {
  Info,
} from './Constants';

import AddIcon from 'material-ui/svg-icons/content/add';
import MinusIcon from 'material-ui/svg-icons/content/remove';
import ArrowDropDownIcon from 'material-ui/svg-icons/navigation/arrow-drop-down';
import ArrowDropUpIcon from 'material-ui/svg-icons/navigation/arrow-drop-up';

import FullmoonBtn from '../../../Unit/FullmoonButton';

@connect((state) => ({
  User : state.get('User'),
  Site : state.get('Site'),
  AssignedStore : state.get('AssignedStore'),
  Elastic : state.get('Elastic'),
  Studio : state.get('Studio'),
}))
class Curator extends Component {
  static contextTypes = {
    UIISystems : PropTypes.object,
    elastic: PropTypes.object,
    zone : PropTypes.object,
    events : PropTypes.object,
    functionStore : PropTypes.object,
    theme : PropTypes.object,
    oldCurationParams : PropTypes.object,
    global : PropTypes.object,
    intl : PropTypes.object,
    layout: PropTypes.object,
    dataSetFromServicePage : PropTypes.object,
  }


  constructor(props) {
    super(props)


  }


  onClickFooterButton(item, e, node){
    if( item.functionKey ){
      let func = this.context.functionStore.get(item.functionKey);
      func(item, node);
    }
  }

  onClose(){
    if( this.props.staticData.closeHookFunctionKey ){
      let func = this.context.functionStore.get(this.props.staticData.closeHookFunctionKey);

      if( typeof func === 'function' ){
        return func(this);
      }
    }

    return true;
  }

  getCurationSheet(){
    let { curationMethodSheet } = this.props.staticData;
    return cloneDeep(curationMethodSheet.typeStructure);
  }

  onApply(){
    let reqID = this.context.global.uiBlockingReqBegin();

    this.curator.onApply()
      .then(({madeParams, item, id})=>{
        console.log(item);
        this.context.global.uiBlockingReqFinish(reqID);

        if( this.props.staticData.applyFunctionKey ){
          let func = this.context.functionStore.get(this.props.staticData.applyFunctionKey);

          if( typeof func === 'function' ){

            notification.open({
              message : this.context.intl.formatMessage({id : 'app.common.success-apply'}),
              description : 'Wow',
              icon: <Icon type="smile-circle" style={{ color: '#108ee9' }} />,
            });

            return func({madeParams, item, id});
          }
        }
      });
  }




  render() {
    let sheet = this.getCurationSheet();
    let { componentProps, upperData } = this.props.staticData;
    let {typeId} = sheet.type;
    let {pageInfo} = this.props.Studio;


    let templatingValueSet = {
      $ : {
        params : this.props.Studio.pageInfo.params || {},
        query : this.props.Studio.pageInfo.queries || {},
        props : componentProps || {},
        ...this.context.dataSetFromServicePage,
      },
      upper : upperData,
    };

    if( this.props.staticData.curationId ){
      templatingValueSet.$curation = {
        id : this.props.staticData.curationId, // 이전에 설정된 CurationId 필요에 따라 사용 가능.
      }
    }


    // sheet templating FRONT 테마의 templating 과 동일한 결과를 반환해야 한다.
    sheet = mapValuesDeep(sheet, (value) => {

      if( typeof value === 'string' ){
        try{

          return _.template(value)(templatingValueSet)
        } catch(e) {
          return value;
        }
      }
      return value;
    });



    return (
      <Tool className="curator">
        <ToolHeader
          title={this.context.intl.formatMessage({id : "app.tool.curator"})}
          iconType={ Info.iconType }
          icon={ Info.icon }
          iconColor={ Info.iconColor }
          onCloseHook={ ()=> this.onClose() }>

        </ToolHeader>
        <ToolBody useNativeScroll>
          <div className="title-box">
            {(() => {
              switch(typeId){
                case "theme" :
                 return 'Theme : ';
                case "nodeItem" :
                  return 'Single : ';
                case "nodeItems" :
                  return 'Multiple : ';
                case "GeometryNodeDisplayer":
                  return this.context.intl.formatMessage({id : 'app.curation.grid-curation'});
                case "NodeCurationGroup":
                  return this.context.intl.formatMessage({id : 'app.curation.node-group-curation'});
              }
            })()}

            <span> - </span>

            {
              sheet.targetLabel ?
                sheet.targetLabel :
                this.context.intl.formatMessage({id : 'app.curation.developer.input-targetlabel-in-info'})
            }
          </div>
          {
            (() => {
              switch(typeId){
                case "theme" :
                  return <ThemeCurator
                    defaultPopupTarget={this.context.layout.layoutType}
                    ref={(curator) => this.curator = curator}
                    sheet={sheet}
                    oldCurationParams={this.props.staticData.oldCurationParams}
                    componentProps={componentProps}/>;

                case "nodeItem" :
                  return <NodeItemCurator
                    defaultPopupTarget={this.context.layout.layoutType}
                    ref={(curator) => this.curator = curator}
                    sheet={sheet}
                    oldCurationParams={this.props.staticData.oldCurationParams}
                    componentProps={componentProps}/>;

                case "nodeItems" :
                  return <NodeItemsCurator
                    defaultPopupTarget={this.context.layout.layoutType}
                    ref={(curator) => this.curator = curator}
                    sheet={sheet}
                    oldCurationParams={this.props.staticData.oldCurationParams}
                    componentProps={componentProps}/>;

                case "GeometryNodeDisplayer":
                  return <GeometryNodeDisplayer
                    defaultPopupTarget={this.context.layout.layoutType}
                    /* component props */
                    ref={(curator) => this.curator = curator}
                    sheet={sheet}
                    gridBoxWidth={this.props.width - 20}


                    /* Curation Params */
                    // geometryExtraFilters={[
                    //   'ion',
                    //   'introBeltBanners03',
                    // ]}
                    // itemReferenceNodeType="urlBasedBanner"
                    // itemReferenceNodePid="urlBasedBannerId"
                    // componentProps={componentProps}
                    // itemReferenceNodeThumbnailDisplayRule={{
                    //   type : 'overlayTitle',
                    //   imagePid : 'file',
                    //   titlePid : 'title',
                    // }}
                    // geometryPresets={[
                    //   {
                    //     key : 'default',
                    //     title : '기본',
                    //     horizonCellLength : 4,
                    //     rowHeight:50,
                    //     width:150,
                    //     polygons : [
                    //       {i: 'a', x: 0, y: 0, w: 1, h: 1},
                    //       {i: 'b', x: 1, y: 0, w: 1, h: 1},
                    //       {i: 'c', x: 2, y: 0, w: 2, h: 1},
                    //     ],
                    //   },
                    //
                    //   {
                    //     key : 'A',
                    //     title : '기본A',
                    //     horizonCellLength : 4,
                    //     rowHeight:50,
                    //     width:150,
                    //     polygons : [
                    //       {i: 'a', x: 0, y: 0, w: 2, h: 1},
                    //       {i: 'b', x: 2, y: 0, w: 1, h: 1},
                    //       {i: 'c', x: 3, y: 0, w: 1, h: 1},
                    //     ],
                    //   },
                    //
                    //   {
                    //     key : 'B',
                    //     title : '기본B',
                    //     horizonCellLength : 4,
                    //     rowHeight:50,
                    //     polygons : [
                    //       {i: 'a', x: 0, y: 0, w: 1, h: 1},
                    //       {i: 'b', x: 1, y: 0, w: 1, h: 1},
                    //       {i: 'c', x: 2, y: 0, w: 1, h: 1},
                    //       {i: 'd', x: 3, y: 0, w: 1, h: 1},
                    //     ],
                    //   },
                    // ]}




                    componentProps={componentProps}
                    displayNodeBindHelper={sheet.displayNodeBindHelper}
                    geometryExtraFilters={sheet.groupTarget || []}
                    itemReferenceNodeType={sheet.displayNodeType || ''}
                    itemReferenceNodePid={sheet.displayNodePid || ''}
                    itemReferenceNodeThumbnailDisplayRule={sheet.thumbnailDisplayRuleAtBuilder || {}}
                    itemReferenceCustomUxRuleMap={sheet.referenceCustomUxRuleMap}
                    addRules={sheet.addRules}
                    geometryPresets={sheet.geometryPresets || []}/>;
                case "NodeCurationGroup":
                  return <NodeCurationGroup
                    defaultPopupTarget={this.context.layout.layoutType}
                    /* component props */
                    ref={(curator) => this.curator = curator}
                    sheet={sheet}


                    upperData={upperData}
                    implementedGroupNodeType={sheet.implementedGroupNodeType} // NodeCurationGroup 으로 사용 할 노드타입 Tid
                    groupRetrieveType={sheet.groupRetrieveType} // NodeCurationGroup 의 아이템을 가져오는 방법 oneOfList, read
                    groupTargetParams={sheet.groupTargetParams} // NodeCurationGroup 의 아이템을 가져올 파라미터 셋
                    groupReferenceItemThumbnailDisplayRule={sheet.groupReferenceItemThumbnailDisplayRule} // NodeCurationGroup 이 참조하는 아이템을 랜더링 할 규칙 오브젝트
                    managingPids={sheet.managingPids}  // 관리(편집)할 Pid 목록
                    referencesPid={sheet.referencesPid}  // references 컬럼으로 사용되는 pid
                    referencesNodeTypeIdPid={sheet.referencesNodeTypeIdPid} //
                    referencesNodeTypeTid={sheet.referencesNodeTypeTid}
                    referenceCustomUxRuleMap={sheet.referenceCustomUxRuleMap}

                    itemCurationSheet={sheet.itemCuration}
                    itemHelper={sheet.itemHelper}
                    addRules={sheet.addRules}/>;
              }
            })()
          }
        </ToolBody>


        <ToolFooter>
          <FullmoonBtn
            shape="round"
            normalStyle={{
              display: 'inline-block',
              height: 30,
              borderWidth: 0,

              borderRadius: 100,
              fontSize: 12,
              color: this.context.theme.Tool.layout.footer.fullmoonBtn.textColor.default,
              fontFamily: 'Nanum Square',
              backgroundColor: this.context.theme.Tool.layout.footer.fullmoonBtn.bgColor.default,
              margin:10,
              padding:'0 20px',
              textAlign:'left',
            }}
            hoverStyle={{
              borderWidth: 0,
              backgroundColor:this.context.theme.Tool.layout.footer.fullmoonBtn.bgColor.hovered,
            }}
            moonBlender='overlay'
            onClick={(e)=>this.onApply()}>
            {this.context.intl.formatMessage({id : 'app.common.apply'})}
          </FullmoonBtn>
        </ToolFooter>

      </Tool>
    )
  }
}

export default Curator;
