import React, {Component} from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import Promise from 'bluebird';

_.templateSettings.interpolate = /\{([\s\S]+?)\}/g;

import SelectFromApi from '../../../../../ICE2Elements/Input/selectFromApi';
import SortableDataList from '../../../../../Ice2SortableDataList/SortableDataList';
import ThemeItem from './ThemeItem';
import ThemeChildItem from './ThemeChildItem';

import './Theme.scss';

export default class ThemeCurator extends Component {

  static contextTypes = {
    ice2 : PropTypes.func,
  }

  static propTypes = {
    oldCurationParams : PropTypes.object,
  }

  constructor(props){
    super(props);

    this.state = {
      madeParams : null,
      themeItems : [],
      themeChildItems : [],
      subCurationIndex : null,
      subCurationItem : null,
    }
  }

  onSelectParam(key, value){
    this.setState({
      madeParams : {
        ...this.state.madeParams,
        [key] : value,
      },
    })
  }

  onClickSubCuration(item, i){
    if( this.state.subCurationIndex === i ){
      this.setState({
        subCurationIndex:null,
        subCurationItem : null,
      })
    } else {
      this.setState({
        subCurationIndex:i,
        subCurationItem : item,
      });
    }
  }

  onApply(){

    return new Promise((s,j) => {
      // 일단 하나만
      this.themeListComponent.applyEachItemsToServer()
        .then(()=>{
          s(this.state.madeParams);
          // this.themeSubListComponent.applyEachItemsToServer()
          //   .then(()=>{
          //     s(this.state.madeParams);
          //   });
        })
    })
  }


  componentDidMount(){


    let {sheet} = this.props;
    let {
      params,
    } = sheet;


    let defaultParams = {};
    let paramKeys = Object.keys(params);
    for(let i = 0; i < paramKeys.length ; i++ ){
      defaultParams[paramKeys[i]] = params[paramKeys[i]].default;
    }

    this.setState({
      madeParams : this.props.oldCurationParams || defaultParams,
    });
  }

  render(){
    let {sheet} = this.props;
    let {
      params,
      categoryAPITemplate,
      categorySaveAPITemplate,
      categoryItemsAPITemplate = '',
      categoryItemsSaveAPITemplate = '',
      categorySaveIdFieldName,
      categorySaveFields,
      categoryItemsSaveIdFieldName,
      categoryItemsSaveFields,
    } = sheet;

    let paramKeys = Object.keys(params);

    return <div className="theme-type">
      <div className="param-decorator">
        {
          paramKeys.map((key) => <div className="param" key={key}>
            <div className="name">
              {params[key].label} :
            </div>

            {
              params[key].source === 'api' && <SelectFromApi
                param={params[key]}
                value={this.state.madeParams && this.state.madeParams[key]}
                onChange={(value)=>this.onSelectParam(key, value)}/>
            }
          </div>)
        }
      </div>

      <div className="data-curation">
        {this.state.madeParams && <SortableDataList
          ref={(themeListComponent) => this.themeListComponent = themeListComponent}
          itemType="theme"
          dataUrl={_.template(categoryAPITemplate)({params:this.state.madeParams})}
          itemKeyFieldName="themeId"
          orderModifyUrl={_.template(categorySaveAPITemplate)({params:this.state.madeParams})}
          idFieldName={categorySaveIdFieldName}
          saveFields={categorySaveFields}
          itemComponent={ ({item, index}) => <ThemeItem
            subCurationOpened={ this.state.subCurationItem && this.state.subCurationIndex == index }
            item={item}
            index={index}
            curationParams={this.state.madeParams}
            subDataUrlTemplate={categoryItemsAPITemplate}
            onSubCuration={(item, i) => this.onClickSubCuration(item, i)}/> }
            /> }
      </div>

      {this.state.subCurationItem && <div className="sub-data-curation">
        <SortableDataList
          ref={(themeSubListComponent) => this.themeSubListComponent = themeSubListComponent}
          itemType="theme-sub-item"
          itemKeyFieldName="productToThemeMapId"
          dataUrl={_.template(categoryItemsAPITemplate)({params: this.state.madeParams, upper: this.state.subCurationItem})}
          orderModifyUrl={_.template(categoryItemsSaveAPITemplate)({params: this.state.madeParams, upper: this.state.subCurationItem})}
          idFieldName={categoryItemsSaveIdFieldName}
          saveFields={categoryItemsSaveFields}
          itemComponent={({item,index}) =><ThemeChildItem item={item} index={index} />}/>
      </div>
      }
    </div>;
  }
}
