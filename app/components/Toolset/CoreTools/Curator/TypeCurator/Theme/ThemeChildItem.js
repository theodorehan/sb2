import React, {Component} from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import classnames from 'classnames';

import EditorBubbleChart from 'material-ui/svg-icons/editor/bubble-chart';

_.templateSettings.interpolate = /\{([\s\S]+?)\}/g;


import './Theme.scss';

export default class ThemeCurator extends Component {

  static contextTypes = {
    ice2 : PropTypes.func,
  }

  static propTypes = {
    item : PropTypes.object,
    index : PropTypes.number,
  }

  constructor(props){
    super(props);

  }


  render(){

    return <div className="theme-child-item">
      <div className={classnames("head")}>
        <div className="thumb">
          <img width='100%' src="/public/images/thumb_sample_600.jpg"/>
        </div>
        <div className="desc">
          {this.props.item.productId.label}
        </div>


      </div>
    </div>;
  }
}
