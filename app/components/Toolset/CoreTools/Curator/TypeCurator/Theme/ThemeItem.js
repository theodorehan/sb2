import React, {Component} from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import classnames from 'classnames';

import EditorBubbleChart from 'material-ui/svg-icons/editor/bubble-chart';

_.templateSettings.interpolate = /\{([\s\S]+?)\}/g;

import './Theme.scss';

export default class ThemeCurator extends Component {

  static contextTypes = {
    ice2 : PropTypes.func,
  }

  static propTypes = {
    item : PropTypes.object,
    index : PropTypes.number,
    subDataUrlTemplate : PropTypes.string,
    curationParams : PropTypes.object,
    onSubCuration : PropTypes.func,
    subCurationOpened : PropTypes.bool,
  }

  constructor(props){
    super(props);

  }

  onClickSubList(){
    if( this.props.onSubCuration ) this.props.onSubCuration(this.props.item, this.props.index );
  }


  render(){
    let {
      curationParams,
      item,
      index,
      subDataUrlTemplate,
    } = this.props;

    return <div className="theme-item">
      <div className={classnames("head", this.props.subCurationOpened && 'sub-curation-opened')}>
        {this.props.item.name}

        <div className="options">
          {
            this.props.onSubCuration && <button onClick={ ()=> this.onClickSubList() }>
              <EditorBubbleChart style={{color: this.props.subCurationOpened ? '#fff':'#333'}}/>
            </button>
          }
        </div>
      </div>
    </div>;
  }
}
