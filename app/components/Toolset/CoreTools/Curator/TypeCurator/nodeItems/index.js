import React, {Component} from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import Promise from 'bluebird';
import { Input } from 'antd';

const Search = Input.Search;
_.templateSettings.interpolate = /\{([\s\S]+?)\}/g;

import CuratorBase from '../CuratorBase';
import SelectFromApi from '../../../../../ICE2Elements/Input/selectFromApi';
import SortableDataList from '../../../../../Ice2SortableDataList/SortableDataList';

import './style.scss';



export default class NodeItems extends CuratorBase {

  static contextTypes = {
    ice2 : PropTypes.func,
  }

  static propTypes = {
    componentProps : PropTypes.object,
  }


  constructor(props){
    super(props);

    this.state = Object.assign(this.state,{

    });
  }


  getImageUrl(image){

    if( image ){
      let baseUrl = this.context.ice2.defaults.baseURL;

      return baseUrl + '/' + image.storePath;
    }

    return '';
  }

  onApply(){

    return new Promise((s,j) => {
      // 일단 하나만
      this.nodeItemList.applyEachItemsToServer()
        .then(()=>{
          s(this.state.madeParams);
        })
    })
  }

  componentDidMount(){
    super.componentDidMount();

  }

  render(){
    let {sheet, componentProps} = this.props;
    let {
      params,
      nodeType,
      saveIdFieldName,
      updateFields,
      imagePid,
      idPid,
      titlePid,
    } = sheet; // typeStructure

    let {
      exposeItemLimit,
    } = componentProps;


    return <div className="nodeItem-type">
      <div className="sub-title-box">
        Let's curation about { nodeType }.
      </div>
       <SortableDataList
         ref={(nodeItemList) => this.nodeItemList = nodeItemList}
         dataUrl={`/node/${nodeType}/list.json`}
         itemKeyFieldName="id"
         itemType="1levelNode"
         orderModifyUrl={`/node/${nodeType}/save.json`}
         idFieldName={saveIdFieldName}
         saveFields={updateFields}
         itemComponent={

         ({item, index}) => <div className="record" key={index} style={{ width: '100%', opacity: index < exposeItemLimit ? 1 : 0.5 }}>
            <span className="id">
              {item[idPid]}
            </span>

            <img src={this.getImageUrl(item[imagePid]) || `http://placehold.it/100x30?text=${item[titlePid]}`} width="100px"/>

            <span className="head">
              {item[titlePid]}
            </span>
         </div>

       }>

       </SortableDataList>
    </div>;
  }
}
