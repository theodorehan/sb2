import React, {Component} from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import Promise from 'bluebird';
_.templateSettings.interpolate = /\{([\s\S]+?)\}/g;


export default class CuratorBase extends Component {

  static propTypes = {
    oldCurationParams : PropTypes.object,
  }

  constructor(props){
    super(props);

    this.state = {
      madeParams : null,
    }


  }


  onApply(){

    return new Promise((s,j) => {

      s(this.state.madeParams);
    })
  }


  componentDidMount(){
    let {sheet} = this.props;
    let {
      params,
    } = sheet;


    let defaultParams = {};
    let paramKeys = Object.keys(params);
    for(let i = 0; i < paramKeys.length ; i++ ){
      defaultParams[paramKeys[i]] = params[paramKeys[i]].default;
    }

    this.setState({
      madeParams : this.props.oldCurationParams || defaultParams,
    });
  }


}
