import React, {Component} from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import Promise from 'bluebird';
import { Input } from 'antd';

const Search = Input.Search;
_.templateSettings.interpolate = /\{([\s\S]+?)\}/g;

import SelectFromApi from '../../../../../ICE2Elements/Input/selectFromApi';

import './style.scss';

export default class NodeItem extends Component {

  static contextTypes = {
    ice2 : PropTypes.func,
  }

  static propTypes = {
    oldCurationParams : PropTypes.object,
  }

  constructor(props){
    super(props);

    this.state = {
      items : [],
      madeParams : null,
    }


    this.onSearch = this.onSearch.bind(this);
  }

  onSelectParam(key, value){
    this.setState({
      madeParams : {
        ...this.state.madeParams,
        [key] : value,
      },
    })
  }


  onApply(){

    return new Promise((s,j) => {

          s(this.state.madeParams);

    })
  }

  onSearch({value}){
    console.log(value);
    let {sheet} = this.props;

    this.context.ice2.get(`/node/${sheet.nodeType}/list.json`, {
      params : {title_matching : value},
    })
      .then((res)=>{
        this.setState({
          items : res.data.items,
        });
      });
  }



  getImageUrl(image){
    if( image ){
      let baseUrl = this.context.ice2.defaults.baseURL;

      return baseUrl + '/' + image.storePath;
    }

    return '';
  }

  componentDidMount(){


    let {sheet} = this.props;
    let {
      params,
    } = sheet;


    let defaultParams = {};
    let paramKeys = Object.keys(params);
    for(let i = 0; i < paramKeys.length ; i++ ){
      defaultParams[paramKeys[i]] = params[paramKeys[i]].default;
    }

    this.setState({
      madeParams : this.props.oldCurationParams || defaultParams,
    });


    this.context.ice2.get(`/node/${sheet.nodeType}/list.json`)
      .then((res)=>{
        this.setState({
          items : res.data.items,
        });
      });
  }


  renderContentsCard(item, index){
    let {sheet} = this.props;
    let {
      imagePid,
      idPid,
      titlePid,
    } = sheet;

    let id = item[idPid];

    let selected = this.state.madeParams.ids.find((oid) => oid ==id);


    return <div className="record" key={index} style={{ width: '100%' }}>
      <span className="id">{id}</span>

      <img src={this.getImageUrl(item[imagePid]) || 'http://placehold.it/100x30'} width="100px"/>

      <span className="head">
        {item[titlePid]}
      </span>

      { selected ? <span className="show"> Exposing </span> : <span className="show want" onClick={()=>this.onSelectParam('ids', [id])}> Expose </span>}
    </div>;
  }

  render(){
    let {sheet} = this.props;
    let {
      params,
      nodeType,
      refreshMethod,
      imagePid,
      idPid,
      titlePid,
    } = sheet;

    let paramKeys = Object.keys(params);

    return <div className="nodeItem-type">
      <div className="sub-title-box">
        Let's curation about { nodeType }.
      </div>
      <div className="search-box">
        <Search
          placeholder="input search text"
          style={{ width: '100%' }}
          onChange={(e)=> this.onSearch({value:e.target.value})}
          onSearch={(value) => this.onSearch({value})}
        />
      </div>
      {
        this.state.items.map((item, i)=> this.renderContentsCard(item, i))
      }
    </div>;
  }
}
