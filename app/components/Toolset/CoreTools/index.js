import * as Canvas from './Canvas';
import * as CodeEditor from './CodeEditor';
import * as ComponentPalette from './ComponentPalette';
import * as Database from './Database';
import * as DataInspector from './DataInspector';
import * as Inspector from './Inspector';
import * as ProjectExplorer from './ProjectExplorer';
import * as ProjectSettings from './ProjectSettings';
import * as TreeGraph from './TreeGraph';
import * as VueEditor from './VueEditor';
import * as Pages from './Pages';
import * as API from './API';
import * as Resources from './Resources';
import * as Elements from './Elements';
import * as NPM from './NPM';
import * as WebpackManager from './WebpackManager';
import * as Notifications from './Notifications';
import * as GridEditor from './GridEditor';
import * as NodeTreeInspector from './NodeTreeInspector';
import * as Git from './Git';
import * as Settings from './Settings';
import * as PageOptions from './PageOptions';
import * as Dialog from './Dialog';
import * as ResourceManager from './ResourceManager';
import * as Publisher from './Publisher';
import * as Curator from './Curator';
import * as ICE2Data from './ICE2Data';
import * as YoutubeExplorer from './YoutubeExplorer';
import * as ICE2VersionManager from './ICE2VersionManager';
import * as FragmentTemplateManager from './FragmentTemplateManager';

export * as Canvas from './Canvas';
export * as CodeEditor from './CodeEditor';
export * as ComponentPalette from './ComponentPalette';
export * as Database from './Database';
export * as DataInspector from './DataInspector';
export * as Inspector from './Inspector';
export * as ProjectExplorer from './ProjectExplorer';
export * as ProjectSettings from './ProjectSettings';
export * as TreeGraph from './TreeGraph';
export * as VueEditor from './VueEditor';
export * as Pages from './Pages';
export * as API from './API';
export * as Resources from './Resources';
export * as Elements from './Elements';
export * as NPM from './NPM';
export * as WebpackManager from './WebpackManager';
export * as Notifications from './Notifications';
export * as GridEditor from './GridEditor';
export * as NodeTreeInspector from './NodeTreeInspector';
export * as Git from './Git';
export * as Settings from './Settings';
export * as PageOptions from './PageOptions';
export * as Dialog from './Dialog';
export * as ResourceManager from './ResourceManager';
export * as Publisher from './Publisher';
export * as Curator from './Curator';
export * as ICE2Data from './ICE2Data';
export * as YoutubeExplorer from './YoutubeExplorer';
export * as ICE2VersionManager from './ICE2VersionManager';
export * as FragmentTemplateManager from './FragmentTemplateManager';

export const ToolAccessMap = {
  [Canvas.ToolKey] : Canvas,
  [CodeEditor.ToolKey] : CodeEditor,
  [ComponentPalette.ToolKey] : ComponentPalette,
  [Database.ToolKey] : Database,
  [DataInspector.ToolKey] : DataInspector,
  [Inspector.ToolKey] : Inspector,
  [ProjectExplorer.ToolKey] : ProjectExplorer,
  [ProjectSettings.ToolKey] : ProjectSettings,
  [TreeGraph.ToolKey] : TreeGraph,
  [VueEditor.ToolKey] : VueEditor,
  [Pages.ToolKey] : Pages,
  [API.ToolKey] : API,
  [Resources.ToolKey] : Resources,
  [Elements.ToolKey] : Elements,
  [NPM.ToolKey] : NPM,
  [WebpackManager.ToolKey] : WebpackManager,
  [Notifications.ToolKey] : Notifications,
  [GridEditor.ToolKey] : GridEditor,
  [NodeTreeInspector.ToolKey] : NodeTreeInspector,
  [Git.ToolKey] : Git,
  [Settings.ToolKey] : Settings,
  [PageOptions.ToolKey] : PageOptions,
  [Dialog.ToolKey] : Dialog,
  [ResourceManager.ToolKey] : ResourceManager,
  [Publisher.ToolKey] : Publisher,
  [Curator.ToolKey] : Curator,
  [ICE2Data.ToolKey] : ICE2Data,
  [YoutubeExplorer.ToolKey] : YoutubeExplorer,
  [ICE2VersionManager.ToolKey] : ICE2VersionManager,
  [FragmentTemplateManager.ToolKey] : FragmentTemplateManager,
}
