import * as actionTypes from './actionTypes';


export function CHANGE_SEARCH_VALUE(value) {
  return {
    type : actionTypes.CHANGE_SEARCH_VALUE,
    value,
  };
}

export function CHANGE_COMPLEX_MODE(checked) {
  return {
    type : actionTypes.CHANGE_COMPLEX_MODE,
    checked,
  };
}


export function TOGGLE_GROUP_SPREAD_STATE(groupName, checked) {
  return {
    type : actionTypes.TOGGLE_GROUP_SPREAD_STATE,
    groupName,
    checked,
  };
}
