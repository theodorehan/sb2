import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import classnames from 'classnames';


import Tool from '../common/Tool';
import ToolHeader from '../common/ToolHeader';
import ToolFooter from '../common/ToolFooter';
import ToolBody from '../common/ToolBody';


import BalancedItemTray from '../../../BalancedItemTray';

import './style.scss';

import {
  Info,
} from './Constants';

import * as actions from './actions';


@connect((state) => ({
  Site : state.get('Site'),
  AssignedStore : state.get('AssignedStore'),
}))
class PageOptions extends Component {
  static contextTypes = {
    toolSystem: PropTypes.object,
    elastic: PropTypes.object,
    UIISystems : PropTypes.object,
    zone : PropTypes.object,
  }

  constructor(props) {
    super(props)
  }


  render() {
    return (
      <Tool className="pages">
        <ToolHeader
          title="Page Options"
          iconType={ Info.iconType }
          icon={ Info.icon }
          iconColor={ Info.iconColor }/>
        <ToolBody>

        </ToolBody>
        <ToolFooter>
          <BalancedItemTray className="styling-button-set" divisionBorderColor="#073044">

          </BalancedItemTray>
        </ToolFooter>
      </Tool>
    )
  }
}

export default PageOptions;
