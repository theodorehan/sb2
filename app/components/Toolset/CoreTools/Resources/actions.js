import * as actionTypes from './actionTypes';


export function FILE_UPLOAD(value) {
  return {
    type : actionTypes.FILE_UPLOAD,
    value,
  };
}
