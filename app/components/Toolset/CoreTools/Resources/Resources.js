import React, {Component} from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";

import _ from "underscore";

import {Dialog, FlatButton, GridList, GridTile, RaisedButton} from "material-ui";

import Tool from "../common/Tool";
import ToolHeader from "../common/ToolHeader";
import ToolFooter from "../common/ToolFooter";
import ToolBody from "../common/ToolBody";

import * as AssignedStoreNetwork from "../../../../Core/Network/AssignedStore";
import * as AssignedStoreActions from "../../../../actions/AssignedStore";

import "./style.scss";
import {Info} from "./Constants";

const styles = {
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
  },
  gridList: {
    width: 500,
    overflowY: 'auto',
  },
};


let NodeIcon = (_props) => {
  if (_props.node.isDir) {
    return <i className="fa fa-folder"/>;
  }

  switch (_props.node.extension) {
    default :
      return <i className="fa fa-file-text"/>;
  }
}

var originalImageSrc;
var currentImage;
var csdkImageEditor;

@connect((state) => ({
  Site: state.get('Site'),
  AssignedStore: state.get('AssignedStore'),
  User: state.get('User'),
}))
class Resources extends Component {
  static contextTypes = {
    toolSystem: PropTypes.object,
    elastic: PropTypes.object,
    // AssignedStore: PropTypes.object,
    // Site: PropTypes.object,
  }

  constructor(props) {
    super(props)
    this.state = {open: false, disabled: true};
  }

  componentDidMount() {
    var _userInfo = this.props.User.info;
    var _siteInfo = this.props.Site.info;
    var dispatch = this.props.dispatch;
    /*----------------------------------------------*/
    /*Adobe creativeSDK*/
    csdkImageEditor = new Aviary.Feather({
      language: 'ko',
      jpgQuality: 100,
      apiKey: '6c3c7f71fdb949dc922f53b2809d4279',
      onSaveButtonClicked: function (imageID) {
        csdkImageEditor.getImageData(function (base64img) {
          AssignedStoreNetwork.imageSave(base64img, imageID, _siteInfo.assignedStoreID)
            .then(function (_result) {
              let {status, data} = _result;
              if (status === 200 && data.result === 'success') {
                console.log('_siteInfo.assignedStoreID', _siteInfo.assignedStoreID);
                AssignedStoreNetwork.getTree(_userInfo.sessionID, _siteInfo.id)
                  .then(function (_result) {
                    let {status, data} = _result;
                    if (status === 200 && data.result === 'success') {
                      dispatch(AssignedStoreActions.LOAD_TREE(data.root));
                    }
                  });
              }
            }.bind(this));
        }.bind(this))
      },
      onSave: function (imageID, newURL) {
        currentImage.src = newURL;
        csdkImageEditor.close();
        console.log('newURL', newURL);
      },

      onError: function (errorObj) {
        console.log(errorObj.code);
        console.log(errorObj.message);
        console.log(errorObj.args);
      },
    });
    /*----------------------------------------------*/
  }

  _fileUpload() {
    var data = new FormData();
    var _userInfo = this.props.User.info;
    var _siteInfo = this.props.Site.info;
    var dispatch = this.props.dispatch;
    data.append('file', document.getElementById('file').files[0]);
    data.append('assignedStoreID', _siteInfo.assignedStoreID);
    AssignedStoreNetwork.fileUpload(data, _siteInfo.assignedStoreID)
      .then(function (_result) {
        let {status, data} = _result;
        if (status === 200 && data.result === 'success') {
          AssignedStoreNetwork.getTree(_userInfo.sessionID, _siteInfo.id)
            .then(function (_result) {
              let {status, data} = _result;

              if (status === 200 && data.result === 'success') {
                dispatch(AssignedStoreActions.LOAD_TREE(data.root));
              }
            });
          this._handleClose();
        }
      }.bind(this));
  }

  _renderNodeTypeList(_node, _indent, _i, _name) {
    var self = this;

    let childrenContainer = null;

    if (_node.isDir && _node.children.length > 0) {
      let sortedChildren = _node.children.sort((_a, _b) => {
        if (_a.isDir === _b.isDir) {
          if (_a.name > _b.name) {
            return 1;
          } else {
            return -1;
          }
        }

        if (_a.isDir) {
          return -1;
        }

        if (_b.isDir) {
          return 1;
        }

      });
      childrenContainer = <div style={styles.root}>
        <GridList cellHeight={180} style={styles.gridList}>
          {_node.children.map(function (_child, _i) {
            return self._renderTile(_child, _indent + 1, _i)
          })}
        </GridList>
      </div>;
      // childrenContainer = <li className='has-children'>
      //   {_node.children.map(function (_child, _i) {
      //     return self._renderNodeTypeList(_child, _indent + 1, _i)
      //   })}
      // </li>;
    }

    return (
      <ul className="node" key={_i}>
        { this._renderNode(_node, _indent, _i, _name)}
        { childrenContainer }
      </ul>
    );
  }

  _renderTile(_node, _indentCount, _i, _name) {
    var _siteInfo = this.props.Site.info;
    var _session = this.props.User.session;
    return <GridTile
      key={ _node.name }
      title={ _node.name }
      subtitle={<span>by <b>I-ON</b></span>}
      onTouchTap={::this._imageOnClick}>
      <img id={_node.name} name={_node.name} src={ "/dataStore/" + _siteInfo.assignedStoreID + "/src/statics/images/" + _node.name + "?siteID=" + _siteInfo.id + "&userSessionID=" + _session.id}/>
    </GridTile>
  }

  _renderNode(_node, _indentCount, _i, _name) {
    let isDir = _node.isDir;

    return (
      <li className='item' key={_i}>
        <span className='indent' style={{width: _indentCount * 25}}/>

        { isDir ?
          <span className='node-show-sub'>
            <i className="fa fa-caret-down"/>
          </span> : ''}
        <span className='node-icon'>
          <NodeIcon node={_node}/>
        </span>
        <span className='node-name'>
          { _name || _node.name }
        </span>
        <span className='node-option'>

        </span>
        <span className='node-tid'>
        </span>
      </li>
    )
  }

  _onChange(e) {
    var value = e.target.value;
    var that = this;
    if (value != undefined || value != null) {
      that.setState({disabled: false});
    }
  }

  _handleOpen = () => {
    this.setState({open: true});
  };

  _handleClose = () => {
    this.setState({open: false});
  };

  _getChild(key, data) {
    var i = 0, found;
    for (; i < data.length; i++) {
      if (data[i].name === key) {
        return data[i];
      } else if (_.isArray(data[i].children)) {
        found = this._getChild(key, data[i].children);
        if (found) {
          return found;
        }
      }
    }
  }

  _imageOnClick = (e) => {
    console.log('_imageOnClick~!')
    currentImage = e.target;
    csdkImageEditor.launch({
      image: currentImage.id,
      url: currentImage.src,
    });
  };

  _getImageData = () => {

  }

  render() {
    let {AssignedStore, Site} = this.props;
    let mainStoreRoot = AssignedStore.mainStoreRoot.children;
    let resourceRoot = this._getChild('images', mainStoreRoot);

    const actions = [
      <FlatButton
        label="Cancel"
        primary={true}
        onTouchTap={::this._handleClose}
      />,
      <FlatButton
        label="Upload"
        primary={true}
        onTouchTap={::this._fileUpload}
      />,
    ];
    return (
      <Tool className="project-explorer">
        <ToolHeader
          title="Resources"
          iconType={ Info.iconType }
          icon={ Info.icon }
          iconColor={ Info.iconColor }/>
        <ToolBody>
          <FlatButton label="View Mode Change" fullWidth={true}/>
          <div className='node-tree-wrapper'>
            {this._renderNodeTypeList(resourceRoot, 0, 0, Site.info.siteName) }
          </div>
        </ToolBody>
        <ToolFooter>
          <div>
            <RaisedButton
              label="Choose an Image"
              labelPosition="before"
              containerElement="label"
              labelStyle={{lineHeight: "32px"}}
              style={{height: "29px"}} onTouchTap={::this._handleOpen}>
            </RaisedButton>
            <RaisedButton
              label="Choose an Image"
              labelPosition="before"
              containerElement="label"
              labelStyle={{lineHeight: "32px"}}
              style={{height: "29px"}} onTouchTap={::this._getImageData}>
            </RaisedButton>
            <Dialog
              title="Image Upload"
              actions={actions}
              modal={true}
              open={this.state.open}>
              <form>
                <input id="file" type="file" onChange={::this._onChange}/>
              </form>
            </Dialog>
          </div>
        </ToolFooter>
      </Tool>
    )
  }
}

export default Resources;
