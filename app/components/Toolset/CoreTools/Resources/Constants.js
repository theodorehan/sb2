import defineUIIStatement from '../../../../utils/UII/defineUIIStatement';
import defineActSheet, { Types } from '../../../../utils/UII/actSheet';

export const ToolKey = "Resources";
export const Info = {
  type : 'button',
  key : 'Resources',
  title : 'app.tool.resource',
  titleType : 'code',
  iconType: 'img',
  icon: '/public/images/picture-pencil-icon.png',
  iconColor : '#ff9d00',
  options: {
    toolKey : ToolKey,
  },

  actionSheet : defineActSheet({
    "action" : "open",
    "type": Types.ToolInterfacePipeline, // ToolInterfacePipeline
    "options" : {
      "toolKey" : ToolKey,
      "areaIndicator" : "[chaining]",
    },
  }),
}

export const UIIStatement = defineUIIStatement(Info);
