import * as actionTypes from './actionTypes';


export const initialState = {
  searchValue: '',
  complexMode : true,
  spreadList : [],
};

export default function ProjectExplorerReducer(state = initialState, action) {

  switch (action.type) {
    case actionTypes.FILE_UPLOAD :
      return Object.assign({}, state, {
        value: action.value,
      });

    default :
      return state;
  }
}
