import * as actionTypes from './actionTypes';

export function TOGGLE_EDITING_MODE(checked) {
  return {
    type : actionTypes.TOGGLE_EDITING_MODE,
    checked,
  };
}

export function TOGGLE_MINI_CONTROLLER(checked){
  return {
    type : actionTypes.TOGGLE_MINI_CONTROLLER,
    checked,
  }
}

export function TOGGLE_DROP_FOCUSING(checked){
  return {
    type : actionTypes.TOGGLE_DROP_FOCUSING,
    checked,
  }
}

export function TOGGLE_NODE_GRAPH(checked) {
  return {
    type : actionTypes.TOGGLE_NODE_GRAPH,
    checked,
  };
}

/**
 *
 * @param treeJSON
 * @param directiveName
 * @param selectDirectiveNodePos : 옵션 DirectiveNode 로딩과 동시에 특정 노드를 선택하게 할 때 사용
 * @param selectedDirectiveName : 옵션 위와 같음
 * @returns {{type, treeJSON: *, directiveName: *, selectDirectiveNodePos: *, selectedDirectiveName: *}}
 * @constructor
 */
export function LOADED_ROUTE_DIRECTIVE_TREE(
  treeJSON,
  directiveName,
  selectDirectiveNodePos,
  selectedDirectiveName,
  serviceFragmentId,
  fragmentAcceptComponents,
  historyAction,) {
  return {
    type : actionTypes.LOADED_ROUTE_DIRECTIVE_TREE,
    treeJSON,
    directiveName,
    selectDirectiveNodePos,
    selectedDirectiveName,
    serviceFragmentId,
    fragmentAcceptComponents,
    historyAction,
  };
}

export function HISTORY_MOVE(directiveName,cursor){
  return {
    type : actionTypes.HISTORY_MOVE,
    historyAction : 'move',
    directiveName,
    cursor,
  }
}


export function BIND_DIRECTIVE_FRAGMENT_ID(directiveName, serviceFragmentId) {
  return {
    type : actionTypes.BIND_DIRECTIVE_FRAGMENT_ID,
    directiveName,
    serviceFragmentId,
  }
}



export function LOAD_ROUTE_DIRECTIVE_TREE(directiveName) {
  return {
    type : actionTypes.LOAD_ROUTE_DIRECTIVE_TREE,
    directiveName,
  };
}


export function SELECT_DIRECTIVE_NODE(directiveName, directiveNode, directiveNodePos, selectingPoint, withContextMenu, componentEditRules) {
  return {
    type : actionTypes.SELECT_DIRECTIVE_NODE,
    directiveName,
    directiveNode,
    directiveNodePos,
    selectingPoint,
    withContextMenu,
    componentEditRules,
  };
}


export function RESET_DIRECTIVE_NODE() {
  return {
    type : actionTypes.RESET_DIRECTIVE_NODE,
  };
}

export function CLOSE_CONTEXT_MENU() {
  return {
    type : actionTypes.CLOSE_CONTEXT_MENU,
  };
}

export function SELECT_NODE_OF_COMPONENT(selectedComponentPath, selectedNodeOfComponent, selectedRootComponentNodePos) {
  return {
    type : actionTypes.SELECT_NODE_OF_COMPONENT,
    selectedComponentPath,
    selectedNodeOfComponent,
    selectedRootComponentNodePos,
  };
}

export function RESET_SELECTED_NODE_OF_COMPONENT() {
  return {
    type : actionTypes.RESET_SELECTED_NODE_OF_COMPONENT,
  };
}




export function SET_IFRAME_STATE(screenWidth, totalWidth, totalHeight, path, title ) {
  return {
    type : actionTypes.SET_IFRAME_STATE,
    screenWidth,
    totalHeight,
    totalWidth,
    path,
    title,
  }
}

export function SET_BODY_HTML_DIRECTIVE_NODE(node) {
  return {
    type : actionTypes.SET_BODY_HTML_DIRECTIVE_NODE,
    node,
  }
}


export function SET_ROUTE_DIRECTIVE_NAME(directiveName) {
  return {
    type : actionTypes.SET_ROUTE_DIRECTIVE_NAME,
    directiveName,
  }
}


export function COMPONENT_DRAGGING_OVER_DIRECTIVE(dragOverDirectiveName, dragOverDirectiveNode, nearby, componentJSON) {
  return {
    type : actionTypes.COMPONENT_DRAGGING_OVER_DIRECTIVE,
    dragOverDirectiveName,
    dragOverDirectiveNode,
    componentJSON,
    nearby,
  }
}


export function COMPONENT_DRAGGING_OVER_COMPONENT(dragOverComponentName, dragOverNodeOfComponent, componentJSON) {
  return {
    type : actionTypes.COMPONENT_DRAGGING_OVER_COMPONENT,
    dragOverComponentName,
    dragOverNodeOfComponent,
    componentJSON,
  }
}

export function MOVE_REMOTE_CONTROL(x,y) {
  return {
    type : actionTypes.MOVE_REMOTE_CONTROL,
    x,
    y,
  }
}

export function LAUNCH_WYSIWYG_EDITOR() {
  return {
    type : actionTypes.LAUNCH_WYSIWYG_EDITOR,
  }
}

