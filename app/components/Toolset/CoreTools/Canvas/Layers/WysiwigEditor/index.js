import React, { Component } from 'react';

import PropTypes from 'prop-types';

import ReactTinymce from 'react-tinymce';
import { Tooltip } from 'antd';
import classnames from 'classnames';

import EditorFormatBoldIcon from 'material-ui/svg-icons/editor/format-bold';
import EditorFormatItalicIcon from 'material-ui/svg-icons/editor/format-italic';
import EditorFormatUnderlinedIcon from 'material-ui/svg-icons/editor/format-underlined';
import EditorFormatColorTextIcon from 'material-ui/svg-icons/editor/format-color-text';
import EditorFormatColorFillIcon from 'material-ui/svg-icons/editor/format-color-fill';
import EditorFormatSizeIcon from 'material-ui/svg-icons/editor/format-size';
import EditorFormatAlignJustifyIcon from 'material-ui/svg-icons/editor/format-align-justify';
import EditorFormatAlignLeftIcon from 'material-ui/svg-icons/editor/format-align-left';
import EditorFormatAlignRightIcon from 'material-ui/svg-icons/editor/format-align-right';
import EditorFormatListBulletedIcon from 'material-ui/svg-icons/editor/format-list-bulleted';
import EditorFormatListNumberedIcon from 'material-ui/svg-icons/editor/format-list-numbered';

import { Menu, Dropdown, Icon } from 'antd';

import { ChromePicker } from 'react-color';

import FullmoonBtn from '../../../../../Unit/FullmoonButton';


import './style.scss';

export default class WysiwygEditor extends Component {
  static contextTypes = {
    intl : PropTypes.object,
    theme : PropTypes.object,
  }

  static propTypes = {
    onWheel : PropTypes.func,
    dispatch : PropTypes.func,
    x : PropTypes.number,
    y : PropTypes.number,
    areaWidth : PropTypes.number,
    width : PropTypes.number,
    wysiwygTargetEditRule: PropTypes.object,
  }



  constructor(props){
    super(props);

    this.state = {
      colorPickerDisplay : false,
      colorPickerPositioning: null,
      colorPickerTarget : null,
      colorPickerCallback : null,
    }
  }

  getCanvasIframeWindow(){
    return this.props.iframe.contentWindow;
  }

  getIframeWindow(){
    return this.iframe.contentWindow;
  }

  getValue(){

    if( this.props.wysiwygTargetEditRule.type === 'rich-text' ){
      return this.getIframeWindow().document.body.innerHTML;
    } else {
      return this.getIframeWindow().document.body.innerText;
    }
  }

  onSelectSize(size){
    alert('개발중입니다.')
    return;

    // https://github.com/timdown/rangy 사용

    console.log('apply size', size);
    this.getIframeWindow().document.execCommand('fontSize', false, 7 );

    console.log(this.state.selection);
    this.state.selection.baseNode.parentNode.style.fontSize = size+'px';
  }

  onClickBold(){
    this.getIframeWindow().document.execCommand('bold');
  }

  onClickItalic(){
    this.getIframeWindow().document.execCommand('italic');
  }

  onClickUnderline(){
    this.getIframeWindow().document.execCommand('underline');
  }

  onClickAlignLeft(){
    this.getIframeWindow().document.execCommand('justifyLeft');
  }

  onClickAlignJustify(){
    this.getIframeWindow().document.execCommand('justifyFull');
  }

  onClickAlignRight(){
    this.getIframeWindow().document.execCommand('justifyRight');
  }

  onClickListBullet(){
    this.getIframeWindow().document.execCommand('insertUnorderedList');
  }

  onClickListNumber(){
    this.getIframeWindow().document.execCommand('insertOrderedList');
  }

  onClickColor(){

    // this.getIframeWindow().document.execCommand('foreColor', false, '#534c7a');
    this.setState({
      colorPickerDisplay : true,
      colorPickerTarget : 'foreColor',
      colorPickerPositioning: 'text-selection',
    })
  }

  onClickBackColor(){
    this.setState({
      colorPickerDisplay : true,
      colorPickerTarget : 'hiliteColor',
      colorPickerPositioning: 'text-selection',
    })
  }

  onSelectColor(color){
    if( this.state.colorPickerTarget === 'foreColor' ){
      this.getIframeWindow().document.execCommand('foreColor', false, `rgba(${color.rgb.r},${color.rgb.g},${color.rgb.b},${color.rgb.a})`);
    } else if ( this.state.colorPickerTarget === 'hiliteColor' ){
      this.getIframeWindow().document.execCommand('hiliteColor', false, `rgba(${color.rgb.r},${color.rgb.g},${color.rgb.b},${color.rgb.a})`);
    }
  }

  onSelectFillColor(){

  }

  onScroll(e){

    this.props.onWheel(e);
  }

  onInput(e){
    this.props.wysiwygTargetDOM.innerHTML = this.getIframeWindow().document.body.innerHTML;
    this.forceUpdate();

  }

  componentDidMount(){
    let window = this.getIframeWindow();
    let canvasWindow = this.getCanvasIframeWindow();
    window.document.designMode = 'on';
    let currentText = this.props.wysiwygTargetDOM.innerHTML;
    this.props.wysiwygTargetDOM.setAttribute('contenteditable', true);
    let successiveComputedStyle = canvasWindow.getComputedStyle(this.props.wysiwygTargetDOM);
    let successiveCSSText = successiveComputedStyle.cssText;
    this.props.wysiwygTargetDOM.removeAttribute('contenteditable');

    window.document.write(currentText);
    window.document.body.setAttribute('style',successiveCSSText);
    window.document.body.addEventListener('input', (e)=>{
     this.onInput(e);
    });

    window.document.body.addEventListener('wheel', (e)=>{
      this.onScroll(e);
    });

    window.document.addEventListener('selectionchange', () => {
      if( this.state.colorPickerDisplay ){
        this.setState({
          colorPickerDisplay : false,
          selection : this.getIframeWindow().document.getSelection(),
        })
      } else {
        this.setState({
          selection : this.getIframeWindow().document.getSelection(),
        })
      }

    }, false);
    this.getIframeWindow().document.execCommand('styleWithCSS', false, true);
    window.document.body.focus();
  }

  renderFontSizeMenu(){
    let sizes = [6,9,12,16,18,24,28,34];


    return <Menu>
      {
        sizes.map((size) => <Menu.Item key={size} style={{fontSize : size}} >
          <span style={{display:'block', minWidth:'100%', minHeight:'100%'}} onClick={()=>this.onSelectSize(size)}>{size}</span>
        </Menu.Item>)
      }
    </Menu>
  }

  renderColorPicker(baseStyle, targetDOMRect, canvasRect){
    let style = {};
    if( this.state.colorPickerPositioning === 'text-selection' ){
      // let selection = this.getIframeWindow().document.getSelection();

      let selection = this.state.selection;
      let range = selection.getRangeAt(0);
      let rangeRect = range.getBoundingClientRect();

      style.display = 'block';
      style.left = rangeRect.left + baseStyle.left + 10;
      style.top = rangeRect.top + baseStyle.top;
      if( style.top > 250 ){
        style.top -= 250;
      } else if ( canvasRect.height > style.top + rangeRect.height + 250 ){
        style.top += rangeRect.height;
      } else {
        style.top = rangeRect.top + 10 + baseStyle.top;
      }

      console.log(range.getBoundingClientRect());
    }

    return  <div className={ classnames("color-picker" )} style={style}>
      <div className="palette">
        <ChromePicker

          color={this.state.colorPickerColor}
          onChange={(color) => this.setState({colorPickerColor:color})}/>
      </div>

      <div className="sub-panel">
        <FullmoonBtn
          shape="round"
          normalStyle={{
            display: 'inline-block',
            height: 30,
            borderWidth: 0,

            borderRadius: 100,
            fontSize: 12,
            color: this.context.theme.Tool.layout.footer.fullmoonBtn.textColor.default,
            fontFamily: 'Nanum Square',
            backgroundColor: this.context.theme.Tool.layout.footer.fullmoonBtn.bgColor.default,
            margin:10,
            padding:'0 20px',
            textAlign:'left',
          }}
          hoverStyle={{
            borderWidth: 0,
            backgroundColor:this.context.theme.Tool.layout.footer.fullmoonBtn.bgColor.hovered,
          }}
          moonBlender='overlay'
          onClick={(e)=>this.onSelectColor(this.state.colorPickerColor)}>
          Apply
        </FullmoonBtn>
      </div>
    </div>
  }

  render(){
    // console.log(this.props.wysiwygTargetDOM);
    let canvasIfrmae = this.props.iframe;
    let canvasRect = this.props.iframeBoundingRect;
    // console.log('Canvas rect', canvasRect)
    let rect = this.props.wysiwygTargetDOM.getBoundingClientRect();
    // console.log('rect', rect);
    let style = {};
    style.left = canvasRect.left + rect.left - 8;
    style.top = canvasRect.top + rect.top - 8;
    style.width = rect.width + 16;
    style.height = rect.height + 16;


    return <div className="wysiwyg-editor">

      <div
        style={{left : Math.max(style.left, canvasRect.left + 10), top : Math.max(style.top, canvasRect.top + 40)}}
        className="text-modifier">
        { this.props.wysiwygTargetEditRule.type == 'rich-text' && <div className="wrapper">


          <div className="option">
            <button>
              <Tooltip title="Bold">
                <Dropdown overlay={this.renderFontSizeMenu()}>
                  <EditorFormatSizeIcon/>
                </Dropdown>
              </Tooltip>
            </button>
          </div>

          <div className="option">
            <button onClick={::this.onClickBold}>
              <Tooltip title="Bold">
                <EditorFormatBoldIcon style={{color:'inherit'}}/>
              </Tooltip>
            </button>
          </div>
          <div className="option">
            <button onClick={::this.onClickItalic}>
              <Tooltip title="Italic">
                <EditorFormatItalicIcon style={{color:'inherit'}}/>
              </Tooltip>
            </button>
          </div>
          <div className="option">
            <button onClick={::this.onClickUnderline}>
              <Tooltip title="Underline">
                <EditorFormatUnderlinedIcon style={{color:'inherit'}}/>
              </Tooltip>
            </button>
          </div>

          <div className={classnames("option", this.state.colorPickerDisplay && this.state.colorPickerTarget == 'foreColor' && 'active')}>
            <button onClick={:: this.onClickColor }>
              <Tooltip title="Text color">
                <svg viewBox="0 0 24 24" style={{"display":"inline-block", "color": "inherit", fill: "currentcolor", height: 24, width: 24, userSelect: "none", transition: "all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms"}}><path d="M11 3L5.5 17h2.25l1.12-3h6.25l1.12 3h2.25L13 3h-2zm-1.38 9L12 5.67 14.38 12H9.62z"></path></svg>
              </Tooltip>
            </button>

          </div>

          <div className={classnames("option", this.state.colorPickerDisplay && this.state.colorPickerTarget == 'hiliteColor' && 'active')}>
            <button onClick={:: this.onClickBackColor }>
              <Tooltip title="Text hiliting">
                <svg viewBox="0 0 24 24" style={{display: "inline-block", color: "inherit" ,fill: "currentcolor", height: 24, width: 24, userSelect: "none", transition: "all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms"}}><path d="M16.56 8.94L7.62 0 6.21 1.41l2.38 2.38-5.15 5.15c-.59.59-.59 1.54 0 2.12l5.5 5.5c.29.29.68.44 1.06.44s.77-.15 1.06-.44l5.5-5.5c.59-.58.59-1.53 0-2.12zM5.21 10L10 5.21 14.79 10H5.21zM19 11.5s-2 2.17-2 3.5c0 1.1.9 2 2 2s2-.9 2-2c0-1.33-2-3.5-2-3.5z"></path></svg>
              </Tooltip>
            </button>
          </div>

          <div className="option">
            <button onClick={::this.onClickAlignLeft}>
              <Tooltip title="Align left">
                <EditorFormatAlignLeftIcon style={{color:'inherit'}}/>
              </Tooltip>
            </button>
          </div>
          <div className="option">
            <button onClick={::this.onClickAlignJustify}>
              <Tooltip title="Align justify">
                <EditorFormatAlignJustifyIcon style={{color:'inherit'}}/>
              </Tooltip>
            </button>
          </div>
          <div className="option">
            <button onClick={::this.onClickAlignRight}>
              <Tooltip title="Align right">
                <EditorFormatAlignRightIcon style={{color:'inherit'}}/>
              </Tooltip>
            </button>
          </div>
          <div className="option">
            <button onClick={::this.onClickListBullet}>
              <Tooltip title="Align right">
                <EditorFormatListBulletedIcon style={{color:'inherit'}}/>
              </Tooltip>
            </button>
          </div>
          <div className="option">
            <button onClick={::this.onClickListNumber}>
              <Tooltip title="Align right">
                <EditorFormatListNumberedIcon style={{color:'inherit'}}/>
              </Tooltip>
            </button>
          </div>
        </div>}
      </div>

      { this.state.colorPickerDisplay && this.renderColorPicker(style, rect, canvasRect) }

      <iframe
        style={style}
        frameBorder={0}
        onInput={::this.onInput}
        ref={ (iframe) => this.iframe = iframe } />
    </div>
  }
}
