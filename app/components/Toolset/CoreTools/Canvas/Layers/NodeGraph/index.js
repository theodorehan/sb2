import React , { Component } from 'react';
import PropTypes from 'prop-types';
import './style.scss';
import classnames from 'classnames';

class NodeGraphLayer extends Component {
  static propTypes = {
    iframe : PropTypes.object,
    graphTree: PropTypes.object,
    graphLoadingState : PropTypes.string,
    selectedDirectivePos : PropTypes.string,
  }

  constructor(props){
    super(props);
  }

  clickNode(id){

    console.log(id);
  }

  getNodeDisplayName(node){
    switch(node.tag){
      case "core-system-grid" :
        return "GRID";
      case "core-system-row" :
        return "ROW";
      case "core-system-column" :
        return "COLUMN";
      case "core-system-layer" :
        return "LAYER";
      default:
        return node.tag
    }
  }


  renderChildren(children, myWidth, stride, id){
    let childrenWidthSum = myWidth - 20;
    let Wstep = childrenWidthSum / children.length;


    return  children.map((graph, i) => {

      if( i === 0 ){
        return this.renderNodeGraph(graph, i, 10, Wstep, stride, id + '.' + i );
      } else {
        return this.renderNodeGraph(graph, i, 10 + i * Wstep, Wstep, stride, id + '.' + i );
      }
    });
  }


  renderNodeGraph(graphTree, myIndex, left, width, stride, id){
    let style = {}


    style.left = left;
    style.width = width;

    let nextStride = [];
    let trail = false;

    if( parseInt(stride[0]) === myIndex ){
      nextStride = stride.slice(1);
      trail = true;
    }

    return <div className="node" style={style} key={myIndex}>
        <div
          className={classnames("bar", typeof graphTree === 'object' ? graphTree.tag:'', trail ? 'trail':'')}
          title={typeof graphTree === 'object' ? graphTree.tag : JSON.stringify(graphTree)}
          onClick={ ()=> this.clickNode(id) }>
          {typeof graphTree === 'object' ? this.getNodeDisplayName(graphTree) : JSON.stringify(graphTree) }
        </div>

        {
          Array.isArray(graphTree.children) && <div className="childGraph">
            { this.renderChildren(graphTree.children, width, nextStride, id) }
          </div>
        }
      </div>
  }

  render(){
    if( this.props.graphTree ){
      let selectedStrides = (this.props.selectedDirectivePos || '' ).split('.');

      return <div className="node-graph-layer">
        { this.props.graphLoadingState === 'loaded' && this.renderNodeGraph(this.props.graphTree, 0, 0,this.props.width, selectedStrides, '0' ) }
      </div>;
    }else {
      return <div></div>
    }
  }
}

export default NodeGraphLayer;
