import React, { Component } from 'react';
import './style.scss';

import ScrollWrapper from 'react-scrollbar';
import PropTypes from 'prop-types';

import Draggable from '../../../../../../Core/Components/Draggable';

import ContentsDecorationPalette from './ContentsDecorationPalette';

import CurationPalette from './CurationPalette';

import {
  MOVE_REMOTE_CONTROL,
} from '../../actions'

export default class RemoteControl extends Component {
  static propTypes = {
    dispatch : PropTypes.func,
    x : PropTypes.number,
    y : PropTypes.number,
    areaWidth : PropTypes.number,
    width : PropTypes.number,
  }



  constructor(props){
    super(props);
  }

  onDragMove(x,y,deltaX, deltaY){
    let nextX = this.props.x + deltaX;

    if( nextX > this.props.areaWidth - this.props.width ){
      nextX = this.props.areaWidth - this.props.width;
    }

    if( nextX < 0 ){
      nextX = 0;
    }

    this.props.dispatch(MOVE_REMOTE_CONTROL(nextX, this.props.y + deltaY));
  }

  render(){
    return <div className="remote-control" style={{left: this.props.x , top: this.props.y, width:this.props.width, height:this.props.height}}>
      <div className="top">
        <Draggable
          onDragMove={:: this.onDragMove}
          _draggableData={{}}
          _draggableType="none"
          component={()=><div>:::</div>}
          className="move-handle"
          invisibleGhost={true}
        />
      </div>

      <div className="body">
        <ScrollWrapper style={{width:'100%', height:'100%'}}>
          { this.props.mode == 'curation' ? <CurationPalette width={this.props.width}/> : <ContentsDecorationPalette width={this.props.width}/> }
        </ScrollWrapper>
      </div>

      <div className="bottom">

      </div>
    </div>
  }
}
