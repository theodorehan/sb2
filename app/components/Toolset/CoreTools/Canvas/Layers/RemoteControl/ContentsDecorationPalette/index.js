import React, { Component } from 'react';
import {connect} from 'react-redux';
import './style.scss';

import PropTypes from 'prop-types';

import Draggable from '../../../../../../../Core/Components/Draggable';
import {
  getBuiltInComponentCategories,
  getBuiltInComponentCategorySub,
  getBuiltInComponentCategoryComponentPreview,
} from '../../../../../../../Core/Network/AssignedStore';

import ComponentItemWrapper from '../../../../ComponentPalette/ComponentItemWrapper';

// http://builder.localhost:8090/api/user/e82eb890-30f0-410f-a64e-f75940084c3c/site/ytn.0aa8814b-5afa-4b8d-873d-a4c4d0f06c54/system/store/builtin-component-categories/sub?path=about


@connect((state) => ({
  User: state.get('User'),
  Site : state.get('Site'),
}))
export default class ContentsDecorationPalette extends Component {

  constructor(props){
    super(props);

    this.state = {
      components : [],
    }
  }



  componentDidMount(){
    getBuiltInComponentCategorySub(this.props.User.session.id, this.props.Site.info.id, this.props.Site.info.owner, 'contentsDecoration')
      .then((res)=>{
        this.setState({
          components : res.data.filledCategoryItem.children,
        })
      })
  }

  render(){
    return <div>
      {
        this.state.components.map(
          (item, i) => <ComponentItemWrapper item={item} key={i} fullPath={`contentsDecoration/${item.pathName}`} width={this.props.width - 10} imageMaxHeight={100}/>)
      }

    </div>
  }
}
