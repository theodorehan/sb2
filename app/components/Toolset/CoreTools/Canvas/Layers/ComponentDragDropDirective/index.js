import React , { Component } from 'react';
import PropTypes from 'prop-types';
import {Icon} from 'antd';
import './style.scss';
import classnames from 'classnames';

import {
  CreateOpenActionSheet as CreateGridEditorOpenActionSheet,
} from '../../../GridEditor';

import {
  findDirectiveRootNode,
  TEMPLATE_JSON_DIRECTIVE_SIGN,
  DIRECTIVE_POS_ATTR_SIGN,
  getDirectiveNodeByPos,
  getDirectiveNodeName,
} from '../../../../../../Core/Helper/VueSupports';

import {
  getAppendOrInsertComponentMethod,
} from '../../../../../../Core/Helper/GridDirectiveModifyPolicing';

function getNodeIcon(node){
  switch( node.tag ){
    case "core-system-grid":
      return <i className="fa fa-table"/>;
    case "core-system-row":
      return <i className="fa fa-align-justify"/>;
    case "core-system-column":
      return <i className="fa fa-columns"/>;
    case "core-system-layer":
      return <i className="fa fa-clone"/>;
    default:
      return <i className="fa fa-cubes"/>;
  }
}


class ComponentDragDropDirective extends Component {
  static propTypes = {
    iframe : PropTypes.object,

    directivesMap : PropTypes.object,

    dragOverDirectiveName : PropTypes.string,
    dragOverDirectiveNode : PropTypes.object,
  }

  static contextTypes = {
    toolSystem: PropTypes.object,
    elastic: PropTypes.object,
    UIISystems : PropTypes.object,
    zone : PropTypes.object,
    events : PropTypes.object,
  }

  constructor(props){
    super(props);

  }

  renderComponentDropGuideNotice(rect, iframeRect, boundingDom, dragOverDirectiveNearbyRegion){
    let method = getAppendOrInsertComponentMethod(this.props.dragOverDirectiveNode,dragOverDirectiveNearbyRegion);

    return <div className="drop-guide-notice-box">
      {method.method}
    </div>
  }

  renderWillDropArea(rect, iframeRect, boundingDom, dragOverDirectiveNearbyRegion){
    let style = {};
    let iconClass;
    if( dragOverDirectiveNearbyRegion === 'in' ){
      style.left = rect.left + 20;
      style.top = rect.top + 20;
      style.width = rect.width - 40;
      style.height = rect.height - 40;

      iconClass = 'plus';
    } else if ( dragOverDirectiveNearbyRegion === 'left' ){
      style.left = rect.left - 3;
      style.top = rect.top;
      style.width = 40;
      style.height = rect.height;

      iconClass = 'left-circle';
    } else if ( dragOverDirectiveNearbyRegion === 'right' ){
      style.left = rect.left + rect.width - 40;
      style.top = rect.top;
      style.width = 40;
      style.height = rect.height;

      iconClass = 'right-circle';
    }else if ( dragOverDirectiveNearbyRegion === 'top' ){
      style.left = rect.left;
      style.top = rect.top - 3;
      style.width = rect.width;
      style.height = 40;

      iconClass = 'up-circle';
    } else if ( dragOverDirectiveNearbyRegion === 'bottom' ){
      style.left = rect.left;
      style.top = rect.top + rect.height - 40;
      style.width = rect.width;
      style.height = 40;

      iconClass = 'down-circle';
    }

    style.left += iframeRect.left;
    style.top += iframeRect.top;


    return <div
      style={style}
      className={"drop-zone-preview " + dragOverDirectiveNearbyRegion}>
        <div className='guide'>
          <Icon type={iconClass} style={{color:'inherit', fontSize:'inherit'}} />
        </div>
    </div>
  }

  boundingAreaHalfHiding(rect, iframeRect, boundingDom){
    let boundingRect = boundingDom.getBoundingClientRect();

    return <div className="bounding-container">
      <div className="part left" style={{width:boundingRect.left + iframeRect.left}}/>
      <div className="part top" style={{left:boundingRect.left + iframeRect.left, width : boundingRect.width, height: boundingRect.top}}/>
      <div className="part right" style={{left : boundingRect.left + boundingRect.width + iframeRect.left}}/>
      <div className="part bottom" style={{left:boundingRect.left + iframeRect.left, top : boundingRect.top + boundingRect.height, width : boundingRect.width}}/>
    </div>
  }


  renderText(x,y, text, colorClass){
    return <div className={classnames('text')} style={{left:x, top:y}}>
      {text}
    </div>
  }

  renderHorizontalLine(x1, w, y, key, colorClass, i){
    return <div className={classnames('line h', colorClass)} style={{left:x1, width:w, top:y}} key={key}></div>
  }

  renderVerticalLine(y1, h, x, key,colorClass){
    return <div className={classnames('line v', colorClass)} style={{top:y1, height:h, left:x}} key={key}></div>
  }


  renderBox(rect, iframeRect){
    let lines = [];


    lines.push(this.renderHorizontalLine(rect.left + iframeRect.left , rect.width, rect.top, '1'));
    lines.push(this.renderHorizontalLine(rect.left + iframeRect.left , rect.width, rect.bottom, '2'));
    lines.push(this.renderVerticalLine(rect.top + iframeRect.top , rect.height, rect.left + iframeRect.left , '3'));
    lines.push(this.renderVerticalLine(rect.top + iframeRect.top , rect.height, rect.right + iframeRect.right, '4'));

    // lines.push(this.renderText(rect.left, rect.top - 20, 'selected'));

    return lines;
  }

  renderOptions(rect, iframeRect){
    let style = {
      left : rect.left + iframeRect.left,
      top : rect.top + iframeRect.top - 35,
    };

    return <div className="options" style={style}>
      <div className={classnames('text')}>
        {getNodeIcon(this.props.dragOverDirectiveNode)}
        {getDirectiveNodeName(this.props.dragOverDirectiveNode)}
      </div>
    </div>
  }

  render(){
    let { dragOverDirectiveName, dragOverDirectiveNode, selectedDirectiveName, selectedDirectiveNode, dragOverDirectiveNearbyRegion, iframe, areaWidth } = this.props;
    let iframeWindow = iframe.contentWindow;
    if( !(iframeWindow.document && iframeWindow.document.readyState === 'complete' && dragOverDirectiveNode) ){
      return <div className="modifing-layer"></div>;
    }

    if( dragOverDirectiveNode ){
      let dom = iframeWindow.document.querySelector(
        `[${TEMPLATE_JSON_DIRECTIVE_SIGN}="${dragOverDirectiveName}"][${DIRECTIVE_POS_ATTR_SIGN}="${dragOverDirectiveNode.pos}"], 
      [${TEMPLATE_JSON_DIRECTIVE_SIGN}="${dragOverDirectiveName}"] [${DIRECTIVE_POS_ATTR_SIGN}="${dragOverDirectiveNode.pos}"]`
      )

      if ( !dom ) return <div></div>;

      let box = dom.getBoundingClientRect();
      let iframeRect = {
        left : iframe.offsetLeft,
        right : iframe.offsetLeft,
        top : iframe.offsetTop,
      };


      let boundingDom;
      if(selectedDirectiveNode ) {
        boundingDom = iframeWindow.document.querySelector(
            `[${TEMPLATE_JSON_DIRECTIVE_SIGN}="${selectedDirectiveName}"][${DIRECTIVE_POS_ATTR_SIGN}="${selectedDirectiveNode.pos}"], 
        [${TEMPLATE_JSON_DIRECTIVE_SIGN}="${selectedDirectiveName}"] [${DIRECTIVE_POS_ATTR_SIGN}="${selectedDirectiveNode.pos}"]`
          )
      };



      return <div className="component-drag-drop-layer">
        { this.renderBox(box, iframeRect) }
        { this.renderOptions(box, iframeRect)}
        { this.renderWillDropArea(box, iframeRect, boundingDom, dragOverDirectiveNearbyRegion) }
        { this.renderComponentDropGuideNotice(box, iframeRect, boundingDom, dragOverDirectiveNearbyRegion)}
        { selectedDirectiveNode && this.boundingAreaHalfHiding(box, iframeRect, boundingDom) }
      </div>;
    } else {

      return <div className="component-drag-drop-layer">
        { this.renderWillDropArea(box, iframeRect) }
      </div>;
    }
  }
}

export default ComponentDragDropDirective;
