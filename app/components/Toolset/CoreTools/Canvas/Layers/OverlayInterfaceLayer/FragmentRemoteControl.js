import React,{Component} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import Draggable from '../../../../../../Core/Components/Draggable';
import ComponentItemWrapper from '../../../ComponentPalette/ComponentItemWrapper';
import RedoIcon from 'material-ui/svg-icons/av/fast-forward';
import UndoIcon from 'material-ui/svg-icons/av/fast-rewind';
import StarBorderIcon from 'material-ui/svg-icons/toggle/star-border';
import StarIcon from 'material-ui/svg-icons/toggle/star';
import ArrowDownIcon from 'material-ui/svg-icons/hardware/keyboard-arrow-down';
import ArrowUpIcon from 'material-ui/svg-icons/hardware/keyboard-arrow-up';

import ScrollWrapper from 'react-scrollbar';

export default class FragmentRemoteControl extends Component {
  static propTypes = {
    x : PropTypes.number,
    y : PropTypes.number,
    acceptComponents : PropTypes.array,
    onMove : PropTypes.func,
    onApplyDirective : PropTypes.func,
    directiveHumanName : PropTypes.string,
    onUndo : PropTypes.func,
    onRedo : PropTypes.func,
    metaComponents : PropTypes.array,
  };

  static contextTypes = {
    ice2 : PropTypes.func,
  }


  constructor(){
    super()

    this.state = {
      collapsed : true,
      customComponents : [],
    }
  }


  prevent(e){
    e.preventDefault();
    e.stopPropagation();
  }

  onDragMove(x, y, deltaX, deltaY){
    this.props.onMove(deltaX, deltaY);
  }

  toggleCollapse(){
    this.setState({
      collapsed : !this.state.collapsed,
    })
  }

  onContentWheel(e){
    let dY = e.nativeEvent.deltaY;

    if( this.bodyDOM ){
      this.bodyDOM.scrollTop += dY;
    }
  }

  componentDidMount(){

  }

  render(){
    return <div
      style={{left:this.props.x, top:this.props.y}}
      className={classnames("fragment-remote-control", this.state.collapsed && 'collapsed')}
      onClick={:: this.prevent}
      onContextMenu={:: this.prevent}
      onWheel={:: this.prevent }>

      <Draggable
        _draggableType="null"
        _draggableData={{}}
        _additionalStyle={{width:'100%'}}
        onDragMove={:: this.onDragMove}
        invisibleGhost
        component={() => <div
          className="frc-head">
          { this.props.directiveHumanName || '영역 리모컨'}

          <button onClick={:: this.toggleCollapse} onMouseMove={:: this.prevent} >
            { this.state.collapsed ? <ArrowDownIcon style={{width:20, color:'inherit'}}/> : <ArrowUpIcon style={{width:20, color:'inherit'}}/> }
          </button>
        </div>}/>

      <div className="frc-body" onWheel={(e) => this.onContentWheel(e)} ref={(dom) => this.bodyDOM = dom}>
        { (this.props.acceptComponents || []).map((c,i) => c ? <ComponentItemWrapper
          item={{
            componentKey: c.toLowerCase().replace(/\.vue$/, '').replace(/\//g, '-'),
            name: c.split('/').pop(),
            pathName: c.split('/').pop(),
            componentLocation: c.replace(/\.vue$/, ''),
          }}
          key={i}
          fullPath={c}
          width={170}
          imageMaxHeight={100}/> : null) }


        { this.props.metaComponents.map((metaComp, i) => <ComponentItemWrapper
          item={{
            componentKey: 'meta-component',
            name: metaComp.layoutnm,
            pathName: 'meta-component',
            fixedProps : {
              mainId : metaComp.layoutid.toString(),
            },
          }}
          ice2fileType={metaComp.thumbnail}
          key={i}
          width={170}
          imageMaxHeight={100}/>)}
      </div>

      <div className="frc-footer">
        <button onClick={this.props.onUndo}>
          <UndoIcon style={{color:'inherit'}}/>
        </button>

        <button onClick={this.props.onApplyDirective}>
          영역 반영
        </button>

        <button onClick={this.props.onRedo}>
          <RedoIcon style={{color:'inherit'}}/>
        </button>
      </div>
    </div>
  }
}
