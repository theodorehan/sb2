import React, {Component} from 'react';
import PropTypes from 'prop-types';
import ReactDom from 'react-dom';
import isEmpty from 'lodash/isEmpty';
import moment from 'moment';

import $ from 'jquery';
import CurationIcon from 'material-ui/svg-icons/editor/bubble-chart';
import html2canvas from 'html2canvas';
import FragmentRemoteControl from './FragmentRemoteControl';
import get from 'lodash/get';

import {
  collisionDetectAllUseBoundingClientRect,
  filterHasEmptyChildren,
  classificationBloodLine,
  zContextRace,
  checkItIsBoundary,
} from '../../../../../../utils/ElementsFindByCoordinate';
import {
  Tooltip,
  Icon,
} from 'antd';

import './style.scss';

import {
  COMPONENT_PATH_ATTRIBUTE,
  findDirectiveRootNode,
  TEMPLATE_JSON_DIRECTIVE_SIGN,
  DIRECTIVE_POS_ATTR_SIGN,
  getDirectiveNodeByPos,
  getDirectiveNodeName,
} from '../../../../../../Core/Helper/VueSupports';


const TEMPLATE_PATH_COMPONENT_KEY_MAP = {};

/**
 * OverlayInterfaceLayer
 * 사용자의 마우스 입력 이벤트를 받는 레이어
 * 마우스에 관련한 이벤트를 감지하고 해당 좌표에 존재하는 DOM을 찾아내는 레이어이다.
 */
export default class OverlayInterfaceLayer extends Component {
  static contextTypes = {
    events : PropTypes.object,
    intl : PropTypes.object,
  }


  static propTypes = {
    onWheel : PropTypes.func,

    fragmentInfoMap : PropTypes.object,
    historyMap : PropTypes.object,

    onMouseMoveRaytracingResult : PropTypes.func,
    onMouseDBClickRaytracingResult: PropTypes.func,
    onMouseClickRaytracingResult: PropTypes.func,
    onContextMenuRaytracingResult : PropTypes.func,
    onUndoDirective : PropTypes.func,
    onRedoDirective : PropTypes.func,
    getNodeEditingRules : PropTypes.func,

    draggableAimed : PropTypes.bool,
    dropFocusing : PropTypes.bool,
    onComponentDrop : PropTypes.func,
    onComponentDragging : PropTypes.func,

    // excludeRect가 지정되면 해당 부분을 제외하고 OverlayLayer를 덮는다. // 일단 텍스트 에디팅 전용
    excludeRect : PropTypes.object,


    getNodeEditingRules : PropTypes.func,
    clickComponentHandle : PropTypes.func,
    metaComponents : PropTypes.array,

    directiveRootDoms : PropTypes.array,
    onClickDirectiveApply : PropTypes.func,

    iframeScrollTop : PropTypes.number,
  }

  constructor(props){
    super(props);

    this.wrappedOnDragDrop = this.onDragDrop.bind(this);
    this.wrappedOnDragMove = this.onDragMove.bind(this);

    this.directivePreviewDom = {};

    this.state = {
      fragmentRemoteControlStore : {

      },
    }


    this.remoteControls = {};
  }

  getDirectiveNodeDOM(){

  }

  raytraceInIframe(rootDom, left, top){


    // 해당 위치를 감싸고 있는 요소들을 찾아냄
    let collisions =
      collisionDetectAllUseBoundingClientRect(rootDom, left, top);
    // console.log('1', collisions)

    // console.log(collisions);
    // console.log('collisions', collisions);


    //
    let lastDescendants =
      classificationBloodLine(collisions, left, top);
    // console.log('2' ,lastDescendants)


    // console.log('lastDescendants  ', lastDescendants);

    let winner = zContextRace(lastDescendants);

    // console.log('winner =', winner)

    return winner;
  }

  getRevisedWithIframePoint(x,y){
    let iframeBoundingRect = this.props.iframeBoundingRect;

    let interpolatedLeft = x - iframeBoundingRect.left;
    let interpolatedTop = y - iframeBoundingRect.top;

    return {x : interpolatedLeft, y : interpolatedTop};
  }

  raytrace(iframe, revisedPoint, boundingDom){
    let tracedDom = this.raytraceInIframe(boundingDom || iframe.contentWindow.document.body, revisedPoint.x, revisedPoint.y);


    // if( tracedDom ){
    //   let tracedDomRect = tracedDom.getBoundingClientRect();
    //
    //   console.log(tracedDomRect, clientX, clientY)
    //   if( tracedDomRect.left ){
    //   }
    //
    //   return {tracedDom};
    // } else if(boundingDom){
    //   return {tracedDom:null};
    // }

    return tracedDom;
  }


  /**
   * getNearbyRegion
   * @description 입력된 DOM을 기준으로 포인트의 위치에서 제일 가까운 지점값을 반환한다.
   * @param dom
   * @param revisedPoint
   * @return 'left' | 'right' | 'top' | 'bottom' | 'in' | null
   */
  getNearbyRegion(dom, revisedPoint){
    if( !dom ) return null;

    let rect = dom.getBoundingClientRect();


    if( checkItIsBoundary({
        left : rect.left,
        top : rect.top,
        width: rect.width,
        height : 40,
      }, revisedPoint.x, revisedPoint.y) ){

      return 'top';
    } else if (checkItIsBoundary({
        left : rect.left,
        top : rect.top + rect.height - 40,
        width: rect.width,
        height : 40,
      }, revisedPoint.x, revisedPoint.y)){

      return 'bottom';
    } else if (checkItIsBoundary({
        left : rect.left,
        top : rect.top,
        width: 40,
        height : rect.height,
      }, revisedPoint.x, revisedPoint.y)){

      // 임시 좌우 드롭 방지 처리
      // return 'left';
    } else if (checkItIsBoundary({
        left : rect.left + rect.width - 40,
        top : rect.top,
        width: 40,
        height : rect.height,
      }, revisedPoint.x, revisedPoint.y)){

      // 임시 좌우 드롭 방지 처리
      // return 'right';
    } else if(checkItIsBoundary({
        left : rect.left + (rect.width/4),
        top : rect.top + (rect.height/4),
        width: (rect.width/2),
        height : (rect.height/2),
      }, revisedPoint.x, revisedPoint.y)){

      return 'in';

      // In 은 일단 그리드 컴포넌트의 경우에만 허용하도록 한다.
      let helperComponentKey = dom.getAttribute('data-helper-component-key');
      if(
        helperComponentKey == 'core-system-grid' ||
        helperComponentKey == 'core-system-row' ||
        helperComponentKey == 'core-system-column' ||
        helperComponentKey == 'core-system-layer'
      ){

        return 'in';
      }
    }


    return null;


    /**
     * 가까운 포인트로 위치 선정 기법
     */

    {
      let rect = dom.getBoundingClientRect();
      let regionDistances = {

        left : this.getDistance(revisedPoint, {
          x : rect.left,
          y : rect.top + rect.height /2,
        }),
        right : this.getDistance(revisedPoint, {
          x : rect.left + rect.width,
          y : rect.top + rect.height /2,
        }),
        top : this.getDistance(revisedPoint, {
          x : rect.left +rect.width / 2,
          y : rect.top,
        }),
        bottom : this.getDistance(revisedPoint, {
          x : rect.left + rect.width /2,
          y : rect.top + rect.height,
        }),
        in : this.getDistance(revisedPoint, {
          x : rect.left + rect.width /2,
          y : rect.top + rect.height /2,
        }),
      }

      let regionKeys = Object.keys(regionDistances);
      let distanceMin = regionDistances[regionKeys[0]];
      let nearby = regionKeys[0];
      let currKey;
      for( let i = 1; i < regionKeys.length; i++ ){
        currKey = regionKeys[i];


        if(regionDistances[currKey] < distanceMin ){
          distanceMin = regionDistances[currKey];
          nearby = currKey;
        }

      }

      return nearby;
    }
  }

  getDistance(pointA, pointB){
    return Math.sqrt(Math.pow(pointA.x - pointB.x,2) + Math.pow(pointA.y - pointB.y,2));
  }


  onMouseLeave(e){
    this.props.onMouseLeave(e);
  }

  onMouseMove(e){
    let { clientX, clientY } = e.nativeEvent;
    let iframeWindow = this.props.iframe.contentWindow;

    if( !(iframeWindow.document && iframeWindow.document.readyState === 'complete') ){
      return;
    }

    let raytracedResult = this.raytrace(this.props.iframe, this.getRevisedWithIframePoint(clientX, clientY) );

    this.props.onMouseMoveRaytracingResult(raytracedResult);
  }

  onClick(e){
    let { clientX, clientY } = e.nativeEvent;
    let iframeWindow = this.props.iframe.contentWindow;

    if( !(iframeWindow.document && iframeWindow.document.readyState === 'complete') ){
      return;
    }

    let raytracedResult = this.raytrace(this.props.iframe, this.getRevisedWithIframePoint(clientX, clientY) );



    // console.log('click');

    this.props.onMouseClickRaytracingResult(raytracedResult, {
      x : clientX,
      y : clientY,
    });
  }

  onDBClick(e){
    let { clientX, clientY } = e.nativeEvent;
    let iframeWindow = this.props.iframe.contentWindow;

    if( !(iframeWindow.document && iframeWindow.document.readyState === 'complete') ){
      return;
    }

    let raytracedResult = this.raytrace(this.props.iframe, this.getRevisedWithIframePoint(clientX, clientY) );

    this.props.onMouseDBClickRaytracingResult(raytracedResult, {
      x : clientX,
      y : clientY,
    });
  }

  onContextMenu(e){
    let { clientX, clientY } = e.nativeEvent;
    let iframeWindow = this.props.iframe.contentWindow;

    if( !(iframeWindow.document && iframeWindow.document.readyState === 'complete') ){
      return;
    }
    e.preventDefault();

    let raytracedResult = this.raytrace(this.props.iframe, this.getRevisedWithIframePoint(clientX, clientY) );

    this.props.onContextMenuRaytracingResult(raytracedResult, {
      x : clientX,
      y : clientY,
    });

    return false;
  }

  onWheel(e){
    e.preventDefault();
    e.stopPropagation();
    this.props.onWheel(e);

    let iframeWindow = this.props.iframe.contentWindow;
    if( !(iframeWindow.document && iframeWindow.document.readyState === 'complete') ){
      return;
    }

    this.props.onWheel(e);
  }

  onDragDrop(data){
    if( !this.props.draggableAimed ) return;

    let { aimX, aimY } = data;
    let iframeWindow = this.props.iframe.contentWindow;

    if( !(iframeWindow.document && iframeWindow.document.readyState === 'complete') ){
      return;
    }

    if( data.draggableType === 'component' ){
      let { selectedDirectiveName, selectedDirectiveNode, dropFocusing } = this.props;

      // Bounding 선택된 Node 가 있을 때 드래그 영역을 선택된 Node 로 한정하여 raytrace 하도록한다.
      let boundingDOM;
      if( dropFocusing && selectedDirectiveNode) {
        boundingDOM = iframeWindow.document.querySelector(
          `[${TEMPLATE_JSON_DIRECTIVE_SIGN}="${selectedDirectiveName}"][${DIRECTIVE_POS_ATTR_SIGN}="${selectedDirectiveNode.pos}"],
        [${TEMPLATE_JSON_DIRECTIVE_SIGN}="${selectedDirectiveName}"] [${DIRECTIVE_POS_ATTR_SIGN}="${selectedDirectiveNode.pos}"]`
        )
      }

      let revisedPoint = this.getRevisedWithIframePoint(aimX, aimY);
      let raytracedResult = this.raytrace(this.props.iframe, revisedPoint, boundingDOM );


      /**
       * finalDom 구하기 raytracing 결과가 없을 때 boundingDOM을 최종DOM으로 선택하고,
       * boundingDOM 이 없다면 mainDirective 의 RootNode를 선택하도록 한다.
       **/
      let finalDom;
      if(!raytracedResult){
        if( boundingDOM ){
          finalDom = boundingDOM;
        } else {
          finalDom = iframeWindow.document.querySelector(
            `[${TEMPLATE_JSON_DIRECTIVE_SIGN}="${this.props.mainDirectiveName}"][${DIRECTIVE_POS_ATTR_SIGN}="${0}"],
        [${TEMPLATE_JSON_DIRECTIVE_SIGN}="${this.props.mainDirectiveName}"] [${DIRECTIVE_POS_ATTR_SIGN}="${0}"]`
          );
        }
      } else {
        finalDom = raytracedResult;
      }




      // 현재 포착된 요소를 포함하고 있는 컴포넌트의 루트 요소를 찾는다.
      let overComponentElement = $(finalDom).closest('[__directive_pos]');

      let nearByRegion = this.getNearbyRegion(overComponentElement[0], revisedPoint);
      if( nearByRegion ){
        this.props.onComponentDrop(finalDom, nearByRegion, data.draggableData);
      }
    }
  }

  onDragMove(data){
    if( !this.props.draggableAimed ) return;

    let { aimX, aimY } = data;
    let iframeWindow = this.props.iframe.contentWindow;

    if( !(iframeWindow.document && iframeWindow.document.readyState === 'complete') ){
      return;
    }

    if( data.draggableType === 'component' ){
      let { selectedDirectiveName, selectedDirectiveNode, dropFocusing } = this.props;

      // Bounding 선택된 Node 가 있을 때 드래그 영역을 선택된 Node 로 한정하여 raytrace 하도록한다.
      let boundingDOM;
      if( dropFocusing && selectedDirectiveNode) {
        boundingDOM = iframeWindow.document.querySelector(
          `[${TEMPLATE_JSON_DIRECTIVE_SIGN}="${selectedDirectiveName}"][${DIRECTIVE_POS_ATTR_SIGN}="${selectedDirectiveNode.pos}"],
        [${TEMPLATE_JSON_DIRECTIVE_SIGN}="${selectedDirectiveName}"] [${DIRECTIVE_POS_ATTR_SIGN}="${selectedDirectiveNode.pos}"]`
        )
      }
      let revisedPoint = this.getRevisedWithIframePoint(aimX, aimY);
      let raytracedResult = this.raytrace(this.props.iframe, revisedPoint, boundingDOM );

      /**
       * finalDom 구하기 raytracing 결과가 없을 때 boundingDOM을 최종DOM으로 선택하고,
       * boundingDOM 이 없다면 mainDirective 의 RootNode를 선택하도록 한다.
       **/
      let finalDom;
      if(!raytracedResult){
        if( boundingDOM ){
          finalDom = boundingDOM;
        } else {
          finalDom = iframeWindow.document.querySelector(
            `[${TEMPLATE_JSON_DIRECTIVE_SIGN}="${this.props.mainDirectiveName}"][${DIRECTIVE_POS_ATTR_SIGN}="${0}"],
        [${TEMPLATE_JSON_DIRECTIVE_SIGN}="${this.props.mainDirectiveName}"] [${DIRECTIVE_POS_ATTR_SIGN}="${0}"]`
          );
        }
      } else {
        finalDom = raytracedResult;
      }


      // 현재 포착된 요소를 포함하고 있는 컴포넌트의 루트 요소를 찾는다.
      let overComponentElement = $(finalDom).closest('[__directive_pos]');

      let nearByRegion = this.getNearbyRegion(overComponentElement[0], revisedPoint);

      if( nearByRegion ){
        this.props.onComponentDragging(finalDom, nearByRegion,  data.draggableData, revisedPoint );
      }else{
        this.props.onComponentDragging(null, null, null, revisedPoint);
      }
    }
  }

  onClickHandle(e, dom, editProp, key){
    e.preventDefault();
    e.stopPropagation();

    this.props.clickComponentHandle && this.props.clickComponentHandle(dom, editProp, key);
  }

  onRedo(directiveName){
    this.props.onRedoDirective(directiveName);
  }

  onUndo(directiveName){
    this.props.onUndoDirective(directiveName);
  }

  componentDidMount(){
    this.context.events.listen('DRAG_DROP', this.wrappedOnDragDrop);
    this.context.events.listen('DRAG_MOVE', this.wrappedOnDragMove);
  }

  componentWillUnmount(){
    this.context.events.removeListen('DRAG_DROP', this.wrappedOnDragDrop);
    this.context.events.removeListen('DRAG_MOVE', this.wrappedOnDragMove);
  }



  renderScheduledComponentMark(dom, rect){
    let start = dom.getAttribute('data-sc-start');
    let end = dom.getAttribute('data-sc-end');

    if( start || end ){

      let current = parseInt(moment(new Date()).format('YYYYMMDDHHmmss'));

      if( start ){
        if( parseInt(start) > current ){
          return <button>
            <span>
              <Icon type="schedule" />

              표시 예정
            </span>
          </button>
        }
      }

      if( end ){
        if( parseInt(end) < current ){
          return <button>
            <span>
              <Icon type="schedule" />
              만료됨
            </span>
          </button>
        }
      }


      return <button>
        <span>
          <Icon type="schedule" />
          스케쥴됨
        </span>
      </button>
    }


    return null;
  }


  renderComponentHandler(dom, componentKey, editRule, i){
    let {props, textEditable} = editRule || {};
    let propKeys = Object.keys(props || {});

    let rect = dom.getBoundingClientRect();

    return <div
      className="component-handler"
      key={i}
      style={{
        left : this.props.iframeBoundingRect.left + rect.right-10,
        top: rect.top+10+this.props.iframeBoundingRect.top,
      }}>
      <div className="handlers">
        {
          this.renderScheduledComponentMark(dom, rect)
        }



        {propKeys.map((key, i) => {
          let prop = props[key];

          switch( props[key].type ){
            case "curation-source":
              return <button key={i} onClick={(e)=> this.onClickHandle(e, dom, prop, key)} onContextMenu={(e)=> this.onClickHandle(e, dom, prop, key)}>

                <span>{prop.label}</span>
              </button>
            default:
              return null;
          }

        })}
      </div>
    </div>;
  }

  renderComponentHandlers(){
    let domComponents = [];
    let iframe = this.props.iframe;
    if(iframe){
      if( iframe.contentWindow && iframe.contentWindow.document.readyState === 'complete' ){

        domComponents = [];


        for(let i =0; i < this.props.directiveRootDoms.length; i++ ){
          domComponents.push.apply(domComponents, this.props.directiveRootDoms[i].querySelectorAll(`[${COMPONENT_PATH_ATTRIBUTE}]`))
        }
      } else {
        return [];
      }
    }


    // console.log(domComponents,COMPONENT_PATH_ATTRIBUTE, `[${COMPONENT_PATH_ATTRIBUTE}]`);
    return Array.prototype.map.call(domComponents, ((dom, i) => {



      let componentKey = dom.getAttribute('data-helper-component-key');
      let componentPos = dom.getAttribute(DIRECTIVE_POS_ATTR_SIGN);
      // 'componentSchedule';

      if( !componentKey ){
        let templatePath = dom.getAttribute(COMPONENT_PATH_ATTRIBUTE);
        componentKey = TEMPLATE_PATH_COMPONENT_KEY_MAP[templatePath];

        if( !componentKey ){
          componentKey = TEMPLATE_PATH_COMPONENT_KEY_MAP[templatePath] = templatePath.toLowerCase().replace('/src/components/','').replace(/\//g, '-').replace('.vue','');
        }
      }

      let editRule = this.props.getNodeEditingRules(componentKey);

      return this.renderComponentHandler(dom, componentKey, editRule, i);

      if( editRule ){
        if( isEmpty(editRule) ){
          return null;
        }

        return this.renderComponentHandler(dom, componentKey, editRule, i);
      } else {
        return null;
      }
    }))
  }

  getFrcHeight(name){
    if( this.remoteControls[name] ){
      return ReactDom.findDOMNode(this.remoteControls[name]).offsetHeight;
    }
    return 300;
  }

  renderDirectiveHandlers(){




    return this.props.directiveRootDoms.map((dom, i) => {

      let rect = dom.getBoundingClientRect();
      let directiveName = dom.getAttribute(TEMPLATE_JSON_DIRECTIVE_SIGN);
      let directiveHumanName = dom.getAttribute('data-fragment-name');

      if(this.props.selectedDirectiveName != directiveName ) return null;

      if( !this.props.fragmentInfoMap[directiveName] ) return null;

      let x = Math.max(rect.left + get(this.state, `fragmentRemoteControlStore.${directiveName}.x`, 10), 10);
      let y = Math.max(get(this.state, `fragmentRemoteControlStore.${directiveName}.y`, 10) + rect.top, 10);


      if( rect.top + rect.height < 0 ){
        y = -99999;
      }




      // 리모컨이 영역의 bottom 을 넘어가지 않도록 보정함.
      let rh = this.getFrcHeight(directiveName) + 10;
      // console.log(y, rect.top, this.props.iframeScrollTop,dom.offsetTop, '...', dom.offsetTop + rect.height - this.props.iframeScrollTop);
      if( y + rh > dom.offsetTop + rect.height - this.props.iframeScrollTop) {
        y = dom.offsetTop + rect.height - this.props.iframeScrollTop - rh;
      }

      if( y < 0 ){
        y = 0;
      }



      return <FragmentRemoteControl
        onApplyDirective={() => this.props.onClickDirectiveApply(directiveName)}
        ref={(frc) => this.remoteControls[directiveName] = frc}
        key={i}
        onRedo={() => this.onRedo(directiveName)}
        onUndo={() => this.onUndo(directiveName)}
        directiveHumanName={directiveHumanName}
        metaComponents={this.props.metaComponents}
        acceptComponents={this.props.fragmentInfoMap[directiveName].fragmentAcceptComponents}
        onMove={(dx,dy) => this.setState({
          fragmentRemoteControlStore : {
            ...this.state.fragmentRemoteControlStore,
            [directiveName] : {
              x : Math.max(get(this.state, `fragmentRemoteControlStore.${directiveName}.x`, 10) + dx, 0),
              y : Math.max(get(this.state, `fragmentRemoteControlStore.${directiveName}.y`, 10) + dy, 0),
            },
          },
        })}
        x={x}
        y={y}/>

    })
  }


  render(){
    this.directivePreviewDom = {};

    return <div
      className="overlay-interface-layer"
      onMouseMove={:: this.onMouseMove }
      onWheel={:: this.onWheel }
      onClick={:: this.onClick }
      onDoubleClick={:: this.onDBClick }
      onContextMenu={:: this.onContextMenu }
      onMouseLeave={:: this.onMouseLeave}>
      { this.renderComponentHandlers() }
      { this.renderDirectiveHandlers() }
    </div>
  }
}
