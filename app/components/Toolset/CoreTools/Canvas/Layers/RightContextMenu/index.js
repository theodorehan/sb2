import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Paper from 'material-ui/Paper';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import Divider from 'material-ui/Divider';
import ArrowDropRight from 'material-ui/svg-icons/navigation-arrow-drop-right';

import {
  TEMPLATE_JSON_DIRECTIVE_SIGN,
  DIRECTIVE_POS_ATTR_SIGN,
} from '../../../../../../Core/Helper/VueSupports';

import {
  createColumnNode,
  createRowNode,
  createGridNode,
  createLayerNode,
  modifyColumnSizingWithAppendingAndNewColumnProps,

  APPEND_NEW_COLUMN_BEFORE,
  APPEND_NEW_COLUMN_AFTER,
} from '../../../../../../Core/Helper/GridDirectiveModifyPolicing'

import './style.scss';

const MenuItemStyle = {
  lineHeight : '20px',
  minHeight:20,
  fontSize:14,
  padding: 5,
}

export default  class RightContextMenu extends Component {
  static propTypes = {
    onModifiedDirectiveNode : PropTypes.func,
    onDeleteSelectedNode : PropTypes.func,
    onApplyPart : PropTypes.func,
    onApplyDirectiveFull : PropTypes.func,
    onFragmentVersionManaging : PropTypes.func,
    onSaveAsTemplate : PropTypes.func,
    onTemplateManaging: PropTypes.func,


    rootNodeOfSelectedDirectiveNode : PropTypes.object,
    selectedDirectiveName : PropTypes.string,
    selectedDirectiveNode : PropTypes.object,
    selectingPoint : PropTypes.object,
    iframe : PropTypes.any,

    onRedo : PropTypes.func,
    onUndo : PropTypes.func,

    undoable : PropTypes.bool,
    redoable : PropTypes.bool,
  };

  static contextTypes = {
    intl : PropTypes.object,
  }

  constructor(props){
    super(props);
  }

  onDelete(){
    this.props.onDeleteSelectedNode();
  }


  onApplyPart(){
    this.props.onApplyPart();
  }

  onApplyDirectiveFull(){
    this.props.onApplyDirectiveFull();
  }

  onFragmentVersionManaging(){
    this.props.onFragmentVersionManaging();
  }

  onSaveAsTemplate(){
    this.props.onSaveAsTemplate();
  }

  onTemplateManaging(){
    this.props.onTemplateManaging();
  }

  onRedo(){
    this.props.onRedo();
  }

  onUndo(){
    this.props.onUndo();
  }

  getCustomMenuList(){
    let mItems = [];
    let nodeTag = this.props.selectedDirectiveNode.tag;
    let selectedNode = this.props.selectedDirectiveNode;


    // mItems.push({
    //   text : 'Select to Parent',
    //   onClick : () => this.props.onJumpToParent(),
    // });

    // mItems.push({
    //   text : 'Open GridEditor',
    //   onClick : () => this.props.onOpenGridEditor(),
    // });

    mItems.push({
      text : this.context.intl.formatMessage({id: 'app.common.component-setting'}),
      onClick : () => {
        this.props.onClose();
        this.props.onOpenSettings()
      },
    });

    return mItems;

    if( nodeTag === 'core-system-grid' ){
      mItems.push({
        text : 'Create row at first',
        onClick : () => {
          selectedNode.prependChild(createRowNode());

          this.props.onModifiedDirectiveNode({
            exportedJSON : this.props.rootNodeOfSelectedDirectiveNode.exportToJSON(),
            directiveName : this.props.selectedDirectiveName,
          })
        },
      });

      mItems.push({
        text : 'Create row at last',
        onClick : () => {
          selectedNode.appendChild(createRowNode());

          this.props.onModifiedDirectiveNode({
            exportedJSON : this.props.rootNodeOfSelectedDirectiveNode.exportToJSON(),
            directiveName : this.props.selectedDirectiveName,
          })
        },
      });
    } else if( nodeTag === 'core-system-row' ){
      mItems.push({
        text : 'Create column at first',

        onClick : () => {
          let columnProps = modifyColumnSizingWithAppendingAndNewColumnProps(APPEND_NEW_COLUMN_BEFORE,0, selectedNode.children);
          selectedNode.prependChild(createColumnNode(columnProps));

          this.props.onModifiedDirectiveNode({
            exportedJSON : this.props.rootNodeOfSelectedDirectiveNode.exportToJSON(),
            directiveName : this.props.selectedDirectiveName,
          })
        },
      });

      mItems.push({
        text : 'Create column at last',

        onClick : () => {
          let columnProps = modifyColumnSizingWithAppendingAndNewColumnProps(APPEND_NEW_COLUMN_AFTER,selectedNode.children.length -1, selectedNode.children);
          selectedNode.appendChild(createColumnNode(columnProps));

          this.props.onModifiedDirectiveNode({
            exportedJSON : this.props.rootNodeOfSelectedDirectiveNode.exportToJSON(),
            directiveName : this.props.selectedDirectiveName,
          })
        },
      });

      mItems.push({
        text : 'Create row before',

        onClick : () => {
          let nodeIndex = parseInt(selectedNode.pos.split('.').pop());

          selectedNode.parent.appendChildBefore(nodeIndex, createRowNode());

          this.props.onModifiedDirectiveNode({
            exportedJSON : this.props.rootNodeOfSelectedDirectiveNode.exportToJSON(),
            directiveName : this.props.selectedDirectiveName,
          })
        },
      });

      mItems.push({
        text :  'Create row after',

        onClick : () => {
          let nodeIndex = parseInt(selectedNode.pos.split('.').pop());

          selectedNode.parent.appendChildAfter(nodeIndex, createRowNode());

          this.props.onModifiedDirectiveNode({
            exportedJSON : this.props.rootNodeOfSelectedDirectiveNode.exportToJSON(),
            directiveName : this.props.selectedDirectiveName,
          })
        },
      });
    } else if( nodeTag === 'core-system-column' ){

      mItems.push({
        text : 'Create column before',

        onClick : () => {
          let nodeIndex = parseInt(selectedNode.pos.split('.').pop());
          let columnProps = modifyColumnSizingWithAppendingAndNewColumnProps(APPEND_NEW_COLUMN_BEFORE,nodeIndex, selectedNode.parent.children);
          selectedNode.parent.appendChildBefore(nodeIndex, createColumnNode(columnProps));

          this.props.onModifiedDirectiveNode({
            exportedJSON : this.props.rootNodeOfSelectedDirectiveNode.exportToJSON(),
            directiveName : this.props.selectedDirectiveName,
          })
        },
      });

      mItems.push({
        text :  'Create column after',

        onClick : () => {
          let nodeIndex = parseInt(selectedNode.pos.split('.').pop());
          let columnProps = modifyColumnSizingWithAppendingAndNewColumnProps(APPEND_NEW_COLUMN_AFTER,nodeIndex, selectedNode.parent.children);
          selectedNode.parent.appendChildAfter(nodeIndex, createColumnNode(columnProps));

          this.props.onModifiedDirectiveNode({
            exportedJSON : this.props.rootNodeOfSelectedDirectiveNode.exportToJSON(),
            directiveName : this.props.selectedDirectiveName,
          })
        },
      });

      mItems.push({
        text :  'Create Grid',

        onClick : () => {

          selectedNode.appendChild(createGridNode());

          this.props.onModifiedDirectiveNode({
            exportedJSON : this.props.rootNodeOfSelectedDirectiveNode.exportToJSON(),
            directiveName : this.props.selectedDirectiveName,
          })
        },
      });
    }

    return mItems;
  }

  renderMenuItems(menuList){
    return menuList.map(
      (item, i) =>
        <MenuItem
          key={i}
          style={MenuItemStyle}
          primaryText={item.text}
          secondaryText={item.secondaryText}
          onClick={item.onClick}
          menuItems={item.children && this.renderMenuItems(item.children)}
          rightIcon={item.children  && <ArrowDropRight />}
        />
    )
  }

  render(){
    let { selectedDirectiveName, selectedDirectiveNode, selectingPoint, iframe } = this.props;
    let iframeWindow = iframe.contentWindow;
    if( !(iframeWindow.document && iframeWindow.document.readyState === 'complete' && selectedDirectiveNode) ){
      return <div className="floating-right-context-menu"></div>;
    }

    let dom = iframeWindow.document.querySelector(
      `[${TEMPLATE_JSON_DIRECTIVE_SIGN}="${selectedDirectiveName}"][${DIRECTIVE_POS_ATTR_SIGN}="${selectedDirectiveNode.pos}"],
      [${TEMPLATE_JSON_DIRECTIVE_SIGN}="${selectedDirectiveName}"] [${DIRECTIVE_POS_ATTR_SIGN}="${selectedDirectiveNode.pos}"]`
    )

    let iframeBoundingRect = this.props.iframeBoundingRect;


    let style = {
      left : selectingPoint.x - iframeBoundingRect.left + iframe.offsetLeft,
      top : selectingPoint.y - iframeBoundingRect.top + iframe.offsetTop,
    };

    return <div className="floating-right-context-menu" style={style}>
      <Paper style={{display:'inline-block'}}>
        <Menu desktop={true} width={256}>
          { this.renderMenuItems(this.getCustomMenuList()) }
          <MenuItem style={MenuItemStyle} primaryText={this.context.intl.formatMessage({id : 'app.common.delete-component'})}  onClick={:: this.onDelete}/>
          <MenuItem style={MenuItemStyle} primaryText={this.context.intl.formatMessage({id : 'app.canvas.apply-directive-full'})}  onClick={:: this.onApplyDirectiveFull}/>
          <MenuItem style={MenuItemStyle} primaryText={this.context.intl.formatMessage({id : 'app.canvas.fragment-version-managing'})}  onClick={:: this.onFragmentVersionManaging}/>
          <MenuItem style={MenuItemStyle} primaryText={this.context.intl.formatMessage({id : 'app.canvas.save-as-template'})}  onClick={:: this.onSaveAsTemplate}/>
          <MenuItem style={MenuItemStyle} primaryText={this.context.intl.formatMessage({id : 'app.canvas.show-template-list'})}  onClick={:: this.onTemplateManaging}/>


          {/*<MenuItem style={MenuItemStyle} primaryText={this.context.intl.formatMessage({id : 'app.canvas.apply-directive-part'})}  onClick={:: this.onApplyPart}/>*/}

          <Divider />

          <MenuItem
            style={MenuItemStyle}
            primaryText="Undo"
            // secondaryText="Alt+Shift+5"
            disabled={!this.props.undoable}
            onClick={:: this.onUndo }/>
          <MenuItem
            style={MenuItemStyle}
            primaryText="Redo"
            // secondaryText="&#8984;."
            disabled={!this.props.redoable}
            onClick={:: this.onRedo }/>

          {/*<MenuItem style={MenuItemStyle} primaryText="Copy" secondaryText="&#8984;," />*/}
          {/*<MenuItem style={MenuItemStyle} primaryText="Cut" secondaryText="&#8984;," />*/}
          {/*<MenuItem style={MenuItemStyle} primaryText="Paste" secondaryText="&#8984;," />*/}
          {/*<MenuItem style={MenuItemStyle} primaryText="Clone" secondaryText="&#8984;," />*/}

          {/*<Divider />*/}
          {/*<MenuItem*/}
            {/*style={MenuItemStyle}*/}
            {/*primaryText="Custom: 1.2"*/}
            {/*rightIcon={<ArrowDropRight />}*/}
            {/*menuItems={[*/}
              {/*<MenuItem*/}
                {/*primaryText="Show"*/}
                {/*rightIcon={<ArrowDropRight />}*/}
                {/*menuItems={[*/}
                  {/*<MenuItem style={MenuItemStyle} primaryText="Show Level 2" />,*/}
                  {/*<MenuItem style={MenuItemStyle} primaryText="Grid lines" checked={true} />,*/}
                  {/*<MenuItem style={MenuItemStyle} primaryText="Page breaks" insetChildren={true} />,*/}
                  {/*<MenuItem style={MenuItemStyle} primaryText="Rules" checked={true} />,*/}
                {/*]}*/}
              {/*/>,*/}
              {/*<MenuItem style={MenuItemStyle} primaryText="Grid lines" checked={true} />,*/}
              {/*<MenuItem style={MenuItemStyle} primaryText="Page breaks" insetChildren={true} />,*/}
              {/*<MenuItem style={MenuItemStyle} primaryText="Rules" checked={true} />,*/}
            {/*]}*/}
          {/*/>*/}
          {/*<Divider />*/}
          <MenuItem style={MenuItemStyle} primaryText={this.context.intl.formatMessage({id:'app.common.close'})} onClick={ this.props.onClose }/>
        </Menu>
      </Paper>
    </div>
  }
}
