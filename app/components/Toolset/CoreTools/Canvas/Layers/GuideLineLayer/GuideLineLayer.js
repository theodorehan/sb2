import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import './style.scss';

export default class GuideLineLayer extends Component {
  static propTypes = {
    iframe : PropTypes.object,
    directiveRootDoms : PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
  }

  constructor(props){
    super(props);
  }


  renderHorizontalLine(y, colorClass, key){
    return <div className={classnames('line h', colorClass)} style={{top:y}} key={key}></div>
  }

  renderVerticalLine(x, colorClass, key){
    return <div className={classnames('line v', colorClass)} style={{left:x}} key={key}></div>
  }

  renderText(x,y, text, colorClass, key){
    return <div className={classnames('text')} style={{left:x, top:y}} key={key}>
      {text}
    </div>
  }


  renderDirectiveGuide(directiveDOM, iframeRect, i){
    let lines = [];

    let bb = directiveDOM.getBoundingClientRect();
    return <div key={i}>
      {this.renderHorizontalLine(bb.top + iframeRect.top, 'grid', `${i}-1`)}
      {this.renderHorizontalLine(bb.bottom, 'grid', `${i}-2`)}
      {this.renderVerticalLine(bb.left + iframeRect.left, 'grid', `${i}-3`)}
      {this.renderVerticalLine(bb.right + iframeRect.left, 'grid', `${i}-4`)}
      {this.renderText(bb.left + iframeRect.left + 50, bb.top + 50 , 'Editable Grid Area', `${i}-5`)}
    </div>
    // lines.push(this.renderHorizontalLine(bb.top + iframeRect.top, 'grid', `${i}-1`));
    // lines.push(this.renderHorizontalLine(bb.bottom, 'grid', `${i}-2`));
    // lines.push(this.renderVerticalLine(bb.left + iframeRect.left, 'grid', `${i}-3`));
    // lines.push(this.renderVerticalLine(bb.right + iframeRect.left, 'grid', `${i}-4`));
    //
    // lines.push( this.renderText(bb.left + iframeRect.left, bb.top, 'Root Grid', `${i}-5`));



    return lines;
  }


  render(){
    let iframeWindow = this.props.iframe.contentWindow;
    try {
      if( !(iframeWindow.document && iframeWindow.document.readyState === 'complete') ){
        return <div className="guide-line-layer"></div>;
      }
    } catch(e){
      return  <div className="guide-line-layer"></div>;
    }


    let document = iframeWindow.document;
    let iframeRect = {
      left : this.props.iframe.offsetLeft,
      top : this.props.iframe.offsetTop,
    };


    // console.log(roots);

    return <div className="guide-line-layer">
      { Array.prototype.map.call(this.props.directiveRootDoms, (dom, i)=> this.renderDirectiveGuide(dom, iframeRect, i))}
      {/*{this.renderVerticalLine('50%')}*/}
    </div>
  }
}
