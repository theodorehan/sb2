import React, {Component} from 'react';
import PropTypes from 'prop-types';

import {
  isVueComponent,
  getVueComponentInfo,
} from '../../../../../../Core/Helper/VueSupports';

import './style.scss';

class FocusingLabel extends Component {

  constructor(props){
    super(props);
  }

  stylePositioning(info, foundSubEditable){
    let style = {};

    let iframeRect = this.props.iframeBoundingRect;
    let focusDOM = this.props.mouseHoverIFrameDOM;

    if( foundSubEditable ){
      focusDOM = this.props.mouseHoverIFrameDOMReal;
    }

    let rect = focusDOM.getBoundingClientRect();

    style.left = iframeRect.left + rect.left - 1;
    if( rect.top > 21 ){
      style.top = iframeRect.top + rect.top - 21;
    }



    return style;
  }


  renderInfoBox(info, foundSubEditable){

    if( foundSubEditable ){
      let {connectProp} = foundSubEditable;

      return <span className="info-box">
      { info.name } > { info.editRules.props[connectProp].label }
    </span>;
    }


    return <span className="info-box">
      { info.name }
    </span>;
  }


  render(){
    if (!this.props.mouseHoverIFrameDOM) return <div></div>;
    let iframeWindow = this.props.iframe.contentWindow;
    if( !(iframeWindow.document && iframeWindow.document.readyState === 'complete') ){
      return <div></div>;
    }


    let targetDOM = this.props.mouseHoverIFrameDOM;
    let realTargetedDOM = this.props.mouseHoverIFrameDOMReal;
    let info = {};
    if( isVueComponent(targetDOM) ){
      info = getVueComponentInfo(targetDOM);
    } else {
      info.name = targetDOM.nodeName;
    }

    let foundSubEditable;
    if( info.editRules && info.editRules.textEditable ){
      let editables = info.editRules.textEditable;

      for(let i = 0; i < editables.length; i++ ){
        if(targetDOM.querySelector(editables[i].target) == realTargetedDOM ) {
          foundSubEditable = editables[i];
          break;
        }
      }
    }



    return <div
      className='mouse-overed-element-focusing-label'
      style={this.stylePositioning(info, foundSubEditable)}>

      { this.renderInfoBox(info, foundSubEditable) }
    </div>
  }
}


export default FocusingLabel;
