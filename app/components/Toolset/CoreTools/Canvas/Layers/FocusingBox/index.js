import React, {Component} from 'react';


import './style.scss';


import {
  isVueComponent,
  getVueComponentInfo,
} from '../../../../../../Core/Helper/VueSupports';


class FocusingBox extends Component {

  constructor(props){
    super(props);
  }


  render(){
    if (!this.props.mouseHoverIFrameDOM) return <div></div>;
    let iframeWindow = this.props.iframe.contentWindow;
    if( !(iframeWindow.document && iframeWindow.document.readyState === 'complete') ){
      return <div></div>;
    }

    let iframeRect = this.props.iframeBoundingRect;
    let rect,foundSubEditable;

    let targetDOM = this.props.mouseHoverIFrameDOM;
    let realTargetedDOM = this.props.mouseHoverIFrameDOMReal;
    let info = {};
    if( isVueComponent(targetDOM) ){
      info = getVueComponentInfo(targetDOM);
    } else {
      info.name = targetDOM.nodeName;
    }

    if( info.editRules && info.editRules.textEditable ){
      let editables = info.editRules.textEditable;

      for(let i = 0; i < editables.length; i++ ){
        if(targetDOM.querySelector(editables[i].target) == realTargetedDOM ) {
          foundSubEditable = editables[i];
          break;
        }
      }
    }

    if( foundSubEditable ){
      rect = this.props.mouseHoverIFrameDOMReal.getBoundingClientRect();
    } else {
      rect = this.props.mouseHoverIFrameDOM.getBoundingClientRect();
    }




    return <div
      className='mouse-overed-element-focusing-box'
      style={{
        left : iframeRect.left + rect.left,
        top: iframeRect.top + rect.top,
        width : rect.width,
        height : rect.height,
      }}>
    </div>
  }
}


export default FocusingBox;
