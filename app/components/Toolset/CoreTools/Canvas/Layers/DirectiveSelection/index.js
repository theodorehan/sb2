import React , { Component } from 'react';
import PropTypes from 'prop-types';
import './style.scss';
import classnames from 'classnames';
import GridIcon from 'material-ui/svg-icons/action/view-quilt';
import SettingIcon from 'material-ui/svg-icons/action/settings';
import ParentIcon from 'material-ui/svg-icons/image/center-focus-strong';
import { Tooltip } from 'antd';

import Draggable from '../../../../../../Core/Components/Draggable';

import VirticalAlignBottomIcon from 'material-ui/svg-icons/editor/vertical-align-bottom';
import KeybordTabIcon from 'material-ui/svg-icons/hardware/keyboard-tab';
import AddIcon from 'material-ui/svg-icons/content/add';
import CloseIcon from 'material-ui/svg-icons/navigation/close';
import DeleteIcon from 'material-ui/svg-icons/action/delete';
import DragHandleIcon from 'material-ui/svg-icons/editor/drag-handle';
import SwapVerticalCircleIcon from 'material-ui/svg-icons/action/swap-vertical-circle';

import {
  findDirectiveRootNode,
  TEMPLATE_JSON_DIRECTIVE_SIGN,
  DIRECTIVE_POS_ATTR_SIGN,
  getDirectiveNodeByPos,
  getDirectiveNodeName,
} from '../../../../../../Core/Helper/VueSupports';


function getNodeIcon(node){



  switch( node && node.tag ){
    case "core-system-grid":
      return <i className="fa fa-table"/>;
    case "core-system-row":
      return <i className="fa fa-align-justify"/>;
    case "core-system-column":
      return <i className="fa fa-columns"/>;
    case "core-system-layer":
      return <i className="fa fa-clone"/>;
    default:
      return <i className="fa fa-cubes"/>;
  }
}


class DirectiveSelection extends Component {
  static propTypes = {
    iframe : PropTypes.object,
    graphTree: PropTypes.object,
    graphLoadingState : PropTypes.string,
    selectedIFrameDOM : PropTypes.object,
    directiveName : PropTypes.string,
    directivesMap : PropTypes.object,
    directiveNode : PropTypes.object,
    directiveNodePos : PropTypes.string,
    onJumpToParent : PropTypes.func,
    onCloseSelected : PropTypes.func,
    onOpenGridEditor : PropTypes.func,
    mode : PropTypes.string,
  }

  static contextTypes = {
    toolSystem: PropTypes.object,
    elastic: PropTypes.object,
    zone : PropTypes.object,
    intl : PropTypes.object,
  }

  constructor(props){
    super(props);


    this.state = {
      horizontalStretchingAdder : 0,
      verticalStretchingAdder : 0,
    };
  }

  onStartDragComponent(e){
    console.log('started drag')
  }

  onClickGrid(){
    this.props.onOpenGridEditor();
  }

  onClickParent(){
    this.props.onJumpToParent();
  }

  onClickSetting(){
    this.props.onOpenSettings();
  }

  onClickClose(){
    this.props.onCloseSelected();
  }

  onVerticalStretching(x,y, deltaX, deltaY){
    this.setState({
      verticalStretchingAdder : deltaY + this.state.verticalStretchingAdder,
    });


  }

  onVerticalStretchingStop(){
    this.setState({
      horizontalStretchingAdder : 0,
    })
  }

  onHorizontalStretching(x,y, deltaX, deltaY){
    this.setState({
      horizontalStretchingAdder : deltaX + this.state.horizontalStretchingAdder,
    });
  }

  onHorizontalStretchingStop(){
    this.setState({
      verticalStretchingAdder : 0,
    })
  }

  renderText(x,y, text, colorClass){
    return <div className={classnames('text')} style={{left:x, top:y}}>
      {text}
    </div>
  }

  renderHorizontalLine(x1, w, y, key, colorClass, i){
    return <div className={classnames('line h', colorClass)} style={{left:x1, width:w, top:y}} key={key}></div>
  }

  renderVerticalLine(y1, h, x, key,colorClass){
    return <div className={classnames('line v', colorClass)} style={{top:y1, height:h, left:x}} key={key}></div>
  }


  renderBox(rect, iframeRect){
    let lines = [];

    lines.push(this.renderHorizontalLine(rect.left + iframeRect.left , rect.width, rect.top + iframeRect.top, '1'));
    lines.push(this.renderHorizontalLine(rect.left + iframeRect.left , rect.width, rect.bottom + iframeRect.top, '2'));
    lines.push(this.renderVerticalLine(rect.top + iframeRect.top , rect.height, rect.left + iframeRect.left , '3'));
    lines.push(this.renderVerticalLine(rect.top + iframeRect.top , rect.height, rect.right + iframeRect.right, '4'));

    // lines.push(this.renderText(rect.left, rect.top - 20, 'selected'));

    return lines;
  }

  renderOptions(rect, iframeRect){

    let style = {
      left : this.props.selectingPoint.x - iframeRect.left,
      top : this.props.selectingPoint.y -  iframeRect.top,
    }

    // let style = {
    //   left :iframeRect.left + rect.left,
    //   top : iframeRect.top + rect.top - 35,
    // }

    if( style.top < iframeRect.top + 5 ){
      style.top = 5 + iframeRect.top;
    }


    if( this.props.showContextMenu ){
      return <div className="options" style={style}>
        { !this.props.mode && <div className={classnames('text')}>
          {getNodeIcon(this.props.directiveNode)}
          {getDirectiveNodeName(this.props.directiveNode)}
        </div>}
      </div>
    } else {
      return <div className="options" style={style}>
        {!this.props.mode  && <div className={classnames('text')}>
          {getNodeIcon(this.props.directiveNode)}
          {getDirectiveNodeName(this.props.directiveNode)}
        </div>}

        {/*<button onClick={:: this.onClickSetting }>*/}
          {/*<Draggable*/}
            {/*onDragBegin={:: this.onStartDragComponent }*/}
            {/*_draggableType='component-move'*/}
            {/*_draggableData={{data:''}}*/}
            {/*component={<Tooltip title={this.context.intl.formatMessage({id: 'app.common.move-component'})} placement='top'>*/}
            {/*<i><SwapVerticalCircleIcon/></i>*/}
          {/*</Tooltip>}>*/}

          {/*</Draggable>*/}
        {/*</button>*/}

        { !this.props.mode  && <button onClick={:: this.onClickParent }>
          <Tooltip title="Jump to parent" placement='top'>
            <i><ParentIcon style={{width:18, height:18}}/></i>
          </Tooltip>
        </button> }
        { !this.props.mode && <button onClick={:: this.onClickGrid }>
          <Tooltip title="Open grid editor" placement='top'>
            <i><GridIcon style={{width:18, height:18}}/></i>
          </Tooltip>
        </button> }
        <button onClick={:: this.onClickSetting }>
          <Tooltip title={this.context.intl.formatMessage({id: 'app.common.component-setting'})} placement='top'>
            <i><SettingIcon style={{width:18, height:18}}/></i>
          </Tooltip>
        </button>
        { <button onClick={:: this.onClickClose }>
          <i><CloseIcon style={{width:18, height:18}}/></i>
        </button> }
      </div>
    }
  }

  renderTopDraggable(){


    let directiveName = this.props.directiveName;
    let directiveNode = this.props.directiveNode;
    let directiveNodePos = this.props.directiveNodePos;
    let directiveNodeObject = directiveNode.exportToJSON();
    // console.log(directiveNodePos)

    return <Draggable
      onDragMove={:: this.onVerticalStretching}
      onDragEnd={:: this.onVerticalStretchingStop}
      className="floating-opt top-draggable"
      _draggableData={{
        dropSource : 'canvas',


        sourcePosition : directiveNodePos,
        sourceDirectiveName : directiveName,
        source : directiveNodeObject,
      }}
      _draggableType='component'
      invisibleGhost={true}
      component={ ()=><DragHandleIcon color="#333" style={{width:20,height:20}}/>}/>;
  }

  renderSideOptionVerticalStretch(){

    return <Draggable
      onDragMove={:: this.onVerticalStretching}
      onDragEnd={:: this.onVerticalStretchingStop}
      className="floating-opt height-stretch"
      _draggableData={{}}
      _draggableType='none'
      invisibleGhost={true}
      component={ ()=><VirticalAlignBottomIcon color="#333" style={{width:15,height:15}}/>}/>;
  }

  renderSideOptionHorizontalStretch(){

    return <Draggable
      onDragMove={:: this.onHorizontalStretching}
      onDragEnd={:: this.onHorizontalStretchingStop}
      className="floating-opt width-stretch"
      _draggableData={{}}
      _draggableType='none'
      invisibleGhost={true}
      component={ ()=><KeybordTabIcon color="#333" style={{width:15, height:15}}/>}/>;
  }

  renderSideOptionAddRow(){

    return <button className="floating-opt row-add">
      <AddIcon color="#333" style={{width:15,height:15}}/>
    </button>
  }

  renderSideOptionDelete(){

    return <button className="floating-opt delete" onClick={() => this.props.onDeleteSelectedNode()}>
      <DeleteIcon color="#333" style={{width:15,height:15}}/>
    </button>
  }

  renderSideOptionAddColumn(){

    return <button className="floating-opt column-add">
      <AddIcon color="#333" style={{width:15,height:15}}/>
    </button>
  }

  renderSideOptions(rect, iframeRect){
    let {tag} = this.props.directiveNode;
    let style = {
      left : rect.left + iframeRect.left,
      top : rect.top + iframeRect.top,
      width : rect.width,
      height : rect.height,
    };

    return <div className="side-option-container" style={style}>
      { this.renderTopDraggable() }
      { this.renderSideOptionVerticalStretch() }
      { this.renderSideOptionVerticalStretch() }
      { this.renderSideOptionHorizontalStretch() }
      { tag === 'core-system-row' && this.renderSideOptionAddRow() }
      { tag === 'core-system-column' &&  this.renderSideOptionAddColumn() }
      { this.renderSideOptionDelete() }
    </div>
  }

  render(){
    let { directiveName, directiveNodePos, iframe } = this.props;

    let iframeWindow = iframe.contentWindow;
    if( !(iframeWindow.document && iframeWindow.document.readyState === 'complete' && directiveNodePos) ){
      return <div className="modifing-layer"></div>;
    }

    let iframeBRect = this.props.iframeBoundingRect;;

    let dom = iframeWindow.document.querySelector(
      `[${TEMPLATE_JSON_DIRECTIVE_SIGN}="${directiveName}"][${DIRECTIVE_POS_ATTR_SIGN}="${directiveNodePos}"],
      [${TEMPLATE_JSON_DIRECTIVE_SIGN}="${directiveName}"] [${DIRECTIVE_POS_ATTR_SIGN}="${directiveNodePos}"]`
    )

    if( !dom ){
      return <div></div>;
    }

    let box = dom.getBoundingClientRect();
    let iframeRect = {
      left : iframeBRect.left,
      right :  iframeBRect.left,
      top : iframeBRect.top,
    };

    return <div className="modifing-layer">
      { this.renderBox(box, iframeRect) }
      { !this.props.mode && this.renderOptions(box, iframeRect) }
      { this.renderSideOptions(box, iframeRect) }
    </div>;
  }
}

export default DirectiveSelection;
