import React, {Component} from 'react';
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import classnames from 'classnames';
import TextField from 'material-ui/TextField';
import Toggle from 'material-ui/Toggle';
import set from 'lodash/set';
import cloneDeep from 'lodash/cloneDeep';

import NavigationMenuIcon from 'material-ui/svg-icons/navigation/menu';
import NavigationCloseIcon from 'material-ui/svg-icons/navigation/close';
import ImageGrainIcon from 'material-ui/svg-icons/image/grain';
import ImagePaletteIcon from 'material-ui/svg-icons/image/palette';
import ImageCenterFocusWeakIcon from 'material-ui/svg-icons/image/center-focus-weak';
import EditorChartIcon from 'material-ui/svg-icons/editor/insert-chart';
import HardwareCastConnectedIcon from 'material-ui/svg-icons/hardware/cast-connected';
import ActionCodeIcon from 'material-ui/svg-icons/action/code';

import beautifyJson from 'json-beautify';
import flatten from 'lodash/flatten';
import {notification, Icon} from 'antd';

import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import {VueNuxtNode} from 'abstract-component-node';

import Tool from '../common/Tool';

import GuideLineLayer from './Layers/GuideLineLayer';

import OverlayInterfaceLayer from './Layers/OverlayInterfaceLayer';
import FocusingBox from './Layers/FocusingBox';
import FocusingLabel from './Layers/FocusingLabel';
import NodeGraphLayer from './Layers/NodeGraph';
import DirectiveSelection from './Layers/DirectiveSelection';
import ComponentDragDropDirectiveLayer from './Layers/ComponentDragDropDirective';
import RemoteControl from './Layers/RemoteControl';
import RightContextMenu from './Layers/RightContextMenu';
import WysiwigEditor from './Layers/WysiwigEditor';

import BalancedItemTray from '../../../BalancedItemTray';

import ComponentDropInitializer from './utils/ComponentDropInitializer';


import {
  CreateOpenActionSheet as CreateGridEditorOpenActionSheet,
} from '../GridEditor';

import {
  CreateOpenActionSheet as CreateCodeEditorOpenActionSheet,
  ToolKey as CodeEditorToolKey,
} from '../CodeEditor';


import {
  CreateOpenActionSheet as CreateCuratorOpenActionSheet,
  ToolKey as CuratorToolKey,
} from '../Curator/Constants';

import {
  CreateOpenActionSheet as CreateSettingsOpenActionSheet,
} from '../Settings';


import {
  isVueComponent,
  getVueComponentInfo,
  findRootDirectiveVueComponent,
} from '../../../../Core/Helper/VueSupports';

import {
  CLOSE_LAYOUT_WITH_ELASTIC,
} from '../../UIISystemActionSheets';

import {
  GET_ROUTE_DIRECTIVE_TREE,
  MODIFY_ROUTE_DIRECTIVE_JSON,
} from '../../../../Core/Network/Editing';

import {
  findComponentOwner,
  findDirectiveNode,
  findDirectiveRootNode,
  TEMPLATE_JSON_DIRECTIVE_SIGN,
  DIRECTIVE_POS_ATTR_SIGN,
  getDirectiveNodeByPos,
  getDirectiveNodeName,
  hasCurationInEditingRules,
} from '../../../../Core/Helper/VueSupports';

import {
  getNearbyGridSystemNode,
} from '../../../../Core/Helper/DirectiveNodeWithGridSystem';

import {
  getAppendOrInsertComponentMethod,
  createNodeColumnWithComponent,
  createNodeRowWithColumnWithComponent,
  createNodeGridWithRowWithColumnWithComponent,
  createNodeComponent,
  modifyColumnSizingWithAppendingAndNewColumnProps,

  APPEND_NEW_COLUMN_AFTER,
  APPEND_NEW_COLUMN_BEFORE,

  APPEND_NEW_ROW_WITH_COLUMN_AFTER,
  APPEND_NEW_ROW_WITH_COLUMN_BEFORE,

  INSERT_COLUMN_FIRST,
  INSERT_COLUMN_LAST,

  INSERT_ROW_WITH_COLUMN_FIRST,
  INSERT_ROW_WITH_COLUMN_LAST,

  INSERT_INSIDE,
  INSERT_INSIDE_WITH_GRID,
} from '../../../../Core/Helper/GridDirectiveModifyPolicing';

import './style.scss';


import {
  Info,
  ROUTE_MODIFIED_EVENT_KEY_BASE,
  EDITING_SERVICE_GLOBAL_EVENT_EMITTER_KEY,
  SERVICE_FLASH_RENDERED_EVENT_KEY,
  SPAWN_NEW_ICE_FRAGMENT_KEY,
} from './Constants';

import * as actions from './actions';


import * as StudioActions from '../../../../actions/Studio';


const toggleStyles = {
  combinationModeToggle: {
    width: 150,
    display: 'inline-block',
    margin: 1,
    marginLeft: 5,
    float: 'left',
  },

  default: {
    width: 125,
    display: 'inline-block',
    margin: 1,
    marginLeft: 5,
    float: 'left',
  },

  thumbOff: {
    backgroundColor: 'rgb(94, 147, 185)',
  },
  trackOff: {
    backgroundColor: 'rgb(139, 182, 214)',
  },
  thumbSwitched: {
    backgroundColor: '#fff',
  },
  trackSwitched: {
    backgroundColor: 'rgb(5, 149, 255)',
  },
  labelStyle: {
    color: '#f9f9f9',
  },
};

@connect((state) => ({
  User: state.get('User'),
  Site: state.get('Site'),
  AssignedStore: state.get('AssignedStore'),
  Studio: state.get("Studio"),
  DragSystem: state.get('DragSystem'),
}))
class Canvas extends Component {
  static contextTypes = {
    elastic: PropTypes.object,
    zone: PropTypes.object,
    elasticToolSystems: PropTypes.object,
    events: PropTypes.object,
    UIISystems: PropTypes.object,
    functionStore: PropTypes.object,
    global : PropTypes.object,
    ice2 : PropTypes.func,
  }

  static propTypes = {
    combinationMode: PropTypes.bool,
    nodeGraphOverlayMode: PropTypes.bool,
    routeDirectiveJSONTREE_map: PropTypes.object,
    routeDirectiveJSONTREE_loading_state_map: PropTypes.object,
    _elasticID: PropTypes.string,
    remoteConX: PropTypes.number,
    remoteConY: PropTypes.number,
  }

  constructor(props) {
    super(props);

    this.state = {
      mouseHoverIFrameDOM: null,
      mouseHoverIFrameDOMreal: null,
      mounted: false,
      iframeLoaded: false,
      informationBottom: false,
      showMenus: false,
      iframeRect: {
        bottom: 619,
        height: 559,
        left: 645,
        right: 1157,
        top: 60,
        width: 512,
        x: 645,
        y: 60,
      },


      wysiwygTargetEditRule : null,
      metaComponents : [],
    }

    this.wrappedOnSelectDirectiveNode = this.onSelectDirectiveNode.bind(this);
    this.wrappedOnModifiedDirectiveNode = this.onModifiedDirectiveNode.bind(this);
    this.wrappedDisplayComponentRefresh = this.onDisplayComponentRefresh.bind(this);
    this.wrappedSettingUpdateComponent = this.settingUpdateComponent.bind(this);
  }


  getIframeWidth() {
    switch (this.props.Studio.etc.deviceMode) {
      case "smaller":
        return 512;
      case "mobile":
        return 768;
      case "tablet":
        return 992;
      case "desktop":
        return 1200;
      case "fit":
        return 99999;
    }
  }


  eventListening() {
    this.context.events.listen('SELECT-DIRECTIVE-NODE', this.wrappedOnSelectDirectiveNode);
    this.context.events.listen('MODIFIED-DIRECTIVE-NODE', this.wrappedOnModifiedDirectiveNode);
    this.context.events.listen('CANVAS-DISPLAY-COMPONENT-REFRESH', this.wrappedDisplayComponentRefresh);
    this.context.events.listen('SETTING-UPDATE-COMPONENT', this.wrappedSettingUpdateComponent);
  }

  eventListeningClear() {
    this.context.events.removeListen('SELECT-DIRECTIVE-NODE', this.wrappedOnSelectDirectiveNode);
    this.context.events.removeListen('MODIFIED-DIRECTIVE-NODE', this.wrappedOnModifiedDirectiveNode);
    this.context.events.removeListen('CANVAS-DISPLAY-COMPONENT-REFRESH', this.wrappedDisplayComponentRefresh);
    this.context.events.removeListen('SETTING-UPDATE-COMPONENT', this.wrappedSettingUpdateComponent);
  }


  onDisplayComponentRefresh({componentName, componentPos, refreshMethod, directiveName}) {
    // let editingRule = this.getNodeEditingRules(directiveNode.tag);

    let iframeWindow = this.iframe.contentWindow;

    let dom = iframeWindow.document.querySelector(
      `[${TEMPLATE_JSON_DIRECTIVE_SIGN}="${directiveName}"][${DIRECTIVE_POS_ATTR_SIGN}="${componentPos}"],
      [${TEMPLATE_JSON_DIRECTIVE_SIGN}="${directiveName}"] [${DIRECTIVE_POS_ATTR_SIGN}="${componentPos}"]`
    );

    //
    // console.log(`[${TEMPLATE_JSON_DIRECTIVE_SIGN}="${directiveName}"][${DIRECTIVE_POS_ATTR_SIGN}="${componentPos}"],
    //   [${TEMPLATE_JSON_DIRECTIVE_SIGN}="${directiveName}"] [${DIRECTIVE_POS_ATTR_SIGN}="${componentPos}"]`);

    if (typeof dom.__vue__[refreshMethod] == 'function') {
      dom.__vue__[refreshMethod]();
    } else {
      console.error(`${componentName} 컴포넌트에 refreshMethod[${refreshMethod}] 가 구현되지 않았습니다.`);
    }
  }


  settingUpdateComponent({directiveNode, directiveNodePos, directiveName}){

    let directive = this.props.routeDirectiveJSONTREE_map[directiveName];


    let directiveJSON = directive.exportToJSON();

    set({ children: [directiveJSON]}, directiveNodePos.replace(/(\d+)/g, 'children[$1]'), directiveNode);


    // console.log(directiveJSON);

    this.onModifiedDirectiveNode({directiveName, exportedJSON: directiveJSON});


    // console.log(directiveName, exportedJSON);

    // iframe 내부에 eventEmitter 에 directiveNode 변경 이벤트를 전송한다.
    // this.iframe.contentWindow[EDITING_SERVICE_GLOBAL_EVENT_EMITTER_KEY].emit(`${ROUTE_MODIFIED_EVENT_KEY_BASE}${directiveName}`, {
    //   json: directiveJSON,
    // });

    // directiveNode 가 변경되고 iframe 내부에서 메인 랜더링이 완료 되었을 때 변경된 좌표값과 크기 등등 정보를 Canvas 에 반영하기 위해 Canvas 를 다시 랜더링하고.
    // 변경된 Body Directive Node 트리를 저장한다.
    // this.iframe.contentWindow[EDITING_SERVICE_GLOBAL_EVENT_EMITTER_KEY].once(SERVICE_FLASH_RENDERED_EVENT_KEY, () => {
    //   this.forceUpdate();
    //
    //   // console.log('::feedback updated');
    //   this.context.elastic.dispatch(
    //     actions.SET_BODY_HTML_DIRECTIVE_NODE(
    //       VueNuxtNode.importFromHTMLElementNode(this.iframe.contentWindow.document.body)
    //     ));
    // });
    //
    // let id = this.props.fragmentInfoMap[directiveName].id || '';
    // this.context.elastic.dispatch(
    //   actions.LOADED_ROUTE_DIRECTIVE_TREE(
    //     VueNuxtNode.importFromJSON(directiveJSON),
    //     directiveName,
    //     directiveNodePos,
    //     directiveName,
    //     id,
    //   )
    // );
  }



  getEditTargetContentsHeight(){
    if( this.iframe ){
      try{

        return this.iframe.contentDocument.body.scrollHeight;
      } catch(e){
        console.error(e);
      }
    }

    return 100;
  }

  emitContentsHeight(){
    console.log(this.getEditTargetContentsHeight())
    window.postMessage({
      event : 'content-height',
      h : this.getEditTargetContentsHeight(),
    }, '*')
  }

  frameOnLoad() {



    this.iframe.contentWindow[EDITING_SERVICE_GLOBAL_EVENT_EMITTER_KEY].on(SPAWN_NEW_ICE_FRAGMENT_KEY, (event) => {
      console.log('sadasds>>>',event);

    })

    // location.reload();


    this.setState({
      iframeLoaded: true,
    });

    this.emitContentsHeight();


    this.context.elastic.dispatch(actions.SET_IFRAME_STATE(
      this.iframe.contentWindow.innerWidth,
      this.iframe.contentWindow.document.documentElement.scrollWidth,
      this.iframe.contentWindow.document.documentElement.scrollHeight,
      this.iframe.contentWindow.location.pathname,
      this.iframe.contentWindow.document.title,
    ));

    this.iframe.contentWindow.addEventListener('resize', () => {
      this.context.elastic.dispatch(actions.SET_IFRAME_STATE(
        this.iframe.contentWindow.innerWidth,
        this.iframe.contentWindow.document.documentElement.scrollWidth,
        this.iframe.contentWindow.document.documentElement.scrollHeight
      ));


      this.emitContentsHeight();
    });

    this.iframe.contentWindow.addEventListener('message', (e) => {

      // console.log(e);
    });


    let referencedDirectiveDoms = this.iframe.contentDocument.querySelectorAll('[__vue_component_directive]');
    Promise.all(Array.prototype.map.call(referencedDirectiveDoms, (dom) => {
      let directiveName = dom.getAttribute('__vue_component_directive');
      let directiveNameFallback = dom.getAttribute('__vue_component_directive_fallback');
      let fragmentAcceptComponents = dom.getAttribute('data-accept-components') || '';

      fragmentAcceptComponents = fragmentAcceptComponents.split(',').map((compKey) => compKey.trim());

      return GET_ROUTE_DIRECTIVE_TREE(this.props.User.session.id, this.props.Site.info.id, this.props.Site.info.owner, directiveName, directiveNameFallback)
        .then((res) => {
          let {data} = res;
          let {treeJSON, serviceFragmentId} = data;

          console.log('directive',directiveName, data);
          this.context.elastic
            .dispatch(
              actions.LOADED_ROUTE_DIRECTIVE_TREE(
                VueNuxtNode.importFromJSON(treeJSON),
                directiveName,
                null,
                null,
                serviceFragmentId,
                fragmentAcceptComponents
              )
            );
        });
    }))
      .then(() => {
        console.log('directive all loaded');
      });



    this.props.dispatch(StudioActions.SET_PAGE_INFO_VUEX(cloneDeep(this.iframe.contentWindow.__VUEX_STATE_FOR_BUILDER__)));

    this.context.elastic.dispatch(actions.SET_BODY_HTML_DIRECTIVE_NODE(VueNuxtNode.importFromHTMLElementNode(this.iframe.contentWindow.document.body)));
  }

  /**
   * 대상 노드의 Editing Rules 를 찾아 반환한다.
   * @param tag:string
   * @param props:object
   * @returns {*|{}}
   */
  getNodeEditingRules(tag, props) {
    let iframeWindow = this.iframe.contentWindow;
    if (!(iframeWindow.document && iframeWindow.document.readyState === 'complete')) {
      throw new Error("Iframe 이 완전히 로드되지 않았습니다. 호출 흐름을 점검하세요.");
    }


    if( tag == 'meta-component' && props ){
      let mainId = props.mainId.toString();

      let foundMetaComponent = this.state.metaComponents.find((item) => item.layoutid.toString() == mainId) || {}

      // console.log('meta component rule', foundMetaComponent)
      return {
        props: {
          dynamicProps: {
            label: '속성입력',
            type: 'object',
            typeStructure: {
              ...foundMetaComponent.editRuleObject,
            },
          },
          textEditable: {},
        },
      }

    } else {
      let includedComponentCollection = iframeWindow._EDIT_COMPONENT_COLLECTION;
      let editRulesCollection = includedComponentCollection.EDIT_RULES;
      let componentEditRules = editRulesCollection[tag];
      // console.log(componentEditRules);

      return componentEditRules || {};
    }
  }


  getSelectedNode() {
    let selectedDirectiveNode = null;
    if (this.props.routeDirectiveJSONTREE_map[this.props.selectedDirectiveName] && this.props.selectedDirectiveNodePos) {
      selectedDirectiveNode = this.props.routeDirectiveJSONTREE_map[this.props.selectedDirectiveName].findByLocation(this.props.selectedDirectiveNodePos);
    }
    return selectedDirectiveNode;
  }

  onOpenGridEditor() {
    this.context.UIISystems.emitActionSheet(CreateGridEditorOpenActionSheet("rightArea"), {
      directiveName: this.props.selectedDirectiveName,
      directivesMap: this.props.routeDirectiveJSONTREE_map,
      directiveNode: this.getSelectedNode(),
      directiveNodePos: this.props.selectedDirectiveNodePos,
    }, {DISABLE_DUPLICATE_UNDEPLOY: true});

    this.context.events.emit('on-show-rightArea');
  }

  onOpenSettings() {
    this.context.UIISystems.emitActionSheet(CreateSettingsOpenActionSheet("rightArea"), {
      directiveName: this.props.selectedDirectiveName,
      directivesMap: this.props.routeDirectiveJSONTREE_map,
      directiveNode: this.getSelectedNode(),
      directiveNodePos: this.props.selectedDirectiveNodePos,
    }, {DISABLE_DUPLICATE_UNDEPLOY: true});

    this.context.events.emit('on-show-rightArea');
  }

  onSelectDirectiveNode({node, directiveName}) {
    // wysiwyg 모드가 켜져있을 경우에는 먼저 저장 후 선택을 처리한다.
    if (this.state.wysiwygLaunched) {
      this.state.wysiwygApplyFunction(this.wysiwigEditor.getValue());
    }


    let iframeWindow = this.iframe.contentWindow;
    if (!(iframeWindow.document && iframeWindow.document.readyState === 'complete')) {
      return;
    }

    if (this.props.routeDirectiveJSONTREE_map[directiveName]) {
      this.selectDirectiveNodeDelegate(directiveName, node, node.pos);
    } else {
      GET_ROUTE_DIRECTIVE_TREE(this.props.User.session.id, this.props.Site.info.id, this.props.Site.info.owner, directiveName)
        .then((res) => {
          let {data} = res;
          let {treeJSON} = data;

          this.context.elastic.dispatch(actions.LOADED_ROUTE_DIRECTIVE_TREE(VueNuxtNode.importFromJSON(treeJSON), directiveName));
          this.selectDirectiveNodeDelegate(directiveName, node, node.pos);
        });
    }
  }


  selectDirectiveNodeDelegate(directiveName, directiveNode, directiveNodePos, selectingPoint, withContextMenu) {

    // wysiwyg 모드가 켜져있을 경우에는 먼저 저장 후 선택을 처리한다.
    if (this.state.wysiwygLaunched) {
      this.state.wysiwygApplyFunction(this.wysiwigEditor.getValue());
    }

    console.log(directiveNode)
    let editingRule = this.getNodeEditingRules(directiveNode.tag, directiveNode.props);


    this.context.events.emit('CANVAS-SELECT-NODE', {
      directiveName,
      directiveNode : directiveNode.exportToJSON(),
      directiveNodePos,
    });


    this.context.elastic.dispatch(actions.SELECT_DIRECTIVE_NODE(directiveName, directiveNode, directiveNodePos, selectingPoint, withContextMenu, editingRule));
  }

  selectParentDirective() {
    let currentNode = this.getSelectedNode();
    let directiveName = this.props.selectedDirectiveName;

    if (currentNode.parent) {
      this.selectDirectiveNodeDelegate(directiveName, currentNode.parent, currentNode.parent.pos, this.props.selectClickPoint, this.props.showContextMenu);
    }
  }


  deleteNode(rootDirectiveNode, directivePosition){
    let rootNode = rootDirectiveNode;


    let node = getDirectiveNodeByPos(rootNode, directivePosition);
    // console.log(foundNode);
    if (node.parent) {
      let targetNode = node;

      while( targetNode.parent ){
        if( targetNode.parent.children.length < 2 && targetNode.parent.parent ){
          targetNode = targetNode.parent;
        } else {
          break;
        }
      }

      let pos = targetNode.pos;
      targetNode.parent.removeChild(pos);

      let rootNode = this.props.routeDirectiveJSONTREE_map[this.props.selectedDirectiveName];


      return rootNode;
    } else {
      console.warn('최상위 Node 는 제거 할 수 없습니다.');
      console.warn('최상위 Node 는 제거 할 수 없습니다.');
    }
  }


  onDeleteSelectedNode() {
    let node = this.getSelectedNode();


    if (node.parent) {
      let targetNode = node;

      while( targetNode.parent ){
        if( targetNode.parent.children.length < 2 && targetNode.parent.parent ){
          targetNode = targetNode.parent;
        } else {
          break;
        }
      }

      let pos = targetNode.pos;
      targetNode.parent.removeChild(pos);

      let rootNode = this.props.routeDirectiveJSONTREE_map[this.props.selectedDirectiveName];

      this.onModifiedDirectiveNode({
        exportedJSON: rootNode.exportToJSON(),
        directiveName: this.props.selectedDirectiveName,
      }, targetNode.parent.pos);
    } else {
      console.warn('최상위 Node 는 제거 할 수 없습니다.');
      console.warn('최상위 Node 는 제거 할 수 없습니다.');
    }
  }

  onApplyDirectiveFull(){
    let directiveName = this.props.selectedDirectiveName;



    this.directiveApply(directiveName);
  }


  getPageInstanceUrl(additionalQueries = {}){
    let {staticData,Studio} = this.props;

    const usp = new URLSearchParams();
    const inputQueries = {...Studio.pageInfo.queries, ...additionalQueries};
    const queryKeys = Object.keys(inputQueries);

    for(let i = 0; i < queryKeys.length; i++ ){
      usp.append(queryKeys[i], inputQueries[queryKeys[i]]);
    }


    let src = '//' +
      [
        // Studio.session.id,
        location.hostname,
      ].join('.') +
      (location.port ? ":" + location.port:'') +
      (staticData.path || Studio.pageInfo.path ) + '?' +
      `__s__=${Studio.session.id}` + '&' +
      usp.toString();

    return src;
  }

  onFragmentVersionManaging(){
    let directiveName = this.props.selectedDirectiveName;
    let directive = this.props.routeDirectiveJSONTREE_map[directiveName];





    let directiveDom = this.iframe.contentDocument.querySelector(`[__vue_component_directive='${directiveName}']`);
    let areaName = directiveDom.getAttribute('data-fragment-name') || '';

    let apply = this.context.UIISystems.getUnique();

    let versionLoadFunctionKey = this.context.UIISystems.getUnique();
    this.context.UIISystems.openTool('ICE2VersionManager', this.props.defaultPopupTarget || '[modal]' , {
      width:1024,
      height:500,
      UNIQUE_LAYER : true,
    }, {
      title : areaName+' 영역 버전 관리',
      directiveName,
      idField : 'serviceFragmentId',
      pageBaseUrl : this.iframe.contentWindow.location.href,
      versionLoadFunctionKey : versionLoadFunctionKey,
      footerBtnCallbacks : {
        apply,
      },
    }).then((closer) => {
      this.context.functionStore.register(versionLoadFunctionKey, (record)=>{

        this.resetSelected();



        this.onModifiedDirectiveNode({
          exportedJSON: record.json,
          directiveName: directiveName,
        }, '', record.version);

        this.context.elastic
          .dispatch(
            actions.LOADED_ROUTE_DIRECTIVE_TREE(
              VueNuxtNode.importFromJSON(record.json),
              directiveName.replace(/@\d+/, '@' + record.version),
              null,
              null,
              record.serviceFragmentId,
              this.props.fragmentInfoMap[directiveName].fragmentAcceptComponents
            )
          );

        closer();
      })
    })
  }

  async onSaveAsTemplate(){
    let directiveName = this.props.selectedDirectiveName;


    let directiveDom = this.iframe.contentDocument.querySelector(`[__vue_component_directive='${directiveName}']`);
    let areaName = directiveDom.getAttribute('data-fragment-name') || '';

    let okBtn = this.context.UIISystems.getUnique();
    let noBtn = this.context.UIISystems.getUnique();

    let closer = await this.context.UIISystems.openTool('Dialog', '[modal]',{
      width:500,
      height:300,
    }, {
      title : areaName +" 템플릿 저장",
      fields : [
        {
          label : '템플릿 이름',
          key : 'name',
          type : 'input',
          inputType : 'string',
          placeholder : '템플릿 이름을 입력하세요.',
        },
        {
          label : '템플릿 설명',
          key : 'message',
          type : 'input',
          inputType : 'text',
          placeholder : '템플릿 설명을 입력하세요.',
        },

        {
          type : 'button-set',
          buttons : [
            {
              label : '저장',
              functionKey : okBtn ,
              level : 'positive',
            },
            {
              label : '취소' ,
              functionKey : noBtn,
              level : 'negative',
            },
          ],
        },
      ],
    });

    this.context.functionStore.register(okBtn, async (data)=> {
      closer();

      let {message = {}, name, status} = data;
      let messageValue = message.value;
      let nameValue = name.value;



      let directive = this.props.routeDirectiveJSONTREE_map[directiveName];

      let json = directive.exportToJSON();
      let id = this.props.fragmentInfoMap[directiveName].id || '';
      this.resetSelected();




      let lastVersion = 0;
      let res;
      let blockingId;
      try{
        blockingId = this.context.global.uiBlockingReqBegin();
        // res = await MODIFY_ROUTE_DIRECTIVE_JSON(
        //   this.props.User.session.id,
        //   this.props.Site.info.id,
        //   this.props.Site.info.owner,
        //   directiveName,
        //   json,
        //   id,
        //   messageValue,
        //   status ? status.value : false);


        // let templateRegisteredList = await this.context.ice2.request({
        //   url : '/node/serviceFragment/list.json',
        //   method:'get',
        //   params : {
        //     metaKey1_matching : '__template__',
        //     metaKey2_matching : nameValue,
        //
        //     limit: 1,
        //     sorting : 'version desc',
        //   },
        // })



        let oldListRes = await this.context.ice2.request({
          url : '/node/serviceFragment/list.json',
          method:'get',
          params : {
            metaKey1_matching : '__template__',
            metaKey2_matching : nameValue,

            limit: 1,
            sorting : 'version desc',
          },
        })


        let oldList = oldListRes.data.items;
        let saveId = null;
        console.log(oldList);

        if( oldList && oldList[0] ){
          lastVersion = oldList[0].version || 0;
          saveId = oldList[0].serviceFragmentId;
        }


        let formData = new FormData();
        formData.append('serviceFragmentId', saveId);
        formData.append('metaKey1', '__template__');
        formData.append('metaKey2', nameValue);
        formData.append('level', 0);
        formData.append('version', lastVersion + 1);
        formData.append('json', JSON.stringify(json));
        formData.append('name', nameValue);
        formData.append('messageValue', messageValue);

        let saveRes = await this.context.ice2.request({
          url : '/node/serviceFragment/save.json',
          method:'post',
          data : formData,


          //   {
          //   serviceFragmentId : saveId,
          //   metaKey1 : '__template__',
          //   metaKey2 : nameValue,
          //   level:0,
          //   version : lastVersion + 1,
          //   json : JSON.stringify(json),
          //   name: nameValue,
          //   description : messageValue,
          // },
        })


        this.context.global.uiBlockingReqFinish(blockingId);
      } catch(e){
        this.context.global.uiBlockingReqFinish(blockingId);
        notification.open({
          message : '템플릿 저장 실패',
          description: `${directiveName.replace(/@\d+/, '')} 영역의 반영을 실패 하였습니다. 관리자에게 문의 하세요.`,
          icon: <Icon type="smile-circle" style={{ color: '#108ee9' }} />,
        });


        return;
      }



      console.log("Server modify success", directiveName, json, res);

      notification.open({
        message : '템플릿 저장 완료',
        description: `${directiveName.replace(/@\d+/, '')} 영역의 버전이 [${lastVersion+1}](으)로 상승 되었습니다.`,
        icon: <Icon type="smile-circle" style={{ color: '#108ee9' }} />,
      });




      this.context.functionStore.remove(okBtn);
      this.context.functionStore.remove(noBtn);
    });

    this.context.functionStore.register(noBtn, () => {
      this.context.functionStore.remove(okBtn);
      this.context.functionStore.remove(noBtn);

      closer();
      closer = null;
    });
  }

  onTemplateManaging(){
    let directiveName = this.props.selectedDirectiveName;
    let directive = this.props.routeDirectiveJSONTREE_map[directiveName];

    let {Studio, staticData} = this.props;

    // console.log(Studio.pageInfo.query)
    let templatePreviewPageUrl = '//' +
      [
        // Studio.session.id,
        location.hostname,
      ].join('.') +
      (location.port ? ":" + location.port:'') +
      ('/fragmentPreview' ) + '?' +
      `__s__=${Studio.session.id}`;



    let directiveDom = this.iframe.contentDocument.querySelector(`[__vue_component_directive='${directiveName}']`);
    let areaName = directiveDom.getAttribute('data-fragment-name') || '';

    let apply = this.context.UIISystems.getUnique();

    let versionLoadFunctionKey = this.context.UIISystems.getUnique();
    this.context.UIISystems.openTool('FragmentTemplateManager', this.props.defaultPopupTarget || '[modal]' , {
      width:1024,
      height:500,
      UNIQUE_LAYER : true,
    }, {
      title : '템플릿 관리',
      directiveName,
      idField : 'serviceFragmentId',
      pageBaseUrl : this.iframe.contentWindow.location.href,
      templatePreviewPageUrl,
      versionLoadFunctionKey : versionLoadFunctionKey,
      footerBtnCallbacks : {
        apply,
      },
    }).then((closer) => {
      this.context.functionStore.register(versionLoadFunctionKey, (record)=>{

        this.resetSelected();



        this.onModifiedDirectiveNode({
          exportedJSON: record.json,
          directiveName: directiveName,
        }, '', record.version);

        this.context.elastic
          .dispatch(
            actions.LOADED_ROUTE_DIRECTIVE_TREE(
              VueNuxtNode.importFromJSON(record.json),
              directiveName.replace(/@\d+/, '@' + record.version),
              null,
              null,
              record.serviceFragmentId,
              this.props.fragmentInfoMap[directiveName].fragmentAcceptComponents
            )
          );

        closer();
      })
    })
  }

  onApplyPart(){

    notification.open({
      message : '준비중 입니다.',
      description : '...',
      icon: <Icon type="smile-circle" style={{ color: '#108ee9' }} />,
    });
return;

    let node = this.getSelectedNode();
    let pos = node.pos;

    if (node.parent) {
      node.parent.removeChild(pos);

      let rootNode = this.props.routeDirectiveJSONTREE_map[this.props.selectedDirectiveName];

      this.onModifiedDirectiveNode({
        exportedJSON: rootNode.exportToJSON(),
        directiveName: this.props.selectedDirectiveName,
      }, node.parent.pos);
    } else {
      console.warn('최상위 Node 는 제거 할 수 없습니다.');
      console.warn('최상위 Node 는 제거 할 수 없습니다.');
    }
  }

  /**
   * onModifiedDirectiveNode
   * @description DirectiveNode 를 변경하고 Canvas 에 적용 할 때 호출한다.
   * DirectiveNode 를 변경하고 적용 할 때 반드시 이 메서드를 타야한다.
   * 타 툴에 의해 호출 될 수도 있다.
   * + 히스토리 관리
   * @param {directiveName, exportedJSON}
   * @param nextSelectNodePos
   */
  async onModifiedDirectiveNode({directiveName, exportedJSON}, nextSelectNodePos, version) {



    console.log('ee',exportedJSON);

      // console.log(directiveName, exportedJSON);

      // iframe 내부에 eventEmitter 에 directiveNode 변경 이벤트를 전송한다.
      this.iframe.contentWindow[EDITING_SERVICE_GLOBAL_EVENT_EMITTER_KEY].emit(`${ROUTE_MODIFIED_EVENT_KEY_BASE}${directiveName}`, {
        json: exportedJSON,
        version : version,
      });

      // directiveNode 가 변경되고 iframe 내부에서 메인 랜더링이 완료 되었을 때 변경된 좌표값과 크기 등등 정보를 Canvas 에 반영하기 위해 Canvas 를 다시 랜더링하고.
      // 변경된 Body Directive Node 트리를 저장한다.
      this.iframe.contentWindow[EDITING_SERVICE_GLOBAL_EVENT_EMITTER_KEY].once(SERVICE_FLASH_RENDERED_EVENT_KEY, () => {
        this.forceUpdate();

        // console.log('::feedback updated');
        this.context.elastic.dispatch(
          actions.SET_BODY_HTML_DIRECTIVE_NODE(
            VueNuxtNode.importFromHTMLElementNode(this.iframe.contentWindow.document.body)
          ));


        this.emitContentsHeight();
      });



      let id = this.props.fragmentInfoMap[directiveName].id || '';
      return await this.context.elastic.dispatch(
        actions.LOADED_ROUTE_DIRECTIVE_TREE(
          VueNuxtNode.importFromJSON(exportedJSON),
          directiveName,
          nextSelectNodePos,
          directiveName,
          id,
          null,
          'push',
        ),
        true
      );

      // MODIFY_ROUTE_DIRECTIVE_JSON(this.props.User.session.id, this.props.Site.info.id, this.props.Site.info.owner, directiveName, exportedJSON)
      //   .then((res) => {
      //     console.log("Server modify success", directiveName, exportedJSON, res);
      //   })
  }

  onHistoryMove(directiveName, action){
    console.log(directiveName, action);
    // return alert('개발중입니다.');
    let currentStack = this.props.routeDirectiveJSONTREE_history_map[directiveName] || [];
    let cursor = this.props.routeDirectiveJSONTREE_history_cursor_map[directiveName];
    let nextSheet;


    console.log(currentStack);

    if( action == 'back' && currentStack.length > 0 && cursor > 0 ){
      cursor = cursor -1;
      nextSheet = currentStack[cursor];
      console.log( cursor, nextSheet );

    } else if( action == 'go' && currentStack.length-1 < cursor + 1){
      cursor = cursor + 1;
      nextSheet = currentStack[cursor-1];

    } else {
      return notification.open({
        message : 'Can\'t history explore',
        icon: <Icon type="smile-circle" style={{ color: '#108ee9' }} />,
      })
    }


    // iframe 내부에 eventEmitter 에 directiveNode 변경 이벤트를 전송한다.
    this.iframe.contentWindow[EDITING_SERVICE_GLOBAL_EVENT_EMITTER_KEY].emit(`${ROUTE_MODIFIED_EVENT_KEY_BASE}${directiveName}`, {
      json: nextSheet,
    });

    // directiveNode 가 변경되고 iframe 내부에서 메인 랜더링이 완료 되었을 때 변경된 좌표값과 크기 등등 정보를 Canvas 에 반영하기 위해 Canvas 를 다시 랜더링하고.
    // 변경된 Body Directive Node 트리를 저장한다.
    this.iframe.contentWindow[EDITING_SERVICE_GLOBAL_EVENT_EMITTER_KEY].once(SERVICE_FLASH_RENDERED_EVENT_KEY, () => {
      this.forceUpdate();

      // console.log('::feedback updated');
      this.context.elastic.dispatch(
        actions.SET_BODY_HTML_DIRECTIVE_NODE(
          VueNuxtNode.importFromHTMLElementNode(this.iframe.contentWindow.document.body)
        ));
    });


    this.context.elastic.dispatch(
      actions.HISTORY_MOVE(
        directiveName,
        cursor,
      )
    );
  }


  async directiveApply(directiveName){


    let directiveDom = this.iframe.contentDocument.querySelector(`[__vue_component_directive='${directiveName}']`);
    let areaName = directiveDom.getAttribute('data-fragment-name') || '';

    let okBtn = this.context.UIISystems.getUnique();
    let noBtn = this.context.UIISystems.getUnique();

    let closer = await this.context.UIISystems.openTool('Dialog', '[modal]',{
      width:500,
      height:240,
    }, {
      title : areaName +" 영역 반영",
      fields : [
        {
          label : '반영 메시지',
          key : 'message',
          type : 'input',
          inputType : 'text',
          placeholder : '변경 사항에 대한 메시지를 기록하세요.',
        },


        {
          label : '즉시 게시 하기',
          key : 'status',
          type : 'toggle',
          defaultValue : false,
          placeholder : '변경 사항에 대한 메시지를 기록하세요.',
        },

        {
          type : 'button-set',
          buttons : [
            {
              label : '반영',
              functionKey : okBtn ,
              level : 'positive',
            },
            {
              label : '취소' ,
              functionKey : noBtn,
              level : 'negative',
            },
          ],
        },
      ],
    });

    this.context.functionStore.register(okBtn, async (data)=> {
      closer();

      let {message, status} = data;
      let messageValue = message.value;



      let directive = this.props.routeDirectiveJSONTREE_map[directiveName];

      let json = directive.exportToJSON();
      let id = this.props.fragmentInfoMap[directiveName].id || '';
      this.resetSelected();





      let res;
      let blockingId;
      try{
        blockingId = this.context.global.uiBlockingReqBegin();
        res = await MODIFY_ROUTE_DIRECTIVE_JSON(
          this.props.User.session.id,
          this.props.Site.info.id,
          this.props.Site.info.owner,
          directiveName,
          json,
          id,
          messageValue,
          status ? status.value : false);

        this.context.global.uiBlockingReqFinish(blockingId);
      } catch(e){
        this.context.global.uiBlockingReqFinish(blockingId);
        notification.open({
          message : '영역 반영 실패',
          description: `${directiveName.replace(/@\d+/, '')} 영역의 반영을 실패 하였습니다. 관리자에게 문의 하세요.`,
          icon: <Icon type="smile-circle" style={{ color: '#108ee9' }} />,
        });


        return;
      }


      let {
        serviceFragmentId,
        directiveJSON,
        version,
      } = res.data;



      this.onModifiedDirectiveNode({
        exportedJSON: directiveJSON,
        directiveName: directiveName,
      }, '', version);

      this.context.elastic
        .dispatch(
          actions.LOADED_ROUTE_DIRECTIVE_TREE(
            VueNuxtNode.importFromJSON(directiveJSON),
            directiveName.replace(/@\d+/, '@' + version),
            null,
            null,
            serviceFragmentId,
            this.props.fragmentInfoMap[directiveName].fragmentAcceptComponents
          )
        );

      // 서비스 프래그먼트 아이디가 내려오게 되면 해당 directive에 id를 바인딩한다.
      if( serviceFragmentId ) {
        this.context.elastic.dispatch(actions.BIND_DIRECTIVE_FRAGMENT_ID(directiveName, serviceFragmentId));
      }


      console.log("Server modify success", directiveName, json, res);

      notification.open({
        message : '영역 반영 완료',
        description: `${directiveName.replace(/@\d+/, '')} 영역의 버전이 [${version}](으)로 상승 되었습니다.`,
        icon: <Icon type="smile-circle" style={{ color: '#108ee9' }} />,
      });




      this.context.functionStore.remove(okBtn);
      this.context.functionStore.remove(noBtn);
    });

    this.context.functionStore.register(noBtn, () => {
      this.context.functionStore.remove(okBtn);
      this.context.functionStore.remove(noBtn);

      closer();
      closer = null;
    });
  }


  onClickDirectiveApply(directiveName){
    this.directiveApply(directiveName);
  }


  onToggleElementType(_e, _isChecked) {
    this.context.elastic.dispatch(actions.TOGGLE_EDITING_MODE(_isChecked));
  }

  onToggleMiniController(_e, _isChecked) {
    this.context.elastic.dispatch(actions.TOGGLE_MINI_CONTROLLER(_isChecked));
  }

  onToggleGraph(_e, _isChecked) {
    this.context.elastic.dispatch(actions.TOGGLE_NODE_GRAPH(_isChecked));
  }

  onToggleDropFocusing(_e, _isChecked) {
    this.context.elastic.dispatch(actions.TOGGLE_DROP_FOCUSING(_isChecked));
  }

  openDirectiveJSONEditor() {

    let modalId;
    this.context.UIISystems.emitActionSheet(CreateCodeEditorOpenActionSheet("[modal]"), {
      title: `Page Editing <${this.props.staticData.name}${this.props.staticData.path}>`,
      lang: 'json',
      raw: beautifyJson(this.props.routeDirectiveJSONTREE_map[this.props.staticData.routeDirective].exportToJSON(), null, 2, 100),

      footerButtons: [
        {
          title: 'Apply',
          functionKey: 'apply',
        },
        {
          title: 'Cancel',
          functionKey: 'cancel',
        },
      ],
    }, {
      width: '90%',
      height: '90%',
      onLayoutID: function (id) {
        modalId = id;
      },
    });

    this.context.functionStore.register('apply', (data, validate) => {
      if (validate.length == 0) {

        this.onModifiedDirectiveNode({
          directiveName: this.props.staticData.routeDirective,
          exportedJSON: JSON.parse(data),
        });
      } else {
        notification.error({
          message: 'Invalid JSON',
          description: 'Syntax error',
          style: {
            zIndex: 20000,
          },
        });
      }
    });

    this.context.functionStore.register('cancel', (data, validate) => {
      this.context.UIISystems.emitActionSheet(CLOSE_LAYOUT_WITH_ELASTIC(modalId, CodeEditorToolKey, true));
      return true;
    });
  }

  onWheel(_e, deltaX, deltaY) {
    let dx = deltaX, dy = deltaY;

    if( _e ){
      let e = _e.nativeEvent || _e;

      dx = e.deltaX;
      dy = e.deltaY;
    }



    let document = this.iframe.contentDocument;


    let scrollHandleDOM = document.querySelector('html');
    scrollHandleDOM.scrollTop += dy;
    scrollHandleDOM.scrollLeft += dx;

    this.setState({
      mouseHoverIFrameDOM: null,
      mouseClickedIFrameDOM: null,
    });
  }

  onComponentDragging(dom, nearby, component, point) {
    let resultDom = this.props.combinationMode ? dom : findDirectiveNode(dom);



    let iframeHeight = this.iframe.offsetHeight;
    if( point.y < 10 ){
      this.onWheel(null, 0, -30)
    } else if ( point.y + 10 > iframeHeight){
      this.onWheel(null, 0, 30)
    }

    if (this.props.combinationMode) {

      // this.context.elastic.dispatch(actions.RESET_DIRECTIVE_NODE());
      // this.context.elastic.dispatch(actions.SELECT_NODE_OF_COMPONENT(componentPath, pos));
    } else {
      if (!resultDom) {
        let iframeWindow = this.iframe.contentWindow;

        resultDom = iframeWindow.document.querySelector(
          `[${TEMPLATE_JSON_DIRECTIVE_SIGN}="${this.props.staticData.routeDirective}"][${DIRECTIVE_POS_ATTR_SIGN}="0"],
          [${TEMPLATE_JSON_DIRECTIVE_SIGN}="${this.props.staticData.routeDirective}"] [${DIRECTIVE_POS_ATTR_SIGN}="0"]`
        );
      }


      if (!resultDom) {
        return this.context.elastic.dispatch(actions.COMPONENT_DRAGGING_OVER_DIRECTIVE(null, null, null, null));
      }



      let rootDriectiveDOM = findDirectiveRootNode(resultDom);
      let directiveName = rootDriectiveDOM.getAttribute(TEMPLATE_JSON_DIRECTIVE_SIGN);
      let rootDirectiveNode = this.props.routeDirectiveJSONTREE_map[directiveName];
      let pos = resultDom.getAttribute(DIRECTIVE_POS_ATTR_SIGN);
      let directiveNode = getDirectiveNodeByPos(rootDirectiveNode, pos);


      let rules = this.getNodeEditingRules(directiveNode.tag, directiveNode.props);

      console.log(rules, directiveNode.tag);

      if (rules.acceptChildren) {
        this.context.elastic.dispatch(actions.COMPONENT_DRAGGING_OVER_DIRECTIVE(directiveName, directiveNode, nearby, component));
      } else {
        let gridSystemNode = getNearbyGridSystemNode(directiveNode);
        this.context.elastic.dispatch(actions.COMPONENT_DRAGGING_OVER_DIRECTIVE(directiveName, gridSystemNode, nearby, component));
      }
    }
  }

  onComponentDragStop(dom, component) {
    // console.log(dom, component)
  }

  async onComponentDrop(dom, nearby, component) {

    console.log('drop component',component)

    if( component.dropSource == 'canvas' ){
      let sourcePosition = component.sourcePosition;
      let sourceDirectiveName  = component.sourceDirectiveName;

      await this.onComponentDropDecided(dom, nearby, component.source, sourceDirectiveName, sourcePosition);

      // await this.deleteNode(sourceDirectiveName, sourcePosition);
    } else {

      let fixedProps = component.fixedProps;
      let newComponentNode = createNodeComponent(component.componentKey);
      newComponentNode.props = {
        ...fixedProps,
      }

      new ComponentDropInitializer(this, newComponentNode, {
        onPassDrop: (initializedComponentNode) => {
          this.onComponentDropDecided(dom, nearby, initializedComponentNode);
        },
        onCancel: (initializedComponentNode) => {

        },
      });
    }
  }



  onComponentDropDecided(dom, nearby, componentNode, sourceDirectiveName, sourcePosition) {

    let resultDom = this.props.combinationMode ? dom : findDirectiveNode(dom);
    if (!resultDom) return this.resetSelected();

    // console.log(dom, nearby);
    if (this.props.combinationMode) {
      // this.context.elastic.dispatch(actions.RESET_DIRECTIVE_NODE());
      // this.context.elastic.dispatch(actions.SELECT_NODE_OF_COMPONENT(componentPath, pos));
    } else {
      if (!resultDom) {
        let iframeWindow = this.iframe.contentWindow;

        resultDom = iframeWindow.document.querySelector(
          `[${TEMPLATE_JSON_DIRECTIVE_SIGN}="${this.props.staticData.routeDirective}"][${DIRECTIVE_POS_ATTR_SIGN}="0"],
          [${TEMPLATE_JSON_DIRECTIVE_SIGN}="${this.props.staticData.routeDirective}"] [${DIRECTIVE_POS_ATTR_SIGN}="0"]`
        );
      }

      let rootDriectiveDOM = findDirectiveRootNode(resultDom);
      let directiveName = rootDriectiveDOM.getAttribute(TEMPLATE_JSON_DIRECTIVE_SIGN);
      let directive = this.props.routeDirectiveJSONTREE_map[directiveName];
      let pos = resultDom.getAttribute(DIRECTIVE_POS_ATTR_SIGN);
      let directiveNode = getDirectiveNodeByPos(directive, pos);


      if(sourceDirectiveName){
        if( directiveName != sourceDirectiveName ){
          return alert('다른 영역으로는 이동 할 수 없습니다.');
        } else {

          if( sourcePosition ){
            this.deleteNode(directive, sourcePosition)
          } else {
            return alert('위치가 유효하지 않습니다.')
          }
        }
      }


      let gridSystemNode = getNearbyGridSystemNode(directiveNode);

      let dropMethod = getAppendOrInsertComponentMethod(gridSystemNode, nearby);

      let dropTarget = getNearbyGridSystemNode(gridSystemNode, dropMethod.target);


      // 대상 노드 인덱스를 구해서 appendAfter 또는 appendBefore 해야함
      let dropTargetPos = dropTarget.location;
      let gridSystemNodePos = gridSystemNode.location;
      let appendIndex = parseInt(gridSystemNodePos.replace(dropTargetPos).split('.').slice(1).shift());

      // 원래 선택된 노드가 자식컴포넌트를 받는 경우 dropTarget 과 dropMethod 를 재지정 한다.
      // 2018.2.26 본격적인 Grid 컴포넌트외에 자식 컴포넌트를 받는 컴포넌트를 사용하기 시작함
      // 하지만 아래 코드때문에 버그가 발생
      // 이 코드가 필요한 코드인가?
      // 이 코드 삽입 시점은 2017.7.10~11 임 일단 주석처리
      // if (this.getNodeEditingRules(directiveNode.tag).acceptChildren) {
      //   console.log(directiveNode.tag + ' is children accepting.');
      //   dropTarget = directiveNode;
      //   dropMethod = getAppendOrInsertComponentMethod(directiveNode, nearby);
      //   console.log('redirected', dropMethod);
      // }
      console.log(dropTarget, nearby)
      if (this.getNodeEditingRules(directiveNode.tag, directiveNode.props).acceptChildren) {
        console.log(directiveNode.tag + ' is children accepting.');

        dropMethod = getAppendOrInsertComponentMethod(directiveNode, nearby);

         dropTarget = getNearbyGridSystemNode(directiveNode, dropMethod.target);

        dropTargetPos = dropTarget.location;
        gridSystemNodePos = directiveNode.location;
        appendIndex = parseInt(gridSystemNodePos.replace(dropTargetPos).split('.').slice(1).shift());

      };

      this.drop(dropMethod, dropTarget, componentNode, appendIndex, directiveName, directive, nearby);


      // if( resultDom ){
      //
      // } else {
      //
      //   let directiveName = this.props.routeDirectiveName
      //   let rootDirectiveNode = this.props.routeDirectiveJSONTREE_map[this.props.routeDirectiveName];
      //
      //   console.log('이게 타긴 타는건가...')
      //
      //   this.context.elastic.dispatch(actions.COMPONENT_DRAGGING_OVER_DIRECTIVE(directiveName, rootDirectiveNode, component));
      // }
    }
  }


  drop(dropMethod, dropTarget, componentNode, appendIndex, directiveName,directive, nearby){
    let appendingNode;
    switch (dropMethod.method) {
      case APPEND_NEW_COLUMN_BEFORE: {
        let newColumnProps = modifyColumnSizingWithAppendingAndNewColumnProps(APPEND_NEW_COLUMN_BEFORE, appendIndex, dropTarget.children);

        appendingNode = createNodeColumnWithComponent(componentNode, newColumnProps);
        dropTarget.appendChildBefore(appendIndex, appendingNode);
      }
        break;
      case APPEND_NEW_COLUMN_AFTER: {
        let newColumnProps = modifyColumnSizingWithAppendingAndNewColumnProps(APPEND_NEW_COLUMN_BEFORE, appendIndex, dropTarget.children);

        appendingNode = createNodeColumnWithComponent(componentNode, newColumnProps);
        dropTarget.appendChildAfter(appendIndex, appendingNode);
      }
        break;
      case APPEND_NEW_ROW_WITH_COLUMN_BEFORE:
        appendingNode = createNodeRowWithColumnWithComponent(componentNode, {
          xs: 12,
          sm: 12,
          md: 12,
          lg: 12,
        });
        dropTarget.appendChildBefore(appendIndex, appendingNode);
        break;

      case APPEND_NEW_ROW_WITH_COLUMN_AFTER:
        appendingNode = createNodeRowWithColumnWithComponent(componentNode, {
          xs: 12,
          sm: 12,
          md: 12,
          lg: 12,
        });
        dropTarget.appendChildAfter(appendIndex, appendingNode);
        break;

      case INSERT_COLUMN_FIRST:
        appendingNode = createNodeColumnWithComponent(componentNode, {
          xs: 12,
          sm: 12,
          md: 12,
          lg: 12,
        });
        dropTarget.prependChild(appendingNode);
        break;

      case INSERT_COLUMN_LAST:
        appendingNode = createNodeColumnWithComponent(componentNode, {
          xs: 12,
          sm: 12,
          md: 12,
          lg: 12,
        });
        dropTarget.appendChild(appendingNode);
        break;

      case INSERT_ROW_WITH_COLUMN_FIRST :
        appendingNode = createNodeRowWithColumnWithComponent(componentNode, {
          xs: 12,
          sm: 12,
          md: 12,
          lg: 12,
        });
        dropTarget.prependChild(appendingNode);
        break;

      case INSERT_ROW_WITH_COLUMN_LAST :
        appendingNode = createNodeRowWithColumnWithComponent(componentNode, {
          xs: 12,
          sm: 12,
          md: 12,
          lg: 12,
        });
        dropTarget.appendChild(appendingNode);
        break;

      case INSERT_INSIDE:
        appendingNode = componentNode;
        dropTarget.appendChild(appendingNode)
        break;

      case INSERT_INSIDE_WITH_GRID:
        appendingNode = createNodeGridWithRowWithColumnWithComponent(componentNode, {
          xs: 12,
          sm: 12,
          md: 12,
          lg: 12,
        });
        dropTarget.prependChild(appendingNode);
    }



    this.onModifiedDirectiveNode({
      modifiedNode: dropTarget,
      directiveName,
      exportedJSON: directive.exportToJSON(),
      nearby,
    });
  }


  onMouseMoveRaytracingResult(dom) {
    let resultDom = !this.props.combinationMode ? findDirectiveNode(dom) : dom;


    this.setState({
      mouseHoverIFrameDOM: resultDom,
      mouseHoverIFrameDOMreal: dom,
    });
  }

  onMouseDBClickRaytracingResult(dom, selectingPoint) {
    let resultDom = !this.props.combinationMode ? findDirectiveNode(dom) : dom;
    if (!resultDom) return this.resetSelected();
    let componentNodeDOM = resultDom;
    let realDOM = dom;

    let info = {};

    if (isVueComponent(componentNodeDOM)) {
      info = getVueComponentInfo(componentNodeDOM);
    }

    let rootDriectiveDOM = findDirectiveRootNode(componentNodeDOM);
    let directiveName = rootDriectiveDOM.getAttribute(TEMPLATE_JSON_DIRECTIVE_SIGN);
    let rootDirectiveNode = this.props.routeDirectiveJSONTREE_map[directiveName];
    let pos = resultDom.getAttribute(DIRECTIVE_POS_ATTR_SIGN);
    let directiveNode = getDirectiveNodeByPos(rootDirectiveNode, pos);

    let foundSubEditable;
    if (info.editRules && info.editRules.textEditable) {
      let editables = info.editRules.textEditable;


      let subDOM, targetSubDom;
      for (let i = 0; i < editables.length; i++) {
        subDOM = componentNodeDOM.querySelector(editables[i].target);
        targetSubDom = realDOM;
        while (targetSubDom) {
          if (targetSubDom == subDOM) {
            foundSubEditable = editables[i];
            i = 99999; // 높은 수를 지정하여 for문이 종료되도록 한다.
          }

          targetSubDom = targetSubDom.parentNode;
        }
      }
    }


    if (foundSubEditable) {
      let redirectEditDOM = componentNodeDOM.querySelector(foundSubEditable.target);
      console.log('wysiwyg');


      let targetProp = foundSubEditable.connectProp;

      this.setState({
        wysiwygLaunched: true,
        wysiwygTargetDOM: redirectEditDOM,
        wysiwygTargetEditRule : info.editRules.props[targetProp],
        wysiwygApplyFunction: (value) => {
          directiveNode.props = directiveNode.props || {};
          directiveNode.props[foundSubEditable.connectProp] = value;
          this.onModifiedDirectiveNode({directiveName, exportedJSON: rootDirectiveNode.exportToJSON()}, pos)

          this.setState({
            wysiwygLaunched: false,
            wysiwygTargetDOM: null,
            wysiwygApplyFunction: null,
          });
        },
      });
    } else {

    }
  }

  onMouseClickRaytracingResult(dom, selectingPoint) {
    // wysiwyg 모드가 켜져있을 경우에는 먼저 저장 후 선택을 처리한다.
    if (this.state.wysiwygLaunched) {
      this.state.wysiwygApplyFunction(this.wysiwigEditor.getValue());
    }

    let resultDom = !this.props.combinationMode ? findDirectiveNode(dom) : dom;
    if (!resultDom) return this.resetSelected();

    if (!this.props.combinationMode) {
      let rootDriectiveDOM = findDirectiveRootNode(resultDom);
      let directiveName = rootDriectiveDOM.getAttribute(TEMPLATE_JSON_DIRECTIVE_SIGN);


      let directiveJSON = this.props.routeDirectiveJSONTREE_map[directiveName];


      console.log("directiveJSON222",directiveJSON);
      if (!directiveJSON) {

        console.log("not found directive json");
        GET_ROUTE_DIRECTIVE_TREE(this.props.User.session.id, this.props.Site.info.id, this.props.Site.info.owner, directiveName)
          .then((res) => {
            let {data} = res;
            let {treeJSON, serviceFragmentId} = data;

            let directiveName = rootDriectiveDOM.getAttribute('__vue_component_directive');
            let directiveNameFallback = rootDriectiveDOM.getAttribute('__vue_component_directive_fallback');
            let fragmentAcceptComponents = rootDriectiveDOM.getAttribute('data-accept-components') || '';

            fragmentAcceptComponents = fragmentAcceptComponents.split(',').map((compKey) => compKey.trim());

            console.log("directiveJSON222data",data);

            this.context.elastic.dispatch(actions.LOADED_ROUTE_DIRECTIVE_TREE(
              VueNuxtNode.importFromJSON(treeJSON),
              directiveName,
              null,
              null,
              serviceFragmentId,
              fragmentAcceptComponents
            ), true)
              .then(() => {
                this.onMouseClickRaytracingResult(dom, selectingPoint);
              })
          });
        return;
      }

      let pos = resultDom.getAttribute(DIRECTIVE_POS_ATTR_SIGN);
      let directiveNode = getDirectiveNodeByPos(directiveJSON, pos);

      console.log(directiveName, directiveNode, directiveJSON);

      this.selectDirectiveNodeDelegate(directiveName, directiveNode, pos, selectingPoint);
    } else {
      // this.context.elastic.dispatch(actions.RESET_DIRECTIVE_NODE());
      // this.context.elastic.dispatch(actions.SELECT_NODE_OF_COMPONENT(componentPath, pos));
    }
  }

  clickComponentHandle(dom, editProp, key) {
    let resultDom = findDirectiveNode(dom);
    let rootDriectiveDOM = findDirectiveRootNode(resultDom);
    let directiveName = rootDriectiveDOM.getAttribute(TEMPLATE_JSON_DIRECTIVE_SIGN);
    let directiveNodePos = dom.getAttribute(DIRECTIVE_POS_ATTR_SIGN);


    let directiveJSON = this.props.routeDirectiveJSONTREE_map[directiveName];
    let directiveNode = getDirectiveNodeByPos(directiveJSON, directiveNodePos);


    let applyKey = this.context.UIISystems.getUnique();
    this.context.UIISystems.openTool('Curator', '[modal]', {
        width: 600,
        height: 500,
        DISABLE_DUPLICATE_UNDEPLOY: true,
        UNIQUE_LAYER: true,
      },
      {
        applyFunctionKey: applyKey,
        curationMethodSheet: editProp,
        oldCurationParams: directiveNode.props ? (directiveNode.props[key] || {}) : {},
        componentProps: directiveNode.props || {},
      }).then((closer) => {
      this.context.functionStore.register(applyKey, ({madeParams, item, id}) => {
        let descentList = directiveNode.getLinealDescentList();
        let rootNode = descentList[0];
        directiveNode.props[key] = id;


        this.context.events.emit('MODIFIED-DIRECTIVE-NODE', {
          exportedJSON: rootNode.exportToJSON(),
          directiveNode,
          directiveName,
          directiveNodePos,
        });
        closer();

        setTimeout(() => {
          this.context.events.emit('CANVAS-DISPLAY-COMPONENT-REFRESH', {
            componentPos: directiveNode.pos,
            componentName: directiveNode.tag,
            directiveName: directiveName,
            refreshMethod: editProp.typeStructure.refreshMethod,
          })
        }, 100);
      });
    });
  }

  onContextMenuRaytracingResult(dom, selectingPoint) {
    let resultDom = !this.props.combinationMode ? findDirectiveNode(dom) : dom;
    if (!resultDom) return this.resetSelected();

    if (!this.props.combinationMode) {
      let rootDriectiveDOM = findDirectiveRootNode(resultDom);
      let directiveName = rootDriectiveDOM.getAttribute(TEMPLATE_JSON_DIRECTIVE_SIGN);
      let directiveJSON = this.props.routeDirectiveJSONTREE_map[directiveName];
      let pos = resultDom.getAttribute(DIRECTIVE_POS_ATTR_SIGN);
      let directiveNode = getDirectiveNodeByPos(directiveJSON, pos);


      this.selectDirectiveNodeDelegate(directiveName, directiveNode, pos, selectingPoint, true);
    } else {
      // this.context.elastic.dispatch(actions.RESET_DIRECTIVE_NODE());
      // this.context.elastic.dispatch(actions.SELECT_NODE_OF_COMPONENT(componentPath, pos));
    }
  }

  resetSelected() {
    this.context.elastic.dispatch(actions.RESET_DIRECTIVE_NODE());
  }

  closeContextMenu(){
    this.context.elastic.dispatch(actions.CLOSE_CONTEXT_MENU());
  }

  onMouseLeave() {
    this.setState({
      mouseHoverIFrameDOM: null,
    })
  }

  loadRouteDirectiveTree() {
    this.context.elastic.dispatch(actions.LOAD_ROUTE_DIRECTIVE_TREE(this.props.staticData.routeDirective));


    GET_ROUTE_DIRECTIVE_TREE(this.props.User.session.id, this.props.Site.info.id, this.props.Site.info.owner, this.props.staticData.routeDirective)
      .then((res) => {
        let {data} = res;
        let {treeJSON} = data;

        this.context.elastic.dispatch(actions.LOADED_ROUTE_DIRECTIVE_TREE(VueNuxtNode.importFromJSON(treeJSON), this.props.staticData.routeDirective));
      });
  }


  iframeBoundingRect(){
    let areaWidth = this.props.width;
    let iframeWidth = this.getIframeWidth();

    if (iframeWidth > areaWidth) {
      iframeWidth = areaWidth;
    }

    let iframeLeftOffset = areaWidth > iframeWidth ? (areaWidth - iframeWidth) / 2 : 0;

    return {
      bottom: this.props.height + this.props.top,
      height: this.props.height,
      left: iframeLeftOffset + this.props.left,
      right: iframeLeftOffset + iframeWidth + this.props.left,
      top: this.props.top,
      width: iframeWidth,
      x: iframeLeftOffset + this.props.left,
      y: this.props.top,
    }
  }

  iframeScrollTop(){
    if(this.iframe){
      return this.iframe.contentDocument.querySelector('html').scrollTop;
    }
    return 0;
  }

  getDirectiveRootDoms(){
    let iframe = this.iframe;
    if( iframe && iframe.contentWindow && iframe.contentWindow.document ){
      let arr = [];
      let doms =  findRootDirectiveVueComponent(iframe.contentWindow.document);
      for(let i = 0; i < doms.length; i++ ){
        arr.push(doms[i]);
      }

      return arr;
    }

    return [];
  }

  componentDidMount() {
    let superDOM = ReactDOM.findDOMNode(this);
    let iframe = this.iframe;
    let iframeWidth = this.iframe.offsetWidth;
    let iframeHeight = this.iframe.offsetHeight;
    let iframeLeft = this.iframe.offsetLeft;
    let iframeTop = this.iframe.offsetTop;

    this.setState({
      mounted: true,
      superDOM: superDOM,
    });


    this.eventListening();

    window.addEventListener("resize", () => this.forceUpdate());

    this.context.elastic.dispatch(actions.SET_ROUTE_DIRECTIVE_NAME(this.props.staticData.routeDirective));


    this.iframeSrcMutaionOp = new MutationObserver((mutations) => {
      mutations.some((mutation) => {
        if (mutation.type === 'attributes' && mutation.attributeName === 'src') {
          console.log(mutation);
          console.log('Old src: ', mutation.oldValue);
          console.log('New src: ', mutation.target.src);

          this.context.global.uiBlockingReqBegin('page-load')

          return true;
        }

        return false;
      });
    }).observe(iframe, {
      attributes: true,
      attributeFilter: ['src'],
      attributeOldValue: true,
      characterData: false,
      characterDataOldValue: false,
      childList: false,
      subtree: true,
    });

    iframe.addEventListener('load', ()=>{
      this.context.global.uiBlockingReqFinish('page-load');
    })


    this.context.ice2.request({
      url : '/node/layout/list.json',
      method : 'get',
    }).then((res) => {

      let list = res.data.items || [];

      list = list.map((item) => {


        let editRule = {};


        try {
          let func = new Function(`return ${item.editrule}`);
          // console.log(func, `return ${item.editrule}`);
          editRule = func();

          // console.log(editRule)

        } catch(e){

        }

        return {
          ...item,
          editRuleObject : editRule,
        }
      });

      this.setState({
        metaComponents : list,
      })
    });
  }

  componentWillUnmount() {
    this.eventListeningClear();
  }


  componentDidUpdate(prevProps) {

    // if (!this.props.routeDirectiveJSONTREE_map[this.props.staticData.routeDirective]) {
    //   if (!this.props.routeDirectiveJSONTREE_loading_state_map[this.props.staticData.routeDirective])
    //     this.loadRouteDirectiveTree();
    // }
  }

  renderRemoteControl() {
    if (this.state.mounted && this.state.iframeLoaded) {
      let areaWidth = this.props.width;
      let x = this.props.remoteConX;
      let controllerWidth = 200;

      if (x === null) {
        x = areaWidth - controllerWidth;
      } else if (x > areaWidth - controllerWidth) {
        x = areaWidth - controllerWidth;
      } else if (x < 0) {
        x = 1;
      }


      return <RemoteControl
        iframe={this.iframe}
        mode={this.props.Studio.mode}
        height={600}
        width={controllerWidth}
        x={x}
        y={this.props.remoteConY}
        areaWidth={areaWidth}
        combinationMode={this.props.combinationMode}
        dispatch={this.context.elastic.dispatch}

        mainDirectiveName={this.props.staticData.routeDirective}
        directivesMap={this.props.routeDirectiveJSONTREE_map}
      />
    }
  }


  render() {
    let {AssignedStore, Site, Studio, staticData, DragSystem, aimedDraggableType, aimedDraggableData, draggableAimed} = this.props;
    let {hoveringZoneID, aimX, aimY, draggableType, draggableData} = DragSystem.dragState;

    let itemHovering = hoveringZoneID === this.context.zone.id;
    // console.log( itemHovering, aimX, aimY, draggableType, draggableData);


    const usp = new URLSearchParams();
    const inputQueries = Studio.pageInfo.queries || {};
    const queryKeys = Object.keys(inputQueries);

    for(let i = 0; i < queryKeys.length; i++ ){
      usp.append(queryKeys[i], inputQueries[queryKeys[i]]);
    }

    // console.log(Studio.pageInfo.query)
    let src = '//' +
      [
        // Studio.session.id,
        location.hostname,
      ].join('.') +
      (location.port ? ":" + location.port:'') +
      (staticData.path || Studio.pageInfo.path ) + '?' +
      `__s__=${Studio.session.id}` + '&' +
      usp.toString();

    console.log(src);
    // src = `//local.jejudfs.com:3999/${staticData.path  || Studio.pageInfo.path }?__s__=${Studio.session.id}&${usp.toString()}`




    let areaWidth = this.props.width;
    let iframeWidth = this.getIframeWidth();

    if (iframeWidth > areaWidth) {
      iframeWidth = areaWidth;
    }

    let iframeLeftOffset = areaWidth > iframeWidth ? (areaWidth - iframeWidth) / 2 : 0;


    let selectedDirectiveNode = this.getSelectedNode();


    let iframeBoundingRect = this.iframeBoundingRect();
    let iframeScrollTop = this.iframeScrollTop();

    let directiveRootDoms = this.getDirectiveRootDoms().filter((dom) => !dom.hasAttribute('fragment-readonly'));




    return (
      <Tool className="canvas" ref={(dom) => this.rootDOM = dom}>
        <div className={classnames("canvas-menu-wrapper")}>
          <button
            className={classnames("canvas-menu-item", "show", this.state.showMenus && 'active')}
            title="trigger"
            onClick={() => this.setState({showMenus: !this.state.showMenus})}>
            {this.state.showMenus ? <NavigationCloseIcon style={{color: 'inherit'}}/> :
              <NavigationMenuIcon style={{color: 'inherit'}}/>}
          </button>


          {/*<button*/}
            {/*className={classnames("canvas-menu-item", this.state.showMenus && 'show', this.props.combinationMode && 'active')}*/}
            {/*style={{right: this.state.showMenus && 50}}*/}
            {/*onClick={(e) => this.onToggleElementType(e, !this.props.combinationMode)}*/}
            {/*title="Combination Mode">*/}
            {/*<ImageGrainIcon style={{color: 'inherit'}}/>*/}
          {/*</button>*/}
          <button
            className={classnames("canvas-menu-item", this.state.showMenus && 'show', this.props.showMiniController && 'active')}
            style={{right: this.state.showMenus && 50}}
            onClick={(e) => this.onToggleMiniController(e, !this.props.showMiniController)}
            title="Mini Controller">
            <ImagePaletteIcon style={{color: 'inherit'}}/>
          </button>
          <button
            className={classnames("canvas-menu-item", this.state.showMenus && 'show', this.props.dropFocusing && 'active')}
            style={{right: this.state.showMenus && 100}}
            onClick={(e) => this.onToggleDropFocusing(e, !this.props.dropFocusing)}
            title="trigger">
            <ImageCenterFocusWeakIcon style={{color: 'inherit'}}/>
          </button>
          {/*<button*/}
            {/*className={classnames("canvas-menu-item", this.state.showMenus && 'show', this.props.showNodeGraph && 'active')}*/}
            {/*style={{right: this.state.showMenus && 200}}*/}
            {/*onClick={(e) => this.onToggleGraph(e, !this.props.showNodeGraph)}*/}
            {/*title="trigger">*/}
            {/*<EditorChartIcon style={{color: 'inherit'}}/>*/}
          {/*</button>*/}
          <button
            className={classnames("canvas-menu-item", this.state.showMenus && 'show')}
            style={{right: this.state.showMenus && 150}}
            onClick={(e) => window.open(src, '_blank', `width=${iframeWidth}, height=${window.innerHeight}`)}
            title="preview">
            <HardwareCastConnectedIcon style={{color: 'inherit'}}/>
          </button>
          {/*<button*/}
            {/*className={classnames("canvas-menu-item", this.state.showMenus && 'show')}*/}
            {/*style={{right: this.state.showMenus && 300}}*/}
            {/*onClick={(e) => this.openDirectiveJSONEditor()}*/}
            {/*title="directive json edit">*/}
            {/*<ActionCodeIcon style={{color: 'inherit'}}/>*/}
          {/*</button>*/}
        </div>

        <div className={classnames("information", this.state.informationBottom && 'bottom')}>
          <div
            className="information-bar"
            onMouseEnter={() => this.setState({informationBottom: !this.state.informationBottom})}>
              {this.props.iframeState.title || staticData.name} {this.props.iframeState.path || staticData.path}
          </div>
        </div>


        <div className="render-screen">
          <iframe
            style={{width: iframeWidth, left: iframeLeftOffset}}
            ref={(iframe) => {
              this.iframe = iframe
            }}
            className={classnames('renderer')}
            src={src + '&hideadmin=true'}
            onLoad={:: this.frameOnLoad}
          ></iframe>

          {this.state.mounted && this.state.iframeLoaded &&
          <GuideLineLayer
            iframe={this.iframe}
            iframeBoundingRect={iframeBoundingRect}
            directiveRootDoms={directiveRootDoms}/>}


          <OverlayInterfaceLayer
            iframe={this.iframe}

            selectedDirectiveName={this.props.selectedDirectiveName}
            selectedDirectiveNode={selectedDirectiveNode}
            mainDirectiveRootNode={this.props.routeDirectiveJSONTREE_map[this.props.staticData.routeDirective]}
            mainDirectiveName={this.props.staticData.routeDirective}
            fragmentInfoMap={this.props.fragmentInfoMap}
            getNodeEditingRules={:: this.getNodeEditingRules}
            metaComponents={ this.state.metaComponents }
            iframeBoundingRect={iframeBoundingRect}
            iframeScrollTop={iframeScrollTop}
            onUndoDirective={(d) => this.onHistoryMove(d, 'back')}
            onRedoDirective={(d) => this.onHistoryMove(d, 'go')}


            dropFocusing={this.props.dropFocusing}
            draggableAimed={draggableAimed}
            onWheel={:: this.onWheel}
            onComponentDragging={:: this.onComponentDragging}
            onComponentDrop={:: this.onComponentDrop}
            onMouseDBClickRaytracingResult={:: this.onMouseDBClickRaytracingResult}
            onMouseMoveRaytracingResult={:: this.onMouseMoveRaytracingResult}
            onMouseClickRaytracingResult={:: this.onMouseClickRaytracingResult}
            onContextMenuRaytracingResult={:: this.onContextMenuRaytracingResult}
            onMouseLeave={:: this.onMouseLeave}
            directiveRootDoms={directiveRootDoms}
            onClickDirectiveApply={:: this.onClickDirectiveApply }
            clickComponentHandle={:: this.clickComponentHandle}/>


          {this.state.mounted && this.state.iframeLoaded && this.state.mouseHoverIFrameDOM &&
          <FocusingBox
            iframeBoundingRect={iframeBoundingRect}
            mouseHoverIFrameDOM={this.state.mouseHoverIFrameDOM}
            mouseHoverIFrameDOMReal={this.state.mouseHoverIFrameDOMreal}
            getNodeEditingRules={:: this.getNodeEditingRules}
            iframe={this.iframe}
            combinationMode={this.props.combinationMode}/>}

          {this.state.mounted && this.state.iframeLoaded && this.state.mouseHoverIFrameDOM &&
          <FocusingLabel
            iframeBoundingRect={iframeBoundingRect}
            mouseHoverIFrameDOM={this.state.mouseHoverIFrameDOM}
            mouseHoverIFrameDOMReal={this.state.mouseHoverIFrameDOMreal}
            getNodeEditingRules={:: this.getNodeEditingRules}
            iframe={this.iframe}
            combinationMode={this.props.combinationMode}/>}

          {this.state.mounted && this.state.iframeLoaded && this.props.showNodeGraph &&
          <NodeGraphLayer
            iframe={this.iframe}
            iframeBoundingRect={iframeBoundingRect}
            combinationMode={this.props.combinationMode}
            nodeGraphOverlayMode={this.props.nodeGraphOverlayMode}
            graphTree={this.props.routeDirectiveJSONTREE_map[this.props.selectedDirectiveName]}
            graphLoadingState={this.props.routeDirectiveJSONTREE_loading_state_map[this.props.selectedDirectiveName]}
            width={this.state.superDOM.offsetWidth}
            selectedDirectivePos={this.props.selectedDirectiveNodePos}/>}


          {this.state.mounted &&
          this.state.iframeLoaded &&
          !this.props.combinationMode &&
          this.props.selectedDirectiveNodePos &&
          // this.props.Studio.mode != 'curation' &&
          !this.state.wysiwygLaunched &&
          <DirectiveSelection
            iframe={this.iframe}
            mode={this.props.Studio.mode}
            mainDirectiveName={this.props.staticData.routeDirective}
            directivesMap={this.props.routeDirectiveJSONTREE_map}
            iframeBoundingRect={iframeBoundingRect}
            directiveName={this.props.selectedDirectiveName}
            directiveNode={selectedDirectiveNode}
            directiveNodePos={this.props.selectedDirectiveNodePos}
            selectingPoint={this.props.selectingPoint}

            onCloseSelected={:: this.resetSelected }
            onJumpToParent={:: this.selectParentDirective}
            onOpenGridEditor={:: this.onOpenGridEditor}
            onOpenSettings={:: this.onOpenSettings}
            onDeleteSelectedNode={:: this.onDeleteSelectedNode}

            showContextMenu={this.props.showContextMenu}
          />}

          {this.state.mounted && this.state.iframeLoaded && !this.props.combinationMode && draggableAimed &&
          <ComponentDragDropDirectiveLayer
            iframe={this.iframe}
            x={this.props.DragSystem.dragState.aimX}
            y={this.props.DragSystem.dragState.aimY}
            iframeBoundingRect={iframeBoundingRect}
            mainDirectiveName={this.props.staticData.routeDirective}
            directivesMap={this.props.routeDirectiveJSONTREE_map}

            dragOverDirectiveName={this.props.dragOverDirectiveName}
            dragOverDirectiveNode={this.props.dragOverDirectiveNode}
            dragOverDirectiveNearbyRegion={this.props.dragOverDirectiveNearbyRegion}

            selectedDirectiveName={this.props.selectedDirectiveName}
            selectedDirectiveNode={this.props.dropFocusing && selectedDirectiveNode}
            draggableAimed={draggableAimed}
            areaWidth={areaWidth}
          />}

          {/*{this.props.showMiniController && this.renderRemoteControl()}*/}

          {this.props.showContextMenu &&
          <RightContextMenu
            iframe={this.iframe}
            areaWidth={areaWidth}
            iframeBoundingRect={iframeBoundingRect}
            selectingPoint={this.props.selectingPoint}
            selectedDirectiveName={this.props.selectedDirectiveName}
            selectedDirectiveNode={selectedDirectiveNode}
            rootNodeOfSelectedDirectiveNode={this.props.routeDirectiveJSONTREE_map[this.props.selectedDirectiveName]}

            onModifiedDirectiveNode={::this.onModifiedDirectiveNode}
            onClose={:: this.closeContextMenu}
            onOpenGridEditor={:: this.onOpenGridEditor}
            onOpenSettings={:: this.onOpenSettings}
            onJumpToParent={:: this.selectParentDirective}
            onDeleteSelectedNode={:: this.onDeleteSelectedNode}
            onApplyPart={:: this.onApplyPart}
            onApplyDirectiveFull={:: this.onApplyDirectiveFull}
            onFragmentVersionManaging={:: this.onFragmentVersionManaging}
            onTemplateManaging={:: this.onTemplateManaging}
            onSaveAsTemplate={:: this.onSaveAsTemplate}/>}

          {this.state.wysiwygLaunched &&
          <WysiwigEditor
            onWheel={::this.onWheel}
            iframeBoundingRect={iframeBoundingRect}
            ref={(wysiwigEditor) => this.wysiwigEditor = wysiwigEditor}
            wysiwygTargetDOM={this.state.wysiwygTargetDOM}
            wysiwygTargetEditRule={this.state.wysiwygTargetEditRule}
            iframe={this.iframe}/>}
        </div>
      </Tool>
    )
  }
}

export default Canvas;
