import namespaceBindGenerator from '../../../../utils/redux/namespaceBindGenerator';
let defineType = namespaceBindGenerator('CanvasTool');


export const TOGGLE_EDITING_MODE = defineType('TOGGLE_EDITING_MODE');
export const TOGGLE_MINI_CONTROLLER = defineType('TOGGLE_MINI_CONTROLLER');
export const TOGGLE_DROP_FOCUSING = defineType('TOGGLE_DROP_FOCUSING');
export const TOGGLE_NODE_GRAPH = defineType('TOGGLE_NODE_GRAPH');
export const LOADED_ROUTE_DIRECTIVE_TREE = defineType('LOADED_ROUTE_DIRECTIVE_TREE');
export const LOAD_ROUTE_DIRECTIVE_TREE = defineType('LOAD_ROUTE_DIRECTIVE_TREE');
export const BIND_DIRECTIVE_FRAGMENT_ID = defineType('BIND_DIRECTIVE_FRAGMENT_ID');
export const HISTORY_MOVE = defineType('HISTORY_MOVE');

export const SELECT_DIRECTIVE_NODE = defineType('SELECT_DIRECTIVE_NODE');
export const RESET_DIRECTIVE_NODE = defineType('RESET_DIRECTIVE_NODE');
export const CLOSE_CONTEXT_MENU = defineType('CLOSE_CONTEXT_MENU');

export const SELECT_NODE_OF_COMPONENT = defineType('SELECT_NODE_OF_COMPONENT');
export const RESET_SELECTED_NODE_OF_COMPONENT = defineType('RESET_SELECTED_NODE_OF_COMPONENT');

export const SET_ROUTE_DIRECTIVE_NAME = defineType('SET_ROUTE_DIRECTIVE_NAME');

export const SET_IFRAME_STATE = defineType('SET_IFRAME_STATE');
export const SET_BODY_HTML_DIRECTIVE_NODE = defineType('SET_BODY_HTML_DIRECTIVE_NODE');


export const COMPONENT_DRAGGING_OVER_DIRECTIVE = defineType('COMPONENT_DRAGGING_OVER_DIRECTIVE');
export const COMPONENT_DRAGGING_OVER_COMPONENT = defineType('COMPONENT_DRAGGING_OVER_COMPONENT');


export const MOVE_REMOTE_CONTROL = defineType('MOVE_REMOTE_CONTROL');

export const LAUNCH_WYSIWYG_EDITOR = defineType('LAUNCH_WYSIWYG_EDITOR');
