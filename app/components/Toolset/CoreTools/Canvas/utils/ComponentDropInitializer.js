import isEmpty from 'lodash/isEmpty';

import {
  CreateOpenActionSheet as CreateSettingsOpenActionSheet,
  ToolKey,
} from '../../Settings';
import {
  CLOSE_LAYOUT_WITH_ELASTIC,
} from '../../../UIISystemActionSheets';

export default class ComponentDropInitializer {
  constructor(canvas, componentNode, options){
    let {
      onPassDrop,
      onCancel,
    } = options;

    this.canvas = canvas;
    this.componentNode = componentNode;


    let editingRule = canvas.getNodeEditingRules(componentNode.tag);

    if( !editingRule ){
      return onPassDrop(componentNode);
    } else {
      if( isEmpty(editingRule.props) ){
        return onPassDrop(componentNode);
      }
    }


    // defaultProps 세팅
    componentNode.props = componentNode.props || {};
    let propKeys = Object.keys(editingRule.props);
    for( let i =0; i < propKeys.length; i++ ){
      if( editingRule.props[propKeys[i]].default ){

        if( typeof editingRule.props[propKeys[i]].default == 'function' ){
          componentNode.props[propKeys[i]] = editingRule.props[propKeys[i]].default();
        } else {
          componentNode.props[propKeys[i]] = editingRule.props[propKeys[i]].default;
        }
      }

    }


    let layoutID;
    canvas.context.functionStore.register('confirm', (btnOption, componentNode)=>{
      // require 체크

      onPassDrop(componentNode);
      canvas.context.UIISystems.emitActionSheet(CLOSE_LAYOUT_WITH_ELASTIC(layoutID, ToolKey, true));
    });

    canvas.context.functionStore.register('cancel', (resultComponentProps,  componentNode)=>{
      // require 체크

      canvas.context.UIISystems.emitActionSheet(CLOSE_LAYOUT_WITH_ELASTIC(layoutID, ToolKey, true));
    });

    canvas.context.UIISystems.emitActionSheet(CreateSettingsOpenActionSheet("[modal]"), {
      configurationTargetMethod : 'virtual',
      configurationRules : editingRule,
      virtualComponentNode : componentNode,


      closeHookFunctionKey : 'cancel',
      footerButtons : [
        {
          title : '확인',
          functionKey : 'confirm',
        },

        {
          title : '취소',
          functionKey : 'cancel',
        },
      ],
    }, {
      width : 500,
      height: 600,
      onLayoutID(id){
        layoutID = id;
      },
    });
  }


}
