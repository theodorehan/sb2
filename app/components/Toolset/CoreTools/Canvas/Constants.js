import defineUIIStatement from '../../../../utils/UII/defineUIIStatement';
import defineActSheet, { Types } from '../../../../utils/UII/actSheet';

export const ToolKey = "Canvas";
export const CreateOpenActionSheet = (areaIndicator) => defineActSheet({
  "action" : "open",
  "type": Types.ToolInterfacePipeline, // ToolInterfacePipeline
  "options" : {
    "toolKey" : ToolKey,
    "areaIndicator" : areaIndicator,
  },
});

export const Info = {
  type : 'button',
  key: 'Canvas',
  title: 'Canvas',
  iconType: 'fa',
  icon: 'icon',
  options: {
    toolKey : ToolKey,
  },

  actionSheet : CreateOpenActionSheet('central'),
}

export const SPAWN_NEW_ICE_FRAGMENT_KEY = 'SPAWN_NEW_ICE_FRAGMENT:';
export const ROUTE_MODIFIED_EVENT_KEY_BASE = 'FLASH_MODIFIED_DIRECTIVE:';
export const EDITING_SERVICE_GLOBAL_EVENT_EMITTER_KEY = '_STUDIO_COMMUNICATION_EVENT_SYSTEM';
export const SERVICE_FLASH_RENDERED_EVENT_KEY = 'FLASH_RENDERED';

export const UIIStatement = defineUIIStatement(Info);
