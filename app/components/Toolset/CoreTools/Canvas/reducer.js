import * as actionTypes from './actionTypes';
import parseUrl from 'url-parse';
import cloneDeep from 'lodash/cloneDeep';


export const initialState = initializeStateFilter({
  combinationMode : false,
  showNodeGraph : false,
  routeDirectiveJSONTREE_map : {},
  routeDirectiveJSONTREE_history_map : {},
  routeDirectiveJSONTREE_history_cursor_map: {},
  routeDirectiveJSONTREE_loading_state_map : {},
  fragmentInfoMap : {},
  routeDirectiveName : null,

  selectedDirectiveName : null,
  selectedDirectiveNodePos : null,
  selectedDirectiveNodeEditRules : null,

  // selectedDirectiveNode : null,
  selectClickPoint: null,
  showContextMenu : false,
  dropFocusing : false,

  // drag states
  ...{
    // dragOverDirective
    dragOverDirectiveName : null,
    dragOverDirectiveNode : null,
    dragOverDirectiveNearbyRegion : null,

    // dragOverNodeOfComponent
    dragOverComponentName : null,
    dragOverNodeOfComponent : null,
    dragOverComponentNearbyRegion : null,

    // drag component
    dragOverComponentInfo : null,
  },

  // remote control states
  ...{
    showMiniController : false,
    remoteConX : 30,
    remoteConY : 30,
  },


  selectedNodeOfComponent: null, // 선택된 컴포넌트의 노드
  selectedComponentPath : null,
  selectedRootComponentNodePos: null, // Directive Node Pos. body로 부터 생성된 componentNode 트리상의 pos 값

  iframeState : {
    totalWidth: 0,
    totalHeight : 0,
    screenWidth : 0,
  },

  bodyHTMLDirectiveNode : null,


  iframeLocationInfo : {
    path : '',
    title : '',
  },
});



function initializeStateFilter(initialState){
  let parsedURL = parseUrl(location.href, true);

  let state = Object.assign({}, initialState);

  let {
    canvastools = '',
  } = parsedURL.query;


  if( canvastools ){
    // state.layoutUsingMap = {};
    canvastools = canvastools.split(',');
    let useContentsPalette = canvastools.find((a)=> a =='contents-palette');
    if( useContentsPalette ){
      state.showMiniController = true;
    }
  }


  return state;
}

export default function CanvasReducer(state = initialState, action) {

  let nextState;
  switch (action.type) {
    case actionTypes.TOGGLE_EDITING_MODE :
      return Object.assign({}, state, {
        combinationMode: action.checked,
      });

    case actionTypes.TOGGLE_NODE_GRAPH :
      return Object.assign({}, state, {
        showNodeGraph: action.checked,
      });

    case actionTypes.TOGGLE_DROP_FOCUSING :
      return Object.assign({}, state, {
        dropFocusing: action.checked,
      });

    case actionTypes.TOGGLE_MINI_CONTROLLER:
      return Object.assign({}, state, {
        showMiniController : action.checked,
      });

    case actionTypes.LOAD_ROUTE_DIRECTIVE_TREE :
      return Object.assign({}, state, {
        routeDirectiveJSONTREE_loading_state_map : {
          ...state.routeDirectiveJSONTREE_loading_state_map,
          [action.directiveName] : 'loading',
        },
      });

    case actionTypes.LOADED_ROUTE_DIRECTIVE_TREE :
      nextState = Object.assign({}, state, {
        routeDirectiveJSONTREE_map : {
          ...state.routeDirectiveJSONTREE_map,
          [action.directiveName] : action.treeJSON,
        },

        routeDirectiveJSONTREE_loading_state_map : {
          ...state.routeDirectiveJSONTREE_loading_state_map,
          [action.directiveName] : 'loaded',
        },

        fragmentInfoMap : {
          ...state.fragmentInfoMap,
          [action.directiveName] : {
            ...state.fragmentInfoMap[action.directiveName],
            id : action.serviceFragmentId,
            fragmentAcceptComponents : action.fragmentAcceptComponents ? action.fragmentAcceptComponents : state.fragmentInfoMap[action.directiveName].fragmentAcceptComponents ,
          },
        },


        selectedDirectiveName : action.selectedDirectiveName || state.selectedDirectiveName,
        selectedDirectiveNodePos : action.selectDirectiveNodePos || state.selectedDirectiveNodePos,
      });

      let currentStack = state.routeDirectiveJSONTREE_history_map[action.directiveName] || [];
      let currentCursor = state.routeDirectiveJSONTREE_history_cursor_map[action.directiveName] || 0;
      if( action.historyAction == 'push' ){

        nextState.routeDirectiveJSONTREE_history_map = {
          ...state.routeDirectiveJSONTREE_history_map,
          [action.directiveName] : [...currentStack.slice(0, currentCursor), cloneDeep(action.treeJSON.exportToJSON())],
        };

        nextState.routeDirectiveJSONTREE_history_cursor_map = {
          ...state.routeDirectiveJSONTREE_history_cursor_map,
          [action.directiveName] : currentCursor +1,
        };
      }



      return nextState;
    case actionTypes.HISTORY_MOVE:
      return Object.assign({}, state, {
        routeDirectiveJSONTREE_history_cursor_map : {
          ...state.routeDirectiveJSONTREE_history_cursor_map,
          [action.directiveName] : action.cursor,
        },
      });
    case actionTypes.BIND_DIRECTIVE_FRAGMENT_ID:
      return Object.assign({}, state, {

        fragmentInfoMap : {
          ...state.fragmentInfoMap,
          [action.directiveName] : {
            ...state.fragmentInfoMap[action.directiveName],
            id : action.serviceFragmentId,
          },
        },
      });
    case actionTypes.SELECT_DIRECTIVE_NODE :
      return Object.assign({}, state, {
        selectedDirectiveName : action.directiveName,
        selectedDirectiveNodePos : action.directiveNodePos,
        // selectedDirectiveNode : action.directiveNode,
        selectingPoint : action.selectingPoint || state.selectingPoint,
        showContextMenu : action.withContextMenu || false,
        selectedDirectiveNodeEditRules : action.componentEditRules,
      });

    case actionTypes.RESET_DIRECTIVE_NODE :
      return Object.assign({}, state, {
        selectedDirectiveName : null,
        selectedDirectiveNodePos : null,
        // selectedDirectiveNode : null,
        showContextMenu : false,
        selectedDirectiveNodeEditRules : null,
      });


    case actionTypes.CLOSE_CONTEXT_MENU:
      return Object.assign({}, state, {
      showContextMenu : false,
    });

    case actionTypes.SELECT_NODE_OF_COMPONENT:
      return Object.assign({}, state, {
        selectedNodeOfComponent : action.selectedNodeOfComponent,
        selectedRootComponentNodePos : action.selectedRootComponentNodePos,
        selectedComponentPath : action.selectedComponentPath,
      });

    case actionTypes.RESET_SELECTED_NODE_OF_COMPONENT :
      return Object.assign({}, state, {
        selectedNodeOfComponent : null,
        selectedRootComponentNodePos : null,
        selectedComponentPath : null,
      });

    case actionTypes.SET_IFRAME_STATE :
      return Object.assign({}, state, {
        iframeState : Object.assign({}, state.iframeState, {
          totalWidth: action.totalWidth,
          totalHeight : action.totalHeight,
          screenWidth : action.screenWidth,
          path : action.path || state.iframeState.path,
          title : action.title || state.iframeState.title ,
        }),
      })

    case actionTypes.SET_BODY_HTML_DIRECTIVE_NODE :
      return Object.assign({}, state, {
        bodyHTMLDirectiveNode : action.node,
      });

    case actionTypes.SET_ROUTE_DIRECTIVE_NAME :
      return Object.assign({}, state, {
        routeDirectiveName : action.directiveName,
      });

    case actionTypes.COMPONENT_DRAGGING_OVER_DIRECTIVE :
      return Object.assign({}, state, {
        dragOverDirectiveName : action.dragOverDirectiveName,
        dragOverDirectiveNode : action.dragOverDirectiveNode,
        dragOverDirectiveNearbyRegion : action.nearby,
        dragOverComponentInfo : action.componentJSON,
      });

    case actionTypes.COMPONENT_DRAGGING_OVER_COMPONENT :
      return Object.assign({}, state, {
        dragOverComponentName : action.dragOverComponentName,
        dragOverNodeOfComponent : action.dragOverNodeOfComponent,
        dragOverComponentNearbyRegion : action.nearby,
        dragOverComponentInfo : action.componentJSON,
      });

    case actionTypes.MOVE_REMOTE_CONTROL :
      return Object.assign({}, state, {
        remoteConX : action.x,
        remoteConY : action.y,
      })
    default :
      return state;
  }
}
