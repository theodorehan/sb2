import defineUIIStatement from '../../../../utils/UII/defineUIIStatement';
import defineActSheet, { Types } from '../../../../utils/UII/actSheet';

export const ToolKey = "Settings";

export const CreateOpenActionSheet = (areaIndicator) => defineActSheet({
  "action" : "open",
  "type": Types.ToolInterfacePipeline, // ToolInterfacePipeline
  "options" : {
    "toolKey" : ToolKey,
    "areaIndicator" : areaIndicator,
  },
});


export const Info = {
  type : 'button',
  key : 'Settings',
  title : 'app.tool.settings',
  titleType : 'code',
  iconType: 'material',
  icon: 'ActionSettings',
  // iconColor : '#fdfdfd',
  options: {
    toolKey : ToolKey,
  },

  actionSheet : CreateOpenActionSheet('[chaining]'),
}



export const UIIStatement = defineUIIStatement(Info);




export const defaultStaticData = {
  configurationTargetMethod : 'canvas-selected', // or virtual
  // configurationApplyMethod : 'tick', // or output
  configurationRules : null, // or EditRulesJSON of component
}
