import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import classnames from 'classnames';
import _ from 'lodash';

React.PropTypes = PropTypes;


// Require Editor JS files.
import 'froala-editor/js/froala_editor.pkgd.min.js';

// Require Editor CSS files.
import 'froala-editor/css/froala_style.min.css';
import 'froala-editor/css/froala_editor.pkgd.min.css';


import FroalaEditor from 'react-froala-wysiwyg';

// let momom = require('moment');
// momom.isMoment = momom.defaults.isMoment;

import { Select, Popover, InputNumber, Radio,Input, Icon } from 'antd';
import DatePicker from '../../../DatePicker';

// const RangePicker = require('antd/dist/antd').DatePicker.RangePicker;
// import {DatetimePicker} from 'rc-datetime-picker';
// function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
// const RangePicker  = _interopRequireDefault(require('./AntRangePicker')).default;
import moment from 'moment';
// console.dir(require('moment'))
// console.dir(RangePicker);

const Option = Select.Option;
const { TextArea } = Input;
import Toggle from 'material-ui/Toggle';
const RadioGroup = Radio.Group;

import getByPath from 'lodash/get';
import setByPath from 'lodash/set';
import isEmpty from 'lodash/isEmpty';
import { ChromePicker, SketchPicker } from 'react-color';
import get from 'lodash/get';

import Tool from '../common/Tool';
import ToolHeader from '../common/ToolHeader';
import ToolFooter from '../common/ToolFooter';
import ToolBody from '../common/ToolBody';

import ComponentPropValidator from './utils/ComponentPropValidator';


import Ice2Input from '../../../ICE2Elements/Input';
import Ice2Thumbnail from '../../../ICE2Elements/Display/nodeThumbnail';

import './style.scss';

// Settings
_.templateSettings.interpolate = /\{([\s\S]+?)\}/g;

import {
  SET_STATIC_DATA_BY_LAYOUT,
  CLOSE_LAYOUT_WITH_ELASTIC,
} from '../../UIISystemActionSheets';

import {
ToolKey as CanvasToolKey,
} from '../Canvas';


import {
  CreateOpenActionSheet as CreateCuratorOpenActionSheet,
  ToolKey as CuratorToolKey,
} from '../Curator/Constants';


import {
  CreateOpenActionSheet as CreateResourceOpenActionSheet,
} from '../ResourceManager/Constants';

import * as actions from './actions';

import {
  Info,
} from './Constants';

import AddIcon from 'material-ui/svg-icons/content/add';
import MinusIcon from 'material-ui/svg-icons/content/remove';
import ArrowDropDownIcon from 'material-ui/svg-icons/navigation/arrow-drop-down';
import ArrowDropUpIcon from 'material-ui/svg-icons/navigation/arrow-drop-up';
import ActionSearch from 'material-ui/svg-icons/action/search';

import cloneDeep from 'lodash/cloneDeep';


import FullmoonBtn from '../../../Unit/FullmoonButton';

const iconStyle = {
  width:20,
  height:20,
}

function colorObjectToCSSColor(color){
  if( color.rgb.a === 1) {
    return color.hex;
  }

  return `rgba(${color.rgb.r},${color.rgb.g},${color.rgb.b},${color.rgb.a})`
}

@connect((state) => ({
  User : state.get('User'),
  Site : state.get('Site'),
  AssignedStore : state.get('AssignedStore'),
  Elastic : state.get('Elastic'),
  DragSystem : state.get('DragSystem'),
}))
class Settings extends Component {
  static contextTypes = {
    UIISystems : PropTypes.object,
    elastic: PropTypes.object,
    zone : PropTypes.object,
    events : PropTypes.object,
    functionStore : PropTypes.object,
    theme : PropTypes.object,
    intl: PropTypes.object,
    ice2 : PropTypes.func,
  }

  static propTypes = {
    spreadList : PropTypes.object,
  }


  constructor(props){
    super(props)

    // console.log('>>>>>>>', props.staticData.direc);

    this.state = {
      mounted : false,
      directiveName : props.staticData.directiveName,
      directiveNodePos : props.staticData.directiveNodePos,
      saveNode : cloneDeep(get(props,'staticData.directiveNode',{props:{}})),
      subOptions : {},
    };

    // console.log('>>>>?',this.state);


    this.doms = {};
    this.onCanvasSelectNodeWrapped = this.onCanvasSelectNode.bind(this);
  }

  onCanvasSelectNode({directiveName, directiveNode, directiveNodePos}){
    console.log('canvas select node', directiveNode);


    this.setState({
      saveNode : cloneDeep(directiveNode),
      directiveNodePos,
      directiveName,
    })
  }


  onManualApply(){
    let {
      saveNode,
      directiveNodePos,
      directiveName,
    } = this.state;

    console.log(saveNode, directiveNodePos, directiveName);

    this.context.events.emit('SETTING-UPDATE-COMPONENT', {
      directiveNode : saveNode,
      directiveNodePos,
      directiveName,
    })
  }

  onToggleSpreadAll(checked){
    if ( checked )
      this.context.elastic.dispatch(actions.SPREAD_EACH())
    else
      this.context.elastic.dispatch(actions.SPREAD_ALL());
  }

  spreadTest(spreadId, forceTrue){
    return forceTrue || this.props.spreadList[spreadId];
  }


  onClickFooterButton(item, e, node){
    if( item.functionKey ){
      let func = this.context.functionStore.get(item.functionKey);
      func(item, node);
    }
  }

  onClose(){
    if( this.props.staticData.closeHookFunctionKey ){
      let func = this.context.functionStore.get(this.props.staticData.closeHookFunctionKey);

      if( typeof func === 'function' ){
        return func(this);
      }
    }

    return true;
  }

  onModifiedDirectiveJSON(exportedJSON, modifiedNode, directiveName, modifiedNodePos){

    this.context.events.emit('MODIFIED-DIRECTIVE-NODE', {exportedJSON, modifiedNode, directiveName, modifiedNodePos});
  }

  getNodePropByPath(selectedNode, path){
    let props = selectedNode.props;

    if( !props ){
      return null;
    }

    return getByPath(props, path);
  }



  setNodePropByPath(selectedNode, path, value, defineObject){
    let validator = new ComponentPropValidator(this, selectedNode.props, defineObject );

    // validation 을 실패하면 프로퍼티를 수정하지 않는다.
    if( validator.check(path, value) ){
      selectedNode.props = selectedNode.props || {};
      let saveNode = cloneDeep(this.state.saveNode);
      saveNode.props = saveNode.props || {};
      setByPath(saveNode.props , path, value);



      this.setState({
        saveNode : saveNode,
      });


      // 실제 canvas 에 변경사항을 반영
      if( false && this.props.staticData.configurationTargetMethod === 'canvas-selected' ){
        let canvasState = this.context.elastic.getOtherOnlyOneToolState(CanvasToolKey);
        let {
          selectedDirectiveName,
          routeDirectiveJSONTREE_map,
        } = canvasState;

        let rootNode = routeDirectiveJSONTREE_map[selectedDirectiveName];
        this.onModifiedDirectiveJSON(rootNode.exportToJSON(), selectedNode, selectedDirectiveName, selectedNode.pos);

        if( defineObject && defineObject.refreshMethod ){
          console.log('go refresh', defineObject.refreshMethod)
          setTimeout(()=>{
            this.context.events.emit('CANVAS-DISPLAY-COMPONENT-REFRESH', {
              componentPos : selectedNode.pos,
              componentName : selectedNode.tag,
              refreshMethod : defineObject.refreshMethod ,
            });
          },100);
        }
      } else {
        this.forceUpdate();


      }



    } else {
      // 검증 통과 실패 알림 notice
    }
  }

  getSelectedNode(){
    let { configurationTargetMethod, virtualComponentNode } = this.props.staticData;

    if( configurationTargetMethod === 'canvas-selected' ){
      let canvasState = this.context.elastic.getOtherOnlyOneToolState(CanvasToolKey);
      let {   selectedDirectiveName, selectedDirectiveNodePos, routeDirectiveJSONTREE_map, selectedDirectiveNodeEditRules } = canvasState;


      let directive, selectedNode;
      if( selectedDirectiveNodePos ){
        directive = routeDirectiveJSONTREE_map[selectedDirectiveName];

        return directive.findByLocation(selectedDirectiveNodePos);
      }

      return null;
    } else {
      return this.state.saveNode;
    }
  }


  /**
   * Templating
   * @param templateString : require
   * @param selectedNode : require
   * @param defineObject
   * @param upperLabelPath
   */
  templating(templateString, selectedNode, defineObject, upperLabelPath){
    return _.template(templateString)({
      vm : {...selectedNode.props},
    });
  }

  checkInterestingDragContext(interested = []){
    if( this.props.DragSystem.nativeDragState.dragging ){
      if( interested.find((type)=> type == this.props.DragSystem.nativeDragState.contextType)){
        return true;
      }
      return false;
    }

    return false;
  }


  convertComponentDateToMomentSet(componentDate){
    let {
      start,
      end,
    } = componentDate;

    if( start ){
      start = moment(start, 'YYYYMMDDHHmmss')
    }

    if( end ){
      end = moment(end, 'YYYYMMDDHHmmss')
    }

    return {
      start,
      end,
    }
  }

  convertMomentSetToComponentDate(momentSet){
    let {
      start,
      end,
    } = momentSet;

    if( start ){
      start = start.format('YYYYMMDDHHmmss')
    }

    if( end ){
      end = end.format('YYYYMMDDHHmmss')
    }

    return {
      start,
      end,
    }
  }


  componentDidMount(){

    let { configurationTargetMethod, virtualComponentNode } = this.props.staticData;
    console.log(this);
    if( configurationTargetMethod === 'canvas-selected' ){
      let { configurationTargetMethod, virtualComponentNode } = this.props.staticData;
      let canvasState = this.context.elastic.getOtherOnlyOneToolState(CanvasToolKey);
      let {
        selectedDirectiveName,
        selectedDirectiveNodePos,
        routeDirectiveJSONTREE_map,
      } = canvasState;

      let directive = routeDirectiveJSONTREE_map[selectedDirectiveName];

      let directiveNode = getByPath({ children: [directive]}, selectedDirectiveNodePos.replace(/(\d)/g, 'children[$1]'), {});

      this.setState({
        saveNode : cloneDeep(directiveNode),
        directiveNodePos : selectedDirectiveNodePos,
        directiveName : selectedDirectiveName,
      })
    } else {
      let { configurationTargetMethod, virtualComponentNode } = this.props.staticData;

      this.setState({
        saveNode : cloneDeep(virtualComponentNode),
      })
    }


    this.context.events.listen('CANVAS-SELECT-NODE', this.onCanvasSelectNodeWrapped);
  }

  componentWillUnmount(){
    this.context.events.removeListen('CANVAS-SELECT-NODE', this.onCanvasSelectNodeWrapped);
  }


  renderYoutubeVideoControl(defineObject, path, selectedNode, upperLabelPath){
    let videoConfig = this.getNodePropByPath(selectedNode, path) || null;

    let openYE = () => {
      this.context.UIISystems.openTool('YoutubeExplorer', '[modal]',{width:'70%', height:'80%'}, {
        selectable : true,
        defaultSelectedItems : videoConfig ? [videoConfig] : [],
        applyFunctionKey : 'apply',
      })
        .then(( closer )=>{
          this.context.functionStore.register('apply', (videoConfigs)=>{
            closer();
            this.setNodePropByPath(selectedNode, path, videoConfigs[0], defineObject);
          })
        })
    }

    let previewUrl = `https://www.youtube.com/embed/${videoConfig && videoConfig.id}?autoplay=0`;


    return <div className="youtube-control">

      <div className="input">
        <FullmoonBtn
          shape="round"
          normalStyle={{
            display: 'inline-block',
            height: 30,
            borderWidth: 0,

            borderRadius: 100,
            fontSize: 12,
            color: this.context.theme.Tool.layout.footer.fullmoonBtn.textColor.default,
            fontFamily: 'Nanum Square',
            backgroundColor: this.context.theme.Tool.layout.footer.fullmoonBtn.bgColor.default,
            margin:10,
            padding:'0 20px',
            textAlign:'left',
          }}
          hoverStyle={{
            borderWidth: 0,
            backgroundColor:this.context.theme.Tool.layout.footer.fullmoonBtn.bgColor.hovered,
          }}
          moonBlender='overlay'
          onClick={openYE}>
          {this.context.intl.formatMessage({id:'app.settings.openYoutubeExplorer'})}
        </FullmoonBtn>
      </div>

      <div className="preview">
        <iframe className="iframe" width="161" height="100" src={previewUrl} frameBorder="0" allowFullScreen></iframe>
      </div>

    </div>
  }


  renderNodeTypeControl(defineObject, path, selectedNode, upperLabelPath){
    let value = this.getNodePropByPath(selectedNode, path) || null;


    let helperData = this.props.helperData[path] || {};

    let loadingHelperData = ()=>{
      this.context.elastic.dispatch(actions.LOAD_HELPER_DATA(path));

      this.context.ice2.get(`/node/nodeType/list.json`, {
        params : {
          repositoryType_matching : 'node,data',
          includeReferenced:true,
        },
      })
        .then((res) => {
          let {data} = res;

          this.context.elastic.dispatch(actions.LOAD_HELPER_DATA(path, data));
        })
    };


    let onDragOver = (e) => {
      e.preventDefault();
    }

    let onDrop = (e) => {
      e.preventDefault();
      this.setNodePropByPath(selectedNode, path, this.props.DragSystem.nativeDragState.data.nodeType.tid, defineObject);
    }

    if( !helperData.data ){
      return <div
        className={classnames("code-control", this.checkInterestingDragContext(['nodeType']) && 'interest' )}
        onDragOver={this.checkInterestingDragContext(['nodeType']) && onDragOver}
        onDrop={this.checkInterestingDragContext(['nodeType']) && onDrop}
        >

        <Select
          showSearch
          value={helperData.loading ? 'Loading...' : value}
          style={{ width: '100%' }}
          placeholder="Select"
          optionFilterProp="children"
          onFocus={loadingHelperData}
        />
      </div>
    }

    return <div
      className={classnames("code-control", this.checkInterestingDragContext(['nodeType']) && 'interest' )}
      onDragOver={this.checkInterestingDragContext(['nodeType']) && onDragOver}
      onDrop={this.checkInterestingDragContext(['nodeType']) && onDrop}
      >
      <Select
        showSearch
        value={value}
        style={{ width: '100%' }}
        placeholder="Select"
        optionFilterProp="children"
        onChange={(value)=> this.setNodePropByPath(selectedNode, path, value, defineObject) }
        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
      >
        <Option value={''}>
          NONE
        </Option>

        {
          helperData.data.items.map((item, i) => <Option value={item.tid} key={i}>
            {item.typeName}
          </Option>)
        }

      </Select>
    </div>
  }

  renderPropertyTypeControl(defineObject, path, selectedNode, upperLabelPath){
    let value = this.getNodePropByPath(selectedNode, path) || null;


    let helperData = this.props.helperData[path] || {};

    let loadingHelperData = ()=>{
      this.context.elastic.dispatch(actions.LOAD_HELPER_DATA(path));

      this.context.ice2.get(`/node/propertyType/list.json`, {
        params : {
          tid : this.templating(defineObject.typeStructure.nodeType, selectedNode),
        },
      })
        .then((res) => {
          let {data} = res;

          this.context.elastic.dispatch(actions.LOAD_HELPER_DATA(path, data));
        })
    };



    let onDragOver = (e) => {
      e.preventDefault();
    }

    let onDrop = (e) => {
      e.preventDefault();
      this.setNodePropByPath(selectedNode, path, this.props.DragSystem.nativeDragState.data.key.split('.').pop(), defineObject);
    }

    if( !helperData.data ){
      return <div
        className={classnames("code-control", this.checkInterestingDragContext(['propertyType']) && 'interest' )}
        onDragOver={this.checkInterestingDragContext(['propertyType']) && onDragOver}
        onDrop={this.checkInterestingDragContext(['propertyType']) && onDrop}
      >
        <Select
          showSearch
          value={helperData.loading ? 'Loading...' : value}
          style={{ width: '100%' }}
          placeholder="Select"
          optionFilterProp="children"
          onFocus={loadingHelperData}
        />
      </div>
    }

    return <div
      className={classnames("code-control", this.checkInterestingDragContext(['propertyType']) && 'interest' )}
      onDragOver={this.checkInterestingDragContext(['propertyType']) && onDragOver}
      onDrop={this.checkInterestingDragContext(['propertyType']) && onDrop}
    >
      <Select
        showSearch
        value={value}
        style={{ width: '100%' }}
        placeholder="Select"
        optionFilterProp="children"
        onChange={(value)=> this.setNodePropByPath(selectedNode, path, value, defineObject) }
        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
      >
        <Option value={''}>
          NONE
        </Option>

        {
          helperData.data.items.map((item, i) => <Option value={item.pid} key={i}>
            {item.propertyTypeName}
          </Option>)
        }

      </Select>
    </div>
  }


  renderNodeItemIdControl(defineObject, path, selectedNode, upperLabelPath){
    let value = this.getNodePropByPath(selectedNode, path) || null;

    let openSelector = async () => {
      let nodeType = await this.context.ice2.loadNodeType(defineObject.typeStructure.tid, null, null, null,defineObject.typeStructure.customNodeUx,defineObject.typeStructure.customPropertyUxMap);

      let selectFuncKey = this.context.UIISystems.getUnique();
      this.context.UIISystems.openTool('ICE2Data', '[modal]',{
        width:'80%',
        height:'70%',
        // UNIQUE_LAYER : true,
      }, {
        selectable : true,
        nodeType : nodeType,
        multiSelection : this.props.multiple,
        customNodeUx : defineObject.typeStructure.customNodeUx || {},
        customPropertyUxMap : defineObject.typeStructure.customPropertyUxMap || {},
        extraListQuery : defineObject.typeStructure.extraListQuery,
        listRepresenter : defineObject.typeStructure.listRepresenter,
        defaultSearches : defineObject.typeStructure.defaultSearches || [],
        thumbnailRule : defineObject.typeStructure.thumbnailRule,
        type : 'list',
        footerBtnCallbacks : {
          'select' : selectFuncKey,
        },
        target : defineObject.typeStructure.target,
      }).then((closer) => {
        this.context.functionStore.register(selectFuncKey, (items)=>{
          if( items.length > 0 ){

            let returnData;

            switch (defineObject.typeStructure.acceptType){
              case 'item':
                returnData = items[0];
                break;




              case 'id':
              default :
                returnData = items[0][defineObject.typeStructure.idPid];
            }

            this.setNodePropByPath(selectedNode, path, returnData, defineObject);
            closer();
          } else {
            alert('선택되지 않았습니다.');
          }
        })
      })
    }


    let id;

    if( value ){
      switch (defineObject.typeStructure.acceptType) {
        case 'item':
          id = _.get(value,defineObject.typeStructure.idPid);
          break;
        case 'id':
        default :
          id = value;
      }
    }


    return <div>

      {value && <Ice2Thumbnail
        key={id}
        id={id}
        tid={defineObject.typeStructure.tid}
        pid={defineObject.typeStructure.idPid}
        rule={defineObject.typeStructure.thumbnailRule} />}
      <FullmoonBtn
        shape="round"
        normalStyle={{
          display: 'inline-block',
          height: 30,
          borderWidth: 0,

          borderRadius: 100,
          fontSize: 12,
          color: this.context.theme.Tool.layout.footer.fullmoonBtn.textColor.default,
          fontFamily: 'Nanum Square',
          backgroundColor: this.context.theme.Tool.layout.footer.fullmoonBtn.bgColor.default,
          margin:10,
          padding:'0 20px',
          textAlign:'left',
        }}
        hoverStyle={{
          borderWidth: 0,
          backgroundColor:this.context.theme.Tool.layout.footer.fullmoonBtn.bgColor.hovered,
        }}
        moonBlender='overlay'
        onClick={(e)=>openSelector()}>
        {this.context.intl.formatMessage({id:'app.common.selection'})}
      </FullmoonBtn>


      <FullmoonBtn
        shape="round"
        normalStyle={{
          display: 'inline-block',
          height: 30,
          borderWidth: 0,

          borderRadius: 100,
          fontSize: 12,
          color: this.context.theme.Tool.layout.footer.fullmoonBtn.textColor.default,
          fontFamily: 'Nanum Square',
          backgroundColor: this.context.theme.Tool.layout.footer.fullmoonBtn.bgColor.default,
          margin:10,
          padding:'0 20px',
          textAlign:'left',
        }}
        hoverStyle={{
          borderWidth: 0,
          backgroundColor:this.context.theme.Tool.layout.footer.fullmoonBtn.bgColor.hovered,
        }}
        moonBlender='overlay'
        onClick={(e)=>this.setNodePropByPath(selectedNode, path, null, defineObject)}>
        {this.context.intl.formatMessage({id:'app.common.delete'})}
      </FullmoonBtn>
    </div>
  }

  renderCurationSourceControl(defineObject, path, selectedNode, upperLabelPath){
    let curationParams = this.getNodePropByPath(selectedNode, path) || [];

    const openCurator = ()=>{
      let transferData = {
        applyFunctionKey : 'apply',
        curationMethodSheet : defineObject,
        oldCurationParams : curationParams,
        componentProps : selectedNode.props,
      };

      let layoutId;
      this.context.UIISystems.emitActionSheet(CreateCuratorOpenActionSheet("[modal]"), transferData, {
        width : 600,
        height: 500,
        onLayoutID(id){
          layoutId = id;
        },
      });

      this.context.functionStore.register('apply', (madeParams)=>{
        // let reqID = this.context.global.uiBlockingReqBegin();
        // this.context.global.uiBlockingReqFinish(reqID);
        this.setNodePropByPath(this.getSelectedNode(), path, madeParams, defineObject);

        this.context.UIISystems.emitActionSheet(CLOSE_LAYOUT_WITH_ELASTIC(layoutId, CuratorToolKey, true));

        this.context.events.emit('CANVAS-DISPLAY-COMPONENT-REFRESH', {
          componentPos : this.state.directiveNodePos,
          componentName : selectedNode.tag,
          directiveName : this.state.directiveName,
          refreshMethod : defineObject.typeStructure.refreshMethod,
        });
      })
    }

    return <div className="curation-source-control">
      <FullmoonBtn
        shape="round"
        normalStyle={{
          display: 'inline-block',
          height: 30,
          borderWidth: 0,

          borderRadius: 100,
          fontSize: 12,
          color: this.context.theme.Tool.layout.footer.fullmoonBtn.textColor.default,
          fontFamily: 'Nanum Square',
          backgroundColor: this.context.theme.Tool.layout.footer.fullmoonBtn.bgColor.default,
          margin:10,
          padding:'0 20px',
          textAlign:'left',
        }}
        hoverStyle={{
          borderWidth: 0,
          backgroundColor:this.context.theme.Tool.layout.footer.fullmoonBtn.bgColor.hovered,
        }}
        moonBlender='overlay'
        onClick={(e)=>openCurator()}>
        { defineObject.typeStructure.targetLabel || this.context.intl.formatMessage({id:'app.curation.verb'})}
      </FullmoonBtn>
    </div>
  }

  renderArrayControl(defineObject, path, selectedNode, upperLabelPath){
    let array = this.getNodePropByPath(selectedNode, path) || [];

    let maxLength = -1;

    let useLimit = false;
    if( defineObject.hasOwnProperty('maxLength') ){
      useLimit = true;
      maxLength = defineObject.maxLength;
    }

    return <div>
      { array.map((value, i) =>
        this.renderTypeEditor(defineObject.typeStructure, path + '.' + i, selectedNode, `${defineObject.label}[${i}]`, [{
          icon : 'trash-o',
          onClick : ()=>this.setNodePropByPath(selectedNode, path, [...array.slice(0, i), ...array.slice(i+1)], defineObject),
        }]))
      }

      {
        useLimit && ( array.length >= maxLength ) ? null :
        (
            <div className="array-control">
              <div
                  className="adder"
                  onClick={()=> this.setNodePropByPath(selectedNode, path, [...array,undefined], defineObject)}>
                { defineObject.label } ({array.length + 1} 번째) <AddIcon/>
              </div>
            </div>
        )

      }

    </div>
  }


  renderResourceControl(defineObject, path, selectedNode, upperLabelPath){
    let value = this.getNodePropByPath(selectedNode, path) || null;


    const openResource = (e)=>{
      let transferData = {
        use : defineObject.use || ['image'],
        selectable : true,
        editable : false,
        removable : false,
        maxSelectCount : 1,
        selectionCallback : 'selection',
      };

      this.context.UIISystems.emitActionSheet(CreateResourceOpenActionSheet("[modal]"), transferData, {
        width : 600,
        height: 500,
      });

      this.context.functionStore.register('selection', (selectionList)=>{
        let first = selectionList[0];

        this.setNodePropByPath(this.getSelectedNode(), path, first, defineObject);
      })
    }



    return <div className="resource-control" onClick={openResource}>
      <div className="real">
        {((value)=>{
          if( !value ) return <div className="image-holder"> 선택하세요 </div>;


          if( /\.(jpe?g|png|gif)$/.test(value)) {
            return <img src={`/api/user/${this.props.User.session.id}/site/${this.props.Site.info.id}/${this.props.Site.info.owner}/store/resource/get/${value}`}/>
          } else if ( /\.svg$/.test(value) ){
            return <img src={`/api/user/${this.props.User.session.id}/site/${this.props.Site.info.id}/${this.props.Site.info.owner}/store/resource/get/${value}`}/>
          } else if ( /\.(mp3)$/.test(value) ){
            return <audio style={{width:200}} controls>
              <source src={`/api/user/${this.props.User.session.id}/site/${this.props.Site.info.id}/${this.props.Site.info.owner}/store/resource/get/${value}`} type="audio/mpeg"/>
            </audio>;
          } else if ( /\.(mp4|webm)/.test(value) ){
            return <video controls width="200px">
              <source src={`/api/user/${this.props.User.session.id}/site/${this.props.Site.info.id}/${this.props.Site.info.owner}/store/resource/get/${value}`} type="video/mp4"/>
            </video>;
          } else if ( /\.(eot|ttf)/.test(value) ){
            return value;
          }
        })(value)}
      </div>
      <div className="name">
        {value}
      </div>
    </div>
  }

  renderObjectControl(defineObject, path, selectedNode, upperLabelPath){
    let value = this.getNodePropByPath(selectedNode, path) || null;
    let objectKeys = Object.keys(defineObject.typeStructure);


    return objectKeys.map((key) => this.renderTypeEditor(defineObject.typeStructure[key], path + '.' + key, selectedNode, upperLabelPath +'.'+ defineObject.label));
  }

  renderCodeControl(defineObject, path, selectedNode, upperLabelPath){
    let value = this.getNodePropByPath(selectedNode, path) || null;
    let enumerationID = selectedNode.tag + '/' + path + '/' + 'enumeration';

    let codeEnumerationStyle = {};
    if( this.doms[path] ){
      let rect = this.doms[path].getBoundingClientRect();

      if( rect.top > (window.innerHeight /2) ){
        codeEnumerationStyle.bottom = window.innerHeight - rect.bottom + rect.height;
      }
    }


    const onTriggerCodeControlPopup = (e)=>{

      if( this.props.spreadList[enumerationID] ){
        this.context.elastic.dispatch( actions.CANCEL_SPREAD_ITEM(enumerationID) );
      } else {
        // let closeTrap = (e) =>{
        //   this.context.elastic.dispatch( actions.CANCEL_SPREAD_ITEM(enumerationID) );
        //
        //   document.body.removeEventListener('click', closeTrap);
        // }
        // document.body.addEventListener('click', closeTrap, true);

        this.context.elastic.dispatch( actions.SPREAD_ITEM(enumerationID) );
      }
    }

    const onProtectCloseTrap = (e)=>{
      e.stopPropagation();
    }

    return <div className="code-control">
      <Select
        showSearch
        value={value}
        style={{ width: '100%' }}
        placeholder="Select"
        optionFilterProp="children"
        onChange={(value)=> this.setNodePropByPath(selectedNode, path, value, defineObject) }
        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
      >
        {
          defineObject.typeStructure.map((lowerDefineObject,i) =>
            <Option value={( typeof lowerDefineObject === 'object' ) ? lowerDefineObject.value : lowerDefineObject} key={i}>
              { ( typeof lowerDefineObject === 'object' ) ? ( lowerDefineObject.label + ` (${JSON.stringify(lowerDefineObject.value)})`) : lowerDefineObject }
            </Option>
          )
        }
      </Select>
    </div>
  }

  renderStringControl(defineObject, path, selectedNode, upperLabelPath){
    let value = this.getNodePropByPath(selectedNode, path) || '';







    if( defineObject.type === 'rich-text' ){

      let bgDark = this.state.subOptions[path];

      return <div className={ classnames("string-control", bgDark ? 'bg-dark':'bg-white')}>

        <div className="string-control-wrapper">
          <div>
            <Toggle
              label={bgDark ? '어두운 에디터 배경색':'밝은 에디터 배경색'}
              defaultToggled={bgDark}
              toggled={bgDark}
              onToggle={(_, checked) => this.setState({
                ...this.state,
                subOptions : {
                  ...this.state.subOptions,
                  [path] : checked,
                },
              })}
            />
          </div>

          <FroalaEditor
            config={{
              requestWithCredentials : true,
              withCredentials: true,
              key : 'LDIE1QCYRWa2GPIb1d1H1==',
              imageInsertButtons: ["imageBack", "|", "imageUpload", "imageByURL"],
              imageUploadURL: this.context.ice2.defaults.baseURL+"/node/editorStore/saveImage.json",
              imageUploadParams: {
                fileType: "image",
                host: this.context.ice2.defaults.baseURL,
              },
                events: {
                "froalaEditor.image.uploaded": function(){},
                "froalaEditor.image.error": function(){},
              },
            }}

            model={value}
            onModelChange={(model) => { console.log('asdasd'); this.setNodePropByPath(selectedNode, path, model, defineObject)} }
          />
        </div>
      </div>
    }


    return <div className="string-control">

      <div className="string-control-wrapper">
        { defineObject.type === 'text' ? <TextArea placeholder="input" autosize value={value} onChange={(e)=>this.setNodePropByPath(selectedNode, path, e.nativeEvent.target.value, defineObject)}/>
            : <Input placeholder="input" value={value} onChange={(e)=>this.setNodePropByPath(selectedNode, path, e.nativeEvent.target.value, defineObject)}/>}
      </div>
    </div>
  }

  renderBooleanControl(defineObject, path, selectedNode, upperLabelPath){
    let value = this.getNodePropByPath(selectedNode, path) || null;

    return <div className="boolean-control">
      <Toggle
        label={value ? 'On':'Off'}
        defaultToggled={value}
        toggled={value}
        onToggle={(_, checked) => this.setNodePropByPath(selectedNode, path, checked, defineObject)}
        thumbStyle={{}}
        trackStyle={{}}
        thumbSwitchedStyle={{}}
        trackSwitchedStyle={{}}
      />
    </div>
  }

  renderColorControl(defineObject, path, selectedNode, upperLabelPath){
    let value = this.getNodePropByPath(selectedNode, path) || null;



    return <div className="color-control">
      <div className="color-control-wrapper">
        <div
          className="coloring">
          <Popover
            content={<SketchPicker
              color={this.getNodePropByPath(selectedNode, path) || ''}
              onChange={(color) => this.setNodePropByPath(selectedNode, path, colorObjectToCSSColor(color), defineObject)}/>}>
            <div className="color" style={{backgroundColor:value, boxShadow: `0 0 5px 0 ${value}`}}>
              <span class="hex">{value || 'No coloring'}</span>
            </div>
          </Popover>
        </div>
      </div>
    </div>
  }

  renderNumberControl(defineObject, path, selectedNode, upperLabelPath){
    let value = this.getNodePropByPath(selectedNode, path) || 0;

    return <div className="numerical-control">
      <div className="numerical-control-wrapper">
        <div className="input-wrapper">
          <InputNumber
            style={{ width: '100%' }}
            defaultValue={value}
            onChange={(value) => this.setNodePropByPath(selectedNode, path, value , defineObject)}
            value={value}/>
        </div>
      </div>
    </div>
  }


  renderEditableContentsBox(defineObject, path, selectedNode, upperLabelPath){
    let value = this.getNodePropByPath(selectedNode, path) || '';

    return <div className="editable-control">
      <div>
        <div
          className="editable-control-wrapper"
          contentEditable='true'
          onChange={(e) => {
            this.setNodePropByPath(selectedNode, path, e.nativeEvent.target.innerHTML, defineObject)
          }}
          onBlur={(e) => {
            this.setNodePropByPath(selectedNode, path, e.nativeEvent.target.innerHTML, defineObject)
          }} dangerouslySetInnerHTML={{__html:value}}>

        </div>
      </div>
    </div>
  }


  renderUnitControl(defineObject, path, selectedNode, upperLabelPath){
    let value = this.getNodePropByPath(selectedNode, path) || 0;
    let number;
    let currentUnit;

    if( value ){
      let matches = value.match(/^(-?[\d.]*)([^\d^.]*)$/);
      if( matches ){
        number = parseFloat(matches[1]);
        currentUnit = matches[2];
      } else {
        number = 0;
      }
    } else {
      number = 0;
    }

    if( !currentUnit ){
      let defaultUnit = defineObject.typeStructure[0];

      if( typeof defaultUnit === 'object' ){
        currentUnit = defaultUnit.value;
      } else {
        currentUnit = defaultUnit;
      }
    }


    return [<div className="numerical-control">
      <div className="numerical-control-wrapper">
        <InputNumber
          style={{ width: '100%' }}
          defaultValue={number}
          onChange={(number) => this.setNodePropByPath(selectedNode, path, `${number}${currentUnit}`  )}
          value={number}/>
      </div>
    </div>,
      <div className="unit-control" style={{paddingTop:0}}>

        <RadioGroup onChange={(e)=> this.setNodePropByPath(selectedNode, path, `${number}${e.target.value}`, defineObject)} value={currentUnit} defaultValue={currentUnit}>
          { defineObject.typeStructure.map(
            (unit, i) => {
              const unitValue = (typeof unit === 'object') ? unit.value : unit;

              return <Radio
                key={i}
                value={unitValue}
                keys={i}>
              { (typeof unit === 'object') ? (unit.label +` (${unit.value})`) : unit }
              </Radio>
            }
          )}
        </RadioGroup>
      </div>,
    ]
  }


  renderControls(defineObject, path, selectedNode, upperLabelPath = ''){
      let controlElement = null;




      if( typeof defineObject.type === 'string' ){
        switch(defineObject.type.toLowerCase()){
          case "number" :
            controlElement = this.renderNumberControl(defineObject, path, selectedNode, upperLabelPath);
            break;
          case "color":
            controlElement = this.renderColorControl(defineObject, path, selectedNode, upperLabelPath);
            break;
          case "boolean" :
            controlElement = this.renderBooleanControl(defineObject,path, selectedNode, upperLabelPath);
            break;
          case "rich-text" :
          case "text" :
          case "string" :
            controlElement = this.renderStringControl(defineObject,path, selectedNode, upperLabelPath);
            break;
          case "code" :
            controlElement = this.renderCodeControl(defineObject,path, selectedNode, upperLabelPath);
            break;
          case "unit" :
            controlElement = this.renderUnitControl(defineObject,path, selectedNode, upperLabelPath);
            break;
          case "object" :
            controlElement = this.renderObjectControl(defineObject, path, selectedNode, upperLabelPath);
            break;
          case "array" :
            controlElement = this.renderArrayControl(defineObject, path, selectedNode, upperLabelPath);
            break;
          case "resource" :
            controlElement = this.renderResourceControl(defineObject, path, selectedNode, upperLabelPath);
            break;
          case "curation-source" :
            controlElement = this.renderCurationSourceControl(defineObject, path, selectedNode, upperLabelPath);
            break;
          case "nodetype" :
            controlElement = this.renderNodeTypeControl(defineObject, path, selectedNode, upperLabelPath);
            break;
          case "propertytype" :
            controlElement = this.renderPropertyTypeControl(defineObject, path, selectedNode, upperLabelPath);
            break;
          case "nodeitemid" :
            controlElement = this.renderNodeItemIdControl(defineObject, path, selectedNode, upperLabelPath);
            break;
          case "youtube-video" :
            controlElement = this.renderYoutubeVideoControl(defineObject, path, selectedNode, upperLabelPath);
            break;
          case "editable" :
            controlElement = this.renderEditableContentsBox(defineObject, path, selectedNode, upperLabelPath);
            break;
        }

        return <div className="controls">
          { Array.isArray(controlElement) ? controlElement.map(
              (ele, i) =>
                <div className="control-row" key={i}>
                  {ele}
                </div>
            ) :
            <div className="control-row">
              { controlElement }
            </div>
          }
        </div>
      } else {
        <div className="controls">
          type of {defineObject.label} must be String.
        </div>
      }
  }

  renderTypeEditor(defineObject, path, selectedNode, upperLabelPath = '', additionActions = []){
    let optionID = selectedNode.tag +'/'+ path;


    if( defineObject.hidden ){
      return null;
    }


    let {
      props,
    } = selectedNode;


    let {
      if : propertyCondition,
    } = defineObject;

    let upperData;
    let upperPath = path.split('.');
    if( upperPath.length > 1 ){
      upperPath.pop();
      upperPath = upperPath.join('.');
      upperData = this.getNodePropByPath(selectedNode, upperPath);
    }

    // eval scope value $
    let $ = {
      props : props,
      upperObject : upperData || {},
    };


    if( propertyCondition ) {
      if( !eval(propertyCondition) ){
        return null;
      }
    }




    return <div className={ classnames("field-row", 'd'+(upperLabelPath ? upperLabelPath.split('.').length : 0))} key={path}>
      <div className="label">
        { upperLabelPath ?
          <span>
            <span>
              {upperLabelPath}
            </span>
            .
            <span className="interest">
              {defineObject.label}
            </span>
          </span>:
          <span className="interest">
            {defineObject.label}
          </span>}

        {defineObject.required && " (required)"}

        <div className="action">
          {
            additionActions.map((action, i)=> <button key={i} onClick={()=>action.onClick()}>
              <i className={classnames('fa', 'fa-'+action.icon)}/>
            </button>)
          }

          {
            !this.props.spreadAll && <button onClick={() => this.context.elastic.dispatch( this.spreadTest(optionID) ? actions.CANCEL_SPREAD_ITEM(optionID) : actions.SPREAD_ITEM(optionID) )}>
              {
                this.spreadTest(optionID) ? <i className="fa fa-angle-up"/> : <i className="fa fa-angle-down"/>
              }
            </button>
          }
        </div>
      </div>
      {  this.spreadTest(optionID, this.props.spreadAll) && this.renderControls(defineObject, path, selectedNode, upperLabelPath) }
      { defineObject.description && <div className='desc'>
        { defineObject.description  }
      </div> }
    </div>
  }



  renderScheduleOverview({start, end}){


      let current = parseInt(moment(new Date()).format('YYYYMMDDHHmmss'));

      if( start ){
        if( parseInt(start) > current ){
          return <div className="schedule-feedback be-scheduled">
            표시 예정
          </div>
        }
      }

      if( end ){
        if( parseInt(end) < current ){
          return <div className="schedule-feedback expired">
            기간이 만료된 컴포넌트
          </div>
        }
      }


      return <div className="schedule-feedback scheduled">
         표시중
      </div>
  }

  render() {
    let { configurationTargetMethod, virtualComponentNode } = this.props.staticData;
    let canvasState = this.context.elastic.getOtherOnlyOneToolState(CanvasToolKey);
    let {
      selectedDirectiveNodePos,
      selectedDirectiveNodeEditRules = {},
    } = canvasState;




    let editableProps;
    let propKeys;

    let directive, selectedNode;
    if( configurationTargetMethod === 'canvas-selected' ){
      if( selectedDirectiveNodePos ){


        selectedNode = this.state.saveNode;
      }
    } else {
      selectedNode = this.state.saveNode;
      selectedDirectiveNodeEditRules = this.props.staticData.configurationRules;
    }

    if( selectedDirectiveNodeEditRules ){
      editableProps = selectedDirectiveNodeEditRules.props || {};
      propKeys = Object.keys(editableProps);
    } else {
      editableProps = {};
      propKeys = [];
    }

    let timeValue = [];
    if( selectedNode ){
      timeValue = this.getNodePropByPath(selectedNode, 'componentSchedule') || {};
    }


    if( isEmpty(editableProps) ){
      return (
        <Tool className="settings-editor">
          <ToolHeader
            title="Component Settings"
            iconType={ Info.iconType }
            icon={ Info.icon }
            iconColor={ Info.iconColor }/>
          <ToolBody ref={(toolBody) => { this.toolBodyComp = toolBody } } nofooter={true}>
            <div className="no-rules"> no rules </div>
          </ToolBody>
        </Tool>
      )
    }



    let { footerButtons = [] } = this.props.staticData;

    return (
      <Tool className={classnames("settings-editor", this.props.DragSystem.nativeDragState.dragging && 'nativeDragging')}>
        <ToolHeader
          title="Component Settings"
          iconType={ Info.iconType }
          icon={ Info.icon }
          iconColor={ Info.iconColor }
          onCloseHook={ ()=> this.onClose() }>

          <button onClick={ () => this.onToggleSpreadAll(this.props.spreadAll) }>
            { this.props.spreadAll ? 'all' : 'each' }
          </button>
        </ToolHeader>
        <ToolBody ref={(toolBody) => { this.toolBodyComp = toolBody } }>
          {/*<div className="title">*/}
            {/*<div className="title-wrap">*/}
              {/*{ selectedNode.tag }*/}
            {/*</div>*/}
          {/*</div>*/}
          { selectedDirectiveNodeEditRules.description && <div className="description">
            { selectedDirectiveNodeEditRules.description }
          </div> }



          <div className="time-setting">



            <div className="label">
              <Icon type="schedule" style={{verticalAlign: 'middle'}} />
              <span> </span>기간 설정
            </div>

            { timeValue && (timeValue.start || timeValue.end) ? this.renderScheduleOverview(timeValue)  : <div className='schedule-feedback off'/> }

            <div className="control">

              <DatePicker
                value={this.convertComponentDateToMomentSet(timeValue)}
                onChange={(value) => this.setNodePropByPath(selectedNode, 'componentSchedule', this.convertMomentSetToComponentDate(value)) }/>
            </div>
          </div>



          {
            propKeys.map((key, i)=> this.renderTypeEditor(editableProps[key], key, selectedNode ))
          }

        </ToolBody>

        <ToolFooter>
          {footerButtons.map((btn,i) => <FullmoonBtn
            key={i}
            shape="round"
            normalStyle={{
              display: 'inline-block',
              height: 30,
              borderWidth: 0,

              borderRadius: 100,
              fontSize: 12,
              color: this.context.theme.Tool.layout.footer.fullmoonBtn.textColor.default,
              fontFamily: 'Nanum Square',
              backgroundColor: this.context.theme.Tool.layout.footer.fullmoonBtn.bgColor.default,
              margin:10,
              padding:'0 20px',
              textAlign:'left',
            }}
            hoverStyle={{
              borderWidth: 0,
              backgroundColor:this.context.theme.Tool.layout.footer.fullmoonBtn.bgColor.hovered,
            }}
            moonBlender='overlay'
            onClick={(e)=>this.onClickFooterButton(btn, e, selectedNode)}>
            {btn.title}
          </FullmoonBtn>)}


          { this.props.staticData.configurationTargetMethod === 'canvas-selected' && <FullmoonBtn
            shape="round"
            normalStyle={{
              display: 'inline-block',
              height: 30,
              borderWidth: 0,

              borderRadius: 100,
              fontSize: 12,
              color: this.context.theme.Tool.layout.footer.fullmoonBtn.textColor.default,
              fontFamily: 'Nanum Square',
              backgroundColor: this.context.theme.Tool.layout.footer.fullmoonBtn.bgColor.default,
              margin:10,
              padding:'0 20px',
              textAlign:'left',
            }}
            hoverStyle={{
              borderWidth: 0,
              backgroundColor:this.context.theme.Tool.layout.footer.fullmoonBtn.bgColor.hovered,
            }}
            moonBlender='overlay'
            onClick={(e)=>this.onManualApply()}>
            반영
          </FullmoonBtn>}
        </ToolFooter>

      </Tool>
    )
  }
}

export default Settings;
