import {asyncComponent} from 'react-async-component';

import * as Constants from './Constants';
export const Info = Constants.Info;
export const UIIStatement = Constants.UIIStatement;
export const ToolKey = Constants.ToolKey;
export const CreateOpenActionSheet = Constants.CreateOpenActionSheet;

export reducer from './reducer';
export {initialState} from './reducer';
export const Component = asyncComponent({
  resolve: () => System.import('./Settings'),
});

export {defaultStaticData} from './Constants';

