import defineUIIStatement from '../../../../utils/UII/defineUIIStatement';
import defineActSheet, { Types } from '../../../../utils/UII/actSheet';

export const ToolKey = "ExternalStoreComponentsPalette";
export const Info = {
  type: 'button',
  key: 'ExternalStoreComponentsPalette',
  title: 'Store',
  iconType: 'img',
  icon: '/public/images/vue-logo.png',
  iconColor: '#ce2348',
  options: {
    toolKey : ToolKey,
  },

  actionSheet: defineActSheet({
    "action": "open",
    "type": Types.ToolInterfacePipeline, // ToolInterfacePipeline
    "options": {
      "toolKey": ToolKey,
      "areaIndicator": "[chaining]",
    },
  }),
};


export const UIIStatement = defineUIIStatement(Info);
