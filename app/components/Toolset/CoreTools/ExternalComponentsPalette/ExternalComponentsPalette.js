import React, {Component} from 'react';
import PropTypes from 'prop-types';
import TextField from 'material-ui/TextField';

import Tool from '../common/Tool';
import ToolHeader from '../common/ToolHeader';
import ToolFooter from '../common/ToolFooter';
import ToolBody from '../common/ToolBody';

import './ExternalComponentsPalette.scss';

import {
  Info,
} from './Constants';

class ExternalComponentsPalette extends Component {
  static contextTypes = {
    toolSystem: PropTypes.object,
  }

  constructor(props) {
    super(props)
  }

  render() {
    return (
      <Tool className="component-palette">
        <ToolHeader title="Store" iconType={ Info.iconType } icon={ Info.icon } iconColor={ Info.iconColor }/>

        <ToolBody>
          <TextField
            style={ { width : "100%" } }
            hintText="Search"
          />

          <ul>
            <li>
              Component Palette
            </li>
          </ul>
        </ToolBody>

        <ToolFooter />
      </Tool>
    )
  }
}

export default ExternalComponentsPalette;
