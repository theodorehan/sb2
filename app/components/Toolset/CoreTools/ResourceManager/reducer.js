import * as actionTypes from './actionTypes';
import {
  MAJOR_CATEGORY_WILL_LOAD,
  MAJOR_CATEGORY_LOADED,
} from './Constants';

export const initialState = {
  resourceTarget : '',

  majorCategoryState : null,

  search: '',

  categoriesMap : {},
  rootCategories : [],

  loadingSubCategoryData : true,
  loadingSubCategoryNames : [],

  openedRootCategoryPath : '',
  openedSubCategoryFullPaths : {},

  selectedNodeList : [],
};

export default function ComponentPalette(state = initialState, action) {

  switch (action.type) {
    case actionTypes.CHANGE_RESOURCE_TARGET :
      return Object.assign({}, state, {
        resourceTarget: action.resTarget,
        majorCategoryState : MAJOR_CATEGORY_WILL_LOAD,
        loadedSubCategoryDataListByPath : {},
      });


    case actionTypes.LOADED_COMPONENT_ROOT_CATEGORIES :
      return Object.assign({}, state, {
        categoriesMap : action.categoriesMap, // Set initialize
        rootCategories : action.categories,

        majorCategoryState : MAJOR_CATEGORY_LOADED,
        loadedSubCategoryDataListByPath : {},
      });

    case actionTypes.OPEN_ROOT_CATEGORY:
      return Object.assign({}, state, {
        openedRootCategoryPath : action.categoryPath,
      });


    case actionTypes.OPEN_SUB_CATEGORY:
      return Object.assign({}, state, {
        openedSubCategoryFullPaths : Object.assign({}, state.openedSubCategoryFullPaths, {
          [action.categoryFullPath] : true,
        }),
      });

    case actionTypes.CLOSE_SUB_CATEGORY:
      return Object.assign({}, state, {
        openedSubCategoryFullPaths : Object.assign({}, state.openedSubCategoryFullPaths, {
          [action.categoryFullPath] : false,
        }),
      });
    case actionTypes.START_LOAD_CATEGORY_SUB_DATA_WITH_PATH:
      return Object.assign({}, state, {
        loadingSubCategoryData : true,
        loadingSubCategoryNames : [...state.loadingSubCategoryNames, action.path],
      });

    case actionTypes.SELECT_NODE:
      if( action.maxSize ){
        return Object.assign({}, state, {
          selectedNodeList : [...state.selectedNodeList.slice(0, action.maxSize-1), action.key],
        });
      } else {
        return Object.assign({}, state, {
          selectedNodeList : [...state.selectedNodeList, action.key],
        });
      }
    case actionTypes.UNSELECT_NODE:
      return Object.assign({}, state, {
        selectedNodeList : state.selectedNodeList.filter((key) => key !== action.key),
      });

    case actionTypes.LOADED_CATEGORY_SUB_DATA_BY_PATH:

      let firstFoundIndex = state.loadingSubCategoryNames.indexOf(action.path);

      let loadingSubCategoryNames = state.loadingSubCategoryNames.splice(0, firstFoundIndex).concat(state.loadingSubCategoryNames.slice(firstFoundIndex+1));

      return Object.assign({}, state, {
        categoriesMap : Object.assign({}, state.categoriesMap, {
          [action.path] : action.categories,
        }),
        loadingSubCategoryData : loadingSubCategoryNames.length > 0,
        loadingSubCategoryNames,
      });
    default :
      return state;
  }
}
