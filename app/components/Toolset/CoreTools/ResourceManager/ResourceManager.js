import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import {connect} from 'react-redux';
import TextField from 'material-ui/TextField';

import Tool from '../common/Tool';
import ToolHeader from '../common/ToolHeader';
import ToolFooter from '../common/ToolFooter';
import ToolBody from '../common/ToolBody';
import {
  FooterButton,
} from '../common/ToolButton';

import Draggable from '../../../../Core/Components/Draggable';

import CategoryBox from '../../../../components/CategoryBox';

import LoadableDiv from '../../../../components/LoadableDiv';

import {
  removeVueExtensionAtName,
} from '../../../../Core/Helper/VueSupports';

import {
  loadResourceCategories,
  loadResourceSubCategories,
  uploadResourceImageBase64,
} from '../../../../Core/Network/Site';

import {
  CreateOpenActionSheet as CreateDialogOpenActionSheet,
  ToolKey,
} from '../Dialog/Constants';

import {
  SET_STATIC_DATA_BY_LAYOUT,
  CLOSE_LAYOUT_WITH_ELASTIC,
} from '../../UIISystemActionSheets';

import './style.scss';

import {
  Info,
  RESOURCE_TARGET_IMAGE,
  RESOURCE_TARGET_MUSIC,
  RESOURCE_TARGET_FONT,
  RESOURCE_TARGET_VIDEO,
  RESOURCE_TARGET_SVG,

  MAJOR_CATEGORY_LOADED,
  MAJOR_CATEGORY_WILL_LOAD,
} from './Constants';

import * as Actions from './action';


const CategoryItemKernel = ({item, fullPath, className, componentHTML, componentStyle, loadedComponent}) =>
  <div className={classnames("component-item", className)}>
    <div dangerouslySetInnerHTML={{__html : loadedComponent.html}}></div>
    <style>{loadedComponent.style}</style>
    <div className="component-title">{removeVueExtensionAtName(item.name)}</div>
  </div>


@connect((state) => ({
  User: state.get('User'),
  Site: state.get('Site'),
}))
class ComponentItemWrapper extends Component {
  static propTypes = {
    item: PropTypes.object,
    fullPath: PropTypes.string,
    className: PropTypes.string,
    rightTopUI : PropTypes.func,
    onClickNode : PropTypes.func,
    selectedNodeList : PropTypes.array,
  }

  static contextTypes = {
    layout : PropTypes.object,
    events : PropTypes.object,
    functionStore : PropTypes.object,
  }

  constructor(props) {
    super(props);

    this.state = {
      component : null,
    }
  }

  onDragBegin(){
    this.context.events.emit('component-drag-start');
    // if( this.context.layout.isFloatLayout() ){
    //   this.context.layout.onFlashClose();
    // }
  }

  onDragEnd(){
    this.context.events.emit('component-drag-end');
    // if( this.context.layout.isFloatLayout() ){
    //   this.context.layout.onFlashOpen();
    // }
  }

  componentDidMount(){



  }

  componentDidUpdate(prevProps, prevState){

    if( this.props.fullPath !== prevProps.fullPath ){
      this.setState({
        component : null,
      });
    }
  }

  renderMusic(item){
    return <audio style={{width:200}} controls>
      <source src={`/api/user/${this.props.User.session.id}/site/${this.props.Site.info.id}/${this.props.Site.info.owner}/store/resource/get/${item.file}`} type="audio/mpeg"/>
    </audio>;
  }

  renderVideo(item){
    return <video controls width="200px">
      <source src={`/api/user/${this.props.User.session.id}/site/${this.props.Site.info.id}/${this.props.Site.info.owner}/store/resource/get/${item.file}`} type="video/mp4"/>
    </video>;
  }

  renderImage(item){

    return <div style={{width:100}}>
      <img
        ref={(dom) => this.image = dom}
        style={{maxWidth:100}}
        id={item.file}
        name={item.file}
        src={`/api/user/${this.props.User.session.id}/site/${this.props.Site.info.id}/${this.props.Site.info.owner}/store/resource/get/${item.file}?${item.modify}`}/>
    </div>;
  }


  /**

   Accept-Ranges:bytes
   Cache-Control:public, max-age=0
   Connection:keep-alive
   Date:Wed, 19 Jul 2017 17:33:36 GMT
   ETag:W/"2fdef-15bdd34f8c0"
   Last-Modified:Sat, 06 May 2017 10:01:28 GMT
   X-Powered-By:Express

   src="http://builder.localhost:8080/dataStore/revolution/src/static/image/banner.jpg"
   */

  /**
   *
   * @param item
   * @returns {*}
   */
  renderItemDisplay(item){
    if( /\.(jpe?g|png|gif)$/.test(item.name)) {
      return this.renderImage(item);
    } else if ( /\.svg$/.test(item.name) ){
      return this.renderImage(item);
    } else if ( /\.(mp3)$/.test(item.name) ){
      return this.renderMusic(item);
    } else if ( /\.(mp4|webm)/.test(item.name) ){
      return this.renderVideo(item);
    } else if ( /\.svg$/.test(item.name) ){
      return this.renderImage(item);
    }
  }

  render() {
    let {item, fullPath, className, selectedNodeList} = this.props;

    let isLoading = !this.state.component;

    let extension = item.name.split('.').pop();
    let style = {wordBreak:'break-all'};

    switch(extension){
      case 'jpg':
      case 'png':
      case 'gif':
        style.width = 100;
    }

    return <div
      style={style}
      className={classnames("component-item-wrapper", selectedNodeList.indexOf(item.file) > -1 && "selected" )}
      onClick={(e) => this.props.onClickNode && this.props.onClickNode(e,item)}>
      <div className={classnames("component-item", className)}>

        { this.renderItemDisplay(item) }

        <div className="component-title">
          {item.name.replace(/\.\w+$/,'')}
          {/*<span className="tag">{item.name.split('.').pop()}</span>*/}
        </div>
      </div>

      <div className="right-top-ui-wrapper">
        { this.props.rightTopUI && this.props.rightTopUI(item, this) }
      </div>

      <div className="left-top-ui-wrapper">

      </div>

      <div className="extension-info">
        <span className="text-wrapper">
          {extension}
        </span>
      </div>
    </div>
  }
}

/**

 <LoadableDiv isLoading={isLoading}>
 { isLoading ?
   <span>
     <img src="/public/images/vue-logo.png" width="40px"/>
     <div>{removeVueExtensionAtName(item.name)}</div>
   </span> :
   <Draggable
     component={CategoryItemKernel}
     onDragBegin={:: this.onDragBegin }
     onDragEnd={:: this.onDragEnd }
     _draggableType="component"
     _draggableData={{...item, fullPath}}
     item={item}
     ghostOpacity={1}
     fullPath={fullPath}
     className={className}
     loadedComponent={this.state.component}/>
 }
 </LoadableDiv>

 */

@connect((state) => ({
  User: state.get('User'),
  Site: state.get('Site'),
}))
class ResourceManager extends Component {
  static contextTypes = {
    toolSystem: PropTypes.object,
    elastic: PropTypes.object,
    UIISystems: PropTypes.object,
    functionStore : PropTypes.object,
    global : PropTypes.object,
  }

  static propTypes = {
    sourceComponentType: PropTypes.string,
    User: PropTypes.object,
    Site: PropTypes.object,
    rootCategoryState: PropTypes.any,
    search: PropTypes.string,
    categoriesMap: PropTypes.object,
    rootCategories: PropTypes.array,
    loadingSubCategoryData: PropTypes.bool,
    openedRootCategoryPath: PropTypes.string,
    openedSubCategoryFullPaths: PropTypes.object,
  }

  constructor(props) {
    super(props)
  }

  loadComponentPreview(categoryPath){

  }

  loadComponentCategory() {
    let {resourceTarget} = this.props;

    switch (resourceTarget) {
      case RESOURCE_TARGET_IMAGE :
        return loadResourceCategories(this.props.User.session.id, this.props.Site.info.id, this.props.Site.info.owner, RESOURCE_TARGET_IMAGE);
      case RESOURCE_TARGET_MUSIC :
        return loadResourceCategories(this.props.User.session.id, this.props.Site.info.id, this.props.Site.info.owner, RESOURCE_TARGET_MUSIC);
      case RESOURCE_TARGET_VIDEO :
        return loadResourceCategories(this.props.User.session.id, this.props.Site.info.id, this.props.Site.info.owner, RESOURCE_TARGET_VIDEO);
      case RESOURCE_TARGET_FONT :
        return loadResourceCategories(this.props.User.session.id, this.props.Site.info.id, this.props.Site.info.owner, RESOURCE_TARGET_FONT);
      case RESOURCE_TARGET_SVG :
        return loadResourceCategories(this.props.User.session.id, this.props.Site.info.id, this.props.Site.info.owner, RESOURCE_TARGET_SVG);
    }
  }

  loadSubCategoryData(targetCategoryPath) {
    let {resourceTarget} = this.props;
    this.context.elastic.dispatch(Actions.START_LOAD_CATEGORY_SUB_DATA_WITH_PATH(targetCategoryPath));

    switch (resourceTarget) {
      case RESOURCE_TARGET_IMAGE :
        return loadResourceSubCategories(this.props.User.session.id, this.props.Site.info.id,  this.props.Site.info.owner, RESOURCE_TARGET_IMAGE, targetCategoryPath);
      case RESOURCE_TARGET_MUSIC :
        return loadResourceSubCategories(this.props.User.session.id, this.props.Site.info.id,  this.props.Site.info.owner, RESOURCE_TARGET_MUSIC, targetCategoryPath);
      case RESOURCE_TARGET_VIDEO :
        return loadResourceSubCategories(this.props.User.session.id, this.props.Site.info.id,  this.props.Site.info.owner, RESOURCE_TARGET_VIDEO, targetCategoryPath);
      case RESOURCE_TARGET_FONT :
        return loadResourceSubCategories(this.props.User.session.id, this.props.Site.info.id,  this.props.Site.info.owner, RESOURCE_TARGET_FONT, targetCategoryPath);
      case RESOURCE_TARGET_SVG :
        return loadResourceSubCategories(this.props.User.session.id, this.props.Site.info.id,  this.props.Site.info.owner, RESOURCE_TARGET_SVG, targetCategoryPath);
    }
  }

  onRequestOpenAndFillSubCategoryChildren(cat, fullPathname) {
    if (fullPathname.split('/').length === 1) {
      this.context.elastic.dispatch(Actions.OPEN_ROOT_CATEGORY(fullPathname));
    } else {
      this.context.elastic.dispatch(Actions.OPEN_SUB_CATEGORY(fullPathname));
    }

   this.fillSubCategory(fullPathname);
  }

  fillSubCategory(fullPathname){
    return new Promise((resolve,reject)=>{
      this.loadSubCategoryData(fullPathname)
        .then((res) => {
          let {data} = res;
          let {filledCategoryItem} = data;

          this.context.elastic.dispatch(Actions.LOADED_CATEGORY_SUB_DATA_BY_PATH(fullPathname, filledCategoryItem));

          resolve();
        });
    })
  }

  onRequestCloseSubCategory(cat, fullPathname) {
    if (fullPathname.split('/').length === 1) {
    } else {
      this.context.elastic.dispatch(Actions.CLOSE_SUB_CATEGORY(fullPathname));
    }
  }

  componentDidMount() {

    let initialResourceTarget = RESOURCE_TARGET_IMAGE;
    if( this.props.staticData ){
      if ( this.props.staticData.use && Array.isArray(this.props.staticData.use) ){
        let first = this.props.staticData.use[0];
        if( first ){
          initialResourceTarget = first;
        }
      }
    }
    this.context.elastic.dispatch(Actions.CHANGE_RESOURCE_TARGET(initialResourceTarget));




    /*Adobe creativeSDK*/
    window.ceditor = this.csdkImageEditor = new Aviary.Feather({
      language: 'ko',
      jpgQuality: 100,
      apiKey: '6c3c7f71fdb949dc922f53b2809d4279',
      onSaveButtonClicked: (imagePath) => {
        let imagePathTokens = imagePath.split('/');
        imagePathTokens.shift(); // 이미지 패스의 앞부분 리소스 타겟(image) 제거
        let imageName = imagePathTokens.pop();
        let parentPath = imagePathTokens.join('/');

        let imageNameTokens = imageName.split('.');
        let imageExt = imageNameTokens.pop();
        imageName = imageNameTokens.join('.');

        let transferData = {
          title : "Image Save",
          fields : [
            {
              label : 'Image name',
              key : 'name',
              type : 'input',
              inputType : 'string',
              placeholder : 'Input image name...',
              defaultValue : imageName,
              validator : {
                functionKey : 'imageNameValidator',
              },
            },

            {
              type : 'button-set',
              buttons : [
                {
                  label : 'Overwrite',
                  functionKey : 'overwrite' ,
                  level : 'force-positive',
                  validate : true,
                },
                {
                  label : 'Save',
                  functionKey : 'Save' ,
                  level : 'positive',
                  validate : true,
                },
                {
                  label : 'Cancel' ,
                  functionKey : 'cancel',
                  level : 'negative',
                },
              ],
            },
          ],
        };

        this.context.UIISystems.emitActionSheet(CreateDialogOpenActionSheet("modal"), transferData, {
          width : 450,
          height: 180,
        });

        this.context.functionStore.register('imageNameValidator', (name)=>{
          if( /\//.test(name) ){
            return {
              result : false,
              message : '이름에 "/" 는 사용할 수 없습니다.',
            };
          }

          let categoryNode = this.props.categoriesMap[parentPath || '___etc'] ;

          if( categoryNode.children.find((node)=> node.name === `${name}.png`) ){
            return {
              result : 0,
              message : '다른 이미지와 이름이 중복됩니다. 덮어 쓰시려면 Overwrite 를 클릭하세요.',
            }
          }

          // if( `${name}.${imageExt}` === `${imageName}.${imageExt}`){
          //   if( imageExt != 'png' ){
          //     return {
          //       result : true,
          //       level : 'info',
          //       message : '편집중인 이미지와 이름은 같지만 png 포맷이 아니므로 새로운 이미지가 생성됩니다.',
          //     }
          //   }
          //
          //   return {
          //     result : 0,
          //     message : '편집중인 이미지와 같은 이름입니다. 덮어쓰시려면 "Overwrite Save"를 클릭하세요.',
          //   }
          // }



          return {
            result : true,
          }
        });


        this.context.functionStore.register('overwrite', (fields)=>{
          let {name : nameField} = fields;
          let {value : imageName} = nameField;

          if( nameField.validation.result > -1 ){
            save(imageName, true)
              .then(()=>{

              });
          }
        });

        this.context.functionStore.register('Save', (fields)=>{
          let {name : nameField} = fields;
          let {value : imageName} = nameField;

          if( nameField.validation.result === true ){
            save(imageName)
              .then(()=>{

              });
          }
        });


        const save = (saveImageName, overwirte)=>{
          let reqID = this.context.global.uiBlockingReqBegin();

          return new Promise((resolve, reject)=>{
            console.log(this.csdkImageEditor);
            window.aa = this.csdkImageEditor;

            this.csdkImageEditor.getImageData( (base64img) => {
              uploadResourceImageBase64(this.props.User.session.id, this.props.Site.info.id, this.props.Site.info.owner, RESOURCE_TARGET_IMAGE, base64img, `${parentPath}/${saveImageName}`, overwirte)
                .then((res) => {
                  this.fillSubCategory(parentPath)
                    .then(()=>{
                      this.context.UIISystems.emitActionSheet(CLOSE_LAYOUT_WITH_ELASTIC("modal", ToolKey, true));
                      this.csdkImageEditor.close();
                      this.context.global.uiBlockingReqFinish(reqID);
                    });
                });
            })
          });
        }



        return false; // false 를 반환하여 adobe 서버에 데이터가 전송되는것을 막는다.
      },

      onSave: function (imageID, newURL) {

      },

      onError: function (errorObj) {
        console.error(errorObj);
      },
    });
  }

  onClickNode(e, node){

    let file = node.file;

    if( this.props.selectedNodeList.indexOf(file) > -1 ){
      this.context.elastic.dispatch(Actions.UNSELECT_NODE(file), true)
        .then(()=>{
          if( this.props.staticData.selectionCallback ){
            this.context.functionStore.get(this.props.staticData.selectionCallback)(this.props.selectedNodeList);
          }
        })
    } else {
      this.context.elastic.dispatch(Actions.SELECT_NODE(file, this.props.staticData.maxSelectCount), true)
        .then(()=>{
          if( this.props.staticData.selectionCallback ){
            this.context.functionStore.get(this.props.staticData.selectionCallback)(this.props.selectedNodeList);
          }
        })
    }
  }

  onClickStore(e){
    console.log('Click Store');
  }

  onClickNodeEdit(e, node, nodeElement){
    e.stopPropagation();
    e.preventDefault();

    let pathTokens = node.file.split('/');
    let resTarget = pathTokens[0];


    switch(resTarget){
      case RESOURCE_TARGET_IMAGE :
        this.csdkImageEditor.launch({
          image: node.file,
          url: `http://${location.host}/api/user/${this.props.User.session.id}/site/${this.props.Site.info.id}/${this.props.Site.info.owner}/store/resource/get/${node.file}?${node.modify}${Math.random()}`,
        });
        break;
    }
  }

  onClickNodeRemove(e, node){
    e.stopPropagation();
    e.preventDefault();

    console.log('remove', e, node);
  }



  componentDidUpdate(prevProps, prevState) {
    // let prevSourceComponentType = prevProps.sourceComponentType;
    // let currentSourceComponentType = this.props.sourceComponentType;


    if (this.props.majorCategoryState === MAJOR_CATEGORY_WILL_LOAD) {
      this.loadComponentCategory()
        .then((res) => {
          let {data} = res;
          let {categories} = data;
          let categoriesMap = {};

          categories.map((cat) => {
            categoriesMap[cat.pathName] = cat;
          })

          this.context.elastic.dispatch(Actions.LOADED_COMPONENT_ROOT_CATEGORIES(categoriesMap, categories));
        });
    }
  }

  render() {
    return (
      <Tool className="resource-manager">
        <ToolHeader
          title="Resource Manager"
          iconType={ Info.iconType } icon={ Info.icon }
          iconColor={ Info.iconColor }/>

        <ToolBody disableScroll={true} isLoading={this.props.rootCategoryState !== MAJOR_CATEGORY_LOADED} nofooter>
          <div className="tab">
            <ul>
              { this.props.staticData.use.map((target, i)=>
                <li key={i}>
                <button
                  className={ target === this.props.resourceTarget && 'active' }
                  onClick={()=>this.context.elastic.dispatch(Actions.CHANGE_RESOURCE_TARGET(target))}>
                  {target}
                </button>
              </li>)}
            </ul>
          </div>

          {
            this.props.majorCategoryState === MAJOR_CATEGORY_LOADED &&
            <CategoryBox
              className="category-box"
              onRequestOpenAndFillSubCategoryChildren={(categoryItem, fullPathname) => this.onRequestOpenAndFillSubCategoryChildren(categoryItem, fullPathname)}
              onRequestCloseSubCategory={(categoryItem, fullPathname) => this.onRequestCloseSubCategory(categoryItem, fullPathname)}
              selectedNodeList={this.props.selectedNodeList}
              rootCategoryPaths={this.props.rootCategories.map((cat) => cat.pathName)}
              categoriesMap={this.props.categoriesMap}
              subLoading={this.props.loadingSubCategoryData}
              openedRootCategoryPath={this.props.openedRootCategoryPath}
              openedSubCategoryFullPaths={this.props.openedSubCategoryFullPaths}
              itemNameFilter={removeVueExtensionAtName}
              CategoryItemComponent={ComponentItemWrapper}
              rootCategoryMenuAdditionArray={[{name:'Store', onClick: (e)=>{this.onClickStore(e)}} ]}
              rightTopUI={ (node, categoryElement) => <span>
                { this.props.staticData.removable && <button className="remove" onClick={(e) => this.onClickNodeRemove(e, node) }><i className="fa fa-trash-o"/></button>}
                <span> </span>
                { this.props.staticData.editable && RESOURCE_TARGET_IMAGE === this.props.resourceTarget && <button className="edit" onTouchTap={(e) => this.onClickNodeEdit(e, node, categoryElement) }><i className="fa fa-pencil"/></button>}
              </span>}
              onClickNode={ (e, node) => this.onClickNode(e, node) } />
          }
        </ToolBody>


      </Tool>
    )
  }
}


export default ResourceManager;
