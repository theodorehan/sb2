import * as ActionTypes from './actionTypes';

export function CHANGE_RESOURCE_TARGET(resTarget){
  return {
    type : ActionTypes.CHANGE_RESOURCE_TARGET,
    resTarget,
  }
}



export function LOADED_COMPONENT_ROOT_CATEGORIES ( categoriesMap, categories ) {
  return {
    type : ActionTypes.LOADED_COMPONENT_ROOT_CATEGORIES,
    categoriesMap,
    categories,
  }
}



// SUB
export function OPEN_ROOT_CATEGORY(categoryPath) {
  return {
    type : ActionTypes.OPEN_ROOT_CATEGORY,
    categoryPath,
  }
}

export function OPEN_SUB_CATEGORY(categoryFullPath) {
  return {
    type : ActionTypes.OPEN_SUB_CATEGORY,
    categoryFullPath,
  }
}

export function CLOSE_SUB_CATEGORY(categoryFullPath) {
  return {
    type : ActionTypes.CLOSE_SUB_CATEGORY,
    categoryFullPath,
  }
}

export function LOADED_CATEGORY_SUB_DATA_BY_PATH (path, categories) {
  return {
    type : ActionTypes.LOADED_CATEGORY_SUB_DATA_BY_PATH,
    path,
    categories,
  }
}

export function START_LOAD_CATEGORY_SUB_DATA_WITH_PATH(path) {
  return {
    type : ActionTypes.START_LOAD_CATEGORY_SUB_DATA_WITH_PATH,
    path,
  }
}


export  function UNSELECT_NODE(key) {
  return {
    type : ActionTypes.UNSELECT_NODE,
    key,
  }
}


export  function SELECT_NODE(key,maxSize) {
  return {
    type : ActionTypes.SELECT_NODE,
    key,
    maxSize,
  }
}

export function RESET_SELECTEDS() {
  return {
    type : ActionTypes.RESET_SELECTEDS,
  }
}
