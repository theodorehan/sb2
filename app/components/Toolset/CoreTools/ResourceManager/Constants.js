import defineUIIStatement from '../../../../utils/UII/defineUIIStatement';
import defineActSheet, { Types } from '../../../../utils/UII/actSheet';

export const ToolKey = "ResourceManager";

export const CreateOpenActionSheet = (areaIndicator) => defineActSheet({
  "action" : "open",
  "type": Types.ToolInterfacePipeline, // ToolInterfacePipeline
  "options" : {
    "toolKey" : ToolKey,
    "areaIndicator" : areaIndicator,
  },
});

export const Info = {
  type: 'button',
  key: 'ResourceManager',
  iconType: 'material',
  icon: 'ImageCamera',
  iconColor: 'rgb(216, 81, 81)',
  options: {
    toolKey : ToolKey,
  },

  actionSheet: CreateOpenActionSheet('[chaining]'),
};


export const UIIStatement = defineUIIStatement(Info);


export const RESOURCE_TARGET_IMAGE = 'image';
export const RESOURCE_TARGET_VIDEO = 'video';
export const RESOURCE_TARGET_MUSIC = 'music';
export const RESOURCE_TARGET_SVG = 'svg';
export const RESOURCE_TARGET_FONT = 'font';

export const MAJOR_CATEGORY_WILL_LOAD = 'root-will-load';
export const MAJOR_CATEGORY_LOADED = 'root-loaded';


export const defaultStaticData = {
  use : [
    RESOURCE_TARGET_IMAGE,
    RESOURCE_TARGET_VIDEO,
    RESOURCE_TARGET_MUSIC,
    RESOURCE_TARGET_SVG,
    RESOURCE_TARGET_FONT,
  ],

  selectable : false, // selectable 리소스를 선택할 수 있다.
  maxSelectCount : 1,
  editable : true,
  removable : true,

  selectionCallback : null, // functionKey of functionStore
}
