import namespaceBindGenerator from '../../../../utils/redux/namespaceBindGenerator';
let defineType = namespaceBindGenerator('ResourceManager');


export const CHANGE_RESOURCE_TARGET = defineType('CHANGE_RESOURCE_TARGET');
export const LOADED_COMPONENT_ROOT_CATEGORIES = defineType('LOADED_COMPONENT_ROOT_CATEGORIES');

// SUB
export const OPEN_ROOT_CATEGORY = defineType('OPEN_ROOT_CATEGORY');
export const OPEN_SUB_CATEGORY = defineType('OPEN_SUB_CATEGORY');
export const CLOSE_SUB_CATEGORY = defineType('CLOSE_SUB_CATEGORY');
export const LOADED_CATEGORY_SUB_DATA_BY_PATH = defineType('LOADED_CATEGORY_SUB_DATA_BY_PATH');
export const START_LOAD_CATEGORY_SUB_DATA_WITH_PATH = defineType('START_LOAD_CATEGORY_SUB_DATA_WITH_PATH');


export const SELECT_NODE = defineType('SELECT_NODE');
export const UNSELECT_NODE = defineType('UNSELECT_NODE');

export const RESET_SELECTEDS = defineType('RESET_SELECTEDS');
