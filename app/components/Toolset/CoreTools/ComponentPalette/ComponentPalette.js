import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import {connect} from 'react-redux';
import TextField from 'material-ui/TextField';

import Tool from '../common/Tool';
import ToolHeader from '../common/ToolHeader';
import ToolFooter from '../common/ToolFooter';
import ToolBody from '../common/ToolBody';
import {
  FooterButton,
} from '../common/ToolButton';

import Draggable from '../../../../Core/Components/Draggable';

import CategoryBox from '../../../../components/CategoryBox';

import LoadableDiv from '../../../../components/LoadableDiv';
import ComponentItemWrapper from './ComponentItemWrapper';

import {
  removeVueExtensionAtName,
} from '../../../../Core/Helper/VueSupports';

import './ComponentPalette.scss';

import {
  Info,
  SOURCE_COMPONENT_TYPE_BUILT_IN,
  SOURCE_COMPONENT_TYPE_FRAGMENT,
  SOURCE_COMPONENT_TYPE_INCLUDED,
  SOURCE_COMPONENT_TYPE_STORE,
  ROOT_CATEGORY_LOADED,
  ROOT_CATEGORY_WILL_LOAD,
} from './Constants';

import * as Actions from './action';

import {
  getBuiltInComponentCategories,
  getBuiltInComponentCategorySub,

  getFragmentComponentCategories,
  getFragmentComponentCategorySub,

  getIncludedComponentCategories,
  getIncludedComponentCategorySub,
} from '../../../../Core/Network/AssignedStore';


@connect((state) => ({
  User: state.get('User'),
  Site: state.get('Site'),
}))
class ComponentPalette extends Component {
  static contextTypes = {
    toolSystem: PropTypes.object,
    elastic: PropTypes.object,
  }

  static propTypes = {
    sourceComponentType: PropTypes.string,
    User: PropTypes.object,
    Site: PropTypes.object,
    rootCategoryState: PropTypes.any,
    search: PropTypes.string,
    categoriesMap: PropTypes.object,
    rootCategories: PropTypes.array,
    loadingSubCategoryData: PropTypes.bool,
    openedRootCategoryPath: PropTypes.string,
    openedSubCategoryFullPaths: PropTypes.object,
  }

  constructor(props) {
    super(props)
  }

  loadComponentPreview(categoryPath){

  }

  loadComponentCategory() {
    let {sourceComponentType} = this.props;

    switch (sourceComponentType) {
      case SOURCE_COMPONENT_TYPE_BUILT_IN :
        return getBuiltInComponentCategories(this.props.User.session.id, this.props.Site.info.id, this.props.Site.info.owner);
      case SOURCE_COMPONENT_TYPE_FRAGMENT :
        return getFragmentComponentCategories(this.props.User.session.id, this.props.Site.info.id, this.props.Site.info.owner);
      case SOURCE_COMPONENT_TYPE_INCLUDED :
        return getIncludedComponentCategories(this.props.User.session.id, this.props.Site.info.id, this.props.Site.info.owner);
    }
  }

  loadSubCategoryData(targetCategoryPath) {
    let {sourceComponentType} = this.props;
    this.context.elastic.dispatch(Actions.START_LOAD_CATEGORY_SUB_DATA_WITH_PATH(targetCategoryPath));


    switch (sourceComponentType) {
      case SOURCE_COMPONENT_TYPE_BUILT_IN :
        return getBuiltInComponentCategorySub(this.props.User.session.id, this.props.Site.info.id, this.props.Site.info.owner, targetCategoryPath);
      case SOURCE_COMPONENT_TYPE_FRAGMENT :
        return getFragmentComponentCategorySub(this.props.User.session.id, this.props.Site.info.id, this.props.Site.info.owner, targetCategoryPath);
      case SOURCE_COMPONENT_TYPE_INCLUDED :
        return getIncludedComponentCategorySub(this.props.User.session.id, this.props.Site.info.id, this.props.Site.info.owner, targetCategoryPath);
    }
  }

  onRequestOpenAndFillSubCategoryChildren(cat, fullPathname) {
    if (fullPathname.split('/').length === 1) {
      this.context.elastic.dispatch(Actions.OPEN_ROOT_CATEGORY(fullPathname));
    } else {
      this.context.elastic.dispatch(Actions.OPEN_SUB_CATEGORY(fullPathname));
    }

    this.loadSubCategoryData(fullPathname)
      .then((res) => {
        let {data} = res;
        let {filledCategoryItem} = data;

        this.context.elastic.dispatch(Actions.LOADED_CATEGORY_SUB_DATA_BY_PATH(fullPathname, filledCategoryItem));
      });
  }

  onRequestCloseSubCategory(cat, fullPathname) {
    if (fullPathname.split('/').length === 1) {
    } else {
      this.context.elastic.dispatch(Actions.CLOSE_SUB_CATEGORY(fullPathname));
    }
  }

  componentDidMount() {
    this.context.elastic.dispatch(Actions.CHANGE_SOURCE_COMPONENT_TYPE(SOURCE_COMPONENT_TYPE_BUILT_IN));
  }

  componentDidUpdate(prevProps, prevState) {
    // let prevSourceComponentType = prevProps.sourceComponentType;
    // let currentSourceComponentType = this.props.sourceComponentType;


    if (this.props.rootCategoryState === ROOT_CATEGORY_WILL_LOAD) {
      this.loadComponentCategory()
        .then((res) => {
          let {data} = res;
          let {categories} = data;
          let categoriesMap = {};

          categories.map((cat) => {
            categoriesMap[cat.pathName] = cat;
          })

          this.context.elastic.dispatch(Actions.LOADED_COMPONENT_ROOT_CATEGORIES(categoriesMap, categories));
        });
    }
  }

  render() {
    return (
      <Tool className="component-palette">
        <ToolHeader
          title="Internal Components"
          iconType={ Info.iconType } icon={ Info.icon }
          iconColor={ Info.iconColor }/>

        <ToolBody disableScroll={true} isLoading={this.props.rootCategoryState !== ROOT_CATEGORY_LOADED} nofooter>
          {
            this.props.rootCategoryState === ROOT_CATEGORY_LOADED &&
            <CategoryBox
              onRequestOpenAndFillSubCategoryChildren={(categoryItem, fullPathname) => this.onRequestOpenAndFillSubCategoryChildren(categoryItem, fullPathname)}
              onRequestCloseSubCategory={(categoryItem, fullPathname) => this.onRequestCloseSubCategory(categoryItem, fullPathname)}
              rootCategoryPaths={this.props.rootCategories.map((cat) => cat.pathName)}
              categoriesMap={this.props.categoriesMap}
              subLoading={this.props.loadingSubCategoryData}
              openedRootCategoryPath={this.props.openedRootCategoryPath}
              openedSubCategoryFullPaths={this.props.openedSubCategoryFullPaths}
              itemNameFilter={removeVueExtensionAtName}
              CategoryItemComponent={ComponentItemWrapper}/>
          }
        </ToolBody>


      </Tool>
    )
  }
}


export default ComponentPalette;
