import * as actionTypes from './actionTypes';
import {
  ROOT_CATEGORY_WILL_LOAD,
  ROOT_CATEGORY_LOADED,
} from './Constants';

export const initialState = {
  sourceComponentType : '',

  rootCategoryState : null,

  search: '',

  categoriesMap : {},
  rootCategories : [],

  loadingSubCategoryData : true,
  loadingSubCategoryNames : [],

  openedRootCategoryPath : '',
  openedSubCategoryFullPaths : {},
};

export default function ComponentPalette(state = initialState, action) {

  switch (action.type) {
    case actionTypes.CHANGE_SOURCE_COMPONENT_TYPE :
      return Object.assign({}, state, {
        sourceComponentType: action.sourceComponentType,
        rootCategoryState : ROOT_CATEGORY_WILL_LOAD,
        loadedSubCategoryDataListByPath : {},
      });


    case actionTypes.LOADED_COMPONENT_ROOT_CATEGORIES :
      return Object.assign({}, state, {
        categoriesMap : action.categoriesMap, // Set initialize
        rootCategories : action.categories,

        rootCategoryState : ROOT_CATEGORY_LOADED,
        loadedSubCategoryDataListByPath : {},
      });

    case actionTypes.OPEN_ROOT_CATEGORY:
      return Object.assign({}, state, {
        openedRootCategoryPath : action.categoryPath,
      });


    case actionTypes.OPEN_SUB_CATEGORY:
      return Object.assign({}, state, {
        openedSubCategoryFullPaths : Object.assign({}, state.openedSubCategoryFullPaths, {
          [action.categoryFullPath] : true,
        }),
      });

    case actionTypes.CLOSE_SUB_CATEGORY:
      return Object.assign({}, state, {
        openedSubCategoryFullPaths : Object.assign({}, state.openedSubCategoryFullPaths, {
          [action.categoryFullPath] : false,
        }),
      });
    case actionTypes.START_LOAD_CATEGORY_SUB_DATA_WITH_PATH:
      return Object.assign({}, state, {
        loadingSubCategoryData : true,
        loadingSubCategoryNames : [...state.loadingSubCategoryNames, action.path],
      });

    case actionTypes.LOADED_CATEGORY_SUB_DATA_BY_PATH:

      let firstFoundIndex = state.loadingSubCategoryNames.indexOf(action.path);

      let loadingSubCategoryNames = state.loadingSubCategoryNames.splice(0, firstFoundIndex).concat(state.loadingSubCategoryNames.slice(firstFoundIndex+1));

      return Object.assign({}, state, {
        categoriesMap : Object.assign({}, state.categoriesMap, {
          [action.path] : action.categories,
        }),
        loadingSubCategoryData : loadingSubCategoryNames.length > 0,
        loadingSubCategoryNames,
      });
    default :
      return state;
  }
}
