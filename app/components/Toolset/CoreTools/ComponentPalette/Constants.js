import defineUIIStatement from '../../../../utils/UII/defineUIIStatement';
import defineActSheet, { Types } from '../../../../utils/UII/actSheet';

export const ToolKey = "ComponentPalette";
export const Info = {
  type: 'button',
  key: 'ComponentPalette',
  title: 'app.tool.components',
  titleType : 'code',
  iconType: 'img',
  icon: '/public/images/vue-logo.png',
  // iconColor: '#ce2348',
  options: {
    toolKey : ToolKey,
  },

  actionSheet: defineActSheet({
    "action": "open",
    "type": Types.ToolInterfacePipeline, // ToolInterfacePipeline
    "options": {
      "toolKey": ToolKey,
      "areaIndicator": "[chaining]",
    },
  }),
};


export const UIIStatement = defineUIIStatement(Info);


export const SOURCE_COMPONENT_TYPE_BUILT_IN = 'built-in';
export const SOURCE_COMPONENT_TYPE_FRAGMENT = 'fragment';
export const SOURCE_COMPONENT_TYPE_INCLUDED = 'included';
export const SOURCE_COMPONENT_TYPE_STORE = 'store';

export const ROOT_CATEGORY_WILL_LOAD = 'root-will-load';
export const ROOT_CATEGORY_LOADED = 'root-loaded';
