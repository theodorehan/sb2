import namespaceBindGenerator from '../../../../utils/redux/namespaceBindGenerator';
let defineType = namespaceBindGenerator('ComponentPalette');


export const CHANGE_SOURCE_COMPONENT_TYPE = defineType('CHANGE_SOURCE_COMPONENT_TYPE');
export const LOADED_COMPONENT_ROOT_CATEGORIES = defineType('LOADED_COMPONENT_ROOT_CATEGORIES');

// SUB
export const OPEN_ROOT_CATEGORY = defineType('OPEN_ROOT_CATEGORY');
export const OPEN_SUB_CATEGORY = defineType('OPEN_SUB_CATEGORY');
export const CLOSE_SUB_CATEGORY = defineType('CLOSE_SUB_CATEGORY');
export const LOADED_CATEGORY_SUB_DATA_BY_PATH = defineType('LOADED_CATEGORY_SUB_DATA_BY_PATH');
export const START_LOAD_CATEGORY_SUB_DATA_WITH_PATH = defineType('START_LOAD_CATEGORY_SUB_DATA_WITH_PATH');
