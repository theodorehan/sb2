import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import './style.scss';

import {
  removeVueExtensionAtName,
} from '../../../../../Core/Helper/VueSupports';


import {
  getBuiltInComponentCategoryComponentPreview,
} from '../../../../../Core/Network/AssignedStore';


import Draggable from '../../../../../Core/Components/Draggable';

const CategoryItemKernel = ({item, fullPath, className, componentHTML, componentStyle, loadedComponent}) =>
  <div className={classnames("component-item", className)}>
    <div dangerouslySetInnerHTML={{__html : loadedComponent.html}}></div>
    <style>{loadedComponent.style}</style>
    <div className="component-title">{removeVueExtensionAtName(item.name)}</div>
  </div>


@connect((state) => ({
  User: state.get('User'),
  Site: state.get('Site'),
  Studio: state.get('Studio')
}))
export default class ComponentItemWrapper extends Component {
  static propTypes = {
    item: PropTypes.object,
    fullPath: PropTypes.string,
    className: PropTypes.string,
    width : PropTypes.any,
    imageMaxHeight : PropTypes.number,
    ice2fileType : PropTypes.any,
  }

  static contextTypes = {
    layout : PropTypes.object,
    events : PropTypes.object,
    ice2 : PropTypes.func,
  }

  static defaultProps = {
    width : 100,
  }

  constructor(props) {
    super(props);

    this.state = {
      component : null,
    }
  }

  onDragBegin(){
    this.context.events.emit('on-hide-leftArea');
    this.context.events.emit('on-hide-rightArea');
    // if( this.context.layout.isFloatLayout() ){
    //   this.context.layout.onFlashClose();
    // }
  }

  onDragEnd(){
    this.context.events.emit('on-show-leftArea');
    this.context.events.emit('on-show-rightArea');
    // if( this.context.layout.isFloatLayout() ){
    //   this.context.layout.onFlashOpen();
    // }
  }

  loadComponent(){
    getBuiltInComponentCategoryComponentPreview(this.props.User.session.id, this.props.Site.info.id, this.props.Site.info.owner, this.props.fullPath)
      .then((res)=>{
        let { data } = res;
        let { previewComponent } = data;

        this.setState({
          component : previewComponent,
        });
      }).catch((e) => {

      this.loadComponent();
    });
  }

  componentDidMount(){
    // this.loadComponent();
  }

  componentDidUpdate(prevProps, prevState){

    // if( this.props.fullPath !== prevProps.fullPath ){
    //   this.setState({
    //     component : null,
    //   });
    //
    //   this.loadComponent();
    // }
  }

  // render() {
  //   let {item, fullPath, className} = this.props;
  //
  //   let isLoading = !this.state.component;
  //
  //   return <div className="component-item-wrapper">
  //     <LoadableDiv isLoading={isLoading}>
  //       { isLoading ?
  //         <span>
  //           <img src="/public/images/vue-logo.png" width="40px"/>
  //           <div>{removeVueExtensionAtName(item.name)}</div>
  //         </span> :
  //         <Draggable
  //           component={CategoryItemKernel}
  //           onDragBegin={:: this.onDragBegin }
  //           onDragEnd={:: this.onDragEnd }
  //           _draggableType="component"
  //           _draggableData={{...item, fullPath}}
  //           item={item}
  //           ghostOpacity={1}
  //           fullPath={fullPath}
  //           className={className}
  //           loadedComponent={this.state.component}/>
  //       }
  //     </LoadableDiv>
  //   </div>
  // }

  render() {
    let {item, fullPath, className} = this.props;

    let isLoading = !this.state.component;

    // console.log(item);
    let imageUrl = `/api/user/${this.props.User.session.id}/site/${this.props.Site.info.id}/${this.props.Site.info.owner}/store/builtin-component-categories/component-preview-image?path=${this.props.fullPath}`
    imageUrl = `/componentImages/${item.componentLocation}.png?__s__=${this.props.Studio.session.id}`

    if( this.props.ice2fileType ){
      let ift = this.props.ice2fileType;
      if( ift.storePath ){
        imageUrl = this.context.ice2.defaults.baseURL + '/' + ift.storePath;
      } else {
        imageUrl = ift;
      }

    }


    return <div className="component-item-wrapper" style={{width:this.props.width}}>
      <Draggable
        component={CategoryItemKernel}
        onDragBegin={:: this.onDragBegin }
        onDragEnd={:: this.onDragEnd }
        _draggableType="component"
        _draggableData={{...item, fullPath}}
        _useDetachedGhostImage={imageUrl}
        _additionalStyle={{width:'100%'}}
        item={item}
        ghostOpacity={0}
        fullPath={fullPath}
        className={className}
        loadedComponent={{html:`<img style="max-width:${ parseFloat(this.props.width-10) }px;${this.props.imageMaxHeight && ('max-height:'+this.props.imageMaxHeight+'px')}" src="${imageUrl}"/>`}}/>
    </div>
  }
}
