import defineUIIStatement from '../../../../utils/UII/defineUIIStatement';
import defineActSheet, { Types } from '../../../../utils/UII/actSheet';

export const ToolKey = "VueEditor";
export const Info = {
  type : 'button',
  key : 'VueEditor',
  title : 'Vue Editor',
  iconType : 'fa',
  icon : 'icon',
  options: {
    toolKey : ToolKey,
  },

  actionSheet : defineActSheet({
    "action" : "open",
    "type": Types.ToolInterfacePipeline, // ToolInterfacePipeline
    "options" : {
      "toolKey" : ToolKey,
      "areaIndicator" : "[chaining]",
    },
  }),
}

export const UIIStatement = defineUIIStatement(Info);
