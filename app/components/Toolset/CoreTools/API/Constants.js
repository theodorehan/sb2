import defineUIIStatement from '../../../../utils/UII/defineUIIStatement';
import defineActSheet, { Types } from '../../../../utils/UII/actSheet';

export const ToolKey = "API";

export const CreateOpenActionSheet = (areaIndicator) => defineActSheet({
  "action" : "open",
  "type": Types.ToolInterfacePipeline, // ToolInterfacePipeline
  "options" : {
    "toolKey" : ToolKey,
    "areaIndicator" : areaIndicator,
  },
});


export const Info = {
  type : 'button',
  key : 'API',
  title : 'app.tool.api',
  titleType : 'code',
  iconType: 'material',
  icon: 'ActionFingerprint',
  // iconColor : '#fdfdfd',
  options: {
    toolKey : ToolKey,
  },

  actionSheet : CreateOpenActionSheet('[chaining]'),
}



export const UIIStatement = defineUIIStatement(Info);


export const defaultStaticData = {

};
