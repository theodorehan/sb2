import namespaceBindGenerator from '../../../../utils/redux/namespaceBindGenerator';
let defineType = namespaceBindGenerator('API');

export const SET_CATEGORIES = defineType("SET_CATEGORIES");
export const SET_CATEGORY_CHILDREN = defineType("SET_CATEGORY_CHILDREN");


export const SET_NODES = defineType("SET_NODES");

export const CALL_NODE_EVENT = defineType("CALL_NODE_EVENT");

export const RESPOND_NODE_EVENT = defineType("RESPOND_NODE_EVENT");

export const RESPOND_ERROR_NODE_EVENT = defineType('RESPOND_ERROR_NODE_EVENT');

export const START_ROOT_LOADING = defineType("START_ROOT_LOADING");

export const FINISH_ROOT_LOADING = defineType('FINISH_ROOT_LOADING');


export const FILTER_STRINGS = defineType('FILTER_STRINGS');

export const FILTER_SEARCH = defineType('FILTER_SEARCH');
