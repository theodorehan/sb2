import * as actionTypes from './actionTypes';


export const initialState = {
  apiTarget : 'node',
  categories : {},
  categoryLoading : false,
  nodes : [],
  dataFrames : {},

  filterStrings : [],
  filterSearch : '',
};

export default function APIReducer(state = initialState, action) {

  switch (action.type) {
    case actionTypes.SET_CATEGORIES:
      return Object.assign({}, state, {
        categories: action.categories,
      });
    case actionTypes.START_ROOT_LOADING:
      return Object.assign({}, state, {
        categoryLoading : true,
      });
    case actionTypes.FINISH_ROOT_LOADING:
      return Object.assign({}, state, {
        categoryLoading : false,
      });
    case actionTypes.SET_CATEGORY_CHILDREN:
      return Object.assign({}, state, {
        categories : Object.assign({}, state.categories, {
          [action.categoryId] : Object.assign({}, state.categories[action.categoryId] || {}, {
            children : action.children,
          }),
        }),
      });
    case actionTypes.SET_NODES :
      return Object.assign({}, state, {
        nodes : action.nodes,
      });

    case actionTypes.CALL_NODE_EVENT:
      return Object.assign({}, state, {
        dataFrames : Object.assign({}, state.dataFrames, {
          [`node/${action.nodeId}/${action.eventId}`] : Object.assign({}, state.dataFrames[`node/${action.nodeId}/${action.eventId}`], {
            called : true,
            loaded : false,
            data : {},
          }),
        }),
      })

    case actionTypes.RESPOND_NODE_EVENT:
      return Object.assign({}, state, {
        dataFrames : Object.assign({}, state.dataFrames, {
          [`node/${action.nodeId}/${action.eventId}`] : Object.assign({}, state.dataFrames[`node/${action.nodeId}/${action.eventId}`], {
            called : true,
            loaded : true,
            data : action.data,
          }),
        }),
      })
    case actionTypes.RESPOND_ERROR_NODE_EVENT:
      return Object.assign({}, state, {
        dataFrames : Object.assign({}, state.dataFrames, {
          [`node/${action.nodeId}/${action.eventId}`] : Object.assign({}, state.dataFrames[`node/${action.nodeId}/${action.eventId}`], {
            called : true,
            loaded : true,
            error : action.error,
          }),
        }),
      })
    case actionTypes.FILTER_SEARCH:
      return Object.assign({}, state, {
        filterSearch: action.search,
      });
    case actionTypes.FILTER_STRINGS:
      return Object.assign({}, state, {
        filterStrings: action.strings,
      })
    default :
      return state;
  }
}
