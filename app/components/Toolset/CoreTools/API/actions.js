import * as actionTypes from './actionTypes';

export function SET_CATEGORIES(categories) {
  return {
    type : actionTypes.SET_CATEGORIES,
    categories,
  }
}

export function START_ROOT_LOADING() {
  return {
    type : actionTypes.START_ROOT_LOADING,
  }
}

export function FINISH_ROOT_LOADING() {
  return {
    type : actionTypes.FINISH_ROOT_LOADING,
  }
}

export function SET_CATEGORY_CHILDREN(key, children) {
  return {
    type : actionTypes.SET_CATEGORY_CHILDREN,
    categoryId : key,
    children,
  }
}

export function SET_NODES(nodes) {
  return {
    type : actionTypes.SET_NODES,
    nodes,
  }
}

export function CALL_NODE_EVENT(nodeId, eventId) {
  return {
    type : actionTypes.CALL_NODE_EVENT,
    nodeId,
    eventId,
  }
}

export function RESPOND_NODE_EVENT(nodeId, eventId, data) {
  return {
    type : actionTypes.RESPOND_NODE_EVENT,
    nodeId,
    eventId,
    data,
  }
}

export function RESPOND_ERROR_NODE_EVENT(nodeId, eventId, error) {
  return {
    type : actionTypes.RESPOND_ERROR_NODE_EVENT,
    nodeId,
    eventId,
    error,
  }
}

export function FILTER_STRINGS(strings) {
  return {
    type : actionTypes.FILTER_STRINGS,
    strings,
  }
}


export function FILTER_SEARCH(search) {
  return {
    type : actionTypes.FILTER_SEARCH,
    search,
  }
}
