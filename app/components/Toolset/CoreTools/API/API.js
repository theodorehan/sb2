import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import classnames from 'classnames';
import _ from 'lodash';


import Toggle from 'material-ui/Toggle';
import { Radio, InputNumber, Select, Input,Collapse, Card, AutoComplete  } from 'antd';
const RadioGroup = Radio.Group;
const Option = Select.Option;
const { TextArea, Search } = Input;
const Panel = Collapse.Panel;

import Tool from '../common/Tool';
import ToolHeader from '../common/ToolHeader';
import ToolFooter from '../common/ToolFooter';
import ToolBody from '../common/ToolBody';


import './style.scss';


import * as actions from './actions';

import {
  Info,
} from './Constants';

import {
  CreateOpenActionSheet as CreateICE2DataOpenActionSheet,
  ToolKey as ICE2DataToolKey,
} from '../ICE2Data/Constants';


import {
  CreateOpenActionSheet as CreateSettingsOpenActionSheet,
} from '../Settings';


import {
  CLOSE_LAYOUT_WITH_ELASTIC,
} from '../../UIISystemActionSheets';

import AddIcon from 'material-ui/svg-icons/content/add';
import MinusIcon from 'material-ui/svg-icons/content/remove';
import ArrowDropDownIcon from 'material-ui/svg-icons/navigation/arrow-drop-down';
import ArrowDropUpIcon from 'material-ui/svg-icons/navigation/arrow-drop-up';

import FullmoonBtn from '../../../Unit/FullmoonButton';

@connect((state) => ({
  User : state.get('User'),
  Site : state.get('Site'),
}))
class API extends Component {
  static contextTypes = {
    UIISystems : PropTypes.object,
    elastic: PropTypes.object,
    zone : PropTypes.object,
    events : PropTypes.object,
    functionStore : PropTypes.object,
    theme : PropTypes.object,
    oldCurationParams : PropTypes.object,
    global : PropTypes.object,
    ice2 : PropTypes.func,
    nativeDrag : PropTypes.object,
  }


  constructor(props) {
    super(props)


  }


  onChangeToggle(keys){

    if( this.props.apiTarget === 'api') {
      for(let i = 0; i < keys.length; i++ ){
        let category = this.props.categories[keys[i]];

        if( !category.children ){
          this.context.ice2.get('/node/apiConfig', {
            params : {category_matching : category.id},
          })
            .then((res)=>{
              let {data} = res;
              this.context.elastic.dispatch(actions.SET_CATEGORY_CHILDREN(category.id, data.items));
            });
        }
      }
    } else if(this.props.apiTarget === 'node' ) {

    }

    console.log(arguments);
  }

  loadingAPICategory(){

    this.context.elastic.dispatch(actions.START_ROOT_LOADING())
    this.context.ice2.get('/node/apiCategory')
      .then((res)=>{
        let {data} = res;
        let categories = {};

        let category
        for(let i = 0; i < data.items.length; i++ ){
          category = data.items[i];

          categories[category.id] = category;
        }

        this.context.elastic.dispatch(actions.SET_CATEGORIES(categories))

        this.context.elastic.dispatch(actions.FINISH_ROOT_LOADING())
      });
  }


  loadingNodeTypes(){
    this.context.elastic.dispatch(actions.START_ROOT_LOADING());
    this.context.ice2.get('/node/nodeType/list.json', {
      params : {
        count : 1000,
        repositoryType_matching:'node,data',
        includeReferenced:true,
      },
    })
      .then((res) => {
        let {data} = res;

        this.context.elastic.dispatch(actions.SET_NODES(data.items))

        this.context.elastic.dispatch(actions.FINISH_ROOT_LOADING())
      })
  }

  callEvent(nodeType, event){

    this.context.elastic.dispatch(actions.CALL_NODE_EVENT(nodeType.tid, event.event));
    this.context.ice2.get(`/node/${nodeType.tid}/${event.event}.json`, {
      params : {
        count : 1,
      },
    })
      .then((res) => {
        let {data} = res;

        this.context.elastic.dispatch(actions.RESPOND_NODE_EVENT(nodeType.tid, event.event, data));
      }, (err) => {
        this.context.elastic.dispatch(actions.RESPOND_ERROR_NODE_EVENT(nodeType.tid, event.event, err.response || {status:500} ));
      })
  }

  openEventViewer(nodeType, event){
    this.context.UIISystems.openTool('ICE2Data', '[layer]',{
      width:1024,
      height:564,
      UNIQUE_LAYER : true,
      // DISABLE_DUPLICATE_UNDEPLOY : true,
    }, {
      applyFunctionKey : 'apply',
      nodeType,
      type : 'list',
    })
      .then(( closer )=>{
        this.context.functionStore.register('apply', (value)=>{
          closer();
          // this.setNodePropByPath(selectedNode, path, value, defineObject);
        })
      })
  }

  onStartDragFrame(e, nodeType, eventType, key, object){
    if( this.props.apiTarget === 'node' ){
      this.context.nativeDrag.start('propertyType', {
        nodeType,
        eventType,
        key,
      });


    }

    this.context.UIISystems.emitActionSheet(CreateSettingsOpenActionSheet("rightArea"), {
    }, {DISABLE_DUPLICATE_UNDEPLOY:true});

    this.context.events.emit('on-show-rightArea');
  }

  onStartDragNode(e, nodeType){
    this.context.nativeDrag.start('nodeType', {
      nodeType,
    });

    this.context.UIISystems.emitActionSheet(CreateSettingsOpenActionSheet("rightArea"), {
    }, {DISABLE_DUPLICATE_UNDEPLOY:true});

    this.context.events.emit('on-show-rightArea');
  }

  onStartDragNodeEvent(e, nodeType, eventType){
    this.context.nativeDrag.start('nodeEvent', {
      nodeType,
      eventType,
    });

    this.context.UIISystems.emitActionSheet(CreateSettingsOpenActionSheet("rightArea"), {
    }, {DISABLE_DUPLICATE_UNDEPLOY:true});

    this.context.events.emit('on-show-rightArea');
  }

  onEndNativeDrag(e){
    this.context.nativeDrag.finish();
  }

  filteringNode(nodeType){
    if( this.props.filterSearch || this.props.filterStrings.length > 0 ){
      let name = nodeType.typeName;
      let tid = nodeType.tid;

      if( this.props.filterStrings.find((str) => str ==name)){
        return true;
      }

      if( name.indexOf(this.props.filterSearch) > -1 || tid.indexOf(this.props.filterSearch) > -1){
        return true;
      }

      return false;
    }

    return true;
  }

  componentDidMount(){
    if( this.props.apiTarget === 'node' ){
      this.loadingNodeTypes();
    } else if( this.props.apiTarget === 'api' ){
      this.loadingAPICategory();
    }
  }



  renderDataFrameUnit(nodeType, event, _key, _object, _parentPath, _depth){

    let type = typeof _object;
    let indentArray = _.range(_depth);
    switch(type){
      case "boolean":
      case "string":
      case "number":
        return <li className={type} key={_parentPath+"/"+_key}>
          <div
            className='item'
            draggable={true}
            onDragEnd={(e) => this.onEndNativeDrag()}
            onDragStart={(e)=>this.onStartDragFrame(e, nodeType,event,(_parentPath+"."+_key), _object)}>
            { indentArray.map((_, i) => {
              if( i == _depth-1 ){
                return <span className='indent guide' key={i}/>;
              }
              return <span className='indent' key={i}/>;
            })}
            <span className='key'>{_key}</span>
            <span className='value'>
              <span className='wrapper' title={_object}>
                {type === 'boolean' ? JSON.stringify(_object):_object}
              </span>
            </span>
          </div>
        </li>;
      case "object":
        let typeDetail = Array.isArray(_object) ? "array":"object";
        return (
          <li className={typeDetail} key={_parentPath+"/"+_key}>
            <div
              className='item'
              draggable={true}
              onDragEnd={(e) => this.onEndNativeDrag()}
              onDragStart={(e)=>this.onStartDragFrame(e, nodeType,event,(_parentPath+"."+_key), _object)}>
              { indentArray.map((_, i) => {
                if( i == _depth-1 ){
                  return <span className='indent guide' key={i}/>;
                }
                return <span className='indent' key={i}/>;
              })}
              <span className='key'>{_key}</span>
              <span className='value'> </span>
            </div>
            <ul>
              {Object.keys(_object  || {}).map((__key) => this.renderDataFrameUnit(nodeType, event, __key, _object[__key], _parentPath+'.'+_key, _depth+1))}
            </ul>
          </li>
        )
    }
  }




  renderAPIDetail(api, categoryId){

    const openAPIViewer = () => {
      console.log('open' , api.apiName);
    }


    return <div className="api-detail">
      <FullmoonBtn
        shape="round"
        normalStyle={{
          display: 'inline-block',
          height: 30,
          borderWidth: 0,

          borderRadius: 100,
          fontSize: 12,
          color: this.context.theme.Tool.layout.footer.fullmoonBtn.textColor.default,
          fontFamily: 'Nanum Square',
          backgroundColor: this.context.theme.Tool.layout.footer.fullmoonBtn.bgColor.default,
          margin:10,
          padding:'0 20px',
          textAlign:'left',
        }}
        hoverStyle={{
          borderWidth: 0,
          backgroundColor:this.context.theme.Tool.layout.footer.fullmoonBtn.bgColor.hovered,
        }}
        moonBlender='overlay'
        onClick={openAPIViewer}>
        API View
      </FullmoonBtn>
    </div>
  }

  renderCategory(item ,i){

    return <Panel
      key={item.id}
      header={item.categoryName}
    >
      <Collapse>
        {
          item.children && item.children.map((api) => <Panel header={api.apiName} key={api.apiId}>
            { this.renderAPIDetail(api, item.id)}
          </Panel>)
        }

      </Collapse>



    </Panel>
  }

  renderNodeDataFrame(nodeType, event, dataFrame){

    if( dataFrame.error ){
      return <div style={{color:'rgb(181, 33, 33)'}}>Error : {dataFrame.error.status}</div>
    }

    if( dataFrame.data.items ){
      if( dataFrame.data.items[0] ){
        return this.renderDataFrameUnit(nodeType, event, '', dataFrame.data.items[0], '', 0);
      }
    } else if(dataFrame.data.item) {

      return this.renderDataFrameUnit(nodeType, event, '', dataFrame.data.item, '', 0);
    }

    return <div style={{color:'rgb(181, 33, 33)'}}>No data.</div>
  }

  renderNodeEvent(nodeType, event){

    let dataFrame = this.props.dataFrames[`node/${nodeType.tid}/${event.event}`] || {};


    return <Panel
      header={
        <span
          className="in-panel-drag-item"
          draggable={true}
          onDragEnd={(e) => this.onEndNativeDrag()}
          onDragStart={(e) => this.onStartDragNodeEvent(e, nodeType, event)}>{event.eventName}</span>
      }
      key={event.event}>
      <FullmoonBtn
        shape="round"
        disabled={dataFrame.called && !dataFrame.loaded}
        normalStyle={{
          display: 'inline-block',
          height: 30,
          borderWidth: 0,

          borderRadius: 100,
          fontSize: 12,
          color: this.context.theme.Tool.layout.footer.fullmoonBtn.textColor.default,
          fontFamily: 'Nanum Square',
          backgroundColor: this.context.theme.Tool.layout.footer.fullmoonBtn.bgColor.default,
          margin:10,
          padding:'0 20px',
          textAlign:'left',
        }}
        hoverStyle={{
          borderWidth: 0,
          backgroundColor:this.context.theme.Tool.layout.footer.fullmoonBtn.bgColor.hovered,
        }}
        moonBlender='overlay'
        onClick={()=>this.callEvent(nodeType,event)}>
        {
          dataFrame.called && !dataFrame.loaded ? 'Loading...' : "Request"
        }
      </FullmoonBtn>

      <FullmoonBtn
        shape="round"
        disabled={dataFrame.called && !dataFrame.loaded}
        normalStyle={{
          display: 'inline-block',
          height: 30,
          borderWidth: 0,

          borderRadius: 100,
          fontSize: 12,
          color: this.context.theme.Tool.layout.footer.fullmoonBtn.textColor.default,
          fontFamily: 'Nanum Square',
          backgroundColor: this.context.theme.Tool.layout.footer.fullmoonBtn.bgColor.default,
          margin:10,
          padding:'0 20px',
          textAlign:'left',
        }}
        hoverStyle={{
          borderWidth: 0,
          backgroundColor:this.context.theme.Tool.layout.footer.fullmoonBtn.bgColor.hovered,
        }}
        moonBlender='overlay'
        onClick={()=>this.openEventViewer(nodeType,event)}>
        Viewer
      </FullmoonBtn>

      <div className="data-frame">
        { dataFrame.loaded && this.renderNodeDataFrame(nodeType, event, dataFrame) }
      </div>

    </Panel>
  }

  renderNode(nodeType){
    const openNodeViewer = () => {

      let transferData = {
        applyFunctionKey : 'apply',
        nodeType : nodeType,
      };

      let layoutId;
      this.context.UIISystems.emitActionSheet(CreateICE2DataOpenActionSheet("[modal]"), transferData, {
        width : 1024,
        height: 768,
        onLayoutID(id){
          layoutId = id;
        },
      });

      this.context.functionStore.register('apply', (madeParams)=>{

        this.context.UIISystems.emitActionSheet(CLOSE_LAYOUT_WITH_ELASTIC(layoutId, ICE2DataToolKey, true));

      })
    }


    let events = [
      {
        tid: {
          value: nodeType.tid,
          label: nodeType.typeName,
          refId: nodeType.tid,
        },
        event: "read",
        eventName: "Read",
        noneExecute: null,
        eventActions: [ ],
        eventListeners: [ ],
      },
      {
        tid: {
          value: nodeType.tid,
          label: nodeType.typeName,
          refId: nodeType.tid,
        },
        event: "list",
        eventName: "List",
        noneExecute: null,
        eventActions: [ ],
        eventListeners: [ ],
      },
      ...nodeType.events,
    ]


    return <Panel
      key={nodeType.tid}
      header={
        <span
          className="in-panel-drag-item"
          draggable={true}
          onDragEnd={(e) => this.onEndNativeDrag()}
          onDragStart={(e) => this.onStartDragNode(e, nodeType)}>
            {nodeType.typeName}
            <span className="tid">{nodeType.tid}</span>
        </span>}>

      <Collapse>
        {
          events.map((event) => this.renderNodeEvent(nodeType, event))
        }

      </Collapse>
    </Panel>
  }

  render() {

    return (
      <Tool className="api">
        <ToolHeader
          title="A.P.I"
          iconType={ Info.iconType }
          icon={ Info.icon }
          iconColor={ Info.iconColor } >

        </ToolHeader>
        <ToolBody isLoading={this.props.categoryLoading} nofooter>
          <div className="search-and-filter">
            {/*<AutoComplete*/}
              {/*className="certain-category-search"*/}
              {/*dropdownClassName="certain-category-search-dropdown"*/}
              {/*dropdownMatchSelectWidth={false}*/}
              {/*dropdownStyle={{ width: 300 }}*/}
              {/*size="large"*/}
              {/*style={{ width: '100%' }}*/}
              {/*dataSource={options}*/}
              {/*placeholder="input here"*/}
              {/*optionLabelProp="value"*/}
            {/*>*/}
              {/*<Input suffix={<Icon type="search" className="certain-category-icon" />} />*/}
            {/*</AutoComplete>*/}

            <Search
              autoComplete="on"
              name="api-search-input"
              placeholder="Filter"
              style={{ width: 200, height:30 }}
              onSearch={(search) => this.context.elastic.dispatch(actions.FILTER_SEARCH(search))}
            />

            {/*<Select*/}
              {/*mode="single"*/}
              {/*size='large'*/}
              {/*placeholder="Filter"*/}
              {/*notFoundContent="Not found"*/}
              {/*defaultValue={[]}*/}
              {/*onChange={(strings)=>this.context.elastic.dispatch(actions.FILTER_STRINGS([strings]))}*/}
              {/*onSearch={(search) => this.context.elastic.dispatch(actions.FILTER_SEARCH(search))}*/}
              {/*style={{ width: '100%' }}*/}
            {/*>*/}
              {/*{*/}
                {/*(() => {*/}
                  {/*switch(this.props.apiTarget ){*/}
                    {/*case 'api' :*/}
                      {/*let categoryKeys = Object.keys(this.props.categories || {});*/}
                    {/*// return categoryKeys.map((key)=><Option key={this.props.categories[key].categoryName}>{i.toString(36) + i}</Option>);*/}
                    {/*case 'node' :*/}
                      {/*return this.props.nodes.map((node,i) => <Option key={i}>{node.typeName}</Option> );*/}
                  {/*}*/}
                {/*})()*/}
              {/*}*/}
            {/*</Select>*/}
          </div>

          <div className="api-panel">

            <Collapse defaultActiveKey={[]} onChange={:: this.onChangeToggle}>
              {
                (() => {
                  switch(this.props.apiTarget ){
                    case 'api' :
                      let categoryKeys = Object.keys(this.props.categories || {});
                      return categoryKeys.map((categoryKey,i)=>this.renderCategory(this.props.categories[categoryKey], i));
                    case 'node' :
                      return this.props.nodes.filter(::this.filteringNode).map((node) => this.renderNode(node));
                  }
                })()
              }
            </Collapse>
          </div>

        </ToolBody>

      </Tool>
    )
  }
}

export default API;
