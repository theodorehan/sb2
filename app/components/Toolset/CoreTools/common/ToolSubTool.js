import React, { Component } from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import ToolProvider from '../../../../Core/Components/ToolProvider';



@connect((state) => ({
  Elastic : state.get('Elastic'),
  Deployment : state.get('Deployment'),
}))
export default class ToolSubTool extends Component {
  static contextTypes = {
    layout : PropTypes.object,
  }

  constructor(props){
    super(props);
  }

  render() {

    return <div className={classnames('sub-tool', this.props.className)}>
      <ToolProvider
        Elastic={this.props.Elastic}
        elasticID={this.props.elasticID}/>
    </div>
  }
}
