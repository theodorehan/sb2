import React, { Component } from 'react';
import PropTypes from 'prop-types';

import classnames from 'classnames';


let BalancedItemElementWrapper = (props)=> <div className="balanced-item" style={{width:props.widthPercent + '%'}}>
  { props.element }
</div>;


export default class ToolFooter extends Component {
  static contextTypes = {
    layout : PropTypes.object,
    theme: PropTypes.object,
  }

  static propTypes = {
    type : PropTypes.string,

  }

  constructor(props){
    super(props);
  }

  renderChildren(){
    if( this.props.type === 'balanced-item-tray' ){

      let children;

      if( Array.isArray(this.props.children) ){
        children = this.props.children;
      } else {
        children = [this.props.children];
      }

      let step = 100 / children.length;

      return <div className="balanced-item-tray">
        {children.map((element, i)=> <BalancedItemElementWrapper element={element} widthPercent={step} key={i}/>)}
      </div>
    }

    return this.props.children;
  }


  render(){


    return <div className={ classnames("footer", this.props.className || '' ) }>
      { this.props.children && this.renderChildren() }
    </div>
  }
}

export const height = 50;
