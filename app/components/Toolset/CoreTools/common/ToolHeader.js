import React, { Component } from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import * as MaterialIcons from 'material-ui/svg-icons';
import CloseIcon from 'material-ui/svg-icons/navigation/close';
import LayersIcon from 'material-ui/svg-icons/maps/layers';
import LayersClearIcon from 'material-ui/svg-icons/maps/layers-clear';
import HelpIcon from 'material-ui/svg-icons/action/help-outline';
import FullscreenIcon from 'material-ui/svg-icons/navigation/fullscreen';
import FullscreenExitIcon from 'material-ui/svg-icons/navigation/fullscreen-exit';
import Draggable from '../../../../Core/Components/Draggable';



@connect((state) => ({
  Layout : state.get('Layout'),
}))
export default class ToolHeader extends Component {
  static contextTypes = {
    layout : PropTypes.object,
    theme: PropTypes.object,
  }

  static propTypes = {
    onCloseHook : PropTypes.func,
  }

  constructor(props){
    super(props);
  }

  onFocus(){
    if( this.context.layout ){
      if( typeof this.context.layout.onFocus == 'function' ){
        this.context.layout.onFocus();
      }
    }
  }

  onClose(){
    if( this.context.layout ){
      if( typeof this.context.layout.onClose == 'function' ){

        // onCloseHook 이 프로퍼티로 입력되면 onCloseHook 을 호출하여 주고 true 를 반환하면 close 하도록 한다.
        if( this.props.onCloseHook ){

          if( this.props.onCloseHook() ){
            this.context.layout.onClose();
          }
        } else {

          this.context.layout.onClose();
        }

      }
    }
  }

  onFullscreen(){
    this.context.layout.onFullscreen();
  }

  onFullscreenExit(){
    this.context.layout.onFullscreenExit();
  }

  onHelp(){
    console.log('help');
  }

  onDragMove(x,y, deltaX, deltaY){
    if( this.context.layout ){
      if( typeof this.context.layout.onDragMove == 'function' ){
        this.context.layout.onDragMove(x,y, deltaX, deltaY);
      }
    }
  }

  renderIcon(){
    let icon;

    if( !this.props.icon ){
      return undefined;
    }

    if( this.props.iconType === 'fa' ){
      icon = <i className={classnames('fa', 'fa-'+this.props.icon)} style={{ color : this.props.iconColor }}/>;
    } else if( this.props.iconType === 'img' ){
      icon = <img src={this.props.icon}/>;
    } else if( this.props.iconType === 'material' ){
      icon = React.createElement(MaterialIcons[this.props.icon], {
        style : {color:this.props.iconColor, width:'1em', height:'1em'},
      });
    } else {
      icon = <span>
        {this.props.iconType}
        {this.props.icon}
      </span>
    }

    return <span className="icon-wrapper">
      { icon }
    </span>
  }

  render(){
    let float, fullscreen;
    if( this.context.layout ){
      float = this.props.Layout[this.context.layout.props.isa].float;
      fullscreen = this.props.Layout[this.context.layout.props.isa].fullscreen;
    }

    const iconStyle = {
      width: '1em',
      height : '1em',
      color : this.context.theme.Tool.layout.header.actionBtn.color.default,
    }

    return <Draggable
      _draggableData={{}}
      _draggableType='none'
      _additionalStyle={{width:'100%'}}
      onDragMove={::this.onDragMove}
      invisibleGhost={true}
      component={<div className={ classnames("header", this.props.className || '', fullscreen && 'fullscreen' ) } onClick={()=> this.onFocus() }>
        <div> {this.renderIcon()} {this.props.title} </div>
        <div className="actions">

          <div className="inject-actions">
            { this.props.children }
          </div>

          <button onClick={()=> this.onHelp() }>
            <HelpIcon style={iconStyle}/>
          </button>

          { this.context.layout.onFullscreen && ( !fullscreen ? <button onClick={() => this.onFullscreen()}>
            <FullscreenIcon style={iconStyle}/>
          </button> : <button onClick={() => this.onFullscreenExit()}>
            <FullscreenExitIcon style={iconStyle}/>
          </button> ) }

          {
            typeof this.context.layout.onRequestPin === 'function' && <button onClick={() => float ? this.context.layout.onRequestPin() : this.context.layout.onRequestReleasePin() }>
              { float ?
                <LayersIcon style={iconStyle}/> :
                <LayersClearIcon style={iconStyle}/>
              }
            </button>
          }

          <button onClick={()=> this.onClose() }>
            <CloseIcon style={iconStyle}/>
          </button>
        </div>
      </div>} />
  }
}


export const height = 30;
