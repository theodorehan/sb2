import React, { Component } from 'react';
import classnames from 'classnames';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';

import './ToolLayout.scss';


@connect((state) => ({
  Layout : state.get('Layout'),
}))
export default class Tool extends Component {
  static propTypes = {
    sizingContentWrap :PropTypes.bool,
  }

  static contextTypes = {
    layout : PropTypes.object,
  }

  constructor(props){
    super(props);
  }

  render(){

    let float, fullscreen;
    if( this.context.layout ){
      fullscreen = this.props.Layout[this.context.layout.props.isa].fullscreen;
    }


    return <div className={ classnames("tool", this.props.sizingContentWrap && 'wrap-kernel-style', this.props.className || '', fullscreen && 'fullscreen' ) }>
      { this.props.children }
    </div>
  }
}
