import React, {Component} from 'react';
import PropTypes from 'prop-types';

import classnames from 'classnames';
import { Scrollbars as ScrollWrapper} from 'react-custom-scrollbars';

import LoadableDIV from '../../../LoadableDiv';


export default class ToolBody extends Component {
  static propTypes = {
    disableScroll : PropTypes.bool,
    isLoading: PropTypes.bool,
    nofooter : PropTypes.bool,
    onReachScrollBottom : PropTypes.func,
    onScroll : PropTypes.func,
    blockObserveScroll: PropTypes.bool,
    useNativeScroll : PropTypes.bool,
  }

  static contextTypes = {
    layout : PropTypes.object,
  }


  constructor(props) {
    super(props);
  }

  onFocus(){
    if( this.context.layout ){
      if( typeof this.context.layout.onFocus == 'function' ){
        this.context.layout.onFocus();
      }
    }
  }

  onScroll(obj){
    if( this.props.blockObserveScroll ) return;

    let scrollEnd = obj.realHeight - obj.containerHeight;

    if( scrollEnd == obj.topPosition ){
      this.props.onReachScrollBottom && this.props.onReachScrollBottom();
    }

    this.props.onScroll && this.props.onScroll(obj);
  }


  render() {
    return <LoadableDIV

      onClick={:: this.onFocus }
      className={ classnames("body", this.props.className || '', this.props.nofooter && 'no-footer', this.props.useNativeScroll && 'overflow-auto') }
      isLoading={this.props.isLoading}>
        {
          this.props.useNativeScroll || this.props.disableScroll ?
            this.props.children :
            <ScrollWrapper
              style={{width:'100%', height:'100%', position:'relative'}}
              onScroll={:: this.onScroll }>
              { this.props.children }
            </ScrollWrapper>
        }
    </LoadableDIV>
  }
}



export const ScrollableBox = (props) => {
  const onScroll = (obj) => {
    if( props.blockObserveScroll ) return;

    let scrollEnd = obj.realHeight - obj.containerHeight;

    if( scrollEnd == obj.topPosition ){
      props.onReachScrollBottom && props.onReachScrollBottom();
    }

    props.onScroll && props.onScroll(obj);
  }

  return <ScrollWrapper
    style={{width:props.width || '100%', height:props.height || '100%'}}
    onScroll={onScroll }>
    { props.children }
  </ScrollWrapper>
}


ScrollableBox.propTypes = {
  onReachScrollBottom : PropTypes.func,
  onScroll : PropTypes.func,
  blockObserveScroll: PropTypes.bool,
}
