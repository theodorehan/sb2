import React, { Component } from 'react';
import classnames from 'classnames';

import FullmoonButton from '../../../Unit/FullmoonButton';



export const FooterButton = (props) => <FullmoonButton

  normalStyle={{
    display: 'inline-block',
    width: '100%',
    height: '100%',
    border:0,
    borderRadius: 0,
    fontSize: 12,
    color: props.active ? 'rgb(29, 183, 255)':'#fff',
    fontFamily: 'Nanum Square',
  }}
  hoverStyle={{
    border:0,
    backgroundColor:'#021219',
  }}
  moonBlender='overlay'
  onClick={props.onClick}>

  {props.title}
</FullmoonButton>
