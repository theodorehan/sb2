import * as actionTypes from './actionTypes';


export const initialState = {
  spreadList : [],
  spreadAll : false,
};

export default function NodeTreeInspector(state = initialState, action) {
  let newSpreadNodeList = [];

  switch (action.type) {
    case actionTypes.SPREAD_NODE :
      if( state.spreadList.indexOf(action.id) > -1 ){

        newSpreadNodeList = state.spreadList.filter((id)=> id !== action.id );
      } else {
        newSpreadNodeList = [...state.spreadList, action.id];
      }

      return Object.assign({}, state, {
        spreadList : newSpreadNodeList,
      });
    default :
      return state;
  }
}
