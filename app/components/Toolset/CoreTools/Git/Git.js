import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import classnames from 'classnames';


import Tool from '../common/Tool';
import ToolHeader from '../common/ToolHeader';
import ToolFooter from '../common/ToolFooter';
import ToolBody from '../common/ToolBody';


import './style.scss';


import {
Info,
} from './Constants';


@connect((state) => ({
}))
class Git extends Component {
  static contextTypes = {
  }

  static propTypes = {
  }


  constructor(props) {
    super(props)


  }


  render() {

    return (
      <Tool className="node-tree-inspector">
        <ToolHeader
          title="Git Management"
          iconType={ Info.iconType }
          icon={ Info.icon }
          iconColor={ Info.iconColor }/>
        <ToolBody ref={(toolBody) => { this.toolBodyComp = toolBody } }>


        </ToolBody>
        <ToolFooter/>
      </Tool>
    )
  }
}

export default Git;
