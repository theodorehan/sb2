import defineUIIStatement from '../../../../utils/UII/defineUIIStatement';
import defineActSheet, { Types } from '../../../../utils/UII/actSheet';

export const ToolKey = "Git";

export const CreateOpenActionSheet = (areaIndicator) => defineActSheet({
  "action" : "open",
  "type": Types.ToolInterfacePipeline, // ToolInterfacePipeline
  "options" : {
    "toolKey" : ToolKey,
    "areaIndicator" : areaIndicator,
  },
});


export const Info = {
  type : 'button',
  key : 'Git',
  title : 'app.tool.git',
  titleType : 'code',
  iconType: 'fa',
  icon: 'git',
  // iconColor : '#fdfdfd',
  options: {
    toolKey : ToolKey,
  },

  actionSheet : CreateOpenActionSheet('[layer]'),
}



export const UIIStatement = defineUIIStatement(Info);
