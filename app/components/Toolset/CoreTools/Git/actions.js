import * as actionTypes from './actionTypes';


export function SPREAD_NODE(id) {
  return {
    type : actionTypes.SPREAD_NODE,
    id,
  };
}
