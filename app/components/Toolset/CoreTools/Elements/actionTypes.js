import namespaceBindGenerator from '../../../../utils/redux/namespaceBindGenerator';
let defineType = namespaceBindGenerator('ElementTool');


export const CHANGE_SEARCH_VALUE = defineType('CHANGE_SEARCH_VALUE');
export const CHANGE_COMPLEX_MODE = defineType('CHANGE_COMPLEX_MODE');
export const TOGGLE_GROUP_SPREAD_STATE = defineType('TOGGLE_GROUP_SPREAD_STATE');
