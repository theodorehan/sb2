import * as actionTypes from './actionTypes';


export const initialState = {
  searchValue: '',
  complexMode : true,
  spreadList : [],
};

export default function ElementReducer(state = initialState, action) {

  switch (action.type) {
    case actionTypes.CHANGE_SEARCH_VALUE :
      return Object.assign({}, state, {
        searchValue: action.value,
      });

    case actionTypes.CHANGE_COMPLEX_MODE :
      return Object.assign({}, state, {
        complexMode : action.checked,
      });

    case actionTypes.TOGGLE_GROUP_SPREAD_STATE :
      let newList = Object.assign([],state.spreadList);
      if( action.checked ){
        newList.push(action.groupName);
      } else {
        newList = newList.filter((_gn)=> action.groupName !== _gn );
      }

      return Object.assign({}, state, {
        spreadList : newList,
      });

    default :
      return state;
  }
}
