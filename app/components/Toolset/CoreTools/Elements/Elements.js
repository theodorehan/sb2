import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import TextField from 'material-ui/TextField';
import Toggle from 'material-ui/Toggle';

import Tool from '../common/Tool';
import ToolHeader from '../common/ToolHeader';
import ToolFooter from '../common/ToolFooter';
import ToolBody from '../common/ToolBody';

import './Element.scss';
import {
  ProvideSingleElementGroup,
  ProvideComplexElementGroup,
} from './Snippets';


import Draggable from '../../../../Core/Components/Draggable';

import {
  Info,
} from './Constants';

import * as actions from './actions';

const toggleStyles = {
  default: {
    width: 100,
    margin:3,
    float:'right',
  },
  thumbOff: {
    backgroundColor: 'rgb(94, 147, 185)',
  },
  trackOff: {
    backgroundColor: 'rgb(139, 182, 214)',
  },
  thumbSwitched: {
    backgroundColor: '#fff',
  },
  trackSwitched: {
    backgroundColor: 'rgb(5, 149, 255)',
  },
  labelStyle: {
    color: '#f9f9f9',
  },
};

class Element extends Component {
  static contextTypes = {
    toolSystem: PropTypes.object,
    elastic: PropTypes.object,
  }

  constructor(props) {
    super(props)
  }

  toggleFold(_groupName, _e){
    if( this.props.spreadList.indexOf(_groupName) > -1 ){
      this.context.elastic.dispatch(actions.TOGGLE_GROUP_SPREAD_STATE(_groupName, false));
    } else {
      this.context.elastic.dispatch(actions.TOGGLE_GROUP_SPREAD_STATE(_groupName, true));
    }
  }


  onSearchInput(_e, _newVal) {

    this.context.elastic.dispatch(actions.CHANGE_SEARCH_VALUE(_newVal));
  }

  onToggleElementType(_e, _isChecked){
    this.context.elastic.dispatch(actions.CHANGE_COMPLEX_MODE(_isChecked));
  }

  renderItem(_item, _i) {
    return <Draggable
      key={_i}
      component={ () =>
        <div className='item' key={_i}>
          <div>

          </div>
          <div dangerouslySetInnerHTML={{__html:_item.name}}></div>
        </div>
      }
      _draggableType='component'
      _draggableData={{}}/>;
  }

  renderGroup(_group, _i) {

    let showItems = false;
    let filtering = this.props.searchValue.length > 0;
    let filteredItems = _group.items;

    if( filtering ){
      filteredItems = filteredItems.filter((_item)=>{
        let startIndex = _item.name.indexOf(this.props.searchValue);

        return startIndex > -1;
      });

      filteredItems = filteredItems.map((_item)=>(
        {
          ..._item,
          name : _item.name.replace(this.props.searchValue,
            `<span class='search-matched'>${this.props.searchValue}</span>`),
        }
      ));

      if( filteredItems.length < 1 ) return;
    }


    if( filtering || this.props.spreadList.indexOf(_group.groupName) > -1 ){
      showItems = true;
    }


    return <div className='elements-group' key={_i}>
      <div
        className={classnames("group-head", filtering ? '':'toggle-able')}
        onClick={ filtering ? () => undefined : this.toggleFold.bind(this, _group.groupName ) } >

        { _group.groupName }

        { filtering ? undefined :
          <div className="toggle-button">
            <i className={ classnames("fa", "fa-angle-" + ( showItems ? 'up' : "down") )}/>
          </div>
        }
      </div>

      <div className="group-items">
        { showItems && filteredItems.map(this.renderItem.bind(this))}
      </div>
    </div>
  }

  renderGroups() {
    return <div className="elements-groups">
      { ( this.props.complexMode ? ProvideComplexElementGroup : ProvideSingleElementGroup ).map(this.renderGroup.bind(this))}
    </div>
  }

  render() {
    return (
      <Tool className="elements-palette">
        <ToolHeader title="Element Palette" iconType={ Info.iconType } icon={ Info.icon } iconColor={ Info.iconColor }/>

        <ToolBody>
          <div className="search-box">
            <TextField
              style={ {width: "100%"} }
              hintText="Element search"
              value={this.props.searchValue }
              onChange={:: this.onSearchInput }
            />
          </div>

          { this.renderGroups() }
        </ToolBody>

        <ToolFooter>
          <Toggle
            label="Complex"
            defaultToggled={this.props.complexMode}
            onToggle={:: this.onToggleElementType}
            style={toggleStyles.default}
            thumbStyle={toggleStyles.thumbOff}
            trackStyle={toggleStyles.trackOff}
            thumbSwitchedStyle={toggleStyles.thumbSwitched}
            trackSwitchedStyle={toggleStyles.trackSwitched}
            labelStyle={toggleStyles.labelStyle}
          />
        </ToolFooter>
      </Tool>
    )
  }
}

export default Element;
