import groupBy from 'lodash/groupBy';
import {HTMLTagCollection} from '../../../../../Core/Constants';

let groupByMap = groupBy(HTMLTagCollection, function(_htmlInfo){
  return _htmlInfo.group;
});


export const ProvideSingleElementGroup = [
  {
    'groupName': 'Basic',
    'items' : groupByMap['basic'],
  },

  {
    'groupName': 'Styles and Semantics',
    'items' : groupByMap['styles&semantics'],
  },

  {
    'groupName': 'Form and Input',
    'items' : groupByMap['form&input'],
  },

  {
    'groupName': 'Tables',
    'items' : groupByMap['tables'],
  },

  {
    'groupName': 'Images',
    'items' : groupByMap['images'],
  },

  {
    'groupName' : 'Audio Video',
    'items' :  groupByMap['audio/video'],
  },

  {
    'groupName': 'Links',
    'items' : groupByMap['links'],
  },

  {
    'groupName': 'Lists',
    'items' : groupByMap['lists'],
  },

  {
    'groupName': 'Meta Info',
    'items' : groupByMap['meta_info'],
  },

  {
    'groupName': 'Formatting',
    'items' : groupByMap['formatting'],
  },

  {
    'groupName': 'Frames',
    'items' : groupByMap['frames'],
  },

  {
    'groupName': 'Programming',
    'items' : groupByMap['programming'],
  },
];


export const ProvideComplexElementGroup = [

  {
    'groupName': 'Layout',
    'items' : groupByMap['basic'],
  },

  {
    'groupName': 'List',
    'items' : groupByMap['basic'],
  },

  {
    'groupName': 'Table',
    'items' : groupByMap['styles&semantics'],
  },
];
