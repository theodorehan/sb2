import defineUIIStatement from '../../../../utils/UII/defineUIIStatement';
import defineActSheet, { Types } from '../../../../utils/UII/actSheet';

export const ToolKey = "Elements";
export const Info = {
  type : 'button',
  key : 'Elements',
  title : 'Elements',
  iconType: 'img',
  icon: '/public/images/html5-logo.png',
  iconColor : 'rgb(122, 198, 255)',
  options: {
    toolKey : ToolKey,
  },

  actionSheet : defineActSheet({
    "action" : "open",
    "type": Types.ToolInterfacePipeline, // ToolInterfacePipeline
    "options" : {
      "toolKey" : ToolKey,
      "areaIndicator" : "[chaining]",
    },
  }),
}


export const UIIStatement = defineUIIStatement(Info);
