import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import classnames from 'classnames';
import axios from 'axios';


import Toggle from 'material-ui/Toggle';
import { Radio, InputNumber, Select, Input,Collapse, Card  } from 'antd';
const RadioGroup = Radio.Group;
const Option = Select.Option;
const { TextArea, Search } = Input;

const Panel = Collapse.Panel;

import Tool from '../common/Tool';
import ToolHeader from '../common/ToolHeader';
import ToolFooter from '../common/ToolFooter';
import ToolBody, {ScrollableBox} from '../common/ToolBody';

import Ice2Table from '../../../Ice2Table';

import './style.scss';


import * as actions from './actions';

import {
  Info,
} from './Constants';

import AddIcon from 'material-ui/svg-icons/content/add';
import MinusIcon from 'material-ui/svg-icons/content/remove';
import ArrowDropDownIcon from 'material-ui/svg-icons/navigation/arrow-drop-down';
import ArrowDropUpIcon from 'material-ui/svg-icons/navigation/arrow-drop-up';

import * as YoutubeAPI from '../../../../Core/Network/Youtube';

import FullmoonBtn from '../../../Unit/FullmoonButton';

@connect((state) => ({
  User : state.get('User'),
  Site : state.get('Site'),
}))
class YoutubeExplorer extends Component {
  static contextTypes = {
    UIISystems : PropTypes.object,
    elastic: PropTypes.object,
    zone : PropTypes.object,
    events : PropTypes.object,
    functionStore : PropTypes.object,
    theme : PropTypes.object,
    global : PropTypes.object,
    intl : PropTypes.object,
    glowUpdate : PropTypes.func,
  }


  constructor(props) {
    super(props)

    this.state = {
      searchResult : null,
      searchItems : [],
      loading: false,
      selectedItems : [],
    }
  }

  onClose(){
    if( this.props.staticData.closeHookFunctionKey ){
      let func = this.context.functionStore.get(this.props.staticData.closeHookFunctionKey);

      if( typeof func === 'function' ){
        return func(this);
      }
    }

    return true;
  }

  onSelectItem(item){
    if( this.props.staticData.selectable ){
      let {videoId} = item.id;



      if( this.state.selectedItems.find((id) => id == videoId)){

        this.setState({
          selectedItems : this.state.selectedItems.filter((id) => id != videoId),
        });
      } else {
        this.setState({
          selectedItems : this.props.staticData.multipleSelect ? [
            ...this.state.selectedItems,
            {
              id : videoId,
            },
          ] : [
            {
              id : videoId,
            },
          ],
        })
      }
    }
  }

  onSearch(value){
    this.setState({loading:true});
    YoutubeAPI.search(value,15)
      .then((res) => {
        let {data} = res;

        this.setState({
          searchValue : value,
          searchResult : data,
          loading: false,
          searchItems : data.items,
        })
      });
  }

  onApply(){
    if( this.props.staticData.applyFunctionKey ){
      let func = this.context.functionStore.get(this.props.staticData.applyFunctionKey);

      if( typeof func === 'function' ){
        func(this.state.selectedItems);
      }
    }
  }

  componentDidMount(){
    this.setState({
      selectedItems : [...this.props.staticData.defaultSelectedItems],
    })
  }

  componentDidUpdate() {
    this.context.glowUpdate && this.context.glowUpdate()
  }

  onSearchBoxScroll(){
    this.context.glowUpdate && this.context.glowUpdate();
  }

  onReachScrollBottom(){
    if( this.state.searchResult ){
      let {nextPageToken} = this.state.searchResult;
      this.setState({loading: true});

      YoutubeAPI.search(this.state.searchValue,15, nextPageToken)
        .then((res) => {
          let {data} = res;

          this.setState({
            searchResult : data,
            loading: false,
            searchItems : [...this.state.searchItems,...data.items],
          })
        });
    }
  }

  renderRecord(item, i){

    let {videoId} = item.id;

    let selected = this.state.selectedItems.find((selectedItem) => selectedItem.id == videoId);

    return <div className={classnames("record", selected && 'selected')} key={i} >
      <div className="preview" onClick={() => this.onSelectItem(item, i) }>
        <img src={item.snippet.thumbnails.medium.url} width='100%' height='100%'/>
        <button className="overlay"><AddIcon style={{color:'inherit', width:54}}/></button>
      </div>
      <div className="info">
        <div className="title" onClick={()=> window.open(`https://www.youtube.com/watch?v=${item.id.videoId}`, '_blank')}>
          {item.snippet.title}
          <span className="hidden-after">
            <i className="fa fa-youtube"/>
          </span>
        </div>
        <div className="desc">
          {item.snippet.description}
        </div>
      </div>
    </div>
  }

  renderSelections(){
    return <div className="selection-box">
      { this.state.selectedItems.map((item, i)=> <div className="item">
        <button className="trash" onClick={()=> this.setState({selectedItems : this.state.selectedItems.filter((selectedItem) => selectedItem.id !== item.id)}) }>
          <MinusIcon style={{color:'inherit', width:15}}/>
        </button>
        <iframe key={i} frameBorder='0' src={`https://www.youtube.com/embed/${item.id}?autoplay=0`}/>
      </div>)}
    </div>
  }

  renderResultRecords(withSelectionBox){
    if( !this.state.searchResult ){
      return <div className={classnames("result-box", withSelectionBox && 'with-selection-box')}>
        <div className="no-record" style={{padding:10, textAlign:'center'}}> No Result </div>
      </div>
    }

    return  <div className={classnames("result-box", withSelectionBox && 'with-selection-box')}>
      <ScrollableBox
        onScroll={:: this.onSearchBoxScroll}
        onReachScrollBottom={:: this.onReachScrollBottom}
        blockObserveScroll={this.state.loading}>
      { this.state.searchItems.map((item) => this.renderRecord(item)) }
      </ScrollableBox>
    </div>
  }

  render() {

    return (
      <Tool className="youtube">
        <ToolHeader
          title={this.context.intl.formatMessage({id:'app.tool.youtubeExplorer'})}
          iconType={ Info.iconType }
          icon={ Info.icon }
          iconColor={ Info.iconColor }
          onCloseHook={ ()=> this.onClose() }>

        </ToolHeader>
        <ToolBody
          isLoading={this.state.loading}
          disableScroll>
          <div className="search-box">
            <Search onSearch={::this.onSearch}></Search>
          </div>
          { this.state.selectedItems.length > 0 && this.renderSelections() }
          { this.renderResultRecords(this.state.selectedItems.length > 0) }
        </ToolBody>

        <ToolFooter>
          <FullmoonBtn
            shape="round"
            normalStyle={{
              display: 'inline-block',
              height: 30,
              borderWidth: 0,

              borderRadius: 100,
              fontSize: 12,
              color: this.context.theme.Tool.layout.footer.fullmoonBtn.textColor.default,
              fontFamily: 'Nanum Square',
              backgroundColor: this.context.theme.Tool.layout.footer.fullmoonBtn.bgColor.default,
              margin:10,
              padding:'0 20px',
              textAlign:'left',
            }}
            hoverStyle={{
              borderWidth: 0,
              backgroundColor:this.context.theme.Tool.layout.footer.fullmoonBtn.bgColor.hovered,
            }}
            moonBlender='overlay'
            onClick={(e)=>this.onApply()}>
            Apply
          </FullmoonBtn>
        </ToolFooter>

      </Tool>
    )
  }
}

export default YoutubeExplorer;
