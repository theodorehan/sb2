import defineUIIStatement from '../../../../utils/UII/defineUIIStatement';
import defineActSheet, { Types } from '../../../../utils/UII/actSheet';

export const ToolKey = "YoutubeExplorer";

export const CreateOpenActionSheet = (areaIndicator) => defineActSheet({
  "action" : "open",
  "type": Types.ToolInterfacePipeline, // ToolInterfacePipeline
  "options" : {
    "toolKey" : ToolKey,
    "areaIndicator" : areaIndicator,
  },
});


export const Info = {
  type : 'button',
  key : 'YoutubeExplorer',
  title : 'app.tool.youtubeExplorer',
  titleType : 'code',
  iconType: 'fa',
  icon: 'youtube',
  // iconColor : '#fdfdfd',
  options: {
    toolKey : ToolKey,
  },

  actionSheet : CreateOpenActionSheet('[chaining]'),
}



export const UIIStatement = defineUIIStatement(Info);


export const defaultStaticData = {
  selectable : false,
  multipleSelect : false,
  defaultSelectedItems : [],
}

