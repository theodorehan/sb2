import * as actionTypes from './actionTypes';


export const initialState = {
  searchValue: '',
  complexMode : true,
  spreadList : [],
};

export default function ProjectExplorerReducer(state = initialState, action) {

  switch (action.type) {
    case actionTypes.CHANGE_SEARCH_VALUE :
      return Object.assign({}, state, {
        searchValue: action.value,
      });


    default :
      return state;
  }
}
