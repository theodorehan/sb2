import defineUIIStatement from '../../../../utils/UII/defineUIIStatement';
import defineActSheet, { Types } from '../../../../utils/UII/actSheet';

export const ToolKey = "Pages";
export const Info = {
  type : 'button',
  key : 'Pages',
  title : 'app.tool.pages',
  titleType : 'code',
  iconType: 'img',
  icon: '/public/images/book-icon.png',
  iconColor : 'rgb(239, 215, 20)',
  options: {
    toolKey : ToolKey,
  },

  actionSheet : defineActSheet({
    "action" : "open",
    "type": Types.ToolInterfacePipeline, // ToolInterfacePipeline
    "options" : {
      "toolKey" : ToolKey,
      "areaIndicator" : "[chaining]",
    },
  }),
}

export const UIIStatement = defineUIIStatement(Info);
