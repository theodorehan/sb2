import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import classnames from 'classnames';
import RouteNode from '../../../../utils/RouteNode';

import NodeTree from '../../../NodeTree';

import Tool from '../common/Tool';
import ToolHeader from '../common/ToolHeader';
import ToolFooter from '../common/ToolFooter';
import ToolBody from '../common/ToolBody';


import BalancedItemTray from '../../../BalancedItemTray';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';


import './style.scss';

import {
  CreateOpenActionSheet as CreatePageOptionsOpenActionSheet,
} from '../PageOptions/Constants';



import {
  CreateOpenActionSheet as CreateDialogOpenActionSheet,
  ToolKey,
} from '../Dialog/Constants';


import {
  CreateOpenActionSheet as CreateCanvasOpenActionSheet,
} from '../Canvas/Constants';

import {
    SET_STATIC_DATA_BY_LAYOUT,
  CLOSE_LAYOUT_WITH_ELASTIC,
} from '../../UIISystemActionSheets';

import {
  Info,
} from './Constants';

import * as actions from './actions';

// Networks
import {
  CREATE_NEW_PAGE,
} from '../../../../Core/Network/Editing';

import {
  readRootRoute,
} from '../../../../Core/Network/Site';

import * as SiteActions from '../../../../actions/Site';


import FullmoonBtn from '../../../Unit/FullmoonButton';

let NodeIcon = (_props)=>{
  if( _props.node.isDir ){
    return <i className="fa fa-folder"/>;
  }

  switch( _props.node.extension ){
    default :
      return <i className="fa fa-file-text"/>;
  }
}



@connect((state) => ({
  Site : state.get('Site'),
  User : state.get('User'),
  AssignedStore : state.get('AssignedStore'),
}))
class Pages extends Component {
  static contextTypes = {
    toolSystem: PropTypes.object,
    elastic: PropTypes.object,
    UIISystems : PropTypes.object,
    zone : PropTypes.object,
    Site : PropTypes.object,
    User : PropTypes.object,
    functionStore : PropTypes.object,
    global : PropTypes.object,
    theme : PropTypes.object,
  }

  constructor(props) {
    super(props)
  }


  onNewPageUnder(e,node){
    let { route } = this.props.Site;

    let routeNode = RouteNode.importFromJSON(route);
    let nodeGraph = node.getLinealDescentList();
    let pathGraph = nodeGraph.map((node) => node.path);
    let basePath = pathGraph.join('/').replace(/^\/\/?/, '/').replace(/index$/, '/').replace(/(\/\/?)?$/ ,'/');

    console.log(node.getLinealDescentList());

    let transferData = {
      title : "Create new page",
      fields : [
        {
          label : 'Page name',
          key : 'name',
          type : 'input',
          inputType : 'string',
          placeholder : 'Input page name',
          defaultValue : '',
          validator : {
            functionKey : 'pageNameValidator',
          },
        },

        {
          label : 'Page path',
          key : 'path',
          type : 'input',
          inputType : 'string',
          placeholder : 'Input page name',
          filterFunctionKey : 'pagePathFilter',
          defaultValue : basePath,
          validator : {
            functionKey : 'pagePathValidator',
          },
        },



        {
          type : 'button-set',
          buttons : [
            {
              label : 'Create',
              functionKey : 'ok' ,
              level : 'positive',
              validate : true,
            },
            {
              label : 'Cancel' ,
              functionKey : 'cancel',
              level : 'negative',
            },
          ],
        },
      ],
    };


    let modalId;
    this.context.UIISystems.emitActionSheet(CreateDialogOpenActionSheet("[modal]"), transferData, {
      width : 400,
      height: 300,
      onLayoutID : function(id){
        modalId = id;
      },
    });

    this.context.functionStore.register('pagePathFilter',
      (value) => {

      if( value.indexOf(basePath) != 0 ){
        return basePath;
      } else {
        return value.replace(new RegExp(`^${basePath}(.+)$`), basePath+'$1').replace(/\s/g, '-');
      }
    });

    this.context.functionStore.register('pageNameValidator', (value)=>{
      if( /[\w가-힣]+/.test(value) ){
        return {
          result : true,
        }
      } else {
        return {
          result : false,
          message : '페이지명은 영문자 또는 한글로 시작해야 합니다.',
        }
      }
    });

    this.context.functionStore.register('pagePathValidator', (value) => {
      let {Site} = this.props;
      let { route } = Site;

      let routeNode = RouteNode.importFromJSON(route);

      if( routeNode.visitRecursive((node) => node.getRoutePath().replace(/\/$/,'') == value.replace(/\/$/,'')) ){
        return {
          result : false,
          message : '중복된 경로 입니다.',
        }
      }

      return {
        result : true,
      }
    });

    this.context.functionStore.register('ok', (fieldsData)=>{
      let pathField = fieldsData.path;
      let nameField = fieldsData.name;

      if( !(pathField.validation.result && nameField.validation.result) ){
        return;
      }

      let reqID = this.context.global.uiBlockingReqBegin();
      CREATE_NEW_PAGE(this.props.User.session.id, this.props.Site.info.id, this.props.Site.info.owner, nameField.value, pathField.value)
        .then((res)=>{
          console.log('created new page', res);
          // event 를 이용해서 전체적으로 반영


          readRootRoute(this.props.User.session.id, this.props.Site.info.id, this.props.Site.info.owner)
            .then( (_result) => {
              let {status, data} = _result;

              if (status === 200 && data.result === 'success') {
                this.props.dispatch(SiteActions.LOAD_ROUTE(data.route));
              } else {
                alert("페이지 추가 실패");
              }


              this.context.UIISystems.emitActionSheet(CLOSE_LAYOUT_WITH_ELASTIC(modalId, ToolKey, true));
              this.context.global.uiBlockingReqFinish(reqID);
            });
        })
    });

    this.context.functionStore.register('cancel', (fieldsData)=>{
      this.context.UIISystems.emitActionSheet(CLOSE_LAYOUT_WITH_ELASTIC(modalId, ToolKey, true));
    });
  }

  onPageMore(e, node){
    e.preventDefault();
    e.stopPropagation();

    let {
      clientX,
      clientY,
    } = e;


    // this.context.UIISystems.emitActionSheet(CreatePageOptionsOpenActionSheet("[layer]"), {
    //   pageNode : node,
    // }, {
    //   UNIQUE_LAYER : true,
    //   x : clientX + 20,
    //   y : clientY,
    // });

    this.context.UIISystems.emitActionSheet(CreatePageOptionsOpenActionSheet("[modal]"), {
      pageNode : node,
    }, {
      width : 200,
      height: 200,
    });
  }

  onClickNode(e, graph){
    let node = graph[graph.length -1];

    if( node.page ){
      this.context.UIISystems.emitActionSheet(CreateCanvasOpenActionSheet("central"), {
        path : node.getRoutePath(true),
        name : node.name,
        routeDirective : node.getDirectivePath(true),
      }, {DISABLE_DUPLICATE_UNDEPLOY:true});
    }
  }

  renderNodeOptions(node){
    if( !node.isDir ){


      return <div onClick={(e) => e.stopPropagation() }>
        { !node.page && <FullmoonBtn
          shape="round"
          normalStyle={{
            display: 'inline-block',
            height: 20,
            borderWidth: 0,

            borderRadius: 100,
            fontSize: 12,
            color: this.context.theme.Tool.layout.footer.fullmoonBtn.textColor.default,
            fontFamily: 'Nanum Square',
            backgroundColor: this.context.theme.Tool.layout.footer.fullmoonBtn.bgColor.default,
            margin:5,
            marginLeft : 0,
            padding:'0 20px',
            textAlign:'left',
          }}
          hoverStyle={{
            borderWidth: 0,
            backgroundColor:this.context.theme.Tool.layout.footer.fullmoonBtn.bgColor.hovered,
          }}
          moonBlender='overlay'
          onClick={(e)=>this.onNewPageUnder(e, node)}>
          Make page
        </FullmoonBtn> }

        <FullmoonBtn
          shape="round"
          normalStyle={{
            display: 'inline-block',
            height: 20,
            borderWidth: 0,

            borderRadius: 100,
            fontSize: 12,
            color: this.context.theme.Tool.layout.footer.fullmoonBtn.textColor.default,
            fontFamily: 'Nanum Square',
            backgroundColor: this.context.theme.Tool.layout.footer.fullmoonBtn.bgColor.default,
            margin:5,

            padding:'0 20px',
            textAlign:'left',
          }}
          hoverStyle={{
            borderWidth: 0,
            backgroundColor:this.context.theme.Tool.layout.footer.fullmoonBtn.bgColor.hovered,
          }}
          moonBlender='overlay'
          onClick={(e)=>this.onPageMore(e, node)}>
          SEO & visibility
        </FullmoonBtn>

        <FullmoonBtn
          shape="round"
          normalStyle={{
            display: 'inline-block',
            height: 20,
            borderWidth: 0,

            borderRadius: 100,
            fontSize: 12,
            color: this.context.theme.Tool.layout.footer.fullmoonBtn.textColor.default,
            fontFamily: 'Nanum Square',
            backgroundColor: this.context.theme.Tool.layout.footer.fullmoonBtn.bgColor.default,
            margin:5,
            marginRight : 0,
            padding:'0 20px',
            textAlign:'left',
          }}
          hoverStyle={{
            borderWidth: 0,
            backgroundColor:this.context.theme.Tool.layout.footer.fullmoonBtn.bgColor.hovered,
          }}
          moonBlender='overlay'
          onClick={(e)=>this.onNewPageUnder(e, node)}>
          Create page under
        </FullmoonBtn>
      </div>;
    }

    return null;
  }


  render() {
    let {Site} = this.props;
    let { route } = Site;


    return (
      <Tool className="pages">
        <ToolHeader
          title="Pages"
          iconType={ Info.iconType }
          icon={ Info.icon }
          iconColor={ Info.iconColor }/>
        <ToolBody nofooter>
          <div className='node-tree-wrapper'>


            <NodeTree
              node={RouteNode.importFromJSON(route)}
              name={Site.info.siteName}
              onClickNode={:: this.onClickNode}
              getName={(node) =>  (node.page ? '<i class="fa fa-file-text"></i> ':'' )+ `<span>${node.name}</span><span class="path">${node.getRoutePath(true)}</span>`}
              getNodeOptions={::this.renderNodeOptions}/>

          </div>
        </ToolBody>
      </Tool>
    )
  }
}

export default Pages;
