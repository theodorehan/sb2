import namespaceBindGenerator from '../../../../utils/redux/namespaceBindGenerator';
let defineType = namespaceBindGenerator('Curator');

export const SPREAD_ITEM = defineType("SPREAD_ITEM");

export const CANCEL_SPREAD_ITEM = defineType("CANCEL_SPREAD_ITEM");


export const SPREAD_ALL = defineType("SPREAD_ALL");

export const SPREAD_EACH = defineType("SPREAD_EACH");
