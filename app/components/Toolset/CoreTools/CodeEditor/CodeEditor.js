import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import classnames from 'classnames';

import brace from 'brace';
import AceEditor from 'react-ace';

import 'brace/mode/json';
import 'brace/mode/javascript';
import 'brace/mode/html';
import 'brace/theme/github';

import { Select } from 'antd';
const Option = Select.Option;
import { Input } from 'antd';
const { TextArea } = Input;
import Toggle from 'material-ui/Toggle';
import { InputNumber } from 'antd';
import { Radio } from 'antd';
const RadioGroup = Radio.Group;


import Tool from '../common/Tool';
import ToolHeader, {height as headerHeight} from '../common/ToolHeader';
import ToolFooter, {height as footerHeight} from '../common/ToolFooter';
import ToolBody from '../common/ToolBody';


import './style.scss';

import {
ToolKey as CanvasToolKey,
} from '../Canvas';

import {
  CreateOpenActionSheet as CreateResourceOpenActionSheet,
} from '../ResourceManager/Constants';

import * as actions from './actions';

import {
  Info,
} from './Constants';

import AddIcon from 'material-ui/svg-icons/content/add';
import MinusIcon from 'material-ui/svg-icons/content/remove';
import ArrowDropDownIcon from 'material-ui/svg-icons/navigation/arrow-drop-down';
import ArrowDropUpIcon from 'material-ui/svg-icons/navigation/arrow-drop-up';

import FullmoonBtn from '../../../Unit/FullmoonButton';

@connect((state) => ({
  User : state.get('User'),
  Site : state.get('Site'),
  AssignedStore : state.get('AssignedStore'),
  Elastic : state.get('Elastic'),
}))
class CodeEditor extends Component {
  static contextTypes = {
    UIISystems : PropTypes.object,
    elastic: PropTypes.object,
    zone : PropTypes.object,
    events : PropTypes.object,
    functionStore : PropTypes.object,
    theme : PropTypes.object,
    layout : PropTypes.object,
  }


  constructor(props) {
    super(props)


    this.state = {
      raw : '',
      validation : [],
    }
  }


  onClickFooterButton(item, e){
    if( item.functionKey ){
      let func = this.context.functionStore.get(item.functionKey);
      func(this.state.raw, this.state.validation);
    }
  }

  onClose(){
    if( this.props.staticData.closeHookFunctionKey ){
      let func = this.context.functionStore.get(this.props.staticData.closeHookFunctionKey);

      if( typeof func === 'function' ){
        return func(this.state.raw, this.state.validation);
      }
    }

    return true;
  }

  onValidate(validationResults){
    this.setState({validation :validationResults});
  }

  componentWillReceiveProps(next){
    if( this.props.staticData.raw !== next.staticData.raw ){
      this.setState({raw : next.staticData.raw})
    }
  }

  componentDidMount(){
    this.setState({raw : this.props.staticData.raw});
  }

  render() {

    let { footerButtons } = this.props.staticData;
    let useFooter = (Array.isArray(footerButtons) && footerButtons.length > 0);

    let bodyHeight = this.props.height - (useFooter && footerHeight) - headerHeight;

    return (
      <Tool className="Editor">
        <ToolHeader
          title={'Code Editor' + (this.props.staticData.title && ' - ' + this.props.staticData.title) }
          iconType={ Info.iconType }
          icon={ Info.icon }
          iconColor={ Info.iconColor }
          onCloseHook={ ()=> this.onClose() }>

        </ToolHeader>
        <ToolBody nofooter={ !useFooter }>
          <AceEditor
            width="100%"
            height={bodyHeight + 'px'}
            mode={this.props.staticData.lang}
            theme="github"
            value={this.state.raw}
            onChange={(value, e) => { this.setState({raw : value})}}
            onValidate={:: this.onValidate }
            name="UNIQUE_ID_OF_DIV"
            editorProps={{$blockScrolling: true}}
          />
        </ToolBody>
        {
          useFooter &&
          <ToolFooter>
            {footerButtons.map((btn,i) => <FullmoonBtn
              key={i}
              shape="round"
              normalStyle={{
                display: 'inline-block',
                height: 30,
                borderWidth: 0,

                borderRadius: 100,
                fontSize: 12,
                color: this.context.theme.Tool.layout.footer.fullmoonBtn.textColor.default,
                fontFamily: 'Nanum Square',
                backgroundColor: this.context.theme.Tool.layout.footer.fullmoonBtn.bgColor.default,
                margin:10,
                padding:'0 20px',
                textAlign:'left',
              }}
              hoverStyle={{
                borderWidth: 0,
                backgroundColor:this.context.theme.Tool.layout.footer.fullmoonBtn.bgColor.hovered,
              }}
              moonBlender='overlay'
              onClick={(e)=>this.onClickFooterButton(btn, e)}>
              {btn.title}
            </FullmoonBtn>)}
          </ToolFooter>
        }

      </Tool>
    )
  }
}

export default CodeEditor
