import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import classnames from 'classnames';
import moment from 'moment';


import { Radio, InputNumber, Select, Input,Collapse, Card,Table, Icon, Switch  } from 'antd';
const RadioGroup = Radio.Group;
const Option = Select.Option;
const { TextArea } = Input;
const Panel = Collapse.Panel;

import Tool from '../common/Tool';
import ToolHeader from '../common/ToolHeader';
import ToolFooter from '../common/ToolFooter';
import ToolBody from '../common/ToolBody';

import Ice2Table from '../../../Ice2Table';
import Ice2ItemViewer from '../../../Ice2ItemViewer';
import Ice2Form from '../../../Ice2Form';


import {directiveNameIceSchema} from "../../../../utils/iceFragment";


import './style.scss';


import * as actions from './actions';

import {
  Info,
} from './Constants';

import AddIcon from 'material-ui/svg-icons/content/add';
import MinusIcon from 'material-ui/svg-icons/content/remove';
import ArrowDropDownIcon from 'material-ui/svg-icons/navigation/arrow-drop-down';
import ArrowDropUpIcon from 'material-ui/svg-icons/navigation/arrow-drop-up';

import FullmoonBtn from '../../../Unit/FullmoonButton';




@connect((state) => ({
  User : state.get('User'),
  Site : state.get('Site'),
}))
class ICE2Data extends Component {
  static contextTypes = {
    UIISystems : PropTypes.object,
    elastic: PropTypes.object,
    zone : PropTypes.object,
    events : PropTypes.object,
    functionStore : PropTypes.object,
    theme : PropTypes.object,
    global : PropTypes.object,
    ice2 : PropTypes.func,
    layout : PropTypes.object,
    intl: PropTypes.object,
  }



  constructor(props) {
    super(props)



    this.state = {
      sortedInfo : null,
      filteredInfo : null,
      list : [],
    };
  }


  onClose(){
    if( this.props.staticData.closeHookFunctionKey ){
      let func = this.context.functionStore.get(this.props.staticData.closeHookFunctionKey);

      if( typeof func === 'function' ){
        return func(this);
      }
    }

    return true;
  }


  async toggleUseYn(record){
    let formData = new FormData();
    formData.append(this.props.staticData.idField,record[this.props.staticData.idField]);
    formData.append('status', record.status && record.status.value && record.status.value == 'y' ? 'n' : 'y')


    let blockingId = this.context.global.uiBlockingReqBegin();
    await this.context.ice2.request({
      url : '/node/serviceFragment/save.json',
      method:'post',
      data :formData,
    });

    await this.load();
    this.context.global.uiBlockingReqFinish(blockingId);
  }

  async load(){
    let {
      directiveName,
    } = this.props.staticData;

    let schema = directiveNameIceSchema(directiveName);
    let currentVersion = schema.version;


    let blockingId = this.context.global.uiBlockingReqBegin();
    let listResponse = await this.context.ice2.request({
      url : '/node/serviceFragment/list.json',
      params : {
        metaKey1 : schema.meta1,
        metaKey2 : schema.meta2,
        metaKey3 : schema.meta3,
        sorting: 'version desc',
        limit:99999,
      },
    });


    this.setState({
      list : listResponse.data.items,
    });
    this.context.global.uiBlockingReqFinish(blockingId);
  }

  loadThisVersion(record){
    let func = this.context.functionStore.get(this.props.staticData.versionLoadFunctionKey);
    func(record);
  }

  handleChange(pagination, filters, sorter){
    console.log('Various parameters', pagination, filters, sorter);
    this.setState({
      filteredInfo: filters,
      sortedInfo: sorter,
    });
  }


  componentDidMount(){
    this.load();
  }


  render() {
    let {
      directiveName,
    } = this.props.staticData;

    let schema = directiveNameIceSchema(directiveName);

    let { sortedInfo, filteredInfo, list } = this.state;
    sortedInfo = sortedInfo || {};
    filteredInfo = filteredInfo || {};


    const columns = [{
      title: '활성 여부',
      dataIndex: 'status',
      key: 'status',
      // filters: [
      //   { text: '사용', value: 'y' },
      //   { text: '미사용', value: 'n' },
      // ],
      // filteredValue: filteredInfo.name || null,
      // onFilter: (value, record) => record.status ? record.status.value.includes(value) : false,
      render : (status, record) => <Switch

        checked={status && status.value == 'y'}
        onChange={() => this.toggleUseYn(record)} />,
    }, {
      title: '버전',
      dataIndex: 'version',
      key: 'version',
      sorter: (a, b) => a.version - b.version,
      sortOrder: sortedInfo.columnKey === 'version' && sortedInfo.order,
    },

    {
      title: '반영 메시지',
      dataIndex: 'description',
      key: 'description',
    },

      {
      title: '생성자',
      dataIndex: 'owner',
      key: 'owner',
      // sorter: (a, b) => a.owner.value - b.owner.value,
      // sortOrder: sortedInfo.columnKey === 'owner' && sortedInfo.order,
      render : (owner) => owner && owner.label || '?',
    }, {
      title: '생성일',
      dataIndex: 'created',
      key: 'created',
      sorter: (a, b) => a.created - b.created,
      sortOrder: sortedInfo.columnKey === 'created' && sortedInfo.order,
      render : (created) => moment(created, 'YYYYMMDDhhmmss').format('YYYY/MM/DD h:mm:ss a') ,
    },{
      title: '작업',
      key: '$actions',
      render : (created, record) => <div className="works">
        {this.props.staticData.versionLoadFunctionKey && <button onClick={() => this.loadThisVersion(record)}>불러오기</button>}
        <button onClick={() => window.open(`${this.props.staticData.pageBaseUrl}&${schema.meta1};${schema.meta2};${schema.meta3}=${record.version}`)}>미리보기</button>
      </div>,
    }];


    return (
      <Tool className="ice2version-manager">
        <ToolHeader
          title={this.props.staticData.title || 'Version Manager'}
          iconType={ Info.iconType }
          icon={ Info.icon }
          iconColor={ Info.iconColor }
          onCloseHook={ ()=> this.onClose() }>

        </ToolHeader>
        <ToolBody nofooter>
          <div className="wall">
            <Icon className="icon" type="exclamation-circle-o"/>
            <span className='text'>실제 사이트에서는 버전이 제일 높은 항목으로 자동 적용됩니다.</span>
          </div>
          <div className="wall">
            <Icon className="icon" type="eye" />
            <span className='text'>활성여부를 토글하여 최종적으로 보여질 버전을 결정하세요.</span>
          </div>
          <div className="wall">
            <Icon className="icon" type="edit" />
            <span className='text'>불러오기를 사용하여 새로운 최신버전을 제작하세요.</span>
          </div>
          <div className=''>
            <Table size='small' columns={columns} dataSource={list} onChange={:: this.handleChange} />
          </div>
        </ToolBody>

      </Tool>
    )
  }
}

export default ICE2Data;
