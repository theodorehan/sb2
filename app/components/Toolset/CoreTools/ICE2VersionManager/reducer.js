import * as actionTypes from './actionTypes';


export const initialState = {
  apiTarget : 'node',
  categories : {},
  categoryLoading : false,
  nodes : [],
};

export default function APIReducer(state = initialState, action) {

  switch (action.type) {
    case actionTypes.SET_CATEGORIES:
      return Object.assign({}, state, {
        categories: action.categories,
      });
    case actionTypes.START_ROOT_LOADING:
      return Object.assign({}, state, {
        categoryLoading : true,
      });
    case actionTypes.FINISH_ROOT_LOADING:
      return Object.assign({}, state, {
        categoryLoading : false,
      });
    case actionTypes.SET_CATEGORY_CHILDREN:
      return Object.assign({}, state, {
        categories : Object.assign({}, state.categories, {
          [action.categoryId] : Object.assign({}, state.categories[action.categoryId] || {}, {
            children : action.children,
          }),
        }),
      });
    case actionTypes.SET_NODES :
      return Object.assign({}, state, {
        nodes : action.nodes,
      });
    default :
      return state;
  }
}
