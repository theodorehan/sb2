import defineUIIStatement from '../../../../utils/UII/defineUIIStatement';
import defineActSheet, { Types } from '../../../../utils/UII/actSheet';

export const ToolKey = "GridEditor";

export const CreateOpenActionSheet = (areaIndicator) => defineActSheet({
  "action" : "open",
  "type": Types.ToolInterfacePipeline, // ToolInterfacePipeline
  "options" : {
    "toolKey" : ToolKey,
    "areaIndicator" : areaIndicator,
  },
});


export const Info = {
  type : 'button',
  key : 'GridEditor',
  title : 'app.tool.grid-editor',
  titleType : 'code',
  iconType: 'material',
  icon: 'ActionViewQuilt',
  // iconColor : '#fdfdfd',
  options: {
    toolKey : ToolKey,
  },

  actionSheet : CreateOpenActionSheet('[chaining]'),
}

export const GRIDBOX_OUTER_WIDTH = 30 * 3;


export const UIIStatement = defineUIIStatement(Info);
