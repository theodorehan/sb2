import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import classnames from 'classnames';
import {Directive} from 'abstract-component-node';


import Tool from '../common/Tool';
import ToolHeader from '../common/ToolHeader';
import ToolFooter from '../common/ToolFooter';
import ToolBody from '../common/ToolBody';

import HierarchyLineViewer from './HierarchyLineViewer';
import ChildrenModifier from './ChildrenModifier';


import './style.scss';

import {
CreateOpenActionSheet as CreateCanvasOpenActionSheet,
ToolKey as CanvasToolKey,
} from '../Canvas';

import {
  getEffectiveColumnSizingTypeByScreenWidth,
  getSpecialNodeStyleValue,
  isBrotherhoodNode,
} from '../../../../Core/Helper/DirectiveNodeWithGridSystem';


import {
Info,
  GRIDBOX_OUTER_WIDTH,
} from './Constants';

import * as actions from './actions';

import Draggable from '../../../../Core/Components/Draggable';


let NodeIcon = (_props)=>{
  if( _props.node.isDir ){
    return <i className="fa fa-folder"/>;
  }

  switch( _props.node.extension ){
    default :
      return <i className="fa fa-file-text"/>;
  }
}

@connect((state) => ({
  Site : state.get('Site'),
  AssignedStore : state.get('AssignedStore'),
  Elastic : state.get('Elastic'),
}))
class GridEditor extends Component {
  static contextTypes = {
    elastic: PropTypes.object,
    zone : PropTypes.object,
    events : PropTypes.object,
  }


  constructor(props) {
    super(props)


    this.state = {
      mounted : false,
      bodyWidth : 0,
      bodyHeight : 0,
    }


    this.gridNodeDOMList = {};
  }

  componentWillUnmount(){
    console.log('GridEditor unmount');
  }

  componentDidMount(){
    console.log("GridEditor Mounted");
    let toolBodyDOM = ReactDOM.findDOMNode(this.toolBodyComp);

    this.setState({
      mounted : true,
      bodyDOM : toolBodyDOM,
      bodyWidth : toolBodyDOM.offsetWidth,
      bodyHeight : toolBodyDOM.offsetHeight,
    });
  }


  onModifiedDirectiveJSON(exportedJSON, modifiedNode, directiveName, modifiedNodePos){

    this.context.events.emit('MODIFIED-DIRECTIVE-NODE', {exportedJSON, modifiedNode, directiveName, modifiedNodePos});
  }

  onDragColumnHookLeft(node, deltaX, deltaY){
    console.log(this.gridNodeDOMList[node.pos], deltaX, deltaY);
  }

  onDragColumnHookRight(node, deltaX, deltaY){
    console.log(node, deltaX, deltaY);
  }

  onClickNode(e, node){
    e.preventDefault();
    e.stopPropagation();

    let canvasState = this.context.elastic.getOtherOnlyOneToolState(CanvasToolKey);
    let {   selectedDirectiveName } = canvasState;

    this.context.events.emit('SELECT-DIRECTIVE-NODE', {node : node, directiveName:selectedDirectiveName});
  }

  onHoverNode(e, node){
    e.preventDefault();
    e.stopPropagation();

    let canvasState = this.context.elastic.getOtherOnlyOneToolState(CanvasToolKey);
    let {   selectedDirectiveName } = canvasState;

    this.context.events.emit('HOVER-DIRECTIVE-NODE', {node : node, directiveName:selectedDirectiveName});
  }

  onHoverCancelNode(e){
    e.preventDefault();
    e.stopPropagation();


    this.context.events.emit('HOVER-LEAVE-DIRECTIVE-NODE');
  }

  renderNoSelect(){
    return <div className="no-select-info">
      <span>Select Grid Element</span>
    </div>
  }

  renderNodeLayer(directiveNode, selectPos, screenWidth, wRatio ){

    return <div
      ref={(dom) => this.gridNodeDOMList[directiveNode.pos] = dom }

      onClick={ (e)=> this.onClickNode(e, directiveNode) }
      onMouseOver={ (e)=> this.onHoverNode(e, directiveNode) }
      onMouseOut={ (e)=> this.onHoverCancelNode(e, directiveNode) }

      onFocus={ (e)=> this.onHoverNode(e, directiveNode) }
      onBlur={ (e)=> this.onHoverCancelNode(e, directiveNode) }

      className={classnames('node', 'layer', directiveNode.pos === selectPos && 'selected', isBrotherhoodNode(selectPos, directiveNode) && 'brotherhood')}
      key={directiveNode.pos}>

      { directiveNode.children && directiveNode.children.map((node)=> this.renderByType(node, selectPos, screenWidth, wRatio))}
    </div>
  }

  renderNodeColumn(directiveNode, selectPos, screenWidth, wRatio ){
    let columnClass ='';

    if( directiveNode.props ){
      columnClass = `spacing-${directiveNode.props[getEffectiveColumnSizingTypeByScreenWidth(screenWidth, directiveNode.props)]}`
    }

    return <div
      ref={(dom) => this.gridNodeDOMList[directiveNode.pos] = dom }
      onClick={ (e)=> this.onClickNode(e, directiveNode) }
      onMouseOver={ (e)=> this.onHoverNode(e, directiveNode) }
      onMouseOut={ (e)=> this.onHoverCancelNode(e, directiveNode) }

      onFocus={ (e)=> this.onHoverNode(e, directiveNode) }
      onBlur={ (e)=> this.onHoverCancelNode(e, directiveNode) }

      className={classnames('node', 'column', directiveNode.pos === selectPos && 'selected', columnClass, isBrotherhoodNode(selectPos, directiveNode) && 'brotherhood')}
      key={directiveNode.pos}>

      { directiveNode.children && directiveNode.children.map((node)=> this.renderByType(node, selectPos,screenWidth, wRatio))}

      <Draggable
        className="drag-hook-left"
        onDragMove={(x,y_, deltaX, deltaY) => this.onDragColumnHookLeft(directiveNode, deltaX, deltaY)}
        component={()=><div/>}
        _draggableType="hook"
        _draggableData={{}}/>
      <Draggable
        className="drag-hook-right"
        onDragMove={(x,y_, deltaX, deltaY) => this.onDragColumnHookRight(directiveNode, deltaX, deltaY)}
        component={()=><div/>}
        _draggableType="hook"
        _draggableData={{}}/>
    </div>
  }

  renderNodeRow(directiveNode, selectPos, screenWidth, wRatio ){

    return <div
      ref={(dom) => this.gridNodeDOMList[directiveNode.pos] = dom }

      onClick={ (e)=> this.onClickNode(e, directiveNode) }
      onMouseOver={ (e)=> this.onHoverNode(e, directiveNode) }
      onMouseOut={ (e)=> this.onHoverCancelNode(e, directiveNode) }

      onFocus={ (e)=> this.onHoverNode(e, directiveNode) }
      onBlur={ (e)=> this.onHoverCancelNode(e, directiveNode) }

      className={classnames('node', 'row', directiveNode.pos === selectPos && 'selected', isBrotherhoodNode(selectPos, directiveNode) && 'brotherhood')}
      key={directiveNode.pos}>

      { directiveNode.children && directiveNode.children.map((node)=> this.renderByType(node, selectPos, screenWidth, wRatio))}
    </div>
  }

  renderNodeGrid(directiveNode, selectPos, screenWidth, wRatio){

    return <div
      ref={(dom) => this.gridNodeDOMList[directiveNode.pos] = dom }

      onClick={ (e)=> this.onClickNode(e, directiveNode) }
      onMouseOver={ (e)=> this.onHoverNode(e, directiveNode) }
      onMouseOut={ (e)=> this.onHoverCancelNode(e, directiveNode) }

      onFocus={ (e)=> this.onHoverNode(e, directiveNode) }
      onBlur={ (e)=> this.onHoverCancelNode(e, directiveNode) }

      className={classnames('node', 'grid', directiveNode.pos === selectPos && 'selected', isBrotherhoodNode(selectPos, directiveNode) && 'brotherhood')}
      key={directiveNode.pos}>

      { directiveNode.children && directiveNode.children.map((node)=> this.renderByType(node,  selectPos, screenWidth, wRatio))}
    </div>
  }

  renderByType(directiveNode, selectPos, screenWidth, wRatio){
    switch( directiveNode.tag ){
      case "core-system-grid":
        return this.renderNodeGrid(directiveNode, selectPos, screenWidth, wRatio);
      case "core-system-row":
        return this.renderNodeRow(directiveNode, selectPos, screenWidth, wRatio);
      case "core-system-column":
        return this.renderNodeColumn(directiveNode, selectPos, screenWidth, wRatio);
      case "core-system-layer":
        return this.renderNodeLayer(directiveNode, selectPos, screenWidth, wRatio);
    }
  }

  renderDirectiveLayout(directive, selectPos, screenWidth, wRatio){
    if( directive.rootNode.tag !== 'core-system-grid' ) throw new Error("Directive 의 rootNode는 Grid 여야 합니다.");

    return this.renderByType(directive.rootNode, selectPos, screenWidth, wRatio);
  }

  renderGridBox(directive, selectedPos, selectedNode, screenWidth, totalWidth, totalHeight){


    let gridBoxMaxWidth = this.props.width - GRIDBOX_OUTER_WIDTH;
    let widthRatio = gridBoxMaxWidth/totalWidth * 100;

    // console.log(widthRatio)
    return <div className="simple-render-editor">
      <div className="margin-box">
        <input className="box-input top" value={getSpecialNodeStyleValue(selectedNode,'marginTop')}/>
        <input className="box-input bottom" value={getSpecialNodeStyleValue(selectedNode,'marginBottom')}/>
        <input className="box-input left" value={getSpecialNodeStyleValue(selectedNode,'marginLeft')}/>
        <input className="box-input right" value={getSpecialNodeStyleValue(selectedNode,'marginRight')}/>
        <span className="box-label"> margin </span>

        <div className="border-box">
          <input className="box-input top" value={getSpecialNodeStyleValue(selectedNode,'borderTopWidth')}/>
          <input className="box-input bottom" value={getSpecialNodeStyleValue(selectedNode,'borderBottomWidth')}/>
          <input className="box-input left" value={getSpecialNodeStyleValue(selectedNode,'borderLeftWidth')}/>
          <input className="box-input right" value={getSpecialNodeStyleValue(selectedNode,'borderRightWidth')}/>
          <span className="box-label"> border </span>

          <div className="padding-box">
            <input className="box-input top" value={getSpecialNodeStyleValue(selectedNode,'paddingTop')}/>
            <input className="box-input bottom" value={getSpecialNodeStyleValue(selectedNode,'paddingBottom')}/>
            <input className="box-input left" value={getSpecialNodeStyleValue(selectedNode,'paddingLeft')}/>
            <input className="box-input right" value={getSpecialNodeStyleValue(selectedNode,'paddingRight')}/>
            <span className="box-label"> padding </span>

            <div className="grid-box" style={{width:gridBoxMaxWidth}}>
              { this.renderDirectiveLayout(directive, selectedPos, screenWidth, widthRatio)}
            </div>
          </div>
        </div>
      </div>
    </div>
  }

  renderEditor(directive, selectedPos, selectedNode, screenWidth, totalWidth, totalHeight){
    return <div className="editor">
      { this.renderGridBox(directive, selectedPos, selectedNode, screenWidth, totalWidth, totalHeight) }
      <span className="info-location">
        <span className="component-name">
          {(()=>{
            switch(selectedNode.tag){
              case "core-system-grid" :
                return "GRID";
              case "core-system-row" :
                return "ROW";
              case "core-system-column" :
                return "COLUMN";
              case "core-system-layer" :
                return "LAYER";
              default:
                return selectedNode.tag
            }
          })()}
        </span>
        <span className="loc"> {selectedPos} </span>
      </span>
    </div>
  }

  render() {
    // let {staticData, Site} = this.props;
    // let { directivesMap, directiveName, directiveNodePos } = staticData;
    let canvasState = this.context.elastic.getOtherOnlyOneToolState(CanvasToolKey);
    let {   selectedDirectiveName, selectedDirectiveNodePos, routeDirectiveJSONTREE_map, iframeState } = canvasState;
    let {screenWidth, totalWidth, totalHeight} = iframeState;

    let directive, selectedNode;
    if( selectedDirectiveNodePos ){
      directive = new Directive(selectedDirectiveName, routeDirectiveJSONTREE_map[selectedDirectiveName]);

      selectedNode = directive.rootNode.findByLocation(selectedDirectiveNodePos);
    }

    return (
      <Tool className="grid-editor">
        <ToolHeader
          title="Layout Grid Editor"
          iconType={ Info.iconType }
          icon={ Info.icon }
          iconColor={ Info.iconColor }/>
        <ToolBody ref={(toolBody) => { this.toolBodyComp = toolBody } } nofooter>


          {
            selectedDirectiveNodePos ?
              this.renderEditor(
                new Directive(selectedDirectiveName, routeDirectiveJSONTREE_map[selectedDirectiveName]),
                selectedDirectiveNodePos,
                selectedNode,
                screenWidth,
                totalWidth,
                totalHeight,
              ) :
              this.renderNoSelect()
          }

          { selectedDirectiveNodePos &&
          <HierarchyLineViewer
            directive={directive}
            selectedDirectiveNodePos={selectedDirectiveNodePos}
            selectedNode={selectedNode}
            onClickNode={:: this.onClickNode }
            onHoverNode={:: this.onHoverNode }
            onHoverCancelNode={:: this.onHoverCancelNode }/> }

          { selectedDirectiveNodePos &&
            <ChildrenModifier
              screenWidth={screenWidth}
              directive={directive}
              selectedDirectiveNodePos={selectedDirectiveNodePos}
              selectedNode={selectedNode}
              onModifiedDirectiveJSON={:: this.onModifiedDirectiveJSON}/> }


        </ToolBody>
      </Tool>
    )
  }
}

export default GridEditor;
