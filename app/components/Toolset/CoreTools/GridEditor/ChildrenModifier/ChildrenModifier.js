import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import './style.scss';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import range from 'lodash/range';
import Slider from 'material-ui/Slider';

import BalancedItemTray from '../../../../BalancedItemTray';



import {
  COLUMN_DECIDE_BEHAVIOR_LG,
  COLUMN_DECIDE_BEHAVIOR_MD,
  COLUMN_DECIDE_BEHAVIOR_SM,
} from '../../../../../Core/Helper/GridSystemConstants';


import {
  getEffectiveColumnSizingTypeByScreenWidth,
  getSpecialNodeStyleValue,
} from '../../../../../Core/Helper/DirectiveNodeWithGridSystem';

import RcSlider, { Range } from 'rc-slider';
// We can just import Slider or Range to reduce bundle size
// import Slider from 'rc-slider/lib/Slider';
// import Range from 'rc-slider/lib/Range';
import 'rc-slider/assets/index.css';

const columnSizeSliderStyle = {
  default: {
    paddingLeft:10,
    paddingRight:10,
  },

  sliderStyle : {
    marginTop:0,
    marginBottom : 0,
  },
}


function getColumns(rowNode){
  return rowNode.children.filter((node) => node.tag === 'core-system-column');
}

function getColumnSizeArray(type, nodes){
  return nodes.map((node)=> node.props[type] );
}

const ROW_OPTIONS = (props) => {
  let node = props.node;
  let columnsOfRow = getColumns(node);
  let columnSizesArray = getColumnSizeArray('xs', columnsOfRow);


  return <div className="options">
    <div className="label">Row columns</div>
  </div>
}

const COLUMN_OPTIONS = (props) => {

  let effectiveColumnSizingType= getEffectiveColumnSizingTypeByScreenWidth( props.screenWidth, props.node.props);

  return <div className="options">
    <div className="label">Column size relative to screen size</div>

    <div className={classnames("option", effectiveColumnSizingType === 'xs' ? 'active':'')}>
      <div className="label">
        ({props.node.props.xs || 0}) Default
      </div>
      <Slider
        min={0}
        max={12}
        step={1}
        value={props.node.props.xs || 0}
        sliderStyle={columnSizeSliderStyle.sliderStyle}
        style={columnSizeSliderStyle.default}
        onChange={(e,i)=>props.onChangeColumnSize('xs', i)}
      />
    </div>

    <div className={classnames("option", effectiveColumnSizingType === 'sm' ? 'active':'')}>
      <div className="label">
        ({props.node.props.sm || 0}) At Screen Small  (Screen Width >= {COLUMN_DECIDE_BEHAVIOR_SM})
      </div>
      <Slider
        min={0}
        max={12}
        step={1}
        value={props.node.props.sm || 0}
        sliderStyle={columnSizeSliderStyle.sliderStyle}
        style={columnSizeSliderStyle.default}
        onChange={(e,i)=>props.onChangeColumnSize('sm', i)}
      />
    </div>

    <div className={classnames("option", effectiveColumnSizingType === 'md' ? 'active':'')}>
      <div className="label">
        ({props.node.props.md || 0}) At Screen Medium (Screen Width >= {COLUMN_DECIDE_BEHAVIOR_MD})
      </div>
      <Slider
        min={0}
        max={12}
        step={1}
        value={props.node.props.md || 0}
        sliderStyle={columnSizeSliderStyle.sliderStyle}
        style={columnSizeSliderStyle.default}
        onChange={(e,i)=>props.onChangeColumnSize('md', i)}
      />
    </div>

    <div className={classnames("option", effectiveColumnSizingType === 'lg' ? 'active':'')}>
      <div className="label">
        ({props.node.props.lg || 0}) At Screen Large (Screen Width >= {COLUMN_DECIDE_BEHAVIOR_LG})
      </div>
      <Slider
        min={0}
        max={12}
        step={1}
        value={props.node.props.lg || 0}
        sliderStyle={columnSizeSliderStyle.sliderStyle}
        style={columnSizeSliderStyle.default}
        onChange={(e,i)=>props.onChangeColumnSize('lg', i)}
      />
    </div>

  </div>
}

//
//
// let radioStyle = {
//   horizontal : {
//     float : 'left',
//     display : 'inline-block',
//     width:'auto',
//   },
// }
const COMMON_OPTIONS = ({node, onChangeStyle}) => {
  let lintEscape = 'arrow-body-style';


  return <div className="options">
    <div className="label">Common</div>

    <div className={classnames("option")}>
      <div className="label">
        Position
      </div>
      <BalancedItemTray className="styling-button-set" divisionBorderColor="#073044">
        <button className={classnames(getSpecialNodeStyleValue(node,'position') === 'static' && 'active')} onClick={ ()=> onChangeStyle('position', 'static') }>Static</button>
        <button className={classnames(getSpecialNodeStyleValue(node,'position') === 'relative' && 'active')} onClick={ ()=> onChangeStyle('position', 'relative') }>Relative</button>
        <button className={classnames(getSpecialNodeStyleValue(node,'position') === 'absolute' && 'active')} onClick={ ()=> onChangeStyle('position', 'absolute') }>Absolute</button>
        <button className={classnames(getSpecialNodeStyleValue(node,'position') === 'fixed' && 'active')} onClick={ ()=> onChangeStyle('position', 'fixed') }>Fixed</button>
      </BalancedItemTray>

    </div>

    <div className={classnames("option")}>
      <div className="label">
        Display
      </div>
      <BalancedItemTray className="styling-button-set" divisionBorderColor="#073044">
        <button className={classnames(getSpecialNodeStyleValue(node,'position') === 'static' && 'active')} onClick={ ()=> onChangeStyle('position', 'static') }>Static</button>
        <button className={classnames(getSpecialNodeStyleValue(node,'position') === 'relative' && 'active')} onClick={ ()=> onChangeStyle('position', 'relative') }>Relative</button>
        <button className={classnames(getSpecialNodeStyleValue(node,'position') === 'absolute' && 'active')} onClick={ ()=> onChangeStyle('position', 'absolute') }>Absolute</button>
        <button className={classnames(getSpecialNodeStyleValue(node,'position') === 'fixed' && 'active')} onClick={ ()=> onChangeStyle('position', 'fixed') }>Fixed</button>
      </BalancedItemTray>

    </div>
  </div>
}

export default class ChildrenModifier extends Component {
  static propTypes = {
    selectedNode : PropTypes.object,
    directive : PropTypes.object,
    directiveName : PropTypes.string,
    onModifiedDirectiveJSON : PropTypes.func,
    screenWidth: PropTypes.number,
  }

  constructor(props){
    super(props);
  }

  onChangeColumnSize(type, number){
    this.props.selectedNode.props[type] = number;

    this.props.onModifiedDirectiveJSON(this.props.directive.rootNode.exportToJSON(), this.props.selectedNode, this.props.directive.directiveName, this.props.selectedNode.pos);
  }

  onChangeColumnsOfRow(e, i){
    console.log(e, i);
  }

  onChangeStyle(styleName, value){
    this.props.selectedNode.style = this.props.selectedNode.style || {};
    this.props.selectedNode.style[styleName] = value;

    this.props.onModifiedDirectiveJSON(this.props.directive.rootNode.exportToJSON(), this.props.selectedNode, this.props.directive.directiveName, this.props.selectedNode.pos);
  }

  render(){
    // Action
    return <div className="children-modifier">
      {/*<COMMON_OPTIONS node={this.props.selectedNode} screenWidth={this.props.screenWidth} onChangeStyle={:: this.onChangeStyle}/>*/}
      { this.props.selectedNode.tag === 'core-system-column' &&
        <COLUMN_OPTIONS node={this.props.selectedNode} screenWidth={this.props.screenWidth} onChangeColumnSize={:: this.onChangeColumnSize}/> }
      { this.props.selectedNode.tag === 'core-system-row' &&
        <ROW_OPTIONS node={this.props.selectedNode} screenWidth={this.props.screenWidth} onChangeColumnsOfRow={:: this.onChangeColumnsOfRow}/> }
    </div>
  }
}
