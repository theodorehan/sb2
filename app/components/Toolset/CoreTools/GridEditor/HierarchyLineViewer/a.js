import React, {Component} from 'react';
import PropTypes from 'prop-types';

import './style.scss';

export default class HierarchyLineViewer extends Component {
  static propTypes = {
    directiveNodePos : PropTypes.string,
    directiveNode : PropTypes.object,
  }

  constructor(props){
    super(props)
  }

  renderElementNodeBlock(node, _reversedIndex, _isLast){
    let that = this;
    let type = node.getType();
    let style = {
      zIndex : _reversedIndex
    };


    let props = node.props;
    return <li className='tag-based' style={style}>
      {type !== 'html' ? <span className='type'>{type}</span>:''}
      <span className='tagName'>
        {node.tag}
      </span>
      { ( props.id !== undefined && props.id !== '' ) ? <span className='id'>{props.id}</span>:''}
      { ( props.class !== undefined && props.class !== '' ) ? <span className='class'>{props.class}</span>:''}
    </li>
  }

  renderList(){
    if( this.props.directiveNodePos === null ){
      return <li className='placeholder'>
        No Selected
      </li>
    }

    let that = this;
    let parentList = this.props.directiveNode.getParentList();
    let parentCount = parentList.length;
    let renderElementNodeBlocks = [];

    parentList.map(function(_elementNode, _i){
      renderElementNodeBlocks.push(that.renderElementNodeBlock(_elementNode,parentCount - _i));
    });
    renderElementNodeBlocks.push( this.renderElementNodeBlock(this.props.selectedElementNode, 0,true) );
    return renderElementNodeBlocks;
  }

  render(){
    return (
      <div className='ElementHierarchyViewer'>
        <ul>
          {this.renderList()}
        </ul>
      </div>
    )
  }
}

