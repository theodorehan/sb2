
import React, {Component} from 'react';
import PropTypes from 'prop-types';

import './style.scss';

import {
  getDirectiveNodeName,
} from '../../../../../Core/Helper/VueSupports';

export default class HierarchyLineViewer extends Component {
  static propTypes = {
    selectedDirectiveNodePos : PropTypes.string,
    directive : PropTypes.object,
    selectedNode : PropTypes.object,
    onClickNode: PropTypes.func,
  }

  constructor(props){
    super(props);
  }

  render(){
    let linealDescentList = this.props.selectedNode.getLinealDescentList();

    return <div className="hierarchy-line-viewer">
      <ul>
        { linealDescentList.map( ( node, i ) =>
          <li style={{zIndex:linealDescentList.length - i}} key={node.pos}>
            <span
              className="tagName"
              onClick={ (e)=> this.props.onClickNode(e, node) }
              onMouseOver={ (e)=> this.props.onHoverNode(e, node) }
              onMouseOut={ (e)=> this.props.onHoverCancelNode(e, node) }

              onFocus={ (e)=> this.props.onHoverNode(e, node) }
              onBlur={ (e)=> this.props.onHoverCancelNode(e, node) }>

              {getDirectiveNodeName(node)}
            </span>
          </li>)}
      </ul>
    </div>
  }
}
