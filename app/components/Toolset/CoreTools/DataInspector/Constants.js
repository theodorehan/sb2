import defineUIIStatement from '../../../../utils/UII/defineUIIStatement';
import defineActSheet, { Types } from '../../../../utils/UII/actSheet';

export const ToolKey = "DataInspector";
export const Info = {
  type : 'button',
  key : 'DataInspector',
  title : 'Data Inspector',
  iconType : 'fa',
  icon : 'tint',

  iconColor : 'rgb(81,142,171)',
  options: {
    toolKey : ToolKey,
  },


  actionSheet : defineActSheet({
    "action" : "open",
    "type": Types.ToolInterfacePipeline, // ToolInterfacePipeline
    "options" : {
      "toolKey" : ToolKey,
      "areaIndicator" : "[chaining]",
    },
  }),
}


export const UIIStatement = defineUIIStatement(Info);
