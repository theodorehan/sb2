import * as actionTypes from './actionTypes';

export function SPREAD_ITEM(id) {
  return {
    type : actionTypes.SPREAD_ITEM,
    id,
  }
}


export function CANCEL_SPREAD_ITEM(id) {
  return {
    type : actionTypes.CANCEL_SPREAD_ITEM,
    id,
  }
}


export function SPREAD_ALL() {
  return {
    type : actionTypes.SPREAD_ALL,
  }
}


export function SPREAD_EACH() {
  return {
    type : actionTypes.SPREAD_EACH,
  }
}

export function LOAD_HELPER_DATA(key, data){
  return {
    type : actionTypes.LOAD_HELPER_DATA,
    data : data,
    key,
    startedLoading : !data,
  }
}
