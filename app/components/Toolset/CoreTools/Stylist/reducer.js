import * as actionTypes from './actionTypes';


export const initialState = {
  spreadList : {},
  spreadAll : true,
  helperData : {

  },
};

export default function ProjectExplorerReducer(state = initialState, action) {


  switch (action.type) {
    case actionTypes.SPREAD_ALL :
      return Object.assign({}, state, {
        spreadAll : true,
      });

    case actionTypes.SPREAD_EACH :
      return Object.assign({}, state, {
        spreadAll : false,
      });

    case actionTypes.SPREAD_ITEM :
    {
      return Object.assign({}, state, {
        spreadList : Object.assign({}, state.spreadList, {
          [action.id]: true,
        }),
      });
    }
    case actionTypes.CANCEL_SPREAD_ITEM:
    {
      let spreadList = Object.assign({}, state.spreadList);
      delete spreadList[action.id];

      return Object.assign({}, state, {
        spreadList : Object.assign({}, spreadList),
      });
    }

    case actionTypes.LOAD_HELPER_DATA:
      return Object.assign({}, state, {
        helperData : Object.assign({}, state.helperData, {
          [action.key] : Object.assign({}, state.helperData[action.key], {
            isLoading : action.startedLoading,
            data : action.data,
          }),
        }),
      })
    default :
    {
      return state;
    }

  }
}
