import React, {Component} from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';
import {connect} from 'react-redux';
import classnames from 'classnames';
import Promise from 'bluebird';
import $ from 'jquery';

import Tool from '../common/Tool';
import ToolHeader from '../common/ToolHeader';
import ToolFooter from '../common/ToolFooter';
import ToolBody from '../common/ToolBody';
import { Cascader, Input, Switch } from 'antd';
import Toggle from 'material-ui/Toggle';

const {
  TextArea,
} = Input;

import BalancedItemTray from '../../../BalancedItemTray';

import './style.scss';

import {
  Info,
} from './Constants';

import * as actions from './actions';


const toggleStyles = {
  labelStyle : {
    fontFamily: 'Nanum Square',
    fontWeight:'bold',
  },

  thumbOff : {
    backgroundColor: '#fff',
    border: '2px solid #fff',
  },

  trackOff: {
    backgroundColor: 'rgb(189, 189, 189)',
    border: '2px solid rgb(189, 189, 189)',
  },

  thumbSwitched: {
    backgroundColor: '#0076ff',
    border: '2px solid #3975ff',
  },

  trackSwitched: {
    backgroundColor: 'rgb(255, 255, 255)',
    border: '2px solid #3975ff',
  },

  labelStyle : {

  },
};


/**
 * Dialog의 디테일한 사용은 Pages Tool 에서 참고 가능함
 */
@connect((state) => ({
}))
class Dialog extends Component {
  static contextTypes = {
    toolSystem: PropTypes.object,
    elastic: PropTypes.object,
    UIISystems : PropTypes.object,
    zone : PropTypes.object,
    functionStore : PropTypes.object,
  }

  constructor(props) {
    super(props);

    this.inputDoms = {};
  }

  /**
   * onFieldValueChange
   * @param field
   * @return dispatch complete Promise
   */
  onFieldValueChange(field){

    let value;

    if( field.hasOwnProperty('value') ){
      value = field.value;
    } else {
      value = this.inputDoms[field.key].value;
    }

    if( field.filterFunctionKey ){
      let filterFunc = this.context.functionStore.get(field.filterFunctionKey);
      value = filterFunc(value);
    }

    let validateResult;
    if( field.validator ){
      if( field.validator.functionKey ){
        validateResult = this.context.functionStore.get(field.validator.functionKey)(value);
      }
    }

    // Promise 사용함 플래그(마지막 true 파라미터)를 이용해서 dispatch 가 완료되고 상태가 스토어에 반영된 후의 시점을 동기화 한다.
    return this.context.elastic.dispatch(actions.SET_VALUE(field.key, value, validateResult), true);
  }



  onFieldButtonClick(e, field){
    const functionCall = ()=>{
      if( field.functionKey ){

        let func = this.context.functionStore.get(field.functionKey);

        if( typeof func  === 'function' ){
          func(this.props.fields, field);
        } else {
          throw new Error(`${field.functionKey} 함수가 등록되지 않았습니다.`);
        }
      }
    }

    if( field.validate ){
      let {staticData} = this.props;
      let { fields } = staticData;

      let field;
      let fieldDispatchPromise = fields.filter((field)=> field.type === 'input' )
        .map((field)=> this.onFieldValueChange(field));

      Promise.all(fieldDispatchPromise)
        .then(()=>{
          functionCall();
        })
    } else {
      functionCall();
    }
  }


  componentDidMount(){


    let tabableDOMs = ReactDOM.findDOMNode(this).querySelectorAll('.tabable');

    if( tabableDOMs ){
      let lastDom = tabableDOMs[tabableDOMs.length -1];

      if( lastDom ){
        lastDom.focus();
      }
    }
  }

  renderToggle({label, key, filterFunctionKey, placeholder, defaultValue = false , validator, inputType }, i, opt){

    let fieldData = this.props.fields[key] || {};

    return <div className="field toggle" key={i}>
      <Toggle
        label={label + ( (fieldData.value) ? ' (선택됨)' : ' (선택안됨)' ) }
        defaultToggled={defaultValue}

        onToggle={(e, isInputChecked) => this.onFieldValueChange({label, key, filterFunctionKey, validator, value: isInputChecked})}
        thumbStyle={toggleStyles.thumbOff}
        trackStyle={toggleStyles.trackOff}
        thumbSwitchedStyle={toggleStyles.thumbSwitched}
        trackSwitchedStyle={toggleStyles.trackSwitched}
        labelStyle={toggleStyles.labelStyle}
      />
    </div>;
  }

  renderFieldString({label, key, filterFunctionKey, placeholder, defaultValue, validator, inputType }, i, opt){
    let fieldData = this.props.fields[key] || {};


    switch (inputType){

      case 'text':
        return <div className="field input" key={i}>
          { label && <div className="label"> {label} </div> }

          <TextArea
            ref={(dom) => this.inputDoms[key] = dom }
            placeholder={placeholder || ''}
            // value={fieldData.value}
            defaultValue={defaultValue}
            onBlur={(e) => this.onFieldValueChange({label, key, filterFunctionKey, validator, value: e.target.value })}
            autosize={{ minRows: 3, maxRows: 3 }} />


          { fieldData.validation && fieldData.validation.message  && <div className={classnames("validation", fieldData.validation.level || 'error')}>
            { fieldData.validation.message }
          </div>}
        </div>
      case 'string' :
      default:
        return <div className="field input" key={i}>
          { label && <div className="label"> {label} </div> }



          <input
            ref={(dom) => this.inputDoms[key] = dom }
            type="text"
            value={fieldData.value}
            defaultValue={defaultValue}
            onBlur={(e) => this.onFieldValueChange({label, key, filterFunctionKey, validator, value : e.target.value})}
            placeholder={placeholder || ''}/>


          { fieldData.validation && fieldData.validation.message  && <div className={classnames("validation", fieldData.validation.level || 'error')}>
            { fieldData.validation.message }
          </div>}
        </div>
    }
  }

  renderCascaderSelect({label, key, options,filterFunctionKey, validator}, i, opt){
    let fieldData = this.props.fields[key] || {};
    console.log(options);

    return <div className="field input" key={i}>
      { label && <div className="label"> {label} </div> }
      <Cascader options={options} onChange={(value,selectedOptions) => this.onFieldValueChange({label, key, filterFunctionKey, validator, value, selectedOptions})} placeholder="Please select" />


      { fieldData.validation && fieldData.validation.message  && <div className={classnames("validation", fieldData.validation.level || 'error')}>
        { fieldData.validation.message }
      </div>}
    </div>
  }

  renderFieldButtonSet({label, buttons}, i, opt){
    let cellSize = 100 / buttons.length;
    return <div className="field button-set" key={i}>
      { label && <div className="label"> {label} </div> }

      { buttons.map(({label, functionKey, level, validate}, i)=>
          <div
            className="button-wrap "
            key={i}
            style={{width: cellSize + '%'}}>
            <button
              tabIndex={i}
              className={classnames(level, 'tabable')}
              onClick={(e) => this.onFieldButtonClick(e, {label, functionKey, level, validate})}>
              {label}
            </button>
          </div>) }
    </div>
  }


  renderColumns({columns}, i, opt){
    return <div className="row" key={i}>
      {columns.map((col, i) => <div key={i} className="col" style={{ width : col.size + '%' }}>
        { this.renderField(col, i, opt)}
      </div>)}
    </div>
  }


  renderLabel({contents}, i, opt){
    return <div className="field label" key={i}>
      {contents}
    </div>
  }

  renderField( field , i, opt){
    switch( field.type ){
      case 'label' :
        return this.renderLabel(field, i, opt);
      case 'columns' :
        return this.renderColumns(field, i, opt);
      case 'toggle' :
        return this.renderToggle(field, i, opt);
      case "cascader" :
        return this.renderCascaderSelect(field, i,opt);
      case "input" :
        switch(field.inputType){
          case 'string' :
            return this.renderFieldString(field,i,opt);
          case 'text' :
            return this.renderFieldString(field,i,opt);
        }
        return null;
      case "button-set":
        return this.renderFieldButtonSet(field, i,opt);
    }
  }


  render() {
    let {staticData} = this.props;
    let { fields } = staticData;

    return (
      <Tool className="dialog" sizingContentWrap>
        <ToolHeader
          title={staticData.title ||'Dialog'}
          iconType={ Info.iconType }
          icon={ Info.icon }
          iconColor={ Info.iconColor }/>
        <ToolBody nofooter>
          <div className="dynamic-contents">
            { fields.map((field, i) => this.renderField(field, i) )}
          </div>

        </ToolBody>
      </Tool>
    )
  }
}

export default Dialog;
