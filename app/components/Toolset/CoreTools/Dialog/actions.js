import * as actionTypes from './actionTypes';

export function SET_VALUE(key, value, validation) {
  return {
    type : actionTypes.SET_VALUE,
    key,
    value,
    validation,
  }
}
