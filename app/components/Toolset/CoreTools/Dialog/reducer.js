import * as actionTypes from './actionTypes';


export const initialState = {
  fields : {},
};

export default function DialogReducer(state = initialState, action) {

  switch (action.type) {
    case actionTypes.SET_VALUE:
      return Object.assign({}, state, {
        fields : Object.assign({}, state.fields, {
          [action.key] : {
            value : action.value,
            validation : action.validation,
          },
        }),
      });
    default :
      return state;
  }
}
