import defineUIIStatement from '../../../../utils/UII/defineUIIStatement';
import defineActSheet, { Types } from '../../../../utils/UII/actSheet';

export const ToolKey = "Dialog";
export const CreateOpenActionSheet = (areaIndicator) => defineActSheet({
  "action" : "open",
  "type": Types.ToolInterfacePipeline, // ToolInterfacePipeline
  "options" : {
    "toolKey" : ToolKey,
    "areaIndicator" : areaIndicator,
  },
});

export const Info = {
  type : 'button',
  key : 'Dialog',
  title : 'Dialog',
  iconType: 'material',
  icon: 'ActionLightbulbOutline',
  iconColor : 'rgb(239, 215, 20)',
  options: {
    toolKey : ToolKey,
  },

  actionSheet : CreateOpenActionSheet('[chaining]'),
}

export const UIIStatement = defineUIIStatement(Info);
