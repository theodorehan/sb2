import namespaceBindGenerator from '../../../../utils/redux/namespaceBindGenerator';
let defineType = namespaceBindGenerator('Dialog');

export const SET_VALUE = defineType("SET_VALUE");
