import defineUIIStatement from '../../../../utils/UII/defineUIIStatement';
import defineActSheet, { Types } from '../../../../utils/UII/actSheet';

export const ToolKey = "Notifications";
export const Info = {
  type: 'button',
  key: 'Notifications',
  title: 'Notices',
  iconType: 'fa',
  icon: 'circle-o-notch',
  iconColor: '#fff5fc',
  options: {
    toolKey : ToolKey,
  },

  actionSheet: defineActSheet({
    "action": "open",
    "type": Types.ToolInterfacePipeline, // ToolInterfacePipeline
    "options": {
      "toolKey": ToolKey,
      "areaIndicator": "[chaining]",
    },
  }),
};


export const UIIStatement = defineUIIStatement(Info);
