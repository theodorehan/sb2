import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {List, ListItem} from 'material-ui/List';
import ActionInfo from 'material-ui/svg-icons/action/info';

import Tool from '../common/Tool';
import ToolHeader from '../common/ToolHeader';
import ToolFooter from '../common/ToolFooter';
import ToolBody from '../common/ToolBody';

import './Notifications.scss';

import {
  Info,
} from './Constants';

@connect((state) => ({
  Notifications : state.get('Notifications'),
  Studio : state.get('Studio'),
}))
class Notifications extends Component {
  static contextTypes = {
    toolSystem: PropTypes.object,
  }

  static propTypes = {
    Notifications : PropTypes.object,
    Studio : PropTypes.object,
  }

  constructor(props) {
    super(props)
  }

  render() {
    return (
      <Tool className="component-palette">
        <ToolHeader title="Notifications" iconType={ Info.iconType } icon={ Info.icon } iconColor={ Info.iconColor }/>

        <ToolBody>
          <List>
            { this.props.Notifications.messages.map((mess, i) =>
              <ListItem primaryText={mess.desc} key={i} rightIcon={<ActionInfo />} />)
            }
          </List>
        </ToolBody>

        <ToolFooter />
      </Tool>
    )
  }
}

export default Notifications;
