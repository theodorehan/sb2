import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import classnames from 'classnames';
import _ from 'lodash';



import Toggle from 'material-ui/Toggle';
import { Radio, InputNumber, Select, Input,Collapse, Card  } from 'antd';
const RadioGroup = Radio.Group;
const Option = Select.Option;
const { TextArea } = Input;
const Panel = Collapse.Panel;

import Tool from '../common/Tool';
import ToolHeader from '../common/ToolHeader';
import ToolFooter from '../common/ToolFooter';
import ToolBody from '../common/ToolBody';

import Ice2Table from '../../../Ice2Table';
import Ice2ItemViewer from '../../../Ice2ItemViewer';
import Ice2Form from '../../../Ice2Form';


import './style.scss';


import * as actions from './actions';

import {
  Info,
} from './Constants';

import AddIcon from 'material-ui/svg-icons/content/add';
import MinusIcon from 'material-ui/svg-icons/content/remove';
import ArrowDropDownIcon from 'material-ui/svg-icons/navigation/arrow-drop-down';
import ArrowDropUpIcon from 'material-ui/svg-icons/navigation/arrow-drop-up';

import FullmoonBtn from '../../../Unit/FullmoonButton';

@connect((state) => ({
  User : state.get('User'),
  Site : state.get('Site'),
}))
class ICE2Data extends Component {
  static contextTypes = {
    UIISystems : PropTypes.object,
    elastic: PropTypes.object,
    zone : PropTypes.object,
    events : PropTypes.object,
    functionStore : PropTypes.object,
    theme : PropTypes.object,
    global : PropTypes.object,
    ice2 : PropTypes.func,
    layout : PropTypes.object,
    intl: PropTypes.object,
    dataSetFromServicePage : PropTypes.object,
  }


  constructor(props) {
    super(props)


  }

  onClickFooterSelect(){
    if( this.props.staticData.footerBtnCallbacks.select ){
      let func = this.context.functionStore.get(this.props.staticData.footerBtnCallbacks.select);

      let records = this.ice2table.getSelectedRecords();


      func && func(records);
    }
  }

  onClose(){
    if( this.props.staticData.closeHookFunctionKey ){
      let func = this.context.functionStore.get(this.props.staticData.closeHookFunctionKey);

      if( typeof func === 'function' ){
        return func(this);
      }
    }

    return true;
  }

  onClickFooterRegister(){
    // globalEvent 처리..

    let form = this.ice2form;

    form.submit()
      .then((item) => {
        if( this.props.staticData.footerBtnCallbacks.submit ){
          let func = this.context.functionStore.get(this.props.staticData.footerBtnCallbacks.submit);

          func && func(item);
        }
      });
  }

  onClickFooterModify(){
    // globalEvent 처리..

    let form = this.ice2form;

    form.submit()
      .then((item) => {
        if( this.props.staticData.footerBtnCallbacks.submit ){
          let func = this.context.functionStore.get(this.props.staticData.footerBtnCallbacks.submit);

          func && func(item);
        }
      })
  }

  onClickFooterReset(){
    let form = this.ice2form;
    form.reset();
  }

  renderTable() {
    let targetToDefaultSearches = [];

    if( this.props.staticData.target ){
      let targetKeys = Object.keys(this.props.staticData.target);
      let key;
      for(let i =0; i < targetKeys.length; i++ ){
        key = targetKeys[i];

        targetToDefaultSearches.push({
          pid : key,
          value : this.props.staticData.target[key],
          method : 'matching',
          fixed : true,
        })
      }
    }

    // let a ={
    //   pid : 'upperCode',
    //   method : 'matching',
    //   schema : classification.map.upperCode ,
    //   value : codeFilter,
    //   fixed : true,
    // }


    let defaultSearches = [...targetToDefaultSearches,...this.props.staticData.defaultSearches];
    console.log(defaultSearches, this.context.dataSetFromServicePage)
    defaultSearches = defaultSearches.map((search) => ({
        ...search,
        value : this.context.ice2.resolver.resolve(search.value, {}, this.context.dataSetFromServicePage),
      }))


    console.log(defaultSearches);

    return <Ice2Table
      {...this.props.staticData}
      ref={(ice2table) => this.ice2table = ice2table}
      defaultPopupTarget={this.context.layout.layoutType}
      nodeType={this.props.staticData.nodeType}
      tid={this.props.staticData.tid}
      customNodeUx={this.props.staticData.customNodeUx}
      extraListQuery={this.props.staticData.extraListQuery}
      listRepresenter={this.props.staticData.listRepresenter}
      customPropertyUxMap={this.props.staticData.customPropertyUxMap}
      defaultSearches={defaultSearches}
      thumbnailRule={this.props.staticData.thumbnailRule}
      multiSelection={this.props.staticData.multiSelection}
      selectable={this.props.staticData.selectable}/>;
  }

  renderItemViewer() {
    return <Ice2ItemViewer
      {...this.props.staticData}
      withModifyButton
      withDeleteButton
      withLookListButton
      thumbnailRule={this.props.staticData.thumbnailRule}
      customNodeUx={this.props.staticData.customNodeUx}
      customPropertyUxMap={this.props.staticData.customPropertyUxMap}
      defaultPopupTarget={this.context.layout.layoutType}
      nodeType={this.props.staticData.nodeType}
      target={this.props.staticData.target}/>;
  }

  renderForm(){

    if(this.props.staticData.type === 'create'){
      return <Ice2Form
        {...this.props.staticData}
        customNodeUx={this.props.staticData.customNodeUx}
        customPropertyUxMap={this.props.staticData.customPropertyUxMap}
        ref={(c) => this.ice2form = c}
        cloneData={this.props.staticData.cloneData}
        nodeType={this.props.staticData.nodeType}/>
    } else if( this.props.staticData.type === 'modify' ) {
      if( !this.props.staticData.target ) console.error('ice2form needs a staticData.target when modify mode');
      /**
       * modify일때는 target이 필요하다.
       */
      return <Ice2Form
        {...this.props.staticData}
        ref={(c) => this.ice2form = c}
        customPropertyUxMap={this.props.staticData.customPropertyUxMap}
        nodeType={this.props.staticData.nodeType}
        target={this.props.staticData.target}/>
    }
  }


  renderKernel(){
    switch( this.props.staticData.type ){
      case 'list' :
        return this.renderTable();
      case 'itemView' :
        return this.renderItemViewer();
      case 'create' :
        return this.renderForm();
      case 'modify' :
        return this.renderForm();
      default:
        return 'Type not found';
    }
  }

  render() {

    return (
      <Tool className="ice2data">
        <ToolHeader
          title={this.props.staticData.title || `${this.props.staticData.nodeType.typeName} [${this.props.staticData.nodeType.tid}] Data viewer`}
          iconType={ Info.iconType }
          icon={ Info.icon }
          iconColor={ Info.iconColor }
          onCloseHook={ ()=> this.onClose() }>

        </ToolHeader>
        <ToolBody isLoading={this.props.categoryLoading} disableScroll useNativeScroll>

          { this.renderKernel() }
        </ToolBody>

        <ToolFooter>
          { this.props.staticData.selectable && <FullmoonBtn
            shape="round"
            normalStyle={{
              display: 'inline-block',
              height: 30,
              borderWidth: 0,

              borderRadius: 100,
              fontSize: 12,
              color: this.context.theme.Tool.layout.footer.fullmoonBtn.textColor.proceed,
              fontFamily: 'Nanum Square',
              backgroundColor: this.context.theme.Tool.layout.footer.fullmoonBtn.bgColor.proceed,
              margin:10,
              padding:'0 20px',
              textAlign:'left',
            }}
            hoverStyle={{
              borderWidth: 0,
              backgroundColor:this.context.theme.Tool.layout.footer.fullmoonBtn.bgColor.proceedHovered,
            }}
            moonBlender='overlay'
            onClick={(e)=>this.onClickFooterSelect()}>
            {this.context.intl.formatMessage({id : 'app.common.select'})}
          </FullmoonBtn>}


          { ( this.props.staticData.type == 'modify' || this.props.staticData.type == 'create' ) && <FullmoonBtn
            shape="round"
            normalStyle={{
              display: 'inline-block',
              height: 30,
              borderWidth: 0,

              borderRadius: 100,
              fontSize: 12,
              color: this.context.theme.Tool.layout.footer.fullmoonBtn.textColor.danger,
              fontFamily: 'Nanum Square',
              backgroundColor: this.context.theme.Tool.layout.footer.fullmoonBtn.bgColor.danger,
              margin:10,
              padding:'0 20px',
              textAlign:'left',
            }}
            hoverStyle={{
              borderWidth: 0,
              backgroundColor:this.context.theme.Tool.layout.footer.fullmoonBtn.bgColor.dangerHovered,
            }}
            moonBlender='overlay'
            onClick={(e)=>this.onClickFooterReset()}>
            {this.context.intl.formatMessage({id : 'app.common.reset'})}
          </FullmoonBtn>}


          { this.props.staticData.type == 'create' && <FullmoonBtn
            shape="round"
            normalStyle={{
              display: 'inline-block',
              height: 30,
              borderWidth: 0,

              borderRadius: 100,
              fontSize: 12,
              color: this.context.theme.Tool.layout.footer.fullmoonBtn.textColor.proceed,
              fontFamily: 'Nanum Square',
              backgroundColor: this.context.theme.Tool.layout.footer.fullmoonBtn.bgColor.proceed,
              margin:10,
              padding:'0 20px',
              textAlign:'left',
            }}
            hoverStyle={{
              borderWidth: 0,
              backgroundColor:this.context.theme.Tool.layout.footer.fullmoonBtn.bgColor.proceedHovered,
            }}
            moonBlender='overlay'
            onClick={(e)=>this.onClickFooterRegister()}>
            {this.context.intl.formatMessage({id : 'app.common.register'})}
          </FullmoonBtn>}
          { this.props.staticData.type == 'modify' && <FullmoonBtn
            shape="round"
            normalStyle={{
              display: 'inline-block',
              height: 30,
              borderWidth: 0,

              borderRadius: 100,
              fontSize: 12,
              color: this.context.theme.Tool.layout.footer.fullmoonBtn.textColor.proceed,
              fontFamily: 'Nanum Square',
              backgroundColor: this.context.theme.Tool.layout.footer.fullmoonBtn.bgColor.proceed,
              margin:10,
              padding:'0 20px',
              textAlign:'left',
            }}
            hoverStyle={{
              borderWidth: 0,
              backgroundColor:this.context.theme.Tool.layout.footer.fullmoonBtn.bgColor.proceedHovered,
            }}
            moonBlender='overlay'
            onClick={(e)=>this.onClickFooterModify()}>
            {this.context.intl.formatMessage({id : 'app.common.modify'})}
          </FullmoonBtn>}


        </ToolFooter>

      </Tool>
    )
  }
}

export default ICE2Data;
