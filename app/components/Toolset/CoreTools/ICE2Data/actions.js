import * as actionTypes from './actionTypes';

export function SET_CATEGORIES(categories) {
  return {
    type : actionTypes.SET_CATEGORIES,
    categories,
  }
}
