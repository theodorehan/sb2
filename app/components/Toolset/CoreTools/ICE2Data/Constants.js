import defineUIIStatement from '../../../../utils/UII/defineUIIStatement';
import defineActSheet, { Types } from '../../../../utils/UII/actSheet';

export const ToolKey = "ICE2Data";

export const CreateOpenActionSheet = (areaIndicator) => defineActSheet({
  "action" : "open",
  "type": Types.ToolInterfacePipeline, // ToolInterfacePipeline
  "options" : {
    "toolKey" : ToolKey,
    "areaIndicator" : areaIndicator,
  },
});


export const Info = {
  type : 'button',
  key : 'ICE2Data',
  title : 'app.tool.ice2data',
  titleType : 'code',
  iconType: 'material',
  icon: 'ActionFingerprint',
  // iconColor : '#fdfdfd',
  options: {
    toolKey : ToolKey,
  },

  actionSheet : CreateOpenActionSheet('[chaining]'),
}



export const UIIStatement = defineUIIStatement(Info);

export const defaultStaticData = {
  selectable : false,
  multiSelection : false,
  defaultSearches : [],
  defaultSelectedItems : null,
  applyFunctionKey : null,
  tid : null,
  title : null,
  cloneData : null,
  nodeType : null,
  type : null, // list, read, create(form), modify(form)
  footerBtnCallbacks : {
    select : '',
  },

  customNodeUx : {},
  customPropertyUxMap : {},
}
