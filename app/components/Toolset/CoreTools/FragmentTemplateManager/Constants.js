import defineUIIStatement from '../../../../utils/UII/defineUIIStatement';
import defineActSheet, { Types } from '../../../../utils/UII/actSheet';

export const ToolKey = "FragmentTemplateManager";

export const CreateOpenActionSheet = (areaIndicator) => defineActSheet({
  "action" : "open",
  "type": Types.ToolInterfacePipeline, // ToolInterfacePipeline
  "options" : {
    "toolKey" : ToolKey,
    "areaIndicator" : areaIndicator,
  },
});


export const Info = {
  type : 'button',
  key : 'FragmentTemplateManager',
  title : 'app.tool.ice2data',
  titleType : 'code',
  iconType: 'material',
  icon: 'ActionFingerprint',
  // iconColor : '#fdfdfd',
  options: {
    toolKey : ToolKey,
  },

  actionSheet : CreateOpenActionSheet('[chaining]'),
}



export const UIIStatement = defineUIIStatement(Info);

export const defaultStaticData = {
  pageBaseUrl : '',
  title : '',
  directiveName : '',
  versionLoadFunctionKey : '',
  idField : '',
}
