import namespaceBindGenerator from '../../../../utils/redux/namespaceBindGenerator';
let defineType = namespaceBindGenerator('FragmentTemplateManager');

export const SET_CATEGORIES = defineType("SET_CATEGORIES");
