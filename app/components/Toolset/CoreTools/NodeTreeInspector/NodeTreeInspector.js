import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import classnames from 'classnames';
import {Directive} from 'abstract-component-node';


import Tool from '../common/Tool';
import ToolHeader from '../common/ToolHeader';
import ToolFooter from '../common/ToolFooter';
import ToolBody from '../common/ToolBody';

import NodeTree from '../../../NodeTree';

import './style.scss';

import {
CreateOpenActionSheet as CreateCanvasOpenActionSheet,
ToolKey as CanvasToolKey,
} from '../Canvas';


import {
Info,
} from './Constants';

import * as actions from './actions';

import Draggable from '../../../../Core/Components/Draggable';


let NodeIcon = (_props)=>{
  if( _props.node.isDir ){
    return <i className="fa fa-folder"/>;
  }

  switch( _props.node.extension ){
    default :
      return <i className="fa fa-file-text"/>;
  }
}

@connect((state) => ({
  Site : state.get('Site'),
  AssignedStore : state.get('AssignedStore'),
  Elastic : state.get('Elastic'),
}))
class NodeTreeInspector extends Component {
  static contextTypes = {
    elastic: PropTypes.object,
    zone : PropTypes.object,
    events : PropTypes.object,
  }

  static propTypes = {
    spreadList : PropTypes.array,
    spreadAll: PropTypes.bool,
  }


  constructor(props) {
    super(props)


    this.state = {
      mounted : false,
      bodyWidth : 0,
      bodyHeight : 0,
    }


    this.gridNodeDOMList = {};
  }

  componentWillUnmount(){
    console.log('Node Tree unmount');
  }

  componentDidMount(){
    console.log("Node Tree  Mounted");
  }

  onClickNodeSpread(e, node){

    this.context.elastic.dispatch(actions.SPREAD_NODE(node.pos));
  }

  onClickNode(e, node){
    console.log('click',node);
  }

  onMouseEnterNode(e, node){
    console.log('enter',node);
  }

  onMouseLeaveNode(e, node){
    console.log('leave',node);
  }

  toggleNodeVisibility(node){
    console.log('toggle visibility ', node);
  }


  render() {
    // let {staticData, Site} = this.props;
    // let { directivesMap, directiveName, directiveNodePos } = staticData;
    let canvasState = this.context.elastic.getOtherOnlyOneToolState(CanvasToolKey);
    let {   selectedDirectiveName, routeDirectiveName, selectedDirectiveNodePos, routeDirectiveJSONTREE_map, iframeState, combinationMode, bodyHTMLDirectiveNode } = canvasState;
    let {screenWidth, totalWidth, totalHeight} = iframeState;

    let directive, selectedNode;
    let directiveNode;
    if( !combinationMode ){
      directive = new Directive(selectedDirectiveName, routeDirectiveJSONTREE_map[selectedDirectiveName || routeDirectiveName]);
      directiveNode = directive.rootNode;
      //
      // selectedNode = directive.rootNode.findByLocation(selectedDirectiveNodePos);
    } else {
      directiveNode = bodyHTMLDirectiveNode;
    }


    return (
      <Tool className="node-tree-inspector">
        <ToolHeader
          title="Tree Inspector"
          iconType={ Info.iconType }
          icon={ Info.icon }
          iconColor={ Info.iconColor }/>
        <ToolBody ref={(toolBody) => { this.toolBodyComp = toolBody } }>
          <NodeTree
            node={directiveNode}
            spreadList={this.props.spreadList}
            spreadAll={this.props.spreadAll}
            getName={getTagName}
            getNodeId={getNodeId}
            onClickSpread={:: this.onClickNodeSpread}
            onClickNode={:: this.onClickNode }
            onMouseEnterNode={:: this.onMouseEnterNode}
            onMouseLeaveNode={:: this.onMouseLeaveNode}
            getNodeOptions={ (node)=> <i className="node-option fa fa-eye" onClick={ () => this.toggleNodeVisibility(node) }/> }/>

        </ToolBody>
        <ToolFooter/>
      </Tool>
    )
  }
}

export default NodeTreeInspector;

function getTagName(node){

  switch(node.tag){
    case "core-system-grid": return 'grid';
    case "core-system-row": return 'row';
    case "core-system-column": return 'column';
    case "core-system-layer": return 'layer';
  }
  return node.tag;
}

function getNodeId(node){
 return node.pos;
}
