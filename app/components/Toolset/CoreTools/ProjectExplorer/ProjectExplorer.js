import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import classnames from 'classnames';
import TextField from 'material-ui/TextField';
import Toggle from 'material-ui/Toggle';

import Tool from '../common/Tool';
import ToolHeader from '../common/ToolHeader';
import ToolFooter from '../common/ToolFooter';
import ToolBody from '../common/ToolBody';

import './style.scss';


import Draggable from '../../../../Core/Components/Draggable';

import {
  Info,
} from './Constants';

import * as actions from './actions';


let Node = (_props) => <div className="node">
  <div className="item">
    <div className="padding">

    </div>
    { _props.name || _props.node.name }
  </div>
</div>;

let Group = () => <div>

</div>;


let NodeIcon = (_props)=>{
  if( _props.node.isDir ){
    return <i className="fa fa-folder"/>;
  }

  switch( _props.node.extension ){
    default :
      return <i className="fa fa-file-text"/>;
  }
}


@connect((state) => ({
  Site : state.get('Site'),
  AssignedStore : state.get('AssignedStore'),
}))
class ProjectExplorer extends Component {
  static contextTypes = {
    toolSystem: PropTypes.object,
    elastic: PropTypes.object,
    // AssignedStore : PropTypes.object,
    // Site : PropTypes.object,
  }

  constructor(props) {
    super(props)
  }

  onSearchInput(_e, _val) {

  }


  renderNode(_node, _indentCount, _i, _name){
    var self = this;


    let isDir = _node.isDir;

    return (
      <li className='item' key={_i}>
        <span className='indent' style={{width:_indentCount*25}}/>

        { isDir ?
          <span className='node-show-sub'>
            <i className="fa fa-caret-down"/>
          </span> : ''}
        <span className='node-icon'>
          <NodeIcon node={_node}/>
        </span>
        <span className='node-name'>
          { _name || _node.name }
        </span>
        <span className='node-option'>

        </span>
        <span className='node-tid'>

        </span>
      </li>
    )
  }

  renderNodeTypeList(_node, _indent, _i, _name){
    var self = this;

    let childrenContainer = null;

    if( _node.isDir && _node.children.length > 0 ){
      let sortedChildren = _node.children.sort((_a,_b)=>{
        if( _a.isDir && _b.isDir ){
          if( _a.name > _b.name ){
            return 1;
          } else {
            return -1;
          }
        }

        if( _a.isDir ){
          return -1;
        }

        if( _b.isDir ){
          return 1;
        }

      });
      childrenContainer = <li className='has-children'>
        {_node.children.map(function (_child, _i) {
          return self.renderNodeTypeList(_child, _indent + 1, _i)
        })}
      </li>;
    }

    return (
      <ul className="node" key={_i}>
        { this.renderNode(_node, _indent, _i, _name)}
        { childrenContainer }
      </ul>
    );
  }




  render() {
    let {AssignedStore, Site} = this.props;
    let { mainStoreRoot } = AssignedStore;
    return (
      <Tool className="project-explorer">
        <ToolHeader
          title="Project Explorer"
          iconType={ Info.iconType }
          icon={ Info.icon }
          iconColor={ Info.iconColor }/>

        <ToolBody nofooter>
          <div className='node-tree-wrapper'>
            {this.renderNodeTypeList(mainStoreRoot, 0,0,Site.info.siteName) }
          </div>

        </ToolBody>
      </Tool>
    )
  }
}

export default ProjectExplorer;
