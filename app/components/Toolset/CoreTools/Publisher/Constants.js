import defineUIIStatement from '../../../../utils/UII/defineUIIStatement';
import defineActSheet, { Types } from '../../../../utils/UII/actSheet';

export const ToolKey = "Publisher";
export const Info = {
  type: 'button',
  key: 'Publisher',
  title : 'app.publish.verb',
  titleType : 'code',
  tooltip : 'app.publish.tooltip',
  tooltipType : 'code',
  iconType: 'material',
  icon: 'ActionLanguage',
  // iconColor: '#ce2348',
  options: {
    toolKey : ToolKey,
  },

  actionSheet: defineActSheet({
    "action": "open",
    "type": Types.ToolInterfacePipeline, // ToolInterfacePipeline
    "options": {
      "toolKey": ToolKey,
      "areaIndicator": "[modal]",
      width:500,
      height:400,
    },
  }),
};


export const UIIStatement = defineUIIStatement(Info);

