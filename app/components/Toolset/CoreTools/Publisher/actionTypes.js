import namespaceBindGenerator from '../../../../utils/redux/namespaceBindGenerator';
let defineType = namespaceBindGenerator('Publisher');


export const SET_POLICY = defineType('SET_POLICY');
export const SET_NODE_STATES = defineType('SET_NODE_STATES');

export const NEW_NODE = defineType('NEW_NODE');

export const SELECT_NODE_PORT = defineType('SELECT_NODE_PORT');

export const SELECT_NODE_LEVEL = defineType('SELECT_NODE_LEVEL');
