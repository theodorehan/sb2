import * as actionTypes from './actionTypes';

export const initialState = {
  policy : {

  },

  nodeStates : [
    // {
    //   port : 0,
    //   published : false,
    //   level : n,
    // }
  ],
};

export default function Publisher(state = initialState, action) {

  switch (action.type) {
    case actionTypes.SET_POLICY :
      return Object.assign({}, state, {
        policy : action.policy,
      });
    case actionTypes.SET_NODE_STATES :
      return Object.assign({}, state, {
        nodeStates : action.states,
      });
    case actionTypes.NEW_NODE :
      return Object.assign({}, state, {
        nodeStates : [...(state.nodeStates || []), {}],
      })
    case actionTypes.SELECT_NODE_PORT:

      return Object.assign({}, state, {
        nodeStates : state.nodeStates.map((nodeState, i)=>{
          if( i == action.index ){
            return {
              ...nodeState,
              port : action.port,
            }
          }
          return nodeState;
        }),
      })
    case actionTypes.SELECT_NODE_LEVEL:

      return Object.assign({}, state, {
        nodeStates : state.nodeStates.map((nodeState, i)=>{
          if( i == action.index ){
            return {
              ...nodeState,
              level : action.level,
            }
          }
          return nodeState;
        }),
      })
    default :
      return state;
  }
}
