import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import {connect} from 'react-redux';
import TextField from 'material-ui/TextField';
import range from 'lodash/range';

import Tool from '../common/Tool';
import ToolHeader from '../common/ToolHeader';
import ToolFooter from '../common/ToolFooter';
import ToolBody from '../common/ToolBody';
import './Publisher.scss';
import {Info} from './Constants';

import FullmoonBtn from '../../../Unit/FullmoonButton';
import {
  getPolicy,
  getStates,
} from '../../../../Core/Network/Publish';

import * as actions from './action';

// Ant components
import { Form, Icon, Input, Button, Radio, Select } from 'antd';
const FormItem = Form.Item;
const Option = Select.Option;

@connect((state) => ({
  User: state.get('User'),
  Site : state.get('Site'),
}))
class Publisher extends Component {
  static propTypes = {
    policy : PropTypes.object,
    nodeStates : PropTypes.array,
  }

  static contextTypes = {
    elastic : PropTypes.object,
    theme : PropTypes.object,
    intl : PropTypes.object,
    io : PropTypes.object,
  }

  constructor(props) {
    super(props);

  }

  gettingStarted(state){
    if( !state.port ) return alert('Select port');
    if( !state.level ) return alert('Select level');

    this.context.io.publish(state)
      .then(()=>{
        alert('완료');
      })
  }

  onAddNewPublishNode(){
    this.context.elastic.dispatch(actions.NEW_NODE());
  }

  onChangeNodePort(port, index){
    this.context.elastic.dispatch(actions.SELECT_NODE_PORT(port, index));
  }

  onChangeNodeLevel(level, index){
    this.context.elastic.dispatch(actions.SELECT_NODE_LEVEL(level, index));
  }

  componentDidMount(){
    getPolicy(this.props.User.session.id, this.props.Site.info.id, this.props.Site.info.owner)
      .then((res)=>{
        let {policy} = res.data;

        this.context.elastic.dispatch(actions.SET_POLICY(policy));

        getStates(this.props.User.session.id, this.props.Site.info.id, this.props.Site.info.owner)
          .then((res)=>{
            let {states} = res.data;

            this.context.elastic.dispatch(actions.SET_NODE_STATES(states));
          });
      });
  }

  /**
   *
   service_publish_default_method=remote-docker ; or ssh no docker
   service_publish_default_method_fix=true
   service_publish_default_docker_host=125.131.88.206
   service_publish_default_docker_host_fix=true
   service_publish_default_docker_port=2376
   service_publish_default_docker_port_fix=true
   service_publish_default_docker_ca_file=../secret/ca.pem ; or absolute path
   service_publish_default_docker_ca_file_fix=true
   service_publish_default_docker_cert_file=../secret/cert.pem ; or absolute path
   service_publish_default_docker_cert_file_fix=true
   service_publish_default_docker_key_file=../secret/key.pem ; or absolute path
   service_publish_default_docker_key_file_fix=true
   service_publish_default_operation_worker_port=3000
   service_publish_default_operation_worker_port_fix=true
   service_publish_default_operation_port=3090
   service_publish_default_operation_port_fix=false
   * @returns {XML}
   */
  renderPublishing(state, i){
    return <div className="publishing-entity" key={i}>
      <div className="head">
        <span className="ui-item">
          <span className="label">
             Port :
          </span>

          <Select
            showSearch
            value={state.port}
            style={{ width: 100 }}
            placeholder="Select a person"
            optionFilterProp="children"
            onChange={(value) => this.onChangeNodePort(value, i)}>
            {
              this.props.policy.enabledPorts.map((port) => <Option value={port + ''} key={port}>{port}</Option>)
            }
          </Select>
        </span>

        <span className="ui-item">

           <span className="label">
             Level :
          </span>
          <Select
            showSearch
            value={state.level}
            style={{ width: 100 }}
            placeholder="Select a person"
            optionFilterProp="children"
            onChange={(value) => this.onChangeNodeLevel(value, i)}>

            <Option value='dev'>Dev</Option>
            <Option value='stage'>Stage</Option>
            <Option value='prod'>Production</Option>
          </Select>
        </span>

        <span className="ui-item">
          <FullmoonBtn
            shape="round"
            normalStyle={{
              display: 'inline-block',
              height: 30,
              borderWidth: 0,

              borderRadius: 100,
              fontSize: 12,
              color: this.context.theme.Tool.layout.footer.fullmoonBtn.textColor.default,
              fontFamily: 'Nanum Square',
              backgroundColor: this.context.theme.Tool.layout.footer.fullmoonBtn.bgColor.default,
              margin:10,
              padding:'0 20px',
              textAlign:'left',
            }}
            hoverStyle={{
              borderWidth: 0,
              backgroundColor:this.context.theme.Tool.layout.footer.fullmoonBtn.bgColor.hovered,
            }}
            moonBlender='overlay'
            onClick={(e)=>this.gettingStarted(state)}>
            {this.context.intl.formatMessage({id:'app.publish.verb'})}
          </FullmoonBtn>
        </span>
      </div>

      <div className="details">

      </div>
    </div>
  }


  render() {
    return (
      <Tool className="publisher">
        <ToolHeader
          title={this.context.intl.formatMessage({id:'app.publish.verb'})}
          iconType={ Info.iconType } icon={ Info.icon }
          iconColor={ Info.iconColor }/>

        <ToolBody>
          {this.props.nodeStates.map((state, i) => this.renderPublishing(state, i))}


        </ToolBody>
        <ToolFooter>
          <FullmoonBtn
            shape="round"
            normalStyle={{
              display: 'inline-block',
              height: 30,
              borderWidth: 0,

              borderRadius: 100,
              fontSize: 12,
              color: this.context.theme.Tool.layout.footer.fullmoonBtn.textColor.default,
              fontFamily: 'Nanum Square',
              backgroundColor: this.context.theme.Tool.layout.footer.fullmoonBtn.bgColor.default,
              margin:10,
              padding:'0 20px',
              textAlign:'left',
            }}
            hoverStyle={{
              borderWidth: 0,
              backgroundColor:this.context.theme.Tool.layout.footer.fullmoonBtn.bgColor.hovered,
            }}
            moonBlender='overlay'
            onClick={(e)=>this.onAddNewPublishNode()}>
            {this.context.intl.formatMessage({id:'app.publish.add-new-node'})}
          </FullmoonBtn>
        </ToolFooter>
      </Tool>
    )
  }
}


export default Publisher;
