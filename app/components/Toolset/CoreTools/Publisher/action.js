import * as ActionTypes from './actionTypes';

export function SET_POLICY(policy){
  return {
    type : ActionTypes.SET_POLICY,
    policy,
  }
}

export function SET_NODE_STATES(states){
  return {
    type : ActionTypes.SET_NODE_STATES,
    states,
  }
}

export function NEW_NODE(){
  return {
    type : ActionTypes.NEW_NODE,
  }
}

export function SELECT_NODE_PORT(port, index){
  return {
    type : ActionTypes.SELECT_NODE_PORT,
    port,
    index,
  }
}

export function SELECT_NODE_LEVEL(level, index){
  return {
    type : ActionTypes.SELECT_NODE_LEVEL,
    level,
    index,
  }
}
