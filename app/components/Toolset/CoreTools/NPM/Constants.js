import defineUIIStatement from '../../../../utils/UII/defineUIIStatement';
import defineActSheet, { Types } from '../../../../utils/UII/actSheet';

export const ToolKey = "NPM";
export const Info = {
  type : 'button',
  key : 'NPM',
  title : 'NPM',
  iconType: 'img',
  icon: '/public/images/npm-logo.png',
  iconColor : 'rgb(239, 215, 20)',
  options: {
    toolKey : ToolKey,
  },

  actionSheet : defineActSheet({
    "action" : "open",
    "type": Types.ToolInterfacePipeline, // ToolInterfacePipeline
    "options" : {
      "toolKey" : ToolKey,
      "areaIndicator" : "[chaining]",
    },
  }),
}

export const UIIStatement = defineUIIStatement(Info);
