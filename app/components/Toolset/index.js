import * as CoreTools from './CoreTools';

export * as CoreTools from './CoreTools';
export const ToolAccessMap = {
  ...CoreTools.ToolAccessMap,
};
