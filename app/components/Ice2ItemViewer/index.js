import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import Ice2Display, {TABLE_STYLE_SUPPORT} from '../ICE2Elements/Display';
import Ice2Table from '../Ice2Table';
import LaunchIcon from 'material-ui/svg-icons/action/launch';
import ListIcon from 'material-ui/svg-icons/action/list';
import PencilIcon from 'material-ui/svg-icons/content/create';
import DeleteIcon from 'material-ui/svg-icons/content/delete-sweep';

import './style.scss';
import {Tooltip} from 'antd';

export default class Ice2ItemViewer extends Component {
  static propTypes = {
    nodeType : PropTypes.object.isRequired,
    target : PropTypes.object.isRequired,
    style : PropTypes.object,
    withDetachToLayerButton : PropTypes.bool,
    withLookListButton : PropTypes.bool,
    withModifyButton : PropTypes.bool,
    withDeleteButton : PropTypes.bool,
    functionBtnAlign : PropTypes.oneOf(['left', 'right']),
    defaultPopupTarget : PropTypes.string,
  };

  static contextTypes = {
    UIISystems : PropTypes.object,
    ice2 : PropTypes.func,
    intl : PropTypes.object,
    functionStore : PropTypes.object,
  }

  static defaultProps = {
    functionBtnAlign : 'right',
  }

  constructor(props){
    super(props);

    this.state = {
      item : null,
    }
  }

  loadData(){

    return this.context.ice2.get(`/api/node/${this.props.nodeType.tid}/read.json`, {
      params : {
        ...this.props.target,
        includeReferenced : true,
      },
    })
      .then((res)=>{
        let {data} = res;
        console.log(data)

        this.setState({
          item : data.item,
        });
      })
  }

  layerOpen(){

      this.context.UIISystems.openTool('ICE2Data', this.props.defaultPopupTarget || '[layer]',{
        width:500,
        height:600,
        UNIQUE_LAYER : true,
      }, {
        nodeType : this.props.nodeType,
        type : 'itemView',
        target : this.props.target,
      });
  }

  listOpen(){

    this.context.UIISystems.openTool('ICE2Data', this.props.defaultPopupTarget || '[layer]',{
      width:768,
      height:600,
      UNIQUE_LAYER : true,
    }, {
      nodeType : this.props.nodeType,
      type : 'list',
      target : this.props.target, // 해당 아이템 숏컷
    });
  }

  modify(){

    let submitCallbackKey = this.context.UIISystems.getUnique();
    this.context.UIISystems.openTool('ICE2Data', this.props.defaultPopupTarget || '[layer]',{
      width:500,
      height:600,
      UNIQUE_LAYER : true,
    }, {
      nodeType : this.props.nodeType,
      type : 'modify',
      target : this.props.target,
      footerBtnCallbacks : {
        'submit' : submitCallbackKey,
      },
    }).then((closer) => {
      this.context.functionStore.register(submitCallbackKey, (item) => {
        closer();
        this.loadData();
      });
    })
  }

  delete(){
    alert('아직');
  }

  componentDidMount(){



   this.loadData();
  }

  renderField(prop){
    return <Ice2Display
      defaultPopupTarget={this.props.defaultPopupTarget}
      renderAsTableRow
      withLabel
      propertyType={prop}
      key={prop.pid}
      pid={prop.pid}
      tid={this.props.nodeType.tid}
      data={this.state.item[prop.pid]}
      style={{borderBottom: '1px solid #e6e6e6'}}/>
  }



  render(){


    return <div className="ice2-item-viewer" style={{...this.props.style}}>
      <div className={ classnames("title", this.props.functionBtnAlign ) }>
        <span className="type-name">
          {this.props.nodeType.typeName}
        </span>
        <span className="tid">
          {this.props.nodeType.tid}
        </span>

        { this.props.withLookListButton && <Tooltip title={this.context.intl.formatMessage({id:'app.ice2item-viewer.lookList'})}>
          <button className="launch-layer" onClick={::this.listOpen}>
            <ListIcon style={{color:'inherit'}}/>
          </button>
        </Tooltip>}

        { this.props.withDetachToLayerButton && <Tooltip title={this.context.intl.formatMessage({id:'app.ice2item-viewer.openToLayer'})}>
          <button className="launch-layer" onClick={::this.layerOpen}>
            <LaunchIcon style={{color:'inherit'}}/>
          </button>
        </Tooltip>}

        { this.props.withModifyButton && <Tooltip title={this.context.intl.formatMessage({id:'app.ice2item-viewer.modify'})}>
          <button className="launch-layer" onClick={::this.modify}>
            <PencilIcon style={{color:'inherit'}}/>
          </button>
        </Tooltip>}

        { this.props.withDeleteButton && <Tooltip title={this.context.intl.formatMessage({id:'app.ice2item-viewer.delete'})}>
          <button className="launch-layer" onClick={::this.delete}>
            <DeleteIcon style={{color:'inherit'}}/>
          </button>
        </Tooltip>}


      </div>
      <div className="">

        <div style={TABLE_STYLE_SUPPORT}>
        { this.state.item && this.props.nodeType.propertyTypes.map((prop) => this.renderField(prop)) }
        </div>
      </div>
    </div>
  }
}
