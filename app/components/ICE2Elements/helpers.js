import {VALUE_TYPES_ARRAY, FLAGS} from "./constants"

export function getIdableProperty(propertyTypes) {

  for(let i = 0; i < propertyTypes.length; i++ ){
    if( propertyTypes[i].idable ){
      return propertyTypes[i].pid;
    }
  }
}

export function getLabelableProperties(propertyTypes) {
  let props = [];

  for(let i = 0; i < propertyTypes.length; i++ ){
    if( propertyTypes[i].labelable ){
      props.push(propertyTypes[i].pid);
    }
  }

  return props;
}


/**
 *
 * @param propertyTypes
 * @returns {{
 * map: {},
 * primaryProperty: {},
 * IDABLE:[],
 * TREEABLE : [],
 * INDEXABLE:[],
 * LABELABLE:[],
 * STRING:[],
 * DATE:[],
 * INT:[],
 * LONG:[],
 * DOUBLE:[],
 * TEXT:[],
 * FILE:[],
 * CODE:[],
 * REFERENCE:[],
 * REFERENCES:[],
 * REFERENCED:[],
 * BOOLEAN:[],
 * }}
 */
export function classifyProperties(propertyTypes) {
  let classification = {
    [FLAGS.IDABLE] : [],
    [FLAGS.TREEABLE]  : [],
    [FLAGS.INDEXABLE]  : [],
    [FLAGS.LABELABLE]  : [],
    map : {},
    primaryProperty : null,
    FIRST_LABELABLE : null,
  };

  for(let i = 0; i < VALUE_TYPES_ARRAY.length; i++ ){
    classification[VALUE_TYPES_ARRAY[i]] = [];
  }

  classification.UNKNOWN = [];

  let propType, valueType;
  for(let i = 0; i < propertyTypes.length; i++ ){
    propType = propertyTypes[i];

    if( propType.valueType ){
      valueType = propType.valueType.value.toUpperCase();
    } else {
      valueType = 'UNKNOWN';
    }

    if( propType.idable ){
      classification[FLAGS.IDABLE].push(propType);

      // idable 은 여러개일 수 있다.
      // 그중에 reference 된 idable 은 외래키이며
      // reference 하지 않는 property가 기본키로 사용 될 것이다.
      if( !propType.referenceType ){
        classification.primaryProperty = propType;
      }
    }

    if( propType.labelable ){
      classification[FLAGS.LABELABLE].push(propType);
    }

    if( propType.treeable ){
      classification[FLAGS.TREEABLE].push(propType);
    }

    if( propType.indexable ){
      classification[FLAGS.INDEXABLE].push(propType);
    }

    classification[valueType].push(propType);

    classification.map[propType.pid] = propType;
  }

  return classification;
}
