import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Upload, Icon} from 'antd';
import FilePreview from './FilePreview';

const Dragger = Upload.Dragger;


export default class FileStage extends Component {
  static contextTypes = {
    intl : PropTypes.object,
    UIISystems : PropTypes.object,
    functionStore : PropTypes.object,
  };

  static propTypes = {
    isMulti : PropTypes.bool,
    files : PropTypes.array,
    onChange : PropTypes.func,
    iceUrl: PropTypes.string,
  };

  static defaultProps = {
    files : [],
  };

  constructor(){
    super();

  }

  onChange(v){
    console.log(v);
  }

  onBeforeUpload(file){
    if( this.props.isMulti ){
      this.props.onChange([
        ...this.props.files,
        file,
      ]);
    } else {
      this.props.onChange([
        file,
      ]);
    }

    return false;
  }


  onSize(file, width,height){
    this.props.onChange(this.props.files, { size : { width, height}});
  }

  onClickClose(i){




    let okBtn = this.context.UIISystems.getUnique();
    let noBtn = this.context.UIISystems.getUnique();
    this.context.UIISystems.openTool('Dialog', this.props.defaultPopupTarget || '[modal]',{
      width:500,
      height:100,
    }, {
      title : "Do you really delete a item?",
      fields : [
        {
          type : 'button-set',
          buttons : [
            {
              label : 'Delete',
              functionKey : okBtn ,
              level : 'positive',
            },
            {
              label : 'Cancel' ,
              functionKey : noBtn,
              level : 'negative',
            },
          ],
        },
      ],
    }).then((closer) => {
      this.context.functionStore.register(okBtn, ()=> {


        this.props.onChange([
          ...this.props.files.slice(0,i),
          ...this.props.files.slice(i+1),
        ]);


        closer();
      });

      this.context.functionStore.register(noBtn, () => {
        closer();
      })
    })

  }




  renderPreviewArea(){


    return this.props.files.filter((file) => file && typeof file == 'object').map((file, i) => <FilePreview
      iceUrl={this.props.iceUrl}
      key={i}
      className=""
      file={file}
      onSize={(file, w,h) => this.onSize(file, w,h)}
      onClickClose={()=> this.onClickClose(i) }/>)
  }

  render(){
    return <div className="file-stage">
      { <Dragger
        className='drop-zone'
        onChange={:: this.onChange}
        beforeUpload={:: this.onBeforeUpload} >
        <p className="ant-upload-drag-icon">
          <Icon type="inbox" />
        </p>
        <p className="ant-upload-text">{ this.context.intl.formatMessage({id : "app.fileStage.area-click-or-drag"})}</p>
      </Dragger>}
      { this.renderPreviewArea() }
    </div>
  }
}
