import React, {Component} from 'react';
import PropTypes from 'prop-types';
import ContentLoader, { Rect, Circle } from 'react-content-loader';
import moment from 'moment';
import numeral from 'numeral';
import SearchIcon from 'material-ui/svg-icons/action/search';
import './style.scss';
import {VALUE_TYPES} from "../constants";
import classnames from 'classnames';
import {Popover, Input, DatePicker, Select} from 'antd';
const Option = Select.Option;
const { TextArea } = Input;
import Toggle from 'material-ui/Toggle';
import get from 'lodash/get';
import AceEditor from 'react-ace';

import 'brace/mode/json';
import 'brace/mode/javascript';
import 'brace/mode/html';
import 'brace/theme/github';

import Ice2ItemViewer from '../../Ice2ItemViewer';

import Ice2ReferenceSelect from './ReferenceSelect';
import FileStage from './FileStage';

export default class ICE2Input extends Component {

  static propTypes = {
    propertyType : PropTypes.object,

    // 노드 설정 UX보다 우선 순위가 높다
    customUx : PropTypes.object,
    tid : PropTypes.string.isRequired,
    pid : PropTypes.string.isRequired,
    style : PropTypes.object,
    className : PropTypes.string,
    value : PropTypes.any,
    withLabel : PropTypes.bool,
    labelPosition : PropTypes.oneOf(['left', 'top', 'right']),
    column : PropTypes.number,
    fluid : PropTypes.bool.isRequired,

    renderAsTableRow : PropTypes.bool,

    divideLine : PropTypes.bool,
    divideLineColor : PropTypes.string,
    divideLinePos : PropTypes.oneOf(['left']),

    dataMaxWidth : PropTypes.number,

    onChange : PropTypes.func,
    ownNodeItemObject : PropTypes.object,

    iceUrl : PropTypes.string,

    isHidden : PropTypes.bool,
  }

  static defaultProps = {
    labelPosition : 'left',
    fluid : true,
    dataMaxWidth : 768,
  }

  static contextTypes = {
    UIISystems : PropTypes.object,
    ice2 : PropTypes.func,
  }

  constructor(props){
    super();

    this.state = {
      propertyType : {...props.propertyType, ux : Object.assign({}, props.propertyType ? props.propertyType.ux : {}, props.customUx) },
      referenceNodeType : null,
      idablePidOfReferenceNodeType : null,
      labelablePidsOfReferencedNodeType : [],
    };
  }


  loadPropertyType(){
    return this.context.ice2.loadPropertyType(this.props.tid, this.props.pid)
      .then((type) => {
        this.setState({
          propertyType : type,
        });

        return type;
      })
  }

  loadReferenceNodeType(propertyType){
    let {
      referenceType,
      // referenceValue,
    } = this.state.propertyType || propertyType;

    this.context.ice2.loadNodeType(referenceType.value)
      .then((type) => {
        let {propertyTypes} = type;

        let idablePidOfReferenceNodeType;
        let labelablePidsOfReferencedNodeType = [];
        for(let i =0; i < propertyTypes.length; i++ ){
          if( propertyTypes[i].idable ){
            idablePidOfReferenceNodeType = propertyTypes[i].pid;
          }
          if( propertyTypes[i].labelable ){
            labelablePidsOfReferencedNodeType.push(propertyTypes[i].pid);
          }
        }

        this.setState({
          referenceNodeType : type,
          idablePidOfReferenceNodeType,
          labelablePidsOfReferencedNodeType,
        });
      })
  }


  getFieldData(){
    switch(this.state.propertyType.valueType && this.state.propertyType.valueType.value.toUpperCase()){
      case VALUE_TYPES.STRING :
        return this.props.value;
      case VALUE_TYPES.TEXT :
        return this.props.value;
      case VALUE_TYPES.JSON :
        return this.props.value;
      case VALUE_TYPES.OBJECT :
        return typeof this.props.value == 'string' ? this.props.value : JSON.stringify(this.props.value);
      case VALUE_TYPES.CODE :
        return this.props.value;
      case VALUE_TYPES.BOOLEAN :
        return this.props.value;
      case VALUE_TYPES.DATE :
        return this.props.value;
      case VALUE_TYPES.FILE :
        return this.props.value;
      case VALUE_TYPES.FILES :
        return this.props.value;
      case VALUE_TYPES.INT :
        return this.props.value;
      case VALUE_TYPES.LONG :
        return this.props.value;
      case VALUE_TYPES.DOUBLE :
        return this.props.value;
      case VALUE_TYPES.REFERENCE :

        if( this.referenceSelect ){

          return this.referenceSelect.getFieldData()[0];
        }

        return typeof this.props.value == 'object' && this.props.value ? this.props.value.value : this.props.value;
      case VALUE_TYPES.REFERENCES :

        if( this.referenceSelect ){

          return this.referenceSelect.getFieldData().join(',');
        }
        return typeof this.props.value == 'object' && this.props.value ? this.props.value.value : this.props.value;
        // REFERENCED 는 저장 안함
      case VALUE_TYPES.REFERENCED :
        return null;
      default :
        return typeof this.props.value == 'object' && this.props.value ? this.props.value.value : this.props.value;
    }
  }

  componentDidMount(){
    if( !this.state.propertyType ){
      return this.loadPropertyType()
        .then((propertyType) => {


          if( this.state.propertyType.valueType.value.toUpperCase() === VALUE_TYPES.REFERENCED ){
            this.loadReferenceNodeType();
          }
        })
    }

    if( this.state.propertyType.valueType && this.state.propertyType.valueType.value.toUpperCase() === VALUE_TYPES.REFERENCED ){
      this.loadReferenceNodeType();
    }
  }

  renderString(){
    return <Input
      placeholder={get(this.state.propertyType, 'ux.valuePlaceholder', '')}
      value={typeof this.props.value == 'object' && this.props.value ? this.props.value.value : this.props.value }
      onInput={(e) => this.props.onChange(this.state.propertyType, e.nativeEvent.target.value) }/>
  }

  renderText(helperObject){



    if( helperObject ){
      if( helperObject.inputType === 'programming' ){
        return <AceEditor
          width="100%"
          height={'200px'}
          mode={helperObject.lang}
          theme="github"
          value={this.props.value || ''}
          onChange={(value, e) => { this.props.onChange(this.state.propertyType, value)}}
          // onValidate={:: this.onValidate }
          name="UNIQUE_ID_OF_DIV"
          editorProps={{$blockScrolling: true}}
        />
      }
    }

    return <TextArea
      placeholder={get(this.state.propertyType, 'ux.valuePlaceholder', '')}
      value={this.props.value}
      onInput={(e) => this.props.onChange(this.state.propertyType, e.nativeEvent.target.value) }/>
  }

  renderJson(){
    return <AceEditor
      width="100%"
      height={'200px'}
      mode={'json'}
      theme="github"
      value={this.props.value || '{}'}
      onChange={(value, e) => { this.props.onChange(this.state.propertyType, value)}}
      // onValidate={:: this.onValidate }
      name="UNIQUE_ID_OF_DIV"
      editorProps={{$blockScrolling: true}}
    />

    return <TextArea
      placeholder={get(this.state.propertyType, 'ux.valuePlaceholder', '')}
      value={this.props.value}
      onInput={(e) => this.props.onChange(this.state.propertyType, e.nativeEvent.target.value) }/>
  }

  renderObject(){
    return <AceEditor
      width="100%"
      height={'200px'}
      mode={'json'}
      theme="github"
      value={ typeof this.props.value === 'string' ? this.props.value || '{}' : JSON.stringify(this.props.value|| '{}') }
      onChange={(value, e) => { this.props.onChange(this.state.propertyType, value)}}
      // onValidate={:: this.onValidate }
      name="UNIQUE_ID_OF_DIV"
      editorProps={{$blockScrolling: true}}
    />
  }

  renderBoolean(){
    return <Toggle
      toggled={this.props.value}
      onToggle={(e, isChecked) => this.props.onChange(this.state.propertyType, isChecked) }/>
  }

  renderInt(){
    return <Input
      placeholder={get(this.state.propertyType, 'ux.valuePlaceholder', '')}
      value={this.props.value}
      onInput={(e) => this.props.onChange(this.state.propertyType, e.nativeEvent.target.value) }
      type="number"/>
  }

  renderDouble(){
    return <Input
      placeholder={get(this.state.propertyType, 'ux.valuePlaceholder', '')}
      value={this.props.value}
      onInput={(e) => this.props.onChange(this.state.propertyType, e.nativeEvent.target.value) }
      type="number"/>
  }

  renderLong(){
    return <Input
      placeholder={get(this.state.propertyType, 'ux.valuePlaceholder', '')}
      value={this.props.value}
      onInput={(e) => this.props.onChange(this.state.propertyType, e.nativeEvent.target.value) }
      type="number"/>
  }

  renderDate(){
    return <DatePicker
      ownNodeItemObject={this.props.ownNodeItemObject}
      value={this.props.value ? moment(this.props.value, 'YYYYMMDDhhmmss') : null}
      onChange={ (momentDate, str) => this.props.onChange(this.state.propertyType, momentDate.format('YYYYMMDDhhmmss')) }
    />
  }

  renderFile(){
    return <FileStage
      ownNodeItemObject={this.props.ownNodeItemObject}
      iceUrl={this.props.iceUrl || this.context.ice2.defaults.baseUrl}
      files={this.props.value ? [this.props.value] : []}
      onChange={(files, info) => this.props.onChange(this.state.propertyType, files[0], info)}/>
  }


  renderFiles(){
    return <FileStage
      ownNodeItemObject={this.props.ownNodeItemObject}
      isMulti
      onChange={(files) => this.props.onChange(this.state.propertyType, files)}/>
  }


  renderReference(){

    if( !this.state.propertyType.referenceType ){
      return <div>
        referenceType is null
      </div>
    }


    return <Ice2ReferenceSelect
      ownNodeItemObject={this.props.ownNodeItemObject}
      ref={ (c) => this.referenceSelect = c}
      onSelects={(prop, value) => this.props.onChange(prop, value[0]) }
      valueLabelSets={ this.props.value ? [this.props.value]:[]}
      propertyType={this.state.propertyType}
      referenceType={this.state.propertyType.referenceType.value}
      referenceValue={this.state.propertyType.referenceValue ? this.state.propertyType.referenceValue.value : null}/>;
  }

  renderReferences(){
    if( !this.state.propertyType.referenceType ){
      return <div>
        referenceType is null
      </div>
    }

    return <Ice2ReferenceSelect
      ownNodeItemObject={this.props.ownNodeItemObject}
      multiple
      ref={ (c) => this.referenceSelect = c}
      onSelects={(prop, value) => this.props.onChange(prop, value) }
      valueLabelSets={ this.props.value ? this.props.value:[]}
      propertyType={this.state.propertyType}
      referenceType={this.state.propertyType.referenceType.value}
      referenceValue={this.state.propertyType.referenceValue ? this.state.propertyType.referenceValue.value : []}/>;
  }

  renderCode(){
    let {
      code : codes= [],
    } = this.state.propertyType;

    return <Select onChange={(value) => this.props.onChange(this.state.propertyType, value)} style={{width:200}}>
      {
        (codes || []).map((code) => <Option key={code.value}>
          { code.label }
        </Option>)
      }
    </Select>;
  }

  renderByValueTypes(){

    if( this.state.propertyType.idType ){
      return <div className="idtype">
        { this.props.value || this.state.propertyType.idType.label }
      </div>
    }


    let {
      ux = {},
    } = this.state.propertyType;

    let helperObject;
    if( ux.helper ){
      helperObject = this.context.ice2.resolver.resolveFromDataAndErrorToNull(ux.helper, {'this':this.props.ownNodeItemObject});
    }

    switch(this.state.propertyType.valueType && this.state.propertyType.valueType.value.toUpperCase()){
      case VALUE_TYPES.STRING :
        return this.renderString(helperObject);
      case VALUE_TYPES.TEXT :
        return this.renderText(helperObject);
      case VALUE_TYPES.JSON :
        return this.renderJson(helperObject);
      case VALUE_TYPES.OBJECT :
        return this.renderObject(helperObject);
      case VALUE_TYPES.CODE :
        return this.renderCode(helperObject);
      case VALUE_TYPES.BOOLEAN :
        return this.renderBoolean(helperObject);
      case VALUE_TYPES.DATE :
        return this.renderDate(helperObject);
      case VALUE_TYPES.FILE :
        return this.renderFile(helperObject);
      case VALUE_TYPES.FILES :
        return this.renderFiles(helperObject);
      case VALUE_TYPES.INT :
        return this.renderInt(helperObject);
      case VALUE_TYPES.LONG :
        return this.renderLong(helperObject);
      case VALUE_TYPES.DOUBLE :
        return this.renderDouble(helperObject);
      case VALUE_TYPES.REFERENCE :
        return this.renderReference(helperObject);
      case VALUE_TYPES.REFERENCES :
        return this.renderReferences(helperObject);
      // case VALUE_TYPES.REFERENCED :
      //   return this.renderReferenced();
      default :
        return this.renderString(helperObject);
    }
  }

  renderLabel(){
    return <div className="label" title={`${this.state.propertyType.pid}:${this.state.propertyType.valueType && this.state.propertyType.valueType.value}`}>
      { this.state.propertyType.required && <span className="required">*</span>}
      <span className="title">{this.state.propertyType.propertyTypeName}</span>
    </div>
  }

  render(){

    if( this.props.isHidden ){
      return <div></div>;
    }

    if( !this.state.propertyType ){
      return <div className={"ice2-input-element" + this.props.className} style={this.props.style}>
        Loading..
      </div>
    }

    return <div
      className={
        classnames(
          "ice2-input-element",
          this.props.withLabel && `with-label label-${this.props.labelPosition}`,
          this.props.className,
          this.props.renderAsTableRow && 'table-row',
        )
      }
      style={this.props.style}>

      { this.props.withLabel ? this.renderLabel() : ''}
      <div className="data" style={{maxWidth : this.props.dataMaxWidth}}>
        { this.renderByValueTypes() }

        { this.state.propertyType &&  this.state.propertyType.ux && this.state.propertyType.ux.comment && <div className="comment">
          {this.state.propertyType.ux.comment}
        </div>}
      </div>
    </div>
  }
}


export const TABLE_STYLE_SUPPORT = {
  display:'table',
  borderCollapse : 'collapse',
  width:'100%',
}
