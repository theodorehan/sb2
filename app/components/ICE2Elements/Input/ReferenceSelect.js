import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Promise from 'bluebird';
import {Popover, Input, DatePicker, Select} from 'antd';
const Option = Select.Option;
import {classifyProperties} from "../helpers";
import {interpretQueryToPropsGroupList} from "../../Ice2PropertyQueryInput";
import {FLAGS} from "../constants";
import uuid from 'uuid';
import debounce from 'lodash/debounce';
import uniqWith from 'lodash/uniqWith';

export default class ReferenceSelect extends Component {
  static propTypes = {
    ownNodeItemObject : PropTypes.object,

    propertyType : PropTypes.object.isRequired,

    onSelects : PropTypes.func,
    valueLabelSets : PropTypes.array,
    multiple : PropTypes.bool,
  }

  static defaultProps = {
    valueLabelSets : [],
  }

  static contextTypes = {
    ice2 : PropTypes.func,
    UIISystems : PropTypes.object,
    intl : PropTypes.object,
    functionStore : PropTypes.object,
    dataSetFromServicePage : PropTypes.object,
  }

  constructor(props){
    super(props);

    this.state = {
      optionItems : [],
      referenceNodeType : null,
      dataLoaded : false,
      loading : false,
      classification : null,
      moreItems : false,
      search : '',
    };


    this.wrappedOnSearch = debounce(this.onSearch.bind(this), 100);
  }

  getFieldData(){

    let {
      codeFilter,
    } = this.props.propertyType;


    if( codeFilter ){
      return this.props.valueLabelSets.map((valueSet) => valueSet.value);
    }

    return this.props.valueLabelSets.map((valueSet) => valueSet.refId);
  }


  loadReferenceNodeType(){
    let {
      referenceType,
    } = this.props.propertyType;


    if( this.state.classification && this.state.referenceNodeType ){
      return Promise.resolve({
        classification : this.state.classification,
        referenceNodeType :this.state.referenceNodeType,
      });
    }


    return this.context.ice2.loadNodeType(referenceType.value)
      .then((referenceNodeType) => {
        let classification = classifyProperties(referenceNodeType.propertyTypes);


        this.setState({
          referenceNodeType,
          classification,
        });

        return {
          referenceNodeType,
          classification,
        }
      });
  }

  loadData(fixParams){

    this.setState({
      loading : true,
    });


    let {
      codeFilter,
      ux,
    } = this.props.propertyType;


    this.loadReferenceNodeType()
      .then(({ referenceNodeType, classification }) => {
        let params = {
          ...fixParams,
        };

        // 임시 암묵적 약속에 따라 지정한다.
        if( codeFilter ){
          if( classification.map.upperCode ){
            params.upperCode = codeFilter;
          }
        }

        if( ux ){

          if( ux.logicFilter ){
            let logicFilterO = this.context.ice2.resolver.resolveFromDataAndErrorToNull(
              ux.logicFilter, {
                'this':this.props.ownNodeItemObject,
                ...this.context.dataSetFromServicePage,
              });


            if( logicFilterO ){
              let filterKeys = Object.keys(logicFilterO);
              for(let i =0; i < filterKeys.length; i++ ){
                params[filterKeys[i]] = logicFilterO[filterKeys[i]];
              }
            }
          }
        }

        this.context.ice2.get(`/node/${referenceNodeType.tid}/list.json`, {
          params,
        }).then((res) => {
          let {
            items,
            resultCount,
            totalCount,
          } = res.data;

          let moreItems =false;
          if( resultCount < totalCount ){
            moreItems = true;
          }


          let idPid;
          if( this.state.classification.primaryProperty ){
            idPid = this.state.classification.primaryProperty.pid;
          }

          let labelPid;
          if( this.state.classification[FLAGS.LABELABLE].length ){
            labelPid = this.state.classification[FLAGS.LABELABLE][0].pid;
          }


          if( moreItems ){
            let searchItem = items.find((item) => {
              if( item[labelPid] ){
                if( item[labelPid].label == this.state.search ){
                  return true;
                }
              }

              if( item[idPid] == this.state.search ){
                return true;
              }
            });

            if( searchItem ){
              this.setState({
                optionItems : items || [],
                dataLoaded : true,
                loading:false,
                moreItems : true,
              });
            } else {

              this.context.ice2.get(`/node/${referenceNodeType.tid}/list.json`, {
                params : {
                  ...params,
                  [labelPid] : this.state.search,
                },
              }).then((res) => {
                let {
                  items : searchItems,
                  // resultCount,
                  // totalCount,
                } = res.data;





                this.setState({
                  // 중복 제거
                  optionItems : uniqWith(items, searchItems, function(arrVal, othVal){
                    return arrVal[idPid] == othVal[idPid];
                  }),
                  dataLoaded : true,
                  loading:false,
                  moreItems : true,
                });
              })
            }
          } else {
            this.setState({
              optionItems : items || [],
              dataLoaded : true,
              loading:false,
              moreItems : false,
            });
          }
        });
      });
  }

  focusSelect(){

    // if( !this.state.dataLoaded && !this.state.loading ){
      this.loadData();
    // }
  }


  onSearch(val){
    if( !val ) return;

    let labelPid;
    if( this.state.classification[FLAGS.LABELABLE].length ){
      labelPid = this.state.classification[FLAGS.LABELABLE][0].pid;
    }


    this.setState({
      search : val,
    })

    this.loadData({
      [labelPid] : val,
    })
  }

  onSelectInList(){
    this.loadReferenceNodeType()
      .then(({referenceNodeType, classification}) => {
        let defaultSearches = [];

        let {
          codeFilter,
          ux,
        } = this.props.propertyType;

        // 임시 암묵적 약속에 따라 지정한다.
        if( codeFilter ){
          if( classification.map.upperCode ){
            defaultSearches.push({
              pid : 'upperCode',
              method : 'matching',
              schema : classification.map.upperCode ,
              value : codeFilter,
              fixed : true,
            });
          }
        }


        if( ux ){

          if( ux.logicFilter ){
            let logicFilterO = this.context.ice2.resolver.resolveFromDataAndErrorToNull(ux.logicFilter, {'this':this.props.ownNodeItemObject});

            defaultSearches = interpretQueryToPropsGroupList(logicFilterO, classification);
          }
        }




        let selectFuncKey = this.context.UIISystems.getUnique();
        this.context.UIISystems.openTool('ICE2Data', '[modal]',{
          width:'80%',
          height:'70%',
          // UNIQUE_LAYER : true,
        }, {
          selectable : true,
          title : '목록에서 선택',
          nodeType : referenceNodeType,
          multiSelection : this.props.multiple,
          type : 'list',
          target : {

          },
          footerBtnCallbacks : {
            'select' : selectFuncKey,
          },
          defaultSearches,
        }).then((closer) => {
          this.context.functionStore.register(selectFuncKey, (items)=>{

            let idPid;
            if( this.state.classification.primaryProperty ){
              idPid = this.state.classification.primaryProperty.pid;
            }

            let labelPid;
            if( this.state.classification[FLAGS.LABELABLE].length ){
              labelPid = this.state.classification[FLAGS.LABELABLE][0].pid;
            }



            let idableProps = this.state.classification[FLAGS.IDABLE];


            let valuePairs = items.map((item) => {
              let refId;

              refId = idableProps
                .map((propertyType) => item[propertyType.pid] )
                .join('>');

              return {
                value : item[idPid],
                label : item[labelPid] && item[labelPid].label ? item[labelPid].label: item[labelPid],
                refId,
              }
            });



            if( this.props.multiple ){
              this.props.onSelects(this.props.propertyType, [
                ...this.props.valueLabelSets,
                ...valuePairs,
              ])
            } else {
              this.props.onSelects(this.props.propertyType, [
                ...valuePairs,
              ]);
            }

            closer();
          })
        })
      });

  }

  onChangeByOption(_selectedValues){
    let selectedValues = this.props.multiple ? _selectedValues : [_selectedValues];

    let idPid;
    if( this.state.classification.primaryProperty ){
      idPid = this.state.classification.primaryProperty.pid;
    }



    let labelPid;
    if( this.state.classification[FLAGS.LABELABLE].length ){
      labelPid = this.state.classification[FLAGS.LABELABLE][0].pid;
    }

    let idableProps = this.state.classification[FLAGS.IDABLE];

    let pairs = Promise.map(selectedValues, (selectedValue) => {
      let selectedItem = this.state.optionItems.find((item) => item[idPid] == selectedValue);
      let refId;


      // 이전에 로드한 목록에 선택된 항목이 존재한다면 바로 처리해서 resolve!!
      if( selectedItem ){
        refId = idableProps
          .map((propertyType) => selectedItem[propertyType.pid] )
          .join('>');

        return Promise.resolve({
          value : selectedValue,
          label : selectedItem[labelPid] ? (selectedItem[labelPid].label ? selectedItem[labelPid].label: selectedItem[labelPid]) : selectedValue,
          refId,
        });
      }

      // 항목에 존재 하지 않는다면 아마 refId 가 이미 있을 것이다. 그대로 반환!!

       let exists = this.props.valueLabelSets.find((pair) => pair.value == selectedValue );

      return Promise.resolve(exists);
    })
      .then((pairs) => {
        this.props.onSelects(this.props.propertyType, pairs);
      })
  }

  componentDidMount(){

  }

  renderOptions(){
    if( !this.state.dataLoaded ){

      return this.props.valueLabelSets.map((set, i) => <Option key={i} value={set.value && set.value.toString()}>
        {set.label}
      </Option>);
    } else {

      let idPid;
      if( this.state.classification.primaryProperty ){
        idPid = this.state.classification.primaryProperty.pid;
      }

      let labelPid;
      if( this.state.classification[FLAGS.LABELABLE].length ){
        labelPid = this.state.classification[FLAGS.LABELABLE][0].pid;
      }


      return this.state.optionItems.map((item, i) => item && <Option key={i} value={(item[idPid] || '').toString()}>
        {labelPid ? (item[labelPid] ? item[labelPid].label || item[labelPid] : item[idPid] ) : item[idPid] }
      </Option>);
    }
  }

  renderUnincludedOptions(){

    let idPid;
    if( this.state.classification.primaryProperty ){
      idPid = this.state.classification.primaryProperty.pid;
    }

    return this.props.valueLabelSets.filter((valueSet) => !this.state.optionItems.find((item) => valueSet.value == item[idPid]))
      .map((item, i) => <Option key={i} value={item.value && item.value.toString()}>
      {item.label}
    </Option>);
  }

  render(){

    return <div className="reference-select">
      <Select
        onFocus={::this.focusSelect}
        showSearch
        mode={this.props.multiple && 'multiple'}
        style={{ width: 200 }}
        placeholder="Select"
        optionFilterProp="children"
        onSearch={(val) => this.wrappedOnSearch(val) }
        value={this.props.valueLabelSets.map((valueLabelSet) => valueLabelSet.value && valueLabelSet.value.toString())}
        onChange={::this.onChangeByOption}
        // onBlur={handleBlur}
        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
        {/*<Option value="">*/}
          {/*None*/}
        {/*</Option>*/}


        {
          this.state.classification ? this.renderUnincludedOptions() : []
        }

        {
          this.renderOptions()
        }
      </Select>

      { this.state.moreItems && <button className='more-items' onClick={:: this.onSelectInList}>
        목록에서 선택하기
      </button>}
    </div>
  }
}
