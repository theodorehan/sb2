import React, {Component} from 'react';
import PropTypes from 'prop-types';

import { Select  } from 'antd';
const Option = Select.Option;

export default class SelectFromAPI extends Component {

  static contextTypes = {
    ice2 : PropTypes.func,
  }

  static propTypes = {
    onChange : PropTypes.func,
  }


  constructor(props){
    super(props);
    this.state = {
      items: [],
      // value: '',
    }
  }

  onChange(value){
    // this.setState({
    //   value,
    // });

    this.props.onChange && this.props.onChange(value);
  }

  componentDidMount(){

    this.context.ice2.get(this.props.param.apiUrl)
      .then((res) => {
        this.setState({
          items : res.data.items,
          // value : this.props.param.default,
        });
      })
  }


  renderOption(item, i){

    switch(this.props.param.sourceType){
      case "code" :
        return <Option key={i} value={item.code}>{item.name || item.code}</Option>;
      case "properties" :
        return <Option key={i} value={item.pid}>{item.propertyTypeName || item.pid}</Option>;
    }
  }

  render(){
    return <Select
      value={this.props.value}
      onChange={(value) => this.onChange(value)}
      showSearch
      style={{ width: 100 }}
      placeholder="Select">

      { this.state.items.map( (item, i)=> this.renderOption(item, i)) }
    </Select>
  }
}
