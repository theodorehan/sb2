import React, {Component} from 'react';
import PropTypes from 'prop-types';
import './style.scss';
import CloseIcon from 'material-ui/svg-icons/navigation/close'

let FileViewer;
import('../../Display/fileViewer')
  .then((fileViewer) => {
    FileViewer = fileViewer.default;
  })


export default class FilePreviewer extends Component {

  static propTypes = {
    file : PropTypes.object,
    onClickClose : PropTypes.func,
    iceUrl: PropTypes.string,
    onSize : PropTypes.func,
  }

  constructor(props){
    super(props);

    this.state = {
      bytes : null,
    }
  }

  onClose(){
    this.props.onClickClose && this.props.onClickClose();
  }

  onImgLoad(e){
    this.props.onSize(this.props.file, e.nativeEvent.target.naturalWidth, e.nativeEvent.target.naturalHeight);
  }


  readFileBytes(file){
    var reader = new FileReader();

    reader.onload = (e) => {
      this.setState({
        bytes : e.target.result,
      });
    }

    reader.readAsDataURL(file);
  }



  componentWillReceiveProps(nextProps){
    let contentType = nextProps.file.type;

    switch (contentType) {
      case 'application/msword':

        break;
      case 'application/excel':
      case 'application/vnd.oasis.opendocument.spreadsheet':
        break;
      case 'application/vnd.oasis.opendocument.presentation':
      case 'application/mspowerpoint':
        break;
      case 'application/x-zip-compressed':
        break;
      case 'audio/mp3':
      case 'audio/mp4':
        break;
      case 'image/jpg':
      case 'image/jpeg':
      case 'image/png':
      case 'image/bmp':
        this.readFileBytes(nextProps.file);
        return;
      case 'text/plain':
      case 'text/csv':
        break;
      case 'video/mp4':
      case 'video/MPV':
      case 'video/MP4V-ES':
      case 'video/mpeg4-generic':
        break;
      default :
        break;
    }
  }

  componentDidMount(){

    let contentType = this.props.file.type;

    switch (contentType) {
      case 'application/msword':

        break;
      case 'application/excel':
      case 'application/vnd.oasis.opendocument.spreadsheet':
        break;
      case 'application/vnd.oasis.opendocument.presentation':
      case 'application/mspowerpoint':
        break;
      case 'application/x-zip-compressed':
        break;
      case 'audio/mp3':
      case 'audio/mp4':
        break;
      case 'image/jpg':
      case 'image/jpeg':
      case 'image/png':
      case 'image/bmp':
        this.readFileBytes(this.props.file);
        return;
      case 'text/plain':
      case 'text/csv':
        break;
      case 'video/mp4':
      case 'video/MPV':
      case 'video/MP4V-ES':
      case 'video/mpeg4-generic':
        break;
      default :
        break;
    }
  }


  renderPreview(){

    let contentType = this.props.file.type;



    switch (contentType) {
      case 'application/msword':
        return <div>Word</div>;
        break;
      case 'application/excel':
      case 'application/vnd.oasis.opendocument.spreadsheet':
        return <div>Excel</div>;
      case 'application/vnd.oasis.opendocument.presentation':
      case 'application/mspowerpoint':
        return <div>PPT</div>;
      case 'application/x-zip-compressed':
        return <div>Zip</div>;
      case 'audio/mp3':
      case 'audio/mp4':
        return <div>Audio</div>;
      case 'image/jpg':
      case 'image/jpeg':
      case 'image/png':
      case 'image/bmp':
        return <img src={this.state.bytes} onLoad={(e) => this.onImgLoad(e)}/>;
      case 'text/plain':
      case 'text/csv':
        return <div>CSV</div>;
      case 'video/mp4':
      case 'video/MPV':
      case 'video/MP4V-ES':
      case 'video/mpeg4-generic':
        return <div>Video</div>;
      default :
        return <div>{contentType} Unknown</div>;
    }
  }

  renderPreviewIce2File(){

    let contentType = this.props.file.contentType;



    switch (contentType) {
      case 'application/msword':
        return <div>Word</div>;
        break;
      case 'application/excel':
      case 'application/vnd.oasis.opendocument.spreadsheet':
        return <div>Excel</div>;
      case 'application/vnd.oasis.opendocument.presentation':
      case 'application/mspowerpoint':
        return <div>PPT</div>;
      case 'application/x-zip-compressed':
        return <div>Zip</div>;
      case 'audio/mp3':
      case 'audio/mp4':
        return <div>Audio</div>;
      case 'image/jpg':
      case 'image/jpeg':
      case 'image/png':
      case 'image/bmp':
        return <FileViewer fileValue={this.props.file} baseUrl={this.props.iceUrl}/>
      case 'text/plain':
      case 'text/csv':
        return <div>CSV</div>;
      case 'video/mp4':
      case 'video/MPV':
      case 'video/MP4V-ES':
      case 'video/mpeg4-generic':
        return <div>Video</div>;
      default :
        return <div>{contentType} Unknown</div>;
    }
  }

  render(){

    if( this.props.file instanceof File ){
      return <div className="file-previewer">

        <div className="file-real">
          { this.renderPreview() }

          <button className="close" onClick={:: this.onClose}>
            <CloseIcon style={{color:'inherit'}}/>
          </button>
        </div>
        <div className="title">
        <span className="nickname">
          {this.props.file.name.replace(/(^.*?)(\.[\w\d]+$|$)/, '$1')}
        </span>
          <span className="ext">
          {this.props.file.name.replace(/^.*?(\.[\w\d]+$|$)/, '$1')}
        </span>
        </div>
      </div>
    } else {
      // ice2 File 객체

      return <div className="file-previewer">

        <div className="file-real">
          { this.renderPreviewIce2File() }

          <button className="close" onClick={:: this.onClose}>
            <CloseIcon style={{color:'inherit'}}/>
          </button>
        </div>
        <div className="title">
        <span className="nickname">
          {this.props.file.fileName.replace(/(^.*?)(\.[\w\d]+$|$)/, '$1')}
        </span>
          <span className="ext">
          {this.props.file.fileName.replace(/^.*?(\.[\w\d]+$|$)/, '$1')}
        </span>
        </div>
      </div>
    }
  }
}
