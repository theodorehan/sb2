export const STRING = 'STRING';
export const DATE = 'DATE';
export const INT = 'INT';
export const LONG = 'LONG';
export const DOUBLE = 'DOUBLE';
export const BOOLEAN = 'BOOLEAN';
export const TEXT = 'TEXT';
export const FILE = 'FILE';
export const FILES = 'FILES';
export const CODE = 'CODE';
export const REFERENCE = 'REFERENCE';
export const REFERENCES = 'REFERENCES';
export const REFERENCED = 'REFERENCED';
export const JSON = 'JSON';
export const ARRAY = 'ARRAY';
export const OBJECT = 'OBJECT';

export const SIMPLE = 'SIMPLE';
export const CJK = 'CJK';
export const STANDARD = 'STANDARD';

export const IDABLE = 'IDABLE';
export const LABELABLE = 'LABELABLE';
export const TREEABLE = 'TREEABLE';
export const INDEXABLE = 'INDEXABLE';

export const FLAGS = {
  IDABLE,
  LABELABLE,
  TREEABLE,
  INDEXABLE,
}

export const FLAGS_ARRAY = [
  IDABLE,
  LABELABLE,
  TREEABLE,
  INDEXABLE,
]

export const VALUE_TYPES = {
  STRING,
  DATE,
  INT,
  LONG,
  DOUBLE,
  TEXT,
  FILE,
  FILES,
  CODE,
  REFERENCE,
  REFERENCES,
  REFERENCED,
  BOOLEAN,
  JSON,
  ARRAY,
  OBJECT,
}

export const VALUE_TYPES_ARRAY = [
  STRING,
  DATE,
  INT,
  LONG,
  DOUBLE,
  TEXT,
  FILE,
  CODE,
  REFERENCE,
  REFERENCES,
  REFERENCED,
  BOOLEAN,
  JSON,
  ARRAY,
  OBJECT,
];

export const ANALYZER = {
  SIMPLE,
  CJK,
  CODE,
  STANDARD,
}


export const ANALYZER_ARRAY = [
  SIMPLE,
  CJK,
  CODE,
  STANDARD,
]
