import React, {Component} from 'react';
import PropTypes from 'prop-types';
import ContentLoader, { Rect, Circle } from 'react-content-loader';
import moment from 'moment';
import numeral from 'numeral';
import SearchIcon from 'material-ui/svg-icons/action/search';
import './style.scss';
import {VALUE_TYPES} from "../constants";
import classnames from 'classnames';
import {Popover} from 'antd';

import Ice2ItemViewer from '../../Ice2ItemViewer';
import FileViewer from './fileViewer';
import {classifyProperties} from '../helpers';
import {FLAGS} from '../constants';



export default class Display extends Component {

  static propTypes = {
    style: PropTypes.object,
    propertyType : PropTypes.object,
    tid : PropTypes.string.isRequired,
    pid : PropTypes.string.isRequired,
    style : PropTypes.object,
    className : PropTypes.string,
    data : PropTypes.any,
    withLabel : PropTypes.bool,
    labelPosition : PropTypes.oneOf(['left', 'top', 'right']),
    column : PropTypes.number,
    fluid : PropTypes.bool.isRequired,

    renderAsTableRow : PropTypes.bool,

    divideLine : PropTypes.bool,
    divideLineColor : PropTypes.string,
    divideLinePos : PropTypes.oneOf(['left']),

    dataMaxWidth : PropTypes.number,

    defaultPopupTarget : PropTypes.string,
  }

  static defaultProps = {
    labelPosition : 'left',
    fluid : true,
    dataMaxWidth : 768,
  }

  static contextTypes = {
    UIISystems : PropTypes.object,
    ice2 : PropTypes.func,
  }

  constructor(props){
    super();

    this.state = {
      referencedNodeTypeClassification : null,
      propertyType : props.propertyType,
      referenceNodeType : null,
      idablePidOfReferenceNodeType : null,
      labelablePidsOfReferencedNodeType : [],
    };
  }


  loadPropertyType(){
    return this.context.ice2.loadPropertyType(this.props.tid, this.props.pid)
      .then((type) => {
        this.setState({
          propertyType : type,
        });

        return type;
    })
  }

  loadReferenceNodeType(propertyType){
    let {
      referenceType,
      // referenceValue,
    } = this.state.propertyType || propertyType;

    this.context.ice2.loadNodeType(referenceType.value)
      .then((nodeType) => {
        let {propertyTypes} = nodeType;

        let classification = classifyProperties(nodeType.propertyTypes);


        let idablePidOfReferenceNodeType = classification.primaryProperty.pid;
        let labelablePidOfReferencedNodeType;

        if( classification[FLAGS.LABELABLE][0] ){
          labelablePidOfReferencedNodeType = classification[FLAGS.LABELABLE][0].pid;
        }


        this.setState({
          referencedNodeTypeClassification : classification,
          referenceNodeType : nodeType,
          idablePidOfReferenceNodeType,
          labelablePidOfReferencedNodeType,
        });
      })
  }

  componentDidMount(){
    if( !this.state.propertyType ){
      return this.loadPropertyType()
        .then((propertyType) => {


          if( this.state.propertyType.valueType.value.toUpperCase() === VALUE_TYPES.REFERENCED ){
            this.loadReferenceNodeType();
          }
        })
    } else {
      if( this.state.propertyType.valueType && this.state.propertyType.valueType.value.toUpperCase() === VALUE_TYPES.REFERENCED ){
        this.loadReferenceNodeType();
      }
    }
  }

  renderString(){
    return <div className="string">
      {JSON.stringify(this.props.data)}
    </div>


    if( this.state.propertyType.i18n ){
      let locales = Object.keys(this.props.data || {});
      return locales.map((lo) => {
        <div className='string' key={lo}>
          { this.props.data[lo] }
        </div>
      })
    } else {
      return <div className="string">
        {this.props.data}
      </div>
    }
  }

  renderText(){
    return <div className="string">
      {JSON.stringify(this.props.data)}
    </div>

    if( this.state.propertyType.i18n ){
      let locales = Object.keys(this.props.data || {});
      return locales.map((lo) => {
        <div className='string' key={lo}>
          { this.props.data[lo] }
        </div>
      })
    } else {
      return <div className="string">
        {this.props.data}
      </div>
    }
  }

  renderCode(){
    return <div className="string">
      {this.props.data.label}
    </div>
  }

  renderDate(){

    return <div className="date">
      {moment(this.props.data, 'YYYYMMDDhhmmss').format('YYYY MMMM DD h:mm:ss a')}
    </div>
  }

  renderInt(){

    return <div className={classnames("int", this.state.propertyType.idable && 'idable')}>
      {this.props.data}
    </div>
  }

  renderDouble(){

    return <div className="double">
      {this.props.data}
    </div>
  }

  renderLong(){

    return <div className="long">
      {this.props.data}
    </div>
  }


  renderReference(data = {}, i){
    let {
      value,
      label,
      refId,
    } = data;

    let {
      referenceType,
      referenceValue,
    } = this.state.propertyType;

    let openRef = () => {
      this.context.ice2.loadNodeType(referenceType.value)
        .then((nodeType) => {
        console.log(nodeType);
          let target = {};

          let classificaion = classifyProperties(nodeType.propertyTypes);

          let referenceFieldKey = referenceValue && referenceValue.value;

          if( !referenceFieldKey ){

            referenceFieldKey = classificaion.primaryProperty.pid;
          }


          let refIdToken = refId.split('>');

          for(let i = 0; i < classificaion.IDABLE.length ; i++ ){
            target[classificaion.IDABLE[i].pid] = refIdToken[i];
          }


          this.context.UIISystems.openTool('ICE2Data', this.props.defaultPopupTarget || '[layer]',{
            width:500,
            height:600,
            UNIQUE_LAYER : true,
          }, {
            nodeType,
            type : 'itemView',
            target,
          });
        })
    }


    return <div className="reference" key={i}>
      {label} <span className="value">({value})</span>
      <button className={"detail-call " + (refId ? '':'disabled') } onClick={refId ? openRef : null} >
        <SearchIcon style={{color:"inherit", width:18}}/>
      </button>
    </div>
  }


  renderReferenced(){

    if( !this.state.referenceNodeType ){
      return <div className="referenced">
        loading...
      </div>
    }   ///
       ///
      ///
     ///let nodeType = this.state.referenceNodeType;

    let {
      referenceType,
      referenceValue,
    } = this.state.propertyType;


    return <div className="referenced">
      {
        this.props.data.map((item, i) =>
          <Popover key={i} content={<Ice2ItemViewer withDetachToLayerButton withLookListButton defaultPopupTarget={this.props.defaultPopupTarget} style={{maxWidth:400, maxHeight:500}} nodeType={this.state.referenceNodeType} target={{[this.state.idablePidOfReferenceNodeType] : item[this.state.idablePidOfReferenceNodeType]}}/>}>
            <button onClick={()=>referenceView(i)} className="referenceView">
              { this.state.labelablePidOfReferencedNodeType ? item[this.state.labelablePidOfReferencedNodeType]||i : i}
            </button>
          </Popover>
        )
      }
    </div>
  }

  renderBoolean(){
    return <div className={classnames("bool", JSON.stringify(!!this.props.data))}>
      {JSON.stringify(this.props.data)}
    </div>
  }


  renderFile(){
    let {
      handler,
      contentsType,
      storePath,
      fileName,
      fileSize,
    } = this.props.data;

    let baseURL = this.context.ice2.defaults.baseURL;

    return <div className="file">
      <FileViewer fileValue={this.props.data} baseUrl={baseURL.replace(/\/?$/, '/')} style={{maxWidth:300}}/>
    </div>
  }

  renderByValueTypes(){

    if( this.props.data === null || this.props.data === undefined ){
      return <div className="not">
        {JSON.stringify(this.props.data)}
      </div>
    }

    if( !this.state.propertyType.valueType ){
      return JSON.stringify(this.props.data);
    }


    switch(this.state.propertyType.valueType.value.toUpperCase()){
      case VALUE_TYPES.STRING :
        return this.renderString();
      case VALUE_TYPES.TEXT :
        return this.renderText();
      case VALUE_TYPES.CODE :
        return this.renderCode();
      case VALUE_TYPES.BOOLEAN :
        return this.renderBoolean();

      case VALUE_TYPES.DATE :
        return this.renderDate();
      case VALUE_TYPES.FILE :
        return this.renderFile();
      case VALUE_TYPES.INT :
        return this.renderInt();
      case VALUE_TYPES.LONG :
        return this.renderLong();
      case VALUE_TYPES.DOUBLE :
        return this.renderDouble();
      case VALUE_TYPES.REFERENCE :
        return this.renderReference(this.props.data);
      case VALUE_TYPES.REFERENCES :
        return this.props.data.map(::this.renderReference);
      case VALUE_TYPES.REFERENCED :
        return this.renderReferenced();
      default :
        return JSON.stringify(this.props.data);
    }
  }

  renderLabel(){

    return <div className="label" title={`${this.state.propertyType.pid}:${this.state.propertyType.valueType ? this.state.propertyType.valueType.value.toUpperCase() : 'valueType 엄따'}`}>
      <span className="title">{this.state.propertyType.propertyTypeName}</span>
    </div>
  }

  render(){

    if( !this.state.propertyType ){
      return <div className={"ice2-display-element" + this.props.className} style={this.props.style}>
        Loading..
      </div>
    }

    return <div
      className={
        classnames(
          "ice2-display-element",
          this.props.withLabel && `with-label label-${this.props.labelPosition}`,
          this.props.className,
          this.props.renderAsTableRow && 'table-row',
        )
      }
      style={this.props.style}>

      { this.props.withLabel ? this.renderLabel() : ''}
      <div className="data" style={{maxWidth : this.props.dataMaxWidth}}>
        { this.renderByValueTypes() }
      </div>
    </div>
  }
}


export const TABLE_STYLE_SUPPORT = {
  display:'table',
  borderCollapse : 'collapse',
  width:'100%',
}
