import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { Modal } from 'antd';
import './style.scss';

function getURL(storePath, baseURL){
  if( /^(https?:)?\/\//.test(storePath) ){
    return storePath;
  } else {
    return baseURL + storePath;
  }
}


export function Kernel (props){
  let {
    handler,
    contentType,
    storePath,
    fileName,
    fileSize,
    baseUrl,
    onClick,
    onLoadContent,
    style = {},
  } = props;


  switch (contentType) {
    case 'application/msword':
      return <div>Not yet support</div>;
      break;
    case 'application/excel':
    case 'application/vnd.oasis.opendocument.spreadsheet':
      return <div onClick={onClick}>Not yet support</div>;
    case 'application/vnd.oasis.opendocument.presentation':
    case 'application/mspowerpoint':
      return <div onClick={onClick}>Not yet support</div>;
    case 'application/x-zip-compressed':
      return <div onClick={onClick}>Not yet support</div>;
    case 'audio/mp3':
    case 'audio/mp4':
      return <div onClick={onClick}>Not yet support</div>;
    case 'image':
    case 'image/jpg':
    case 'image/jpeg':
    case 'image/png':
    case 'image/gif':
    case 'image/bmp':
      return <img onClick={onClick} style={style} src={getURL(storePath, baseUrl)} onLoad={onLoadContent}/>;
    case 'text/plain':
    case 'text/csv':
      return <div onClick={onClick}>Not yet support</div>;
    case 'video/mp4':
    case 'video/MPV':
    case 'video/MP4V-ES':
    case 'video/mpeg4-generic':
      return <div onClick={onClick}>Not yet support</div>;
    default :
      return <div onClick={onClick}>Unknown</div>;
  }
}



export default class FileViewer extends Component {
  static propTypes = {
    className : PropTypes.string,
    style : PropTypes.object,
    fileValue : PropTypes.object.isRequired,
    baseUrl : PropTypes.string,
    contentMaxHeight : PropTypes.number,
  };

  static contextTypes = {
    ice2 : PropTypes.func,
  };

  constructor(){
    super();

    this.state = {
      fixedHeight : null,
      modalVisible : false,
    }
  }

  onLoadContent(e){
    let target = e.nativeEvent.currentTarget;
    if( target.offsetHeight > this.props.contentMaxHeight ){
      this.setState({
        fixedHeight : this.props.contentMaxHeight,
        naturalWidth : target.naturalWidth,
        naturalHeight : target.naturalHeight,
      });
    } else {
      this.setState({
        naturalWidth : target.naturalWidth,
        naturalHeight : target.naturalHeight,
      });
    }
  }

  render(){
    let {
      className,
      style,
      fileValue,
      baseUrl,
    } = this.props;

    let kernelStyle = {

    };

    if( this.state.fixedHeight ){
      kernelStyle.height = this.state.fixedHeight;
    }

    if(typeof fileValue == 'string' ){
      let tokens = fileValue.split('.');
      let ext = tokens[tokens.length-1];
      let fileType;

      switch(ext.toLowerCase()){
        case 'jpg':
        case 'jpeg':
        case 'png':
        case 'gif':
          fileType = 'image';
          break;
        case 'pdf':
          break;
      }


      fileValue = {
        storePath : fileValue,
        contentType : fileType,
      }
    }

    return <div style={{...style}} className={classnames('file-viewer',className)} >
      <Modal
        footer={null}
        width='90%'

        bodyStyle={{ textAlign: 'center', backgroundColor:'transparent' }}
        visible={this.state.modalVisible}
        onOk={() => this.setState({modalVisible:false})}
        onCancel={() => this.setState({modalVisible:false})}>
        <div style={{width:'100%'}}>
          <Kernel
            onClick={() => this.setState({modalVisible:false})}
            {...fileValue}
            style={{maxWidth:'100%'}}
            baseUrl={baseUrl || (this.context.ice2.defaults.baseURL + '/')}
          />
        </div>
      </Modal>

      <div className='over-glow'>
        <Kernel
          onClick={() => this.setState({modalVisible:true})}
          {...fileValue}
          style={kernelStyle}
          fixedHeight={this.state.fixedHeight}
          baseUrl={baseUrl || (this.context.ice2.defaults.baseURL + '/')}
          onLoadContent={::this.onLoadContent}/>
      </div>
    </div>
  }
}
