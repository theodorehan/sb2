import React, {Component} from 'react';
import PropTypes from 'prop-types';
import get from 'lodash/get';
import FileStage from '../Input/FileStage';
import {classifyProperties} from "../helpers";

let Ice2FileViewer;
import('./fileViewer.js')
  .then((fileViewer)=> Ice2FileViewer = fileViewer.default);

export default class NodeThumbnail extends Component {
  static propTypes = {
    tid : PropTypes.string,
    pid: PropTypes.string,
    id : PropTypes.any,

    data : PropTypes.object,
    rule : PropTypes.object,
    // height : PropTypes.any,
    style: PropTypes.object,
    itemOptionElement: PropTypes.any,
  }

  static contextTypes = {
    ice2 : PropTypes.func,
    UIISystems : PropTypes.object,
  }

  static defaultProps = {
    rule : {
      type : 'overlayTitle', // overlayTitle
      imagePid : 'file',
      titlePid : 'title',
      loadOptionParams : {},
    },
  }


  constructor(props){
    super();

    this.state = {
      data : props.data,
    };
  }

  async load(){

    let nodeType = await this.context.ice2.loadNodeType(this.props.tid);

    let classification = classifyProperties(nodeType.propertyTypes);

    let {
      IDABLE,
    } = classification;

    let id = this.props.id;
    if(this.props.data){
      id = IDABLE.map((idableProp) => this.props.data[idableProp.pid]).join('>');
    }




    this.context.ice2.get(`/node/${this.props.tid}/read.json`, {
      params : {
        id : id,
        includeReference : true,
        referenceView: true,
        ...this.props.rule.loadOptionParams,
      },
    }).then((res) => {
      let {data} = res;

      this.setState({
        data : data.item,
      });
    })
  }

  onClickId(){
    this.context.ice2.loadNodeType(this.props.tid)
      .then((nodeType) => {


        this.context.UIISystems.openTool('ICE2Data', this.props.defaultPopupTarget || '[modal]',{
          width:500,
          height:600,
          UNIQUE_LAYER : true,
        }, {
          nodeType,
          type : 'itemView',
          target : {
            [this.props.pid] : this.props.id,
          },
        });
      })
  }

  refresh(){
    this.load();
  }

  componentDidMount(){
    this.load();
    return;
    if( !this.state.data ){
      this.load();
    }
  }

  componentDidUpdate(prevProps){
    if( prevProps.data !== this.props.data ){

      this.load();
    }
  }

  renderDisplay(){
    switch ( this.props.rule.type ){
      case 'overlayTitle' :
        return <div className="overlay-title" style={{height:this.props.height}}>
          <Ice2FileViewer style={{height:this.props.height}} fileValue={this.state.data[this.props.rule.imagePid]}/>
          { this.state.data[this.props.rule.titlePid] && <div className="overlay">
            <div className="title">
              {this.state.data[this.props.rule.titlePid]}
            </div>
          </div> }
        </div>;

      case 'thumbnail' :
        let fileValue = get(this.state.data,this.props.rule.imagePid);
        return <div className="thumbnail-type">
          { fileValue ? <Ice2FileViewer
            style={{maxHeight:this.props.rule.imageMaxHeight || 100, width:this.props.rule.imageWidth || 200}}
            contentMaxHeight={this.props.rule.imageMaxHeight || 100}
            fileValue={fileValue}/> : null && <FileStage
            iceUrl={this.context.ice2.defaults.baseURL.replace(/\/?$/, '/')}
            onChange={:: this.onDropFile }
            files={this.state.file}/>}
          <div className="text-display" style={{}}>


            <div className="title" onClick={:: this.onClickId}>
              { this.props.rule.idPid && <div className="id">
                {get(this.state.data,this.props.rule.idPid)}
              </div> }
              {get(this.state.data,this.props.rule.titlePid, 'Untitled')}
            </div>
            { this.props.rule.descPid && <div className="desc">
              {get(this.state.data,this.props.rule.descPid, 'Empty description')}
            </div> }


            { this.props.itemOptionElement && <div className="options">
              { this.props.itemOptionElement }
            </div>}
          </div>
        </div>;

      case "referencesNest" :
        // console.log(this.state.data, this.props.rule.titlePid);
        return <div className="references-nest">
          <div className="title">
            {get(this.state.data,this.props.rule.titlePid, 'Untitled')}

            <div className="count">
              { (get(this.state.data,this.props.rule.referencesPid, [])|| []).length } 개
            </div>
          </div>
          {this.props.rule.subTitlePid && <div className="subTitle">
            {get(this.state.data,this.props.rule.subTitlePid, 'Untitled')}
          </div>}

          { this.props.itemOptionElement && <div className="options">
            { this.props.itemOptionElement }
          </div>}
        </div>;

      default:
        return <div style={{height:this.props.height, color:'#f00'}}>
          { this.props.rule.type } is invalid type
        </div>
    }
  }

  render(){

    if( !this.state.data ){
      return <div> Loading... </div>
    }

    return <div className="node-thumbnail" style={{...this.props.style}}>
      { this.renderDisplay() }
    </div>;
  }
}
