import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import './style.scss';


import Draggable from '../../../../Core/Components/Draggable';

import * as LayoutActions from '../../../../actions/Layout';

import {
  CLOSE_LAYOUT_WITH_ELASTIC,
} from '../../../Toolset/UIISystemActionSheets';

export default class SideArea extends Component {
  static propTypes = {
    isa : PropTypes.string.isRequired,
    Layout : PropTypes.object.isRequired,
    Studio : PropTypes.object.isRequired,
    elementCall : PropTypes.func.isRequired,
  }

  static contextTypes = {
    UIISystems : PropTypes.object,
  }

  static childContextTypes = {
    layout : PropTypes.object,
  }

  constructor(props){
    super(props);

    this.layoutType = '[layer]';
  }


  getChildContext() {
    return {
      layout: this,
    };
  }

  onClose(){
    console.log('close', CLOSE_LAYOUT_WITH_ELASTIC(this.props.isa))

    this.context.UIISystems.emitActionSheet(CLOSE_LAYOUT_WITH_ELASTIC(this.props.isa, null, true));
  }

  onFocus(){
    let { Layout, isa } = this.props;
    let myLayout = Layout[isa];

    let layers = Object.keys(Layout).filter((id) => id.indexOf('layer_') == 0 );
    let zIndexMax = 0;
    for(let i = 0; i < layers.length; i++ ){
      zIndexMax = Math.max(zIndexMax, Layout[layers[i]].zIndex );
    }


    if( zIndexMax !== myLayout.zIndex ){
      this.props.dispatch(
        LayoutActions.MODIFY_CUSTOM_LAYOUT(isa, myLayout.x, myLayout.y, myLayout.width, myLayout.height, zIndexMax + 1 )
      );
    }
  }

  onFullscreen(){
    let { Layout, isa } = this.props;
    let myLayout = Layout[isa];

    this.props.dispatch(
      LayoutActions.LAYOUT_FULLSCREEN(isa)
    );
  }

  onFullscreenExit(){
    let { Layout, isa } = this.props;
    let myLayout = Layout[isa];

    this.props.dispatch(
      LayoutActions.LAYOUT_FULLSCREEN_EXIT(isa)
    );
  }

  onDragWidthStretch(x,y, deltaX, deltaY){
    let { Layout, isa } = this.props;
    let myLayout = Layout[isa];

    this.props.dispatch(LayoutActions.MODIFY_CUSTOM_LAYOUT(isa, myLayout.x, myLayout.y, myLayout.width + deltaX ));
  }

  onDragHeightStretch(x,y, deltaX, deltaY){
    let { Layout, isa } = this.props;
    let myLayout = Layout[isa];

    this.props.dispatch(LayoutActions.MODIFY_CUSTOM_LAYOUT(isa, myLayout.x, myLayout.y, myLayout.width, myLayout.height + deltaY ));
  }

  onDragMultiStretch(x,y, deltaX, deltaY){
    let { Layout, isa } = this.props;
    let myLayout = Layout[isa];

    this.props.dispatch(LayoutActions.MODIFY_CUSTOM_LAYOUT(isa, myLayout.x, myLayout.y, myLayout.width + deltaX, myLayout.height + deltaY ));
  }

  onDragMove(x,y, deltaX, deltaY){
    let { Layout, isa } = this.props;
    let myLayout = Layout[isa];


    this.props.dispatch(LayoutActions.MODIFY_CUSTOM_LAYOUT(isa, myLayout.x + deltaX, myLayout.y + deltaY));
  }

  render(){
    let { Layout, Studio, isa, uiiStatementIDList, draggableAimed, aimedDraggableType, aimedDraggableData } = this.props;
    let style = {};

    let myLayout = Layout[isa];
    style.left = myLayout.x;
    style.top = myLayout.y;
    style.width = myLayout.width;
    style.height = myLayout.height;
    style.zIndex = myLayout.zIndex;

    if( myLayout.fullscreen ){
      style.left = 0;
      style.top = 0;
      style.width = window.innerWidth
      style.height = window.innerHeight;
    }

    return (
      <div className={ classnames("studio-layer", draggableAimed ? 'draggable-hovering':'')} style={style}>
        <Draggable
          onDragMove={:: this.onDragWidthStretch}
          _draggableType="none"
          _draggableData={{}}
          className={classnames('width-control',isa )}
          component={<div></div>}/>

        <Draggable
          onDragMove={:: this.onDragHeightStretch}
          _draggableType="none"
          _draggableData={{}}
          className={classnames('height-control',isa )}
          component={<div></div>}/>

        <Draggable
          onDragMove={:: this.onDragMultiStretch}
          _draggableType="none"
          _draggableData={{}}
          className={classnames('wh-control',isa )}
          component={<div></div>}/>

        { this.props.elementCall({width:style.width , height:style.height}) }
      </div>
    )
  }
}
