import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import './style.scss';

import Layer from './Layer';

import ToolProvider from '../../../Core/Components/ToolProvider';
import Zone from '../../../Core/Components/Zone';

@connect((state) => ({
  Deployment : state.get('Deployment'),
}))
class LayerSystemArea extends Component {
  constructor(props){
    super(props);
  }

  renderLayers(){
    let layerLayoutIds = Object.keys(this.props.Layout).filter((id)=>id.indexOf('layer_') === 0);


    return layerLayoutIds.map((layoutID)=><Zone
      key={layoutID}
      component={Layer}
      acceptDraggableTypes={[]}
      zoneZDepth={0}
      zoneLayoutISA={layoutID}

      isa={layoutID}
      Layout={this.props.Layout}
      Studio={this.props.Studio}
      Elastic={this.props.Elastic}
      Deployment={this.props.Deployment}
      dispatch={this.props.dispatch}
      elementCall={
        (props) => <ToolProvider
          width={props.width}
          height={props.height}
          Elastic={this.props.Elastic}
          elasticID={this.props.Deployment[layoutID]}/>
      }/>)
  }

  render(){



    return <div className="studio-layer-system-area">
      { this.renderLayers() }
    </div>
  }
}

export default LayerSystemArea;
