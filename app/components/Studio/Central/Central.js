import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import {connect} from 'react-redux';
import uuid from 'uuid';

import {
  RESERVE_LAYOUT_ELEMENT_ID_BOTTOM,
  RESERVE_LAYOUT_ELEMENT_ID_CENTRAL,
  RESERVE_LAYOUT_ELEMENT_ID_LEFT,
  RESERVE_LAYOUT_ELEMENT_ID_LEFTAREA,
  RESERVE_LAYOUT_ELEMENT_ID_RIGHT,
  RESERVE_LAYOUT_ELEMENT_ID_RIGHTAREA,
  RESERVE_LAYOUT_ELEMENT_ID_TOP,
} from '../Constants'

import {
  Menubar,
} from '../Sidebar';

import './Central.scss'

export default class Central extends Component {

  static contextTypes = {

    events: PropTypes.object,
  }

  static propTypes = {
    isa : PropTypes.string.isRequired,
    Layout : PropTypes.object.isRequired,
    Studio : PropTypes.object.isRequired,
    elementCall : PropTypes.func.isRequired,
  }


  constructor(props){
    super(props);
  }

  onMouseEnter(){
    this.context.events.emit('on-enter-central');
  }

  calculatedStyle(){
    let { Layout, Studio, isa } = this.props;

    let style = {
      left:0,
      right:0,
      top:0,
      bottom:0,
    };

    if( Studio.layoutUsingMap[RESERVE_LAYOUT_ELEMENT_ID_TOP] ){
      style.top += Layout[RESERVE_LAYOUT_ELEMENT_ID_TOP].height;
    }

    if( Studio.layoutUsingMap[RESERVE_LAYOUT_ELEMENT_ID_BOTTOM] ){
      style.bottom += Layout[RESERVE_LAYOUT_ELEMENT_ID_BOTTOM].height;
    }

    if( Studio.layoutUsingMap[RESERVE_LAYOUT_ELEMENT_ID_LEFT] ){
      let sideType = Layout[RESERVE_LAYOUT_ELEMENT_ID_LEFT].menuType;

      style.left += Menubar.ConstantsOfType[sideType].width;
    }

    if( Studio.layoutUsingMap[RESERVE_LAYOUT_ELEMENT_ID_LEFTAREA] ){
      if( !Layout[RESERVE_LAYOUT_ELEMENT_ID_LEFTAREA].float ){
        style.left += Layout[RESERVE_LAYOUT_ELEMENT_ID_LEFTAREA].width;
      }
    }

    if( Studio.layoutUsingMap[RESERVE_LAYOUT_ELEMENT_ID_RIGHT] ){
      let sideType = Layout[RESERVE_LAYOUT_ELEMENT_ID_RIGHT].menuType;

      style.right += Menubar.ConstantsOfType[sideType].width;
    }

    if( Studio.layoutUsingMap[RESERVE_LAYOUT_ELEMENT_ID_RIGHTAREA] ){
      if( !Layout[RESERVE_LAYOUT_ELEMENT_ID_RIGHTAREA].float ) {
        style.right += Layout[RESERVE_LAYOUT_ELEMENT_ID_RIGHTAREA].width;
      }
    }


    return style;
  }

  render(){
    let { Layout, isa, draggableAimed, aimedDraggableType, aimedDraggableData } = this.props;
    let style = this.calculatedStyle();
    let componentWidth = window.innerWidth - style.left - style.right;
    let componentHeight = window.innerHeight - style.top - style.bottom;

    return (
      <div onMouseEnter={::this.onMouseEnter} className={ classnames("studio-central", draggableAimed ? 'draggable-hovering':'')} style={style}>
        { this.props.elementCall({
          width : componentWidth,
          height: componentHeight,
          left : style.left,
          right : style.right,
          top : style.top,
          bottom : style.bottom,

          draggableAimed,
          aimedDraggableType,
          aimedDraggableData,
        }) }
      </div>
    )
  }
}

