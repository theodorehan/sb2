import React from 'react';

import Button from './Button'

import * as UnitComponents from '../../../Unit';

const UII = (props) => {
  let {uiiID, uiiCollection} = props;
  let uiiStatement = uiiCollection[uiiID];

  let injectProps = {
    ...props,
    uiiStatement: uiiStatement,
  };

  switch (uiiStatement.type.toLowerCase()) {
    case 'button' :
      return <Button {...injectProps}/>;
  }
}

export default UII;
