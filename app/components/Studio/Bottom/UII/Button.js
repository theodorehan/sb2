import React from 'react';
import * as MaterialIcons from 'material-ui/svg-icons';
import classnames from 'classnames';
import * as Unit from '../../../Unit';

let Button = (props) => {
  let {uiiStatement} = props;


  let Icon, Title = uiiStatement.title;
  if (uiiStatement.iconType === 'fa') {
    let iconC = "fa-" + uiiStatement.icon;
    Icon = <i className={classnames("fa", iconC)} style={{color: uiiStatement.iconColor}}/>;
  }

  if (uiiStatement.iconType === 'material') {
    Icon = React.createElement(MaterialIcons[uiiStatement.icon], {
      style: {color: uiiStatement.iconColor},
    })
  }

  if (uiiStatement.iconType === 'unit') {
    let IconClass = Unit.get(uiiStatement.icon);

    Icon = <IconClass/>;
  }

  if( uiiStatement.titleType === 'unit'){
    let TitleClass = Unit.get(uiiStatement.title);
    Title = <TitleClass />;
  }


  return <div className={classnames('uii-button', uiiStatement.options.sideLabel ? 'side-label':'')}>
    <div className="icon-wrapper">
     {Icon}
    </div>
    <div className="title-wrapper" style={{ color : uiiStatement.titleColor }}>
      <span>{Title}</span>
    </div>
  </div>
}


export default Button;
