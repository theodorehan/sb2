import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import {connect} from 'react-redux';

import uuid from 'uuid';

import './SideArea.scss';

import {
  POSITION_RIGHT,
  POSITION_LEFT,
  Menubar,
} from '../Sidebar';


import {
  RESERVE_LAYOUT_ELEMENT_ID_LEFT,
  RESERVE_LAYOUT_ELEMENT_ID_RIGHT,
  RESERVE_LAYOUT_ELEMENT_ID_TOP,
  RESERVE_LAYOUT_ELEMENT_ID_BOTTOM,
} from '../Constants';

import {
  LEFTAREA_WIDTH_MODIFY,
  RIGHTAREA_WIDTH_MODIFY,
  FLASH_HIDE_LAYOUT,
  FLASH_SHOW_LAYOUT,
  LAYOUT_PIN,
  LAYOUT_PIN_RELEASE,
} from '../../../actions/Layout';

import Draggable from '../../../Core/Components/Draggable';


import {
  CLOSE_LAYOUT_WITH_ELASTIC,
  CLOSE_LAYOUT_ONLY,
  OPEN_LAYOUT_ONLY,
} from '../../Toolset/UIISystemActionSheets';

// @connect()
export default class SideArea extends Component {
  static propTypes = {
    isa : PropTypes.string.isRequired,
    Layout : PropTypes.object.isRequired,
    Studio : PropTypes.object.isRequired,
    elementCall : PropTypes.func.isRequired,
  }

  static childContextTypes = {
    layout : PropTypes.object,
  }

  static contextTypes = {
    UIISystems : PropTypes.object,
    events : PropTypes.object,
  }

  constructor(props){
    super(props);


    this.wrappedListeners = {
      onHide : this.onHide.bind(this),
      onShow : this.onShow.bind(this),
      onEnterCentral : this.onEnterCentral.bind(this),
    }
  }

  getChildContext() {
    return {
      layout: this,
    };
  }

  onEnterCentral(){
    if( this.props.isa != 'rightArea' ){
      console.log('onEnterCentral -> hide Area');
      this.onHide();
    }
  }

  onHide(){
    if( this.props.Layout[this.props.isa].float ){
      this.onFlashClose();
    }
  }

  onShow(){
    this.onFlashOpen();
  }

  onMouseLeave(){
    // this.onHide();
    // UX를 위해 onEnterCentral로 대체 한다.
  }

  /**
   * layout context interface
   * @returns {*}
   */
  isFloatLayout(){
    return this.props.Layout[this.props.isa].float;
  }

  /**
   * layout context interface
   */
  onFlashClose(){
    this.props.dispatch(FLASH_HIDE_LAYOUT(this.props.isa));
  }

  /**
   * layout context interface
   */
  onFlashOpen(){
    this.props.dispatch(FLASH_SHOW_LAYOUT(this.props.isa));
  }

  /**
   * layout context interface
   */
  onClose(){

    this.context.UIISystems.emitActionSheet(CLOSE_LAYOUT_WITH_ELASTIC(this.props.isa));
  }

  /**
   * layout context interface
   */
  onRequestPin(){
    this.props.dispatch(LAYOUT_PIN(this.props.isa));
  }

  /**
   * layout context interface
   */
  onRequestReleasePin(){
    this.props.dispatch(LAYOUT_PIN_RELEASE(this.props.isa));
  }

  onDragOutline(x,y){
    let { Layout, Studio, isa, uiiStatementIDList, draggableAimed, aimedDraggableType, aimedDraggableData } = this.props;

    let screenWidth = window.innerWidth;


    let followUpType = Layout[isa].followUp;
    let followLayout = Layout[followUpType];

    let position = followLayout.position;
    let menuType = followLayout.menuType;


    if( position === POSITION_LEFT ){
      let leftValue = 0;
      if( Studio.layoutUsingMap[followUpType] ){

        leftValue = Menubar.ConstantsOfType[menuType].width;
      }

      let calculatedWidth = Math.max(x - leftValue, 200);

      this.props.dispatch(LEFTAREA_WIDTH_MODIFY(calculatedWidth));
      // console.log(currentWidth);
    } else if( position === POSITION_RIGHT ){
      let rightValue = 0;
      if( Studio.layoutUsingMap[followUpType] ){

        rightValue = Menubar.ConstantsOfType[menuType].width;
      }

      let calculatedWidth = Math.max(screenWidth - x - rightValue, 200);
      this.props.dispatch(RIGHTAREA_WIDTH_MODIFY(calculatedWidth));
    }
  }


  componentDidMount(){

    this.context.events.listen('on-hide-'+this.props.isa, this.wrappedListeners.onHide);
    this.context.events.listen('on-show-'+this.props.isa, this.wrappedListeners.onShow);
    this.context.events.listen('on-enter-central', this.wrappedListeners.onEnterCentral);
  }

  componentWillUnmount(){
    this.context.events.removeListen('on-hide-'+this.props.isa, this.wrappedListeners.onHide);
    this.context.events.removeListen('on-show-'+this.props.isa, this.wrappedListeners.onShow);
    this.context.events.removeListen('on-enter-central', this.wrappedListeners.onEnterCentral);
  }

  render(){
    let { Layout, Studio, isa, uiiStatementIDList, draggableAimed, aimedDraggableType, aimedDraggableData } = this.props;
    let style = {};

    let myLayout = Layout[isa];

    let followUpType = Layout[isa].followUp;
    let followLayout = Layout[followUpType];

    let position = followLayout.position;
    let menuType = followLayout.menuType;

    if( position === POSITION_LEFT ){
      if( Studio.layoutUsingMap[followUpType] ){

        style.left = Menubar.ConstantsOfType[menuType].width;
      } else {
        style.left = 0;
      }
    } else if( position === POSITION_RIGHT ){
      if( Studio.layoutUsingMap[followUpType] ){

        style.right = Menubar.ConstantsOfType[menuType].width;
      } else {
        style.right = 0;
      }
    }


    // float 모드면 central 보다 zIndex를 1 높게 준다.
    if( this.props.Layout[this.props.isa].float ){
      style.zIndex = 2;
    }

    let topHeight = ( Studio.layoutUsingMap[RESERVE_LAYOUT_ELEMENT_ID_TOP] && Layout[RESERVE_LAYOUT_ELEMENT_ID_TOP].height ) || 0;
    let bottomHeight = ( Studio.layoutUsingMap[RESERVE_LAYOUT_ELEMENT_ID_BOTTOM] && Layout[RESERVE_LAYOUT_ELEMENT_ID_BOTTOM].height ) || 0;

    style.top = topHeight;
    style.bottom = bottomHeight;

    style.width = Layout[isa].width;
    return (
      <div
        className={ classnames("studio-side-area", draggableAimed ? 'draggable-hovering':'', this.props.Layout[this.props.isa].flashHide && 'hide')}
        style={style}
        onMouseLeave={:: this.onMouseLeave}>
        <div className={"bound-shadow "+ isa}>
        </div>
        <Draggable
          onDragMove={:: this.onDragOutline}
          _draggableType="none"
          _draggableData={{}}
          className={classnames('width-control',isa )}
          component={ ()=> <div></div>
        }/>

        { this.props.elementCall({width:Layout[isa].width}) }
      </div>
    )
  }
}

export const defaultWidth = 400;
