import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import ScrollArea from 'react-scrollbar';
import uuid from 'uuid';

import './Top.scss'

import UII from './UII';
import Draggable from '../../../Core/Components/Draggable';

export default class Top extends Component {

  static propTypes = {
    isa: PropTypes.string.isRequired,
    Layout: PropTypes.object.isRequired,
    Studio: PropTypes.object.isRequired,
    uiiStatementIDList: PropTypes.array,
  }


  constructor(props) {
    super(props);
  }


  renderUIITray(uiiList, _i, _width) {

    let {uiiStatementIDList} = this.props;

    let totalTray = uiiStatementIDList.length;

    let style = {
      width: _width,
      textAlign: 'center',
    };

    if (_i == 0) {
      style.textAlign = 'left';
    } else if (_i + 1 === totalTray) {
      style.textAlign = 'right';
    }


    return <div className="uii-tray" style={style} key={_i}>
      { uiiList.map(
        (uiiStatementID) =>
          <Draggable
            component={UII}

            _draggableType="uii"
            _draggableData={{uiiID: uiiStatementID}}
            _additionalStyle={{display: 'inline-block', height: '100%'}}

            uiiID={uiiStatementID}
            uiiCollection={this.props.Studio.uiiCollection}
            key={uiiStatementID}/>
      )}
    </div>
  }

  renderUIITraies() {
    let {uiiStatementIDList} = this.props;

    return uiiStatementIDList.map(
      (uiiList, i) => this.renderUIITray(uiiList, i, (100 / uiiStatementIDList.length) + '%')
    );
  }

  render() {
    let {Layout, isa, Studio, uiiStatementIDList, draggableAimed, aimedDraggableType, aimedDraggableData} = this.props;

    return (
      <div className={ classnames("studio-top", draggableAimed ? 'draggable-hovering':'')} style={{height: Layout[isa].height}}>
        <div className="uii-tray-wrapper">
          { this.renderUIITraies() }
        </div>
      </div>
    )
  }
}

export const defaultHeight = 60;
