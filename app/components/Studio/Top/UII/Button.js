import React from 'react';
import PropTypes from 'prop-types';
import * as MaterialIcons from 'material-ui/svg-icons';
import classnames from 'classnames';
import * as Unit from '../../../Unit';
import { Tooltip } from 'antd';

let Button = (props, ctx) => {
  let {uiiStatement} = props;


  let Icon, Title = uiiStatement.title,  tooltip = uiiStatement.tooltip;
  if (uiiStatement.iconType === 'fa') {
    let iconC = "fa-" + uiiStatement.icon;
    Icon = <i className={classnames("fa", iconC)} style={{color: uiiStatement.iconColor || ctx.theme.UII.iconColor}}/>;
  }

  if (uiiStatement.iconType === 'material') {
    Icon = React.createElement(MaterialIcons[uiiStatement.icon], {
      style: {color: uiiStatement.iconColor || ctx.theme.UII.iconColor },
    })
  }

  if (uiiStatement.iconType === 'unit') {
    let IconClass = Unit.get(uiiStatement.icon);

    Icon = <IconClass/>;
  }

  if( uiiStatement.titleType === 'unit'){
    let TitleClass = Unit.get(uiiStatement.title);
    Title = <TitleClass />;
  } else if (uiiStatement.titleType == 'code' ){
    Title = ctx.intl.formatMessage({id : uiiStatement.title});
  }

  if( uiiStatement.tooltipType == 'code' ){
    tooltip = ctx.intl.formatMessage({id : uiiStatement.tooltip});
  }


  return <div className={classnames('uii-button', uiiStatement.options.sideLabel ? 'side-label':'', props.active && 'active')} onClick={props.onAction}>
    <Tooltip title={tooltip} placement='bottom'>
      <div className="icon-wrapper">
        {Icon}
      </div>
      <div className="title-wrapper" style={{ color : uiiStatement.titleColor || ctx.theme.UII.titleColor }}>
        <span>{Title}</span>
      </div>
    </Tooltip>
  </div>
}

Button.contextTypes = {
  theme : PropTypes.object,
  intl : PropTypes.object,
}

export default Button;
