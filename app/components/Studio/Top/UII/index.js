import React from 'react';
import PropTypes from 'prop-types';
import Button from './Button';

import * as UnitComponents from '../../../Unit';

const UII = (props, context) => {
  let {uiiID, uiiCollection, associatedElastic} = props;
  let uiiStatement = uiiCollection[uiiID];

  let injectProps = {
    ...props,
    uiiStatement: uiiStatement,
  };

  let active = false;

  let uiiOptions = uiiStatement.options;
  if( uiiOptions && associatedElastic ){

    let uiiToolKey = uiiOptions.toolKey;
    let elasticToolKey = associatedElastic.toolKey;


    if( uiiToolKey == elasticToolKey ){
      active = true;
    }
  }

  switch (uiiStatement.type.toLowerCase()) {
    case 'button' :
      return <Button
        {...injectProps}
        active={active}
        onAction={(_e)=>{
          context.UIISystems.act(uiiStatement, context.zone.layoutISA);
        }}/>;
  }
}


UII.contextTypes = {
  zone : PropTypes.object,
  UIISystems : PropTypes.object,
};

export default UII;
