export const RESERVE_LAYOUT_ELEMENT_ID_LEFT = "left";
export const RESERVE_LAYOUT_ELEMENT_ID_RIGHT = "right";
export const RESERVE_LAYOUT_ELEMENT_ID_LEFTAREA = "leftArea";
export const RESERVE_LAYOUT_ELEMENT_ID_RIGHTAREA = "rightArea";
export const RESERVE_LAYOUT_ELEMENT_ID_TOP = "top";
export const RESERVE_LAYOUT_ELEMENT_ID_BOTTOM = "bottom";
export const RESERVE_LAYOUT_ELEMENT_ID_CENTRAL = "central";
