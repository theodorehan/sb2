import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import uuid from 'uuid';

import './Sidebar.scss';

import {
  RESERVE_LAYOUT_ELEMENT_ID_BOTTOM,
  RESERVE_LAYOUT_ELEMENT_ID_TOP,
} from '../Constants';

import MenuBar from './MenuBar'
import * as MenubarEtc from './MenuBar';

export default class Sidebar extends Component {

  static propTypes = {
    isa: PropTypes.string.isRequired,
    Layout: PropTypes.object.isRequired,
    Studio: PropTypes.object.isRequired,
    Elastic: PropTypes.object.isRequired,
    Deployment: PropTypes.object.isRequired,
    uiiStatementIDList: PropTypes.array,
  }

  static contextTypes = {
    events : PropTypes.object,
  }

  constructor(props) {
    super(props);
  }

  onMouseEnter(){
    this.context.events.emit('on-show-' + this.props.isa + 'Area');
  }

  render() {
    let {Layout, Studio, Elastic, Deployment, isa, uiiStatementIDList, draggableAimed, aimedDraggableType, aimedDraggableData} = this.props;
    let myLayout = Layout[isa];

    let style = {
      width: MenubarEtc.ConstantsOfType[myLayout.menuType].width,
    };

    let position = myLayout.position;
    if (position === POSITION_LEFT) {
      style.left = 0;
    } else if (position === POSITION_RIGHT) {
      style.right = 0;
    }

    let topHeight = ( Studio.layoutUsingMap[RESERVE_LAYOUT_ELEMENT_ID_TOP] && Layout[RESERVE_LAYOUT_ELEMENT_ID_TOP].height ) || 0;
    let bottomHeight = ( Studio.layoutUsingMap[RESERVE_LAYOUT_ELEMENT_ID_BOTTOM] && Layout[RESERVE_LAYOUT_ELEMENT_ID_BOTTOM].height ) || 0;

    style.top = topHeight;
    style.bottom = bottomHeight;


    let chaningLayout = Layout[myLayout.chainingLayoutID];
    if( Studio.layoutUsingMap[myLayout.chainingLayoutID] && chaningLayout.float && !chaningLayout.flashHide ){
      style.zIndex = 2;
    }

    return (
      <div className={ classnames("studio-sidebar", draggableAimed && 'draggable-hovering')} style={style} onMouseEnter={:: this.onMouseEnter }>
        <MenuBar belongto={isa} Studio={Studio} Layout={Layout} Elastic={Elastic} Deployment={Deployment} uiiStatementIDList={uiiStatementIDList}/>
      </div>
    )
  }
}

// export const defaultWidth = 200;
export const POSITION_LEFT = 'left';
export const POSITION_RIGHT = 'right';
export const Menubar = MenubarEtc;
