import React, {Component} from 'react';
import PropTypes from 'prop-types';
import './MenuBar.scss'

import * as Slim from './Slim';
import * as Line from './Line';
import {connect} from 'react-redux';


export default class MenuBar extends Component {
  static propTypes = {
    isa : PropTypes.string,
    belongto : PropTypes.string.isRequired,
    Layout : PropTypes.object.isRequired,
    Studio : PropTypes.object.isRequired,
    Elastic: PropTypes.object.isRequired,
    Deployment: PropTypes.object.isRequired,
    uiiStatementIDList : PropTypes.array,
  }


  render(){
    let { belongto, Layout, Studio, Elastic, Deployment, uiiStatementIDList } = this.props;

    let matchedLayout = Layout[belongto];
    let menuType = matchedLayout.menuType;
    let MenuClass;
    if( menuType === MenubarTypeLine ){
      MenuClass = Line.default;
    } else if ( menuType === MenubarTypeSlim ){
      MenuClass = Slim.default;
    }

    return (
      <div className="studio-side-menubar">
        <MenuClass isa={this.props.isa} Elastic={Elastic} Deployments={Deployment} Layout={Layout} uiiStatementIDList={uiiStatementIDList} uiiCollection={Studio.uiiCollection}/>
      </div>
    )
  }
}

export const MenubarTypeSlim = 'slim';
export const MenubarTypeLine = 'line';
export const ConstantsOfType = {
  [MenubarTypeSlim] : Slim,
  [MenubarTypeLine] : Line,
}
