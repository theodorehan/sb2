import * as etc from './MenuBar';

export default from './MenuBar';
export const MenubarTypeLine = etc.MenubarTypeLine;
export const MenubarTypeSlim = etc.MenubarTypeSlim;
export const ConstantsOfType = etc.ConstantsOfType;

