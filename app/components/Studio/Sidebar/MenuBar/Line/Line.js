import React, {Component} from 'react';
import PropTypes from 'prop-types';
import ScrollArea from 'react-scrollbar';
import classnames from 'classnames';

import './Line.scss';

import UII from './UII';
import Draggable from '../../../../../Core/Components/Draggable';

export default class Line extends Component {

  static propTypes = {
    isa : PropTypes.string,
  }

  static contextTypes = {
    zone : PropTypes.object,
  }

  render(){
    let {uiiStatementIDList, uiiCollection, Elastic, Deployments, Layout} = this.props;

    let style = {
      width : width,
    };

    let belongedLayout = Layout[this.context.zone.layoutISA];

    let chainingLayoutID = belongedLayout.chainingLayoutID;
    let associatedElastic = null;
    if( chainingLayoutID ){

      // let chainedLayout = Layout[chainingLayoutID];
      let deployedElasticID = Deployments[chainingLayoutID];
      let elastic = Elastic[deployedElasticID];


      associatedElastic = elastic;
    }

    return (
      <div className={classnames("menu-bar-line", this.props.isa)} style={style}>
        <ScrollArea horizontal={true} style={{height:'100%'}}>

        { uiiStatementIDList.map(
          (uiiStatementID) =>
            <Draggable
              component={UII}

              _draggableType="uii"
              _draggableData={{uiiID:uiiStatementID}}
              _additionalStyle={{display:'block'}}

              uiiID={uiiStatementID}
              uiiCollection={uiiCollection}
              key={uiiStatementID}
              associatedElastic={associatedElastic}/>
        )}
        </ScrollArea>
      </div>
    )
  }
}


export const width = 20;
