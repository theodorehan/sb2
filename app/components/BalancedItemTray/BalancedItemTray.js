import React,  {Component} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import './style.scss';


let BalancedItemElementWrapper = (props)=> <div className="balanced-item" style={{width:props.widthPercent + '%'}}>
  { props.element }
</div>;


class BalancedItemTray extends Component {
  constructor(props){
    super(props);
  }

  renderChildren(){
    if( this.props.children ){
      if( typeof this.props.children.map === 'function' ){
        let step = 100 / this.props.children.length;

        return this.props.children.map((element, i)=> <BalancedItemElementWrapper element={element} widthPercent={step} key={i}/>)
      }

      return <BalancedItemElementWrapper element={this.props.children} widthPercent={100}/>
    }
  }

  render(){


    return <div className={ classnames("balanced-item-tray", this.props.className) }>
      {this.renderChildren()}
    </div>
  }
}

export default BalancedItemTray;
