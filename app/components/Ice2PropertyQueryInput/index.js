import React, {Component} from 'react';
import PropTypes from 'prop-types';

import {
  Input,
  Tooltip,
  Select,
  Icon,
  DatePicker,
  Button,
} from 'antd';
const InputGroup = Input.Group;
const Option = Select.Option;
import './style.scss';

import {
  VALUE_TYPES,
} from "../ICE2Elements/constants"

import Ice2Input from '../ICE2Elements/Input';
import {classifyProperties} from "../ICE2Elements/helpers";

export default class Ice2PropertyQueryInput extends Component {
  static propTypes = {
    schema : PropTypes.object,
    tid : PropTypes.string,
    pid : PropTypes.string,
    style : PropTypes.object,
    onSearch : PropTypes.func,
    onClose : PropTypes.func,
    onActivate : PropTypes.func,
    disabled : PropTypes.bool,
    onInput : PropTypes.func,
    value : PropTypes.any,
    method : PropTypes.string,
    locale : PropTypes.string,
    onChangeMethod : PropTypes.func,
    onChangeLocale : PropTypes.func,
    fixed : PropTypes.bool,
  }

  static defaultProps = {
    fixed : false,
  }

  static contextTypes = {
    intl : PropTypes.object,
    ice2 : PropTypes.func,
  }


  constructor(props){
    super();

    this.state = {
      schema : props.schema,
    }
  }


  onInput(e){
    let {nativeEvent} = e

    this.props.onInput && this.props.onInput(nativeEvent.target.value);
  }

  onChange(val){

    console.log(val);
    this.props.onInput && this.props.onInput(val);
  }

  onInputKeyDown(e){
    let {nativeEvent} = e;

    if( nativeEvent.keyCode === 13 ){
      this.props.onEnter && this.props.onEnter();
    }
  }

  async componentDidMount(){

    let propType = this.state.schema;
    if( !this.state.schema ){
      propType = await this.context.ice2.loadPropertyType(this.props.tid, this.props.pid);

      this.setState({schema : propType});
    }


    if( propType.valueType && propType.valueType.value == 'REFERENCE' ){
      let referenceNodeType = await this.context.ice2.loadNodeType(propType.referenceType.value);
      let classification = classifyProperties(referenceNodeType.propertyTypes);


      let listRes = await this.context.ice2.request({
        url : `/node/${propType.referenceType.value}/list.json`,
        method:'get',
        params : {
          upperCode_matching : propType.codeFilter,
        },
      })

      this.setState({ optionItems : listRes.data.items, optionClassification : classification });
    }
  }

  getInputFieldTitle(){
    switch(this.props.method){

      case "matching":
      case "notMatching":
        if( this.state.schema.valueType ){
          if( this.state.schema.valueType.value === VALUE_TYPES.REFERENCE || this.state.schema.valueType.value === VALUE_TYPES.REFERENCES ){
            return this.context.intl.formatMessage({id : 'app.ice2table.search-guide-reference'});
          }
        }

        return null;
      case "label_matching" :
      case "label_notMatching" :
      case "wildcard":
        return null;
    }
  }

  getOptionLabel(item){
    let labelable = this.state.optionClassification.LABELABLE[0];

    if(labelable){
      if( item[labelable.pid] && typeof item[labelable.pid] == 'object' ){
        return item[labelable.pid].label;
      } else {
        return item[labelable.pid];
      }
    }

    return this.getOptionValue(item);
  }

  getOptionValue(item){
    let idable = this.state.optionClassification.IDABLE;

    return idable.map((prop) => (item[prop.pid] && typeof item[prop.pid] == 'object') ? item[prop.pid].value : item[prop.pid] ).join('>')
  }

  renderInput(){

    switch (this.state.schema.valueType && this.state.schema.valueType.value){
      case 'REFERENCE' :
      case 'REFERENCES' :
        let items = this.state.optionItems || [];


        return <div className="input-pair">
          <div className="input-label">
            {this.state.schema.propertyTypeName}
          </div>
          <Select
            mode="combobox"
            className="input-value"
            disabled={this.props.fixed}
            addonBefore={`${this.state.schema.propertyTypeName}`}
            style={{ width: '50%' , verticalAlign:'top', display:'table-cell'}}
            value={this.props.value}
            placeholder={this.props.pid}
            onKeyDown={:: this.onInputKeyDown}
            onChange={::this.onChange}>
            { items.map((item, i) => <Option key={i} value={this.getOptionValue(item)}>
                { this.getOptionLabel(item) }
            </Option>)}
          </Select>
        </div>
        break;
      default:
        return <Input
          disabled={this.props.fixed}
          addonBefore={`${this.state.schema.propertyTypeName}`}
          style={{ width: '50%' }}
          value={this.props.value}
          placeholder={this.props.pid}
          onKeyDown={:: this.onInputKeyDown}
          onInput={::this.onInput}/>
    }
  }



  renderLocale(){
    return <Select
      style={{minWidth:70}}
      defaultValue="kr"
      value={this.props.locale}
      onChange={this.props.onChangeLocale}
      disabled={this.props.fixed}>

      <Option value="kr">
        한국어
      </Option>

      <Option value="en">
        영어
      </Option>

      <Option
        className="advanced-option"
        value="cn">
        중국어
      </Option>

      <Option
        value="jp">
        일본어
      </Option>
    </Select>
  }

  render(){

    if( !this.state.schema ){
      return  <div className="Ice2PropertyQueryInput" style={{...this.props.style}}>
        Schema Loading...
      </div>;
    }



    return <div className="Ice2PropertyQueryInput" style={{...this.props.style}}>


      <div className="input-group-wrapper">
        <Tooltip title={ this.props.disabled && this.context.intl.formatMessage({id : 'app.ice2table.is-disabled'})}>

          { this.props.disabled || <Tooltip title={this.getInputFieldTitle()}>
            { this.renderInput() }
          </Tooltip>}


          {/*<Ice2Input*/}
            {/*withLabel*/}
            {/*propertyType={this.state.schema}*/}
            {/*tid={this.props.tid}*/}
            {/*fluid={false}*/}
            {/*value={value}*/}
            {/*onChange={(prop, data) => this.onChange(data) }*/}
            {/*pid={this.props.pid}/>*/}



          <div className="options-wrapper">
            <InputGroup compact>
              { this.props.disabled && <Button type="primary" icon="caret-right" onClick={this.props.onActivate}>
                {`${this.state.schema.propertyTypeName}`} {this.context.intl.formatMessage({id : 'app.ice2table.query-activate'})}
              </Button> }

              {this.props.disabled || <Select
                style={{minWidth:70}}
                defaultValue="wildcard"
                value={this.props.method}
                onChange={this.props.onChangeMethod}
                disabled={this.props.fixed}>

                <Option value="wildcard">
                  디비검색
                </Option>

                <Option value="matching">
                  일반
                </Option>

                <Option
                  className="advanced-option"
                  value="label_matching">
                  (문자열)
                </Option>

                <Option
                  value="notMatching">
                  제외
                </Option>

                <Option
                  className="advanced-option"
                  value="label_notMatching">
                  (문자열)
                </Option>

                <Option value="above">이상</Option>
                <Option value="below">이하</Option>
              </Select>}

              {
                this.state.schema.i18n && this.renderLocale()
              }


              {this.props.disabled || this.props.fixed || <Tooltip title={this.context.intl.formatMessage({id : 'app.ice2table.search-single-condition'},{field : this.state.schema.propertyTypeName})}>
                <Button type="primary" icon="search" onClick={this.props.onSearch} disabled={this.props.fixed}>
                  Search
                </Button>
              </Tooltip>}

              { this.props.fixed || <Button type="danger" icon="close" onClick={this.props.onClose}/>}

              { this.props.fixed && <Button> 고정값 </Button>}
            </InputGroup>
          </div>
        </Tooltip>
      </div>
    </div>;
  }
}


export function interpretQueryToPropsGroupList(queryObject = {}, classification){

  let searches = [];
  let filterKeys = Object.keys(queryObject);
  let filterKey, filterKeyTokens;
  for(let i =0; i < filterKeys.length; i++ ){
    filterKey = filterKeys[i];
    filterKeyTokens = filterKey.split('_');


    /**
     * UX Config 로 설정된 filterObject의 key/value 를 table의 search 설정으로 변환하여 세팅한다.
     * filterObject 의 키는 PID | PID_METHOD | PID_LOCALE_METHOD 의 형태를 가질 수 있다.
     **/
    if( filterKeyTokens.length == 1 ){
      searches.push({
        pid : filterKeyTokens[0],
        method : 'matching',
        schema : classification ? classification.map[filterKeyTokens[0]] : null,
        value : queryObject[filterKey],
        fixed : true,
      });
    } else if (filterKeyTokens.length == 2){
      searches.push({
        pid : filterKeyTokens[0],
        method : filterKeyTokens[1],
        schema : classification ? classification.map[filterKeyTokens[0]] : null,
        value : queryObject[filterKey],
        fixed : true,
      });
    } else if (filterKeyTokens.length == 3){
      searches.push({
        pid : filterKeyTokens[0],
        locale : filterKeyTokens[1],
        method : filterKeyTokens[2],
        schema : classification ? classification.map[filterKeyTokens[0]] : null,
        value : queryObject[filterKey],
        fixed : true,
      });
    } else {
      throw new Error(`UX Config logicFilter Error : filterKey pattern must be [pid]_{[locale]}_{[method]}. ${filterKey}`);
    }
  }


  return searches;
}
