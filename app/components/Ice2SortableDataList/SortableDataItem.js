import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import DeleteIcon from 'material-ui/svg-icons/content/delete-sweep';
import UpIcon from 'material-ui/svg-icons/navigation/expand-less';
import DownIcon from 'material-ui/svg-icons/navigation/expand-more';

export default class SortableDataItem extends Component {
  static propTypes = {
    index: PropTypes.number.isRequired,
    id: PropTypes.any.isRequired,
    component : PropTypes.func.isRequired,
    dragGroupId : PropTypes.string.isRequired,
    onStartedDrag : PropTypes.func,
    onFinishedDrag : PropTypes.func,
    onDelete: PropTypes.func,
    onUp: PropTypes.func,
    onDown : PropTypes.func,
    rightOptions : PropTypes.array,
    leftOptions : PropTypes.array,
  };

  static defaultProps = {
    rightOptions : [],
    leftOptions : [],
  }


  static contextTypes = {
    intl : PropTypes.object,
    ice2 : PropTypes.func,
    functionStore : PropTypes.object,
    UIISystems : PropTypes.object,
  }

  constructor(props){
    super(props);

    this.state = {
      isDragging : false,
    }
  }

  onDragStart(e){
    e.dataTransfer.setData('text', this.props.dragGroupId);

    if( this.props.onStartedDrag ) this.props.onStartedDrag(this.props.item,e);

    this.setState({
      isDragging: true,
    })
  }

  onEndDrag(e){
    if( this.props.onFinishedDrag ) this.props.onFinishedDrag(this.props.item,e);
    this.setState({
      isDragging: false,
    })
  }

  onDelete(){

    let okBtn = this.context.UIISystems.getUnique();
    let noBtn = this.context.UIISystems.getUnique();
    this.context.UIISystems.openTool('Dialog', this.props.defaultPopupTarget || '[modal]',{
      width:500,
      height:150,
    }, {
      title : this.context.intl.formatMessage({id : 'app.common.confirm'}) ,
      fields : [
        {
          type : 'label',
          contents :  this.context.intl.formatMessage({id : 'app.sortable-item-list.confirm-delete'}, {itemIds:2}),
        },
        {
          type : 'button-set',
          buttons : [
            {
              label : this.context.intl.formatMessage({id : 'app.common.no'}) ,
              functionKey : noBtn,
              level : 'negative',
            },
            {
              label : this.context.intl.formatMessage({id : 'app.common.yes'}),
              functionKey : okBtn ,
              level : 'positive',
            },
          ],
        },
      ],
    }).then((closer) => {
      this.context.functionStore.register(okBtn, ()=> {
        this.props.onDelete && this.props.onDelete(this.props.item, this.props.index);
        closer();
      });

      this.context.functionStore.register(noBtn, () => {
        closer();
      });
    });





  }

  onUp(){
    this.props.onUp && this.props.onUp(this.props.item, this.props.index);
  }

  onDown(){
    this.props.onDown && this.props.onDown(this.props.item, this.props.index);
  }

  render() {
    const { component:ItemComponent, item } = this.props;

    return <div
      className={classnames('item', this.state.isDragging && 'dragging')}
      draggable="true"
      data-drag-group-id={this.props.dragGroupId}
      onDragStart={:: this.onDragStart}
      onDragEnd={:: this.onEndDrag}>
      <div className="left">
        {/*<button className="action" onClick={:: this.onUp}>*/}
          {/*<UpIcon style={{color:'inherit', width:20, height:20}}/>*/}
        {/*</button>*/}
        <div className="num"> {this.props.index + 1} </div>
        {/*<button className="action" onClick={:: this.onDown}>*/}
          {/*<DownIcon style={{color:'inherit', width:20, height:20}}/>*/}
        {/*</button>*/}
        { this.props.leftOptions.map((option, i) => <button
          key={i}
          className="action"
          onClick={
            async () => {
              await option.click(this.props.item, this.props.index);

              console.log('clickckckckk')
              this.forceUpdate()
            }
          }>
            {React.createElement(option.iconClass, { style : {color:'inherit', width:20, height:20} })}
        </button>) }
      </div>

      <div className="item-component-wrapper">
        {
          ItemComponent({item, index:this.props.index})
        }

      </div>
      <div className="right">
        { this.props.rightOptions.map((option, i) =>
          <button
            key={i}
            className="action"
            onClick={
              () => option.click(this.props.item, this.props.index).then(() => this.forceUpdate())
            }>
            {React.createElement(option.iconClass, { style : {color:'inherit', width:20, height:20} })}
          </button>) }

        <button className="action" onClick={:: this.onDelete}>
          <DeleteIcon style={{color:'inherit', width:20, height:20}}/>
        </button>
      </div>
    </div>
  }
}

