import React, {Component} from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';
import pullAt from 'lodash/pullAt';
import concat from 'lodash/concat';
import get from 'lodash/get';
import Promise from 'bluebird';


import EyeIcon from 'material-ui/svg-icons/action/visibility';
import PencilIcon from 'material-ui/svg-icons/content/create';

import SortableItemList from './SortableItemList';
import './ReferencesSortableDataList.scss';

import Ice2NodeThumbnailDisplay from '../ICE2Elements/Display/nodeThumbnail';

import Ice2Input , {TABLE_STYLE_SUPPORT} from '../ICE2Elements/Input';

import {    } from 'antd';

import ReferenceItemWrapper from './referenceItemWrapper';

import {classifyProperties} from "../ICE2Elements/helpers";
import {interpretQueryToPropsGroupList} from "../Ice2PropertyQueryInput";
import {Label, Title, Contents, SementicLayout} from "../SementicLayout/index";

export default class ReferencesSortableDataList extends Component {

  static contextTypes = {
    ice2 : PropTypes.func,
    UIISystems : PropTypes.object,
    functionStore : PropTypes.object,
  }

  static propTypes = {
    onChange : PropTypes.func,
    dataUrl : PropTypes.string,
    orderModifyUrl : PropTypes.string,
    idFieldName : PropTypes.string,
    saveFields : PropTypes.array,




    // New
    nodeTypeTid : PropTypes.string.isRequired,
    retrieveParams : PropTypes.object.isRequired,
    retrieveType : PropTypes.oneOf(['oneOfList', 'read']),

    referencesPid : PropTypes.string.isRequired,
    referencesNodeTypeTid : PropTypes.string.isRequired,
    referencesNodeTypeIdPid : PropTypes.string.isRequired,
    referenceCustomUxRuleMap : PropTypes.object,

    referenceItemThumbnailDisplayRule : PropTypes.object.isRequired,

    managingPids : PropTypes.array,
    itemHelper : PropTypes.object,
    addRules : PropTypes.array,

    listManagementTitle : PropTypes.any,

    itemOptionRenderer : PropTypes.func,
  }

  static defaultProps = {
    managingPids : [],
  }


  constructor(props){
    super(props);



    this.state = {
      nodeItem: {},
      referenceItems : [],
      managingPropertyValues : {

      },
    };
  }

  async apply(){
    let search = new URLSearchParams();


    let nodeType = await this.context.ice2.loadNodeType(this.props.nodeTypeTid);
    let referenceNodeType = await this.context.ice2.loadNodeType(this.props.referencesNodeTypeTid);

    let classification = classifyProperties(nodeType.propertyTypes);
    let referenceClassification = classifyProperties(referenceNodeType.propertyTypes);

    let primaryProp = classification.primaryProperty;

    let saveObj = {};


    if( this.state.nodeItem[primaryProp.pid] ){
      saveObj[primaryProp.pid] = this.state.nodeItem[primaryProp.pid];
    }



    let {
      IDABLE : referenceIdableProps,
    } = referenceClassification;
    saveObj[this.props.referencesPid] = this.state.referenceItems.filter((item) => !!item).map((item) => {


      if( referenceIdableProps.length > 1 ){
        return referenceIdableProps.map((idableProp) => item[idableProp.pid] ).join('>');
      } else {
        return item[this.props.referencesNodeTypeIdPid];
      }
    }).join(',');



    let fixParams = this.props.retrieveParams;
    let fixParamsKey = Object.keys(fixParams);


    let key;
    for(let i = 0; i < fixParamsKey.length; i++ ){
      key = fixParamsKey[i];
      saveObj[key] = fixParams[key];
    }



    let value;
    for(let i = 0; i < this.props.managingPids.length ; i++ ){
      value = this.state.managingPropertyValues[this.props.managingPids[i]];
      if( value && value.value ){
        value = value.value;
      }

      if( value ){
        saveObj[this.props.managingPids[i]] = value;
      }
    }

    let saveObjKeys = Object.keys(saveObj);
    for(let i =0; i < saveObjKeys.length; i++ ){
      search.append(saveObjKeys[i], saveObj[saveObjKeys[i]]);
    }




    let saveRes = await this.context.ice2.post(`/node/${this.props.nodeTypeTid}/save.json`,search.toString())



    return { item : saveRes.data.item , id : saveRes.data.item[primaryProp.pid]};
  }


  newItem(rule){
    let selectNodeType = this.props.referencesNodeTypeTid
    if( this.props.itemHelper ){
      if( this.props.itemHelper.type === 'mapped'){
        selectNodeType = this.props.itemHelper.windowNodeTypeTid;
      }
    }

    const submitCallbackKey = this.context.UIISystems.getUnique();
    this.context.ice2.loadNodeType(selectNodeType)
      .then((nodeType) => {

        let selectFuncKey = this.context.UIISystems.getUnique();
        this.context.UIISystems.openTool('ICE2Data', this.props.defaultPopupTarget || '[modal]' , {
          width:1024,
          height:600,
          UNIQUE_LAYER : true,
        }, {
          // disableRegister : true,
          title : '신규 등록',
          customPropertyUxMap:{
            ...rule.propertyUXMap,
            ...this.props.referenceCustomUxRuleMap,
          },
          nodeType : nodeType,
          type : 'create',
          footerBtnCallbacks : {
            'submit' : submitCallbackKey,
          },
        }).then((closer) => {


          this.context.functionStore.register(submitCallbackKey, (item)=>{


            if( this.props.itemHelper ){
              if( this.props.itemHelper.type === 'mapped'){
                let paramRule = this.props.itemHelper.paramRule;
                let paramRuleKeys = Object.keys(paramRule);

                Promise.map([item], (item) => {
                  let blocking = 'asd';

                  return new Promise((resolve, reject) => {
                    let usp = new URLSearchParams();
                    let key,value;
                    for(let i = 0; i < paramRuleKeys.length; i++ ){
                      key = paramRuleKeys[i];
                      value = paramRule[key];

                      if( /^\$/.test(key) ){
                        key = key.replace(/^\$/, '');

                        switch(value){
                          case '$selectedItemId' :
                            value = item[`${selectNodeType}Id`];
                            break;
                          default :
                            value = get(item, value);
                        }
                      }

                      usp.append(key, value);
                    }

                    this.context.ice2.post(`/node/${this.props.referencesNodeTypeTid}/save.json`, usp.toString())
                      .then((res) => {
                        resolve(res.data.item);
                      });
                  });
                }).spread((...items) => {
                  // console.log(items, items.map((item) => item[this.props.referencesNodeTypeIdPid]))
                  this.setState({
                    referenceItems : [
                      ...this.state.referenceItems,
                      ...items,
                    ],
                  })
                  closer();
                })
              }
            } else {
              this.setState({
                referenceItems : [
                  ...this.state.referenceItems,
                  item,
                ],
              })
              closer();
            }
          })
        })
      });
  }


  removeAll(){
    this.setState({
      referenceItems : [],
    })
  }


  addItem({
    name,
    itemFilter, //  목록을 조회할 때 사용될 API 파라미터
  }){

    let selectNodeType = this.props.referencesNodeTypeTid
    if( this.props.itemHelper ){
      if( this.props.itemHelper.type === 'mapped'){
        selectNodeType = this.props.itemHelper.windowNodeTypeTid;
      }
    }






    this.context.ice2.loadNodeType(selectNodeType)
      .then((nodeType) => {


        let defaultSearches = null;
        // rule.itemFilter
        if( itemFilter ){
          let classification = classifyProperties(nodeType.propertyTypes);
          defaultSearches = interpretQueryToPropsGroupList(itemFilter, classification);

        } else {
          defaultSearches = [];
        }



        let selectFuncKey = this.context.UIISystems.getUnique();
        this.context.UIISystems.openTool('ICE2Data', this.props.defaultPopupTarget || '[modal]' , {
          width:1024,
          height:600,
          UNIQUE_LAYER : true,
        }, {
          selectable : true,
          multiSelection : true,
          disableRegister : true,
          customPropertyUxMap:this.props.referenceCustomUxRuleMap,
          // disableRegister : true,
          title : name || '저장소',
          nodeType : nodeType,
          type : 'list',
          footerBtnCallbacks : {
            'select' : selectFuncKey,
          },
          defaultSearches,
        }).then((closer) => {
          this.context.functionStore.register(selectFuncKey, (items)=>{



            if( items.length > 0 ){

              if( this.props.itemHelper ){
                if( this.props.itemHelper.type === 'mapped'){
                  let paramRule = this.props.itemHelper.paramRule;
                  let paramRuleKeys = Object.keys(paramRule);

                  Promise.map(items, (item) => {
                    let blocking = 'asd';

                    return new Promise((resolve, reject) => {
                      let usp = new URLSearchParams();
                      let key,value;
                      for(let i = 0; i < paramRuleKeys.length; i++ ){
                        key = paramRuleKeys[i];
                        value = paramRule[key];

                        if( /^\$/.test(key) ){
                          key = key.replace(/^\$/, '');

                          switch(value){
                            case '$selectedItemId' :
                              value = item[`${selectNodeType}Id`];
                              break;
                            default :
                              value = get(item, value);
                          }
                        }

                        usp.append(key, value);
                      }

                      this.context.ice2.post(`/node/${this.props.referencesNodeTypeTid}/save.json`, usp.toString())
                        .then((res) => {
                          resolve(res.data.item);
                        });
                    });
                  }).spread((...items) => {
                    // console.log(items, items.map((item) => item[this.props.referencesNodeTypeIdPid]))
                    this.setState({
                      referenceItems : [
                        ...this.state.referenceItems,
                        ...items,
                      ],
                    })
                    closer();
                  })
                }
              } else {
                this.setState({
                  referenceItems : [
                    ...this.state.referenceItems,
                    ...items,
                  ],
                })
                closer();
              }
            } else {
              message.error('아이템이 선택되지 않았습니다.');
            }
          })
        })
      });
  }

  changeManagingPropValue(pid, value){
    this.setState({
      managingPropertyValues : {
        ...this.state.managingPropertyValues,
        [pid] : value,
      },
    })
  }

  onChangeArray(items){
    this.setState({
      referenceItems : items,
    })
  }

  load(){
    this.context.ice2.get(this.props.retrieveType == 'oneOfList' ? `/node/${this.props.nodeTypeTid}/list.json` :  `/node/${this.props.nodeTypeTid}/read.json`,{
      params : {
        referenceView : true,
        ...this.props.retrieveParams,
      },
    })
      .then((res) => {

        let nodeItem;
        if( this.props.retrieveType === 'oneOfList' ){
          nodeItem = res.data.items[0] || {};
        } else if ( this.props.retrieveType === 'read' ){
          nodeItem = res.data.item;
        }


        let managingPropertyValues = {};



        let value;
        for(let i = 0; i < this.props.managingPids.length; i++ ){
          value = nodeItem[this.props.managingPids[i]];


          if( value ){
            managingPropertyValues[this.props.managingPids[i]] = value;
          } else {

            // retrieveParams 의 값으로 기본값을 정해준다.
            if( this.props.retrieveParams[this.props.managingPids[i]] ){
              managingPropertyValues[this.props.managingPids[i]] = this.props.retrieveParams[this.props.managingPids[i]];
            }
          }
        }



        this.setState({
          loaded : true,
          nodeItem : nodeItem,
          managingPropertyValues,
          // references 는 item 내에 item 으로 실제 아이템오브젝트가 담긴다.
          referenceItems : (nodeItem[this.props.referencesPid] || []).map((item) => item.item) || [],
        })
      });
  }

  getItemRightOptions(item){
    return [
      { iconClass : PencilIcon, click : async (targetItem, targetIndex) => new Promise((resolve, reject) => this.context.ice2.loadNodeType(this.props.referencesNodeTypeTid)
          .then((nodeType) => {

            let submit = this.context.UIISystems.getUnique();
            this.context.UIISystems.openTool('ICE2Data', this.props.defaultPopupTarget || '[modal]' , {
              width:600,
              height:600,
              UNIQUE_LAYER : true,
            }, {
              nodeType : nodeType,
              type : 'modify',
              target : {
                [this.props.referencesNodeTypeIdPid] : item[this.props.referencesNodeTypeIdPid],
              },
              footerBtnCallbacks : {
                submit,
              },
            }).then((closer) => {
              this.context.functionStore.register(submit, (resultItem)=>{
                // console.log(this.state.referenceItems, targetItem, targetIndex, resultItem);

                this.setState({
                  referenceItems : [
                    ...this.state.referenceItems.slice(0, targetIndex),
                    resultItem,
                    ...this.state.referenceItems.slice(targetIndex+1),
                  ],
                })



                // this.load();
                closer();

                resolve(true);
              })
            })
          })) ,
      },
      { iconClass : EyeIcon, click : async () => this.context.ice2.loadNodeType(this.props.referencesNodeTypeTid)
        .then((nodeType) => {

          this.context.UIISystems.openTool('ICE2Data', this.props.defaultPopupTarget || '[modal]' , {
            width:600,
            height:600,
            UNIQUE_LAYER : true,
          }, {
            nodeType : nodeType,
            type : 'itemView',
            target : {
              [this.props.referencesNodeTypeIdPid] : item[this.props.referencesNodeTypeIdPid],
            },
          })
        }),
      },
    ]
  }

  componentDidMount(){

    this.load();
  }

  componentDidUpdate(oldProps){

  }

  renderFootActions(){


    if( this.props.addRules ){
      return <div className="foot-actions">
        { this.props.addRules.map((rule, i) => <div
          key={i}
          className="action"
          onClick={() => {
            switch (rule.method){
              case "create" :
                return this.newItem(rule);
              case "retrieve" :
                return this.addItem(rule);
              default:
                console.error('Unknown add rule.', rule);
            }
          }}>
          {rule.name}
        </div>)}


        <div className="action inline icon warning" onClick={:: this.removeAll}>
          <i class="fa fa-eraser" aria-hidden="true"></i>
        </div>
      </div>
    }

    return <div className="foot-actions">
      <div className="action" onClick={:: this.newItem}>
        신규 등록
      </div>
      <div className="action" onClick={:: this.addItem}>
        저장소
      </div>
      <div className="action inline icon warning" onClick={:: this.removeAll}>
        <i class="fa fa-eraser" aria-hidden="true"></i>
      </div>
    </div>
  }


  render(){
    this.inputs = {};

    return (
      <div className="references-sortable-data-list">

        {
          this.props.managingPids.length > 0 && <SementicLayout>
            <Title>
              정보
            </Title>
            <Contents style={TABLE_STYLE_SUPPORT}>
              { this.props.managingPids.map((pid) => <Ice2Input
                ref={(c) => this.inputs[pid] = c}
                renderAsTableRow
                withLabel
                key={pid}
                pid={pid}
                onChange={(prop, data) => this.changeManagingPropValue(pid, data) }
                tid={this.props.nodeTypeTid}
                value={this.state.managingPropertyValues && this.state.managingPropertyValues[pid]}
                style={{borderBottom: '1px solid #e6e6e6'}}/>) }
            </Contents>
          </SementicLayout>
        }


        <SementicLayout>
          {this.props.listManagementTitle}

          <Contents inStyle>
            <SortableItemList
              onChangeArray={:: this.onChangeArray }
              itemKeyFieldName={this.props.referencesNodeTypeIdPid}
              dragGroupId="t"
              referenceCustomUxRuleMap={this.props.referenceCustomUxRuleMap}
              items={this.state.referenceItems}
              getItemRightOptions={::this.getItemRightOptions}
              itemComponent={(props) => <ReferenceItemWrapper>
                <Ice2NodeThumbnailDisplay
                  tid={this.props.referencesNodeTypeTid}
                  pid={this.props.referencesNodeTypeIdPid}
                  id={props.item[this.props.referencesNodeTypeIdPid]}
                  data={props.item}
                  itemOptionElement={this.props.itemOptionRenderer && this.props.itemOptionRenderer(props.item)}
                  rule={this.props.referenceItemThumbnailDisplayRule}/>
              </ReferenceItemWrapper>}/>



            { this.renderFootActions() }
          </Contents>
        </SementicLayout>



      </div>
    );
  }
}




/**
 * Ex)
 props : {
            bannerAreaKey: {
                label: '배너 키',
                type: String,
                type2: 'string',
                default: function(){
                    return 'slide' + moment().format('YYYYMMDDhh:mm:ss');
                },
            },

            curation : {
                type : [String,Number,Boolean,Object],
                type2 : 'curation-source',
                'label' : '슬라이드 배너 관리',
                typeStructure: {
                    type: {
                        typeId: 'NodeCurationGroup',
                    },

                    refreshMethod: 'load',

                    targetLabel : '상품 목록 관리',

                    implementedGroupNodeType : 'nodeCurationGroup',
                    groupRetrieveType : 'oneOfList',
                    groupTargetParams : {
                        extraValue1 : '{ $.params.siteId || "mall" }',
                        extraValue2 :  'today-products',
                        extraValue3 : '{ $.props.bannerAreaKey || "common" }',
                    },
                    managingPids : [
//                        'title',
//                        'description',
                    ],

                    groupReferenceItemThumbnailDisplayRule : {
                        type : 'thumbnail',
                        idPid : 'urlBasedBannerId',
                        titlePid : 'title',
                        imagePid : 'file',
//                        subTitlePid : 'name',
//                        referencesPid : 'referenceItems',
                    },



                    // referenceThemes of themeCurationGroup
                    referencesPid : "referenceItems",

                    // referenceThemes to theme
                    referencesNodeTypeTid : "urlBasedBanner",

                    // themeId to theme
                    referencesNodeTypeIdPid : "urlBasedBannerId",

                    itemHelper : {
                        type : 'mapped',
                        windowNodeTypeTid : 'product', // 실제로 사용자에게 선택하게 할 노드타입 아이디.
                        paramRule : {
                            '$product' : '$selectedItemId',
                            '$title' : 'name',
                            'bannerConnectType' : 'product',
                            'urlTarget' : '_blank',
                            'bannerStatus' : 'y',
                        }
                    },
                }
            },

        },
 **/
