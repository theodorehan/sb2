import React, {Component} from 'react';
import PropTypes from 'prop-types';

import './referenceItemWrapper.scss';

export default class ReferenceItemWrapper extends Component {

  static propTypes = {
    item : PropTypes.object,
    children : PropTypes.any,
  }

  constructor(){
    super();
  }

  render(){
    return <div className="reference-item-wrapper">
      {this.props.children}
    </div>
  }
}
