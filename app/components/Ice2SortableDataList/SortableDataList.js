import React, {Component} from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';
import pullAt from 'lodash/pullAt';
import concat from 'lodash/concat';
import Promise from 'bluebird';

import SortableDataItem from './SortableDataItem';
import './SortableDataList.scss';

import {    } from 'antd';

import {
  checkItIsBoundary,
  detectVerticalArea,
  html5dragGetDraggableNodeInPath,
} from '../../utils/ElementsFindByCoordinate';


export default class SortableDataList extends Component {

  static contextTypes = {
    ice2 : PropTypes.func,
  }

  static propTypes = {
    onChange : PropTypes.func,
    dataUrl : PropTypes.string,
    orderModifyUrl : PropTypes.string,
    idFieldName : PropTypes.string,
    saveFields : PropTypes.array,

    itemComponent : PropTypes.func,
    itemKeyFieldName : PropTypes.string.isRequired,
    itemType : PropTypes.string.isRequired, // For DnD
    onSortTo : PropTypes.func,
  }


  constructor(props){
    super(props);



    this.state = {
      items: [],
      draggingState : {
        overIndex :null,
        vArea : null,
      },

      draggingItem : null,
      draggingItemIndex : null,
    };
  }



  onDragOver(e){

    if( !this.state.draggingItem ) return;
    // if( e.dataTransfer.getData('text') != this.props.itemType ) return;
    e.preventDefault();

    let dom,rect;
    for(let i = 0; i < this.itemDoms.length; i++ ){
      dom = ReactDOM.findDOMNode(this.itemDoms[i]);
      rect = dom.getBoundingClientRect();

      if( checkItIsBoundary(rect, e.clientX, e.clientY) ){
        return this.setState({
          draggingState : {
            overIndex : i,
            vArea : detectVerticalArea(rect, e.clientX, e.clientY ),
          },
        });
      }
    }

    this.setState({
      draggingState : {
        overIndex : null,
        vArea : null,
      },
    });
  }

  onDragLeave(){
    this.setState({
      draggingState : {
        overIndex : null,
        vArea : null,
      },
    });
  }

  onStartedDragItem(item, i){
    this.setState({
      draggingItem : item,
      draggingItemIndex : i,
    })
  }

  onFinishedDragItem(item, i){
    this.setState({
      draggingItem : null,
      draggingItemIndex : null,
    })
  }

  onDrop(e){
    e.preventDefault();

    let targetIndex, vArea;
    let dom,rect;
    for(let i = 0; i < this.itemDoms.length; i++ ){
      dom = ReactDOM.findDOMNode(this.itemDoms[i]);
      rect = dom.getBoundingClientRect();

      if( checkItIsBoundary(rect, e.clientX, e.clientY) ){
        targetIndex = i;
        vArea = detectVerticalArea(rect, e.clientX, e.clientY );
        break;
      }
    }


    if( this.state.draggingItemIndex !== targetIndex ){
      let from = this.state.draggingItemIndex;
      let to = targetIndex;

      if( from !== to && from !== to + vArea ){
        this.orderModify(this.state.items[targetIndex], this.state.draggingItemIndex, targetIndex);
      }
    }

    this.setState({
      draggingItem : null,
      draggingItemIndex : null,
      draggingState : {
        overIndex :null,
        vArea : null,
      },
    })
  }

  orderModify(item, from, to ){

    if( this.props.onSortTo) this.props.onSortTo(item, from, to);
    let items = Object.assign([], this.state['items_'+this.props.dataUrl]);
    // let url = this.props.orderModifyUrl;

    let fromItem = pullAt(items, from);

    let firstSection = items.slice(0,to);
    let lastSection = items.slice(to);

    items = concat(firstSection, fromItem, lastSection);

    items = items.map((item, i) => Object.assign({}, item, {
      sortOrder : i + 1,
    }));

    this.setState({
      ['items_'+this.props.dataUrl] : items,
    });
  }


  applyEachItemsToServer(){
    let {
      orderModifyUrl,
      idFieldName,
      saveFields,
    } = this.props;
    let items = this.state['items_'+this.props.dataUrl];

ㄴ
    return new Promise((s,j)=>{
      Promise.all(
        items.map((item)=>new Promise((s, j) => {
          let data = {};
          let dataUrlencode = `${idFieldName}=${item[idFieldName]}&`;
          data[idFieldName] = item[idFieldName];

          for(let i = 0; i < saveFields.length; i++ ){
            data[saveFields[i]] = item[saveFields[i]];
          }

          dataUrlencode += saveFields.map((key)=>{
            let value = item[key];
            if( value.value ){
              value = value.value;
            }

            return `${key}=${value}`;
          }).join('&')

          this.context.ice2.post(orderModifyUrl, dataUrlencode)
            .then((res) => {
              console.log('res', res.data);

              s();
            });
        }))
      ).then(()=>{
        s();
      })
    })
  }

  componentDidMount(){
    this.context.ice2.get(this.props.dataUrl, {
      params : {
        sorting : 'sortOrder',
      },
    })
      .then((res)=>{
        this.setState({
          ['items_'+this.props.dataUrl] : res.data.items || [],
        });
      });
  }

  componentDidUpdate(oldProps){

    if( this.state['items_'+this.props.dataUrl] ) return;

    if( this.props.dataUrl !== oldProps.dataUrl ){
      this.context.ice2.get(this.props.dataUrl, {
        params : {
          sorting : 'sortOrder',
        },
      })
        .then((res)=>{
          this.setState({
            ['items_'+this.props.dataUrl] : res.data.items || [],
          });
        });
    }
  }

  render(){
    const items = this.state['items_'+this.props.dataUrl] || [];
    this.itemDoms = [];

    let basketInMap;

    return (
      <div className="sortable-data-list" onDragOver={::this.onDragOver} onDragLeave={:: this.onDragLeave } onDrop={::this.onDrop}>
        {items.map((item, i) => {
          basketInMap = [<SortableDataItem
            onStartedDrag={() => this.onStartedDragItem(item, i) }
            onFinishedDrag={()=> this.onFinishedDragItem(item, i)}
            dragGroupId={this.props.itemType}
            ref={(dom) => this.itemDoms[i] = dom}
            itemType={this.props.itemType}
            key={item[this.props.itemKeyFieldName]}
            index={i}
            id={item[this.props.itemKeyFieldName]}
            item={item}
            component={this.props.itemComponent}
          />];


          if(
            this.state.draggingState.overIndex !== null &&
            this.state.draggingItemIndex !== this.state.draggingState.overIndex &&
            this.state.draggingItemIndex !== this.state.draggingState.overIndex + this.state.draggingState.vArea &&
            this.state.draggingState.overIndex === i ){

            if( this.state.draggingState.vArea === 1 ){
              basketInMap.push(<div className="cursor"/>)
            } else {
              basketInMap.unshift(<div className="cursor"/>)
            }
          }

          return basketInMap;
          }
        )}
      </div>
    );
  }
}
