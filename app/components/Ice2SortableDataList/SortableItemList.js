import React, {Component} from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';
import pullAt from 'lodash/pullAt';
import concat from 'lodash/concat';
import Promise from 'bluebird';

import SortableDataItem from './SortableDataItem';
import './SortableDataList.scss';

import {    } from 'antd';

import {
  checkItIsBoundary,
  detectVerticalArea,
  html5dragGetDraggableNodeInPath,
} from '../../utils/ElementsFindByCoordinate';


export default class SortableItemList extends Component {

  static contextTypes = {
    ice2 : PropTypes.func,
  }

  static propTypes = {
    onChange : PropTypes.func,
    dataUrl : PropTypes.string,
    orderModifyUrl : PropTypes.string,
    idFieldName : PropTypes.string,
    saveFields : PropTypes.array,

    itemComponent : PropTypes.func,
    itemKeyFieldName : PropTypes.string.isRequired,
    dragGroupId : PropTypes.string.isRequired, // For DnD
    onSortTo : PropTypes.func,


    getItemRightOptions : PropTypes.func,
    getItemLeftOptions : PropTypes.func,

    // New
    items : PropTypes.array.isRequired,
    onChangeArray : PropTypes.func,
  }

  static defaultPropTypes = {
    items : [],
  }


  constructor(props){
    super(props);



    this.state = {
      items: props.items,
      draggingState : {
        overIndex :null,
        vArea : null,
      },

      draggingItem : null,
      draggingItemIndex : null,
    };
  }


  onDragOver(e){

    if( !this.state.draggingItem ) return;

    e.preventDefault();

    let dom,rect;
    for(let i = 0; i < this.itemDoms.length; i++ ){
      dom = ReactDOM.findDOMNode(this.itemDoms[i]);
      rect = dom.getBoundingClientRect();

      if( checkItIsBoundary(rect, e.clientX, e.clientY) ){
        return this.setState({
          draggingState : {
            overIndex : i,
            vArea : detectVerticalArea(rect, e.clientX, e.clientY ),
          },
        });
      }
    }

    this.setState({
      draggingState : {
        overIndex : null,
        vArea : null,
      },
    });
  }

  onDragLeave(){
    this.setState({
      draggingState : {
        overIndex : null,
        vArea : null,
      },
    });
  }

  onStartedDragItem(item, i){
    this.setState({
      draggingItem : item,
      draggingItemIndex : i,
    })
  }

  onFinishedDragItem(item, i){
    this.setState({
      draggingItem : null,
      draggingItemIndex : null,
    })
  }

  onDrop(e){
    e.preventDefault();

    let targetIndex, vArea;
    let dom,rect;
    for(let i = 0; i < this.itemDoms.length; i++ ){
      dom = ReactDOM.findDOMNode(this.itemDoms[i]);
      rect = dom.getBoundingClientRect();

      if( checkItIsBoundary(rect, e.clientX, e.clientY) ){
        targetIndex = i;
        vArea = detectVerticalArea(rect, e.clientX, e.clientY );
        break;
      }
    }


    if( this.state.draggingItemIndex !== targetIndex ){
      let from = this.state.draggingItemIndex;
      let to = targetIndex;

      if( from !== to && from !== to + vArea ){
        this.orderModify(this.state.items[targetIndex], this.state.draggingItemIndex, targetIndex);
      }
    }

    this.setState({
      draggingItem : null,
      draggingItemIndex : null,
      draggingState : {
        overIndex :null,
        vArea : null,
      },
    })
  }

  orderModify(item, from, to ){

    if( this.props.onSortTo) this.props.onSortTo(item, from, to);
    let items = Object.assign([], this.state.items);

    let fromItem = pullAt(items, from);

    let firstSection = items.slice(0,to);
    let lastSection = items.slice(to);

    items = concat(firstSection, fromItem, lastSection);

    this.setState({
      items,
    });


    this.props.onChangeArray && this.props.onChangeArray(items);
  }

  onDeleteItem(item, index){
    let items = [
      ...this.state.items.slice(0, index),
      ...this.state.items.slice(index+1),
    ]

    this.setState({
      items,
    });

    this.props.onChangeArray && this.props.onChangeArray(items);
  }



  componentWillReceiveProps(nextProps){
    this.setState({
      items : nextProps.items,
    })
  }

  // componentDidUpdate(oldProps, oldState){
  //   // items 변경시 새로 세팅
  //
  //   if( oldProps.items !== this.state.items ){
  //
  //     console.log('변경됨');
  //     console.log('asdasd', oldProps, oldState, this.props, this.state);
  //     this.setState({
  //       items : this.props.items,
  //     });
  //   }
  //
  // }




  render(){
    const items = this.state.items;
    this.itemDoms = [];

    let basketInMap;

    return (
      <div className="sortable-data-list" onDragOver={::this.onDragOver} onDragLeave={:: this.onDragLeave } onDrop={::this.onDrop}>
        {items.map((item, i) => {
            basketInMap = [item ? <SortableDataItem
              onStartedDrag={() => this.onStartedDragItem(item, i) }
              onFinishedDrag={()=> this.onFinishedDragItem(item, i)}
              dragGroupId={this.props.dragGroupId}
              ref={(dom) => this.itemDoms[i] = dom}
              key={item[this.props.itemKeyFieldName]}
              index={i}
              id={item[this.props.itemKeyFieldName]}
              item={item}
              component={this.props.itemComponent}
              leftOptions={this.props.getItemLeftOptions ? this.props.getItemLeftOptions(item) : []}
              rightOptions={this.props.getItemRightOptions ? this.props.getItemRightOptions(item) : []}
              onDelete={:: this.onDeleteItem}
            /> : ''];


            if(
              this.state.draggingState.overIndex !== null &&
              this.state.draggingItemIndex !== this.state.draggingState.overIndex &&
              this.state.draggingItemIndex !== this.state.draggingState.overIndex + this.state.draggingState.vArea &&
              this.state.draggingState.overIndex === i ){

              if( this.state.draggingState.vArea === 1 ){
                basketInMap.push(<div className="cursor" key='cursor'/>)
              } else {
                basketInMap.unshift(<div className="cursor" key='cursor'/>)
              }
            }

            return basketInMap;
          }
        )}
      </div>
    );
  }
}
