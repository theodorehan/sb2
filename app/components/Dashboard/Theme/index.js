import React, {Component} from 'react';
import PropTypes from 'prop-types';

import Slider from 'react-slick';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import {FormattedMessage} from 'react-intl';

import "./style.scss";

import {
  CreateOpenActionSheet as CreateDialogOpenActionSheet,
  ToolKey,
} from '../../Toolset/CoreTools/Dialog/Constants';

import {
  SET_STATIC_DATA_BY_LAYOUT,
  CLOSE_LAYOUT_WITH_ELASTIC,
} from '../../Toolset/UIISystemActionSheets';


// Actions
import * as UserActions from '../../../actions/User';
import * as DashboardActions from '../../../actions/Dashboard';

// Networks
import * as UserNetwork from '../../../Core/Network/User';
import * as DashboardNetwork from '../../../Core/Network/Dashboard';
import * as SiteNetwork from '../../../Core/Network/Site';

export default class ThemeItem extends Component {
  static contextTypes = {
    UIISystems : PropTypes.object,
    functionStore : PropTypes.object,
    router : PropTypes.object,
    intl : PropTypes.object,
    global : PropTypes.object,
  }

  constructor(props){
    super(props);
  }

  onCreateProjectFromTemplate(templateItem){

    let transferData = {
      title : this.context.intl.formatMessage({id:'app.dashboard.create-new-site'}),
      fields : [
        {
          label : this.context.intl.formatMessage({id:'app.common.site-name'}),
          key : 'siteName',
          type : 'input',
          inputType : 'string',
          placeholder : this.context.intl.formatMessage({id:'app.dashboard.input-site-name'}),
          defaultValue : '',
          // validator : {
          //   functionKey : 'pageNameValidator',
          // },
        },


        {
          type : 'button-set',
          buttons : [
            {
              label : this.context.intl.formatMessage({id:'app.common.create'}),
              functionKey : 'ok' ,
              level : 'positive',
              validate : true,
            },
            {
              label : this.context.intl.formatMessage({id:'app.common.cancel'}) ,
              functionKey : 'cancel',
              level : 'negative',
            },
          ],
        },
      ],
    };


    let modalID;
    this.context.UIISystems.emitActionSheet(CreateDialogOpenActionSheet("[modal]"), transferData, {
      width : 400,
      height: 200,

      onLayoutID : (id)=>{
        modalID = id;
      },
    });

    this.context.functionStore.register('ok', (field) => {
      let {siteName} = field;



      let reqID = this.context.global.uiBlockingReqBegin();
      DashboardNetwork.createSite(this.props.User.session.id, this.props.item.name, this.props.item.info.themeName,siteName.value)
        .then((res)=>{
          let {data} = res;
          let {siteAccessName} = data;
          alert('사이트 생성 완료');
          this.context.global.uiBlockingReqFinish(reqID);

          this.context.UIISystems.emitActionSheet(CLOSE_LAYOUT_WITH_ELASTIC(modalID, ToolKey, true));
          this.context.functionStore.remove('ok');

          this.context.router.history.push(`/studio?site=${siteAccessName}&owner=${this.props.User.info.id}`);

          return;
          // SiteNetwork.prebuildSite(this.props.User.session.id, siteAccessName)
          //   .then((res)=>{
          //     let {data:buildRes} = res;
          //     alert('사이트 생성 - 빌드 완료.');
          //     console.log('Site create build res data', data, buildRes);
          //     this.context.UIISystems.emitActionSheet(CLOSE_LAYOUT_WITH_ELASTIC(modalID, ToolKey, true));
          //     this.context.functionStore.remove('ok');
          //
          //     this.context.router.history.push(`/studio?site=${siteAccessName}`);
          //   })
        });
    });
  }

  render(){
    let {item} = this.props;
    var settings = {
      dots: false,
      infinite: true,
      speed: 500,
      // slidesToShow: 1,
      // slidesToScroll: 1,
      swipe : false,
      arrows : false,
      nextArrow:false,
      prevArrow : false,
      autoplay:true,
      autoPlay:true,
      autoPlaySpeed : 300,
      fade:true,
    };

    return <div className="photo-item">
      <div className="version">{item.version}</div>
      <div className="thumb">
        <Slider {...settings} style={{width:'100%'}}>
          {item.thumbnails.map(
            (i) => <div key={i} style={{height:'auto'}}>
              <img src={`/api/user/${this.props.User.session.id}/template/site-template-image?sitename=${item.name}&i=${i}`}/>
            </div>)
          }
        </Slider>

      </div>

      <div className="introduce">
        {item.name}
      </div>


      <div className="ui">
        <button onClick={()=> this.onCreateProjectFromTemplate(item) }>
          <FormattedMessage id="app.dashboard.create-new-site"/>
        </button>
      </div>
    </div>;
  }
}
