import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import ReactDOM from 'react-dom';
import { DateRange, Calendar } from 'react-date-range';
import moment from 'moment';
import Cleave from 'cleave.js/react';
import MaskedInput  from 'react-maskedinput';
import { Switch, Icon } from 'antd';

import './style.scss';

export default class DatePicker extends Component {
  static propTypes = {
    onChange : PropTypes.func,
    value : PropTypes.object,
  }

  static defaultProps = {
    value : {},
  }


  constructor(){
    super();


    this.state = {
      open : false,
      type : null,



      enableStart : false,
      enableEnd : false,
    };
  }


  handleSelect(values){




    if( this.state.type == 'range' ){
      let {
        startDate ,
        endDate,
      } = values;

      this.onChange({
        end : endDate,
        start : startDate,
      })

      this.setState({
        enableStart : true,
        enableEnd : true,
      });
    } else {


      this.onChange({
        [this.state.type] : values,
      });


      switch(this.state.type){
        case 'start':
          this.setState({
            enableStart : true,
          });
          break;
        case 'end':
          this.setState({
            enableEnd : true,
          });
          break;
      }
    }
  }

  open(type){
    this.setState({open:true, type});
  }

  close(e){

    e.preventDefault();
    e.stopPropagation();

    this.setState({
      open: false,
    });
  }

  onBlur(e){

    let {
      nativeEvent,
    } = e;

    if( nativeEvent.target.hasOwnProperty('value') ){
      this.onChange({[this.state.type]: this.onInputValueToMoment(e)})
    }

    if( nativeEvent.relatedTarget && nativeEvent.relatedTarget.className ){
      if( nativeEvent.relatedTarget.className.indexOf('rdr-MonthAndYear-button') > -1 ){
        nativeEvent.target.focus();
        return;
      }
    }

    this.setState({open:false});
  }

  onChange(set){

    // console.log(e, e.nativeEvent);
    this.props.onChange({
      ...this.props.value,
      ...set,
    });
  }

  onInputValueToMoment(e){
    let value = e.target.value;

    let matched = value.match(/^([_\d]+)-([_\d]+)-([_\d]+) ([_\d]+):([_\d]+):([_\d]+)$/);

    if( matched ){
      return moment(`${matched[1]}-${matched[2]}-${matched[3]} ${matched[4]}:${matched[5]}:${matched[6]}`);
    }


    return null;
    // this.onChange({
    //   start :moment(startDate),
    //   end :moment(endDate)
    // })
  }

  getStartValueString(_start){

    let start = _start || this.props.value.start;

    return `${start ? start.format("YYYY-MM-DD hh:mm:ss") : ''}`;
  }

  getEndValueString(_end){

    let end = _end || this.props.value.end;

    return `${end ? end.format("YYYY-MM-DD hh:mm:ss") : ''}`;
  }


  renderCalendar(){


    return <div onClick={(e) => e.preventDefault()} tabIndex={1}>
      {
        this.state.type == 'range' ? <DateRange
          startDate={this.props.value.start}
          endDate={this.props.value.end}
          onChange={::this.handleSelect}
        /> : <Calendar
          date={ this.state.type == 'start' ? this.props.value.start : this.props.value.end}
          onChange={::this.handleSelect}
        />
      }


      {/*<div className="actions">*/}
        {/*<button onClick={:: this.close}>Close</button>*/}
      {/*</div>*/}
    </div>
  }

  render(){
    return <fieldset className={classnames("date-picker", 'cal_'+ this.state.type)}>
      <div>

        <div className="input-wrap">
          <div className='input'>
            <div className='prefix'>
              시작일
            </div>
            <MaskedInput
              ref={(input) => this.startInput = input}
              name='start'
              value={this.getStartValueString()}
              mask="1111-11-11 11:11:11"
              placeholder="지정안됨"
              onFocus={ () => this.open('start') }
              onBlur={:: this.onBlur }/>
            <div className='tail'  onClick={ () => this.startInput.focus() }>
              <Icon type="calendar" />
            </div>

            {/*<Switch checked={this.state.enableStart} onChange={(checked) => this.setState({enableStart : checked})}/>*/}


          </div>

          {this.state.open && this.state.type == 'start' &&
            <div className="calendar">
              { this.renderCalendar() }
            </div>}
        </div>

        <div
          tabIndex='1'
          onFocus={ () => this.open('range') }
          onBlur={:: this.onBlur}
          className="length-setting" >

          <Icon
            type={ this.props.value && (this.props.value.start && this.props.value.end) ? "link" : "disconnect"  }/>
        </div>


        <div className="input-wrap">
          <div className='input'>
            <div className='prefix'>
              완료일
            </div>
            <MaskedInput
              ref={(input) => this.endInput = input}
              name='end'
              value={this.getEndValueString()}
              mask="1111-11-11 11:11:11"
              placeholder="지정안됨"
              onFocus={ () => this.open('end') }
              onBlur={:: this.onBlur}/>
           <div className='tail' onClick={ () => this.endInput.focus() }>
             <Icon type="calendar" />
           </div>

            {/*<Switch checked={this.state.enableEnd} onChange={(checked) => this.setState({enableEnd : checked})}/>*/}
          </div>
          { this.state.open && this.state.type == 'end' &&
            <div className="calendar">
              { this.renderCalendar() }
            </div>
          }
        </div>


        { this.state.open && this.state.type == 'range' &&  <div className="calendar">
          { this.renderCalendar() }
        </div>}
      </div>
    </fieldset>
  }
}
