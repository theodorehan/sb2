import React, {Component} from 'react';
import './style.scss';
import PropTypes from 'prop-types';
import classnames from 'classnames';

export default (props) =>   <div className='loading'>
    <div className="loading-eclipse" style={props.style}>
      <div className="loading-eclipse-inner">
        <svg width="100%" height="100%" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid">
          <path stroke="none" d="M10 50A40 40 0 0 0 90 50A40 42 0 0 1 10 50" fill="#ff8401" transform="rotate(222 50 51)">
            <animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 51;360 50 51" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite">

            </animateTransform>
          </path>

          <path stroke="none" d="M10 50A40 40 0 0 0 90 50A40 42 0 0 1 10 50" fill="#ffffff" transform="rotate(222 50 51)">
            <animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 51;360 50 51" keyTimes="0;1" dur=".5s" begin="0s" repeatCount="indefinite">

            </animateTransform>
          </path>
        </svg>
      </div>
    </div>



  {
    Array.isArray(props.texts) &&
      <SkySlider
        className='text-wrapper'
        interval={props.textInterval}
        items={props.texts}
        style={props.textStyle}
        renderer={(item, i) => <div className="text">{item}</div>}/>
  }

  </div>



class SkySlider extends Component {

  static propTypes = {
    style : PropTypes.object,
    className : PropTypes.string,
    items : PropTypes.array.isRequired,
    renderer :PropTypes.func,

    width : PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    height : PropTypes.oneOfType([PropTypes.number, PropTypes.string]),

    // 시간 간격
    interval : PropTypes.number,

  };

  static defaultProps = {
    width : '100%',
    height : 100,
    interval: 2000,
  }

  constructor(props){
    super();

    this.state = {
      cursor : 0,
    }

    this.circularizer = new Circularizer(props.items)
  }

  interval(){
    let nextCursor = this.state.cursor + 1;

    if( this.props.items.length <= nextCursor ){
      nextCursor = 0;
    }

    this.setState({cursor : nextCursor});
  }

  componentWillReceiveProps(props){
    this.circularizer = null;
    this.circularizer = new Circularizer(props.items)
  }

  componentDidMount(){
    this.itv = setInterval(() => this.interval(), this.props.interval);
  }

  componentWillUnmount(){
    clearInterval(this.itv);
  }

  render(){
    return <div className={classnames('sky-slider', this.props.className)} style={{ width : this.props.width, height:this.props.height, ...this.props.style}}>
      {
        this.props.items.map((item, i) => {
            let nearDirection = this.circularizer.nearDirection(this.state.cursor, i);

            return <div
              style={{
                opacity : this.state.cursor == i ? 1 : 0 ,
                transform: this.state.cursor == i ? 'translateX(0%)' : nearDirection == 'fore' ? 'translateX(-100%)' : 'translateX(100%)',
              }}
              className={classnames(
                "slide-item",
                this.state.cursor == i ? '' : nearDirection == 'fore' ? 'no-anime' : ''
              )}
              key={i}>
              {this.props.renderer(item, i)}
            </div>
          }
        )
      }


      <style>
        {`
        .sky-slider {
          overflow:hidden;
        }

        .sky-slider .slide-item {
          position:absolute;
          left:0;
          top:0;
          width:100%;
          display:inline-block;
          transition-duration : 1s;
          transition-property: transform, opacity;
          transition-timing-function: ease-in-out;

        }

        .sky-slider .slide-item.no-anime {
          transition : none;

        }
        `}
      </style>
    </div>
  }
}

class Circularizer {
  constructor(array){
    this.$arr = array;
  }

  nearDirection(base, target){
    let len = this.$arr.length;
    let half = len/2;
    let distance = (base - target);

    if( distance < 0 ){
      if( Math.abs(distance) > half ){
        return 'back';
      } else {
        return 'fore'
      }
    } else if( distance > 0 ) {
      if( Math.abs(distance) > half ){
        return 'fore';
      } else {
        return 'back'
      }
    } else {
      return 'here';
    }
  }
}


window.Circularizer = Circularizer;
