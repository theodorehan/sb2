import React from 'react';
import './style.scss';

export default (props) =>
  <div className="ci-wrapper" style={props.style}>
    <div className="ci-wrapper-inner">
      <div className="box box1"></div>
      <div className="box box2"></div>
      <div className="box box3"></div>
      <div className="box box4"></div>
      <div className="box box5"></div>
      <div className="box box6"></div>
      <div className="box box7"></div>
      <div className="box box8"></div>
      <div className="box box9"></div>
    </div>
  </div>;


