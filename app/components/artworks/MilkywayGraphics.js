// @flow
import React, { Component } from 'react';


export default class MilkywayGraphics extends Component {
  componentDidMount(){


    this.dom = this.refs.root;
    this.canvas = this.canvas;

    this.width = this.dom.offsetWidth;
    this.height = this.dom.offsetHeight;

    this.canvas.width = this.width;
    this.canvas.height = this.height;


    var resizer = () => {
      try {
        this.width = this.dom.offsetWidth;
        this.height = this.dom.offsetHeight;

        this.canvas.width = this.width;
        this.canvas.height = this.height;

        this.milkyway.setSize(this.width, this.height);
      } catch (e){
          window.removeEventListener('resize', resizer);
      }
    }

    window.addEventListener('resize', resizer);

    this.milkyway = new milkyway(this.canvas);
    this.milkyway.run();
  }

  componentWillUnmount(){

    this.milkyway.release();
    delete this.milkyway;
  }

  render() {
    let style = {position:this.props.position || 'absolute', 'left':'0', right:0, top:0, bottom:0};

    return (
      <div className='milkyway-graphics' style={style} ref='root'>
        <canvas
          width="100%"
          height="100%"
          ref={ (_canvas) => {
          this.canvas = _canvas;
        }}>
        </canvas>
      </div>
    );
  }
}



class milkyway {
  constructor(_canvas){
    this.canvas = _canvas;
    this.width = this.canvas.width;
    this.height = this.canvas.height;
    this.context = this.canvas.getContext('2d');
    this.itvid = null;


    this.starpool = [];
    this.starpool_length = 0;

    for( var i = 0; i < 2000; i++ ){
      this.starpool.push(
        new star( Math.random()*this.canvas.width,
                  Math.random()*this.canvas.height,
                  0.1 + Math.random() * 2.0,
                  this))
    }

    this.starpool_length = this.starpool.length;
  }

  setSize(_w, _h){
    this.width = _w;
    this.height = _h;
  }

  run(){
    if( this.itvid ){
      clearInterval(this.itvid);
    }

    this.itvid = setInterval( ()=>{
      this.tick();
      this.update();
    }, 1000/30);
  }

  stop(){
    clearInterval(this.itvid);
    this.itvid = null;
  }

  release(){
    clearInterval(this.itvid);
    this.itvid = null;

    this.starpool = [];

    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
  }



  clearScene(){

    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    // this.context.shadowBlur = 1;
    // var oldArray = this.context.getImageData(0,0,this.canvas.width,this.canvas.height);
    // //count through only the alpha pixels
    // for(var d=3;d<oldArray.data.length;d+=4){
    //     //dim it with some feedback, I'm using .9
    //     oldArray.data[d] = Math.floor(oldArray.data[d]*.9);
    // }
    // this.context.putImageData(oldArray,0,0);
  }

  tick(){
    var star;
    for( var i = 0; i < this.starpool_length; i++ ){
      star = this.starpool[i].tick();
    }
  }

  update(){
    var context = this.context;
    this.clearScene();


    var star;
    for( var i = 0; i < this.starpool_length; i++ ){
      star = this.starpool[i].draw(context);
    }

    // this.context.lineWidth = 5;
    // this.context.strokeStyle = '#003300';
    // this.context.stroke();

  }


}


class star {
  constructor(_x, _y, _radius, _parent){
    this.x = _x || 0;
    this.y = _y || 0 ;
    this.standardAlpha = 0.1 + Math.random() * 0.6;
    this.radius = _radius || 1;


    this.parent = _parent;

    this.alpha = this.standardAlpha;
    this.alphaVector = Math.random() > 0.5 ? -1:1;
    this.alphaVelocity = Math.random() * 0.01;
    this.velocity = 0.1 + Math.random();

  }

  resetPosition(){
    this.x = -5;
    this.y = Math.random() * this.parent.height;
  }

  tick(){
    this.x += this.velocity;
    if( this.x > this.parent.width + 5 ){
      this.resetPosition();
    }

    this.alpha += this.alphaVector * this.alphaVelocity;
    if( this.alpha > this.standardAlpha || this.alpha < 0 ){
      this.alphaVector = this.alphaVector * -1;
    }
  }

  draw(_context){
    _context.beginPath();
    _context.arc(this.x , this.y , this.radius, 0, 2 * Math.PI, false);
    _context.fillStyle = `rgba(255, 255, 255,${this.alpha})`;
    _context.fill();
  }
}
