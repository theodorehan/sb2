// @flow
import React, { Component  } from 'react';
import PropTypes from 'prop-types';
import './svgStyles.scss';

export default class PolarbearSVG extends Component {
  static propTypes = {
    refsOnly: PropTypes.bool,
  };

  class(){
    return this.props.refsOnly ? 'refs-only':'';
  }

  render() {
    return (
<svg className={this.class()} width="287px" height="250px" viewBox="576 387 287 250" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
    <desc>Created with Sketch.</desc>
    <defs>
        <ellipse id="path-1" cx="3.88172713" cy="3.88030967" rx="3.88172713" ry="3.88030967"></ellipse>
        <filter x="-50%" y="-50%" width="200%" height="200%" filterUnits="objectBoundingBox" id="filter-2">
            <feMorphology radius="1" operator="dilate" in="SourceAlpha" result="shadowSpreadOuter1"></feMorphology>
            <feOffset dx="0" dy="0" in="shadowSpreadOuter1" result="shadowOffsetOuter1"></feOffset>
            <feGaussianBlur stdDeviation="3" in="shadowOffsetOuter1" result="shadowBlurOuter1"></feGaussianBlur>
            <feColorMatrix values="0 0 0 0 1   0 0 0 0 1   0 0 0 0 1  0 0 0 0.5 0" type="matrix" in="shadowBlurOuter1"></feColorMatrix>
        </filter>

        <ellipse id="path-3" cx="3.88172713" cy="3.88030967" rx="3.88172713" ry="3.88030967"></ellipse>
        <filter x="-50%" y="-50%" width="600%" height="600%" filterUnits="objectBoundingBox" id="filter-4">
            <feMorphology radius="1" operator="dilate" in="SourceAlpha" result="shadowSpreadOuter1"></feMorphology>
            <feOffset dx="0" dy="0" in="shadowSpreadOuter1" result="shadowOffsetOuter1"></feOffset>
            <feGaussianBlur stdDeviation="3" in="shadowOffsetOuter1" result="shadowBlurOuter1"></feGaussianBlur>
            <feColorMatrix values="0 0 0 0 1   0 0 0 0 1   0 0 0 0 1  0 0 0 0.5 0" type="matrix" in="shadowBlurOuter1"></feColorMatrix>
        </filter>

        <ellipse id="path-5" cx="21.9503957" cy="54.9250125" rx="2.54176007" ry="2.54083191"></ellipse>
        <filter x="-50%" y="-50%" width="200%" height="200%" filterUnits="objectBoundingBox" id="filter-6">
            <feOffset dx="0" dy="0" in="SourceAlpha" result="shadowOffsetOuter1"></feOffset>
            <feGaussianBlur stdDeviation="2" in="shadowOffsetOuter1" result="shadowBlurOuter1"></feGaussianBlur>
            <feColorMatrix values="0 0 0 0 1   0 0 0 0 1   0 0 0 0 1  0 0 0 0.5 0" type="matrix" in="shadowBlurOuter1"></feColorMatrix>
        </filter>

        <ellipse id="path-7" cx="60.2471042" cy="101.629695" rx="2.63816057" ry="2.63719722"></ellipse>
        <filter x="-50%" y="-50%" width="200%" height="200%" filterUnits="objectBoundingBox" id="filter-8">
            <feOffset dx="0" dy="0" in="SourceAlpha" result="shadowOffsetOuter1"></feOffset>
            <feGaussianBlur stdDeviation="2" in="shadowOffsetOuter1" result="shadowBlurOuter1"></feGaussianBlur>
            <feColorMatrix values="0 0 0 0 1   0 0 0 0 1   0 0 0 0 1  0 0 0 0.5 0" type="matrix" in="shadowBlurOuter1"></feColorMatrix>
        </filter>

        <ellipse id="path-9" cx="123.052035" cy="127.548997" rx="2.71849433" ry="2.71750164"></ellipse>
        <filter x="-50%" y="-50%" width="200%" height="200%" filterUnits="objectBoundingBox" id="filter-10">
            <feOffset dx="0" dy="0" in="SourceAlpha" result="shadowOffsetOuter1"></feOffset>
            <feGaussianBlur stdDeviation="2" in="shadowOffsetOuter1" result="shadowBlurOuter1"></feGaussianBlur>
            <feColorMatrix values="0 0 0 0 1   0 0 0 0 1   0 0 0 0 1  0 0 0 0.5 0" type="matrix" in="shadowBlurOuter1"></feColorMatrix>
        </filter>

        <ellipse id="path-11" cx="124.044961" cy="162.802638" rx="3.04946941" ry="3.04835586"></ellipse>
        <filter x="-50%" y="-50%" width="200%" height="200%" filterUnits="objectBoundingBox" id="filter-12">
            <feOffset dx="0" dy="0" in="SourceAlpha" result="shadowOffsetOuter1"></feOffset>
            <feGaussianBlur stdDeviation="2" in="shadowOffsetOuter1" result="shadowBlurOuter1"></feGaussianBlur>
            <feColorMatrix values="0 0 0 0 1   0 0 0 0 1   0 0 0 0 1  0 0 0 0.5 0" type="matrix" in="shadowBlurOuter1"></feColorMatrix>
        </filter>

        <ellipse id="path-13" cx="193.651335" cy="126.500325" rx="3.44670598" ry="3.44544738"></ellipse>
        <filter x="-50%" y="-50%" width="200%" height="200%" filterUnits="objectBoundingBox" id="filter-14">
            <feOffset dx="0" dy="0" in="SourceAlpha" result="shadowOffsetOuter1"></feOffset>
            <feGaussianBlur stdDeviation="2" in="shadowOffsetOuter1" result="shadowBlurOuter1"></feGaussianBlur>
            <feColorMatrix values="0 0 0 0 1   0 0 0 0 1   0 0 0 0 1  0 0 0 0.5 0" type="matrix" in="shadowBlurOuter1"></feColorMatrix>
        </filter>

        <ellipse id="path-15" cx="193.401913" cy="166.169122" rx="3.19728352" ry="3.196116"></ellipse>
        <filter x="-50%" y="-50%" width="200%" height="200%" filterUnits="objectBoundingBox" id="filter-16">
            <feMorphology radius="0.5" operator="dilate" in="SourceAlpha" result="shadowSpreadOuter1"></feMorphology>
            <feOffset dx="0" dy="0" in="shadowSpreadOuter1" result="shadowOffsetOuter1"></feOffset>
            <feGaussianBlur stdDeviation="2" in="shadowOffsetOuter1" result="shadowBlurOuter1"></feGaussianBlur>
            <feColorMatrix values="0 0 0 0 1   0 0 0 0 1   0 0 0 0 1  0 0 0 0.5 0" type="matrix" in="shadowBlurOuter1"></feColorMatrix>
        </filter>


    </defs>
    <g id="Polarbear" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd" transform="translate(578.000000, 388.000000)">
        <polygon id="Path-2" fill="#1E2131" points="1.5237019 33.6912205 0 10.3431823 9.01628565 -2.84217094e-14 24.6842507 10.1263881 34.2560089 26.5747754 36.7381459 44.1281139 47.8392028 63.8186238 59.6552382 76.4822036 66.9743242 86.372216 73.725345 101.497459 83.6846748 109.089453 89.6521575 115.422642 103.379466 112.162336 110.65098 109.275476 134.127743 108.578938 145.065096 114.738691 160.931744 117.801783 173.823745 116.719211 181.769662 114.653372 195.200346 114.843591 212.874728 115.408655 227.493312 117.987807 236.849597 122.93631 239.313544 127.796696 241.163254 125.721066 241.345147 122.968479 244.843085 122.965682 246.843906 125.908488 247.696004 129.732459 250.564313 133.331243 258.971957 135.797102 268.873921 133.932672 270.494166 132.373152 273.222558 138.57067 271.530955 144.694057 274.175396 152.662294 277.073088 159.561945 278.932592 165.584628 280.764113 173.260542 284.673409 178.481786 282.888061 183.570156 279.334156 187.299017 275.181403 186.851442 270.695647 183.027472 276.768068 189.110298 274.28733 191.66847 264.290223 190.801293 260.031133 185.763275 252.994681 184.845746 245.304813 183.668063 240.237 180.691689 235.92754 176.304053 236.005894 181.095905 235.127212 185.206603 239.869017 188.462712 246.54868 191.479649 250.823161 194.809888 254.273527 198.731765 255.057065 205.628619 252.870154 213.687769 245.117324 219.710452 238.597167 225.240803 229.807547 225.22262 224.031752 223.430921 221.181632 219.992984 221.816857 213.761899 224.409529 210.662441 224.666977 207.589557 219.839823 206.291589 216.326493 203.094224 214.411022 210.577122 215.640898 218.895026 218.625338 224.263131 220.973155 227.871706 220.30295 233.5671 215.961309 237.100146 203.265192 236.248355 194.078207 235.322433 185.289987 230.959974 180.953943 226.310787 181.424066 216.128452 183.447273 210.725381 185.322168 205.232794 185.648176 200.478707 175.802179 201.131887 165.133467 201.319309 157.903929 200.554235 149.822292 197.727518 147.604599 195.284038 149.694967 203.343188 152.640231 209.119705 156.448786 212.140837 162.462442 213.966105 166.918815 217.275364 169.684985 225.012819 169.33659 230.112379 161.930755 233.575492 150.686983 233.172674 145.676536 230.811715 135.342227 231.365589 130.14569 230.029857 126.763883 226.284212 128.601 216.690719 128.128079 211.701654 126.222402 209.596652 120.348664 214.879438 114.561675 215.163368 106.747281 215.911658 103.585145 216.995629 106.107858 221.995883 111.830485 225.899578 117.16694 231.359995 121.59393 235.659514 123.52899 239.405158 122.979114 244.174631 118.442988 248 109.041929 247.239122 98.4012006 243.911681 87.6065632 238.66526 80.628876 234.870662 74.5508584 232.625793 67.8418129 230.540373 63.2343286 227.710858 63.8513649 218.846073 68.7358859 211.199531 73.8162914 207.252477 79.4941448 203.908252 82.6185032 194.039219 83.3698604 184.781407 83.4496134 173.232569 82.225335 162.794277 81.3522496 153.291697 81.8839362 145.364021 82.8367746 136.44189 75.776536 134.552284 68.2755572 128.462464 64.6530924 120.898444 60.5996815 115.507961 51.4434783 110.281122 43.3184674 104.666851 39.181106 97.6021567 31.8424315 89.9793921 25.6916569 83.4727682 23.1969273 78.858548 21.8369289 69.2426753 19.2946274 59.5302942 15.7812982 51.7005262 9.99011166 45.7128097 4.15135312 38.6439194"></polygon>
        <g id="Group-6" transform="translate(6.656051, 10.282927)">
            <g id="Oval">
              <animate
                xlinkHref="#Oval"
                attributeName="fill-opacity"
                values='1;0;1;'
                dur="3s"
                repeatCount="indefinite"
                fill="freeze" />

                <use fill="black" fillOpacity="1" filter="url(#filter-4)" xlinkHref="#path-3"></use>
                <use fill="#FFFFFF" fillRule="evenodd" xlinkHref="#path-3"></use>
            </g>
            <g id="Oval-Copy">
                <use fill="black" fillOpacity="1" filter="url(#filter-6)" xlinkHref="#path-5"></use>
                <use fill="#FFFFFF" fillRule="evenodd" xlinkHref="#path-5"></use>
            </g>
            <g id="Oval-Copy-2">
                <use fill="black" fillOpacity="1" filter="url(#filter-8)" xlinkHref="#path-7"></use>
                <use fill="#FFFFFF" fillRule="evenodd" xlinkHref="#path-7"></use>
            </g>
            <g id="Oval-Copy-3">
                <use fill="black" fillOpacity="1" filter="url(#filter-10)" xlinkHref="#path-9"></use>
                <use fill="#FFFFFF" fillRule="evenodd" xlinkHref="#path-9"></use>
            </g>
            <g id="Oval-Copy-5">
                <use fill="black" fillOpacity="1" filter="url(#filter-12)" xlinkHref="#path-11"></use>
                <use fill="#FFFFFF" fillRule="evenodd" xlinkHref="#path-11"></use>
            </g>
            <g id="Oval-Copy-4">
                <use fill="black" fillOpacity="1" filter="url(#filter-14)" xlinkHref="#path-13"></use>
                <use fill="#FFFFFF" fillRule="evenodd" xlinkHref="#path-13"></use>
            </g>
            <g id="Oval-Copy-6">
                <use fill="black" fillOpacity="1" filter="url(#filter-16)" xlinkHref="#path-15"></use>
                <use fill="#FFFFFF" fillRule="evenodd" xlinkHref="#path-15"></use>
            </g>
            <polyline id="Path-3" strokeOpacity="0.27509058" stroke="#FFFFFF" points="3.88172713 3.88030967 21.3673946 54.7465757 59.9034331 101.322176 123.724488 126.908821 194.300627 126.154692 194.108666 166.624345 123.371281 163.498453 123.046868 130.437683"></polyline>


        </g>
    </g>
</svg>
    );
  }
}
