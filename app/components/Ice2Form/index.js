import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Promise from 'bluebird';
import cloneDeep from 'lodash/cloneDeep';
// import Ice2Display, {TABLE_STYLE_SUPPORT} from '../ICE2Elements/Display';
import Ice2Input , {TABLE_STYLE_SUPPORT} from '../ICE2Elements/Input';

import Ice2Table from '../Ice2Table';
import LaunchIcon from 'material-ui/svg-icons/action/launch';
import ListIcon from 'material-ui/svg-icons/action/list';
import './style.scss';
import {Tooltip, notification, Icon} from 'antd';

import {
  VALUE_TYPES,
} from "../ICE2Elements/constants";

let RESERVATION_PIDS = ['owner', 'created', 'changed', 'modifier'];

export default class Ice2Form extends Component {
  static propTypes = {
    nodeType : PropTypes.object.isRequired,
    target : PropTypes.object,
    style : PropTypes.object,
    defaultDataSet : PropTypes.object,
    customPropertyUxMap : PropTypes.object,
  };

  static contextTypes = {
    UIISystems : PropTypes.object,
    ice2 : PropTypes.func,
    intl : PropTypes.object,
    layout : PropTypes.object,
    global : PropTypes.object,
    functionStore : PropTypes.object,
    dataSetFromServicePage : PropTypes.object,
  }

  constructor(props){
    super(props);


    this.___callbacks = [];



    let originDidUpdate = this.componentDidUpdate
    this.componentDidUpdate = (prevProps, prevState) => {
      originDidUpdate && originDidUpdate.bind(this)(prevProps, prevState);

      let callback;
      while( callback = this.___callbacks.pop()){
        callback();
      }
    }


    this.state = {
      altDown : false,
      controlDown : false,

      nodeType : null,

      originData : null,

      freshData : props.cloneData ? cloneDeep(props.cloneData) : {},
    }

    // this.onKeyDown = (e)=>{
    //   console.log(e);
    //
    // }
    //
    // this.onKeyUp = (e)=>{
    //   console.log(e);
    //
    // }
  }


  async setStateAsync(newState){
    await new Promise((resolve) => {

      this.setState({
        ...newState,
      });
      this.___callbacks.push(function(){
        resolve();
      });
    });
  }


  // componentDidMount(){
  //   // $(document).on('keydown', )
  //   document.addEventListener('keydown', this.onKeyDown);
  //   document.addEventListener('keyup', this.onKeyUp);
  // }
  // componentWillUnmount(){
  //   document.removeEventListener('keydown', this.onKeyDown);
  //   document.removeEventListener('keyup', this.onKeyUp);
  // }

  loadItemDataForModify(){

    return this.context.ice2.get(`/api/node/${this.props.nodeType.tid}/read.json`, {
      params : {
        ...this.props.target,
        includeReferenced : true,
      },
    })
      .then((res)=>{
        let {data} = res;

        this.setState({
          originData : data.item,
          freshData : Object.assign({},data.item),
        });
      })
  }

  onChangePropertyValue(){

  }

  async setFreshData(prop, data, info = {}){

    let {
      pid,
    } = prop;

    let {
      size = {},
    } = info;





    if( prop.ux ){


      let {
        helper,
      } = prop.ux;


      if( helper ){
        helper = this.context.ice2.resolver.resolveFromDataAndErrorToNull(helper)
      }

      console.log(helper);

      if( helper && helper.inputAffection ){
        if( helper.inputAffection.size ){
          await this.setStateAsync({
            freshData : {
              ...this.state.freshData,
              [helper.inputAffection.size.width] : size.width,
              [helper.inputAffection.size.height] : size.height,
            },
          });


          console.log('Await end', this.state)
        }
      }
    }



    this.setState({
      freshData : {
        ...this.state.freshData,
        [pid] : data,
      },
    });

    console.log(this.state.freshData);
  }

  reset(){
    this.setState({
      freshData : Object.assign({}, this.state.originData),
    });
  }

  submit(){

    return new Promise((resolve, reject)=>{

      let availablePids = Object.keys(this.inputs);

      let data = {};
      let formData = new FormData();
      let pid,value;
      for(let i = 0; i < availablePids.length; i++ ){
        pid = availablePids[i];

        if( this.inputs[availablePids[i]] ){
          value = this.inputs[availablePids[i]].getFieldData();

          console.log(value != this.state.originData[pid] , value , this.state.originData[pid] )

          if( value || value != this.state.originData[pid] ){
            data[pid] = value;
            formData.append(pid, value);
          }
        }
      }


      let blockingId = this.context.global.uiBlockingReqBegin();
      this.context.ice2.request({
        method : 'post',
        withCredentials : true,
        url : `/api/node/${this.props.nodeType.tid}/save.json`,
        data : formData,
      })
        .then((res) => {
          let {data} = res;

          this.context.global.uiBlockingReqFinish(blockingId);


          if( data.result == '200' && data.item ){
            resolve(data.item);

            notification.open({
              message : this.context.intl.formatMessage({id : 'app.common.success-save'}),
              description : 'Wow',
              icon: <Icon type="smile-circle" style={{ color: '#108ee9' }} />,
            });
          } else {
            throw new Error('Fail save');
          }
        }).catch(() => {
          this.context.global.uiBlockingReqFinish(blockingId);

          notification.open({
            message : this.context.intl.formatMessage({id : 'app.common.fail-save'}),
            description : 'T_T',
            icon: <Icon type="frown" style={{ color: '#108ee9' }} />,
          });
      })
    })
  }


  excludeReservation(prop){
    let cpid = prop.pid;

    if( prop.valueType && prop.valueType.value ){
      if( prop.valueType.value.toUpperCase() == VALUE_TYPES.REFERENCED ){
        return false;
      }
    }

    if(RESERVATION_PIDS.find( (pid) => cpid == pid ) ){
      return false;
    }



    return true;
  }

  componentDidMount(){
    this.context.ice2.loadNodeType(this.props.nodeType.tid, null, null, null, this.props.customPropertyUxMap)
      .then((nodeType) => {

        let defaultDataSet = Object.assign({}, this.props.defaultDataSet);


        let {
          propertyTypes,
        } = nodeType;


        let propType, ux, defaultValue;
        for(let i= 0; i < propertyTypes.length ; i++ ){
          propType = propertyTypes[i];
          ux = propType.ux;


          // form의 프로퍼티로 받은 기본값이 없으면 ux 로 지정된 기본값 룰을 따른다.
          if( ux && !defaultDataSet[propType.pid] && ux.defaultValueRule ){
            defaultValue = this.context.ice2.resolver.resolveFromDataAndErrorToNull(ux.defaultValueRule, this.context.dataSetFromServicePage);
            defaultDataSet[propType.pid] = defaultValue;
          }
        }



        this.setState({
          nodeType,
          originData : Object.assign({}, this.state.originData, defaultDataSet),
          freshData : Object.assign({}, this.state.freshData, defaultDataSet),
        });


        if( this.props.target ){
          this.loadItemDataForModify();
        }
      });


  }

  renderField(prop){
    let {
      ux,
    } = prop;


    let customUx = (this.props.customPropertyUxMap && this.props.customPropertyUxMap[prop.pid]) || {};

    ux = Object.assign({}, ux, customUx);


    if( ux ){
      if( ux.showCondition ){
        let showCondition = this.context.ice2.resolver.resolveFromDataAndErrorToNull(ux.showCondition, {'this':this.state.freshData});
        if( !showCondition ){
          if( this.state.freshData && this.state.freshData[prop.pid] ){
            return <Ice2Input
              isHidden
              ownNodeItemObject={this.state.freshData}
              ref={(c) => this.inputs[prop.pid] = c}
              renderAsTableRow
              withLabel
              propertyType={{...prop, ux}}
              customUx={customUx}
              iceUrl={this.context.ice2.defaults.baseUrl}
              key={prop.pid}
              pid={prop.pid}
              onChange={(prop, data, info) => this.setFreshData(prop, data, info) }
              tid={this.props.nodeType.tid}
              value={this.state.freshData && this.state.freshData[prop.pid]}
              style={{borderBottom: '1px solid #e6e6e6'}}/>
          }
          return null;
        }
      }
    }



    return <Ice2Input
      ownNodeItemObject={this.state.freshData}
      ref={(c) => this.inputs[prop.pid] = c}
      renderAsTableRow
      withLabel
      propertyType={{...prop, ux}}
      customUx={customUx}
      iceUrl={this.context.ice2.defaults.baseUrl}
      key={prop.pid}
      pid={prop.pid}
      onChange={(prop, data, info) => this.setFreshData(prop, data, info) }
      tid={this.props.nodeType.tid}
      value={this.state.freshData && this.state.freshData[prop.pid]}
      style={{borderBottom: '1px solid #e6e6e6'}}/>
  }




  render(){
    this.inputs = {};

    return <div className="ice2-form" style={{...this.props.style}}>
      <div className="title">

        <span className="type-name">
          {this.props.nodeType.typeName}
        </span>
        <span className="tid">
          {this.props.nodeType.tid}
        </span>
      </div>
      <div className="">

        { this.state.nodeType && <div style={TABLE_STYLE_SUPPORT}>
          { this.state.nodeType.propertyTypes.filter(this.excludeReservation).map((prop) => this.renderField(prop)) }
        </div>}
      </div>
    </div>
  }
}
