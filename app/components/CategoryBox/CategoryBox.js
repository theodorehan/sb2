import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import ScrollWrapper from 'react-scrollbar';
import FullmoonButton from '../Unit/FullmoonButton';
import LoadableDIV from '../LoadableDiv';
import {Tooltip} from 'antd';

import './style.scss';


export default class CategoryBox extends Component {
  static propTypes = {
    rootCategoryPaths: PropTypes.array.isRequired,
    categoriesMap : PropTypes.object.isRequired,
    onRequestLoadCategoryData : PropTypes.func,
    onRequestCloseSubCategory : PropTypes.func,
    subLoading : PropTypes.bool,
    openedRootCategoryPath : PropTypes.string,
    openedSubCategoryFullPaths : PropTypes.object,
    CategoryItemComponent: PropTypes.func.isRequired,
    rightTopUI : PropTypes.func,
    onClickNode : PropTypes.func,
    selectedNodeList : PropTypes.array,

    rootCategoryMenuAdditionArray : PropTypes.array,
  }

  static contextTypes = {
    theme : PropTypes.object,
  }

  constructor(props){
    super(props);
  }

  onRequestOpenAndFillSubCategoryChildren(categoryItem, fullPath){
    if( this.props.openedSubCategoryFullPaths[fullPath] ){
      this.props.onRequestCloseSubCategory(categoryItem, fullPath);
    } else {
      this.props.onRequestOpenAndFillSubCategoryChildren(categoryItem, fullPath);
    }
  }

  componentDidUpdate(){
    let { openedRootCategoryPath, categoriesMap} = this.props;
    let openedRootCategoryItem = categoriesMap[openedRootCategoryPath];

    if( openedRootCategoryItem ){
      if( !Array.isArray(openedRootCategoryItem.children) && !this.props.subLoading ){
        // let foundIndex = categories
        this.props.onRequestOpenAndFillSubCategoryChildren(openedRootCategoryItem, openedRootCategoryItem.pathName);
      }
    }
  }

  componentDidMount(){
    if( this.props.rootCategoryPaths[0] ) {
      let rootCategory = this.props.categoriesMap[this.props.rootCategoryPaths[0]];

      this.props.onRequestOpenAndFillSubCategoryChildren(rootCategory, rootCategory.pathName);
    }
  }

  render(){
    let {
      rootCategoryPaths,
      openedRootCategoryPath,
      categoriesMap,
      openedSubCategoryFullPaths,
    } = this.props;

    let openedCategoryItem = categoriesMap[openedRootCategoryPath];

    // let rootCategoryChildren = categoriesMap[openedRootCategoryPath].children;
    // let rootCategoryItemList = this.props.categoryDataListDict[this.props.openedRootCategoryPath] || [];

    return <div className={classnames("category-box", this.props.className)}>
      <aside className="root-category-wrapper">
        <ScrollWrapper style={{width:'100%', height:'100%'}}>
          <ul>
            {
              this.props.rootCategoryMenuAdditionArray && this.props.rootCategoryMenuAdditionArray.map(({name, onClick}, i)=>
                <Tooltip title={name} placement='top' key={i}>
                  <FullmoonButton
                    className="root-category-btn"
                    title={name}

                    normalStyle={{
                      display: 'inline-block',
                      width: '90%',
                      height: 30,
                      borderWidth: 0,

                      borderRadius: 100,
                      fontSize: 12,
                      color:this.context.theme.CategoryBox.fullmoonBtn.aside.textColor.default,
                      fontFamily: 'Nanum Square',
                      margin:5,
                      padding:5,
                      paddingLeft:10,
                      textAlign:'left',
                    }}
                    hoverStyle={{
                      backgroundColor:this.context.theme.CategoryBox.fullmoonBtn.aside.bgColor.hovered,
                    }}
                    moonBlender='overlay'
                    onClick={(e)=>onClick(e)}>

                      { name }
                </FullmoonButton>
              </Tooltip>)
            }

            {
              rootCategoryPaths.map((path, i)=><li key={i}>
                <Tooltip title={`${ categoriesMap[path].name }(${categoriesMap[path].itemCount})`} placement='top'>
                  <FullmoonButton
                    className="root-category-btn"
                    title={`${categoriesMap[path].name}(${categoriesMap[path].itemCount})`}
                    normalStyle={{
                      display: 'inline-block',
                      width: '90%',
                      height: 30,
                      borderWidth: 0,

                      borderRadius: 100,
                      fontSize: 12,
                      color: (this.props.openedRootCategoryPath === path) ?
                        this.context.theme.CategoryBox.fullmoonBtn.aside.textColor.active :
                        this.context.theme.CategoryBox.fullmoonBtn.aside.textColor.default,
                      fontFamily: 'Nanum Square',
                      backgroundColor: (this.props.openedRootCategoryPath === path) ?
                        this.context.theme.CategoryBox.fullmoonBtn.aside.bgColor.active :
                        this.context.theme.CategoryBox.fullmoonBtn.aside.bgColor.default,
                      margin:5,
                      padding:5,
                      paddingLeft:10,
                      textAlign:'left',
                    }}
                    hoverStyle={{
                      borderWidth: 0,
                      backgroundColor:this.context.theme.CategoryBox.fullmoonBtn.aside.bgColor.hovered,
                    }}
                    moonBlender='overlay'
                    onClick={(e)=>this.onRequestOpenAndFillSubCategoryChildren(categoriesMap[path],path)}>

                      { categoriesMap[path].name }({categoriesMap[path].itemCount})

                  </FullmoonButton>
                </Tooltip>
              </li>)
            }
          </ul>
        </ScrollWrapper>
      </aside>
      <LoadableDIV className="category-view" isLoading={this.props.subLoading}>

        <ScrollWrapper style={{width:'100%', height:'100%'}}>
          { openedCategoryItem &&
            <CategoryList
              categoryItem={openedCategoryItem}
              categoriesMap={categoriesMap}
              openedSubCategoryFullPaths={{...openedSubCategoryFullPaths, [openedRootCategoryPath]:true}}
              onRequestOpenAndFillSubCategoryChildren={:: this.onRequestOpenAndFillSubCategoryChildren }
              categoryFullPath={openedCategoryItem.pathName}
              itemNameFilter={this.props.itemNameFilter}
              CategoryItemComponent={this.props.CategoryItemComponent}
              rightTopUI={this.props.rightTopUI}
              onClickNode={this.props.onClickNode}
              selectedNodeList={this.props.selectedNodeList}/> }
        </ScrollWrapper>

      </LoadableDIV>
    </div>
  }
}

const Category = (props) =>
  <props.CategoryItemComponent {...props} className="category"/>;


class CategoryList extends Component {
  static propTypes = {
    categoryItem : PropTypes.object.isRequired,
    categoriesMap : PropTypes.object,
    categoryFullPath : PropTypes.string.isRequired,
    onRequestOpenAndFillSubCategoryChildren : PropTypes.func,
    openedSubCategoryFullPaths : PropTypes.object,
    CategoryItemComponent : PropTypes.func.isRequired,
    rightTopUI : PropTypes.func,
    onClickNode : PropTypes.func,
    selectedNodeList : PropTypes.array,
  }

  constructor(props){
    super(props);
  }

  renderSubCategoryMenus(){
    let els = [];


    if( this.props.categoryFullPath.split('/').length > 1 ){
      els.push(
        <button key={els.length} onClick={ () => this.props.onRequestOpenAndFillSubCategoryChildren(this.props.categoryItem, this.props.categoryFullPath) }>
          {
            this.props.openedSubCategoryFullPaths[this.props.categoryFullPath] ?
              <i className="fa fa-caret-up"/> :
              <i className="fa fa-caret-down"/>
          }
        </button>
      )
    }

    return els;
  }

  renderSubCategory(){
    if( this.props.categoriesMap[this.props.categoryFullPath] && this.props.openedSubCategoryFullPaths[this.props.categoryFullPath]){
      if( this.props.categoriesMap[this.props.categoryFullPath].children ){

        return this.props.categoriesMap[this.props.categoryFullPath].children.sort((a,b)=>{


          // 이거 키면 무한루프 돔
          // if( a.hasSubCategory ){
          //   if( a.name < b.name ){
          //     return 1;
          //   }
          //   return -1;
          // } else {
          //   return -1;
          // }
          // return a.hasSubCategory ? 1:-1;

          if( a.name < b.name ){
            return 1;
          }
          return -1;
        }).map((item, i) => {
          let categoryFullPath = this.props.categoryFullPath + '/' + item.pathName;

          if( item.hasSubCategory ){


            return <CategoryList
              categoryItem={item}
              categoryFullPath={categoryFullPath}
              categoriesMap={this.props.categoriesMap}
              openedSubCategoryFullPaths={this.props.openedSubCategoryFullPaths}
              onRequestOpenAndFillSubCategoryChildren={ this.props.onRequestOpenAndFillSubCategoryChildren }
              CategoryItemComponent={this.props.CategoryItemComponent}
              onClickNode={this.props.onClickNode}
              selectedNodeList={this.props.selectedNodeList}
              rightTopUI={this.props.rightTopUI}
              key={i}/>;
          } else {
            return <Category
              item={item}
              fullPath={categoryFullPath}
              CategoryItemComponent={this.props.CategoryItemComponent}
              key={i}
              onClickNode={this.props.onClickNode}
              selectedNodeList={this.props.selectedNodeList}
              rightTopUI={this.props.rightTopUI}/>
          }
        })
      }
    }
  }

  render(){
    return <div className="category-list">
      <div className="category-list-name">
        { this.props.categoryItem.name } ({this.props.categoryItem.itemCount})

        <div className="sub-category-menus">
          { this.renderSubCategoryMenus() }
        </div>
      </div>
      { this.renderSubCategory() }
    </div>
  }
}


