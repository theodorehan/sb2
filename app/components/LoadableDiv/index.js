import React, {Component} from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import './style.scss';

const LoadableDIV = (props) => <div
  className={classnames('loadable-div', props.className)}
  style={props.style}
  onClick={props.onClick}>
  <div className={classnames('loading-back', props.isLoading && 'loading')}/>

  {props.children}
</div>

LoadableDIV.propTypes = {
  isLoading : PropTypes.bool,
  style : PropTypes.object,
  onClick : PropTypes.func,
  className : PropTypes.string,
  children : PropTypes.any,
}

export default LoadableDIV;
