import React from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';

import './style.scss';


export const SementicLayout = (props) => <div
  className={ classnames('sementic-layout', props.className) }
  style={{
    ...props.style,
  }}>
  { props.children }
</div>

export const Title = (props) => <div
  style={props.style}
  className={
    classnames("sementic-title",
      props.className)
  }>
  {props.children}
</div>

Title.propTypes = {
  className : PropTypes.string,
}


export const Contents = (props) =>  <div
  style={props.style}
  className={
    classnames("sementic-contents",
      props.className,
      props.inStyle && 'in-style')
  }>
  {props.children}
</div>

Contents.propTypes = {
  inStyle : PropTypes.bool,
  className : PropTypes.string,
};


export const Label = (props) =>  <label
  htmlFor={props.for}
  style={props.style}
  className={
    classnames("sementic-label",
      props.className,
      props.inColumn && 'in-column',
      props.isRequired && 'is-required')
  }>

  {props.children}
</label>

Label.propTypes = {
  for : PropTypes.string,
  className : PropTypes.string,
  inColumn : PropTypes.bool,
  isRequired : PropTypes.bool,
}
