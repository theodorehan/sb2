import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { Form, Input, Tooltip, Icon, Cascader, Select, Row, Col, Checkbox, Button, AutoComplete, Popover } from 'antd';
import { Radio, message } from 'antd';
const { TextArea } = Input;
import Promise from 'bluebird';
import cloneDeep from 'lodash/cloneDeep';
import defaultsDeep from 'lodash/defaultsDeep';
import Remove from 'material-ui/'
import GridLayout from 'react-grid-layout';
const FormItem = Form.Item;
const Option = Select.Option;
const AutoCompleteOption = AutoComplete.Option;

import './style.scss';
import {Label, Title, Contents, SementicLayout} from "../SementicLayout/index";

import Ice2NodeThumbnailDisplay from '../ICE2Elements/Display/nodeThumbnail';
import FullmoonBtn from '../Unit/FullmoonButton';
import ComponentItemWrapper from "../Toolset/CoreTools/ComponentPalette/ComponentItemWrapper/index";

export default class GeometryNodeDisplayer extends Component {

  static contextTypes = {
    ice2 : PropTypes.func,
    theme : PropTypes.object,
    UIISystems : PropTypes.object,
    functionStore : PropTypes.object,
    intl : PropTypes.object,
  };

  static propTypes = {
    componentProps : PropTypes.object,
    geometryGroupId : PropTypes.string,
    geometryExtraFilters : PropTypes.array,
    geometryPresets : PropTypes.array,
    marginW : PropTypes.number,
    marginH : PropTypes.number,
    gridBoxWidth:PropTypes.number,
    displayNodeBindHelper : PropTypes.object,
    defaultPresetWidth : PropTypes.number,
    curationComponentType : PropTypes.string,
    itemReferenceNodeType: PropTypes.string,
    itemReferenceNodePid : PropTypes.string,
    itemReferenceNodeThumbnailDisplayRule : PropTypes.object,
    itemReferenceCustomUxRuleMap : PropTypes.object,
    addRules : PropTypes.object,
  };

  static defaultProps = {
    gridBoxWidth : 100,
    defaultPresetWidth : 200,

    geometryPresets :  [
      {
        key : 'default',
        title : '기본',
        horizonCellLength : 4,
        rowHeight:50,
        width:150,
        polygons : [
          {i: 'a', x: 0, y: 0, w: 1, h: 1},
          {i: 'b', x: 1, y: 0, w: 1, h: 1},
          {i: 'c', x: 2, y: 0, w: 2, h: 1},
        ],
      },

      {
        key : 'A',
        title : '기본A',
        horizonCellLength : 4,
        rowHeight:50,
        width:150,
        polygons : [
          {i: 'a', x: 0, y: 0, w: 2, h: 1},
          {i: 'b', x: 2, y: 0, w: 1, h: 1},
          {i: 'c', x: 3, y: 0, w: 1, h: 1},
        ],
      },

      {
        key : 'B',
        title : '기본B',
        horizonCellLength : 4,
        rowHeight:50,
        polygons : [
          {i: 'a', x: 0, y: 0, w: 1, h: 1},
          {i: 'b', x: 1, y: 0, w: 1, h: 1},
          {i: 'c', x: 2, y: 0, w: 1, h: 1},
          {i: 'd', x: 3, y: 0, w: 1, h: 1},
        ],
      },

      {
        key : 'C',
        title : '기본C',
        horizonCellLength : 4,
        rowHeight:50,
        polygons : [
          {i: 'a', x: 0, y: 0, w: 1, h: 1},
          {i: 'b', x: 0, y: 1, w: 1, h: 1},
          {i: 'c', x: 1, y: 0, w: 2, h: 2},
          {i: 'd', x: 3, y: 0, w: 1, h: 2},
        ],
      },
    ],

    marginW : 10,
    marginH : 10,
  };

  constructor(props){
    super(props);
    this.thumbnailDisplay = {};

    this.state = {
      preset : null,

      currentGeometryGroup : null,

      displayGroup : {
        title : '',
        description : '',
        gridPresetKey : null, // presetId 가 존재하면 프리셋대로 랜더링하고, 없으면 항목의 rect를 참조하여 랜더링 할것이다.
        extraValue1 : null,
        extraValue2 : null,
        extraValue3 : null,
        curationComponentType : null,
        items : null,
        itemReferenceType : null,
        itemReferencePid : null,
      },


      contentsBindMap : {

      },

      polygonItemIdMap : {

      },
    };
  }


  get hasSubCuration(){
    if( this.props.displayNodeBindHelper ){
      return this.props.displayNodeBindHelper.method === 'curation';
    }
  }



  onApplyItem(poly, gridDisplayGroupId){
    let search = new URLSearchParams();
    let item;


    item = {
      ...poly,
      referenceId : this.state.contentsBindMap[poly.i] || null,
      gridDisplayItemId : this.state.polygonItemIdMap[poly.i] || undefined,
      gridDisplayGroupId,
      referencePid : this.props.itemReferenceNodePid,
      referenceType : this.props.itemReferenceNodeType,
    };


    let paramKeys = Object.keys(item);
    let paramKey, paramValue;
    for(let i = 0; i < paramKeys.length; i++ ){
      paramKey = paramKeys[i];
      paramValue = item[paramKey];

      if( paramValue !== undefined || paramKey == 'referenceId' ){
        search.append(paramKeys[i], paramValue === null ? null : paramValue.toString() );
      }
    }

    return this.context.ice2.post(`/node/gridDisplayItem/save.json`,search.toString())
      .then((res) => {
        let {data} = res;

        return data.item;
      });
  }

  onApply(){
    let search = new URLSearchParams();

    let paramKeys = Object.keys(this.state.displayGroup);
    let paramKey, paramValue;
    for(let i = 0; i < paramKeys.length; i++ ){
      paramKey = paramKeys[i];
      paramValue = this.state.displayGroup[paramKey];

      // Referenced 필드이므로 패스
      if( paramKey === 'referencedItems' ){
        continue;
      }


      if( paramValue ){
        search.append(paramKeys[i], paramValue);
      }
    }

    return this.context.ice2.post(`/node/gridDisplayGroup/save.json`,search.toString())
      .then((res) => {

        let {
          data : groupData,
        } = res;
        let groupItem = groupData.item;


        return Promise.map(this.state.preset.polygons, (poly) => this.onApplyItem(poly, groupItem.gridDisplayGroupId)).spread((...items) => {

          this.setState({
            displayItems : items,
          });

          return {
            madeParams : {},
            item : groupItem,
            id : groupItem.gridDisplayGroupId,
          };
        });
      });
  }

  setDisplayGroupPreset(presetKey, e){
    e.preventDefault();
    e.stopPropagation();

    let preset = this.props.geometryPresets.find((preset) => preset.key === presetKey);

    // 프리셋 변경시에 남은 요소는 API를 통해 제거 한다.
    // 사용자에게 주의 알림,
    // 이외 : 프리셋에 없는 형태가 등록될 경우 커스텀 프리셋을 생성한다.


    let items = this.state.displayItems;

    let willDeprecatedItems = items.filter((item) => !preset.polygons.find((poly) => poly.i == item.i && item.gridDisplayItemId ));


    if( willDeprecatedItems.length > 0 ){
      let okBtn = this.context.UIISystems.getUnique();
      let noBtn = this.context.UIISystems.getUnique();
      this.context.UIISystems.openTool('Dialog', this.props.defaultPopupTarget || '[modal]',{
        width:500,
        height:150,
      }, {
        title : this.context.intl.formatMessage({id : 'app.common.confirm'}) ,
        fields : [
          {
            type : 'label',
            contents :  this.context.intl.formatMessage({id : 'app.geometry-node-display.warning-preset-change'}, {itemIds:willDeprecatedItems.map((item) => item.i).join(', ')}),
          },
          {
            type : 'button-set',
            buttons : [
              {
                label : this.context.intl.formatMessage({id : 'app.common.no'}) ,
                functionKey : noBtn,
                level : 'negative',
              },
              {
                label : this.context.intl.formatMessage({id : 'app.common.yes'}),
                functionKey : okBtn ,
                level : 'positive',
              },
            ],
          },
        ],
      }).then((closer) => {
        this.context.functionStore.register(okBtn, ()=> {


          Promise.each(willDeprecatedItems, (item) => this.context.ice2.post('/node/gridDisplayItem/delete.json', `gridDisplayItemId=${item.gridDisplayItemId}`)).then(() => {
            this.setState({
              preset : cloneDeep(preset),

              displayGroup : {
                ...this.state.displayGroup,
                gridPresetKey : presetKey,
              },
            });


            closer();
          })
        });

        this.context.functionStore.register(noBtn, () => {
          closer();
        });
      });
    } else {
      this.setState({

        preset : cloneDeep(preset),

        displayGroup : {
          ...this.state.displayGroup,
          gridPresetKey : presetKey,
        },
      });
    }

  }

  setDisplayItemId(polygon, referenceId, index){

    let key = polygon.i;
    this.setState({
      contentsBindMap : {
        ...this.state.contentsBindMap,
        [key] : referenceId,
      },
    })
  }

  removeConnectedContents(poly){
    let key = poly.i;

    let okBtn = this.context.UIISystems.getUnique();
    let noBtn = this.context.UIISystems.getUnique();
    this.context.UIISystems.openTool('Dialog', this.props.defaultPopupTarget || '[modal]',{
      width:500,
      height:100,
    }, {
      title : this.context.intl.formatMessage({id : 'app.common.confirm'}) ,
      fields : [
        {
          type : 'label',
          contents :  this.context.intl.formatMessage({id : 'app.geometry-node-display.are-you-really-disconnect-content'}),
        },
        {
          type : 'button-set',
          buttons : [
            {
              label : this.context.intl.formatMessage({id : 'app.common.no'}) ,
              functionKey : noBtn,
              level : 'negative',
            },
            {
              label : this.context.intl.formatMessage({id : 'app.common.yes'}),
              functionKey : okBtn ,
              level : 'positive',
            },
          ],
        },
      ],
    }).then((closer) => {
      this.context.functionStore.register(okBtn, ()=> {
        this.setState({
          contentsBindMap : {
            ...this.state.contentsBindMap,
            [key] : null,
          },
        })
        closer();
      });

      this.context.functionStore.register(noBtn, () => {
        closer();
      })
    })



  }


  modifyConnectedContents(poly){
    let key = poly.i;

    this.context.ice2.loadNodeType(this.state.displayGroup.itemReferenceType)
      .then((nodeType) => {

        let submit = this.context.UIISystems.getUnique();
        this.context.UIISystems.openTool('ICE2Data', this.props.defaultPopupTarget || '[modal]' , {
          width:600,
          height:600,
          UNIQUE_LAYER : true,
        }, {
          nodeType : nodeType,
          type : 'modify',
          target : {
            [this.props.itemReferenceNodePid] : this.state.contentsBindMap[key],
          },
          footerBtnCallbacks : {
            submit,
          },
        }).then((closer) => {
          this.context.functionStore.register(submit, (items)=>{
            let item = items[0];
            this.thumbnailDisplay[key].refresh();
            closer();
          })
        })
      });
  }

  listupConnectedContents(poly){
    let key = poly.i;
    this.context.ice2.loadNodeType(this.state.displayGroup.itemReferenceType)
      .then((nodeType) => {

        this.context.UIISystems.openTool('ICE2Data', this.props.defaultPopupTarget || '[layer]' , {
          width:768,
          height:600,
          UNIQUE_LAYER : true,
        }, {
          nodeType : nodeType,
          type : 'list',
          target : {
            [this.props.itemReferenceNodePid] : this.state.contentsBindMap[key],
          },
        })
      });
  }

  bindContentsDirect(polygon, i, rule = {}){
    if( this.hasSubCuration ){
      return this.subCuration(polygon, i);
    }

    this.context.ice2.loadNodeType(this.state.displayGroup.itemReferenceType)
      .then((nodeType) => {

        let applyFuncKey = this.context.UIISystems.getUnique();
        this.context.UIISystems.openTool('ICE2Data', this.props.defaultPopupTarget || '[modal]' , {
          width:1024,
          height:600,
          UNIQUE_LAYER : true,
        }, {
          nodeType : nodeType,
          type : 'create',
          customPropertyUxMap:{
            ...rule.propertyUXMap,
          },
          footerBtnCallbacks : {
            'submit' : applyFuncKey,
          },
        }).then((closer) => {
          this.context.functionStore.register(applyFuncKey, (item)=>{


            if( item ){
              this.setDisplayItemId(polygon, item[this.props.itemReferenceNodePid]);
              closer();
            } else {
              message.error('아이템이 선택되지 않았습니다.');
            }
          })
        })
      });
  }


  bindContents(polygon, i, rule = {}){

    if( this.hasSubCuration ){
      return this.subCuration(polygon, i);
    }

    this.context.ice2.loadNodeType(this.state.displayGroup.itemReferenceType)
      .then((nodeType) => {

        let selectFuncKey = this.context.UIISystems.getUnique();
        this.context.UIISystems.openTool('ICE2Data', this.props.defaultPopupTarget || '[modal]' , {
          width:1024,
          height:600,
          UNIQUE_LAYER : true,
        }, {
          selectable : true,
          disableRegister : true,
          nodeType : nodeType,
          type : 'list',
          footerBtnCallbacks : {
            'select' : selectFuncKey,
          },
        }).then((closer) => {
          this.context.functionStore.register(selectFuncKey, (items)=>{
            let item = items[0];

            if( item ){
              this.setDisplayItemId(polygon, item[this.props.itemReferenceNodePid]);
              closer();
            } else {
              message.error('아이템이 선택되지 않았습니다.');
            }
          })
        })
      });
  }

  subCuration(poly, i){
    console.log('polygonItemIdMap>', this.state.polygonItemIdMap[poly.i]);
    let applyKey = this.context.UIISystems.getUnique();
    this.context.UIISystems.openTool('Curator', '[modal]', {
        width: 600,
        height: 500,
        DISABLE_DUPLICATE_UNDEPLOY: true,
        UNIQUE_LAYER: true,
      },
      {
        applyFunctionKey: applyKey,
        curationMethodSheet: {
          typeStructure : this.props.displayNodeBindHelper.typeStructure,
        },
        oldCurationParams: {},
        componentProps: this.props.componentProps,
        curationId : this.state.contentsBindMap[poly.i],
        upperData : {},
      }).then((closer) => {
      this.context.functionStore.register(applyKey, ({madeParams, item, id}) => {

        // console.log('iiiiii',item, id);
        this.setDisplayItemId(poly,id,i);
        closer();
      });
    });
  }

  needRenderPresetPanel(){
    let presets = this.props.geometryPresets;

    if( presets.length > 1 ){
      return true;
    } else {
      let preset = presets[0];

      if( !preset.hide && !preset.fix ){
        return true;
      }
    }

    return false;
  }


  componentDidMount(){
    let params = {
      includeReferenced : true,
    };

    if( this.props.geometryGroupId ){
      params.id_matching = this.props.geometryGroupId;
    }

    if( this.props.geometryExtraFilters ){
      for(let i = 0; i < this.props.geometryExtraFilters.length; i++ ){
        params['extraValue'+(i+1)+'_matching'] = this.props.geometryExtraFilters[i];
      }
    }

    this.context.ice2.get(`/node/gridDisplayGroup/list.json`, {
      params,
    }).then((res) => {
      let {data} = res;
      let {items} = data;
      let displayGroup = items[0];

      if( displayGroup ){
        let displayItems = displayGroup.referencedItems || [];


        let preset = this.props.geometryPresets.find((preset) => preset.key == displayGroup.gridPresetKey);

        let contentsBindMap = {};
        let polygonItemIdMap = {};

        let item;
        for(let i = 0; i < displayItems.length; i++ ){
          item = displayItems[i];
          contentsBindMap[item.i] = item.referenceId;
          polygonItemIdMap[item.i] = item.gridDisplayItemId;
        }


        this.setState({
          preset : cloneDeep(preset),

          displayGroup : {
            ...displayGroup,
            curationComponentType : this.props.curationComponentType,
            itemReferenceType : this.props.itemReferenceNodeType,
            itemReferencePid : this.props.itemReferenceNodePid,
          },

          displayItems : displayItems.map((item) => ({
            ...cloneDeep(item),
          })),

          contentsBindMap,
          polygonItemIdMap,
        });
      } else {
        let defaultPreset = this.props.geometryPresets[0];
        let defaultKey = defaultPreset.key;


        this.setState({
          preset : cloneDeep(defaultPreset),
          displayGroup : {
            title : '',
            description : '',
            gridPresetKey : defaultKey,
            curationComponentType : this.props.curationComponentType,
            itemReferenceType : this.props.itemReferenceNodeType,
            itemReferencePid : this.props.itemReferenceNodePid,
            extraValue1 : this.props.geometryExtraFilters[0] || null,
            extraValue2 : this.props.geometryExtraFilters[1] || null,
            extraValue3 : this.props.geometryExtraFilters[2] || null,
          },

          displayItems : [],
        });
      }
    })
  }


  renderAddButtons(poly, i){


    if( this.props.addRules ) {
      return <span>
        {this.props.addRules.map((rule, i) => <div
          key={i}
          className="action"
          onClick={() => {
            switch (rule.method) {
              case "create" :
                return this.bindContentsDirect(poly, i, rule);
              case "retrieve" :
                return this.bindContents(poly, i, rule);
              default:
                console.error('Unknown add rule.', rule);
            }
          }}>
          {rule.name}
        </div>)}
      </span>
    }


    return <div className="action-container">
      <div
        className="action"
        onClick={(e)=>this.bindContentsDirect(poly, i )}>
        {this.hasSubCuration ? '신규':'신규'}
      </div>
      <div
        className="action"
        onClick={(e)=>this.bindContents(poly, i )}>
        저장소
      </div>
    </div>
  }

  renderRectTooltipContents(poly, i){




    return <div>
      { this.state.contentsBindMap[poly.i] && <div className="actions">
        <div className="action-container">
          <div
            className="action"
            onClick={(e)=>this.removeConnectedContents(poly, i )}>
            삭제
          </div>

          <div
            className="action"
            onClick={(e)=>this.modifyConnectedContents(poly, i )}>
            {this.hasSubCuration ? '수정':'수정'}
          </div>


          { this.renderAddButtons(poly, i) }
        </div>
      </div>}

      <div className="contents">
        { this.state.contentsBindMap[poly.i] ?
          <Ice2NodeThumbnailDisplay
            ref={(comp) => this.thumbnailDisplay[poly.i] = comp}
            height='inherit'
            key={`${poly.i}/${this.state.contentsBindMap[poly.i]}`}
            tid={this.props.itemReferenceNodeType}
            pid={this.props.itemReferenceNodePid}
            id={this.state.contentsBindMap[poly.i]}
            rule={this.props.itemReferenceNodeThumbnailDisplayRule}/> :

          <div>


            { this.renderAddButtons(poly, i) }
          </div>
        }



      </div>
    </div>
  }

  renderRects(preset, polygons){
    this.thumbnailDisplay = {};


    let foundBasePreset = this.props.geometryPresets.find((p) => p.key == preset.key );
    let basePolygonMap = {};
    if( foundBasePreset ){
      for(let i = 0; i < foundBasePreset.polygons.length; i++ ){
        basePolygonMap[foundBasePreset.polygons[i].i] = foundBasePreset.polygons[i];
      }
    }

    return polygons.map((poly, i) => <div
      className={classnames('cell', this.state.contentsBindMap[poly.i] || 'empty' )}
      key={poly.i}>
      <Tooltip title={this.renderRectTooltipContents(poly,i)}>

        <div className="display-wrapper">
          <div className={classnames("contents")} >
            { this.state.contentsBindMap[poly.i]? <Ice2NodeThumbnailDisplay
                ref={(comp) => this.thumbnailDisplay[poly.i] = comp}
                height='inherit'
                key={`${poly.i}/${this.state.contentsBindMap[poly.i]}`}
                tid={this.props.itemReferenceNodeType}
                pid={this.props.itemReferenceNodePid}
                id={this.state.contentsBindMap[poly.i]}
                rule={this.props.itemReferenceNodeThumbnailDisplayRule}/> : '비어있음'}
          </div>
        </div>
      </Tooltip>
      <div className="key">
        {poly.i} {basePolygonMap[poly.i] && basePolygonMap[poly.i].realW && `(${basePolygonMap[poly.i].realW}x${basePolygonMap[poly.i].realH})`}
      </div>
    </div>)
  }

  renderPreset(preset){
    return <div className="preset" key={preset.key}>

      <GridLayout
        className="preset-grid"
        layout={preset.polygons}
        cols={preset.horizonCellLength}
        rowHeight={preset.miniRowHeight || preset.rowHeight || 30}
        isDraggable={true}
        isResizable={false}
        margin={preset.miniMargin || preset.margin || [0,0]}
        style={{ width : preset.miniWidth || preset.width || 100 }}
        width={preset.miniWidth || preset.width || 100 }>
        { preset.polygons.map((poly) => <div className="cell" key={poly.i}> <span className="text"> {poly.i} </span> </div> ) }
      </GridLayout>

      <div className="title" onClick={(e) => this.setDisplayGroupPreset(preset.key,e)}>
        <Radio
          value={preset.key}
          checked={preset.key == this.state.displayGroup.gridPresetKey}/> {preset.title}
      </div>
    </div>
  }

  render(){

    return <div className="geometry-node-display">
            <div className="">
              {/*<SementicLayout className="sementics">*/}
                {/*<Title>*/}
                  {/*기타 정보*/}
                {/*</Title>*/}
                {/*<Contents style={{padding:5}}>*/}
                 {/*<Row className="row">*/}
                   {/*<Col xs={3}>*/}
                     {/*<Label inColumn isRequired>*/}
                       {/*이름*/}
                     {/*</Label>*/}
                   {/*</Col>*/}
                  {/*<Col xs={15}>*/}
                    {/*<Input*/}
                      {/*value={this.state.displayGroup.title}*/}
                      {/*onChange={(e) => this.setState({*/}
                        {/*displayGroup :{*/}
                          {/*...this.state.displayGroup,*/}
                          {/*title : e.nativeEvent.target.value,*/}
                        {/*},*/}
                      {/*})}/>*/}
                  {/*</Col>*/}
                 {/*</Row>*/}
                  {/*<Row className="row">*/}
                    {/*<Col xs={3}>*/}
                      {/*<Label inColumn>*/}
                        {/*설명*/}
                      {/*</Label>*/}
                    {/*</Col>*/}
                    {/*<Col xs={15}>*/}
                      {/*<TextArea*/}
                        {/*value={this.state.displayGroup.description}*/}
                        {/*onChange={(e) => this.setState({*/}
                          {/*displayGroup :{*/}
                            {/*...this.state.displayGroup,*/}
                            {/*description : e.nativeEvent.target.value,*/}
                          {/*},*/}
                        {/*})}/>*/}
                    {/*</Col>*/}
                  {/*</Row>*/}
                {/*</Contents>*/}
              {/*</SementicLayout>*/}

              { this.needRenderPresetPanel() &&
                <SementicLayout className="preset-panel">
                  <Title>
                    {this.context.intl.formatMessage({id: 'app.geometry-node-display.preset'})}
                  </Title>
                  <Contents inStyle className="presets" style={{padding: 5}}>
                    {this.props.geometryPresets.map((preset) => this.renderPreset(preset))}
                  </Contents>
                </SementicLayout>
              }

              <SementicLayout className="viewport">
                <Title>
                  배치 및 컨텐츠연결
                </Title>

                <Contents inStyle style={{padding:5}}>
                  { this.state.preset && <GridLayout
                    className="perspective"
                    layout={this.state.preset.polygons}
                    cols={this.state.preset.horizonCellLength}
                    rowHeight={this.state.preset.rowHeight}
                    isDraggable={!this.state.preset.fix}
                    isResizable={this.state.preset.resizable || false}
                    margin={this.state.preset.margin || [0,0]}
                    style={{width:this.props.gridBoxWidth-5}}
                    width={this.props.gridBoxWidth-5}
                    onLayoutChange={(layout) => this.setState({
                      preset : {
                        ...this.state.preset,
                        polygons : layout,
                      },
                    })}>

                    { this.renderRects(this.state.preset, this.state.preset.polygons) }
                  </GridLayout> }
                </Contents>
              </SementicLayout>
            </div>
          </div>;
  }
}

