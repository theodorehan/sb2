import React, {Component} from 'react';
import PropTypes from 'prop-types';
// import {sortable} from 'react-sortable';
import { Form, Input, Tooltip, Icon, Cascader, Select, Row, Col, Checkbox, Button, AutoComplete } from 'antd';
import { Radio, message } from 'antd';
const { TextArea } = Input;
import Promise from 'bluebird';
import cloneDeep from 'lodash/cloneDeep';

import SortableItemList from '../Ice2SortableDataList/SortableItemList';
import ReferencesSortableDataList from '../Ice2SortableDataList/ReferencesSortableDataList';
import ReferenceItemWrapper from '../Ice2SortableDataList/referenceItemWrapper';
import {Label, Title, Contents, SementicLayout} from "../SementicLayout/index";

import './style.scss';

export default class NodeCurationGroup extends Component {

  static contextTypes = {
    ice2 : PropTypes.func,
    theme : PropTypes.object,
    UIISystems : PropTypes.object,
    functionStore : PropTypes.object,
    intl : PropTypes.object,
  };


  static propTypes = {
    defaultPopupTarget : PropTypes.string,
    sheet : PropTypes.object,
    implementedGroupNodeType : PropTypes.string,
    groupRetrieveType : PropTypes.oneOf(['oneOfList', 'read']),
    groupTargetParams : PropTypes.object,
    // thumbnailDisplayRule : PropTypes.object,
    groupReferenceItemThumbnailDisplayRule : PropTypes.object,
    upperData : PropTypes.object,
    managingPids : PropTypes.array,
    referencesNodeTypeTid : PropTypes.string,
    referencesNodeTypeIdPid : PropTypes.string,
    referenceCustomUxRuleMap : PropTypes.object,
    referencesPid : PropTypes.string,
    itemCurationSheet : PropTypes.object,
    itemHelper : PropTypes.object,
    addRules : PropTypes.array,
  };

  constructor(props){
    super(props);

    this.state = {

    };
  }


  async onApply(){
    let {item, id} = await this.RSDL.apply();
    return {item, id};
  }

  onClickItemCuration(item){

    let applyKey = this.context.UIISystems.getUnique();
    this.context.UIISystems.openTool('Curator', '[modal]', {
        width: 600,
        height: 500,
        DISABLE_DUPLICATE_UNDEPLOY: true,
        UNIQUE_LAYER: true,
      },
      {
        applyFunctionKey: applyKey,
        curationMethodSheet: this.props.itemCurationSheet,
        oldCurationParams: {},
        componentProps: this.props.componentProps,
        curationId : item[this.props.referencesNodeTypeIdPid],
        upperData : {},
      }).then((closer) => {
      this.context.functionStore.register(applyKey, ({madeParams, item, id}) => {

        closer();
      });
    });
  }

  onClickModifyItem(item){
    let submit = this.context.UIISystems.getUnique();
    this.context.ice2.loadNodeType(this.props.referencesNodeTypeTid)
      .then((nodeType) => {

        let submit = this.context.UIISystems.getUnique();
        this.context.UIISystems.openTool('ICE2Data', this.props.defaultPopupTarget || '[modal]' , {
          width:600,
          height:600,
          UNIQUE_LAYER : true,
        }, {
          nodeType : nodeType,
          type : 'modify',
          target : {
            [this.props.referencesNodeTypeIdPid] : item[this.props.referencesNodeTypeIdPid],
          },
          footerBtnCallbacks : {
            submit,
          },
        }).then((closer) => {
          this.context.functionStore.register(submit, (items)=>{
            this.RSDL.load();
            closer();
          })
        })
      });
  }

  itemOptionRenderer(item){
    if( this.props.itemCurationSheet ){

      return <span>
        <button className="sub-curation-btn" onClick={() => this.onClickModifyItem(item)}>
        항목 수정
      </button>;
        <button className="sub-curation-btn" onClick={() => this.onClickItemCuration(item)}>
        하위 큐레이션
      </button>;
      </span>
    }

    return null;
  }

  componentDidMount(){

  }


  render(){

    return <div className="node-curation-group">
      <SementicLayout>
        <Contents inStyle>
          <ReferencesSortableDataList
            ref={(comp) => this.RSDL = comp }
            nodeTypeTid={this.props.implementedGroupNodeType}
            retrieveType={this.props.groupRetrieveType}
            retrieveParams={this.props.groupTargetParams}
            referencesNodeTypeTid={this.props.referencesNodeTypeTid}
            referencesNodeTypeIdPid={this.props.referencesNodeTypeIdPid}
            referenceCustomUxRuleMap={this.props.referenceCustomUxRuleMap}
            referencesPid={this.props.referencesPid}
            referenceItemThumbnailDisplayRule={this.props.groupReferenceItemThumbnailDisplayRule}
            managingPids={this.props.managingPids}
            addRules={this.props.addRules}
            itemHelper={this.props.itemHelper}
            itemOptionRenderer={::this.itemOptionRenderer}
            listManagementTitle={<Title>
              {this.context.intl.formatMessage({id: 'app.node-curation-group.display-item-list-management'})}
            </Title>}/>
        </Contents>
      </SementicLayout>
    </div>
  }
}



/**

  nodeCurationGroup 인터페이스가 구현된 프로퍼티 구조
 {
   "typeId": "propertyType",
   "tid": "nodeCurationGroup",
   "pid": "nodeCurationGroupId",
   "propertyTypeName": "ID",
   "valueType": "INT",
   "required": true,
   "idable": true,
   "labelable": false,
   "treeable": false,
   "indexable": true,
   "analyzer": "code",
   "referenceType": "",
   "referenceValue": "",
   "codeFilter": "",
   "defaultValue": "",
   "idType": "autoIncrement"
 },
 {
   "typeId": "propertyType",
   "tid": "nodeCurationGroup",
   "pid": "title",
   "propertyTypeName": "그룹 제목",
   "valueType": "STRING",
   "required": false,
   "idable": false,
   "labelable": true,
   "treeable": false,
   "indexable": true,
   "analyzer": "code",
   "referenceType": "",
   "referenceValue": "",
   "codeFilter": "",
   "defaultValue": ""
 },
 {
   "typeId": "propertyType",
   "tid": "nodeCurationGroup",
   "pid": "description",
   "propertyTypeName": "그룹 내용",
   "valueType": "TEXT",
   "required": false,
   "idable": false,
   "labelable": false,
   "treeable": false,
   "indexable": false,
   "analyzer": "code",
   "referenceType": "",
   "referenceValue": "",
   "codeFilter": "",
   "defaultValue": ""
 },
 {
   "typeId": "propertyType",
   "tid": "nodeCurationGroup",
   "pid": "extraValue1",
   "propertyTypeName": "추가 분류값 1",
   "valueType": "STRING",
   "required": false,
   "idable": false,
   "labelable": false,
   "treeable": false,
   "indexable": true,
   "analyzer": "code",
   "referenceType": "",
   "referenceValue": "",
   "codeFilter": "",
   "defaultValue": ""
 },
 {
   "typeId": "propertyType",
   "tid": "nodeCurationGroup",
   "pid": "extraValue2",
   "propertyTypeName": "추가 분류값 2",
   "valueType": "STRING",
   "required": false,
   "idable": false,
   "labelable": false,
   "treeable": false,
   "indexable": true,
   "analyzer": "code",
   "referenceType": "",
   "referenceValue": "",
   "codeFilter": "",
   "defaultValue": ""
 },
 {
   "typeId": "propertyType",
   "tid": "nodeCurationGroup",
   "pid": "extraValue3",
   "propertyTypeName": "추가 분류값 3",
   "valueType": "STRING",
   "required": false,
   "idable": false,
   "labelable": false,
   "treeable": false,
   "indexable": true,
   "analyzer": "code",
   "referenceType": "",
   "referenceValue": "",
   "codeFilter": "",
   "defaultValue": ""
 },
 {
   "typeId": "propertyType",
   "tid": "nodeCurationGroup",
   "pid": "extraValue4",
   "propertyTypeName": "추가 분류값 4",
   "valueType": "STRING",
   "required": false,
   "idable": false,
   "labelable": false,
   "treeable": false,
   "indexable": true,
   "analyzer": "code",
   "referenceType": "",
   "referenceValue": "",
   "codeFilter": "",
   "defaultValue": ""
 },
 {
   "typeId": "propertyType",
   "tid": "nodeCurationGroup",
   "pid": "extraValue5",
   "propertyTypeName": "추가 분류값 5",
   "valueType": "STRING",
   "required": false,
   "idable": false,
   "labelable": false,
   "treeable": false,
   "indexable": true,
   "analyzer": "code",
   "referenceType": "",
   "referenceValue": "",
   "codeFilter": "",
   "defaultValue": ""
 },
 {
   "typeId": "propertyType",
   "tid": "nodeCurationGroup",
   "pid": "curationComponentType",
   "propertyTypeName": "대상 컴포넌트 타입",
   "valueType": "STRING",
   "required": false,
   "idable": false,
   "labelable": false,
   "treeable": false,
   "indexable": true,
   "analyzer": "code",
   "referenceType": "",
   "referenceValue": "",
   "codeFilter": "",
   "defaultValue": ""
 },
 {
   "typeId": "propertyType",
   "tid": "nodeCurationGroup",
   "pid": "displayType",
   "propertyTypeName": "표시 형식",
   "valueType": "STRING",
   "required": false,
   "idable": false,
   "labelable": false,
   "treeable": false,
   "indexable": true,
   "analyzer": "code",
   "referenceType": "",
   "referenceValue": "",
   "codeFilter": "",
   "defaultValue": ""
 },
 {
   "typeId": "propertyType",
   "tid": "nodeCurationGroup",
   "pid": "referenceItems",
   "propertyTypeName": "참조 아이템 목록",
   "valueType": "REFERENCES",
   "required": false,
   "idable": false,
   "labelable": false,
   "treeable": true,
   "indexable": false,
   "analyzer": "code",
   "referenceType": "urlBasedBanner",
   "referenceValue": "urlBasedBannerId",
   "codeFilter": "",
   "defaultValue": ""
 }


 */
