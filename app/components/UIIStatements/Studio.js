import defineUIIStatement from '../../utils/UII/defineUIIStatement';
import defineActSheet from '../../utils/UII/actSheet';
import {
  Types,
} from '../../utils/UII/actSheet';

export const OnAirStatus = defineUIIStatement({
  type : 'button',
  key : 'OnAirStatus',

  titleType : 'unit',
  title : 'OnAirConnected.StatusText',
  // titleColor : '#fff',

  iconType : 'unit',
  icon : 'OnAirConnected.StatusIcon',
  // iconColor : '#ce2348',

  options : {
    sideLabel : true,
  },
});


export const DeviceModeSwitch = defineUIIStatement({
  type : 'button',
  key : 'deviceModeSwitch',

  titleType : 'unit',
  title : 'OnAirConnected.StatusText',
  // titleColor : '#fff',

  iconType : 'unit',
  icon : 'DeviceMode.Switch',
  // iconColor : '#ce2348',

  options : {
    sideLabel : true,
  },
});


export const DeviceModeSmaller = defineUIIStatement({
  type : 'button',
  key : 'DeviceModeSmaller',

  title : 'Smaller',
  // titleColor : '#fff',

  iconType : 'fa',
  icon : 'mobile',
  // iconColor : 'rgb(136, 183, 220)',

  actionSheet : defineActSheet({
    "action" : "Studio.CHANGE_CANVAS_DEVICE_MODE",
    "type": Types.ReduxAction, // ToolInterfacePipeline
    "options" : {
      "actionParams" : ['smaller'],
    },
  }),
});

export const DeviceModeMobile = defineUIIStatement({
  type : 'button',
  key : 'DeviceModeMobile',

  title : 'Mobile',
  // titleColor : '#fff',

  iconType : 'fa',
  icon : 'mobile',
  // iconColor : 'rgb(136, 183, 220)',

  actionSheet : defineActSheet({
    "action" : "Studio.CHANGE_CANVAS_DEVICE_MODE",
    "type": Types.ReduxAction, // ToolInterfacePipeline
    "options" : {
      "actionParams" : ['mobile'],
    },
  }),
});

export const DeviceModeTablet = defineUIIStatement({
  type : 'button',
  key : 'DeviceModeTablet',

  title : 'Tablet',
  // titleColor : '#fff',

  iconType : 'fa',
  icon : 'tablet',
  // iconColor : 'rgb(136, 183, 220)',

  actionSheet : defineActSheet({
    "action" : "Studio.CHANGE_CANVAS_DEVICE_MODE",
    "type": Types.ReduxAction, // ToolInterfacePipeline
    "options" : {
      "actionParams" : ['tablet'],
    },
  }),

});

export const DeviceModeDesktop = defineUIIStatement({
  type : 'button',
  key : 'DeviceModeDesktop',

  title : 'Desktop',
  // titleColor : '#fff',

  iconType : 'fa',
  icon : 'desktop',
  // iconColor : 'rgb(136, 183, 220)',

  actionSheet : defineActSheet({
    "action" : "Studio.CHANGE_CANVAS_DEVICE_MODE",
    "type": Types.ReduxAction, // ToolInterfacePipeline
    "options" : {
      "actionParams" : ['desktop'],
    },
  }),

});

export const Fullscreen = defineUIIStatement({
  type : 'button',
  key : 'DeviceModeDesktop',

  title : 'Fullscreen',
  // titleColor : '#fff',

  iconType : 'material',
  icon : 'NavigationFullscreen',
  // iconColor : 'rgb(136, 183, 220)',

  actionSheet : defineActSheet({
    "action" : "toggle-fullscreen",
    "type": Types.InlineAction, // ToolInterfacePipeline
    "options" : {

    },
  }),

});

export const DeviceModeFit = defineUIIStatement({
  type : 'button',
  key : 'DeviceModeDesktop',

  title : 'Fit',
  // titleColor : '#fff',

  iconType : 'material',
  icon : 'ActionSettingsOverscan',
  // iconColor : 'rgb(136, 183, 220)',

  actionSheet : defineActSheet({
    "action" : "Studio.CHANGE_CANVAS_DEVICE_MODE",
    "type": Types.ReduxAction, // ToolInterfacePipeline
    "options" : {
      "actionParams" : ['fit'],
    },
  }),

});


export const ExitButton = defineUIIStatement({
  type : 'button',
  key : 'DeviceModeDesktop',

  title : 'Exit',
  // titleColor : '#fff',

  iconType : 'material',
  icon : 'EditorMultilineChart',
  // iconColor : 'rgb(136, 183, 220)',

  actionSheet : defineActSheet({
    "action" : "exit",
    "type": Types.InlineAction, // ToolInterfacePipeline
    "options" : {

    },
  }),

});
