import defineUIIStatement from '../../utils/UII/defineUIIStatement';

export const UserDisplay = defineUIIStatement({
  type : 'button',
  key : 'UserDisplay',

  titleType : 'unit',
  title : 'UserDisplayUnitConnected.UserName',
  // titleColor : '#fff',

  iconType : 'unit',
  icon : 'UserDisplayUnitConnected.AvatarIcon',
  // iconColor : '#ce2348',

  options : {
    sideLabel : true,
  },
});
