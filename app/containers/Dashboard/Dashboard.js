import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import './style.scss'
import AppBar from 'material-ui/AppBar';
import MenuIcon from 'material-ui/svg-icons/navigation/menu';
import CartIcon from 'material-ui/svg-icons/action/shopping-cart';
import ClearIcon from 'material-ui/svg-icons/content/clear';
import IconButton from 'material-ui/IconButton';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import ModalLayoutContainer from '../../Core/Container/ModalLayoutContainer';
import Supervisor from '../../Core/Container/Supervisor';

import Waterfall from 'async/waterfall';
import ScrollWrapper from 'react-scrollbar';
import messages from './messages';
import {FormattedMessage} from 'react-intl';


import Slider from 'react-slick';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import moment from 'moment';

import { notification, Icon } from 'antd';
// Actions
import * as UserActions from '../../actions/User';
import * as DashboardActions from '../../actions/Dashboard';

// Networks
import * as UserNetwork from '../../Core/Network/User';
import * as SiteNetwork from '../../Core/Network/Site';
import * as DashboardNetwork from '../../Core/Network/Dashboard';
import ION_CI from '../../components/artworks/IonCI';

// Dashboard Components
import ThemeItem from '../../components/Dashboard/Theme';


@connect((state) => (
  {
    User : state.get('User'),
    Dashboard : state.get('Dashboard'),
  }
))
export default class Dashboard extends Component {

  static contextTypes = {
    router : PropTypes.object,
    UIISystems : PropTypes.object,
    intl : PropTypes.object,
    global : PropTypes.object,
  }

  constructor(props) {
    super(props);
  }


  onClickMenuIcon(e){
    e.stopPropagation();
    e.preventDefault();

    if( !this.props.Dashboard.menuOpened ){
      this.props.dispatch(DashboardActions.OPEN_MENU());
    } else {
      this.props.dispatch(DashboardActions.CLOSE_MENU());
    }

    return false;
  }


  componentDidMount() {
    let {dispatch} = this.props;

    // console.log(this.context.intl.formatMessage({id:'app.common.edit'}));
    // Start Data Loading
    Waterfall([
        function signIn(_cb) {

          UserNetwork.signIn('system', 'system')
            .then(function (_result) {
              let {status, data} = _result;

              if (status === 200 && data.result === 'success') {

                dispatch(UserActions.SIGN_IN(data.sessionID));
                dispatch(UserActions.LOAD_USER(data));


                _cb(null, data);
              } else {
                _cb({
                  point: 'signIn',
                  code: 'FAIL_USER_SIGNIN',
                });
              }
            });
        },

        function(user, _cb){
          // template list

          DashboardNetwork.siteTemplates(user.sessionID)
            .then((res)=>{
              let {data} = res;
              let {list} = data;


              dispatch(DashboardActions.LOADED_TEMPLATES(list));

              _cb(null, user, list);
            })
        },

        function(user, templates, _cb){
          // template list

          DashboardNetwork.mySites(user.sessionID)
            .then((res)=>{
              let {data} = res;
              let {list} = data;

              dispatch(DashboardActions.LOADED_SITES(list));

              _cb(null, user, templates, list);
            })
        },
      ],
      function (err, result) {

      });
  }

  onSiteSetting(item){

  }

  onEnterSiteStudio(item){
    this.context.router.history.push(`/studio?site=${item.accessKey}&owner=${item.siteOwner}`);
  }

  onUpdateTheme(item){
    let reqID = this.context.global.uiBlockingReqBegin();
    SiteNetwork.updateTheme(this.props.User.session.id, item.info.siteAccessName, item.info.owner)
      .then((res)=>{

        DashboardNetwork.mySites(this.props.User.info.sessionID)
          .then((res)=>{
            let {data} = res;
            let {list} = data;

            this.props.dispatch(DashboardActions.LOADED_SITES(list));
            this.context.global.uiBlockingReqFinish(reqID);



            notification.open({
              message: 'Update Complete',
              // description: 'This is the content of the notification. This is the content of the notification. This is the content of the notification.',
              icon: <Icon type="smile-circle" style={{ color: '#108ee9' }} />,
            });
          })
      });
  }

  renderSiteItem(item, i ){
    let themeName = item.info.themeName;

    let foundThemeItem = this.props.Dashboard.templates.find((item) => item.info.themeName == themeName );
    let themeVersion = foundThemeItem.version;
    let siteThemeVersion = item.version;


    var settings = {
      dots: false,
      infinite: true,
      speed: 500,
      // slidesToShow: 1,
      // slidesToScroll: 1,
      swipe : false,
      arrows : false,
      nextArrow:false,
      prevArrow : false,
      autoplay:true,
      autoPlay:true,
      autoPlaySpeed : 100,
      fade:true,
    };



    return <div key={i} className="photo-item-site">
      <div className="version">{item.version}</div>
      {/*<div className="thumb">*/}
        {/*<Slider {...settings} style={{width:'100%'}}>*/}
          {/*{item.thumbnails.map(*/}
            {/*(i) => <div key={i} style={{height:'auto'}}>*/}
              {/*<img src={`/api/user/${this.props.User.session.id}/site/${item.name}/${item.siteOwner}/store/site-thumbnail?name=${i}`}/>*/}
            {/*</div>)*/}
          {/*}*/}
        {/*</Slider>*/}
      {/*</div>*/}

      <div className="introduce">
        {
          item.info.serviceVersion && <div className="row">
            <span className="label">
              <FormattedMessage id="app.common.service-version"/> :
            </span>
            <span className="value">
              {item.info.serviceVersion}#{item.info.modifyCount || 0}
            </span>
          </div>
        }

        <div className="row">
          <span className="label"> <FormattedMessage id="app.common.owner"/> : </span> <span className="value">{item.info.owner}</span>
        </div>
        <div className="row">
          <span className="label"> <FormattedMessage id="app.common.site-name"/> : </span> <span className="value">{item.info.siteName}</span>
        </div>
        <div className="row">
          <span className="label"> <FormattedMessage id="app.common.created"/> : </span> <span className="value">{moment(item.info.created).format('MM DD YYYY, hh:mm a')}</span>
        </div>
        {
          item.info.modified && <div className="row">
            <span className="label"> <FormattedMessage id="app.common.modified"/> : </span> <span className="value">{moment(item.info.modified).format('MM DD YYYY, hh:mm a')}</span>
          </div>
        }

        {
          item.info.published ? <div className="row">
            <span className="label">
              <FormattedMessage id="app.common.last-published"/> :
            </span>
            <span className="value">
              {moment(item.info.lastPublished).format('MM DD YYYY, hh:mm a')}
            </span>
          </div> :
            <div className="row">
              <span className="label">
                <FormattedMessage id="app.common.last-published"/> :
              </span>
              <span className="value">
                 <FormattedMessage id="app.common.none-published"/>
              </span>
            </div>
        }


      </div>


      <div className="ui">
        <button
          onClick={()=> this.onUpdateTheme(item) }
          disabled={themeVersion === siteThemeVersion}
          className={themeVersion !== siteThemeVersion ? 'asap' : ''}
          title={this.context.intl.formatMessage({id:'app.common.asap-update'})}>
          <FormattedMessage id="app.common.update"/>
        </button>

        <button onClick={()=> this.onSiteSetting(item) }>
          <FormattedMessage id="app.common.setting"/>
        </button>

        <button onClick={()=> this.onEnterSiteStudio(item) }>
          <FormattedMessage id="app.common.edit"/>
        </button>

        <button onClick={()=> this.onUpdateTheme(item) }>
          <FormattedMessage id="app.common.publish"/>
        </button>
      </div>


      <button className="right-top-button">
        <ClearIcon/>
      </button>
    </div>;
  }


  render() {


    if( this.props.Dashboard.creatingSite ){
      return <div className="dashboard">
        <ION_CI style={{position:'absolute', left:'50%', top:'50%', marginLeft:-200, marginTop:-200}}/>
      </div>;
    }

    return (
      <div className="dashboard">
        <Supervisor ref={(supervisor) => {this.supervisor = supervisor}}>
          <ModalLayoutContainer isa="modal">
            <AppBar
              title={<FormattedMessage {...messages.title}/>}
              onLeftIconButtonTouchTap={:: this.onClickMenuIcon}
              iconElementLeft={
                <IconButton tooltip="Menu" touch={true} tooltipPosition="bottom-right">
                  <MenuIcon color="#fff"/>
                </IconButton>}

              iconElementRight={
                <span style={{color:'#fff'}}>{process.env.BUILDER_VERSION}</span>
              }

              style={{backgroundColor : "#09181f"}}
              titleStyle={{color:'#fff', fontFamily:'Nanum Square,Open Sans,sans-serif', fontWeight:'normal'}}
            />

            <Drawer open={this.props.Dashboard.menuOpened} containerStyle={{backgroundColor : "rgb(9, 24, 31)"}}>
              <MenuItem style={{height:64}} onTouchTap={:: this.onClickMenuIcon}>

                <IconButton touch={true} style={{marginTop:8}}>
                  <MenuIcon color="#fff"/>
                </IconButton>

              </MenuItem>

              <MenuItem><CartIcon/> <FormattedMessage {...messages.themes}/></MenuItem>
            </Drawer>

            <div className="container">
              <ScrollWrapper style={{width:'100%', height:'100%'}}>
                <h1 className="title"> <FormattedMessage {...messages.sites}/> </h1>
                <div className="photo-grid">
                  { this.props.Dashboard.sites.map( (item, i) => this.renderSiteItem(item, i) )}
                </div>

                <h1> <FormattedMessage {...messages.themes}/> </h1>
                <div className="photo-grid">
                  { this.props.Dashboard.templates.map( (item, i) => <ThemeItem item={item} key={i} dispatch={this.props.dispatch} User={this.props.User} /> ) }
                </div>
              </ScrollWrapper>
            </div>
          </ModalLayoutContainer>
        </Supervisor>
      </div>
    )
  }
}
