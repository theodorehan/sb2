import { defineMessages } from 'react-intl';

export default defineMessages({
  title: {
    id: 'app.dashboard.title',
    defaultMessage: 'Dashboard',
  },
  sites: {
    id: 'app.dashboard.sites',
    defaultMessage: 'Sites',
  },
  themes: {
    id: 'app.dashboard.themes',
    defaultMessage: 'Themes',
  },
});
