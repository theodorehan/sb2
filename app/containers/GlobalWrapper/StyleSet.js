
export const light = {
  UII: {
    iconColor: 'rgb(136, 183, 220)',
    titleColor: '#5a5a5a',
  },

  CategoryBox: {
    fullmoonBtn: {
      aside: {
        textColor: {
          active: '#fff',
          hovered: '',
          default: 'rgb(68, 68, 68)',
          danger: 'rgb(255, 237, 237)',
          proceed: 'rgb(255, 255, 255)',
        },
        bgColor: {
          active: '#1c8aff',
          hovered: '#42a4ff',
          default: '',
          danger: 'rgb(212, 70, 70)',
          dangerHovered: 'rgb(218, 42, 42)',
          proceed: 'rgb(44, 150, 2)',
          proceedHovered: 'rgb(53, 181, 2)',
        },
      },
    },
  },

  Tool: {
    layout: {
      header: {
        actionBtn: {
          color: {
            default: '#fff',
          },
        },
      },

      footer: {
        fullmoonBtn: {
          textColor: {
            active: '#fff',
            hovered: '',
            default: 'rgb(68, 68, 68)',
            danger: 'rgb(255, 237, 237)',
            proceed: 'rgb(255, 255, 255)',
          },
          bgColor: {
            active: '#1c8aff',
            hovered: '#42a4ff',
            default: 'rgb(208, 208, 208)',
            danger: 'rgb(212, 70, 70)',
            dangerHovered: 'rgb(218, 42, 42)',
            proceed: 'rgb(44, 150, 2)',
            proceedHovered: 'rgb(53, 181, 2)',
          },
        },
      },
    },
  },


  mui: {
    "spacing": {
      "iconSize": 24,
      "desktopGutter": 24,
      "desktopGutterMore": 32,
      "desktopGutterLess": 16,
      "desktopGutterMini": 8,
      "desktopKeylineIncrement": 64,
      "desktopDropDownMenuItemHeight": 32,
      "desktopDropDownMenuFontSize": 15,
      "desktopDrawerMenuItemHeight": 48,
      "desktopSubheaderHeight": 48,
      "desktopToolbarHeight": 56,
    },
    "fontFamily": "Nanum Square",
    "borderRadius": 2,
    "palette": {
      "primary1Color": "#0868a7",
      "primary2Color": "#0879a7",
      "primary3Color": "#757575",
      "accent1Color": "#ff4081",
      "accent2Color": "#f50057",
      "accent3Color": "#ff80ab",
      "textColor": "rgba(30, 30, 30, 1)",
      "secondaryTextColor": "rgba(30, 30, 30, 0.7)",
      "alternateTextColor": "#303030",
      "canvasColor": "#fff",
      "borderColor": "rgba(0, 0, 0, 0.1)",
      "disabledColor": "rgba(0, 0, 0, 0.3)",
      "pickerHeaderColor": "rgba(0, 0, 0, 0.12)",
      "clockCircleColor": "rgba(0, 0, 0, 0.12)",
    },
  },
}

export const dark = {
  UII: {
    iconColor: 'rgb(136, 183, 220)',
    titleColor: '#5a5a5a',
  },

  CategoryBox: {
    fullmoonBtn: {
      aside: {
        textColor: {
          active: '#fff',
          hovered: '',
          default: '#aaa',
          danger: 'rgb(255, 237, 237)',
          proceed: 'rgb(255, 255, 255)',
        },
        bgColor: {
          active: '#1c8aff',
          hovered: '#42a4ff',
          default: '',
          danger: 'rgb(212, 70, 70)',
          dangerHovered: 'rgb(218, 42, 42)',
          proceed: 'rgb(44, 150, 2)',
          proceedHovered: 'rgb(53, 181, 2)',
        },
      },
    },
  },

  Tool: {
    layout: {
      header: {
        actionBtn: {
          color: {
            default: '#333',
          },
        },
      },

      footer: {
        fullmoonBtn: {
          textColor: {
            active: '#fff',
            hovered: '',
            default: 'rgb(217, 222, 224)',
            danger: 'rgb(255, 237, 237)',
            proceed: 'rgb(255, 255, 255)',
          },
          bgColor: {
            active: '#1c8aff',
            hovered: 'rgb(17, 66, 88)',
            default: 'rgb(4, 21, 29)',
            danger: 'rgb(212, 70, 70)',
            dangerHovered: 'rgb(218, 42, 42)',
            proceed: 'rgb(44, 150, 2)',
            proceedHovered: 'rgb(53, 181, 2)',
          },
        },
      },
    },
  },

  mui: {
    "spacing": {
      "iconSize": 24,
      "desktopGutter": 24,
      "desktopGutterMore": 32,
      "desktopGutterLess": 16,
      "desktopGutterMini": 8,
      "desktopKeylineIncrement": 64,
      "desktopDropDownMenuItemHeight": 32,
      "desktopDropDownMenuFontSize": 15,
      "desktopDrawerMenuItemHeight": 48,
      "desktopSubheaderHeight": 48,
      "desktopToolbarHeight": 56,
    },
    "fontFamily": "Nanum Square",
    "borderRadius": 2,
    "palette": {
      "primary1Color": "#00bcd4",
      "primary2Color": "#0097a7",
      "primary3Color": "#bdbdbd",
      "accent1Color": "#ff4081",
      "accent2Color": "#f5f5f5",
      "accent3Color": "#9e9e9e",
      "textColor": "rgba(0, 0, 0, 0.87)",
      "secondaryTextColor": "rgba(0, 0, 0, 0.54)",
      "alternateTextColor": "#ffffff",
      "canvasColor": "#ffffff",
      "borderColor": "#e0e0e0",
      "disabledColor": "rgba(0, 0, 0, 0.3)",
      "pickerHeaderColor": "#00bcd4",
      "clockCircleColor": "rgba(0, 0, 0, 0.07)",
      "shadowColor": "rgba(0, 0, 0, 1)",
    },
  },
}
