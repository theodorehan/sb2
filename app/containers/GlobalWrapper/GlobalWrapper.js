import React , {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import uuid from 'uuid';
import classnames from 'classnames';

import MilkywayGraphics from '../../components/artworks/MilkywayGraphics'
import ION_CI from '../../components/artworks/IonCI';
import Loading from '../../components/artworks/Loading';

import * as NetworkActions from '../../actions/Network';
import * as ThemeStyleSet from './StyleSet';


// Material UI -->
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';

import './style.scss';



@connect( (state) => ({
  Studio : state.get('Studio'),
  Network : state.get('Network'),
}))
export default class GlobalWrapper extends Component{
  static childContextTypes = {
    global : PropTypes.object,
    theme : PropTypes.object,
  }


  constructor(props) {
    super(props);
  }

  getChildContext() {
    return {
      global: {
        uiBlockingReqBegin : (reqID = uuid.v4()) => {

          this.props.dispatch(NetworkActions.UI_BLOCKING_REQUEST_BEGIN(reqID));

          return reqID;
        },

        uiBlockingReqFinish : (reqID) => {
          if( !reqID ){
            throw new Error("First parameter[reqID] is required");
          }

          this.props.dispatch(NetworkActions.UI_BLOCKING_REQUEST_FINISH(reqID));
        },
      },

      theme : ThemeStyleSet[this.props.Studio.etc.theme],
    };
  }



  render(){


    return <MuiThemeProvider muiTheme={getMuiTheme(ThemeStyleSet[this.props.Studio.etc.theme].mui)}>
      <div className={classnames("global-wrapper", this.props.Studio.etc.theme+"-theme")} >
        { this.props.Network.ui_blocking_requests.length > 0 && <div className="ui-blocking-loading">
          <Loading
            texts={['Processing...', '사이트 컴포넌트 조각 모음중..', 'Please wait for use', 'Little bit more.....']}
            textStyle={{position:'fixed', top:'50%', marginTop:100}}
            style={{position:'absolute', left:'50%', top:'50%', marginLeft:-200, marginTop:-200}}/>
        </div>}




        { this.props.Studio.boot.status !== 'running' && <MilkywayGraphics position='fixed'/> }
        { this.props.children }
      </div>
    </MuiThemeProvider>
  }
}
