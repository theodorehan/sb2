/*
 *
 * LanguageProvider reducer
 *
 */
import ko from '../../translations/ko.json';
import cn from '../../translations/cn.json';
import jp from '../../translations/jp.json';
import en from '../../translations/en.json';
import de from '../../translations/de.json';

import { fromJS } from 'immutable';

import {
  CHANGE_LOCALE,
  DEFAULT_LOCALE,
} from './constants';

const initialState = {
  locale: DEFAULT_LOCALE,
  lang : {
    ko,
    cn,
    jp,
    en,
    de,
  },
};

function languageProviderReducer(state = initialState, action) {
  switch (action.type) {
    case CHANGE_LOCALE:
      return state
        .set('locale', action.locale);
    default:
      return state;
  }
}

export default languageProviderReducer;
