// @flow
// import {ipcRenderer} from 'electron';
import JSCookie from 'js-cookie';
import { notification, Icon } from 'antd';
import shajs from 'sha.js';

import SHA256 from 'crypto-js/sha256';

import React, {Component} from 'react';
import PropTypes from 'prop-types';

import TextField from 'material-ui/TextField';
import Toggle from 'material-ui/Toggle';
// <-- Material UI
import HeadLine from '../../components/NonStudioLayout/headline';
import FootLine from '../../components/NonStudioLayout/footline.js';
import FullmoonBtn from '../../components/Unit/FullmoonButton/FullmoonButton';

import '../NonStudioCommon/layout.scss'
import '../NonStudioCommon/elements.scss'


import {signInIce} from "../../Core/Network/User";

class SigninScene extends Component {
  // static propTypes = {
  //   stageSceneLoaded: PropTypes.bool.isRequired
  // };


  constructor(){
    super();


    this.state = {
      id : JSCookie.get('sb-id') || '',
      pw : '',
      remember :!!JSCookie.get('sb-id'),
    }
  }


  static contextTypes = {
     UIISystems : PropTypes.object,
    intl : PropTypes.object,
    global : PropTypes.object,
    router: PropTypes.object,
  }


  async signIn(){

    let reqID = this.context.global.uiBlockingReqBegin();

    let pw = this.state.pw ? shajs('sha256').update(this.state.pw).digest('base64') : '';

    try{

      let result = await signInIce(this.state.id, pw);

      if( result && this.state.remember ){
        JSCookie.set('sb-id', this.state.id);
      } else {
        JSCookie.set('sb-id', '');
      }

      notification.open({
        message: 'Login Success',
        // description: 'This is the content of the notification. This is the content of the notification. This is the content of the notification.',
        icon: <Icon type="smile-circle" style={{ color: '#108ee9' }} />,
      });


      let query = location.search;

      let queries = query.replace(/^\?/,'').split('&');
      let queryParamMap = {};

      for(let i = 0; i < queries.length; i++ ){
        let pair = queries[i].split('=');
        queryParamMap[pair[0]] = decodeURIComponent(pair[1]);
      }

      if( queryParamMap.from ){
        location.href = queryParamMap.from;
      } else {
        location.href = '/dashboard';
      }
    } catch(e){
      notification.open({
        message: 'Login Fail',
        description: e.response.data.reason,
        icon: <Icon type="meh" style={{ color: '#108ee9' }} />,
      });
      console.dir(e);
    }


    this.context.global.uiBlockingReqFinish(reqID);

  }

  onChange(key, value){
    this.setState({
      [key] : value,
    });
  }

  onKeyUp(e){
    let {nativeEvent} =e;

    if( nativeEvent.keyCode == 13 ){
      this.signIn();
    }
    // console.log(e.keyCode)
  }

  render() {
    // console.log( this.prop.stageSceneLoaded );


    // setTimeout(()=>{
    //   this.props.push('/stage')
    // }, 2000)

    return (
      <div className='index'>
        <HeadLine title="signin"/>

        <div className='contentline'>
          <div className='container'>
            <div className='left-zone'>
              <div className='inline-block vertical-middle'>
                { <FullmoonBtn
                  normalStyle={{
                    width: 100,
                    height: 100,
                    borderWidth: 0,
                    backgroundImage: 'url(/public/svg/chevron-left.svg)',

                    borderRadius: '50%',
                    fontSize: 25,
                    color: '#fff',
                    fontFamily: 'Nanum Square',
                    display: 'block',
                    borderColor: 'rgb(11, 120, 227)',
                    marginTop: 10,
                  }}
                  hoverStyle={{
                    borderWidth: 5,
                  }}
                  moonBlender='overlay'
                  useImageDesc={true}
                  onClick={ () => this.context.router.history.push('/') }/>
                }

              </div>
            </div>

            <div className='center-zone'>

              <div className='inline-block vertical-middle'>

                <TextField
                  value={this.state['id']}
                  onChange={(e, newVal) => this.onChange('id', newVal)}
                  style={{display: 'block'}}
                  hintText="System"
                  floatingLabelStyle={{color:'rgba(255,255,255,.7)', fontFamily:'Nanum Square'}}
                  floatingLabelShrinkStyle={{ color : '#d3fffc'}}
                  inputStyle={{ color:'#f5ffd7' }}
                  underlineStyle={{borderBottom : '1px solid rgba(255,255,255,0.2)'}}
                  floatingLabelText="Input your Id"/>

                <TextField
                  value={this.state['pw']}
                  onChange={(e, newVal) => this.onChange('pw', newVal)}
                  onKeyUp={(e) => this.onKeyUp(e)}
                  style={{display: 'block'}}
                  hintText="PassW0rd **"
                  type="password"
                  floatingLabelStyle={{color:'rgba(255,255,255,.7)', fontFamily:'Nanum Square'}}
                  floatingLabelShrinkStyle={{ color : '#d3fffc'}}
                  inputStyle={{ color:'#f5ffd7' ,fontFamily:'Sans serif'}}
                  underlineStyle={{borderBottom : '1px solid rgba(255,255,255,0.2)'}}
                  floatingLabelText="Input your passW0rd!"/>

                <Toggle
                  style={{textAlign: 'left'}}
                  labelStyle={{ color:'#bbffe7' }}
                  label="Remember me"
                  toggled={this.state.remember}
                  onToggle={(e, t) => this.setState({remember : t})} />

                <FullmoonBtn
                  normalStyle={{
                    display: 'inline-block',
                    width: '45%',
                    height: 30,
                    borderWidth: 1,

                    borderRadius: 22.5,
                    fontSize: 14,
                    color: '#fff',
                    fontFamily: 'Nanum Square',
                    borderColor: 'rgba(255, 255, 255,0.2)',
                    marginTop: 10,
                    marginRight: '5%',
                  }}
                  hoverStyle={{
                    borderWidth: 3,
                    borderColor: 'rgb(11, 120, 227)',
                  }}
                  moonBlender='overlay'
                  onClick={ () => this.context.router.history.push('/signup') }>

                  Sign up
                </FullmoonBtn>

                <FullmoonBtn
                  normalStyle={{
                    display: 'inline-block',
                    width: '45%',
                    height: 30,
                    borderWidth: 1,

                    borderRadius: 22.5,
                    fontSize: 14,
                    color: '#fff',
                    fontFamily: 'Nanum Square',
                    borderColor: 'rgba(255, 255, 255,0.2)',
                    marginTop: 10,
                  }}
                  hoverStyle={{
                    borderWidth: 3,
                    borderColor: 'rgb(11, 120, 227)',
                  }}
                  moonBlender='overlay'
                  onClick={ () => this.signIn() }>

                  Sign in
                </FullmoonBtn>

              </div>
            </div>

            <div className='right-zone'>
              <div className='inline-block vertical-middle'>


              </div>
            </div>
          </div>
        </div>

        <FootLine/>
      </div>
    );
  }
}


export default SigninScene;
