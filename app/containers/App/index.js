/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import React from 'react';
import { Helmet } from 'react-helmet';
import styled from 'styled-components';
import { Switch, Route } from 'react-router-dom';

import Index from '../Index';
import Login from '../Login';
import Signup from '../Signup';
import Dashboard from '../Dashboard';
import Studio from '../Studio';
import GlobalWrapper from "../GlobalWrapper/GlobalWrapper";



export default function App() {
  return (
    <div>
      <Helmet
        titleTemplate="%s - ICE2 - Service Builder"
        defaultTitle="ICE2 - Service Builder"
      >
        <meta name="description" content="A powerfull and fantastic Service builder of ICE2" />
      </Helmet>
      <GlobalWrapper>
      <Switch>
        <Route exact path="/" component={Index} />
        <Route path="/login" component={Login} />
        <Route path="/signup" component={Signup} />
        <Route path="/dashboard" component={Dashboard} />
        <Route path="/studio" component={Studio} />
      </Switch>
      </GlobalWrapper>
    </div>
  );
}
