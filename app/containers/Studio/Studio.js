import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import parseUrl from 'url-parse';
import uuid from 'uuid';
import {MetaNode} from 'abstract-component-node';
import { message } from 'antd';
import Axios from 'axios';
import './Studio.scss'
import DataResolver from '../../Core/Helper/dataResolver';

import Waterfall from 'async/waterfall';
import Promise from 'bluebird';

// Actions
import * as StudioActions from '../../actions/Studio';
import * as UserActions from '../../actions/User';
import * as SiteActions from '../../actions/Site';
import * as AssignedStoreActions from '../../actions/AssignedStore';
import * as NetworkActions from '../../actions/Network';

// Networks
import * as UserNetwork from '../../Core/Network/User';
import * as SiteNetwork from '../../Core/Network/Site';
import * as AssignedStoreNetwork from '../../Core/Network/AssignedStore';
import * as StudioNetwork from '../../Core/Network/Studio';

// Core Libraries
import DraggingGround from '../../Core/Container/DraggingGround';
import Supervisor from '../../Core/Container/Supervisor';
import Zone from '../../Core/Components/Zone';
import ToolProvider from '../../Core/Components/ToolProvider';
import ModalLayoutContainer from '../../Core/Container/ModalLayoutContainer';
// import UIIProvider from '../../Core/Container/UIIProvider';
import Connection from '../../Core/Connection';

import {
  CreateOpenActionSheet as CreateCanvasOpenActionSheet,
} from '../../components/Toolset/CoreTools/Canvas';

// Constants
import {
  RESERVE_LAYOUT_ELEMENT_ID_BOTTOM,
  RESERVE_LAYOUT_ELEMENT_ID_CENTRAL,
  RESERVE_LAYOUT_ELEMENT_ID_LEFT,
  RESERVE_LAYOUT_ELEMENT_ID_LEFTAREA,
  RESERVE_LAYOUT_ELEMENT_ID_RIGHT,
  RESERVE_LAYOUT_ELEMENT_ID_RIGHTAREA,
  RESERVE_LAYOUT_ELEMENT_ID_TOP,
} from '../../components/Studio/Constants';

// Studio Layout Components
import Top from '../../components/Studio/Top'
import Bottom from '../../components/Studio/Bottom'
import Sidebar from '../../components/Studio/Sidebar';
import Central from '../../components/Studio/Central';
import SideArea from '../../components/Studio/SideArea';
import LayerSystemArea from '../../components/Studio/LayerSystemArea';


import ION_CI from '../../components/artworks/IonCI';
import Loading from '../../components/artworks/Loading';

@connect((state) => (
  {
    Studio: state.get('Studio'),
    Site : state.get('Site'),
    Layout: state.get('Layout'),
    Elastic: state.get("Elastic"),
    Deployment: state.get('Deployment'),
    User : state.get('User'),
    Network : state.get('Network'),
  }
))
export default class Studio extends Component {


  static contextTypes = {
    router : PropTypes.object,
    global : PropTypes.object,

  }

  static childContextTypes = {
    io : PropTypes.object,
    ice2 : PropTypes.func,
    dataSetFromServicePage : PropTypes.object,
  }

  getChildContext(){
    let ice2 = Axios.create({
        baseURL: '//' + (this.props.Site.configs && this.props.Site.configs.main && this.props.Site.configs.main.http && this.props.Site.configs.main.http.defaultAPIHost),
        timeout: 100e2, // 10초
        withCredentials : true,
      });

    ice2.loadNodeType = this.loadNodeType.bind(ice2);
    ice2.loadPropertyType = this.loadPropertyType.bind(ice2);
    ice2.resolver = this.resolver;


    // this.resolver.resolve('{{: @this }}',{}, {'this':123});

    window.resolver = this.resolver;
    return {
      io : this.conn,
      ice2,
      dataSetFromServicePage : {
        query : this.props.Studio.pageInfo.queries,
        path : this.props.Studio.pageInfo.path,
        param : this.props.Studio.pageInfo.params,
        session : this.props.Studio.pageInfo.session,
        state : this.props.Studio.pageInfo.storeSnapshot,
      },
    }
  }

  assemblyNodeItem(nodeItem, formula){
    let $this = nodeItem;
    let result = eval(formula);
  }


  async loadNodeType(tid, crudType, group, fixedUxNode, fixUxMapProperty){


    let nodeTypeRes = await this.get(`/node/nodeType/read.json`, {
      params : {
        tid,
        includeReferenced: true,
      },
    });

    let nodeType = nodeTypeRes.data.item;

    try{
      let nodeTypeUxConfigRes = await this.get(`/node/nodeTypeUxConfig/list.json`, {
        params : {
          nodeTypeId : nodeType.tid,
          includeReference: true,
        },
      });

      let nodeTypeUxConfigs = [...nodeTypeUxConfigRes.data.items, fixedUxNode] || [fixedUxNode];

      let rule,keys, key;
      let ux = {};
      for(let i= 0; i < nodeTypeUxConfigs.length; i++ ){
        rule = nodeTypeUxConfigs[i];
        keys = Object.keys(rule || {});

        for(let j = 0; j < keys.length; j++ ){
          key = keys[j];

          if( rule[key] ){
            ux[key] = rule[key];
          }
        }
      }

      nodeType.ux = ux;
    } catch(e) {

      console.error('nodeType UX read error', e)
    }


    try {
      let propertyTypeUxConfigsRes = await this.get(`/node/propertyTypeUxConfig/list.json`, {
        params : {
          nodeTypeId : nodeType.tid,
          group : group,
          crudType,
          includeReference: true,
        },
      });

      let propertyTypeUxConfigs = propertyTypeUxConfigsRes.data.items || [];
      let uxMap = {};


      let sortOrdering = false;
      let pid;
      let objKeys;
      let rules;
      for(let i = 0; i < propertyTypeUxConfigs.length; i++ ){
        pid = propertyTypeUxConfigs[i].propertyTypeId.value;
        rules = propertyTypeUxConfigs[i];

        uxMap[pid] = uxMap[pid] || {};

        let objKeys = Object.keys(rules);
        let key;

        // rules merge
        for(let i = 0; i < objKeys.length; i++ ){
          key = objKeys[i];
          if( rules[key] ){
            uxMap[pid][key] = rules[key];

            if( key == 'sortOrder' && rules[key] ){
              sortOrdering = true;
            }
          }
        }
      }

      nodeType.propertyTypes = nodeType.propertyTypes.map((propType) => ({
        ...propType,
        ux : Object.assign({}, uxMap[propType.pid], fixUxMapProperty && fixUxMapProperty[propType.pid]),
      }));

      if( sortOrdering ){
        let aOrder,
          bOrder;
        nodeType.propertyTypes = nodeType.propertyTypes.sort((a,b) => {
          aOrder = a.ux ? a.ux.sortOrder || 9999999 : 9999999;
          bOrder = b.ux ? b.ux.sortOrder || 9999999 : 9999999;

          return aOrder - bOrder;
        })
      }

    } catch(e){

    }

    return nodeType;
  }

  loadPropertyType(tid,pid){
    return this.get(`/node/propertyType/read.json`, {
      params : {tid,pid},
    }).then((res) => res.data.item);
  }


  constructor(props) {
    super(props);

    this.state = {
      open: false,
    };

    this.conn = new Connection(this);


    this.resolver = new DataResolver();
  }


  handleTouchTap = () => {
    this.setState({
      open: true,
    });
  };

  handleRequestClose = () => {
    this.setState({
      open: false,
    });
  };

  componentWillUnmount(){
    window.onbeforeunload = null;
  }

  componentDidMount() {


    // 페이지 이동 방지
    window.onbeforeunload = function() {
      return "Are you sure you want to navigate away?";
    }

    let {dispatch} = this.props;
    let query = location.search;

    let queries = query.replace(/^\?/,'').split('&');
    let queryParamMap = {};

    for(let i = 0; i < queries.length; i++ ){
      let pair = queries[i].split('=');
      queryParamMap[pair[0]] = pair[1];
    }

    let siteID, siteOwner;
    if(queryParamMap['site']){
      siteID = queryParamMap['site'];
      siteOwner = queryParamMap['owner'];
    } else {

      /**
       * 사이트 API 재구축용, 빠른 개발용 ( 사용 안함 )
       */
      // siteID = 'ae28abcf-a637-47c5-b967-bad8287eb09a';


      // alert('Invalid Site');
      // document.location.href = '/dashboard';
      // return;
    }




    // Start Data Loading
    Waterfall([
        (_cb) => {

          UserNetwork.signIn('system', 'system')
            .then(function (_result) {
              let {status, data} = _result;

              if (status === 200 && data.result === 'success') {

                dispatch(UserActions.SIGN_IN(data.sessionID));
                dispatch(UserActions.LOAD_USER(data));


                _cb(null, data);
              } else {
                _cb({
                  point: 'signIn',
                  code: 'FAIL_USER_SIGNIN',
                });
              }
            });
        },

        function createStudioSession(userData, _cb){

          StudioNetwork.createSession(userData.sessionID, siteID, siteOwner)
            .then(function(_result){
              let {status, data} = _result;
              let studioSessionID = data.studioSessionID;

              if( status === 200 && data.result === 'success' ){

                dispatch(StudioActions.START_SESSION(studioSessionID));

                _cb(null, userData, studioSessionID);
              } else {

                _cb({
                  point: 'createStudioSession',
                  code: 'FAIL_CREATE_STUDIO_SESSION',
                });
              }
            },

            function(err){
              _cb({
                point: 'createStudioSession',
                code: 'FAIL_CREATE_STUDIO_SESSION',
              });
            });
        },


        function loadSite(userData, studioSessionID, _cb) {
          SiteNetwork.readInfo(userData.sessionID, siteID, siteOwner)
            .then(function (_result) {
              let {status, data} = _result;

              if (status === 200 && data.result === 'success') {
                dispatch(SiteActions.LOAD_SITE(data));

                _cb(null, userData, studioSessionID, data);
              } else {


                _cb({
                  point: 'loadSite',
                  code: 'FAIL_SITE_LOAD',
                });
              }
            });
        },

        function readConfigs(userData, studioSessionID, siteInfo, _cb) {
          SiteNetwork.readConfigSet(userData.sessionID, siteID, siteOwner)
            .then(function (_result) {
              let {status, data} = _result;

              if (status === 200 && data.result === 'success') {
                let {
                  level,
                  force,
                } = data;

                let currentConfig = data[level];

                currentConfig = Object.assign({}, {
                  ...currentConfig,
                  'http' : Object.assign({}, currentConfig.http, force.http),
                });


                dispatch(SiteActions.SET_CONFIGS({
                  main : currentConfig,
                  prod : data.prod,
                  stage : data.stage,
                  dev : data.dev,
                  local : data.local,
                }));

                _cb(null, userData, studioSessionID, siteInfo);
              } else {


                _cb({
                  point: 'loadConfigs',
                  code: 'FAIL_SITE_LOAD',
                });
              }
            });
        },

        // function loadAssignedStoreTree(userData, studioSessionID, _siteInfo, _cb) {
        //   AssignedStoreNetwork.getTree(userData.sessionID, siteID, siteOwner)
        //     .then(function (_result) {
        //       let {status, data} = _result;
        //
        //       if (status === 200 && data.result === 'success') {
        //         dispatch(AssignedStoreActions.LOAD_TREE(data.root));
        //
        //         _cb(null, userData, studioSessionID, _siteInfo);
        //       } else {
        //         _cb({
        //           point: 'loadAssignedStoreTree',
        //           code: 'FAIL_SITE_STORE_TREE',
        //         });
        //       }
        //     });
        // },

        // function loadRoutes(userData, studioSessionID, _siteInfo, _cb) {
        //   SiteNetwork.readRootRoute(userData.sessionID, siteID, siteOwner)
        //     .then(function (_result) {
        //       let {status, data} = _result;
        //
        //       if (status === 200 && data.result === 'success') {
        //         dispatch(SiteActions.LOAD_ROUTE(data.route));
        //
        //         _cb(null, userData, studioSessionID, _siteInfo, data.route);
        //       } else {
        //         _cb({
        //           point: 'loadRoutes',
        //           code: 'FAIL_LOAD_SITE_ROUTES',
        //         });
        //       }
        //     });
        // },
      ],
      (_err, userData, studioSessionID, siteInfo, route) => {
        if (_err) {

          alert('Invalid Site');
          // document.location.href = '/dashboard';


          return alert(JSON.stringify(_err));
        }
        console.log("LOADED DATAS");

        this.conn.connectStudio(userData, studioSessionID, siteInfo, route);
        // dispatch(StudioActions.BOOT_COMPLETE());
        // this.startEditorForUser(userData, studioSessionID, siteInfo, route)


        if( process.env.FORCE_BOOT ){
          dispatch(StudioActions.BOOT_COMPLETE());
        }

        if( process.env.DEV_FAST_CANVAS ){
          dispatch(StudioActions.BOOT_COMPLETE());
          this.startEditorForUser(userData, studioSessionID, siteInfo, route)
        }
      });
  }

  startEditorForUser(userSessionData, studioSessionID, siteInfo, route){
    let supervisor = this.supervisor.getWrappedInstance();

    let parsedURL = parseUrl(location.href, true);


    let {
      pageInfo = '{}',
      sessionID,
      site, // studio에서 처리
      owner, // studio에서 처리
      directiveName : designatedDirectiveName = '',
    } = parsedURL.query;

  // console.log(route);

    let parsedPageInfo = JSON.parse(pageInfo);

    let {
      queries,
      params,
      path : designatedPath = '',
    } = parsedPageInfo;

    this.context.global.uiBlockingReqBegin('page-load');

    this.props.dispatch(StudioActions.SET_PAGE_INFO(designatedPath, params, queries, sessionID));


    let routePath = designatedPath;
    // let routeName = route.name;
    let routeDirective = designatedPath === '/' ? 'root/index':designatedPath.replace(/^\//, '');
    // console.log(route)
    if( designatedDirectiveName ){
      supervisor.receiveActionSheet(CreateCanvasOpenActionSheet('central'), {
        name : 'Contents',
        path : designatedDirectiveName
          .replace(/^@contents@/, '/@contents@/') // contents 빌더용
          .replace(/\/?index$/,'/') // 일반 replace
          .replace(/^root\//,'/'), // 페이지 replace, 페이지에는 root 로 시작함 curation모드로 빌더에 입장 할 때
        routeDirective : designatedDirectiveName,
      }, {DISABLE_DUPLICATE_UNDEPLOY:true});
      return;
    } else {
      // supervisor.receiveActionSheet(CreateCanvasOpenActionSheet('central'), {
      //   name : 'Contents',
      //   path :  "/",
      //   routeDirective : "main",
      // }, {DISABLE_DUPLICATE_UNDEPLOY:true});

      // console.error("Can not use other way");
      // return;
    }

    // else if ( designatedRoute ){
    //
    //
    //   routeDirective = designatedPath.replace(/\/$/, '/index').replace(/^\//, '');
    //
    //   let matchedRouteNode;
    //   let routeTree= MetaNode.importFromJSON(route);
    //   routeTree.visitRecursive((node)=>{
    //     let nodeParentGraph = node.getLinealDescentList();
    //     let pathMap = nodeParentGraph.map((node) => node.path);
    //
    //     if( pathMap.join('/').replace(/^\//, '') == designatedPath ){
    //       matchedRouteNode = node;
    //       console.log(matchedRouteNode);
    //       return true;
    //     }
    //   });
    //   console.log(routePath);
    //   routePath = designatedPath;
    //
    //   if( matchedRouteNode ){
    //     routeName = matchedRouteNode.name;
    //   }
    // }

    console.log(routePath);
    supervisor.receiveActionSheet(CreateCanvasOpenActionSheet('central'), {
      name : 'dd',
      path : routePath,
      routeDirective : routeDirective.replace(/:/g,"@"),
    }, {DISABLE_DUPLICATE_UNDEPLOY:true});
  }

  renderLayerSystemArea(){
    return <LayerSystemArea
      Layout={this.props.Layout}
      Deployment={this.props.Deployment}
      Elastic={this.props.Elastic}
      Studio={this.props.Elastic}
      dispatch={this.props.dispatch}/>;
  }


  renderLeftArea() {
    if (!this.props.Studio.layoutUsingMap[RESERVE_LAYOUT_ELEMENT_ID_LEFTAREA]) return;

    return <Zone
      component={SideArea}
      acceptDraggableTypes={[]}
      zoneZDepth={0}
      zoneLayoutISA={RESERVE_LAYOUT_ELEMENT_ID_LEFTAREA}

      isa={RESERVE_LAYOUT_ELEMENT_ID_LEFTAREA}
      Layout={this.props.Layout}
      Studio={this.props.Studio}
      Elastic={this.props.Elastic}
      Deployment={this.props.Deployment}
      elementCall={
        (width) => <ToolProvider
          width={width}
          Elastic={this.props.Elastic}
          elasticID={this.props.Deployment[RESERVE_LAYOUT_ELEMENT_ID_LEFTAREA]}/>
      }/>
  }

  renderRightArea() {
    if (!this.props.Studio.layoutUsingMap[RESERVE_LAYOUT_ELEMENT_ID_RIGHTAREA]) return;

    return <Zone
      component={SideArea}
      acceptDraggableTypes={[]}
      zoneZDepth={0}
      zoneLayoutISA={RESERVE_LAYOUT_ELEMENT_ID_RIGHTAREA}

      isa={RESERVE_LAYOUT_ELEMENT_ID_RIGHTAREA}
      Layout={this.props.Layout}
      Studio={this.props.Studio}
      Elastic={this.props.Elastic}
      Deployment={this.props.Deployment}
      elementCall={
        (props) => <ToolProvider
          {...props}
          Elastic={this.props.Elastic}
          elasticID={this.props.Deployment[RESERVE_LAYOUT_ELEMENT_ID_RIGHTAREA]}/>
      }/>

  }

  renderLeft() {
    if (!this.props.Studio.layoutUsingMap[RESERVE_LAYOUT_ELEMENT_ID_LEFT]) return;
    return <Zone
      component={Sidebar}
      acceptDraggableTypes={['uii']}
      zoneZDepth={0}
      zoneLayoutISA={RESERVE_LAYOUT_ELEMENT_ID_LEFT}

      isa={RESERVE_LAYOUT_ELEMENT_ID_LEFT}
      Layout={this.props.Layout}
      Studio={this.props.Studio}
      Elastic={this.props.Elastic}
      Deployment={this.props.Deployment}
      uiiStatementIDList={this.props.Studio.leftUIIList}/>;
  }

  renderRight() {
    if (!this.props.Studio.layoutUsingMap[RESERVE_LAYOUT_ELEMENT_ID_RIGHT]) return;

    return <Zone
      component={Sidebar}
      acceptDraggableTypes={['uii']}
      zoneZDepth={0}
      zoneLayoutISA={RESERVE_LAYOUT_ELEMENT_ID_RIGHT}

      isa={RESERVE_LAYOUT_ELEMENT_ID_RIGHT}
      Layout={this.props.Layout}
      Studio={this.props.Studio}
      Elastic={this.props.Elastic}
      Deployment={this.props.Deployment}
      uiiStatementIDList={this.props.Studio.rightUIIList}/>;
  }

  renderTop() {
    if (!this.props.Studio.layoutUsingMap[RESERVE_LAYOUT_ELEMENT_ID_TOP]) return;

    return <Zone
      component={Top}
      acceptDraggableTypes={['uii']}
      zoneZDepth={0}
      zoneLayoutISA={RESERVE_LAYOUT_ELEMENT_ID_TOP}

      isa={RESERVE_LAYOUT_ELEMENT_ID_TOP}
      Layout={this.props.Layout}
      Studio={this.props.Studio}
      Elastic={this.props.Elastic}
      Deployment={this.props.Deployment}
      uiiStatementIDList={this.props.Studio.topUIIList}>

    </Zone>
  }

  renderBottom() {
    if (!this.props.Studio.layoutUsingMap[RESERVE_LAYOUT_ELEMENT_ID_BOTTOM]) return;

    return <Zone
      component={Bottom}
      acceptDraggableTypes={['uii']}
      zoneZDepth={0}
      zoneLayoutISA={RESERVE_LAYOUT_ELEMENT_ID_BOTTOM}

      isa={RESERVE_LAYOUT_ELEMENT_ID_BOTTOM}
      Layout={this.props.Layout}
      Studio={this.props.Studio}
      Elastic={this.props.Elastic}
      Deployment={this.props.Deployment}
      uiiStatementIDList={this.props.Studio.bottomUIIList}>

    </Zone>
  }

  renderCentral() {
    if (!this.props.Studio.layoutUsingMap[RESERVE_LAYOUT_ELEMENT_ID_CENTRAL]) return;

    return <Zone
      component={Central}
      acceptDraggableTypes={['component']}
      zoneZDepth={0}
      zoneLayoutISA={RESERVE_LAYOUT_ELEMENT_ID_CENTRAL}

      isa={RESERVE_LAYOUT_ELEMENT_ID_CENTRAL}
      Layout={this.props.Layout}
      Studio={this.props.Studio}
      Elastic={this.props.Elastic}
      Deployment={this.props.Deployment}
      elementCall={
        (props)=><ToolProvider {...props} Elastic={this.props.Elastic} elasticID={this.props.Deployment[RESERVE_LAYOUT_ELEMENT_ID_CENTRAL]}/>
      }/>
  }

  render() {
    if (this.props.Studio.boot.status !== 'running') {
      return <div>
        <Loading
          textInterval={4000}
          texts={texts}
          textStyle={{position:'fixed', top:'50%', marginTop:100}}
          style={{position:'absolute', left:'50%', top:'50%', marginLeft:-200, marginTop:-200}}/>
      </div>;
    }


    return (
      <div className="studio">
        <Supervisor ref={(supervisor) => {this.supervisor = supervisor}}>
          <DraggingGround>
            <ModalLayoutContainer isa="modal">
              { this.renderTop() }
              { this.renderLeft() }
              { this.renderLeftArea() }
              { this.renderRight() }
              { this.renderRightArea() }
              { this.renderCentral() }
              { this.renderBottom() }
              { this.renderLayerSystemArea() }
            </ModalLayoutContainer>
          </DraggingGround>
        </Supervisor>
      </div>
    )
  }
}



const texts = [
  'Let\'s enter the studio.',
  'Loading...',
  'Please wait for use..',
  'Little bit more...',
  'I think he\'s rebuilding...',
  'Need a few time this work...',

  "Think like a man of action and act like man of thought.",
  // (행동하는 사람처럼 생각하고, 생각하는 사람처럼 행동하라.)
"Courage is very important. Like a muscle, it is strengthened by use.",
// (용기는 대단히 중요하다. 근육과 같이 사용함으로써 강해진다.)
"Life is the art of drawing sufficient conclusions from insufficient premises.",
// (인생이란 불충분한 전제로부터 충분한 결론을 끌어내는 기술이다)
"By doubting we come at the truth.",
// (의심함으로써 우리는 진리에 도달한다)
"A man that hath no virtue in himself, ever envieth virtue in others.",
// (자기에게 덕이 없는 자는 타인의 덕을 시기한다.)
"When money speaks, the truth keeps silent.",
// (돈이 말할 때는 진실은 입을 다문다.)


"Better the last smile than the first laughter.",
// (처음의 큰 웃음보다 마지막의 미소가 더 좋다.)
"In the morning of life, work; in the midday, give counsel; in the evening, pray.",
// (인생의 아침에는 일을 하고, 낮에는 충고하며, 저녁에는 기도하라.)
"Painless poverty is better than embittered wealth.",
// (고통 없는 빈곤이 괴로운 부보다 낫다.)
"A poet is the painter of the soul.",
// (시인은 영혼의 화가이다.)
"Error is the discipline through which we advance.",
// (잘못은 그것을 통하여 우리가 발전할 수 있는 훈련이다.)
"Faith without deeds is useless.",
  // 행함이 없는 믿음은 쓸모가 없다.
  "Weak things united become strong.",
  // 약한 것도 합치면 강해진다.


  "We give advice, but we cannot give conduct.",
  // 충고는 해 줄 수 있으나, 행동하게 할 수는 없다.
  "Nature never deceives us; it is always we who deceive ourselves.",
  // 자연은 인간을 결코 속이지 않는다. 우리를 속이는 것은 항상 우리 자신이다.
  "Forgiveness is better than revenge.",
  // 용서는 복수보다 낫다.
  "We never know the worth of water till the well is dry.",
  // 우물이 마르기까지는 물의 가치를 모른다.
  "Pain past is pleasure.",
  // 지나간 고통은 쾌락이다




"Books are ships which pass through the vast seas of time.",
  // 책이란 넓고 넓은 시간의 바다를 지나가는 배다.
  "Who begins too much accomplishes little.",
  // 너무 많이 시작하는 사람은 성취하는 것이 별로 없다.,
  "Better the last smile than the first laughter.",
  // 처음의 큰 웃음보다 마지막 미소가 더 좋다.
  "Faith is a higher faculty than reason.",
  // 믿음은 이성보다 더 고상한 능력이다.
  "Until the day of his death, no man can be sure of his courage.",
  // 죽는 날까지는, 자기의 용기를 확신할 수 있는 사람은 아무도 없다.
  "Great art is an instant arrested in eternity.",
  // 위대한 예술은 영원 속에서 잡은 한 순간이다.


  "Faith without deeds is useless.",
  // 행함이 없는 믿음은 쓸모가 없다.
  "The world is a beautiful book, but of little use to him who cannot read it.",
  // 세상은 한 권의 아름다운 책이다. 그러나 그 책을 읽을 수 없는 사람에게는 별 소용이 없다.
  "Heaven gives its favourites-early death.",
  // 하늘은, 그가 사랑하는 자에게 이른 죽음을 준다.
  "I never think of the future. It comes soon enough.",
  // 나는 미래에 대해서는 결코 생각하지 않는다. 미래는 곧 오고 말것이므로.
  "Suspicion follows close on mistrust.",
  // 의혹은 불신을 뒤따른다.


  "He who spares the rod hates his son, but he who loves him is careful to discipline him.",
  // 매를 아끼는 것은 자식을 사랑하지 않는 것이다. 자식을 사랑하는 사람은 훈계를 게을리하지 않는다.
  "All good things which exist are the fruits of originality.",
  // 현존하는 모든 훌륭한 것들은 독창력의 결실이다.
  "The will of a man is his happiness.",
  // 인간의 마음가짐이 곧 행복이다.
  "He that has no shame has no conscience.",
  // 수치심이 없는 사람은 양심이 없다.
  "Weak things united become strong.",
  // 약한 것도 합치면 강해진다.


  "A minute’s success pays the failure of years.",
  // 단 1분의 성공은 몇 년 동안의 실패를 보상한다
"United we stand, divided we fall.",
  // 뭉치면 서고, 흩어지면 쓰러진다.
  "To doubt is safer than to be secure.",
  // 의심하는 것이 확인하는 것보다 더 안전하다.
  "Time is but the stream I go a-fishing in.",
// 시간은 내가 그 속에서 낚시질을 하는 흐름이다.




  "A full belly is the mother of all evil.",
  // 배부른 것이 모든 악의 어머니이다.
  "Love your neighbor as yourself.",
  // 네 이웃을 네 몸처럼 사랑하여라
"It is a wise father that knows his own child.",
  // 자기 자식을 아는 아버지는 현명한 아버지이다.
  "By doubting we come at the truth.",
  // 의심함으로써 우리는 진리에 도달한다
"Absence makes the heart grow fonder.",
  // 떨어져 있으면 정이 더 깊어진다
"Habit is second nature.",
  // 습관은 제2의 천성이다.


  "Who knows much believes the less.",
  // 많이 아는 사람일수록 적게 믿는다
"Only the just man enjoys peace of mind.",
  // 정의로운 사람만이 마음의 평화를 누린다.
  "Waste not fresh tears over old griefs.",
  // 지나간 슬픔에 새 눈물을 낭비하지 말라.
  "Life itself is a quotation.",
  // 인생 그 자체가 하나의 인용이다.
  "He is greatest who is most often in men’s good thoughts.",
  // 사람들의 좋은 회상 속에 자주 있는 자가 가장 위대하다.


  "Envy and wrath shorten the life.",
  // 시기와 분노는 수명을 단축시킨다.
  "Where there is no desire, there will be no industry.",
  // 욕망이 없는 곳에는 근면도 없다.
  "To be trusted is a greater compliment than to be loved.",
  // 신뢰 받는 것은 사랑받는 것 보다 더 큰 영광이다.
  "Education is the best provision for old age.",
  // 교육은 노년기를 위한 가장 훌륭한 대책이다.
  "To jaw-jaw is better than to war-war.",
  // 전쟁보다 협상이 낫다.


  "Music is a beautiful opiate, if you don’t take it too seriously.",
  // 음악은 너무 심하게 취하지만 않는다면 일종의 아름다운 마취제이다.
  "Appearances are deceptive.",
  // 외모는 속임수이다.
  "Let thy speech be short, comprehending much in few words.",
  // 몇 마디 말에 많은 뜻을 담고, 말은 간단히 하라
"Things are always at their best in the beginning.",
  // 사물은 항상 시작이 가장 좋다.
  "A gift in season is a double favor to the needy.",
  // 필요할 때 주는 것은 필요한 자에게 두배의 은혜가 된다.
  "In giving advice, seek to help, not to please, your friend.",
  // 친구에게 충고할 때는 즐겁게 하지 말고, 도움이 되도록 하라.
  "The difficulty in life is the choice.",
  // 인생에 있어서 어려운 것은 선택이다.


  "The most beautiful thing in the world is, of course, the world itself.",
  // 세상에서 가장 아름다운 것은 물론 세상 그 자체이다.
  "All fortune is to be conquered by bearing it.",
  // 모든 운명은 그것을 인내함으로써 극복해야 한다.
  "Better is to bow than break.",
// 부러지는 것보다 굽는 것이 낫다.
  "Good fences makes good neighbors.",
  // 좋은 울타리는 선한 이웃을 만든다.
  "Give me liberty, or give me death.",
  // 자유가 아니면 죽음을 달라
]
