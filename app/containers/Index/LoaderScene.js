import React from 'react';
import PropTypes from 'prop-types';
import HeadLine from '../../components/NonStudioLayout/headline';
import FootLine from '../../components/NonStudioLayout/footline';
import PolarbearContent from '../../components/NonStudioLayout/content/polarbear';

import MilkywaySvg from '../../components/artworks/withsvg/milkyway_svg';

import '../NonStudioCommon/layout.scss';
import './LoaderScene.scss';


const TitleLoadingScene = (props, context) => (
  <div className='index'>
    <HeadLine/>

    <div className='contentline' style={ {zIndex: -1} }>
      <div className='container'>
        <PolarbearContent />
      </div>

      <div className='background'>
        <div className='milkyway-svg'>
          <MilkywaySvg />
        </div>
      </div>


    </div>

    <button className="enter" onClick={()=>context.router.history.push('/login')} >Enter</button>

    <FootLine statusBar={false}/>
  </div>
);

TitleLoadingScene.contextTypes = {
  router : PropTypes.object,
}


export default TitleLoadingScene;
