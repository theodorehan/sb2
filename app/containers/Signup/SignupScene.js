// @flow
// import {ipcRenderer} from 'electron';

import React, {Component} from 'react';
import PropTypes from 'prop-types';
import axios from 'axios'

import {Field, reduxForm} from 'redux-form/immutable'

import TextField from 'material-ui/TextField';
// <-- Material UI

import HeadLine from '../../components/NonStudioLayout/headline.js';
import FootLine from '../../components/NonStudioLayout/footline.js';
import FullmoonBtn from '../../components/Unit/FullmoonButton/FullmoonButton';

import '../NonStudioCommon/layout.scss';
import '../NonStudioCommon/elements.scss';

const validate = (values) => {
  const errors = {};
  if (!values.get('id')) {
    errors.id = 'Required'
  }

  if (!values.get('password')) {
    errors.password = 'Required'
  } else if (values.get('password').length < 8) {
    errors.password = 'Must be 8 characters or more'
  }

  if (!values.get('confirmPassword')) {
    errors.confirmPassword = 'Required'
  } else if (values.get('confirmPassword').length < 8) {
    errors.confirmPassword = 'Must be 8 characters or more'
  }
  else if (values.get('password') != values.get('confirmPassword')) {
    errors.confirmPassword = 'Not equals password'
  }

  if (!values.get('email')) {
    errors.email = 'Required'
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.get('email'))) {
    errors.email = 'Invalid email address'
  }

  if (!values.get('screenName')) {
    errors.screenName = 'Required'
  }

  return errors
};

const renderTextField = ({input, hintText, floatingLabelText, meta: {touched, error}, ...custom}) => (
  <TextField
    style={{display: 'block'}}
    hintText={hintText}
    floatingLabelText={floatingLabelText}
    errorText={touched && error}
    {...input}
    {...custom}
  />
);

class SignUpScene extends Component {
  static propTypes = {
    handleSubmit: PropTypes.func,
    pristine: PropTypes.bool,
    reset: PropTypes.func,
    submitting: PropTypes.bool,
  };

  static contextTypes = {
    router: PropTypes.object,
  }

  submitFunc = (values) => {
    console.log('JSONStringify', JSON.stringify(values));
    axios.post('/api/user/signup', values.toObject())
      .then(function (response) {
        console.log('status: ',response.status);
      })
      .catch(function (error) {
        console.log('axios error', error);
      });
  };

  render() {
    const {handleSubmit, pristine, reset, submitting} = this.props;
    return (
      <div className='index'>
        <HeadLine title="signup"/>
        <div className='contentline'>
          <div className='container'>
            <div className='left-zone'>
              <div className='inline-block vertical-middle'>
                <FullmoonBtn
                  normalStyle={{
                    width: 100,
                    height: 100,
                    borderWidth: 0,
                    backgroundImage: 'url(/public/svg/chevron-left.svg)',

                    borderRadius: '50%',
                    fontSize: 25,
                    color: '#fff',
                    fontFamily: 'Nanum Square',
                    display: 'block',
                    borderColor: 'rgb(11, 120, 227)',
                    marginTop: 10,
                  }}
                  hoverStyle={{
                    borderWidth: 5,
                  }}
                  moonBlender='overlay'
                  useImageDesc={true}
                  onClick={ () => this.context.router.history.push('/login') }/>
              </div>
            </div>

            <div className='center-zone'>

              <div className='inline-block vertical-middle'>
                <form onSubmit={handleSubmit(this.submitFunc)}>
                  <Field
                    component={renderTextField}
                    name="id"
                    hintText="polaris01"
                    floatingLabelText="Input your ID"/>

                  <Field
                    component={renderTextField}
                    name="password"
                    type='password'
                    hintText="PassW0rd **"
                    floatingLabelText="Input your passW0rd!"/>

                  <Field
                    component={renderTextField}
                    name="confirmPassword"
                    type='password'
                    hintText="PassW0rd **"
                    floatingLabelText="Input your confirm passW0rd!"/>

                  <Field
                    component={renderTextField}
                    name="email"
                    hintText="who@example.com"
                    floatingLabelText="Input your email"/>

                  <Field
                    component={renderTextField}
                    name="screenName"
                    hintText="skyend"
                    floatingLabelText="Input your screen name."/>

                  <FullmoonBtn
                    type="submit"
                    disabled={submitting}
                    normalStyle={{
                      display: 'inline-block',
                      width: '90%',
                      height: 30,
                      borderWidth: 1,

                      borderRadius: 22.5,
                      fontSize: 14,
                      color: '#fff',
                      fontFamily: 'Nanum Square',
                      borderColor: 'rgba(255, 255, 255,0.2)',
                      marginTop: 10,
                    }}
                    hoverStyle={{
                      borderWidth: 3,
                      borderColor: 'rgb(11, 120, 227)',
                    }}
                    moonBlender='overlay'>

                    Sign up
                  </FullmoonBtn>

                </form>
              </div>
            </div>

            <div className='right-zone'>
              <div className='inline-block vertical-middle'>

              </div>
            </div>
          </div>
        </div>

        <FootLine/>
      </div>
    )
  }
}

export default reduxForm({
  form: 'SignUpScene',
  validate,
})(SignUpScene)
