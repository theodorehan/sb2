// These are the pages you can go to.
// They are all wrapped in the App component, which should contain the navbar etc
// See http://blog.mxstbr.com/2016/01/react-apps-with-pages for more information
// about the code splitting business
// import { getAsyncInjectors } from './utils/asyncInjectors';

const errorLoading = (err) => {
  console.error('Dynamic page loading failed', err); // eslint-disable-line no-console
};

const loadModule = (cb) => (componentModule) => {
  cb(null, componentModule.default);
};

export default function createRoutes(/* store */) {
  // create reusable async injectors using getAsyncInjectors factory
  // const { injectReducer, injectSagas } = getAsyncInjectors(store);

  return [
    {
      path: '/',
      getComponent(nextState, cb) {
        import('./containers/Index')
          .then(loadModule(cb))
          .catch(errorLoading);
      },
    },

    {
      path: '/login',
      getComponent(nextState, cb) {
        import('./containers/Login')
          .then(loadModule(cb))
          .catch(errorLoading);
      },
    },

    {
      path: '/signup',
      getComponent(nextState, cb) {
        import('./containers/Signup')
          .then(loadModule(cb))
          .catch(errorLoading);
      },
    },

    {
      path: '/dashboard',
      getComponent(nextState, cb) {
        import('./containers/Dashboard')
          .then(loadModule(cb))
          .catch(errorLoading);
      },
    },

    {
      path: '/studio',
      getComponent(nextState, cb) {
        import('./containers/Studio')
          .then(loadModule(cb))
          .catch(errorLoading);
      },
    },

  ];
}
