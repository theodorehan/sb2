import {
  combineReducers,
} from 'redux';

import * as ActionTypes from '../../actions/Layout/types';

import {
  RESERVE_LAYOUT_ELEMENT_ID_BOTTOM,
  RESERVE_LAYOUT_ELEMENT_ID_CENTRAL,
  RESERVE_LAYOUT_ELEMENT_ID_LEFT,
  RESERVE_LAYOUT_ELEMENT_ID_LEFTAREA,
  RESERVE_LAYOUT_ELEMENT_ID_RIGHT,
  RESERVE_LAYOUT_ELEMENT_ID_RIGHTAREA,
  RESERVE_LAYOUT_ELEMENT_ID_TOP,
} from '../../components/Studio/Constants'

import {
  defaultHeight as TopDefaultHeight,
} from '../../components/Studio/Top';

import {
  defaultHeight as BottomDefaultHeight,
} from '../../components/Studio/Bottom';

import {
  defaultWidth as sidebarDefaultWidth,
  POSITION_LEFT,
  POSITION_RIGHT,
  Menubar,
} from '../../components/Studio/Sidebar';



import {
  defaultWidth as sideAreaDefaultWidth,
} from '../../components/Studio/SideArea';



export const initialState = {
  [RESERVE_LAYOUT_ELEMENT_ID_LEFT] : {
    position : POSITION_LEFT,
    menuType : Menubar.MenubarTypeSlim,

    chainingLayoutID : RESERVE_LAYOUT_ELEMENT_ID_LEFTAREA,
  },

  [RESERVE_LAYOUT_ELEMENT_ID_RIGHT] : {
    position : POSITION_RIGHT,
    menuType : Menubar.MenubarTypeLine,

    chainingLayoutID : RESERVE_LAYOUT_ELEMENT_ID_RIGHTAREA,
  },

  [RESERVE_LAYOUT_ELEMENT_ID_LEFTAREA] : {
    followUp : RESERVE_LAYOUT_ELEMENT_ID_LEFT,
    width : sideAreaDefaultWidth,
    float : true,
  },

  [RESERVE_LAYOUT_ELEMENT_ID_RIGHTAREA] : {
    followUp : RESERVE_LAYOUT_ELEMENT_ID_RIGHT,
    width : sideAreaDefaultWidth,
    float : false,
  },

  [RESERVE_LAYOUT_ELEMENT_ID_TOP] : {
    height : TopDefaultHeight,
  },

  [RESERVE_LAYOUT_ELEMENT_ID_BOTTOM] : {
    height : BottomDefaultHeight,
  },

  [RESERVE_LAYOUT_ELEMENT_ID_CENTRAL] : {

  },

  // Elastic Layout Parts
  elastic : {

  },

}


const LAYER_ZINDEX_START = 99;


function layoutReducer(state = initialState, action){
  switch( action.type ){
    case ActionTypes.LEFTAREA_WIDTH_MODIFY :
      return Object.assign({}, state, {
        [RESERVE_LAYOUT_ELEMENT_ID_LEFTAREA] : Object.assign({}, state[RESERVE_LAYOUT_ELEMENT_ID_LEFTAREA], {
          width : action.width,
        }),
      });
    case ActionTypes.RIGHTAREA_WIDTH_MODIFY :
      return Object.assign({}, state, {
        [RESERVE_LAYOUT_ELEMENT_ID_RIGHTAREA] : Object.assign({}, state[RESERVE_LAYOUT_ELEMENT_ID_RIGHTAREA], {
          width : action.width,
        }),
      });

    case ActionTypes.CREATE_CUSTOM_LAYOUT :

      return (() => {
        let layers = Object.keys(state).filter((id) => id.indexOf('layer_') == 0 );
        let zIndexMax = LAYER_ZINDEX_START;
        for(let i = 0; i < layers.length; i++ ){
          zIndexMax = Math.max(zIndexMax, state[layers[i]].zIndex );
        }

        return Object.assign({}, state, {
          [action.layoutID]: Object.assign({}, state[action.layoutID], {
            x: action.x,
            y: action.y,
            width: action.w,
            height: action.h,
            zIndex : zIndexMax + 1,
          }),
        });
      })()

    case ActionTypes.REMOVE_CUSTOM_LAYOUT :
      return (()=>{
        let newStates = Object.assign({}, state);
        delete newStates[action.layoutID];

        return newStates;
      })();
    case ActionTypes.MODIFY_CUSTOM_LAYOUT :
      if( !state[action.layoutID] ){
        throw new Error(`${action.layoutID} Layout 이 없습니다.`);
      }

      return Object.assign({}, state, {
        [action.layoutID] : Object.assign({}, state[action.layoutID], {
          x : typeof action.x === 'number' ? action.x : state[action.layoutID].x,
          y : typeof action.y === 'number' ? action.y : state[action.layoutID].y,
          width : typeof action.w === 'number' ? action.w : state[action.layoutID].width,
          height : typeof action.h === 'number' ? action.h : state[action.layoutID].height,
          zIndex : typeof action.zIndex === 'number' ? action.zIndex : state[action.layoutID].zIndex,
        }),
      });
    case ActionTypes.FLASH_HIDE_LAYOUT :
      return Object.assign({}, state, {
        [action.layoutID] : Object.assign({}, state[action.layoutID], {
          flashHide : true,
        }),
      });
    case ActionTypes.FLASH_SHOW_LAYOUT :
      return Object.assign({}, state, {
        [action.layoutID] : Object.assign({}, state[action.layoutID], {
          flashHide : false,
        }),
      });

    case ActionTypes.LAYOUT_PIN :
      return Object.assign({}, state, {
        [action.layoutID]  : Object.assign({}, state[action.layoutID], {
          float : false,
        }),
      });
    case ActionTypes.LAYOUT_PIN_RELEASE :
      return Object.assign({}, state, {
        [action.layoutID]  : Object.assign({}, state[action.layoutID], {
          float : true,
        }),
      });

    case ActionTypes.LAYOUT_FULLSCREEN :
      return Object.assign({}, state, {
        [action.layoutID]  : Object.assign({}, state[action.layoutID], {
          fullscreen : true,
        }),
      });

    case ActionTypes.LAYOUT_FULLSCREEN_EXIT :
      return Object.assign({}, state, {
        [action.layoutID]  : Object.assign({}, state[action.layoutID], {
          fullscreen : false,
        }),
      });
    default :
      return state;
  }
}

export default layoutReducer;
