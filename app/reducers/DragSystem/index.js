import {
  combineReducers,
} from 'redux';

import {
  MOUSE_DOWN,
  MOUSE_DOWN_RELEASE,
  START_DRAG,
  DRAG,
  NATIVE_DRAG_FINISH,
  NATIVE_DRAG_START,
} from '../../actions/DraggingSystem/types';

const initialState = {
  dragState: {
    dragging: false,
    mouseDown: false,
    detachedGhostImage : null,

    draggableID : null,
    aimX: 0,
    aimY: 0,
    hoveringZoneID : null,

    clickPointOffsetX: 0,
    clickPointOffsetY: 0,
  },


  nativeDragState : {
    dragging: false,
    contextType : '',
    data : {},
  },
}


function dragStateReducer(state = initialState.dragState, action) {
  switch (action.type) {
    case MOUSE_DOWN :
      return Object.assign({}, state, {
        mouseDown : true,

        clickPointOffsetX: action.offsetX,
        clickPointOffsetY: action.offsetY,
      });

    case MOUSE_DOWN_RELEASE:
      return Object.assign({}, state, {
        mouseDown : false,
        dragging : false,
        draggableID : null,
        hoveringZoneID : null,
        draggableType : null,
        draggableData : null,
      });

    case START_DRAG :
      return Object.assign({}, state, {
        dragging : true,
        draggableID : action.draggableID,
        aimX : action.x,
        aimY : action.y,
        draggableType : action.draggableType,
        draggableData : action.draggableData,
        detachedGhostImage : action.detachedGhostImage || null,
      });

    case DRAG :
      return Object.assign({}, state, {
        aimX : action.x,
        aimY : action.y,
        hoveringZoneID : action.hoveringZoneID,
      });
    default:
      return state;
  }
}


function nativeDragStateReducer(state = initialState.nativeDragState, action) {
  switch (action.type) {

    case NATIVE_DRAG_START :
      return Object.assign({}, state.nativeDragState, {
        dragging : true,
        contextType : action.contextType,
        data : action.data,
      });
    case NATIVE_DRAG_FINISH :
      return Object.assign({}, state.nativeDragState, {
        dragging : false,
      });
    default:
      return state;
  }
}

export default combineReducers({
  dragState: dragStateReducer,
  nativeDragState : nativeDragStateReducer,
});
