import * as ActionTypes from '../../actions/Deployment/types';

export const initialState = {
  left : null,

  right : null,

  top: null,

  bottom :null,

  central : null,

  '[elastic...]' : null,
}


export default function( state = initialState, action ){
  switch(action.type){
    case ActionTypes.DEPLOY :
      return Object.assign({}, state, {
        [action.layoutID] : action.elasticID,
      })

    case ActionTypes.UNDEPLOY :
      return Object.assign({}, state, {
        [action.layoutID] : null,
      });
    default :
      return state;
  }
}
