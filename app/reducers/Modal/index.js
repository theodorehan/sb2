

import * as actionTypes from '../../actions/Modal/types';

const initialState = {
  open : false,
}


function modalReducer(state = initialState, action) {
  switch (action.type) {

    case actionTypes.OPEN :
      return Object.assign({}, state, {
        open : true,
      });

    case actionTypes.CLOSE :
      return Object.assign({}, state, {
        open : false,
      });

    default:
      return state;
  }
}

export default modalReducer;
