import {
  combineReducers,
} from 'redux';

import * as ActionTypes from '../../actions/Dashboard/types';



export const initialState = {
  menuOpened : false,
  templates : [],
  sites : [],


  creatingSite : false,
};



function dashboardReducer(state = initialState, action){
  switch( action.type ){
    case ActionTypes.OPEN_MENU:
      return Object.assign({}, state, {
        menuOpened : true,
      });

    case ActionTypes.CLOSE_MENU:
      return Object.assign({}, state, {
        menuOpened : false,
      });

    case ActionTypes.LOADED_TEMPLATES:
      return Object.assign({}, state, {
        templates : action.list,
      });

    case ActionTypes.LOADED_SITES:
      return Object.assign({}, state, {
        sites : action.list,
      });

    case ActionTypes.CREATING_SITE :
      return Object.assign({}, state, {
        creatingSite : true,
      });

    default:
      return state;
  }
}

export default dashboardReducer;
