import * as actionTypes from '../../actions/Network/types';

const initialState = {
  ui_blocking_requests : [],
}


function networkReducer(state = initialState, action) {
  switch (action.type) {

    case actionTypes.UI_BLOCKING_REQUEST_BEGIN :

      return Object.assign({}, state, {
        ui_blocking_requests : [
          ...state.ui_blocking_requests,
          action.reqID,
        ],
      });
    case actionTypes.UI_BLOCKING_REQUEST_FINISH :
      return Object.assign({}, state ,{
        ui_blocking_requests : state.ui_blocking_requests.filter((id) => id !== action.reqID ),
      });
    default:
      return state;
  }
}

export default networkReducer;
