import * as ActionTypes from '../../actions/Elastic/types';
import {
  ToolAccessMap,
} from '../../components/Toolset';

export const initialState = {
  '[ELASTIC ID]' : {
    toolKey : 'Unknown',
    state : {

    },

    // 정적 데이터 : Elastic 을 할당 할 때 입력됨
    // Elastic 이 직접 변경 불가
    staticData : {

    },
  },
}

export default function (state = initialState, action) {

  if( action.__elasticAction ){
    let elasticID = action.__elasticID;
    let elastic = state[elasticID];
    let toolKey = elastic.toolKey;
    let reducer = ToolAccessMap[toolKey].reducer;
    let unwrappedAction = Object.assign({}, action);

    unwrappedAction.type = action.__originType;

    return Object.assign({}, state, {
      [elasticID] : Object.assign({}, elastic, {
        state : reducer(elastic.state, unwrappedAction),
      }),
    });
  }

  switch (action.type) {
    case ActionTypes.ASSIGN_ELASTIC :
      let toolKey = action.toolKey;
      let toolInitialState = ToolAccessMap[toolKey].initialState;
      let defaultStaticData = ToolAccessMap[toolKey].defaultStaticData || {};

      if( !toolInitialState ){
        console.warn(`Elastic : initialState of ${toolKey} is ${JSON.stringify(toolInitialState)}.`);
      }


      return Object.assign({}, state, {
        [action.id] : {
          toolKey : action.toolKey,
          state : Object.assign({}, toolInitialState),
          staticData : Object.assign({}, defaultStaticData, action.staticData),
        },
      })

    case ActionTypes.RELEASE_ELASTIC:
      let newState = Object.assign({}, state);
      delete newState[action.id];

      return Object.assign({}, newState);

    case ActionTypes.SET_STATIC_DATA:
      return Object.assign({}, state, {
        [action.elasticID] : Object.assign({}, state[action.elasticID], {
          staticData : action.merge ? Object.assign({}, state[action.elasticID].staticData, action.staticData) : action.staticData,
        }),
      });
    default :
      return state;
  }
}
