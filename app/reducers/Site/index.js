import {
  combineReducers,
} from 'redux';

import * as ActionTypes from '../../actions/Site/types';

const initialState = {
  info : {
  },

  configs : {
    main : {

    },

    levels : {

    },
  },

  route : {},
}


function infoReducer (state = initialState.info, action){
  switch (action.type) {
    case ActionTypes.LOAD_SITE :

      return Object.assign({}, action.info);
    default:
      return state;
  }
}

function routesReducer (state = initialState.route, action){
  switch (action.type) {
    case ActionTypes.LOAD_ROUTE :

      return Object.assign({}, action.route);
    default:
      return state;
  }
}


function configReducer (state = initialState.configs, action){
  switch (action.type) {
    case ActionTypes.SET_CONFIGS :

      return Object.assign({}, { main : action.levels.main , levels : action.levels});
    default:
      return state;
  }
}

export default combineReducers({
  info : infoReducer,
  route : routesReducer,
  configs : configReducer,
});
