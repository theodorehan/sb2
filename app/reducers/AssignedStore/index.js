import {
  combineReducers,
} from 'redux';

import * as ActionTypes from '../../actions/AssignedStore/types';

const initialState = {
  mainStoreRoot : {
  },

  mainFiles : {
  },
}


function mainStoreRootReducer (state = initialState.mainStoreRoot, action){
  switch (action.type) {
    case ActionTypes.LOAD_TREE :

      return Object.assign({}, action.root);
    default:
      return state;
  }
}

function mainFilesReducer( state = initialState.mainFiles, action ){
  switch ( action.type ){
    default:
      return state;
  }
}


export default combineReducers({
  mainStoreRoot : mainStoreRootReducer,
  mainFiles : mainFilesReducer,
});
