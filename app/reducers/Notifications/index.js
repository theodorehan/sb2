import * as ActionTypes from '../../actions/Notifications/types';
import { combineReducers } from 'redux';



export const initialState = {
  messages: [],
}


export default combineReducers({
  messages : function ( state = initialState.messages, action ){
    switch(action.type){
      case ActionTypes.NEW_MESSAGE :
        let newMsgStack = Object.assign([], state);

        newMsgStack.push({
          desc : action.message,
          date : action.date,
          level : action.level,
        });

        return newMsgStack;
      default :
        return state;
    }
  },
});
