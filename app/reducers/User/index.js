import {
  combineReducers,
} from 'redux';

import * as ActionTypes from '../../actions/User/types';

const initialState = {
  session : {
    id : null,
  },

  info : {
    screenName : null,
    avatarImg : null,
  },
}


function infoReducer (state = initialState.info, action){
  switch (action.type) {
    case ActionTypes.LOAD_USER :

      return Object.assign({}, action.data);
    default:
      return state;
  }
}

function sessionReducer ( state = initialState.session, action ){
  switch ( action.type ){
    case ActionTypes.SIGN_IN:
      return Object.assign({}, state, {
        id : action.sessionID,
      });
    default:
      return state;
  }
}

export default combineReducers({
  info : infoReducer,

  session : sessionReducer,
});
