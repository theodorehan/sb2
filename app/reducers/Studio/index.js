import { combineReducers } from 'redux';
import parseUrl from 'url-parse';
import * as ActionTypes from '../../actions/Studio/types';
import { fromJS } from 'immutable';
import mapValues from 'lodash/mapValues';

import {
  CoreTools,
} from '../../components/Toolset';

import {
  RESERVE_LAYOUT_ELEMENT_ID_BOTTOM,
  RESERVE_LAYOUT_ELEMENT_ID_CENTRAL,
  RESERVE_LAYOUT_ELEMENT_ID_LEFT,
  RESERVE_LAYOUT_ELEMENT_ID_LEFTAREA,
  RESERVE_LAYOUT_ELEMENT_ID_RIGHT,
  RESERVE_LAYOUT_ELEMENT_ID_RIGHTAREA,
  RESERVE_LAYOUT_ELEMENT_ID_TOP,
} from '../../components/Studio/Constants'

import * as UIIStatements from '../../components/UIIStatements';



export const initialState = initializeStateFilter({
  uiiCollection: {
    [CoreTools.ComponentPalette.UIIStatement.identification]: CoreTools.ComponentPalette.UIIStatement,
    [CoreTools.Pages.UIIStatement.identification]: CoreTools.Pages.UIIStatement,
    [CoreTools.ProjectExplorer.UIIStatement.identification]: CoreTools.ProjectExplorer.UIIStatement,
    [CoreTools.Inspector.UIIStatement.identification]: CoreTools.Inspector.UIIStatement,
    [CoreTools.Database.UIIStatement.identification]: CoreTools.Database.UIIStatement,
    [CoreTools.DataInspector.UIIStatement.identification]: CoreTools.DataInspector.UIIStatement,
    [CoreTools.ProjectSettings.UIIStatement.identification]: CoreTools.ProjectSettings.UIIStatement,
    [CoreTools.API.UIIStatement.identification] : CoreTools.API.UIIStatement,
    [CoreTools.Resources.UIIStatement.identification] : CoreTools.Resources.UIIStatement,
    [CoreTools.Elements.UIIStatement.identification] : CoreTools.Elements.UIIStatement,
    [CoreTools.NPM.UIIStatement.identification] : CoreTools.NPM.UIIStatement,
    [CoreTools.WebpackManager.UIIStatement.identification] : CoreTools.WebpackManager.UIIStatement,
    [CoreTools.Notifications.UIIStatement.identification] : CoreTools.Notifications.UIIStatement,
    [CoreTools.GridEditor.UIIStatement.identification] : CoreTools.GridEditor.UIIStatement,
    [CoreTools.NodeTreeInspector.UIIStatement.identification] : CoreTools.NodeTreeInspector.UIIStatement,
    [CoreTools.Git.UIIStatement.identification] : CoreTools.Git.UIIStatement,
    [CoreTools.Settings.UIIStatement.identification] : CoreTools.Settings.UIIStatement,
    [CoreTools.PageOptions.UIIStatement.identification] : CoreTools.PageOptions.UIIStatement,
    [CoreTools.Dialog.UIIStatement.identification] : CoreTools.Dialog.UIIStatement,
    [CoreTools.ResourceManager.UIIStatement.identification] : CoreTools.ResourceManager.UIIStatement,
    [CoreTools.Publisher.UIIStatement.identification] : CoreTools.Publisher.UIIStatement,
    [CoreTools.Curator.UIIStatement.identification] : CoreTools.Curator.UIIStatement,
    [CoreTools.ICE2Data.UIIStatement.identification] : CoreTools.ICE2Data.UIIStatement,
    [CoreTools.YoutubeExplorer.UIIStatement.identification] : CoreTools.YoutubeExplorer.UIIStatement,
    [CoreTools.FragmentTemplateManager.UIIStatement.identification] : CoreTools.FragmentTemplateManager.UIIStatement,

    // UII Statements
    [UIIStatements.User.UserDisplay.identification] : UIIStatements.User.UserDisplay,
    [UIIStatements.Studio.OnAirStatus.identification] : UIIStatements.Studio.OnAirStatus,
    [UIIStatements.Studio.DeviceModeSwitch.identification] : UIIStatements.Studio.DeviceModeSwitch,

    [UIIStatements.Studio.DeviceModeSmaller.identification] : UIIStatements.Studio.DeviceModeSmaller,
    [UIIStatements.Studio.DeviceModeMobile.identification] : UIIStatements.Studio.DeviceModeMobile,
    [UIIStatements.Studio.DeviceModeTablet.identification] : UIIStatements.Studio.DeviceModeTablet,
    [UIIStatements.Studio.DeviceModeDesktop.identification] : UIIStatements.Studio.DeviceModeDesktop,
    [UIIStatements.Studio.DeviceModeFit.identification] : UIIStatements.Studio.DeviceModeFit,
    [UIIStatements.Studio.Fullscreen.identification] : UIIStatements.Studio.Fullscreen,
    [UIIStatements.Studio.ExitButton.identification] : UIIStatements.Studio.ExitButton,
  },

  layoutUsingMap: {
    [RESERVE_LAYOUT_ELEMENT_ID_LEFT]: true,
    [RESERVE_LAYOUT_ELEMENT_ID_RIGHT]: true,

    [RESERVE_LAYOUT_ELEMENT_ID_TOP]: true,

    [RESERVE_LAYOUT_ELEMENT_ID_BOTTOM]: false,
    [RESERVE_LAYOUT_ELEMENT_ID_CENTRAL]: true,

    [RESERVE_LAYOUT_ELEMENT_ID_LEFTAREA]: false,
    [RESERVE_LAYOUT_ELEMENT_ID_RIGHTAREA]: true,
  },


  modifyAllowTarget : ['curation', 'contents', 'others'],
  canvasTools : ['curation-palette', 'contents-palette', 'mini-controller', 'graph', 'drop-focusing', 'combination'],
  mode: null,

  leftUIIList: [
    // CoreTools.ExternalComponentsPalette.UIIStatement.identification,
    CoreTools.ComponentPalette.UIIStatement.identification,
    // CoreTools.Elements.UIIStatement.identification,
    CoreTools.Pages.UIIStatement.identification,
    // CoreTools.NodeTreeInspector.UIIStatement.identification,
    CoreTools.API.UIIStatement.identification,
    CoreTools.ResourceManager.UIIStatement.identification,
    // CoreTools.GridEditor.UIIStatement.identification,
    // CoreTools.Notifications.UIIStatement.identification,
    // CoreTools.ProjectExplorer.UIIStatement.identification,
    // CoreTools.NPM.UIIStatement.identification,
    // CoreTools.WebpackManager.UIIStatement.identification,
    // CoreTools.Inspector.UIIStatement.identification,
    // CoreTools.Database.UIIStatement.identification,
    // CoreTools.DataInspector.UIIStatement.identification,
    // CoreTools.ProjectSettings.UIIStatement.identification,
  ],

  rightUIIList: [
    CoreTools.GridEditor.UIIStatement.identification,
    CoreTools.Settings.UIIStatement.identification,
    CoreTools.Curator.UIIStatement.identification,
    // CoreTools.Notifications.UIIStatement.identification,
    // CoreTools.ProjectExplorer.UIIStatement.identification,
    // CoreTools.NPM.UIIStatement.identification,
    // CoreTools.WebpackManager.UIIStatement.identification,
    // CoreTools.Inspector.UIIStatement.identification,
    // CoreTools.Database.UIIStatement.identification,
    // CoreTools.DataInspector.UIIStatement.identification,
    // CoreTools.ProjectSettings.UIIStatement.identification,

  ],

  topUIIList: [
    [
      // CoreTools.ProjectExplorer.UIIStatement.identification,
      // CoreTools.Inspector.UIIStatement.identification,

      UIIStatements.User.UserDisplay.identification,
    ],

    [
      // CoreTools.Database.UIIStatement.identification,

      UIIStatements.Studio.DeviceModeSmaller.identification,
      UIIStatements.Studio.DeviceModeMobile.identification,
      UIIStatements.Studio.DeviceModeTablet.identification,
      UIIStatements.Studio.DeviceModeDesktop.identification,
      UIIStatements.Studio.DeviceModeFit.identification,
    ],

    [
      CoreTools.Git.UIIStatement.identification,
      UIIStatements.Studio.Fullscreen.identification,
      // UIIStatements.Studio.DeviceModeSwitch.identification,
      CoreTools.Publisher.UIIStatement.identification,
      UIIStatements.Studio.ExitButton.identification,
    ],
  ],

  bottomUIIList: [
    [

      // CoreTools.Database.UIIStatement.identification,
    ],


    [
      // UIIStatements.Studio.OnAirStatus.identification,
    ],
  ],


  boot : {
    loadStatus : 'none',
  },


  session : {
    id : null,
    onAir:false,
    certified: false,
    certifiedKey : null,
    workerInstanceState : '',

    startTime : null,
    brokenTime: null,
    certifiedTime : null,
  },


  pageInfo : {
    params : {},
    path : null,
    session : null,
    queries : {},
    storeSnapshot : {},
  },

  etc : {
    deviceMode : 'fit',
    fullscreen : false,
    theme : 'light',
  },
});



function initializeStateFilter(initialState){
  let parsedURL = parseUrl(location.href, true);



  let state = Object.assign({}, initialState);

  let {
    allows,
    tools, // Studio reducer에서 처리
    canvastools,
    // route, // studio에서 처리
    sessionID, // studio에서 처리
    site, // studio에서 처리
    owner, // studio에서 처리
    mode,
    deviceMode,

    layout,

    theme,

  } = parsedURL.query;


  if( layout ){
    // state.layoutUsingMap = {};
    let usingLayouts = layout.split(',');

    state.layoutUsingMap = mapValues(state.layoutUsingMap, (value, key) => !!usingLayouts.find((layoutID)=> key === layoutID));
  }

  if( allows ){
    state.modifyAllowTarget = allows.split(',');
  }

  if( canvastools ){
    state.canvasTools = canvastools.split(',');
  }

  if( mode ){

    state.mode = mode;
  }

  if( theme ){
    state.etc.theme = theme;
  }

  if( deviceMode ){
    state.etc.deviceMode = deviceMode || 'fit';
  }

  return state;
}

function reducer_mode(state = (initialState.mode), action){
  switch ( action.type ){

    default :
      return state;
  }
}


function reducer_modifyAllowTarget(state = (initialState.modifyAllowTarget), action){
  switch ( action.type ){

    default :
      return state;
  }
}


function reducer_UIICollection(state = (initialState.uiiCollection), action){
  return state;
}

function reducer_layoutUsingMap(state = (initialState.layoutUsingMap), action){
  switch ( action.type ){
    case ActionTypes.LAYOUT_ENABLE :
      return Object.assign({}, state, {
        [action.layoutID] : true,
      });

    case ActionTypes.LAYOUT_DISABLE :
      return Object.assign({}, state, {
        [action.layoutID] : false,
      });

    case ActionTypes.LAYOUT_ENABLE_TOGGLE :
      return Object.assign({}, state, {
        [action.layoutID] : !state[action.layoutID],
      });
    default:
      return state;
  }
}

function reducer_leftUIIList(state = (initialState.leftUIIList), action){
  return state;
}

function reducer_rightUIIList(state = (initialState.rightUIIList), action){
  return state;
}

function reducer_topUIIList(state = (initialState.topUIIList), action){
  return state;
}

function reducer_bottomUIIList(state = (initialState.bottomUIIList), action){
  return state;
}


function reducer_boot(state = (initialState.boot), action){
  switch( action.type ){
    case ActionTypes.BOOT_COMPLETE :
      return Object.assign({}, state, {
        status : 'running',
      });

    case ActionTypes.RENDER_SUSPENDED :
      return Object.assign({}, state, {
        status : 'suspended',
      });

    case ActionTypes.RENDER_RESUME :
      return Object.assign({}, state, {
        status : 'running',
      });
    default :
      return state;
  }
}

function reducer_session(state = (initialState.session), action){
  switch ( action.type ){
    case ActionTypes.START_SESSION:
      return Object.assign({}, state, {
        id : action.sessionID,
      });

    case ActionTypes.CONNECT_AIR:
      return Object.assign({}, state, {
        onAir : true,
        startTime : action.startTime,
      });

    case ActionTypes.BROKEN_AIR:
      return Object.assign({}, state, {
        onAir : false,
        certified : false,
        certifiedKey : null,
        brokenTime : action.brokenTime,
      });

    case ActionTypes.CERTIFIED_AIR:
      return Object.assign({}, state, {
        certified : true,
        certifiedKey : null,
        certifiedTime : action.time,
      });

    default :
      return state;
  }
}

function pageInfo(state = initialState.pageInfo, action){
  switch (action.type){
    case ActionTypes.SET_PAGE_INFO_VUEX:
      return Object.assign({}, state, {
        storeSnapshot : action.storeSnapshot,
      });
    case ActionTypes.SET_PAGE_INFO:
      return Object.assign({}, state, {
        params : action.params,
        path : action.path,
        session : action.session,
        queries : action.queries,
      });
    default :
      return state;
  }
}

function etc(state = (initialState.etc), action){
  switch ( action.type ){
    case ActionTypes.CHANGE_CANVAS_DEVICE_MODE:
      return Object.assign({}, state, {
        deviceMode : action.deviceMode,
      });

    case ActionTypes.TOGGLE_FULLSCREEN:
      return Object.assign({}, state, {
        fullscreen : !state.fullscreen,
      });

    default :
      return state;
  }
}

export default combineReducers({
  modifyAllowTarget : reducer_modifyAllowTarget,
  uiiCollection : reducer_UIICollection,
  layoutUsingMap : reducer_layoutUsingMap,
  leftUIIList : reducer_leftUIIList,
  rightUIIList : reducer_rightUIIList,
  topUIIList : reducer_topUIIList,
  bottomUIIList : reducer_bottomUIIList,
  boot : reducer_boot,
  session : reducer_session,
  mode : reducer_mode,
  etc : etc,
  pageInfo,
});
