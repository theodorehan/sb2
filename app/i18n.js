/**
 * i18n.js
 *
 * This will setup the i18n language files and locale data for your app.
 *
 */
import { addLocaleData } from 'react-intl';
import enLocaleData from 'react-intl/locale-data/en';
import deLocaleData from 'react-intl/locale-data/de';
import koLocaleData from 'react-intl/locale-data/ko';

import { DEFAULT_LOCALE } from './containers/LanguageProvider/constants';

import enTranslationMessages from './translations/en.json';
import deTranslationMessages from './translations/de.json';
import koTranslationMessages from './translations/ko.json';
import jpTranslationMessages from './translations/jp.json';
import cnTranslationMessages from './translations/cn.json';

addLocaleData(enLocaleData);
addLocaleData(deLocaleData);
addLocaleData(koLocaleData);

export const appLocales = [
  'en',
  'de',
  'jp',
  'ko',
  'cn',
];

export const formatTranslationMessages = (locale, messages) => {
  const defaultFormattedMessages = locale !== DEFAULT_LOCALE
    ? formatTranslationMessages(DEFAULT_LOCALE, enTranslationMessages)
    : {};
  return Object.keys(messages).reduce((formattedMessages, key) => {
    const formattedMessage = !messages[key] && locale !== DEFAULT_LOCALE
      ? defaultFormattedMessages[key]
      : messages[key];
    return Object.assign(formattedMessages, { [key]: formattedMessage });
  }, {});
};

export const translationMessages = {
  en: formatTranslationMessages('en', enTranslationMessages),
  de: formatTranslationMessages('de', deTranslationMessages),
  ko: formatTranslationMessages('ko', koTranslationMessages),
  jp: formatTranslationMessages('jp', jpTranslationMessages),
  cn: formatTranslationMessages('cn', cnTranslationMessages),
};
