var gulp = require('gulp');
var bump = require('gulp-bump');
var git = require('gulp-git');


// Will patch the version
// Basic usage:
gulp.task('bump', function(){
  gulp.src('./package.json')
    .pipe(bump({type:'patch'}))
    .pipe(gulp.dest('./'));

  gulp.run('add');
});
//
// // Run git add with options
gulp.task('add', function(){
  return gulp.src('./package.json')
    .pipe(git.add());
});
//
// // Run git commit
// // src are the files to commit (or ./*)
// gulp.task('commit', function(){
//     return gulp.src('./package.json')
//         .pipe(git.commit('version up'));
// });
//
// // Run git push
// // remote is the remote repo
// // branch is the remote branch to push to
// gulp.task('push', function(){
//     git.push('origin', 'master', function (err) {
//         if (err) throw err;
//     });
// });

gulp.run('bump');
