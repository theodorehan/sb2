import { injectGlobal } from 'styled-components';

/* eslint no-unused-expressions: 0 */
injectGlobal`
  body {
    height: 100%;
    width: 100%;
  }

  body {
    /*font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;*/
  }

  body.fontLoaded {
    /*font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; */
  }

  #app {
   
    min-height: 100%;
    min-width: 100%;
  }

  p,
  label {
    font-family: Georgia, Times, 'Times New Roman', serif;
    line-height: 1.5em;
  }
  
  button, input {
    border : 0;
  }
  
  body {
  
    background-color: #0E0F1A; 
  
    font-family: 'Nanum Square', 'Open Sans', sans-serif;
  
    margin:0px;
  }
  
   
  .ant-notification, .ant-select-dropdown, .ant-cascader-menus {
    z-index : 20000!important;
    color : #333;
    font-smoothing: antialiased;
    -webkit-font-smoothing: antialiased;
  }
  
  .ant-tooltip, .ant-popover, .ant-calendar-picker-container, .ant-message, .ant-modal-wrap {
    z-index : 20000!important;
    font-smoothing: antialiased;
    -webkit-font-smoothing: antialiased;
  }
  
  .ant-popover .ant-popover-inner{
      box-shadow: 0 1px 20px rgba(25, 20, 20, 0.66)!important;
  }
  
  .ant-calendar {
    box-shadow: 0 1px 10px rgba(1, 25, 47, 0.8)!important;
  }
  
  
  .ant-modal-wrap  {
    background-color:rgba(0,0,0,0.5)!important;
  }
  
  
  .ant-modal-content {
    background-color:transparent!important;
  }
  
  
  .ant-select-dropdown {
    width:auto!important;
  }
`;
