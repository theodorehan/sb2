/**
 * Created by seonwoong on 2017. 5. 10..
 */
import namespaceBindGenerator from '../../utils/redux/namespaceBindGenerator';
let defineType = namespaceBindGenerator('USER');


export const SIGN_UP = defineType('SIGN_UP');
export const SIGN_IN = defineType('SIGN_IN');


export const LOAD_USER = defineType("LOAD_USER");
