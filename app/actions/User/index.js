/**
 * Created by seonwoong on 2017. 5. 10..
 */
import * as Types from './types';

export function SIGN_IN(_sessionID){
  return {
    type : Types.SIGN_IN,
    sessionID : _sessionID,
  };
}

export function SIGN_UP(){
  return {
    type : Types.SIGNUP,
  };
}

export function LOAD_USER(data){
  return {
    type : Types.LOAD_USER,
    data,
  }
}
