import namespaceBindGenerator from '../../utils/redux/namespaceBindGenerator';
let defineType = namespaceBindGenerator('Studio');


export const LAYOUT_ENABLE = defineType('LAYOUT_ENABLE');
export const LAYOUT_DISABLE = defineType('LAYOUT_DISABLE');
export const LAYOUT_ENABLE_TOGGLE = defineType('LAYOUT_ENABLE_TOGGLE');
export const BOOT_COMPLETE = defineType('BOOT_COMPLETE');

export const RENDER_SUSPENDED = defineType('RENDER_SUSPENDED');
export const RENDER_RESUME = defineType('RENDER_RESUME');

export const START_SESSION = defineType('START_SESSION');
export const CONNECT_AIR = defineType('CONNECT_AIR');
export const BROKEN_AIR = defineType('BROKEN_AIR');
export const CERTIFIED_AIR = defineType('CERTIFIED_AIR');


export const CHANGE_CANVAS_DEVICE_MODE = defineType('CHANGE_CANVAS_DEVICE_MODE');
export const TOGGLE_FULLSCREEN = defineType('TOGGLE_FULLSCREEN');

export const SET_PAGE_INFO = defineType('SET_PAGE_INFO');
export const SET_PAGE_INFO_VUEX = defineType('SET_PAGE_INFO_VUEX');
