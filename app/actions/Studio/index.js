import * as Types from './types';


export function LAYOUT_ENABLE(_layoutID){
  return {
    type : Types.LAYOUT_ENABLE,
    layoutID : _layoutID,
  }
}


export function LAYOUT_DISABLE(_layoutID){
  return {
    type : Types.LAYOUT_DISABLE,
    layoutID : _layoutID,
  }
}


export function LAYOUT_ENABLE_TOGGLE(_layoutID){
  return {
    type : Types.LAYOUT_ENABLE_TOGGLE,
    layoutID : _layoutID,
  }
}

export function BOOT_COMPLETE(){
  return {
    type : Types.BOOT_COMPLETE,
  };
}

export function RENDER_SUSPENDED(){
  return {
    type : Types.RENDER_SUSPENDED,
  };
}


export function RENDER_RESUME(){
  return {
    type : Types.RENDER_RESUME,
  };
}

export function START_SESSION(sessionID){
  return {
    type : Types.START_SESSION,
    sessionID,
  };
}

export function CONNECT_AIR(startTime) {
  return {
    type : Types.CONNECT_AIR,
    startTime,
  };
}

export function BROKEN_AIR(brokenTime) {
  return {
    type : Types.BROKEN_AIR,
    brokenTime,
  };
}

export function CERTIFIED_AIR(certifiedKey, time) {
  return {
    type : Types.CERTIFIED_AIR,
    time,
    certifiedKey,
  };
}


export function CHANGE_CANVAS_DEVICE_MODE(deviceMode) {
  return {
    type : Types.CHANGE_CANVAS_DEVICE_MODE,
    deviceMode,
  }
}


export function TOGGLE_FULLSCREEN() {
  return {
    type : Types.TOGGLE_FULLSCREEN,
  }
}


export function SET_PAGE_INFO(path, params, queries, session) {
  return {
    type : Types.SET_PAGE_INFO,
    params,
    path,
    queries,
    session,
  }
}

export function SET_PAGE_INFO_VUEX(storeSnapshot){
  return {
    type : Types.SET_PAGE_INFO_VUEX,
    storeSnapshot,
  }
}
