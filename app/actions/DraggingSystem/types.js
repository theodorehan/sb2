import namespaceBindGenerator from '../../utils/redux/namespaceBindGenerator';
let defineType = namespaceBindGenerator('DragSystem');


export const MOUSE_DOWN = defineType('MOUSE_DOWN');
export const MOUSE_DOWN_RELEASE = defineType('MOUSE_DOWN_RELEASE');
export const START_DRAG = defineType('START_DRAG');
export const DRAG = defineType('DRAG');


export const NATIVE_DRAG_START = defineType('NATIVE_DRAG_START');
export const NATIVE_DRAG_FINISH = defineType('NATIVE_DRAG_FINISH');
