import * as Types from './types';

export function MOUSE_DOWN(offsetX, offsetY){
  return {
    type : Types.MOUSE_DOWN,
    offsetX,
    offsetY,
  };
}

export function MOUSE_DOWN_RELEASE(){
  return {
    type : Types.MOUSE_DOWN_RELEASE,
  };
}

export function START_DRAG(_draggableID, _x, _y, _draggableInfo, detachedGhostImage) {
  return {
    type : Types.START_DRAG,
    draggableID : _draggableID,
    x : _x,
    y : _y,
    draggableType : _draggableInfo.type,
    draggableData : _draggableInfo.data,
    detachedGhostImage,
  };
}

export function DRAG(_x, _y, _hoveredZoneID) {
  return {
    type : Types.DRAG,
    x : _x,
    y : _y,
    hoveringZoneID: _hoveredZoneID,
  };
}


export function NATIVE_DRAG_START(contextType, data) {
  return {
    type : Types.NATIVE_DRAG_START,
    contextType,
    data,
  };
}



export function NATIVE_DRAG_FINISH() {
  return {
    type : Types.NATIVE_DRAG_FINISH,
  };
}
