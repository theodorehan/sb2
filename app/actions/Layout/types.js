import namespaceBindGenerator from '../../utils/redux/namespaceBindGenerator';
let defineType = namespaceBindGenerator('Layout');

export const LEFTAREA_WIDTH_MODIFY = defineType("LEFTAREA_WIDTH_MODIFY");
export const RIGHTAREA_WIDTH_MODIFY = defineType("RIGHTAREA_WIDTH_MODIFY");
export const CREATE_CUSTOM_LAYOUT = defineType('CREATE_CUSTOM_LAYOUT');
export const MODIFY_CUSTOM_LAYOUT = defineType('MODIFY_CUSTOM_LAYOUT');
export const REMOVE_CUSTOM_LAYOUT = defineType("REMOVE_CUSTOM_LAYOUT");

export const FLASH_HIDE_LAYOUT = defineType("FLASH_HIDE_LAYOUT");
export const FLASH_SHOW_LAYOUT = defineType("FLASH_SHOW_LAYOUT");
export const LAYOUT_PIN = defineType("LAYOUT_PIN");
export const LAYOUT_PIN_RELEASE = defineType("LAYOUT_PIN_RELEASE");
export const LAYOUT_FULLSCREEN = defineType('LAYOUT_FULLSCREEN');
export const LAYOUT_FULLSCREEN_EXIT = defineType('LAYOUT_FULLSCREEN_EXIT');
