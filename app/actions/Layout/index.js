import * as Types from './types';


export function LEFTAREA_WIDTH_MODIFY(width){
  return {
    type : Types.LEFTAREA_WIDTH_MODIFY,
    width,
  }
}

export function RIGHTAREA_WIDTH_MODIFY(width){
  return {
    type : Types.RIGHTAREA_WIDTH_MODIFY,
    width,
  }
}

export function CREATE_CUSTOM_LAYOUT(layoutID, x, y, w, h, zIndex = 'auto'){
  return {
    type : Types.CREATE_CUSTOM_LAYOUT,
    layoutID,
    x,
    y,
    w,
    h,
    zIndex,
  }
}

export function REMOVE_CUSTOM_LAYOUT(layoutID) {
  return {
    type : Types.REMOVE_CUSTOM_LAYOUT,
    layoutID,
  }
}


export function MODIFY_CUSTOM_LAYOUT(layoutID, x, y, w, h, zIndex){
  return {
    type : Types.MODIFY_CUSTOM_LAYOUT,
    layoutID,
    x,
    y,
    w,
    h,
    zIndex,
  }
}


export function FLASH_SHOW_LAYOUT(layoutID) {
  return {
    type : Types.FLASH_SHOW_LAYOUT,
    layoutID,
  }
}


export function FLASH_HIDE_LAYOUT(layoutID) {
  return {
    type : Types.FLASH_HIDE_LAYOUT,
    layoutID,
  }
}

export function LAYOUT_PIN(layoutID) {
  return {
    type : Types.LAYOUT_PIN,
    layoutID,
  }
}

export function LAYOUT_PIN_RELEASE(layoutID) {
  return {
    type : Types.LAYOUT_PIN_RELEASE,
    layoutID,
  }
}


export function LAYOUT_FULLSCREEN(layoutID) {
  return {
    type : Types.LAYOUT_FULLSCREEN,
    layoutID,
  }
}

export function LAYOUT_FULLSCREEN_EXIT(layoutID) {
  return {
    type : Types.LAYOUT_FULLSCREEN_EXIT,
    layoutID,
  }
}
