import namespaceBindGenerator from '../../utils/redux/namespaceBindGenerator';
let defineType = namespaceBindGenerator('Dashboard');

export const OPEN_MENU = defineType('OPEN_MENU');
export const CLOSE_MENU = defineType('CLOSE_MENU');

export const LOADED_TEMPLATES = defineType('LOADED_TEMPLATES');
export const LOADED_SITES = defineType('LOADED_SITES');
export const CREATING_SITE = defineType('CREATING_SITE');
