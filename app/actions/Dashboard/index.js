import * as Types from './types';

export function OPEN_MENU(){
  return {
    type : Types.OPEN_MENU,
  };
}

export function CLOSE_MENU(){
  return {
    type : Types.CLOSE_MENU,
  };
}


export function LOADED_TEMPLATES(list) {
  return {
    type : Types.LOADED_TEMPLATES,
    list,
  }
}


export function LOADED_SITES(list) {
  return {
    type : Types.LOADED_SITES,
    list,
  }
}


export function CREATING_SITE(){
  return {
    type : Types.CREATING_SITE,
  }
}
