import namespaceBindGenerator from '../../utils/redux/namespaceBindGenerator';
let defineType = namespaceBindGenerator('Deployment');


export const DEPLOY = defineType('DEPLOY');
export const UNDEPLOY = defineType('UNDEPLOY');
