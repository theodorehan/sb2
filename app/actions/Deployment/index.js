import * as Types from './types';

export function DEPLOY(_layoutID, _elasticID){
  return {
    type : Types.DEPLOY,
    layoutID : _layoutID,
    elasticID : _elasticID,
  };
}

export function UNDEPLOY(_layoutID){
  return {
    type : Types.UNDEPLOY,
    layoutID : _layoutID,
  };
}
