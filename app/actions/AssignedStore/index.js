import * as Types from './types';

export function LOAD_TREE(root){
  return {
    type : Types.LOAD_TREE,
    root,
  };
}
