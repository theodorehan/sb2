import namespaceBindGenerator from '../../utils/redux/namespaceBindGenerator';
let defineType = namespaceBindGenerator('Site');


export const LOAD_SITE = defineType('LOAD_SITE');


export const LOAD_ROUTE = defineType('LOAD_ROUTE');


export const SET_CONFIGS = defineType('SET_CONFIGS');
