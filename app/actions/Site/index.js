import * as Types from './types';

export function LOAD_SITE(_info){
  return {
    type : Types.LOAD_SITE,
    info : _info,
  };
}


export function LOAD_ROUTE(route){
  return {
    type : Types.LOAD_ROUTE,
    route,
  };
}

export function SET_CONFIGS(levels) {
  return {
    type : Types.SET_CONFIGS,
    levels,
  }
}
