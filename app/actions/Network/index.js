import * as Types from './types';

export function UI_BLOCKING_REQUEST_BEGIN(reqID){
  return {
    type : Types.UI_BLOCKING_REQUEST_BEGIN,
    reqID,
  };
}

export function UI_BLOCKING_REQUEST_FINISH(reqID){
  return {
    type : Types.UI_BLOCKING_REQUEST_FINISH,
    reqID,
  };
}
