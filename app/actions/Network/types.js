import namespaceBindGenerator from '../../utils/redux/namespaceBindGenerator';
let defineType = namespaceBindGenerator('Network');


export const UI_BLOCKING_REQUEST_BEGIN = defineType('UI_BLOCKING_REQUEST_BEGIN');
export const UI_BLOCKING_REQUEST_FINISH = defineType('UI_BLOCKING_REQUEST_FINISH');
