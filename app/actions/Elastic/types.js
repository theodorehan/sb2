import namespaceBindGenerator from '../../utils/redux/namespaceBindGenerator';
let defineType = namespaceBindGenerator('Elastic');


export const ASSIGN_ELASTIC = defineType('ASSIGN_ELASTIC');
export const RELEASE_ELASTIC = defineType('RELEASE_ELASTIC');

export const SET_STATIC_DATA = defineType('SET_STATIC_DATA');
