import * as Types from './types';


export function ASSIGN_ELASTIC(_id, _toolKey, staticData){
  return {
    type : Types.ASSIGN_ELASTIC,
    id : _id,
    toolKey : _toolKey,
    staticData,
  }
}

export function RELEASE_ELASTIC(_id){
  return {
    type : Types.RELEASE_ELASTIC,
    id : _id,
  }
}

export function SET_STATIC_DATA(elasticID, staticData, merge) {
  return {
    type : Types.SET_STATIC_DATA,
    elasticID,
    staticData,
    merge,
  }
}
