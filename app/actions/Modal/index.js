import * as Types from './types';

export function CLOSE(){
  return {
    type : Types.CLOSE,
  };
}

export function OPEN(){
  return {
    type : Types.OPEN,
  };
}
