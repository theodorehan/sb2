import namespaceBindGenerator from '../../utils/redux/namespaceBindGenerator';
let defineType = namespaceBindGenerator('Modal');


export const OPEN = defineType('OPEN');
export const CLOSE = defineType('CLOSE');
