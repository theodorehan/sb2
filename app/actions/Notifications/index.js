import * as Types from './types';

export function NEW_MESSAGE(message, time, level){
  return {
    type : Types.NEW_MESSAGE,
    message,
    time,
    level,
  };
}
