import Ax from '../Axios';

export function GET_ROUTE_DIRECTIVE_TREE(userSessionID, siteID, siteOwner, directiveName, directiveNameFallback) {
  return Ax.get(`/user/${userSessionID}/site/${siteID}/${siteOwner}/store/getRouteDirectiveTree`, {
    params : {
      directiveName,
      directiveNameFallback,
    },
  });
}


export function MODIFY_ROUTE_DIRECTIVE_JSON(userSessionID, siteID, siteOwner, directiveName, directiveJSON, id, description, publishNow) {
  return Ax.post(`/user/${userSessionID}/site/${siteID}/${siteOwner}/store/modify-directive-json`, {
      directiveName,
      directiveJSON,
      id,
      description,
      publishNow,
    });
}


export function CREATE_NEW_PAGE(userSessionID, siteID, siteOwner, name, path) {
  return Ax.get(`/user/${userSessionID}/site/${siteID}/${siteOwner}/store/create-new-page`, {
    timeout:999999,
    params : {
      name,
      path,
    },
  });
}
