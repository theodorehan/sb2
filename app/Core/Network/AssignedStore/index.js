import Ax from '../Axios';

export function getTree(userSessionID, siteID, siteOwner) {
  return Ax.get(`/user/${userSessionID}/site/${siteID}/${siteOwner}/store/get-tree`, {});
}

//// Built In
export function getBuiltInComponentCategories(userSessionID, siteID, siteOwner) {
  return Ax.get(`/user/${userSessionID}/site/${siteID}/${siteOwner}/store/builtin-component-categories`, {});
}

export function getBuiltInComponentCategorySub(userSessionID, siteID, siteOwner, path) {
  return Ax.get(`/user/${userSessionID}/site/${siteID}/${siteOwner}/store/builtin-component-categories/sub`, {
    params : {path},
  });
}

export function getBuiltInComponentCategoryComponentPreview(userSessionID, siteID, siteOwner, path) {
  return Ax.get(`/user/${userSessionID}/site/${siteID}/${siteOwner}/store/builtin-component-categories/component-preview`, {
    params : {path},
  });
}

//// Fragments
export function getFragmentComponentCategories(userSessionID, siteID, siteOwner) {
  return Ax.get(`/user/${userSessionID}/site/${siteID}/${siteOwner}/store/fragments-component-categories`, {});
}

export function getFragmentComponentCategorySub(userSessionID, siteID, siteOwner) {
  return Ax.get(`/user/${userSessionID}/site/${siteID}/${siteOwner}/store/fragments-component-categories/sub`, {
    params : {path},
  });
}

//// Included
export function getIncludedComponentCategories(userSessionID, siteID, siteOwner) {
  return Ax.get(`/user/${userSessionID}/site/${siteID}/${siteOwner}/store/included-component-categories`, {});
}

export function getIncludedComponentCategorySub(userSessionID, siteID, siteOwner) {
  return Ax.get(`/user/${userSessionID}/site/${siteID}/${siteOwner}/store/included-component-categories/sub`, {
    params : {path},
  });
}

//// Store
export function getStoreComponentCategories(userSessionID, siteID, siteOwner) {
  return Ax.get(`/user/${userSessionID}/site/${siteID}/${siteOwner}/store/store-component-categories`, {});
}

export function getStoreComponentCategorySub(userSessionID, siteID, siteOwner) {
  return Ax.get(`/user/${userSessionID}/site/${siteID}/${siteOwner}/store/store-component-categories/sub`, {
    params : {path},
  });
}




export function getFileDataAsUTF8(assignedStoreID, filepath) {
  return Ax.get('/assignedStore/fileread', {
    params: {
      assignedStoreID,
      filepath,
    },
  });
}

export function fileUpload(data, assignedStoreID) {
  var config = {headers: {'Content-Type': 'application/x-www-form-urlencoded'}};
  return Ax.post('/assignedStore/fileUpload/' + assignedStoreID, data, config);
}

export function imageSave(base64Img, imageID, assignedStoreID) {
  var data = {base64Img: base64Img, imageID: imageID, assignedStoreID: assignedStoreID};
  return Ax.post('/assignedStore/imageSave',data);
}

export function getImageList(data) {
  return Ax.get('/assignedStore/imageList', data);
}

