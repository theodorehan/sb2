import Ax from '../Axios';


export function siteTemplates(userSessionID){
  return Ax.get(`/user/${userSessionID}/template/site-template-list`, {});
}

export function createSite(userSessionID, themeDirName, themeName, siteName){
  return Ax.get(`/user/${userSessionID}/dashboard/create-site`, {
    timeout : 999999999,
    params : {
      themeDirName,
      siteName,
      themeName,
    },
  });
}


export function mySites(userSessionID){
  return Ax.get(`/user/${userSessionID}/dashboard/site-list`, {});
}
