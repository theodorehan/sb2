import Ax from '../Axios';



export function search(value, maxResults, pageToken, order) {
  return Ax.get('/youtube/search', {
    params : {
      value,
      maxResults,
      order,
      pageToken,
    },
  });
}
