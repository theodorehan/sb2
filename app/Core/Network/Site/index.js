import Ax from '../Axios';


export function readInfo(userSessionID, siteID, siteOwner){
  return Ax.get(`/user/${userSessionID}/site/${siteID}/${siteOwner}/info`, {});
}


export function readConfigSet(userSessionID, siteID, siteOwner){
  return Ax.get(`/user/${userSessionID}/site/${siteID}/${siteOwner}/configs`, {});
}


export function updateTheme(userSessionID, siteID, siteOwner){
  return Ax.get(`/user/${userSessionID}/site/${siteID}/${siteOwner}/store/update-theme`, {});
}


export function readRootRoute(userSessionID, siteID, siteOwner){
  return Ax.get(`/user/${userSessionID}/site/${siteID}/${siteOwner}/store/root-route`, {});
}


export function prebuildSite(userSessionID, siteID, siteOwner){
  return Ax.get(`/user/${userSessionID}/site/${siteID}/${siteOwner}/store/build`, {
    timeout : 999999999999999999,
  });
}


export function loadResourceCategories(userSessionID, siteID, siteOwner, target){
  return Ax.get(`/user/${userSessionID}/site/${siteID}/${siteOwner}/store/resource-categories`, {
    params : {
      target,
    },
  });
}


export function loadResourceSubCategories(userSessionID, siteID, siteOwner, target, path){
  return Ax.get(`/user/${userSessionID}/site/${siteID}/${siteOwner}/store/resource-categories/sub`, {
    params : {
      target,
      path,
    },
  });
}


export function uploadResourceImageBase64(userSessionID, siteID, siteOwner, target, data, resourcePath, overwrite){
  var data = {data, resourcePath, overwrite, target};

  return Ax.post(`/user/${userSessionID}/site/${siteID}/${siteOwner}/store/resource/upload-base64-image`, data);
}
