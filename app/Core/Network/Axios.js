import Axios from 'axios';


var instance = Axios.create({
  baseURL: '/api/',
  timeout: 1000 * 100, // 100초
  withCredentials : true,
  // headers: {'X-Custom-Header': 'foobar'}
});


export default instance;
