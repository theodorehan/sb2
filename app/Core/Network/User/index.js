import Ax from '../Axios';

// export function signinUser(_id, _pw){
//
// }

export function signIn(id, pw){
  return Ax.get('/user/signin', {
    params : {
      id,
      pw,
    },
  });
}


export function signInIce(id, pw){
  return Ax.post('/user/sign-in',{
    id,
    pw,
  });
}


export function sessionCheck(id, pw){
  return Ax.get('/user/signin', {
    params : {
      id,
      pw,
    },
  });
}


export function readData(sessionID) {
  return Ax.get('/user/read', {
    params : {
      sessionID,
    },
  });
}
