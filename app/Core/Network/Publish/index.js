import Ax from '../Axios';


export function getPolicy(userSessionID, siteID, siteOwner){
  return Ax.get(`/user/${userSessionID}/site/${siteID}/${siteOwner}/publish/policy`, {});
}


export function getStates(userSessionID, siteID, siteOwner){
  return Ax.get(`/user/${userSessionID}/site/${siteID}/${siteOwner}/publish/states`, {});
}
