import Ax from '../Axios';



export function createSession(userSessionID, siteID, siteOwner){
  return Ax.get(`/user/${userSessionID}/site/${siteID}/${siteOwner}/create-studio-session`, {});
}
