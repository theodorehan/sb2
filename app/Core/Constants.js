export const ToolInterfacePipeline = 'ToolInterfacePipeline';
export const ReduxAction = 'ReduxAction';
export const InlineAction = 'InlineAction';

export HTMLTagCollection from './Constants/HTMLTagInfoCollection';
