import SocketIO from 'socket.io-client';
import * as StudioActions from '../../actions/Studio/index';
import * as NotificationsActions from '../../actions/Notifications/index';
window.io = SocketIO; // for Chrome socket.io devtool

export default class Connection {
  socket;
  studio;

  constructor(_studioComponent) {
    this.studio = _studioComponent;

  }

  connectStudio(userSessionData, studioSessionID, siteInfo, routes){
    let sock = this.socket = SocketIO.connect('/', {
      reconnection: false,
    });

    this.sock = sock;

    console.log('CONNECT', sock)
    /**
     * Stream Communication Manager
     *
     * 계정 / 사이트 인증
     * 도커 컨테이너 생성 요청
     * 도커 컨테이너 레디
     * 컨테이너 서버 레디
     * 컨테이너 서버 에디팅 사이트 어플리케이션 빌드 완료 및 서버 시작 알림
     */
    sock.on('connect',  () => {
      let { dispatch } = this.studio.props;

      sock.emit('identify', {
        iam: "STUDIO",
        studioID: studioSessionID,
        userSessionID: userSessionData.sessionID,
      });

      dispatch(StudioActions.CONNECT_AIR(Date.now()));
    });

    sock.on('disconnect', () => {
      let { dispatch } = this.studio.props;

      dispatch(StudioActions.BROKEN_AIR(Date.now()));
      console.warn('disconnected socket');
    });


    sock.on('certification',(data) => {
      let { dispatch } = this.studio.props;

      if( data.result === 'denied' ){

      } else if (data.result === 'accept' ){
        dispatch(StudioActions.CERTIFIED_AIR(Date.now()));
        dispatch(StudioActions.BOOT_COMPLETE());
        this.studio.startEditorForUser(userSessionData.sessionID, studioSessionID, siteInfo, routes);
      }
    });

    sock.on('worker-ready', (data)=>{
      let { Studio, User, dispatch } = this.studio.props;
    });

    sock.on('notice', (data)=>{
      let { Studio, User, dispatch } = this.studio.props;
      let { message, level, date } = data;

      dispatch(NotificationsActions.NEW_MESSAGE(message, data, level));
    });


    sock.on('chat', (data) => {
      let { Studio, User, dispatch } = this.studio.props;
      let { message, level, date } = data;

    })

    sock.on('ready-to-hmr', ()=>{
      let { dispatch } = this.studio.props;
      //
      // dispatch(StudioActions.BOOT_COMPLETE());
      //
      // this.studio.startEditorForUser(userSessionData.sessionID, studioSessionID, siteInfo, routes);
      console.info("HMR Ready");
    });

    sock.on('ready-to-service', ()=>{
      console.log('IO:ready-to-service')
      let { dispatch } = this.studio.props;

      console.info("Service Ready");
      dispatch(StudioActions.BOOT_COMPLETE());
      this.studio.startEditorForUser(userSessionData.sessionID, studioSessionID, siteInfo, routes);
    })

    sock.on('state', (state)=>{
      let { dispatch } = this.studio.props;

      console.log('IO:state', state)
      if( state.busy ){
        this.studio.context.global.uiBlockingReqBegin('busy');
      } else {
        if( !this.studio.props.Deployment.central ){
          console.info("Service Ready");

          dispatch(StudioActions.BOOT_COMPLETE());
          this.studio.startEditorForUser(userSessionData.sessionID, studioSessionID, siteInfo, routes);
        } else {
          this.studio.context.global.uiBlockingReqFinish('busy');
        }
      }
    });
  }

  publish(state){
    let resId = this.studio.context.global.uiBlockingReqBegin();
    return new Promise((resolve, reject)=>{
      this.sock.emit('publish-node', {
        state,
        resId,
      });

      this.sock.once('res-' + resId, (data)=>{
        resolve(data);
        this.studio.context.global.uiBlockingReqFinish(resId);
      })
    })

  }
}
