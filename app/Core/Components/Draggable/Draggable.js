import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import {
  connect,
} from 'react-redux';
import uuid from 'uuid';
import './Draggable.scss';


@connect((state) => ({DragSystem:state.get('DragSystem')}))
class Draggable extends Component {
  static contextTypes = {
    dragSystem: PropTypes.object,
  };

  static propTypes = {
    component: PropTypes.any.isRequired,

    _additionalStyle: PropTypes.object,
    _draggableType: PropTypes.string.isRequired,
    _draggableData: PropTypes.object.isRequired,
    _useDetachedGhostImage: PropTypes.string,
    ghostOpacity : PropTypes.number,

    onDragBegin: PropTypes.func, // 드래그 시작
    onDragEnd: PropTypes.func, // 드래그 완료
    onDragAiming: PropTypes.func, // 드래그 조준
    onDragMove: PropTypes.func, // 드래그
    onDragDrop: PropTypes.func, // 임시. 드롭


    invisibleGhost : PropTypes.bool,
  };

  static defaultProps = {
    ghostOpacity : 1,
  }

  constructor(props) {
    super(props);

    this.state = {
      mouseDowned: false,
      downPoint : {x:null, y:null},
      accumulatedMoveX : 0,
      accumulatedMoveY : 0,
      prevClientX : 0,
      prevClientY : 0,
    };

    this.draggableID = uuid.v4();
  }


  listenDraggingFromDragSystem(x, y, deltaX, deltaY){
    if( this.props.onDragMove ){
      this.props.onDragMove(x,y, deltaX, deltaY);
    }
  }

  listenDragEndFromDragSystem(x,y){
    if( this.props.onDragEnd ){
      this.props.onDragEnd(x,y);
    }
  }

  listenDropFromDragSystem(x,y){
    if( this.props.onDragDrop ){
      this.props.onDragDrop(x,y);
    }
  }

  onNativeDragStarted(e){
    this.onMouseDown(e);
    let nativeE = e.nativeEvent;
    e.stopPropagation();
    e.preventDefault();

    let deltaX = Math.abs(nativeE.clientX - this.state.prevClientX);
    let deltaY = Math.abs(nativeE.clientY - this.state.prevClientY);

    let amx = this.state.accumulatedMoveX + deltaX;
    let amy = this.state.accumulatedMoveY + deltaY;

    if( amx + amy > 5 ){
      this.context.dragSystem.startDrag(this, nativeE.clientX, nativeE.clientY);

      if( this.props.onDragBegin ){
        this.props.onDragBegin(nativeE.clientX, nativeE.clientY);
      }


      // Drag 감지 권한이 DraggingGround 로 넘어갔기때문에 mouseDown을 false 로 둔다.
      this.setState({
        mouseDowned : false,
      });
    }

    this.setState({
      accumulatedMoveX : amx,
      accumulatedMoveY : amy,
      prevClientX : nativeE.clientX,
      prevClientY : nativeE.clientY,
    });

  }


  onNativeDragEnd(e){

    console.log('dragend');
    this.context.dragSystem.mouseUp(e);
  }

  onMouseDown(e) {
    let {nativeEvent} = e;
    e.stopPropagation();
    e.preventDefault();

    let mouseDowned = true;
    this.setState({
      mouseDowned,
      downPoint : {
        x : nativeEvent.clientX,
        y : nativeEvent.clientY,
      },
      accumulatedMoveX : 0,
      accumulatedMoveY : 0,
      prevClientX : nativeEvent.clientX,
      prevClientY : nativeEvent.clientY,
    });

    let thisBoundingRect = this.dom.getBoundingClientRect();

    this.context.dragSystem.mouseDown(this, nativeEvent.clientX - thisBoundingRect.left, nativeEvent.clientY - thisBoundingRect.top);
  }

  onMouseUp(e) {

    if (this.state.mouseDowned) {
      let mouseDowned = false;
      this.setState({mouseDowned});
    }
  }

  onMouseLeave(e) {

    if (this.state.mouseDowned) {
      let mouseDowned = false;
      this.setState({mouseDowned});
    }
  }

  onMouseMove(e) {
    let nativeE = e.nativeEvent;
    e.stopPropagation();
    e.preventDefault();

    if (this.state.mouseDowned) {
      let deltaX = Math.abs(nativeE.clientX - this.state.prevClientX);
      let deltaY = Math.abs(nativeE.clientY - this.state.prevClientY);

      let amx = this.state.accumulatedMoveX + deltaX;
      let amy = this.state.accumulatedMoveY + deltaY;

      if( amx + amy > 5 ){
        this.context.dragSystem.startDrag(this, nativeE.clientX, nativeE.clientY);

        if( this.props.onDragBegin ){
          this.props.onDragBegin(nativeE.clientX, nativeE.clientY);
        }


        // Drag 감지 권한이 DraggingGround 로 넘어갔기때문에 mouseDown을 false 로 둔다.
        this.setState({
          mouseDowned : false,
        });
      }

      this.setState({
        accumulatedMoveX : amx,
        accumulatedMoveY : amy,
        prevClientX : nativeE.clientX,
        prevClientY : nativeE.clientY,
      });
    }
  }

  am_i_dragging(){
    if( this.props.DragSystem.dragState.dragging ){
      if( this.props.DragSystem.dragState.draggableID === this.draggableID ){
        return true;
      }
    }
    return false;
  }

  copyInnerHTMLAsPlain(){
    this.plainHTML = this.dom.innerHTML;
  }

  renderDraggingVirtualContents(){
    if( this.am_i_dragging() ){
      let style = {
        left : this.props.DragSystem.dragState.aimX - this.props.DragSystem.dragState.clickPointOffsetX ,
        top : this.props.DragSystem.dragState.aimY - this.props.DragSystem.dragState.clickPointOffsetY,
        opacity : this.props.ghostOpacity,
      };

      return <div className="dragging-ghost-container" style={style} dangerouslySetInnerHTML={{__html : this.plainHTML }}/>;
    }
  }

  renderContents(){
    let RealComponent = this.props.component;

    return RealComponent ? this.renderRealComponent(RealComponent) : this.props.children;
  }

  renderRealComponent(RealComponent) {
    if( React.isValidElement(RealComponent) ){
      return RealComponent;
    }

    return (
      <RealComponent { ...this.props} />
    );
  }

  render() {

    let {_additionalStyle} = this.props;

    return <div
      draggable
      onDragStart={:: this.onNativeDragStarted }
      ref={ (dom) => this.dom = dom }
      className={classnames("drag-ghost-agent", this.props.className)}
      style={{..._additionalStyle}}
      onMouseUp={:: this.onMouseUp }
      onDragEnd={:: this.onNativeDragEnd }
      onMouseLeave={:: this.onMouseLeave }
    >
      { this.renderContents() }
      { !this.props.invisibleGhost && this.renderDraggingVirtualContents() }
    </div>
  }
}

export default Draggable;
