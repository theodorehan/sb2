import React, {Component} from "react";
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {ToolAccessMap} from '../../../components/Toolset';

@connect()
class ToolProvider extends Component {

  static childContextTypes = {
    elastic : PropTypes.object,
  }

  static contextTypes = {
    // Supervisor's
    elasticToolSystems : PropTypes.object,
  }

  constructor(props) {
    super(props);

  }

  getChildContext() {
    return {
      elastic: {
        dispatch : this.elasticDispatch.bind(this),
        state : this.props.Elastic[this.props.elasticID],
        getOtherOnlyOneToolState : this.getOtherOnlyOneToolState.bind(this),
        registerElasticTool : this.registerToolInstance.bind(this),
        removeRegisteredElasticTool: this.removeRegisteredToolInstance.bind(this),
        listenUpdatedOnce : (listener) => this.eventManager.once('updated', listener),
      },
    };
  }

  getOtherOnlyOneToolState(toolKey){
    let elasticIds = Object.keys(this.props.Elastic);

    for(let i = 0; elasticIds.length; i++ ){
      if( this.props.Elastic[elasticIds[i]].toolKey === toolKey ){
        return this.props.Elastic[elasticIds[i]].state;
      }
    }

    return null;
  }

  registerToolInstance(inst){
    let elasticID = this.props.elasticID;
    let elastic = this.props.Elastic[this.props.elasticID];

    let toolKey = elastic.toolKey;
    console.log(elasticID, toolKey, inst);
    this.context.elasticToolSystems.registerToolInstance(elasticID, toolKey, inst);
  }

  removeRegisteredToolInstance(){
    let elasticID = this.props.elasticID;
    let elastic = this.props.Elastic[this.props.elasticID];

    let toolKey = elastic.toolKey;

    this.context.elasticToolSystems.removeToolInstance(elasticID, toolKey);
  }

  elasticDispatch(_action, withPromise){
    let originType = _action.type;
    let wrappedType = `E(${originType})`;
    let wrappedAction = Object.assign({}, _action, {
      type : wrappedType, // Develop 용 E(ORIGIN ACTION) 으로 표시한다.
      __originType : originType,
      // __elasticAction 이 1 이면 type 으로 리듀싱하지 않고 __originType 으로 리듀싱을 처리한다.
      // type 의 Action명을 체크 하지 않고 __elasticAction 플래그에 따른 __originType 을 사용하는 이유는
      // type 은 wrapping 된 타입으로 실제 리듀싱 처리시에 언래핑에 큰 비용이 부과되므로 매번 액션이 발생할 때 마다
      // 언래핑 하는 비용을 회피하기 위함이다.
      __elasticAction : 1,

      __elasticID : this.props.elasticID,
    });

    if( withPromise ){
      return new Promise((resolve)=>{
        wrappedAction.__resolve = resolve;
        this.props.dispatch(wrappedAction);
      })
    } else {
      this.props.dispatch(wrappedAction);
    }
  }

  setToolInstance(toolInstance){

    if( !this.toolInstance ){
      this.toolInstance = toolInstance;
      /**
       * ToolComponent 가 Wrapping 되어 있으므로 접근이 ToolComponent Instance 에 접근하는것은 사실상 불가능함.
       */
    }
  }



  render() {

    let elastic = this.props.Elastic[this.props.elasticID];
    if (elastic) {
      let {toolKey, state, staticData} = elastic;

      if (toolKey) {
        let Tool = ToolAccessMap[toolKey];

        let ToolComponent = Tool.Component;

        if (ToolComponent) {

          return <ToolComponent {...state} {...this.props} staticData={staticData} _elasticID={this.props.elasticID} ref={(toolInstance)=>{ this.setToolInstance(toolInstance) }}/>;
        } else {
          return <div>
            { toolKey } Tool Component no exists.
          </div>
        }

      }
    }

    return <div>
      No Tool
    </div>
  }
}

export default ToolProvider;
