import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {
  connect,
} from 'react-redux';
import uuid from 'uuid';


@connect((state) => ({DragSystem:state.get('DragSystem')}))
class Zone extends Component {

  static propTypes = {
    component : PropTypes.any,

    // John의 Z 인덱스 깊이 깊이값이 높은 순서대로 hoveringTarget 이 결정된다.
    zoneZDepth: PropTypes.number.isRequired,

    // John이 받는 draggableType 값 호버링중인 Draggable 의 타입이 허용 목록에 존재 하지 않으면 hovering 판정이 되지 않는다.
    // 목록이 지정되지 않으면 모든 Draggable 에 반응한다.
    acceptDraggableTypes : PropTypes.array,

    zoneLayoutISA: PropTypes.string.isRequired, // 존이 위치한 layout ID
  }

  static contextTypes = {
    zoneSystems: PropTypes.object,
  }

  static childContextTypes = {
    zone : PropTypes.object,
  }



  constructor(props){
    super(props);

    this.zoneID = uuid.v4();

  }

  getChildContext(){
    return {
      zone : {
        id : this.zoneID,
        layoutISA : this.props.zoneLayoutISA,
        aimed : this.aimedMe(),
        aimItemType : this.props.DragSystem.dragState.draggableType,
        aimItemData : this.props.DragSystem.dragState.draggableData,
      },
    }
  }

  componentDidMount(){
    this.context.zoneSystems.zoneRegister(this.zoneID, this, this.props.zoneZDepth, this.props.acceptDraggableTypes);
  }

  componentWillUnmount(){
    this.context.zoneSystems.zoneDestroy(this.zoneID, this);
  }

  aimedMe(){
    let hoveringZoneID = this.props.DragSystem.dragState.hoveringZoneID;

    return this.zoneID === hoveringZoneID;
  }


  render(){
    let Component = this.props.component;
    let aimedMe = this.aimedMe();

    return <Component
      {...this.props}
      zoneID={this.zoneID}
      draggableAimed={aimedMe}
      aimedDraggableType={aimedMe && this.props.DragSystem.dragState.draggableType}
      aimedDraggableData={aimedMe && this.props.DragSystem.dragState.draggableData}/>;
  }
}

export default Zone;
