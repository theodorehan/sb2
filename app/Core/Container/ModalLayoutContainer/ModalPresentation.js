import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {unitToRealNumber} from '../../../utils/computing';

import * as ModalActions from '../../../actions/Modal';
import * as LayoutActions from '../../../actions/Layout';

import ToolProvider from '../../Components/ToolProvider';

import {
  CLOSE_LAYOUT_WITH_ELASTIC,
  CLOSE_LAYOUT_ONLY,
  OPEN_LAYOUT_ONLY,
} from '../../../components/Toolset/UIISystemActionSheets';


export default class ModalPresentation extends Component {
  static propTypes = {
    isa : PropTypes.string.isRequired,
    modalIndex : PropTypes.number.isRequired,
    Layout : PropTypes.object,
    dispatch : PropTypes.func,
  }

  static contextTypes = {
    UIISystems : PropTypes.object,
  }

  static childContextTypes = {
    modal: PropTypes.object,
    layout : PropTypes.object,
    glowUpdate : PropTypes.func,
  }



  constructor(props){
    super(props);

    this.layoutType = '[modal]';
  }

  /**
   * layout context interface
   */
  onClose(){
    this.context.UIISystems.emitActionSheet(CLOSE_LAYOUT_WITH_ELASTIC(this.props.isa,null,true));
  }

  onFullscreen(){
    let { Layout, isa } = this.props;
    let myLayout = Layout[isa];

    this.props.dispatch(
      LayoutActions.LAYOUT_FULLSCREEN(isa)
    );
  }

  onFullscreenExit(){
    let { Layout, isa } = this.props;
    let myLayout = Layout[isa];

    this.props.dispatch(
      LayoutActions.LAYOUT_FULLSCREEN_EXIT(isa)
    );
  }

  getChildContext() {
    return {
      glowUpdate : this.fillShadow.bind(this),
      modal : {



      },

      layout : this,


    }
  }

  fillShadow(){
    if( this.glowDOMBasket && this.toolDOM){
      this.glowDOMBasket.innerHTML = this.toolDOM.innerHTML;
    }
  }


  componentDidMount(){
    this.fillShadow();


    setTimeout(()=>{
      this.fillShadow();
    },1000);
  }

  componentDidUpdate(){
    this.fillShadow();
  }


  render(){
    let layout = this.props.Layout[this.props.isa];

    let width = unitToRealNumber(window.innerWidth, layout.width);
    let height = unitToRealNumber(window.innerHeight, layout.height);

    if( layout.fullscreen ){
      width = window.innerWidth;
      height = window.innerHeight;
    }

    return <div className="modal-presentation" style={{zIndex : 10000 + this.props.modalIndex}}>
      <div className="modal-vertical-positioning">
        <div className="glow" style={{width: width, height: height}} dangerouslySetInnerHTML={{__html : ''}} ref={(dom) => this.glowDOMBasket = dom }>

        </div>
        <div
          ref={(dom) => this.toolDOM = dom }
          className="modal-kernel"
          style={{width: width, height: height, opacity:1}}>
          <ToolProvider
            width={width}
            height={height}
            Elastic={this.props.Elastic}
            elasticID={this.props.Deployment[this.props.isa]}/>
        </div>
      </div>
    </div>;
  }
}
