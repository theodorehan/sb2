import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {
  connect,
} from 'react-redux';
import classnames from 'classnames';
import html2canvas from 'html2canvas';

import * as ModalActions from '../../../actions/Modal';
import './style.scss';

import ToolProvider from '../../Components/ToolProvider';
import ModalPresentation from './ModalPresentation';

import {
  CLOSE_LAYOUT_WITH_ELASTIC,
  CLOSE_LAYOUT_ONLY,
  OPEN_LAYOUT_ONLY,
} from '../../../components/Toolset/UIISystemActionSheets';


@connect((state) => ({
  Modal : state.get('Modal'),
  Deployment : state.get('Deployment'),
  Elastic : state.get('Elastic'),
  Layout : state.get('Layout'),
}), null, null, {
  withRef: true,
})
class ModalLayoutContainer extends Component {

  constructor(props) {
    super(props);
  }


  render() {
    let {Layout} = this.props;


    let modalLayouts;

    let layoutKeys = Object.keys(Layout);
    modalLayouts = layoutKeys.filter((key) => /^modal_/.test(key));
    modalLayouts = modalLayouts.sort();

    return <div className="modal-container">

      <div className={classnames("content", modalLayouts.length > 0 && 'blur')}>
        { this.props.children }
      </div>

      { modalLayouts.map( (layoutKey, i) =>
        <ModalPresentation
          key={layoutKey}
          isa={layoutKey}
          modalIndex={i}
          dispatch={this.props.dispatch}
          Elastic={this.props.Elastic}
          Deployment={this.props.Deployment}
          Layout={this.props.Layout}/> ) }
    </div>
  }
}


export default ModalLayoutContainer;
