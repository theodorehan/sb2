import uuid from 'uuid';

import * as ElasticActions from '../../../../../actions/Elastic/index';
import * as DeploymentActions from '../../../../../actions/Deployment/index';
import * as StudioActions from '../../../../../actions/Studio/index';
import * as LayoutActions from '../../../../../actions/Layout/index';
import * as ModalActions from '../../../../../actions/Modal';

function nextModalLayoutID(Layout){

  let layoutKeys = Object.keys(Layout);
  let modalLayoutKeys = layoutKeys.filter((key) => /^modal_/.test(key));
  modalLayoutKeys = modalLayoutKeys.sort();

  let lastModalLayoutKey = modalLayoutKeys[modalLayoutKeys.length -1 ];

  if( lastModalLayoutKey ){
    let matched = lastModalLayoutKey.match(/^modal_(\d+)$/);
    let lastModalIndexKey = parseInt(matched[1]);
    return `modal_${lastModalIndexKey+1}`;
  } else {
    return 'modal_0';
  }
}

/**
 *
 * @param _supervisor
 * @param _actionSheet
 * @param _belongedLayoutID
 * @param staticData
 * @param options
 */
export const open = function (_supervisor, _actionSheet, _belongedLayoutID, staticData, options) {
  let { props } = _supervisor;
  let { dispatch, Layout, Deployment, Elastic } = props;
  let { areaIndicator, toolKey, width : originWidth, height : originHeight } = _actionSheet.options;

  // Options
  let {
    DISABLE_DUPLICATE_UNDEPLOY, // 중복된 ToolKey Undeploy 하지 않음 같은 Tool로 Elastic 만 교체 가능
    UNIQUE_LAYER, // 레이어를 생성 할 때 매번 새로운 레이어를 생성한다.
    onLayoutID,
  } = options;


  let layoutID = areaIndicator;

  if( _belongedLayoutID ){
    layoutID = findLayoutID(areaIndicator, _belongedLayoutID, Layout);
  }

  // LayoutID 가 [layer] 일 경우 새로운 ToolKey에 기반한 layoutID를 사용한다.
  if( layoutID === '[layer]'){
    layoutID = `layer_${toolKey}`;
  }

  // 요청 layoutID가 [layer] 이고 UNIQUE_LAYER 옵션이 같이 입력 될 경우 layout ID 를 uuid를 사용하여 유일한 ID로 생성한다.
  if( areaIndicator === '[layer]' && UNIQUE_LAYER ){
    layoutID += "_" + uuid.v4();
  }


  if( layoutID === '[modal]'){
    layoutID = nextModalLayoutID(Layout);
  }


  if( typeof onLayoutID === 'function' ){
    onLayoutID(layoutID);
  }

    let livingLayout = Layout[layoutID] || {};
  // 해당 layoutID로 Layout 오브젝트가 없다면 생성한다.
  // if( !Layout[layoutID] ){
    dispatch(LayoutActions.CREATE_CUSTOM_LAYOUT(
      layoutID,
      options.x || livingLayout.x || 20,
      options.y || livingLayout.y || 20,
      options.width || livingLayout.width || originWidth || 300,
      options.height || livingLayout.height || originHeight || 300))
  // }


  let id = uuid.v4();


  if( Deployment[layoutID] ){
    let alreadyDeployedElasticID = Deployment[layoutID];


    // 현재 Deploy 된 layout 에 중복된 ToolKey 가 입력 되었을 때 Undeploy 하고 다른 ToolKey 가 입력되었을 때 다시 Deploy 하는 로직
    // DISABLE_DUPLICATE_UNDEPLOY 이 true 로 입력되었을 경우 중복된 ToolKey 가 들어와도 Undeploy 하지 않고 새 Elasitc 으로 Deploy 한다.
    if( !DISABLE_DUPLICATE_UNDEPLOY && Elastic[alreadyDeployedElasticID].toolKey === toolKey ){
      // 중복된 ToolKey 입력시 Undeploy 하는 블럭

      // Deploy 되어 사요중인 toolKey 와 동일한 toolKey 로 open 액션이 발생 했을 때 layout 을 닫는다.
      // DEPLOY 를 해제 한다.
      // Elastic Release 는 if 블럭 아래에서 진행한다.

      dispatch(StudioActions.LAYOUT_DISABLE(layoutID));
      dispatch(DeploymentActions.UNDEPLOY(layoutID));
    } else {

      dispatch(ElasticActions.ASSIGN_ELASTIC(id, toolKey, staticData));

      dispatch(DeploymentActions.DEPLOY(layoutID, id)); // Deploy 후에 이전에 Deploy 된 Elastic 을 제거한다.
    }

    dispatch(ElasticActions.RELEASE_ELASTIC(alreadyDeployedElasticID)); // 사용하지 않는 Elastic 제거.
  } else {

    dispatch(ElasticActions.ASSIGN_ELASTIC(id, toolKey, staticData));

    dispatch(DeploymentActions.DEPLOY(layoutID, id));
    dispatch(StudioActions.LAYOUT_ENABLE(layoutID));

  }

  return layoutID;
}

export const closeWithLayout = function (_supervisor, _actionSheet, _belongedLayoutID, staticData, options) {
  let { props } = _supervisor;
  let { dispatch, Layout, Deployment, Elastic } = props;
  let { layoutID, toolKey, layoutRemove } = _actionSheet.options;

  let deployedElasticID = Deployment[layoutID];

  dispatch(StudioActions.LAYOUT_DISABLE(layoutID));
  if( layoutRemove ){
    dispatch(LayoutActions.REMOVE_CUSTOM_LAYOUT(layoutID));
  }
  dispatch(DeploymentActions.UNDEPLOY(layoutID));
  dispatch(ElasticActions.RELEASE_ELASTIC(deployedElasticID));
}

export const setStaticDataByLayout = function(_supervisor, _actionSheet, _belongedLayoutID, staticData, options){
  let { props } = _supervisor;
  let { dispatch, Layout, Deployment, Elastic } = props;
  let { layoutID, toolKey, layoutRemove } = _actionSheet.options;
  let elasticID = Deployment[layoutID];

  let {merge} = options;

  dispatch(ElasticActions.SET_STATIC_DATA(elasticID, staticData, merge));
}

export const closeLayoutOnly = function (_supervisor, _actionSheet, _belongedLayoutID, staticData, options) {
  let { props } = _supervisor;
  let { dispatch, Layout, Deployment, Elastic } = props;
  let { layoutID } = _actionSheet.options;

  dispatch(StudioActions.LAYOUT_DISABLE(layoutID));
}

export const openLayoutOnly = function (_supervisor, _actionSheet, _belongedLayoutID, staticData, options) {
  let { props } = _supervisor;
  let { dispatch, Layout, Deployment, Elastic } = props;
  let { layoutID } = _actionSheet.options;

  dispatch(StudioActions.LAYOUT_ENABLE(layoutID));
}

/**
 * FindLayoutID
 * @param _areaIndicator
 * @param _belongedLayoutID
 * @param _Layout
 * @returns {*}
 */
function findLayoutID(_areaIndicator, _belongedLayoutID, _Layout){

  if( _belongedLayoutID ){
    let LayoutInfo = _Layout[_belongedLayoutID];

    if( _areaIndicator === "[chaining]"){
      // 발생한 Action 의 주체가 있는 LayoutID(belonged layoutID) 로부터 액션을 적용할 대상 LayoutID를 구한다.

      return LayoutInfo.chainingLayoutID;
    }
  }


  return _areaIndicator;
}
