import {
  exitFullscreen,
  isFullscreen,
  launchIntoFullscreen,
} from '../../../../utils/fullscreen';

import * as StudioActions from '../../../../actions/Studio';
let actionTargets = {
  Studio : StudioActions,
}

import * as ToolInterfacePipelineActionHanders from './ActionHandlers/ToolInterfacePipeline';

// Type > action


export function ToolInterfacePipeline(_supervisor, _actionSheet, _belongedLayoutID, staticData, options){
  let {action} = _actionSheet;

  switch(action){
    case 'open' :
      return ToolInterfacePipelineActionHanders.open(_supervisor, _actionSheet, _belongedLayoutID, staticData, options);
    case 'closeWithLayout' :
      return ToolInterfacePipelineActionHanders.closeWithLayout(_supervisor, _actionSheet, _belongedLayoutID, staticData, options);
    case 'closeLayoutOnly':
      return ToolInterfacePipelineActionHanders.closeLayoutOnly(_supervisor, _actionSheet, _belongedLayoutID, staticData, options);
    case 'openLayoutOnly':
      return ToolInterfacePipelineActionHanders.openLayoutOnly(_supervisor, _actionSheet, _belongedLayoutID, staticData, options);
    case 'setStaticDataByLayout':
      return ToolInterfacePipelineActionHanders.setStaticDataByLayout(_supervisor, _actionSheet, _belongedLayoutID, staticData, options);
  }
}

export function ReduxAction(_supervisor, _actionSheet, _belongedLayoutID, staticData, options){
  let {action} = _actionSheet;
  let {dispatch} = _supervisor.props;

  let actionTokens = action.split('.');
  let actionTarget = actionTokens[0];
  let actionName = actionTokens[1];

  console.log(_actionSheet);

  let reduxAction = actionTargets[actionTarget][actionName];

  dispatch(reduxAction.apply(null, _actionSheet.options.actionParams));
}

export function InlineAction(_supervisor, _actionSheet, _belongedLayoutID, staticData, options) {
  let {action} = _actionSheet;

  switch(action){
    case 'toggle-fullscreen' :
      if( isFullscreen() ) {
        exitFullscreen();
      }else {
        launchIntoFullscreen();
      }
      break;
    case 'exit' :
      location.href='/dashboard';
      break;
  }
}
