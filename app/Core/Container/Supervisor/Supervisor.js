import React, {Component} from 'react';
import PropTypes from 'prop-types';
import uuid from 'uuid';
import EventEmitter from 'events';
import Promise from 'bluebird';

import {
  connect,
} from 'react-redux';

import * as CoreConstants from '../../Constants';

import * as UIIActionTypeHander from './UIIActionTypeHandler';

import {
  SET_STATIC_DATA_BY_LAYOUT,
  CLOSE_LAYOUT_WITH_ELASTIC,
} from '../../../components/Toolset/UIISystemActionSheets';

import {ToolAccessMap} from '../../../components/Toolset/CoreTools';

import * as DragSystemActions from '../../../actions/DraggingSystem';

@connect((state) => ({
  DragSystem: state.get('DragSystem'),
  Deployment: state.get('Deployment'),
  Elastic: state.get('Elastic'),
  Layout: state.get('Layout'),
}), null, null, {
  withRef: true,
})
class Supervisor extends Component {
  constructor(props) {
    super(props);

    this.zones = {};
    this.events = new EventEmitter();
    this.registeredToolInstances = {};
    this.functionStore = {};

    window.supervisorDebug = this;
  }

  static childContextTypes = {
    zoneSystems: PropTypes.object,
    UIISystems: PropTypes.object,
    ToolInterfacePipeline: PropTypes.object,
    events : PropTypes.object,
    elasticToolSystems : PropTypes.object,
    functionStore : PropTypes.object,
    nativeDrag : PropTypes.object,
  }

  getChildContext() {
    return {
      nativeDrag : {
        start : (contextType, data) => {
          this.props.dispatch(DragSystemActions.NATIVE_DRAG_START(contextType, data))
        },
        finish : () => {
          this.props.dispatch(DragSystemActions.NATIVE_DRAG_FINISH())
        },
      },

      zoneSystems: {
        zoneRegister: this.registerZone.bind(this),
        zoneDestroy: this.destroyedZone.bind(this),
        zones: this.zones,
      },

      UIISystems: {
        act: this.receiveFromUII.bind(this),
        emitActionSheet : this.receiveActionSheet.bind(this),
        openTool : this.requestOpenTool.bind(this),
        getUnique : uuid.v4,
      },

      ToolInterfacePipeline: {},

      events : {
        listen : this.subscribe.bind(this),
        removeListen : this.removeSubscribe.bind(this),
        emit: this.emit.bind(this),
      },

      elasticToolSystems : {
        registerToolInstance : this.registerToolInstance.bind(this),
        removeToolInstance : this.removeRegisteredToolInstance.bind(this),
      },

      functionStore : {
        register : this.registerFunc.bind(this),
        get : (key) => this.functionStore[key],
        remove : (key) => delete this.functionStore[key],
      },
    }
  }

  registerFunc(keyOrFunc, func){
    if( typeof keyOrFunc === 'function' ){
      let unique = uuid.v4();

      this.functionStore[unique] = keyOrFunc;

      return unique;
    } else {

      this.functionStore[keyOrFunc] = func;
    }
  }

  subscribe(name, func){
    this.events.on(name, func);
  }

  removeSubscribe(name, func){
    this.events.removeListener(name, func);
  }

  emit(name, data){
    this.events.emit(name, data);
  }

  registerToolInstance(elasticID, toolKey, instance) {

    this.registeredToolInstances[toolKey] = this.registeredToolInstances[toolKey] || {};
    this.registeredToolInstances[toolKey][elasticID] = instance;
  }

  removeRegisteredToolInstance(elasticID, toolKey) {

    if( !this.registeredToolInstances[toolKey] ){
      throw new Error(`remove fail : ${toolKey} Tool is not registered.`);
    }

    if( !this.registeredToolInstances[toolKey][elasticID] ){
      throw new Error(`remove fail : ${toolKey}:${elasticID} Tool is not registered.`);
    }

    delete this.registeredToolInstances[toolKey][elasticID];
  }

  registerZone(_id, who, zDepth, acceptDraggables) {
    // tool 과 Studio.js 의 layout 파츠를 등록한다.

    this.zones[_id] = {
      who,
      zDepth,
      acceptDraggables,
    };
  }

  destroyedZone(_id, _who) {
    // 등록된 zone 이 unmount 될 때 등록된 존을 해제 한다.

    delete this.zones[_id];
  }

  // UII
  receiveFromUII(_uiiStatement, _belongedLayoutID, data = {}, options = {}) {
    let {actionSheet} = _uiiStatement;
    console.log(actionSheet, _belongedLayoutID);

    switch (actionSheet.type) {
      case CoreConstants.ToolInterfacePipeline :
        UIIActionTypeHander.ToolInterfacePipeline(this, actionSheet, _belongedLayoutID, data, options);
        break;
      case CoreConstants.ReduxAction :
        UIIActionTypeHander.ReduxAction(this, actionSheet, _belongedLayoutID, data, options);
        break;
      case CoreConstants.InlineAction :
        UIIActionTypeHander.InlineAction(this, actionSheet, _belongedLayoutID, data, options);
        break;
    }
  }

  // UII
  receiveActionSheet(actionSheet, data = {}, options = {}) {

    switch (actionSheet.type) {
      case CoreConstants.ToolInterfacePipeline :
        return UIIActionTypeHander.ToolInterfacePipeline(this, actionSheet, null, data, options);
        break;
    }
  }

  requestOpenTool(toolKey, layout, option, staticData) {
    if( !ToolAccessMap[toolKey] ){
      throw new Error(`Not exists tool[${toolKey}].`);
    }
    let that = this;

    if( !layout ){
      throw new Error(`Need a layout argument.`);
    }

    return new Promise((resolve, reject) => {
      this.receiveActionSheet(ToolAccessMap[toolKey].CreateOpenActionSheet(layout), staticData, {
        width : 600,
        height : 400,
        ...option,
        onLayoutID(id){

          resolve(() => {
            that.receiveActionSheet(CLOSE_LAYOUT_WITH_ELASTIC(id, toolKey, true));
          });
        },
      });
    });
  }

  render() {
    return <div>
      { this.props.children }
    </div>
  }
}


export default Supervisor;
