import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import cloneDeep from 'lodash/cloneDeep';

import {
  connect,
} from 'react-redux';

import './DraggingGround.scss';

import {
  MOUSE_DOWN,
  MOUSE_DOWN_RELEASE,
  START_DRAG,
  DRAG,
} from '../../../actions/DraggingSystem';

@connect((state) => ({DragSystem: state.get('DragSystem')}))
class DraggingGround extends Component {


  constructor(props) {
    super(props);

    this.state = {
      dragging: false,
      whoStarted: null,
    }


    this.prevDragX = 0;
    this.prevDragY = 0;
  }

  static contextTypes = {
    zoneSystems : PropTypes.object,
    events : PropTypes.object,
  }

  static childContextTypes = {
    dragSystem: PropTypes.object,
  }

  getChildContext() {
    return {
      dragSystem: {
        startDrag: this.someoneStartedDrag.bind(this),
        mouseDown: this.someoneMouseDowned.bind(this),
        mouseUp: this.onMouseUp.bind(this),
      },
    }
  }

  someoneStartedDrag(_who, _x, _y, type, data, ghostImage) {

    this.setState({
      dragging: true,
      whoStarted: _who,
    });

    if( typeof  _who.copyInnerHTMLAsPlain == 'function'){
      _who.copyInnerHTMLAsPlain();
    }

    this.prevDragX = _x;
    this.prevDragY = _y;
    // console.log(_who.props._useDetachedGhostImage);

    this.props.dispatch(START_DRAG(_who.draggableID,_x,_y, {
      type :type || _who.props._draggableType,
      data :data || _who.props._draggableData,

    }, ghostImage || _who.props._useDetachedGhostImage || null));
  }

  someoneMouseDowned(_who, layerX, layerY) {
    this.props.dispatch(MOUSE_DOWN(layerX, layerY));
  }

  onMouseMove(e) {
    let nativeE = e.nativeEvent;
    if (this.state.dragging) {
      e.stopPropagation();
      e.preventDefault();

      let hoveredZoneID = this.detectHoveringZoneID(nativeE.clientX, nativeE.clientY);
      // console.log('global drag', e.nativeEvent);
      let deltaX = nativeE.clientX - this.prevDragX;
      let deltaY = nativeE.clientY - this.prevDragY;


      this.props.dispatch(DRAG(nativeE.clientX, nativeE.clientY, hoveredZoneID));

      if( typeof this.state.whoStarted.listenDraggingFromDragSystem === 'function'){
        this.state.whoStarted.listenDraggingFromDragSystem(nativeE.clientX, nativeE.clientY, deltaX, deltaY);
      }

      this.prevDragX = nativeE.clientX;
      this.prevDragY = nativeE.clientY;

      this.context.events.emit('DRAG_MOVE',{...this.props.DragSystem.dragState, deltaX, deltaY});
    } else {
    }
  }

  onMouseUp(e) {
  // console.log('mouse up', this.props.DragSystem.dragState.mouseDown );
  //   console.log('asdasdasd',this.props.DragSystem.dragState.mouseDown);


    if( this.props.DragSystem.dragState.mouseDown ){
      e.stopPropagation();
      e.preventDefault();

      let dropEventData = cloneDeep(this.props.DragSystem.dragState);
      dropEventData.dropX = e.clientX;
      dropEventData.dropY = e.clientY;

      this.setState({dragging: false});
      this.props.dispatch(MOUSE_DOWN_RELEASE());

      if( dropEventData.dragging ){

        this.context.events.emit('DRAG_DROP', dropEventData);

        if( typeof this.state.whoStarted.listenDragEndFromDragSystem === 'function'){
          this.state.whoStarted.listenDragEndFromDragSystem(e.clientX, e.clientY);
        }
      } else {

      }
    }
  }

  detectHoveringZoneID(_x, _y){
    let zones = this.context.zoneSystems.zones;
    let zoneIds = Object.keys(zones);

    let id, zone, realDOM, boundingRect, hoveredZoneID = null, zoneInstance, acceptTypes;
    let detectedZones = [];
    for( let i = 0; i < zoneIds.length; i++ ){
      id = zoneIds[i];
      zone = zones[id];
      zoneInstance = zone.who;
      acceptTypes = zone.acceptDraggables;

      realDOM = ReactDOM.findDOMNode(zoneInstance);
      boundingRect = realDOM.getBoundingClientRect();

      if( boundingRect.left <= _x && _x <= boundingRect.right && boundingRect.top <= _y && _y <= boundingRect.bottom ){

        // acceptType 리스트가 존재하면 검사하여 통과시 감지된 목록으로 편입하고
        // 리스트가 존재하지 않으면 바로 감지된 목록으로 편입시킨다.
        if( acceptTypes ){
          if( acceptTypes.indexOf(this.props.DragSystem.dragState.draggableType ) > -1 ){
            detectedZones.push(id);
          }
        } else {
          detectedZones.push(id);
        }
      }
    }


    if( detectedZones.length === 1 ){
      return detectedZones[0];
    } else {
      // Depth 가 제일 높은 zone의 ID를 반환한다.


      // 큰 Depth 가 앞으로 오도록 정렬함. 내림차순정렬
      // [1,5,2] => [5,2,1]
      let sortedDetectedZones = detectedZones.sort(function(a, b){
        if ( zones[a].zDepth < zones[b].zDepth ){
          return 1;
        }else {
          return -1;
        }
      });


      return sortedDetectedZones[0];
    }
  }


  render() {
    return <div className='dragging-ground' onMouseMove={:: this.onMouseMove } onMouseUp={:: this.onMouseUp }>
      { this.state.dragging && <div className="dragging-ground-overlay"/> }
      { this.props.children }
      { this.state.dragging && this.props.DragSystem.dragState.detachedGhostImage &&
        <div
          className="detachedGhost"
          style={{
          left : this.props.DragSystem.dragState.aimX,
          top : this.props.DragSystem.dragState.aimY,
        }}>
          <img
            className="ori"
            src={this.props.DragSystem.dragState.detachedGhostImage}
            />
          <div className="shadow-blur">
            <img
              className="shadow"
              src={this.props.DragSystem.dragState.detachedGhostImage}
            />
          </div>
        </div>}
    </div>;
  }
}

export default DraggingGround;
