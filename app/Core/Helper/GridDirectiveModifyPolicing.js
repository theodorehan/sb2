/**
 * Route Directive Modify Policing
 *
 * Route Directive Node Tree 수정에 관한 정책을 정의 하고 수정한다.
 *
 *
 */
import ComponentNode from 'abstract-component-node';
import flatten from 'lodash/flatten';

export const APPEND_NEW_COLUMN_AFTER = 'append-new-column-after';
export const APPEND_NEW_COLUMN_BEFORE = 'append-new-column-before';

export const APPEND_NEW_ROW_WITH_COLUMN_AFTER = 'new-row-with-column-after';
export const APPEND_NEW_ROW_WITH_COLUMN_BEFORE = 'new-row-with-column-before';

export const INSERT_INSIDE = 'insert-inside';
export const INSERT_INSIDE_WITH_GRID = 'insert-inside-with-grid';

export const INSERT_COLUMN_FIRST = 'insert-column-first';
export const INSERT_COLUMN_LAST = 'insert-column-last';

export const INSERT_ROW_WITH_COLUMN_FIRST = 'insert-row-with-column-first';
export const INSERT_ROW_WITH_COLUMN_LAST = 'insert-row-with-column-last';
/**
 * Grid 요소를 제외한 모든 컴포넌트에 대한 삽입 방식을 반환한다.
 * @param node:DirectiveNode
 * @param nearby:String
 * @returns {*}
 */
export function getAppendOrInsertComponentMethod(node, nearby){



  if( node.tag === 'core-system-column'){
    switch( nearby ){
      case "left" :
        return {
          target : 'core-system-row',
          method : APPEND_NEW_COLUMN_BEFORE,
        };
      case "right" : return {
        target : 'core-system-row',
        method : APPEND_NEW_COLUMN_AFTER,
      };
      case "top" : return {
        target : 'core-system-grid',
        method : APPEND_NEW_ROW_WITH_COLUMN_BEFORE,
      };
      case "bottom" : return {
        target : 'core-system-grid',
        method : APPEND_NEW_ROW_WITH_COLUMN_AFTER,
      };


      case "in" :
        if( node.children && node.children == 0 ){
          return {
            target : 'core-system-column',
            method : INSERT_INSIDE,
          };
        } else {
          return  {
            target : 'core-system-column',
            method : 'reject',
          };;
        }
    }
  } else if ( node.tag === 'core-system-row'){
    switch( nearby ){
      case "left" :return {
        target : 'core-system-row',
        method : APPEND_NEW_COLUMN_BEFORE,
      };
      case "right" : return {
        target : 'core-system-row',
        method : APPEND_NEW_COLUMN_AFTER,
      };
      case "top" : return {
        target : 'core-system-grid',
        method : APPEND_NEW_ROW_WITH_COLUMN_BEFORE,
      };
      case "bottom" : return {
        target : 'core-system-grid',
        method : APPEND_NEW_ROW_WITH_COLUMN_AFTER,
      };
      case "in" : return {
        target : 'core-system-row',
        method : INSERT_COLUMN_FIRST,
      };
    }
  } else if ( node.tag === 'core-system-layer'){
    switch( nearby ){
      // case "in" : return APPEND_NEW_ROW_WIDTH_COLUMN_INNRER;
    }
  } else if ( node.tag === 'core-system-grid'){
    switch( nearby ){
      case "left" : return {
        target : 'core-system-grid',
        method : INSERT_ROW_WITH_COLUMN_FIRST,
      };
      case "right" : return {
        target : 'core-system-grid',
        method : INSERT_ROW_WITH_COLUMN_LAST,
      };
      case "top" :return {
        target : 'core-system-grid',
        method : INSERT_ROW_WITH_COLUMN_FIRST,
      };
      case "bottom" : return {
        target : 'core-system-grid',
        method : INSERT_ROW_WITH_COLUMN_LAST,
      };
      case "in" : return {
        target : 'core-system-grid',
        method : INSERT_ROW_WITH_COLUMN_LAST, // Last 이던 아니던 상관없다 어차피 비어있을거니까
      };
    }
  } else {
    switch( nearby ){
      case "left" : return {
        target : 'core-system-row',
        method : APPEND_NEW_COLUMN_BEFORE,
      };
      case "right" : return {
        target : 'core-system-row',
        method : APPEND_NEW_COLUMN_AFTER,
      };
      case "top" :return {
        target : 'core-system-grid',
        method : APPEND_NEW_ROW_WITH_COLUMN_BEFORE,
      };
      case "bottom" : return {
        target : 'core-system-grid',
        method : APPEND_NEW_ROW_WITH_COLUMN_AFTER,
      };
      case "in" : return {
        target : '[self]',
        method : INSERT_INSIDE_WITH_GRID, // Last 이던 아니던 상관없다 어차피 비어있을거니까
      };
    }
  }
}

export function createNodeGridWithRowWithColumnWithComponent(componentNode, columnProps){
  return ComponentNode.importFromJSON({
    tag : 'core-system-grid',
    children : [
      {
        tag : 'core-system-row',
        children : [
          createColumnNode(columnProps, [componentNode]),
        ],
      },
    ],
  })
}

export function createNodeRowWithColumnWithComponent(componentNode, columnProps){
  return ComponentNode.importFromJSON({
    tag : 'core-system-row',
    children : [
      createColumnNode(columnProps, [componentNode]),
    ],
  })
}

export function createNodeColumnWithComponent(componentNode, columnProps){
  return ComponentNode.importFromJSON(createColumnNode(columnProps, [componentNode]))
}

export function calculateRowColumnSizeMapColumnAppend(appendDirectionMethod, index, sizer, columns){

  // 변수 :
  //  Before 추가 | After 추가
  //    -> Before 이면
  //  direction에 따른 첫번째 컬럼이 1칸 일 경우
  //    -> 다음 요소 또는 제일 많은 크기를 사용하는 요소의 사이즈를 줄이고 남는 공간을 창출하여 남는 크기만큼 설정한다.
  //  컬럼이 두개이상 있고 모든 컬럼이 12칸 일 경우
  //    -> 현재 요소도 12칸으로 설정
  //  모든 컬럼의 사이즈가 동일 할 경우 하나의 크기로
  // xs와 sm의 경우 모든 컬럼의 사이즈가 같을 때 12로
  // lg일 경우 모든 컬럼의 사이즈가 같을 때 최대 6으로 나눈다 ?

  if( sizer === 'xs' || sizer === 'sm' ){
    return { map : columns.map((node) => 12), columnSize : 12}
  } else if ( sizer === 'md' || sizer === 'lg' ){
    if( columns.length == 0 ) return {
      map : [],
      columnSize : 12,
    };


    let gotSpaceSize = null;

    let ment = (decre) => {
      let array = columns;

      if( decre ){
        array = array
          .slice(0, index)
          .reverse();
      } else {
        array = array.slice(index);
      }

      array = array
        .map((column,i) => {
          if( gotSpaceSize ){
            return column.props[sizer] || 12;
          } else {
            if( column.props[sizer] === undefined ){
              return 12;
            }

            if( column.props[sizer] > 1 ){
              let half = column.props[sizer] / 2;
              let floorize = Math.floor(half);

              gotSpaceSize = floorize;
              return column.props[sizer] - floorize;
            } else {
              return column.props[sizer];
            }
          }
        });

      if( decre ){
        array = array.reverse();
      }

      return array;
    }

    let step1Map = [];

    if(appendDirectionMethod === APPEND_NEW_COLUMN_BEFORE){
      step1Map.push(ment(true));
      step1Map.push(ment());
    }else {
      step1Map.push(ment());
      step1Map.unshift(ment(true));
    }

    step1Map = flatten(step1Map);

    if( gotSpaceSize !== null ){
      return {
        map :step1Map,

        columnSize : gotSpaceSize,
      }
    }



    return {

    }
  }
}

export function modifyColumnSizingWithAppendingAndNewColumnProps(appendDirectionMethod, index, columns){
  let maps = {
    xs : calculateRowColumnSizeMapColumnAppend(appendDirectionMethod, index, 'xs',columns),
    sm : calculateRowColumnSizeMapColumnAppend(appendDirectionMethod, index, 'sm',columns),
    md : calculateRowColumnSizeMapColumnAppend(appendDirectionMethod, index, 'md',columns),
    lg : calculateRowColumnSizeMapColumnAppend(appendDirectionMethod, index, 'lg',columns),
  };

  let column;
  for(let i =0; i < columns.length;i++){
    column = columns[i];
    column.props.xs = maps.xs.map[i];
    column.props.sm = maps.sm.map[i];
    column.props.md = maps.md.map[i];
    column.props.lg = maps.lg.map[i];
  }

  return {
    xs : maps.xs.columnSize,
    sm : maps.sm.columnSize,
    md : maps.md.columnSize,
    lg : maps.lg.columnSize,
  };
}

export function createNodeComponent(_componentTag, props = {}){
  return ComponentNode.importFromJSON({
    tag : _componentTag,
    props : props,
  })
}

export function createLayerNode(){
  return ComponentNode.importFromJSON({
    tag : 'core-system-layer',
  });
}

export function createColumnNode(props, children){
  return ComponentNode.importFromJSON({
    tag : 'core-system-column',
    props : Object.assign({
      xs : 12,
      sm : 12,
      md : 12,
      lg : 12,
    }, props),
    children : children || [],
  });
}


export function createRowNode(){
  return ComponentNode.importFromJSON({
    tag : 'core-system-row',
  });
}

export function createGridNode(){
  return ComponentNode.importFromJSON({
    tag : 'core-system-grid',
  });
}
