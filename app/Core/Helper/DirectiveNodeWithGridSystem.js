import {
  COLUMN_DECIDE_BEHAVIOR_LG,
  COLUMN_DECIDE_BEHAVIOR_MD,
  COLUMN_DECIDE_BEHAVIOR_SM,
} from './GridSystemConstants'

export function getEffectiveColumnSizingTypeByScreenWidth(screenWidth, nodeProps){
  let sizingType;

  if( !sizingType && screenWidth >= COLUMN_DECIDE_BEHAVIOR_LG ){
    sizingType = nodeProps.lg ? 'lg' : null;
  }

  if( !sizingType && screenWidth >= COLUMN_DECIDE_BEHAVIOR_MD ){
    sizingType = nodeProps.md ? 'md' : null;
  }

  if( !sizingType && screenWidth >= COLUMN_DECIDE_BEHAVIOR_SM ){
    sizingType = nodeProps.sm ? 'sm' : null;
  }

  if( !sizingType ) {
    sizingType = nodeProps.xs ? 'xs' : null;
  }


  return sizingType;
}


export function getSpecialNodeStyleValue(node, stylename){
  let style = node.style || {};
  if( node.tag === 'core-system-grid' ){
    if( style[stylename] ){
      return style[stylename];
    }

    switch(stylename){
      case 'position':
        return 'static';
    }

    return undefined;
  }

  if( node.tag === 'core-system-row' ){
    if( style[stylename] ){
      return style[stylename];
    }

    switch(stylename){
      case 'position':
        return 'static';
    }

    return undefined;
  }

  if( node.tag === 'core-system-column' ){
    if( style[stylename] ){
      return style[stylename];
    }

    switch(stylename){
      case 'position':
        return 'static';
    }

    return undefined;
  }

  if( node.tag === 'core-system-layer' ){
    if( style[stylename] ){
      return style[stylename];
    }

    switch(stylename){
      case 'position':
        return 'absolute';
    }

    return undefined;
  }


}

export function isBrotherhoodNode(selectedPos, node) {
  let myPos = node.pos;
  let myPosTokens = myPos.split('.');
  let selectPosTokens = selectedPos.split('.');

  if( myPosTokens.length === selectPosTokens.length ){

    // index 배열에서 마지막 요소를 제외하고 나머지를 모두 1:1 비교한다.
    // 1:1 비교중 하나라도 틀린게 있다면 혈통이 다른것으로 간주한다.
    for(let i = 0; i < myPosTokens.length-1; i++ ){
      if( myPosTokens[i] !== selectPosTokens[i] ){
        return false;
      }
    }

    return true;
  }

  return false;
}



export function getStyleValue_padding(node, part){
  if( node.style ){
    if( node.style[part] !== undefined ){

    }
  } else {
    return undefined;
  }
}


export function getNearbyGridSystemNode(_node, targetGridTag = '[all]' ){
  let node = _node;


  if( targetGridTag == '[self]') {
    return node;
  }

  while( node ){

    if( targetGridTag === '[all]' ? isGridSystemNode(node) : (node.tag === targetGridTag) ){
      return node;
    } else {
      if( node.parent ){
        node = node.parent;
      } else {
        return null;
      }
    }
  }
}

export function isGridSystemNode(node) {
  switch(node.tag){
    case "core-system-grid":
    case "core-system-row":
    case "core-system-column":
    case "core-system-layer":
      return true;
    default:
      return false;
  }
}
