import path from 'path';
export const VUE_COMPONENT_FIELDNAME = '__vue__';
export const COMPONENT_PATH_ATTRIBUTE = '__template_path';
export const TEMPLATE_JSON_DIRECTIVE_SIGN = '__vue_component_directive';
export const DIRECTIVE_POS_ATTR_SIGN='__directive_pos';

export function findComponentOwner(dom){
  let targetDOM = dom;
  // console.log(dom, targetDOM instanceof window.HTMLElement, HTMLElement);
  if( targetDOM ){

    while(targetDOM){
      if( targetDOM[VUE_COMPONENT_FIELDNAME] ){
        return targetDOM;
      }

      targetDOM = targetDOM.parentElement;
    }
  }

  return null;
}



export function isVueComponent(dom) {
  if( dom[VUE_COMPONENT_FIELDNAME] ){
    return true;
  }

  return false;
}

export function getVueComponentInfo(dom) {
  let result = {};

  if( dom[VUE_COMPONENT_FIELDNAME] ){
    let vueInstance = dom[VUE_COMPONENT_FIELDNAME];
    let compPath = dom.getAttribute(COMPONENT_PATH_ATTRIBUTE);


    result.editRules = {};
    if( vueInstance.$vnode ){
      if( vueInstance.$vnode.componentOptions ){
        if( vueInstance.$vnode.componentOptions.Ctor ){
          result.editRules = vueInstance.$vnode.componentOptions.Ctor.extendOptions._editRules;
        }
      }
    }
    // name setting
    if( compPath ){
      result.name = path.basename(compPath);
    } else {
      result.name = vueInstance.$options._componentTag;
    }
  } else {
    result.name = 'Unknown';
  }

  return result;
}

export function removeVueExtensionAtName(_name){
  return _name.replace(/\.vue$/i,'');
}


export function findRootDirectiveVueComponent(DOM){
  return DOM.querySelectorAll(`[${TEMPLATE_JSON_DIRECTIVE_SIGN}]`);
}


export function getDirectiveNodeByPos(directiveJSON, pos){

  let posTokens = pos.split('.');

  if( posTokens.length === 1 && parseInt(pos) === 0 ) return directiveJSON;

  let node = directiveJSON;

  for(let i = 1; i < posTokens.length; i++ ){
    node = node.children[parseInt(posTokens[i])];

    if( !node ) return null;
  }


  return node;
}

export function findDirectiveNode(dom){
  let targetDOM = dom;

  if( targetDOM ){

    while(targetDOM){
      if( targetDOM.hasAttribute(DIRECTIVE_POS_ATTR_SIGN) ){
        return targetDOM;
      }

      targetDOM = targetDOM.parentElement;
    }
  }

  return null;
}


export function findDirectiveRootNode(dom){
  let targetDOM = dom;

  if( targetDOM ){

    while(targetDOM){
      if( targetDOM.hasAttribute(TEMPLATE_JSON_DIRECTIVE_SIGN) ){
        return targetDOM;
      }

      targetDOM = targetDOM.parentElement;
    }
  }

  return null;
}
//
// export function removeDirectiveNameFromPos(pos = ''){
//   return pos.split('.').slice(1).join('.');
// }


export function getDirectiveNodeName(node){
  switch(node.tag){
    case 'core-system-column' :
      return 'Column';
    case 'core-system-row' :
      return 'Row';
    case 'core-system-grid' :
      return 'Grid';
    case 'core-system-layer' :
      return 'Layer';
  }

  return typeof node === 'string' ? 'String' : node.tag;
}


export function hasCurationInEditingRules(rule){
  let {props = {}} = rule;

  let propKeys = Object.keys(props);

  let prop;
  for(let i = 0; i < propKeys.length ; i++ ){
    prop = props[propKeys[i]];

    if( prop.type === 'curation-source' ){
      return propKeys[i];
    }
  }

  return null;
}

