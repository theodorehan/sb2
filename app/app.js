/**
 * app.js
 *
 * This is the entry file for the application, only setup and boilerplate
 * code.
 */

// Needed for redux-saga es6 generator support
import 'babel-polyfill';
import "font-awesome/css/font-awesome.css";
import "font-awesome/fonts/fontawesome-webfont.woff2";
import "font-awesome/fonts/fontawesome-webfont.eot";
import "font-awesome/fonts/fontawesome-webfont.svg";
import "font-awesome/fonts/fontawesome-webfont.ttf";
import "font-awesome/fonts/fontawesome-webfont.woff";

// Import all the third party stuff
import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
// import {applyRouterMiddleware} from 'react-router';
import {ConnectedRouter} from 'react-router-redux';

import {
  createBrowserHistory,
} from 'history';
// import {
//   Router
// } from 'react-router-dom';


import App from './containers/App';

import {syncHistoryWithStore} from 'react-router-redux';
import FontFaceObserver from 'fontfaceobserver';
// import useScroll from 'react-router-scroll/lib/useScroll';
import 'sanitize.css/sanitize.css';

import injectTapEventPlugin from 'react-tap-event-plugin';
injectTapEventPlugin();

// Import root app
import GlobalWrapper from 'containers/GlobalWrapper';

// Import selector for `syncHistoryWithStore`
import {makeSelectLocationState} from 'containers/GlobalWrapper/selectors';

// Import Language Provider
import LanguageProvider from 'containers/LanguageProvider';
// import { LocaleProvider } from 'antd';


// Load the favicon, the manifest.json file and the .htaccess file
/* eslint-disable import/no-webpack-loader-syntax */
import '!file-loader?name=[name].[ext]!./images/favicon.ico';
import '!file-loader?name=[name].[ext]!./images/icon-72x72.png';
import '!file-loader?name=[name].[ext]!./images/icon-96x96.png';
import '!file-loader?name=[name].[ext]!./images/icon-120x120.png';
import '!file-loader?name=[name].[ext]!./images/icon-128x128.png';
import '!file-loader?name=[name].[ext]!./images/icon-144x144.png';
import '!file-loader?name=[name].[ext]!./images/icon-152x152.png';
import '!file-loader?name=[name].[ext]!./images/icon-167x167.png';
import '!file-loader?name=[name].[ext]!./images/icon-180x180.png';
import '!file-loader?name=[name].[ext]!./images/icon-192x192.png';
import '!file-loader?name=[name].[ext]!./images/icon-384x384.png';
import '!file-loader?name=[name].[ext]!./images/icon-512x512.png';
import '!file-loader?name=[name].[ext]!./manifest.json';
import 'file-loader?name=[name].[ext]!./.htaccess'; // eslint-disable-line import/extensions
/* eslint-enable import/no-webpack-loader-syntax */

import configureStore from './store';
import initializer from './initializer';

// Import i18n messages
import {translationMessages} from './i18n';

// Import CSS reset and Global Styles
import './global-styles';

// Import routes
import createRoutes from './routes';


let browserHistory = createBrowserHistory();

// Observe loading of Open Sans (to remove open sans, remove the <link> tag in
// the index.html file and this observer)
const openSansObserver = new FontFaceObserver('Open Sans', {});

// When Open Sans is loaded, add a font-family using Open Sans to the body
openSansObserver.load().then(() => {
  document.body.classList.add('fontLoaded');
}, () => {
  document.body.classList.remove('fontLoaded');
});
// console.log(browserHistory,createHistory, createHistory());
// Create redux store with history
// this uses the singleton browserHistory provided by react-router
// Optionally, this could be changed to leverage a created history
// e.g. `const browserHistory = useRouterHistory(createBrowserHistory)();`

const initialState = initializer();
const store = configureStore(initialState, browserHistory);




const render = (messages) => {
  ReactDOM.render(
    <Provider store={store}>
        <LanguageProvider messages={messages}>
          <ConnectedRouter history={browserHistory}>
            <App />
          </ConnectedRouter>
        </LanguageProvider>
    </Provider>,
    document.getElementById('app')
  );
};

// Hot reloadable translation json files
if (module.hot) {
  // modules.hot.accept does not accept dynamic dependencies,
  // have to be constants at compile-time
  module.hot.accept('./i18n', () => {
    render(translationMessages);
  });
}

// Chunked polyfill for browsers without Intl support
if (!window.Intl) {
  (new Promise((resolve) => {
    resolve(import ('intl'));
  }))
  .then(() => Promise.all([
    import ('intl/locale-data/jsonp/en.js'),
    import ('intl/locale-data/jsonp/de.js'),
    import ('intl/locale-data/jsonp/ko.js'),
  ]))
  . then(() => render(translationMessages))
    .catch((err) => {
      throw err;
    });
} else {
  render(translationMessages);
}

// Install ServiceWorker and AppCache in the end since
// it's not most important operation and if main code fails,
// we do not want it installed
if (process.env.NODE_ENV === 'production') {
  require('offline-plugin/runtime').install(); // eslint-disable-line global-require
}


/**
 * Canvas Cross Domain Access 설정
 * 수정 금지
//  */
try{

  window.name = 'polaris';
  let domainTokens = location.hostname.split('.');
  document.domain = [domainTokens.pop(),domainTokens.pop()].reverse().join('.');
}catch(e){

}
// try{
// } catch(e){
//
// }
//
console.info('Version:', process.env.BUILDER_VERSION);


//
//
// import { Observable } from 'rxjs/Observable';
// import 'rxjs/add/observable/of';
// import 'rxjs/add/operator/map';
//
// let d = Observable.of(1,2,3).map(x => x + '!!!'); // etc
// console.log(d);
//
// d.subscribe(function(){
//   console.log(arguments);
// })
