import mapValues from 'lodash/mapValues';
import isObjectLike from 'lodash/isObjectLike';

export default function mapValuesDeep(o, func){
  let oo = func(o);

  let oType = typeof oo;
  if (oType === 'undefined') {
    return oo;
  } else if (oType === 'number') {
    return oo;
  } else if (oType === 'boolean') {
    return oo;
  } else if (oType === 'string') {
    return oo;
  } else if (oType === 'object') {
    if (oo === null) {
      return oo;
    } else if (Array.isArray(oo)) {
      return oo.map((item, i) => mapValuesDeep(item, func));
    } else {
      return mapValues(oo,(v, k) => mapValuesDeep(v, func))
    }
  }

  return oo;
}

