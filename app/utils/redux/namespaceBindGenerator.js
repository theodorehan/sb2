export default function(_ns ){
  if( !_ns ){
    throw new Error(`NS is ${JSON.stringify(_ns)}`);
  }


  let totalNSString;
  if( Array.isArray(_ns) ){
    totalNSString = _ns.join('.');
  } else {
    totalNSString = _ns;
  }

  return function(_real){
    let result = totalNSString + '.' + _real;

    return result;
  }
}
