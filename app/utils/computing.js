
export function unitToRealNumber(baseNumber, number){
  if( typeof number != 'number' ){
    let unit = number.replace(/^\d+/, '');
    let realNumber = parseFloat(number.replace(unit, ''));

    switch(unit){
      case "%":
        return (realNumber / 100) * baseNumber;
      default :
        return number;
    }
  }

  return number;
}
