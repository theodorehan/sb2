class NotificationUtils {

  static build(successNotificationActive, successTitleText, successMessageText, errorNotificationActive, errorTitleText, errorMessageText) {
    let notificationOptions = {
      successNotificationActive: successNotificationActive,
      successTitleText: successTitleText,
      successMessageText: successMessageText,
      errorNotificationActive: errorNotificationActive,
      errorTitleText: errorTitleText,
      errorMessageText: errorMessageText,
    };

    return notificationOptions;
  }
}

export default NotificationUtils;
