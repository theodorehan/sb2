import {MetaNode} from 'abstract-component-node';
import path from 'path';

export default class RouteNode extends MetaNode {
  constructor(json, pos, parent){
    super(json, pos, parent);
  }

  getDirectivePath(){


    let routePath =  this.getRoutePath();

    if( routePath === '/' ){
      return 'root/index';
    } else {
      return 'root/' + routePath.replace(/:/g,'@').replace(/^\//,'') + (this.group ? '/index':'');
    }
  }

  getRoutePath(){
    let parentGraph = this.getLinealDescentList();

    let pathTokens = parentGraph.map((node) => node.path);

    let routePath = path.join('/',pathTokens.join('/'));


    return routePath;
  }
}
