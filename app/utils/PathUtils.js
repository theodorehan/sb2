import _ from 'underscore';

class PathUtils {

  /*
   *
   * Ex) paramsArray = ['a=1&b=2', 'c=3&d=4'] 일 경우 a=1&b=2&c=3&d=4 로 리턴.
   *
   * */
  static paramsMergeBuild(paramsArray) {
    let newParams = [];

    if(!_.isArray(paramsArray)) {
      console.warn('[PathUtils] 의 [paramsMergeBuild] Function 의 paramsArray 인자값이 Array가 아닙니다.');
    } else {
      for(let i=0; i < _.size(paramsArray); i++) {
        let params = paramsArray[i];
        let splitParams = params.split('&');

        for(let j=0; j < _.size(splitParams); j++) {
          let param = splitParams[j];
          if(!_.contains(newParams, param)) {
            newParams.push(param);
          }
        }
      }
    }

    return newParams.join('&');
  }

  /*
   *
   * Ex) params = 'a=1&b=2&c=3' 일 경우 { a: 1, b: 2, c: 3 } 로 리턴.
   *
   * */
  static paramsToObjectBuild(params) {
    let paramObject = {};

    if(_.isUndefined(params)) {
      console.warn('[PathUtils] 의 [paramsToObjectBuild] Function 의 params 인자값이 Undefined 입니다.');
    } else if(_.isNull(params)) {
      console.warn('[PathUtils] 의 [paramsToObjectBuild] Function 의 params 인자값이 Null 입니다.');
    } else if(_.isObject(params)) {
      console.warn('[PathUtils] 의 [paramsToObjectBuild] Function 의 params 인자값이 Object 입니다.');
    } else if(_.isArray(params)) {
      console.warn('[PathUtils] 의 [paramsToObjectBuild] Function 의 params 인자값이 Array 입니다.');
    } else {
      params = params.replace('?', '');
      let paramArray = params.split('&');
      for(let i=0; i < _.size(paramArray); i++) {
        let param = paramArray[i];
        let paramName = null;
        let paramValue = null;

        if(!_.isUndefined(param) && param.indexOf('=') != -1) {
          paramName = param.split('=')[0];
          paramValue = param.split('=')[1];
        }

        if(!_.isNull(paramName) && !_.isEmpty(paramName) && !_.isNull(paramValue) && !_.isEmpty(paramValue)) {
          paramObject[paramName] = paramValue;
        }
      }
    }

    return paramObject;
  }

  /*
  *
  * Ex) path = /DefaultView?tid=contents&id=@{id} 일 경우 @{id}의 값을 data 객체의 id 값으로 대체 한다.
  *
  * */
  static pathParamsDataBuild(data, path) {
    let pathLength = path.length;
    let originalPath = path.substring(0, path.indexOf('?'));
    let paramPath = path.substring(path.indexOf('?') + 1, pathLength);

    let newParams = [];
    let params = paramPath.split('&');

    for (let i = 0; i < _.size(params); i++) {
      let param = params[i];
      let paramName = null;
      let paramValue = null;

      if(!_.isUndefined(param) && param.indexOf('=') != -1) {
        paramName = param.split('=')[0];
        paramValue = param.split('=')[1];
      }

      if(!_.isNull(paramName) && !_.isEmpty(paramName) && !_.isNull(paramValue) && !_.isEmpty(paramValue)) {
        if (paramValue.indexOf('@') == -1) {
          newParams.push(paramName + '=' + paramValue);
        } else {
          let dataKey = paramValue.substring(paramValue.indexOf('@{') + 2, paramValue.indexOf('}'));
          let dataValue = data[dataKey];
          if (_.isUndefined(dataValue)) dataValue = '';
          newParams.push(paramName + '=' + dataValue);
        }
      }
    }

    return originalPath + '?' + newParams.join('&');
  }

  /*
   *
   * Ex) params = category=@{id}&genre=@{genre} 일 경우 category=[data.id]&genre=[data.genre] 으로 리턴한다.
   *
   * */
  static paramsDataBuild(data, params) {
    let newParams = [];

    if(_.isUndefined(data)) {
      console.warn('[PathUtils] 의 [paramsDataBuild] Function 의 data 인자값이 Undefined 입니다.');
    } else if(_.isNull(data)) {
      console.warn('[PathUtils] 의 [paramsDataBuild] Function 의 data 인자값이 Null 입니다.');
    } else if(_.isArray(data)) {
      console.warn('[PathUtils] 의 [paramsDataBuild] Function 의 data 인자값이 Array 입니다.');
    } else {
      let paramArray = params.split('&');
      for(let i=0; i < _.size(paramArray); i++) {
        let param = paramArray[i];
        let paramName = null;
        let paramValue = null;

        if(!_.isUndefined(param) && param.indexOf('=') != -1) {
          paramName = param.split('=')[0];
          paramValue = param.split('=')[1];
        }

        if(!_.isNull(paramName) && !_.isEmpty(paramName) && !_.isNull(paramValue) && !_.isEmpty(paramValue)) {
          if (paramValue.indexOf('@') != -1) {
            let dataKey = paramValue.substring(paramValue.indexOf('@{') + 2, paramValue.indexOf('}'));
            let dataValue = data[dataKey];

            newParams.push(paramName+'='+dataValue);
          } else {
            newParams.push(paramName+'='+paramValue);
          }
        }
      }
    }

    return newParams.join('&');
  }

  /*
   *
   * Ex) params = category=@{id}&genre=@{genre} 일 경우 { category: 'data의 id값', genre: 'data의 genre값' } 으로 리턴한다.
   *
   * */
  static paramsDataToObjectBuild(data, params) {
    let paramObject = {};
    if(_.isUndefined(data) || _.isUndefined(params)) return paramObject;

    let paramArray = params.split('&');
    for(let i=0; i < _.size(paramArray); i++) {
      let param = paramArray[i];
      let paramName = null;
      let paramValue = null;

      if(!_.isUndefined(param) && param.indexOf('=') != -1) {
        paramName = param.split('=')[0];
        paramValue = param.split('=')[1];
      }

      if(!_.isNull(paramName) && !_.isEmpty(paramName) && !_.isNull(paramValue) && !_.isEmpty(paramValue)) {
        if (paramValue.indexOf('@') != -1) {
          let dataKey = paramValue.substring(paramValue.indexOf('@{') + 2, paramValue.indexOf('}'));
          let dataValue = data[dataKey];

          paramObject[paramName] = dataValue;
        } else {
          paramObject[paramName] = paramValue;
        }
      }
    }

    return paramObject;
  }

}

export default PathUtils;
