
let directiveNameSchemaRegexp = /^ice(#|\?)(.*?)(?:@(\d+))?$/;
export function directiveNameIceSchema(directiveName){
  let schema = directiveName.match(directiveNameSchemaRegexp);

  if( schema ){
    if (schema[1] == '#'){
      return {
        method: '#',
        id: schema[2],
      }
    } else if (schema[1] == '?'){
      let metaKeys = schema[2].split(';');
      let version = schema[3];



      return {
        method: '?',
        meta1 : metaKeys[0] || null,
        meta2 : metaKeys[1] || null,
        meta3 : metaKeys[2] || null,
        version,
      };
    }
  }

  return null;
}
