/**
 * collisionDetectAllUseBoundingClientRect
 * @description RootElement 를 시작으로 트리를 순회하여 x,y 좌표에 존재하는 DOMElement 를 찾아낸다.
 * @param rootElement
 * @param _x
 * @param _y
 * @returns {Array}
 */
export function collisionDetectAllUseBoundingClientRect(rootElement, _x, _y){

  let collisions = [];

  let rect, calcW, calcH;
  // 트리 방문 함수
  recursiveVisitRect(rootElement, (dom, inheritedMaxRect)=>{

    if( dom.style.display == 'none' || dom.style.visibility == 'hidden' ) return true;


    rect = dom.getBoundingClientRect();





    // console.log(rect,inheritedMaxRect,_x, _y);
    // console.log('>',inheritedMaxRect)
    if( inheritedMaxRect ){

      calcW = rect.width;
      calcH = rect.height;
      if( rect.left < 0 ){
        calcW += rect.left;
      }

      if( rect.top < 0 ){
        calcH += rect.top;
      }

      // 교차 상자 계산
      rect = {
        left : Math.max(rect.left, inheritedMaxRect.left),
        top : Math.max(rect.top, inheritedMaxRect.top),
        width : Math.min(calcW, inheritedMaxRect.width),
        height : Math.min(calcH, inheritedMaxRect.height),
      }
    }

    if( checkItIsBoundary(rect, _x, _y) ){


      collisions.push(dom);




    }

    let comSt = window.getComputedStyle(dom);

    if( comSt.overflow != 'visible' ){
      return rect;
    }

    if( inheritedMaxRect ){
      return rect;
    }
  });

  return collisions;
}


/**
 * filterHasEmptyChildren
 * @description 입력된 DOMElement 들 중 자식을 가지지 않은 DOMElement 만 필터링한다.
 * @param _domList
 */
export function filterHasEmptyChildren(_domList){
  return _domList.filter((dom) => dom.children.length == 0);
}


/**
 * classificationBloodLine
 * @description 입력된 DOMElement 혈통원들 중 마지막 후손들을 찾아낸다.
 * @example [A의 부모의 부모, A의 부모, A, B의 부모, B] => [A,B]
 * @param domNodeList
 * @returns {Array}
 */
export function classificationBloodLine( domNodeList , x, y){
  let lastDescendantsOflineages = []; // 혈통의 마지막 후손들


  for( let i = 0; i < domNodeList.length ; i++ ){

    let maybeMyDescendant = domNodeList[i+1];
    if( !( domNodeList[i] === (maybeMyDescendant && maybeMyDescendant.parentNode)) ){
      lastDescendantsOflineages.push(domNodeList[i]);
    }
  }


  // lastDescendantsOflineages = lastDescendantsOflineages.filter((dom) => {
  //
  //   return true;
  // })



  return lastDescendantsOflineages;
}


/**
 * zContextRace
 * @description zContextRace 는 입력된 DOMElement 들 중 제일 위에 쌓이는 DOMElement 를 찾아 낸다.
 * 각 입력된 DOMElement 의 부모줄기를 따라 역으로 쌓임 컨텍스트를 추적하여 최종적으로 위에 쌓이는 DOM을 찾는다.
 * @param _racePlayers
 * @returns {*}
 */
export function zContextRace(_racePlayers){

  if( _racePlayers.length <= 1 ){
    return _racePlayers[0];
  }

  let zStackList = _racePlayers.map(function(_dom){
    return reverseTracingZContextStack(_dom);
  });

  let eachStackLengthArray = zStackList.map((arr)=> arr.length );

  let maxLength = Math.max.apply(null, eachStackLengthArray);

  // console.log('zStackList', zStackList, maxLength);



  let highZindex = 0;
  let highStackNumber = 0;
  let prevStackActiveNumber;
  let differentiationElement = false; // 각 스택에 평행인 컨텍스트간 오너가 동일 하다가 다른 오너가 감지 되었을 떄
  for(let stackZSeeker =0; stackZSeeker< maxLength; stackZSeeker++ ){


    highZindex = null;
    highStackNumber = null;
    prevStackActiveNumber = null;
    differentiationElement = false;
    for( let stackNumber = 0; stackNumber < zStackList.length; stackNumber++ ){

      if( zStackList[stackNumber][stackZSeeker] ){
        if( highStackNumber === null ){
          // initialize

          highZindex = zStackList[stackNumber][stackZSeeker].z;
          highStackNumber = stackNumber;
          prevStackActiveNumber = stackNumber;
        } else {
          // step after initialized
          // DOM 이 달라지면
          if(
            zStackList[stackNumber][stackZSeeker].d !==

              // 이 시점에서 prevStackActiveNumber 는 이전에 유효했던 스택 number 를 가진다.
            zStackList[prevStackActiveNumber][stackZSeeker].d ){
            differentiationElement = true;

            if( zStackList[stackNumber][stackZSeeker].z === 'auto' ){
              highZindex = highZindex++;
              highStackNumber = stackNumber;
            } else if( zStackList[stackNumber][stackZSeeker].z > highZindex ){
              highZindex = zStackList[stackNumber][stackZSeeker].z;
              highStackNumber = stackNumber;
            } else if ( highZindex === zStackList[stackNumber][stackZSeeker].z ){
              highZindex = highZindex++;
              highStackNumber = stackNumber;
            }

            prevStackActiveNumber = stackNumber;
          } else {
            // 쌓임 맥락레이스 중 같은 선상에 동일한 요소가 배정되어 있으면 마지막 주자를 highStack 으로 선정한다.
            highStackNumber = stackNumber; // 가능?
          }
        }
      }
    }

    if( differentiationElement ){
      break;
    }
  }

  // console.log(highStackNumber);

  return _racePlayers[highStackNumber];
}

export function reverseTracingZContextStack(_dom){
  let dom = _dom;
  let contextStack = []; // Integer, 'auto'
  /**
   * 쌓임 맥락이 생성되는 경우는
   * position이 relative 또는 absolute 이고 z-index 값이 "auto" 가 아닌 경우
   * position이 fixed 인 경우 ( 모바일 웹킷과 크롬 22이상 )
   * opacity 값이 1 미만일 경우 ( zIndex 는 0이며 쌓임 맥락 생성 )
   * html 태그 자체의 경우
   * z-index 값이 "auto" 가 아닌 flex 요소
   */
  let style = null;
  while(true){
    style = getComputedStyle(dom);

    if( style.position !== 'static' ){

      // fixed 와 absolute 는 zIndex 가 지정이 안 되어있어도 새로운 쌓임 컨텍스트를 생성한다.
      if( style.position == 'fixed' || style.position == 'absolute'){
        contextStack.unshift({ d : dom, z:style.zIndex});
      } else if( style.zIndex !== 'auto' ){
        contextStack.unshift({ d : dom, z:style.zIndex});
      }

    } else if ( style.opacity < 1 ){
      contextStack.unshift({ d : dom, z:0});
    } else if ( style.display == 'flex' && style.zIndex != 'auto' ){
      contextStack.unshift({ d : dom, z:style.zIndex});
    } else if ( dom.nodeName == 'HTML' ){
      contextStack.unshift({ d : dom, z:0});

      // html 태그를 만나도 루프를 벗어난다.
      break;
    }

    if( dom.parentElement ){
      dom = dom.parentElement;
    } else {
      break;
    }
  }

  return contextStack;
}

function recursiveVisit(dom, func){

  if( func(dom) === true ) return;

  if( dom.nodeName !== '#comment'){
    for( let i = 0; i < dom.children.length; i++ ){
      recursiveVisit(dom.children[i], func);
    }
  }
}


/**
 *
 * @param dom
 * @param func(dom, rect) : Object, false
 * @param rect
 */
function recursiveVisitRect(dom, func, rect){

  let boundingRectOrStop = func(dom, rect);

  if( boundingRectOrStop === true ) return;

  if( dom.nodeName !== '#comment'){
    for( let i = 0; i < dom.children.length; i++ ){
      recursiveVisitRect(dom.children[i], func, boundingRectOrStop);
    }
  }
}

export function checkItIsBoundary(rect, _x, _y) {


  if ((rect.left <= _x && rect.left + rect.width >= _x) &&
    (rect.top <= _y && rect.top + rect.height >= _y)) {
    return true;
  }
  return false;
};


/**
 *
 * @param rect
 * @param x
 * @param y
 * @returns 1 : Bottom | 0 : Top
 */
export function detectVerticalArea(rect, x, y){
  let middleY = rect.top + rect.height / 2;


  if( y > middleY ){
    return 1;
  } else {
    return -1;
  }
}


export function html5dragGetDraggableNodeInPath(domPath) {
  for(let i = 0; i > domPath.length; i++ ){
    console.log(domPath[i].getAttribute('draggable'))
    if( domPath[i].getAttribute('draggable') ){
      return domPath[i];
    }
  }
}
