import uuid from 'uuid';

export default function defineUIIDefinition(_uiiOptions){

  if( !_uiiOptions ){
    throw new Error(`Invalid UII define options.`);
  }

  if( !_uiiOptions.type ) {
    throw new Error(`UII Option is required [type] field.`);
  }

  if( !_uiiOptions.icon ) {
    throw new Error(`UII Option is required [icon] field.`);
  }

  // 없어도 됨
  // if( !_uiiOptions.title ) {
  //   throw new Error(`UII Option is required [title] field.`);
  // }

  if( _uiiOptions.type == 'unit' ){
    if( !_uiiOptions.uiiName ){
      throw new Error(`UII Option [unitName] is required, if type is "unit".`);
    }
  }



  return Object.freeze({
    identification : uuid.v4(),

    name : _uiiOptions.name, // uii 명

    type : _uiiOptions.type, // required ( button, unit )

    unitName : _uiiOptions.unitName,

    icon : _uiiOptions.icon,
    iconColor : _uiiOptions.iconColor,
    iconType : _uiiOptions.iconType, // fa, material, unit

    title : _uiiOptions.title,
    titleColor : _uiiOptions.titleColor,
    titleType : _uiiOptions.titleType || 'string' , // string, code, unit

    tooltip : _uiiOptions.tooltip,
    tooltipType : _uiiOptions.tooltipType || 'string' , // string, code

    description : _uiiOptions.description,
    descriptionType : _uiiOptions.descriptionType,


    reacting : _uiiOptions.reacting, // UII 에 입력을 주었을 때 발생할 이벤트 및 처리
    referenceKey : _uiiOptions.referenceKey,

    options : Object.freeze( Object.assign({}, _uiiOptions.options || {}) ),

    actionSheet : _uiiOptions.actionSheet,
  });
}
