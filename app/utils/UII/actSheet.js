import * as CoreConstants from '../../Core/Constants';

export default function defineActingSheet(_options) {
  return Object.freeze({
    type: _options.type,
    action: _options.action,
    options: _options.options,
  });
}

export const Types = {
  ToolInterfacePipeline : CoreConstants.ToolInterfacePipeline,
  ReduxAction : CoreConstants.ReduxAction,
  InlineAction : CoreConstants.InlineAction,
};
