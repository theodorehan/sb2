export function exitFullscreen() {
  if(document.exitFullscreen) {
    document.exitFullscreen();
  } else if(document.mozCancelFullScreen) {
    document.mozCancelFullScreen();
  } else if(document.webkitExitFullscreen) {
    document.webkitExitFullscreen();
  }
}

export function launchIntoFullscreen() {
  if(document.body.requestFullscreen) {
    document.body.requestFullscreen();
  } else if(document.body.webkitRequestFullscreen) {
    document.body.webkitRequestFullscreen();
  } else if(document.body.mozRequestFullScreen) {
    document.body.mozRequestFullScreen();
  } else if(document.body.msRequestFullscreen) {
    document.body.msRequestFullscreen();
  }
}

export function isFullscreen(){
  if(document.isFullscreen) {
    return document.isFullscreen;
  } else if(document.webkitIsFullScreen) {
    return document.webkitIsFullScreen;
  } else if(document.mozIsFullScreen) {
    return document.mozIsFullScreen;
  } else if(document.msIsFullScreen) {
    return document.msIsFullScreen;
  }
  return false;
}
