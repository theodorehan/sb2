import parseUrl from 'url-parse';
import mapValues from 'lodash/mapValues';

import {
  CoreTools,
} from './components/Toolset';

import {
  RESERVE_LAYOUT_ELEMENT_ID_BOTTOM,
  RESERVE_LAYOUT_ELEMENT_ID_CENTRAL,
  RESERVE_LAYOUT_ELEMENT_ID_LEFT,
  RESERVE_LAYOUT_ELEMENT_ID_LEFTAREA,
  RESERVE_LAYOUT_ELEMENT_ID_RIGHT,
  RESERVE_LAYOUT_ELEMENT_ID_RIGHTAREA,
  RESERVE_LAYOUT_ELEMENT_ID_TOP,
} from './components/Studio/Constants'

import * as UIIStatements from './components/UIIStatements';
import {initialState as studioInitialState} from "./reducers/Studio/index";


export default function(){
  let rootInitialState = {};
  return rootInitialState;



  let parsedURL = parseUrl(location.href, true);
  if( parsedURL.query.hasOwnProperty('constraint-mode') ){
    rootInitialState.Studio = studioInitializer(parsedURL);
  }
  console.log(rootInitialState);
  return rootInitialState;
}



